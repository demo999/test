<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function valid_member($objmember) {
        $memberid = $objmember->getMemberid();
        $this->db->select(array('pin', 'archive', 'status', 'loginattempt', 'enable_app_access', "CONCAT(firstname,' ',middlename,' ',lastname) as name", 'gender', 'id'));
        $this->db->from(TBL_PREFIX . 'member');
        $this->db->where(array('id' => $memberid));
        return $this->db->get()->row();
    }

    function insert_member_token($objmember) {
        $arrMemberLogin = array();
        $arrMemberLogin['memberId'] = $objmember->getMemberid();
        $arrMemberLogin['token'] = $objmember->getMemberToken();
        $arrMemberLogin['created'] = $objmember->getLoginCreated();
        $arrMemberLogin['updated'] = $objmember->getLoginUpdate();
        $insert_query = $this->db->insert(TBL_PREFIX . 'member_login', $arrMemberLogin);
        if ($this->db->affected_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    function update_login_attempt($objmember) {
        $arrMember = array();
        $login_attemp_count = $objmember->getloginattempt();
        $login_attemp_count++;

        $this->db->set('loginattempt', $login_attemp_count);
        $this->db->where('id', $objmember->getMemberid());
        $this->db->update(TBL_PREFIX . 'member');
        if ($this->db->affected_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    function get_member_primary_phone($objmember) {
        $memberid = $objmember->getMemberid();
        $this->db->select(array('phone'));
        $this->db->from(TBL_PREFIX . 'member_phone');
        $this->db->where(array('memberId' => $memberid));
        $this->db->where(array('primary_phone' => 1));
        return $this->db->get()->row();
    }

    function get_member_primary_email($objmember) {
        $memberid = $objmember->getMemberid();
        $this->db->select(array('email'));
        $this->db->from(TBL_PREFIX . 'member_email');
        $this->db->where(array('memberId' => $memberid));
        $this->db->where(array('primary_email' => 1));
        return $this->db->get()->row();
    }

    function remove_tokens() {
        $this->db->where("updated < (NOW()-INTERVAL 30 MINUTE)");
        $this->db->delete(TBL_PREFIX . 'member_login');
    }

    function remove_member_tokens($objmember) {
        $this->db->where('memberId', $objmember->getMemberid());
        $this->db->delete(TBL_PREFIX . 'member_login');
    }

    function update_token($objmember) {
        $reposne = false;
        $updated = $objmember->getLoginUpdate();
        $this->db->set('updated', $updated);
        $this->db->where('memberId', $objmember->getMemberid());
        $this->db->update(TBL_PREFIX . 'member_login');
        if ($this->db->affected_rows() > 0) {
            $reposne = true;
        } else {
            $reposne = false;
        }
        return true;
    }

    function update_pin($objmember) {

        $pin = $objmember->getPin();
        $up_data = ['pin' => $pin, 'loginattempt' => 0, 'otp' => '', 'otp_expire_time' => ''];

        $this->db->set($up_data);
        $this->db->where('id', $objmember->getMemberid());
        $this->db->update(TBL_PREFIX . 'member');
        if ($this->db->affected_rows() > 0) {
            $reposne = true;
        } else {
            $reposne = false;
        }
        return true;
    }

    function update_otp_details($objmember) {
        $reposne = false;
        $updated = $objmember->getLoginUpdate();
        $this->db->set('otp', $objmember->getOtp());
        $this->db->set('otp_expire_time', $objmember->getOtp_expire_time());
        $this->db->where('id', $objmember->getMemberid());

        $this->db->update(TBL_PREFIX . 'member');
        if ($this->db->affected_rows() > 0) {
            $reposne = true;
        } else {
            $reposne = false;
        }
        return true;
    }

    // After login check login status every request
    function member_login_status($objmember) {

        $reposne = false;

        // Remove all rows updated time before 20 minuts
        $this->remove_tokens();

        // Get details of token members
        $memberid = $objmember->getMemberid();
        $token = $objmember->getMemberToken();
        $updated = $objmember->getLoginUpdate();

        $this->db->select(array('updated'));
        $this->db->from(TBL_PREFIX . 'member_login');
        $this->db->where(array('memberId' => $memberid));
        $this->db->where(array('token' => $token));
        $response_status = $this->db->get()->row();

        if (!empty($response_status)) {
            // Update token date time
            if ($this->update_token($objmember) > 0) {
                $reposne = true;
            } else {
                $reposne = false;
            }
        } else {
            $reposne = false;
        }
        return $reposne;
    }

    function get_member_otp($objmember) {
        // Get details of token members
        $memberid = $objmember->getMemberid();

        $this->db->select(array('otp', 'otp_expire_time'));
        $this->db->from(TBL_PREFIX . 'member');
        $this->db->where(array('id' => $memberid));

        return $this->db->get()->row();
    }

    //Get Member Info
    function getmemberInfo($objmember) {
        $member_details = array();

        // Profile details
        $tbl_member = TBL_PREFIX . 'member';
        $tbl_member_phone = TBL_PREFIX . 'member_phone';
        $tbl_member_email = TBL_PREFIX . 'member_email';
        $tbl_member_address = TBL_PREFIX . 'member_address';
        $tbl_member_kin = TBL_PREFIX . 'member_kin';

        $member_column = array('tbl_member.id as OCS_id', 'tbl_member.companyId as companyId', 'tbl_member.firstname', 'tbl_member.lastname', 'tbl_member.middlename', 'tbl_member.gender', 'tbl_member_phone.phone', 'tbl_member_email.email');

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $member_column)), false);
        $this->db->from(TBL_PREFIX . 'member');
        $this->db->join(TBL_PREFIX . 'member_phone', 'tbl_member_phone.memberId = tbl_member.id', 'left');
        $this->db->join(TBL_PREFIX . 'member_email', 'tbl_member_email.memberId = tbl_member.id', 'left');

        $this->db->where(array('tbl_member.id' => $objmember->getMemberid()));
        $this->db->where(array('tbl_member_phone.primary_phone' => '1'));
        $this->db->where(array('tbl_member_email.primary_email' => '1'));
        $response_status = $this->db->get()->row();
        #last_query();
        if (count($response_status) > 0) {
            $member_details['profile'] = $response_status;

            // Member Address			
            $member_column_address = array($tbl_member_address . '.id as address_id', $tbl_member_address . '.street', $tbl_member_address . '.city', $tbl_member_address . '.postal', $tbl_member_address . '.state', $tbl_member_address . '.primary_address');

            $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $member_column_address)), false);
            $this->db->from($tbl_member_address);
            $this->db->where(array($tbl_member_address . '.memberId' => $objmember->getMemberid()));
            $query = $this->db->get();
            $response_address = $query->result();
            $member_details['address'] = $response_address;

            // Kin Details
            $member_column_kin = array($tbl_member_kin . '.firstname as firstname', $tbl_member_kin . '.lastname as lastname', $tbl_member_kin . '.relation', $tbl_member_kin . '.phone', $tbl_member_kin . '.email', $tbl_member_kin . '.primary_kin');
            $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $member_column_kin)), false);
            $this->db->from($tbl_member_kin);
            $this->db->where(array($tbl_member_kin . '.memberId' => $objmember->getMemberid()));
            $query = $this->db->get();
            $response_places = $query->result();
            $member_details['kin'] = $response_places;

            // Places
            $member_column = array('tbl_member_place.placeId as placeId', 'tbl_member_place.type as type,tbl_place.name as placename');
            $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $member_column)), false);
            $this->db->from(TBL_PREFIX . 'member_place');
            $this->db->join(TBL_PREFIX . 'place', 'tbl_member_place.placeId = tbl_place.id', 'left');
            $this->db->where(array('tbl_member_place.memberId' => $objmember->getMemberid()));
            $query = $this->db->get();
            $response_places = $query->result();
            $member_details['places'] = $response_places;

            // Activity
            $member_column = array('tbl_member_activity.activityId as activityId', 'tbl_member_activity.type as type,tbl_activity.name as activityname');
            $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $member_column)), false);
            $this->db->from(TBL_PREFIX . 'member_activity');
            $this->db->join(TBL_PREFIX . 'activity', 'tbl_member_activity.activityId = tbl_activity.id', 'left');
            $this->db->where(array('tbl_member_activity.memberId' => $objmember->getMemberid()));
            $query = $this->db->get();
            #last_query();
            $response_activity = $query->result();
            $member_details['activity'] = $response_activity;
        }
        return $member_details;
    }

    // Create App Feedback
    function insert_feedback($objFeedback) {
        $response = false;
        $tbl_app_feedback = TBL_PREFIX . "app_feedback";
        $arrMemberFeedback = array();
        $arrMemberFeedback['memberid'] = $objFeedback->getMemberId();
        $arrMemberFeedback['feedback'] = $objFeedback->getFeedback();
        $arrMemberFeedback['rating'] = $objFeedback->getRating();
        $arrMemberFeedback['created'] = $objFeedback->getCreatedDate();
        $insert_query = $this->db->insert($tbl_app_feedback, $arrMemberFeedback);

        $notification_ary = array('userId' => $objFeedback->getMemberId(), 'user_type' => 1, 'title' => 'Mobile App feedback', 'shortdescription' => 'Mobile App feedback submit by MemberId- ' . $objFeedback->getMemberId(), 'created' => DATE_TIME, 'sender_type' => 1);
        $this->db->insert('tbl_notification', $notification_ary);

        if ($this->db->affected_rows() > 0) {
            $response = true;
        }
        return $response;
    }

    function updatememberInfo_old($objMember) {
        // update main info
        $member_id = $objMember->getMemberid();
        $firstname = $objMember->getFirstname();
        $middlename = $objMember->getMiddlename();
        $lastname = $objMember->getLastname();
        $email_arr = $objMember->getMemberEmail();
        $phone_arr = $objMember->getMemberPhone();
        $this->db->trans_begin(); // Query will be rolled back if true
        $reposne = false;
        //$updated=$objmember->getLoginUpdate();		
        $this->db->set('firstname', $firstname);
        $this->db->set('middlename', $middlename);
        $this->db->set('lastname', $lastname);
        $this->db->where('id', $member_id);
        $this->db->update(TBL_PREFIX . 'member');

        $bulk_insert = array();
        if (count($email_arr) > 0) {
            $tableEmail = TBL_PREFIX . 'member_email';
            foreach ($email_arr as $email) {
                if (isset($email->id) && $email->id > 0) {
                    $type = 'update';
                    $wherekey = 'id';
                    $arrMemberEmail = array("email" => $email->email, "primary_email" => $email->isprimary);
                    if ($email->isprimary == 1) {
                        $update_data = array('primary_email' => 2);
                        $this->db->where('memberId', $objMember->getMemberid());
                        $this->db->update($tableEmail, $update_data);
                    }
                    $this->member_bulk_record($tableEmail, $type, $wherekey, $email->id, $arrMemberEmail);
                } else {
                    if ($email->isprimary == 1) {
                        $update_data = array('primary_email' => 2);
                        $this->db->where('memberId', $objMember->getMemberid());
                        $this->db->update($tableEmail, $update_data);
                    }
                    $arrMemberEmail = array("email" => $email->email, "primary_email" => $email->isprimary, 'memberId' => $objMember->getMemberid());
                    $this->db->insert($tableEmail, $arrMemberEmail);
                    #$bulk_insert[]=$arrMemberEmail;
                }
            }
            /* if(count($bulk_insert)>0){		
              $this->member_bulk_record($tableEmail,'insert',null,null,$bulk_insert);
              } */
        }
        $bulk_insert = array();
        if (count($phone_arr) > 0) {
            $tablePhone = TBL_PREFIX . 'member_phone';
            foreach ($phone_arr as $phone) {
                if ((isset($phone->id)) && $phone->id > 0) {
                    $type = 'update';
                    $wherekey = 'id';
                    $arrMemberPhone = array("phone" => $phone->phone, "primary_phone" => $phone->isprimary);
                    if ($phone->isprimary == 1) {
                        $update_data = array('primary_phone' => 2);
                        $this->db->where('memberId', $objMember->getMemberid());
                        $this->db->update($tablePhone, $update_data);
                    }
                    $this->member_bulk_record($tablePhone, $type, $wherekey, $phone->id, $arrMemberPhone);
                } else {
                    if ($phone->isprimary == 1) {
                        $update_data = array('primary_phone' => 2);
                        $this->db->where('memberId', $objMember->getMemberid());
                        $this->db->update($tablePhone, $update_data);
                    }
                    $arrMemberPhone = array("phone" => $phone->phone, "primary_phone" => $phone->isprimary, 'memberId' => $objMember->getMemberid());
                    $this->db->insert($tablePhone, $arrMemberPhone);
                    #$bulk_insert[]=$arrMemberPhone;
                }
            }

            /* if(count($bulk_insert)>0){		
              $this->member_bulk_record($tablePhone,'insert',null,null,$bulk_insert);
              } */
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
            $reposne = true;
        }

        return $reposne;
    }

    function updatememberInfo($objMember) {
        ob_start();
        $this->load->model('Basic_model');
        // update main info
        $member_id = $objMember->getMemberid();
        $firstname = $objMember->getFirstname();
        $middlename = $objMember->getMiddlename();
        $lastname = $objMember->getLastname();
        $email_arr = $objMember->getMemberEmail();
        $phone_arr = $objMember->getMemberPhone();
        $this->db->trans_begin(); // Query will be rolled back if true
        $reposne = false;
        //$updated=$objmember->getLoginUpdate();
        $approval_record = array();
        $profile_ary = array('firstname' => $firstname, 'middlename' => $middlename, 'lastname' => $lastname, 'id' => $member_id);
        $approval_record['profile_ary'] = $profile_ary;
        $approval_record['email_ary'] = $email_arr;
        $approval_record['phone_ary'] = $phone_arr;

        $this->db->set('firstname', $firstname);
        $this->db->set('middlename', $middlename);
        $this->db->set('lastname', $lastname);
        $this->db->where('id', $member_id);
        $this->db->update(TBL_PREFIX . 'member');

        $bulk_insert = array();
        if (count($email_arr) > 0) {
            $tableEmail = 'member_email';
            $primary_email = array();
            $secondary_email = array();
            $temp_secondary_email = array();
            $temp_primary_email = array();
            $this->Basic_model->delete_records($tableEmail, $where = array('memberId' => $objMember->getMemberid()));
            foreach ($email_arr as $email) {
                if ($email->isprimary == 1 && count($primary_email) == 0) {
                    $temp_primary_email['primary_email'] = 1;
                    $temp_primary_email['email'] = $email->email;
                    $temp_primary_email['memberId'] = $objMember->getMemberid();
                    $primary_email[] = $temp_primary_email;
                } else {
                    $temp_secondary_email['primary_email'] = 2;
                    $temp_secondary_email['email'] = $email->email;
                    $temp_secondary_email['memberId'] = $objMember->getMemberid();
                    $secondary_email [] = $temp_secondary_email;
                }
            }
            if (!empty($primary_email))
                $this->Basic_model->insert_records($tableEmail, $primary_email, TRUE);
            else
                $secondary_email[0]['primary_email'] = 1;

            if (!empty($secondary_email))
                $this->Basic_model->insert_records($tableEmail, $secondary_email, TRUE);
        }

        if (count($phone_arr) > 0) {
            $tablePhone = 'member_phone';
            $primary_ph = array();
            $secondary_ph = array();
            $temp_secondary_ph = array();
            $temp_primary_ph = array();
            $this->Basic_model->delete_records($tablePhone, $where = array('memberId' => $objMember->getMemberid()));
            foreach ($phone_arr as $phone) {
                if ($phone->isprimary == 1 && count($primary_ph) == 0) {
                    $temp_primary_ph['primary_phone'] = 1;
                    $temp_primary_ph['phone'] = $phone->phone;
                    $temp_primary_ph['memberId'] = $objMember->getMemberid();
                    $primary_ph[] = $temp_primary_ph;
                } else {
                    $temp_secondary_ph['primary_phone'] = 2;
                    $temp_secondary_ph['phone'] = $phone->phone;
                    $temp_secondary_ph['memberId'] = $objMember->getMemberid();
                    $secondary_ph [] = $temp_secondary_ph;
                }
            }
            if (!empty($primary_ph))
                $this->Basic_model->insert_records($tablePhone, $primary_ph, TRUE);
            else
                $secondary_ph[0]['primary_phone'] = 1;

            if (!empty($secondary_ph))
                $this->Basic_model->insert_records($tablePhone, $secondary_ph, TRUE);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
            $approval_data = array('userId' => $objMember->getMemberid(), 'user_type' => 1, 'approval_area' => 'UpdateMemberInfo', 'approval_content' => json_encode($approval_record), 'created' => DATE_TIME, 'pin' => 0);
            $this->db->insert('tbl_approval', $approval_data);

            $notification_ary = array('userId' => $objMember->getMemberid(), 'user_type' => 1, 'title' => 'Update Profile Detail', 'shortdescription' => 'Update Member Profile Detail', 'created' => DATE_TIME, 'sender_type' => 1);
            $this->db->insert('tbl_notification', $notification_ary);
            $reposne = true;
        }

        return $reposne;
    }

    function member_bulk_record($table, $type, $wherekey, $wherevalue, $memberData) {
        if ($type == 'insert') {
            $insert_query = $this->db->insert_batch($table, $memberData);
            if ($this->db->affected_rows() > 0) {
                return 1;
            } else {
                return 0;
            }
        } else {

            $this->db->where($wherekey, $wherevalue);
            $this->db->update($table, $memberData);
        }
    }

    function updatemember_activity_places($objMember) {
        $reposne = false;

        $tableMemberActivity = TBL_PREFIX . 'member_activity';
        $tableMemberPlace = TBL_PREFIX . 'member_place';

        $activity_arr = $objMember->getMemberActivity();
        $place_arr = $objMember->getMemberPlace();

        $this->db->trans_begin(); // Query will be rolled back if true
        // Remove all activity and places using member id
        $this->db->where('memberId', $objMember->getMemberid());
        $this->db->delete($tableMemberActivity);

        $this->db->where('memberId', $objMember->getMemberid());
        $this->db->delete($tableMemberPlace);


        $bulk_insert = array();
        if (count($activity_arr) > 0) {
            foreach ($activity_arr as $activity) {
                $arrActivity = array("activityId" => $activity->id, "type" => $activity->type, 'memberId' => $objMember->getMemberid());
                $bulk_insert[] = $arrActivity;
            }
            if (count($bulk_insert) > 0) {
                $this->member_bulk_record($tableMemberActivity, 'insert', null, null, $bulk_insert);

                $approval_data = array('userId' => $objMember->getMemberid(), 'user_type' => 1, 'approval_area' => 'UpdateMemberActivity', 'approval_content' => json_encode($bulk_insert), 'created' => DATE_TIME, 'pin' => 0);
                $this->db->insert('tbl_approval', $approval_data);

                $notification_ary = array('userId' => $objMember->getMemberid(), 'user_type' => 1, 'title' => 'Update Member Activity', 'shortdescription' => 'Update Member Activity', 'created' => DATE_TIME, 'sender_type' => 1);
                $this->db->insert('tbl_notification', $notification_ary);
            }
        }

        $bulk_insert = array();
        if (count($place_arr) > 0) {
            foreach ($place_arr as $places) {
                $arrActivity = array("placeId" => $places->id, "type" => $places->type, 'memberId' => $objMember->getMemberid());
                $bulk_insert[] = $arrActivity;
            }
            if (count($bulk_insert) > 0) {
                $this->member_bulk_record($tableMemberPlace, 'insert', null, null, $bulk_insert);

                $approval_data = array('userId' => $objMember->getMemberid(), 'user_type' => 1, 'approval_area' => 'UpdateMemberPlaces', 'approval_content' => json_encode($bulk_insert), 'created' => DATE_TIME, 'pin' => 0);
                $this->db->insert('tbl_approval', $approval_data);

                $notification_ary = array('userId' => $objMember->getMemberid(), 'user_type' => 1, 'title' => 'Update Member Places', 'shortdescription' => 'Update Member Places', 'created' => DATE_TIME, 'sender_type' => 1);
                $this->db->insert('tbl_notification', $notification_ary);
            }
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
            $reposne = true;
        }
        return $reposne;
    }

    function getMemberQualification($objMemberQly) {

        $member_details = array();
        // Profile details
        $tbl_memberQual = TBL_PREFIX . 'member_qualification';
        $tbl_member = TBL_PREFIX . 'member';

        $member_column = array($tbl_member . '.profile_image', $tbl_member . '.id as oncall_id', $tbl_member . '.firstname', $tbl_member . '.middlename', $tbl_member . '.lastname');
        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $member_column)), false);
        $this->db->from($tbl_member);
        $this->db->where(array($tbl_member . '.status' => '1'));
        $this->db->where(array($tbl_member . '.archive' => '0'));
        $this->db->where(array($tbl_member . '.id' => $objMemberQly->getMemberid()));
        $response_member = $this->db->get()->row();
        if (count($response_member) > 0) {

            $member_details['profile'] = $response_member;

            $member_column_qual = array($tbl_memberQual . '.id', $tbl_memberQual . '.memberId as oncall_id', $tbl_memberQual . '.title', $tbl_memberQual . '.expiry', $tbl_memberQual . '.created', $tbl_memberQual . '.can_delete', $tbl_memberQual . '.filename');

            $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $member_column_qual)), false);
            $this->db->from($tbl_memberQual);
            $this->db->where(array($tbl_memberQual . '.memberId' => $objMemberQly->getMemberid()));
            $this->db->where(array($tbl_memberQual . '.archive' => '0'));
            $query = $this->db->get();
            $response_qual = $query->result();
            $num_rows = $query->num_rows();
            if ($num_rows > 0) {
                $member_details['qualification'] = $response_qual;
            }
        } else {
            $member_details = array('Record Not Found');
        }
        return $member_details;
    }

    function UpdateMemberQualification($objMemberQual) {
        $qualificationId = $objMemberQual->getMemberqualificationid();
        $reposne = false;
        $tableQual = TBL_PREFIX . 'member_qualification';
        #pr($objMemberQual);
        if ($qualificationId > 0) {
            $expiry = date('Y-m-d', strtotime($objMemberQual->getExpiry()));
            $arrMemberQual = array();
            $arrMemberQual['title'] = $objMemberQual->getTitle();
            $arrMemberQual['memberId'] = $objMemberQual->getMemberid();
            $arrMemberQual['filename'] = $objMemberQual->getFilename();
            $arrMemberQual['expiry'] = $expiry;
            $this->db->where('id', $qualificationId);
            $this->db->update($tableQual, $arrMemberQual);
            #last_query();					
            if ($this->db->affected_rows() >= 0) {
                $reposne = true;
            }
        } else {
            $expiry = date('Y-m-d', strtotime($objMemberQual->getExpiry()));
            $arrMemberQual = array();
            $arrMemberQual['title'] = $objMemberQual->getTitle();
            $arrMemberQual['memberId'] = $objMemberQual->getMemberid();
            $arrMemberQual['created'] = $objMemberQual->getCreated();
            $arrMemberQual['filename'] = $objMemberQual->getFilename();
            $arrMemberQual['expiry'] = $expiry;
            $insert_query = $this->db->insert($tableQual, $arrMemberQual);
            #last_query();					
            if ($this->db->affected_rows() > 0) {
                $reposne = true;
            }
        }
        return $reposne;
    }

    function UpdateMemberAddress($objMemberAddress) {
        $response = array();
        $addressId = $objMemberAddress->getMemberAddressId();
        $reposne = false;
        $tableAddress = TBL_PREFIX . 'member_address';

        if ($addressId > 0) {
            $arrMemberAdd = array();
            $arrMemberAdd['street'] = $objMemberAddress->getStreet();
            $arrMemberAdd['city'] = $objMemberAddress->getCity();
            $arrMemberAdd['postal'] = $objMemberAddress->getPostal();
            $arrMemberAdd['memberId'] = $objMemberAddress->getMemberid();
            $arrMemberAdd['primary_address'] = $objMemberAddress->getPrimaryAddress();
            $arrMemberAdd['state'] = $objMemberAddress->getState();
            if ($objMemberAddress->getPrimaryAddress() == 1) {
                $update_data = array('primary_address' => 0);
                $this->db->where('memberId', $objMemberAddress->getMemberid());
                $this->db->update($tableAddress, $update_data);
                #last_query();
            }
            $this->db->where('id', $addressId);
            $this->db->update($tableAddress, $arrMemberAdd);
            if ($this->db->affected_rows() >= 0) {
                $response = array('status' => true, 'success' => 'Updated successfully.');
            }
        } else {
            $arrMemberAdd = array();
            $arrMemberAdd['street'] = $objMemberAddress->getStreet();
            $arrMemberAdd['city'] = $objMemberAddress->getCity();
            $arrMemberAdd['postal'] = $objMemberAddress->getPostal();
            $arrMemberAdd['memberId'] = $objMemberAddress->getMemberid();
            $arrMemberAdd['primary_address'] = $objMemberAddress->getPrimaryAddress();
            $arrMemberAdd['state'] = $objMemberAddress->getState();
            if ($objMemberAddress->getPrimaryAddress() == 1) {
                $update_data = array('primary_address' => 0);
                $this->db->where('memberId', $objMemberAddress->getMemberid());
                $this->db->update($tableAddress, $update_data);
            }
            $insert_query = $this->db->insert($tableAddress, $arrMemberAdd);

            if ($this->db->affected_rows() > 0) {
                $response = array('status' => true, 'success' => system_msgs('insert_success'));
            }
        }
        return $response;
    }

    // Get Places and Activity

    function get_activityplaces() {
        $activity_details = array();
        $tbl_activity = TBL_PREFIX . 'activity';
        $tbl_place = TBL_PREFIX . 'place';

        // Here to get activity 
        $activity_column = array($tbl_activity . '.id', $tbl_activity . '.name');
        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $activity_column)), false);
        $this->db->from($tbl_activity);
        $query = $this->db->get();
        $activity_details['activity'] = $query->result();

        // Here to get places 
        $places_column = array($tbl_place . '.id', $tbl_place . '.name');
        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $places_column)), false);
        $this->db->from($tbl_place);
        $query = $this->db->get();
        $activity_details['places'] = $query->result();
        return $activity_details;
    }

    function get_activityplaces_new($objMember) {
        $main_ary = array();
        $memberId = $objMember->getMemberid();
        $tbl_12 = TBL_PREFIX . 'place';
        $tbl_11 = TBL_PREFIX . 'member_place';

        $tbl_10 = TBL_PREFIX . 'member_activity';
        $tbl_13 = TBL_PREFIX . 'activity';

        $arr = array('placeFav' => '', 'placeLeastFav' => '', 'activityFav' => '', 'activityLeastFav' => '');
        // get places meber
        $dt_query = $this->db->select(array($tbl_12 . ".name as label", $tbl_12 . ".id", $tbl_11 . ".type"));
        $this->db->from($tbl_12);
        $this->db->join($tbl_11, $tbl_12 . '.id = ' . $tbl_11 . '.placeId AND memberId = ' . $memberId, 'left');
        $query = $this->db->get();
        $arr['places'] = $query->result();

        if (!empty($arr['places'])) {
            $fav = $leastFav = [];
            foreach ($arr['places'] as $val) {
                if ($val->type == 1) {
                    $fav[] = $val->label;
                } elseif ($val->type == 2) {
                    $leastFav[] = $val->label;
                }
                $val->type = ($val->type) ? $val->type : 'none';
            }

            #$arr['placeFav'] = implode(', ', $fav);
            #$arr['placeLeastFav'] = implode(', ', $leastFav);
            $main_ary['placeFav'] = $fav;
            $main_ary['placeLeastFav'] = $leastFav;
        }

        // get activity member
        $dt_query = $this->db->select(array($tbl_13 . ".name as label", $tbl_13 . ".id", $tbl_10 . ".type"));
        $this->db->from($tbl_13);
        $this->db->join($tbl_10, $tbl_13 . '.id = ' . $tbl_10 . '.activityId AND memberId = ' . $memberId, 'left');
        $query = $this->db->get();
        $arr['activity'] = $query->result();

        if (!empty($arr['activity'])) {
            $fav = $leastFav = [];
            foreach ($arr['activity'] as $val) {
                if ($val->type == 1) {
                    $fav[] = $val->label;
                } elseif ($val->type == 2) {
                    $leastFav[] = $val->label;
                }
                $val->type = ($val->type) ? $val->type : 'none';
            }
            #$arr['activityFav'] = implode(', ', $fav);
            #$arr['activityLeastFav'] = implode(', ', $leastFav);
            $main_ary['activityFav'] = $fav;
            $main_ary['activityLeastFav'] = $leastFav;
        }
        return $main_ary;
    }

    function check_qualification_exist($objQualification) {
        $reposne = false;
        $Qualid = $objQualification->getMemberqualificationid();
        $this->db->select(array('title'));
        $this->db->from(TBL_PREFIX . 'member_qualification');
        $this->db->where(array('id' => $Qualid));
        $response_qual = $this->db->get()->row();
        if (count($response_qual) > 0) {
            $reposne = true;
        }
        return $reposne;
    }

    public function get_member_upcoming_shift($obj, $where_ary) {
        #print_r($obj->getShiftMember());
        //find shift  = move to app, not accepted, not assign to any member,open shift
        //upcoming shift = Member accept shift 
        $member_id = $obj->getShiftMember();
        $default_date = DATE_TIME;

        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_member = TBL_PREFIX . 'shift_member';
        $tbl_shift_location = TBL_PREFIX . 'shift_location';
        $tbl_state = TBL_PREFIX . 'state';
        $tbl_shift_participant = TBL_PREFIX . 'shift_participant';
        $tbl_participant = TBL_PREFIX . 'participant';
        $time = '';

        $this->db->where($tbl_shift . ".shift_date !=", '0000-00-00');



        //find shift
        if (!empty($where_ary)) {
            $this->db->where_not_in($tbl_shift . ".status", array(4, 5, 6, 7, 8));
            $this->db->where($tbl_shift . ".push_to_app =", 1);

            $this->db->group_start();
            if (isset($where_ary['shift_day']) && $where_ary['shift_day'] != '') {
                $day = $where_ary['shift_day'];
                $this->db->or_where($tbl_shift . ".shift_date !=", $day);
                #$sWhere .= " AND DAYNAME(tbl_shift.shift_date) = '".$day."'" ;
            }

            if (isset($where_ary['shift_start']) && $where_ary['shift_start'] != '') {
                $time = $where_ary['shift_start'];
                //@file_put_contents($_SERVER['DOCUMENT_ROOT'].'/development/application/modules/api/controllers/response.txt', $time.PHP_EOL , FILE_APPEND | LOCK_EX);
                #$this->db->or_where($tbl_shift.".start_time  >=", "STR_TO_DATE('$time', '%l:%i %p')");
                #$sWhere .= " OR tbl_shift.start_time >= STR_TO_DATE('$time', '%l:%i %p')" ;
            }

            if (isset($where_ary['date_range']) && $where_ary['date_range'] != '') {
                $date_range = explode('to', $where_ary['date_range']);
                if (empty($time)) {
                    $this->db->or_where("tbl_shift.start_time BETWEEN '" . trim($date_range[0]) . "' AND '" . trim($date_range[1]) . "'");
                } else {
                    $start_date = date('d/m/Y', strtotime($date_range[0]));
                    $my_time = "STR_TO_DATE('$start_date $time', '%d/%m/%Y %h:%i %p')";
                    $this->db->or_where("tbl_shift.start_time BETWEEN trim($my_time) AND '" . trim($date_range[1]) . "'");
                }
            }

            /* if(isset($where_ary['shift_duration']))
              {
              $duration = $where_ary['shift_duration'];
              $sWhere .= " OR tbl_shift.start_time >= STR_TO_DATE('$time', '%l:%i %p')" ;
              } */
            $this->db->group_end();
        } else {
            $this->db->where_not_in($tbl_shift . ".status", array(4, 5, 6, 8));
            #$sWhere .= " AND ".$tbl_shift.".shift_date >= '".$default_date."'";
            $this->db->where($tbl_shift . ".start_time >", $default_date);
        }

        $select_column = array($tbl_shift . ".id as shift_id", $tbl_shift . ".shift_date as start_date", $tbl_shift . ".status", $tbl_shift . ".start_time", $tbl_shift . ".end_time", $tbl_shift_location . ".address", $tbl_shift_location . ".suburb", $tbl_shift_location . ".state", $tbl_shift_location . ".postal", $tbl_shift_location . ".lat", $tbl_shift_location . ".long", $tbl_state . ".name as state_name", $tbl_participant . ".firstname", $tbl_participant . ".middlename", $tbl_participant . ".lastname", "CONCAT(tbl_participant.firstname,' ', tbl_participant.middlename,' ',tbl_participant.lastname) AS participant_name");

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_shift);
        $this->db->select("CONCAT(MOD( TIMESTAMPDIFF(hour," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 24), ':',MOD( TIMESTAMPDIFF(minute," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 60), ' hrs') as duration");

        if (!empty($where_ary)) {
            /* $this->db->join($tbl_shift_member, $tbl_shift_member . '.shiftId = ' . $tbl_shift . '.id', 'left'); */
        } else {
            $this->db->join($tbl_shift_member, $tbl_shift_member . '.shiftId = ' . $tbl_shift . '.id AND ' . $tbl_shift_member . '.status = 3', 'left');
            #$sWhere .= "AND ".$tbl_shift_member.".memberId = ".$member_id."";
            $this->db->where($tbl_shift_member . ".memberId =", $member_id);
        }

        $this->db->join($tbl_shift_location, $tbl_shift_location . '.shiftId = ' . $tbl_shift . '.id ', 'left');
        $this->db->join($tbl_state, $tbl_state . '.id = ' . $tbl_shift_location . '.state ', 'left');
        $this->db->join($tbl_shift_participant, $tbl_shift_participant . '.shiftId = ' . $tbl_shift . '.id ', 'left');
        $this->db->join($tbl_participant, $tbl_participant . '.id = ' . $tbl_shift_participant . '.participantId ', 'left');

        $this->db->group_by($tbl_shift_location . ".id");
        #$this->db->order_by($tbl_shift . ".shift_date");
        $this->db->order_by($tbl_shift . ".start_time");
        #$this->db->where($sWhere, null, false);
        #$this->db->limit(3);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        # last_query();
        #@file_put_contents($_SERVER['DOCUMENT_ROOT'].'/development/application/modules/api/controllers/response.txt', $this->db->last_query().PHP_EOL , FILE_APPEND | LOCK_EX);
        $dataResult = $query->result();
        //$total_duration = 0.00;
        $main_ary = array();
        if (!empty($dataResult)) {
            $time = array();
            $temp = array();

            foreach ($dataResult as $val) {
                //$time[] = str_replace('hrs','',$val->duration);
                if (!in_array($val->shift_id, $temp)) {
                    $temp['shift_id'] = $val->shift_id;
                    $temp['start_date'] = $val->start_date;
                    $temp['start_time'] = $val->start_time;
                    $temp['shift_for'] = $val->participant_name;
                    $temp['address'] = $val->address . ' ' . $val->suburb . ', ' . $val->state_name . ' ' . $val->postal;
                    $temp['duration'] = $val->duration;
                    $main_ary[] = $temp;
                }
            }
            //$total_duration = add_hour_minute($time);
        }
        $return = array('data' => $main_ary, 'status' => true);
        return $return;
    }

    public function get_member_upcoming_shift_old($obj, $where_ary) {
        #print_r($obj->getShiftMember());
        //find shift  = move to app, not accepted, not assign to any member,open shift
        //upcoming shift = Member accept shift 
        $member_id = $obj->getShiftMember();
        $default_date = date('Y-m-d');

        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_member = TBL_PREFIX . 'shift_member';
        $tbl_shift_location = TBL_PREFIX . 'shift_location';
        $tbl_state = TBL_PREFIX . 'state';
        $tbl_shift_participant = TBL_PREFIX . 'shift_participant';
        $tbl_participant = TBL_PREFIX . 'participant';

        $where = '';
        $where = $tbl_shift . ".shift_date != '0000-00-00' AND " . $tbl_shift . ".status NOT IN(5,6) ";
        $sWhere = $where;


        //find shift
        if (!empty($where_ary)) {
            $sWhere .= " AND tbl_shift.push_to_app = 1";

            if (isset($where_ary['shift_day']) && $where_ary['shift_day'] != '') {
                $day = $where_ary['shift_day'];
                $sWhere .= " AND DAYNAME(tbl_shift.shift_date) = '" . $day . "'";
            }

            if (isset($where_ary['shift_start']) && $where_ary['shift_start'] != '') {
                $time = $where_ary['shift_start'];
                $sWhere .= " OR tbl_shift.start_time >= STR_TO_DATE('$time', '%l:%i %p')";
            }

            if (isset($where_ary['date_range']) && $where_ary['date_range'] != '') {
                $date_range = explode('to', $where_ary['date_range']);
                $sWhere .= " OR tbl_shift.shift_date BETWEEN '" . trim($date_range[0]) . "' AND '" . trim($date_range[1]) . "'";
            }

            /* if(isset($where_ary['shift_duration']))
              {
              $duration = $where_ary['shift_duration'];
              $sWhere .= " OR tbl_shift.start_time >= STR_TO_DATE('$time', '%l:%i %p')" ;
              } */
        } else {
            $sWhere .= " AND " . $tbl_shift . ".shift_date >= '" . $default_date . "'";
        }

        $select_column = array($tbl_shift . ".id as shift_id", $tbl_shift . ".shift_date as start_date", $tbl_shift . ".status", $tbl_shift . ".start_time", $tbl_shift . ".end_time", $tbl_shift_location . ".address", $tbl_shift_location . ".suburb", $tbl_shift_location . ".state", $tbl_shift_location . ".postal", $tbl_shift_location . ".lat", $tbl_shift_location . ".long", $tbl_state . ".name as state_name", $tbl_participant . ".firstname", $tbl_participant . ".middlename", $tbl_participant . ".lastname", "CONCAT(tbl_participant.firstname,' ', tbl_participant.middlename,' ',tbl_participant.lastname) AS participant_name");

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_shift);
        $this->db->select("CONCAT(MOD( TIMESTAMPDIFF(hour," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 24), ':',MOD( TIMESTAMPDIFF(minute," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 60), ' hrs') as duration");

        if (!empty($where_ary)) {
            /* $this->db->join($tbl_shift_member, $tbl_shift_member . '.shiftId = ' . $tbl_shift . '.id', 'left'); */
        } else {
            $this->db->join($tbl_shift_member, $tbl_shift_member . '.shiftId = ' . $tbl_shift . '.id AND ' . $tbl_shift_member . '.status = 3', 'left');
            $sWhere .= "AND " . $tbl_shift_member . ".memberId = " . $member_id . "";
        }

        $this->db->join($tbl_shift_location, $tbl_shift_location . '.shiftId = ' . $tbl_shift . '.id ', 'left');
        $this->db->join($tbl_state, $tbl_state . '.id = ' . $tbl_shift_location . '.state ', 'left');
        $this->db->join($tbl_shift_participant, $tbl_shift_participant . '.shiftId = ' . $tbl_shift . '.id ', 'left');
        $this->db->join($tbl_participant, $tbl_participant . '.id = ' . $tbl_shift_participant . '.participantId ', 'left');

        $this->db->group_by($tbl_shift_location . ".id");
        $this->db->order_by($tbl_shift . ".shift_date");
        $this->db->order_by($tbl_shift . ".start_time");
        $this->db->where($sWhere, null, false);
        #$this->db->limit(3);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        #last_query();
        $dataResult = $query->result();
        //$total_duration = 0.00;
        $main_ary = array();
        if (!empty($dataResult)) {
            $time = array();
            $temp = array();

            foreach ($dataResult as $val) {
                //$time[] = str_replace('hrs','',$val->duration);
                if (!in_array($val->shift_id, $temp)) {
                    $temp['shift_id'] = $val->shift_id;
                    $temp['start_date'] = $val->start_date;
                    $temp['start_time'] = $val->start_time;
                    $temp['shift_for'] = $val->participant_name;
                    $temp['address'] = $val->address . ' ' . $val->suburb . ', ' . $val->state_name . ' ' . $val->postal;
                    $temp['duration'] = $val->duration;
                    $main_ary[] = $temp;
                }
            }
            //$total_duration = add_hour_minute($time);
        }
        $return = array('data' => $main_ary, 'status' => true);
        return $return;
    }

    public function save_shift_feedback($obj) {
        $shift_id = $obj->getShiftId();
        $member_id = $obj->getMemberId();
        $incident_type = $obj->getIncidentType();
        $what_happen = $obj->getWhatHappen();
        #$feedback_data = array('shift_id'=>$shift_id,'member_id'=>$member_id,'incident_type'=>$incident_type,'what_happen'=>$what_happen);
        #$inserted_feedback = $this->Basic_model->insert_records('shift_feedback',$feedback_data);
        $shift_row = $this->basic_model->get_row('shift', $column = array('shift_date'), $where = array('id' => $shift_id));
        if (!empty($shift_row))
            $shift_date = $shift_row->shift_date;
        else
            $shift_date = date('Y-m-d');

        $feedback_data = array('shiftId' => $shift_id, 'initiated_by' => $member_id, 'initiated_type' => 1, 'created' => DATE_TIME, 'event_date' => $shift_date);
        $case_id = $this->Basic_model->insert_records('fms_case', $feedback_data);

        $row = $this->basic_model->get_row('shift_incident_type', $column = array('id', 'name'), $where = array('id' => $incident_type));
        if (!empty($row)) {
            $title = $row->name;
        }

        $reason_data = array('caseId' => $case_id, 'title' => $title, 'description' => $what_happen, 'created_by' => $member_id, 'created_type' => 1, 'created' => DATE_TIME);
        $inserted_feedback = $this->Basic_model->insert_records('fms_case_reason', $reason_data);

        if ($case_id) {
            $notification_ary = array('userId' => $member_id, 'user_type' => 1, 'title' => 'Shift Feedback is submitted', 'shortdescription' => 'New case is created for shiftId - ' . $shift_id, 'created' => DATE_TIME, 'sender_type' => 1);
            $this->db->insert('tbl_notification', $notification_ary);
            return array('status' => true, 'success' => 'Feedback submit successfully.');
        } else {
            return array('status' => false, 'success' => 'Error in save feedback.');
        }
    }

    public function get_shift_record($obj) {
        $member_id = $obj->getShiftMember();
        $shift_id = $obj->getShiftId();

        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_member = TBL_PREFIX . 'shift_member';
        $tbl_shift_location = TBL_PREFIX . 'shift_location';
        $tbl_state = TBL_PREFIX . 'state';
        $tbl_shift_participant = TBL_PREFIX . 'shift_participant';
        $tbl_participant = TBL_PREFIX . 'participant';

        $where = '';
        $where = $tbl_shift . ".id =" . $shift_id . " AND " . $tbl_shift_member . ".memberId = " . $member_id . " ";
        $sWhere = $where;

        $select_column = array($tbl_shift . ".id as shift_id", $tbl_shift . ".shift_date as start_date", $tbl_shift . ".status", $tbl_shift . ".start_time", $tbl_shift . ".end_time", $tbl_shift_location . ".address", $tbl_shift_location . ".suburb", $tbl_shift_location . ".state", $tbl_shift_location . ".postal", $tbl_shift_location . ".lat", $tbl_shift_location . ".long", $tbl_state . ".name as state_name", $tbl_participant . ".firstname", $tbl_participant . ".middlename", $tbl_participant . ".lastname");

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_shift);
        $this->db->select("CONCAT(MOD( TIMESTAMPDIFF(hour," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 24), ':',MOD( TIMESTAMPDIFF(minute," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 60), ' hrs') as duration");

        #$this->db->join($tbl_shift_member, $tbl_shift_member . '.shiftId = ' . $tbl_shift . '.id AND ' . $tbl_shift_member . '.status = 3', 'left');
        $this->db->join($tbl_shift_member, $tbl_shift_member . '.shiftId = ' . $tbl_shift . '.id ', 'left');
        $this->db->join($tbl_shift_location, $tbl_shift_location . '.shiftId = ' . $tbl_shift . '.id ', 'left');
        $this->db->join($tbl_state, $tbl_state . '.id = ' . $tbl_shift_location . '.state ', 'left');
        $this->db->join($tbl_shift_participant, $tbl_shift_participant . '.shiftId = ' . $tbl_shift . '.id ', 'left');
        $this->db->join($tbl_participant, $tbl_participant . '.id = ' . $tbl_shift_participant . '.participantId ', 'left');

        $this->db->group_by($tbl_shift_location . ".id");
        $this->db->order_by($tbl_shift . ".shift_date");
        $this->db->order_by($tbl_shift . ".start_time");
        $this->db->where($sWhere, null, false);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        //last_query();
        $dataResult = $query->row_array();

        $main_ary = array();
        if (!empty($dataResult)) {
            $temp['shift_id'] = $dataResult['shift_id'];
            $temp['start_date'] = $dataResult['start_date'];
            $temp['start_time'] = $dataResult['start_time'];
            $temp['shift_for'] = $dataResult['firstname'];
            $temp['location'] = $dataResult['suburb'];
            $temp['address'] = $dataResult['address'] . ' ' . $dataResult['suburb'] . ', ' . $dataResult['state_name'] . ' ' . $dataResult['postal'];
            $temp['duration'] = $dataResult['duration'];
            $main_ary['shift_data'] = $temp;
            $main_ary['shift_requirement'] = $this->get_shift_requirement($shift_id);
            $main_ary['shift_notes'] = $this->basic_model->get_record_where('shift_notes', $column = array('id', 'title', 'notes', "DATE_FORMAT(created,'%d/%m/%Y') as created"), $where = array('shiftId' => $shift_id, 'archive' => 0));
        }
        $return = array('data' => $main_ary, 'status' => true);
        return $return;
    }

    public function get_shift_requirement($shiftId) {
        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_requirement = TBL_PREFIX . 'shift_requirement';
        $tbl_shift_requirements = TBL_PREFIX . 'shift_requirements';
        $arr['shift_requirement'] = '';
        $dt_query = $this->db->select(array($tbl_requirement . ".id", $tbl_requirement . ".name"));
        $this->db->from($tbl_requirement);
        $this->db->join($tbl_shift_requirements, $tbl_shift_requirements . '.requirementId = ' . $tbl_requirement . '.id AND shiftId = ' . $shiftId, 'inner');
        $query = $this->db->get();
        $arr['requirement'] = $query->result_array();
        return $arr['requirement'];
    }

    public function save_shift_old($obj) {
        $member_id = $obj->getShiftMember();
        $shift_id = $obj->getShiftId();
        $status = $obj->getStatus();

        // 1 accepted and 0 rejected
        if ($status == 1) {
            $shift_status = '7';
            $member_shift_status = '3';
            $shift_msg = 'Shift confirmed successfully';
        } else {
            $shift_status = '4';
            $member_shift_status = '2';
            $shift_msg = 'Shift rejected successfully';
        }

        $row = $this->basic_model->get_row('shift_member', $column = array('created', 'status', 'memberId'), $where = array('shiftId' => $shift_id));
        #last_query();
        if (!empty($row)) {
            if ($row->status == 3 && $row->memberId == $member_id) {
                return array('status' => false, 'error' => 'Shift is already assign to this member.');
            }

            //if($row->status == 1  && $row->memberId == $member_id)
            if (($row->status == 1 || $row->status == 2) && $row->memberId == $member_id) {
                $this->basic_model->update_records('shift_member', $data = array('status' => $member_shift_status), $where = array('shiftId' => $shift_id, 'memberId' => $member_id));
                $this->basic_model->update_records('shift', $data = array('status' => $shift_status), $where = array('id' => $shift_id));

                $notification_ary = array('userId' => $member_id, 'user_type' => 1, 'title' => $shift_msg, 'shortdescription' => $shift_msg . ' -Shift id ' . $shift_id, 'created' => DATE_TIME, 'sender_type' => 1);
                $this->db->insert('tbl_notification', $notification_ary);

                return array('status' => true, 'success' => $shift_msg);
            }

            /* if(($row->status == 2 && $row->memberId != $member_id) || ($row->status == 4 && $row->memberId != $member_id) )
              {
              $this->basic_model->update_records('shift_member', $data = array('status'=>$member_shift_status), $where = array('shiftId'=>$shift_id,'memberId'=>$member_id));
              $this->basic_model->update_records('shift', $data = array('status'=>$shift_status), $where = array('id'=>$shift_id));
              return array('status' => true, 'success' => $shift_msg);
              }
              else
              {
              return array('status' => false, 'error' => 'Previously this shift is Rejected/cancelled by this member.');
              } */
        } else {
            return array('status' => false, 'error' => 'No shift Found.');
        }
    }

    public function save_shift($obj) {
        $member_id = $obj->getShiftMember();
        $shift_id = $obj->getShiftId();
        $status = $obj->getStatus();

        // 1 accepted and 0 rejected
        if ($status == 1) {
            $shift_status = '7';
            $member_shift_status = '3';
            $shift_msg = 'Shift confirmed successfully';
        } else {
            $shift_status = '4';
            $member_shift_status = '2';
            $shift_msg = 'Shift rejected successfully';
        }

        $row = $this->basic_model->get_row('shift', $column = array('id'), $where = array('id' => $shift_id));
        #last_query();
        if (!empty($row)) {

            $row = $this->basic_model->get_row('shift_member', $column = array('shiftId,status,memberId'), $where = array('shiftId' => $shift_id, 'memberId' => $member_id));
            if (empty($row)) {
                $data_1 = array('status' => $member_shift_status, 'shiftId' => $shift_id, 'memberId' => $member_id, 'created' => DATE_TIME);
                $this->Basic_model->insert_records('shift_member', $data_1);
            } else {
                $data_1 = array('status' => $member_shift_status);
                $this->basic_model->update_records('shift_member', $data_1, $where = array('shiftId' => $shift_id, 'memberId' => $member_id));
            }
            $this->basic_model->update_records('shift', $data = array('status' => $shift_status), $where = array('id' => $shift_id));
            $notification_ary = array('userId' => $member_id, 'user_type' => 1, 'title' => $shift_msg, 'shortdescription' => $shift_msg . ' -Shift id ' . $shift_id, 'created' => DATE_TIME, 'sender_type' => 1);
            $this->db->insert('tbl_notification', $notification_ary);
            return array('status' => true, 'success' => $shift_msg);
        } else {
            return array('status' => false, 'error' => 'No shift Found.');
        }
    }

    public function get_member_shift_to_confirm($obj) {
        # assign by admin need to accept or reject by member
        $member_id = $obj->getShiftMember();
        $default_date = date('Y-m-d');

        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_member = TBL_PREFIX . 'shift_member';
        $tbl_shift_location = TBL_PREFIX . 'shift_location';
        $tbl_state = TBL_PREFIX . 'state';
        $tbl_shift_participant = TBL_PREFIX . 'shift_participant';
        $tbl_participant = TBL_PREFIX . 'participant';
        //shift is assign to memeber and need to confirm
        $where = '';
        $where = $tbl_shift . ".status IN (2) AND " . $tbl_shift . ".shift_date != '0000-00-00' ";
        $sWhere = $where;
        #$sWhere .= " AND ".$tbl_shift.".shift_date >= '".$default_date."' AND ".$tbl_shift_member.".memberId = ".$member_id."";
        $sWhere .= " AND " . $tbl_shift_member . ".memberId = " . $member_id . "";

        $select_column = array($tbl_shift . ".id as shift_id", $tbl_shift . ".shift_date as start_date", $tbl_shift . ".status", $tbl_shift . ".start_time", $tbl_shift . ".end_time", $tbl_shift_location . ".address", $tbl_shift_location . ".suburb", $tbl_shift_location . ".state", $tbl_shift_location . ".postal", $tbl_shift_location . ".lat", $tbl_shift_location . ".long", $tbl_state . ".name as state_name", $tbl_participant . ".firstname", $tbl_participant . ".middlename", $tbl_participant . ".lastname");

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_shift);
        $this->db->select("CONCAT(MOD( TIMESTAMPDIFF(hour," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 24), ':',MOD( TIMESTAMPDIFF(minute," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 60), ' hrs') as duration");

        $this->db->join($tbl_shift_member, $tbl_shift_member . '.shiftId = ' . $tbl_shift . '.id AND ' . $tbl_shift_member . '.status = 1', 'left');

        $this->db->join($tbl_shift_location, $tbl_shift_location . '.shiftId = ' . $tbl_shift . '.id ', 'left');
        $this->db->join($tbl_state, $tbl_state . '.id = ' . $tbl_shift_location . '.state ', 'left');
        $this->db->join($tbl_shift_participant, $tbl_shift_participant . '.shiftId = ' . $tbl_shift . '.id ', 'left');
        $this->db->join($tbl_participant, $tbl_participant . '.id = ' . $tbl_shift_participant . '.participantId ', 'left');

        $this->db->group_by($tbl_shift_location . ".id");
        $this->db->order_by($tbl_shift . ".shift_date");
        $this->db->order_by($tbl_shift . ".start_time");
        $this->db->where($sWhere, null, false);
        //$this->db->limit(3);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        //last_query();

        $dataResult = $query->result();
        //$total_duration = 0.00;
        $main_ary = array();
        if (!empty($dataResult)) {
            $time = array();

            foreach ($dataResult as $val) {
                //$time[] = str_replace('hrs','',$val->duration);
                $temp['shift_id'] = $val->shift_id;
                $temp['start_date'] = $val->start_date;
                $temp['start_time'] = $val->start_time;
                $temp['shift_for'] = $val->firstname;
                $temp['address'] = $val->address . ' ' . $val->suburb . ', ' . $val->state_name . ' ' . $val->postal;
                $temp['duration'] = $val->duration;
                $main_ary[] = $temp;
            }
            //$total_duration = add_hour_minute($time);
        }
        $return = array('data' => $main_ary, 'status' => true);
        return $return;
    }

    public function member_availibility_list($obj) {
        $where_array = array('memberId' => $obj->getMemberid(), 'archive' => 0);
        $columns = array('title,id,DATE_FORMAT(start_date, "%e/%m/%Y") as startDate,DATE_FORMAT(end_date, "%e/%m/%Y") as endDate,id');
        return $this->Basic_model->get_result('member_availability', $where_array, $columns, array('id', 'DESC'));
    }

    public function member_availibility_detail($obj) {
        $where_array = array('memberId' => $obj->getMemberid(), 'id' => $obj->getMemberavailabilityid(), 'archive' => 0);
        $columns = array('id,is_default,status,first_week,second_week,flexible_availability,flexible_km,travel_km');
        return $this->Basic_model->get_result('member_availability', $where_array, $columns, array('id', 'DESC'));
    }

    public function shift_sign_off($obj, $shift_info) {
        $member_id = $obj->getShiftMember();
        $shift_id = $obj->getShiftId();

        $shift_amendment = $shift_info['shift_amendment'];
        $goal_ary = isset($shift_info['goal_ary']) ? $shift_info['goal_ary'] : '';
        $signature_ary = isset($shift_info['signature_ary']) ? $shift_info['signature_ary'] : array();
        //pr($signature_ary);			
        $expense_ary = isset($shift_info['expense_ary']) ? $shift_info['expense_ary'] : '';

        $row = $this->basic_model->get_row('shift', $column = array('id,start_time,end_time,status,shift_amendment,shift_date,concat(shift_date," ",end_time) as shift_end_datetime'), $where = array('id' => $shift_id));

        #print_r($row);
        if (!empty($row)) {
            $shift_end_time = $row->shift_end_datetime;

            $shift_end_time = strtotime(date('Y-m-d H:i:s', strtotime("$row->shift_date $row->end_time")));
            #echo '<br/>';
            #$shift_end_time1 = (date('Y-m-d H:i:s',strtotime("$row->shift_date $row->end_time")));
            #echo '<br/>';
            $current_time = strtotime(date('Y-m-d H:i:s'));
            #echo '<br/>';
            #echo $current_time11 = (date('Y-m-d H:i:s'));
            #echo '<br/>';
            $after_Time = strtotime(date("Y-m-d H:i:s", strtotime('+15 minutes', $shift_end_time)));
            #echo '<br/>';
            #echo $after_Time1 = '+15-->>'.(date("Y-m-d H:i:s", strtotime('+15 minutes', $shift_end_time)));
            #echo '<br/>';
            $before_Time = strtotime(date("Y-m-d H:i:s", strtotime('-15 minutes', $shift_end_time)));
            #echo '<br/>';
            #echo $before_Time1 = '-15-->>'.(date("Y-m-d H:i:s", strtotime('-15 minutes', $shift_end_time)));
            #echo '<br/>';
            #die;
            #
			if (($current_time < $after_Time) && ($current_time > $before_Time))
                $submitt = true;
            else
                $submitt = true;
            #var_dump($submitt);die;

            if ($submitt) {
                if ($shift_amendment == 1) {
                    $amendment_data = array('start_time' => $shift_info['start_time'], 'end_time' => $shift_info['end_time'], 'total_break_time' => $shift_info['total_break_time'], 'shift_km' => $shift_info['shift_km'], 'additional_expenses' => $shift_info['additional_expenses'], 'shiftId' => $shift_id);
                    $inserted_amendment = $this->Basic_model->insert_records('shift_amendment', $amendment_data);

                    if ($shift_info['additional_expenses']) {
                        if (!empty($expense_ary)) {
                            foreach ($expense_ary as $key => $value) {
                                $data_ary[] = array('receipt_value' => $value->receipt_value, 'receipt' => $value->receipt, 'shift_amendment_id' => $inserted_amendment, 'shiftId' => $shift_id);
                            }
                            $this->Basic_model->insert_records('shift_expenses_attachment', $data_ary, $multiple = TRUE);
                        }
                    }
                }
                /* else
                  { */
                $participant_row = $this->Basic_model->get_row('shift_participant', array('participantId'), $where = array('shiftId' => $shift_id));
                if (!empty($goal_ary)) {
                    foreach ($goal_ary as $key => $val) {
                        $goal_result[] = array('goalId' => $val->goalId, 'rating' => $val->rating, 'shiftId' => $shift_id, 'participantId' => $participant_row->participantId, 'created' => DATE_TIME);
                    }
                    $inserted_goal = $this->Basic_model->insert_records('participant_goal_result', $goal_result, TRUE);
                }
                //}
                if (!empty($signature_ary)) {
                    $signature_insert_data = array('shiftId' => $shift_id, 'member_signature' => $signature_ary[0]->member_sign, 'approval_signature' => $signature_ary[1]->approval_sign, 'created' => DATE_TIME);
                    $this->Basic_model->insert_records('shift_sign_off', $signature_insert_data);
                }
                $this->Basic_model->update_records('shift', array('status' => 6), array('id' => $shift_id));
                $notification_ary = array('userId' => $member_id, 'user_type' => 1, 'title' => 'Shift Sign-Off', 'shortdescription' => 'Shift Sign-Off ' . ' -Shift id ' . $shift_id, 'created' => DATE_TIME, 'sender_type' => 1);
                $this->db->insert('tbl_notification', $notification_ary);
                return array('status' => true, 'success' => 'Shift Sign-Off successfully.');
            } else {
                return array('status' => false, 'error' => 'Time is expire for Sign-Off.');
            }
        }
    }

    public function get_all_shift_by_date($obj, $selected_date) {
        $member_id = $obj->getShiftMember($obj);

        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_member = TBL_PREFIX . 'shift_member';

        $tbl_shift_location = TBL_PREFIX . 'shift_location';
        $tbl_state = TBL_PREFIX . 'state';
        $tbl_shift_participant = TBL_PREFIX . 'shift_participant';
        $tbl_participant = TBL_PREFIX . 'participant';

        $selected_date = date('Y-m-d', strtotime($selected_date));
        $where = '';
        $where = $tbl_shift . ".status IN (6) AND " . $tbl_shift . ".shift_date != '0000-00-00' ";
        $sWhere = $where;

        $todate = date('Y-m-d', strtotime($selected_date . ' - 30 day'));

        $sWhere .= " AND " . $tbl_shift . ".shift_date BETWEEN '$todate' AND '$selected_date'";

        #$select_column = array($tbl_shift . ".id as shift_id", $tbl_shift . ".shift_date as start_date" , $tbl_shift . ".status",$tbl_shift.".start_time",$tbl_shift.".end_time");

        $select_column = array($tbl_shift . ".id as shift_id", $tbl_shift . ".shift_date as start_date", $tbl_shift . ".status", $tbl_shift . ".start_time", $tbl_shift . ".end_time", $tbl_shift_location . ".address", $tbl_shift_location . ".suburb", $tbl_shift_location . ".state", $tbl_shift_location . ".postal", $tbl_shift_location . ".lat", $tbl_shift_location . ".long", $tbl_state . ".name as state_name", $tbl_participant . ".firstname", $tbl_participant . ".middlename", $tbl_participant . ".lastname", "CONCAT(tbl_participant.firstname,' ', tbl_participant.middlename,' ',tbl_participant.lastname) AS participant_name", "CONCAT(MOD( TIMESTAMPDIFF(hour," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 24), ':',MOD( TIMESTAMPDIFF(minute," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 60), ' hrs') as duration");


        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_shift);

        $this->db->join($tbl_shift_member, $tbl_shift_member . '.shiftId = ' . $tbl_shift . '.id AND ' . $tbl_shift_member . '.status = 3', 'left');
        $sWhere .= "AND " . $tbl_shift_member . ".memberId = " . $member_id . "";

        $this->db->join($tbl_shift_location, $tbl_shift_location . '.shiftId = ' . $tbl_shift . '.id ', 'left');
        $this->db->join($tbl_state, $tbl_state . '.id = ' . $tbl_shift_location . '.state ', 'left');
        $this->db->join($tbl_shift_participant, $tbl_shift_participant . '.shiftId = ' . $tbl_shift . '.id ', 'left');
        $this->db->join($tbl_participant, $tbl_participant . '.id = ' . $tbl_shift_participant . '.participantId ', 'left');

        $this->db->order_by($tbl_shift . ".shift_date");
        $this->db->order_by($tbl_shift . ".start_time");
        $this->db->where($sWhere, null, false);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        #last_query();
        $dataResult = $query->result();

        $main_ary = array();
        if (!empty($dataResult)) {
            $time = array();

            foreach ($dataResult as $val) {
                $temp['shift_id'] = $val->shift_id;
                $temp['start_date'] = $val->start_date;
                $temp['start_time'] = $val->start_time;
                $temp['dropdown_label'] = $val->start_date . '-' . $val->start_time;
                $temp['shift_for'] = $val->participant_name;
                $temp['address'] = $val->address . ' ' . $val->suburb . ', ' . $val->state_name . ' ' . $val->postal;
                $temp['duration'] = $val->duration;
                $main_ary[] = $temp;
            }
        }
        $return = array('data' => $main_ary, 'status' => true);
        return $return;
    }

    public function create_member_availiability($obj) {

        $insert_ary = array('title' => $obj->getTitle(),
            'memberId' => $obj->getMemberid(),
            'start_date' => $obj->getStartDate(),
            'end_date' => $obj->getEndDate(),
            'first_week' => $obj->getFirstWeek(),
            'second_week' => $obj->getSecondWeek(),
            'flexible_availability' => $obj->getFlexibleAvailability(),
            'flexible_km' => $obj->getFlexibleKm(),
            'travel_km' => $obj->getTravelKm(),
            'status' => 1,
            'is_default' => $obj->getIsDefault(),
            'updated' => DATE_TIME
        );

        if ($obj->getIsDefault() == 1) {
            $delete_data = array('memberId' => $obj->getMemberid());
            $this->delete_default_availability($delete_data);
        }

        $availability_id = $row = $this->Basic_model->insert_records('member_availability', $insert_ary, $multiple = FALSE);
        $msg = "Availabitity created successfully.";

        if ($availability_id > 0) {
            $this->generate_member_shift($availability_id);
        }
        if (!empty($availability_id)) {
            echo json_encode(array('status' => true, 'response_msg' => $msg));
            exit();
        } else {
            echo json_encode(array('status' => false, 'response_msg' => 'Something went wrong.'));
            exit();
        }
    }

    function generate_member_shift($member_availability_id) {
        /*
         *  this function is call inside another fun.
          So server side validation is not implemented* */
        $where = array('id' => $member_availability_id, 'archive' => 0);
        $rows = $this->Basic_model->get_row('member_availability', array('*'), $where);

        $x = $this->generate_shift_data($rows, $member_availability_id);

        if (!empty($x))
            $this->Basic_model->insert_records('member_availability_list', $x, $multiple = TRUE);

        $last_query = $this->db->last_query();
        //@file_put_contents($_SERVER['DOCUMENT_ROOT'].'/application/modules/member/controllers/response.txt', $last_query.PHP_EOL , FILE_APPEND | LOCK_EX);
    }

    function generate_shift_data($rows, $member_availability_id) {
        /*
          this function is call inside another fun.
          So server side validation is not implemented*
         */

        $main_ary = array();
        $is_create_shift = TRUE;
        $break_loop = FALSE;

        if (is_json($rows->first_week))
            $week = $rows->first_week;
        else if (is_json($rows->second_week))
            $week = $rows->second_week;
        else
            $is_create_shift = FALSE;

        $day_before = '';
        if (is_json($week)) {
            $start_date = date('Y-m-d', strtotime($rows->start_date));
            // if end date is 00 and default is 1 means create current date shift
            if ($rows->is_default == 1 && $rows->end_date == '0000-00-00 00:00:00') {
                //echo '<br/>';
                //shift is cr
                $day_before = dayDifferenceBetweenDate(date('Y-m-d'), $start_date);
                //echo '<br/>';
                //shift is created if difference between current date and start date is less than 28
                if ($day_before <= 28) {
                    $day_difference = 28 - $day_before;
                    $end_date = date('Y-m-d', strtotime($start_date . " +$day_difference days"));
                } else {
                    $is_create_shift = FALSE;
                }
            } else {
                $end_date = DateFormate($rows->end_date, 'Y-m-d');
            }

            if (strtotime($rows->start_date) >= strtotime(date('Y-m-d')) && $is_create_shift) {
                $dates = dateRangeBetweenDate($start_date, $end_date);
                $avail_ary = json_decode($week, true);
                #pr($dates); 
                foreach ($dates as $dateKey => $date) {
                    $myWweek = '';
                    if ($break_loop) {
                        $myWweek = 'second_week';
                        $break_loop = FALSE;
                        $avail_ary = json_decode($rows->second_week, true);
                    }

                    if (!empty($avail_ary)) {
                        foreach ($avail_ary as $key => $value) {
                            if ($break_loop)
                                break;
                            foreach ($value as $val) {
                                if ($break_loop)
                                    break;
                                foreach ($val as $kk => $day) {
                                    if ($day && ($date == $kk)) {

                                        $shift_row = $this->get_shift_for_availibility(array('member_id' => $rows->memberId, 'shift_date' => $dateKey));
                                        /* if shift is created for any availiibility date then that date will not overide */
                                        if (empty($shift_row)) {
                                            $temp_ary['memberId'] = $rows->memberId;
                                            $temp_ary['flexible_availability'] = $rows->flexible_availability;
                                            $temp_ary['flexible_km'] = $rows->flexible_km;
                                            $temp_ary['is_default'] = $rows->is_default;
                                            $temp_ary['travel_km'] = $rows->travel_km;
                                            $temp_ary['run_mode'] = 'OCS';
                                            $temp_ary['availability_type'] = $key;
                                            $temp_ary['availability_date'] = $dateKey;
                                            $temp_ary['created'] = DATE_TIME;
                                            $temp_ary['member_availability_id'] = $member_availability_id;
                                            $main_ary[] = $temp_ary;
                                        }
                                        // if condition is bocoz, In first/second week we have to create shift till sunday i.e., for a week only
                                        /*  if ($kk == 'Sun' && $myWweek != 'second_week') {
                                          $break_loop = true;
                                          break;
                                          } */
                                    }
                                }
                            }
                        }
                    }
                }
                return $main_ary;
            }
        }
    }

    public function get_shift_for_availibility($rosObj) {
        $member_id = $rosObj['member_id'];
        $shift_date = $rosObj['shift_date'];

        $this->db->select(array("tbl_shift.id"));
        $this->db->from('tbl_shift');
        $this->db->join('tbl_shift_member', 'tbl_shift_member.shiftId = tbl_shift.id', 'left');
        $where = "tbl_shift.shift_date = '" . $shift_date . "' AND tbl_shift_member.memberId = " . $member_id . " AND tbl_shift_member.status = '3' AND tbl_shift.status = '7'";
        $this->db->where($where);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        #last_query();
        return $query->row();
    }

    public function delete_default_availability($delete_data) {
        $where_avail = array('memberId' => $delete_data['memberId'], 'is_default' => 1);
        $data = array('archive' => 1);
        $this->Basic_model->update_records('member_availability', $data, $where_avail);
        $this->Basic_model->update_records('member_availability_list', $data, $where_avail);
    }

    public function get_external_messages($filter, $memberId) {

        $src_columns = array('em.title', 'emc.content', "concat(admin.firstname,' ',admin.lastname)", "concat(sender_admin.firstname,' ',sender_admin.lastname)", "concat(mem.firstname,' ',mem.lastname)");


        if (isset($filter->search_text) && $filter->search_text != "") {
            $this->db->group_start();
            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    if ($serch_column[0] != 'null')
                        $this->db->or_like($serch_column[0], $filter->search_text);
                }
                else if ($column_search != 'null') {
                    $this->db->or_like($column_search, $filter->search_text);
                }
            }

            $this->db->group_end();
        }

        $this->db->select(array('em.id', 'em.title', 'em.is_block', 'ema.is_fav', 'ema.is_flage', 'ema.archive', 'emc.content', 'emc.created', 'emc.is_priority', 'emc.id as contentId', 'emc.sender_type'));

        $this->db->select('(select count(em_r.is_read) from tbl_external_message_content as em_c inner join tbl_external_message_recipient as em_r on em_r.messageContentId = em_c.id where em_r.recipinent_type = 3 AND em_r.recipinentId = ' . $memberId . ' and em_c.messageId = em.id and em_r.is_read = 0) as unread_count');

        $this->db->select("CASE emc.sender_type
            WHEN 1 THEN (select concat(firstname,' ',lastname,'||',profile,'||',gender,'||',id) from tbl_admin where id = emc.userId)
            WHEN 2 THEN (select concat(firstname,' ', middlename,' ',lastname,'||', profile_image,'||',gender,'||',id) from tbl_participant where id = emc.userId)
            WHEN 3 THEN (select concat(firstname,' ', middlename,' ',lastname,'||', profile_image,'||',gender,'||',id)  from tbl_member where id = emc.userId)
            WHEN 4 THEN (select concat(name,'||', logo_file,'||',id)  from tbl_organisation where id = emc.userId)
            ELSE NULL
            END as user_data");

        $this->db->from('tbl_external_message as em');
        $this->db->join('tbl_external_message_content as emc', 'em.id = emc.messageId', 'left');
        $this->db->join('tbl_external_message_action as ema', 'ema.messageId = em.id AND ema.userId = ' . $memberId . ' AND ema.user_type = 3', 'left');
        $this->db->join('tbl_external_message_recipient as emr', 'emr.messageContentId = emc.id', 'left');
        $this->db->join('tbl_external_message_attachment as emattach', 'emattach.messageContentId = emc.id', 'left');

        // add when join user search something
        if (isset($filter->search_text) && $filter->search_text != "") {
            $this->db->join('tbl_admin as admin', 'emr.recipinentId = admin.id AND emr.recipinent_type = 1', 'left');
            $this->db->join('tbl_member as mem', 'emr.recipinentId = mem.id AND emr.recipinent_type = 3', 'left');
            $this->db->join('tbl_admin as sender_admin', 'emc.userId = sender_admin.id AND emc.sender_type = 1', 'left');
        }

        $this->db->where("((emc.sender_type = 3 AND emc.userId = " . $memberId . ") Or (emr.recipinent_type = 3 AND emr.recipinentId = " . $memberId . "))");
        $this->db->where('emc.is_draft', 0);
        $this->db->where('ema.archive', 0);

        $this->db->where('emc.id IN (SELECT MAX(emc.id) FROM tbl_external_message_content as emc left join tbl_external_message_recipient as emr ON emr.messageContentId = emc.id where ((emc.sender_type = 3 AND emc.userId = ' . $memberId . ') Or (emr.recipinent_type = 3 AND emr.recipinentId = ' . $memberId . ')) GROUP BY emc.messageId )');
        $this->db->group_by('emc.messageId');
        $this->db->order_by('emc.created', 'desc');

        $query = $this->db->get();
        $result = $query->result();
//        last_query();
//        print_r($result);

        $ext_msg = array();
        if (!empty($result)) {
            foreach ($result as $val) {
                $x['id'] = $val->id;
                $x['title'] = $val->title;
                $x['mail_date'] = $val->created;
                $x['unread_count'] = $val->unread_count;

                //$x['is_flage'] = $val->is_flage;
                //$x['is_block'] = $val->is_block;
                //$x['is_fav'] = $val->is_fav;
                // $x['user_name'] = $val->user_name;
                // $x['is_priority'] = $val->is_priority;
                // $x['content'] = setting_length($val->content, 100);
                // $x['have_attachment'] = $this->check_attachment($val->contentId);

                $user_data = explode('||', $val->user_data);

                $sender_type = $val->sender_type;

                if (count($user_data) > 1) {
                    // here 0 = user name 
                    $x['user_name'] = $user_data[0];

                    if ($sender_type == 1) {
                        $lastContentId = $val->id;
                        // here 3 = user id // 1 = profile img name // 2 = gender
                        $x['user_img'] = get_admin_img($user_data[3], $user_data[1], $user_data[2]);
                    } else if ($sender_type == 3) {

                        // here 3 = user id  // 1 = profile img name  // 2 = gender
                        $x['user_img'] = get_admin_img($user_data[3], $user_data[1], $user_data[2]);
                    }
                }

                $ext_msg[] = $x;
            }
        }

        return $ext_msg;
    }

    function check_attachment($messageContentId) {
        $this->db->select(array('id'));

        $this->db->from('tbl_external_message_attachment');
        $this->db->where('messageContentId', $messageContentId);

        $query = $this->db->get();
        $result = $query->num_rows();

        if ($result > 0) {
            return true;
        }
        return false;
    }

    public function member_single_chat($messageId, $memberId) {

        $this->db->select(array('em.id', 'em.title', 'em.is_block'));
//        $this->db->select(array('em.id', 'em.title', 'em.is_block', 'ema.is_fav', 'ema.is_flage', 'ema.is_flage'));
        $this->db->from('tbl_external_message as em');

        $this->db->join('tbl_external_message_action as ema', 'ema.messageId = em.id AND ema.userId = ' . $memberId . ' AND ema.user_type = 3', 'left');
        $this->db->where('ema.messageId', $messageId);
        $this->db->where('ema.archive', 0);

        $query = $this->db->get();
        $messageData = $query->row();



        if (!empty($messageData)) {

            // get all content of message
            $content_details = $this->get_external_mail_all_content($messageId, $memberId);

            // mark as read message
            $this->mark_read_unread($messageId, $memberId, 1);

            // if its comes blank mean this user not have permission to mail
            if (!empty($content_details)) {
                $messageData->mail_data = $content_details['ext_msg'];
                $messageData->lastConetntId = $content_details['lastContentId'];

                $return = array('status' => true, 'data' => $messageData);
            } else {
                $return = array('status' => false, 'error' => 'No mail found');
            }
        } else {
            $return = array('status' => false, 'error' => 'No mail found');
        }

        return $return;
    }

    function get_external_mail_all_content($messageId, $memberId) {
        $this->db->select("CASE emc.sender_type
            WHEN 1 THEN (select concat(firstname,' ',lastname,'||',profile,'||',gender,'||',id) from tbl_admin where id = emc.userId)
            WHEN 2 THEN (select concat(firstname,' ', middlename,' ',lastname,'||', profile_image,'||',gender,'||',id) from tbl_participant where id = emc.userId)
            WHEN 3 THEN (select concat(firstname,' ', middlename,' ',lastname,'||', profile_image,'||',gender,'||',id)  from tbl_member where id = emc.userId)
            WHEN 4 THEN (select concat(name,'||', logo_file,'||',id)  from tbl_organisation where id = emc.userId)
            ELSE NULL
            END as user_data");


        $this->db->select(array('emc.id', 'emc.created', 'emc.sender_type', 'is_priority', 'content', 'emr.is_read', 'emc.is_draft', 'is_reply', 'emr.is_notify'));


        $this->db->from('tbl_external_message_content as emc');
        $this->db->join('tbl_external_message_recipient as emr', 'emr.messageContentId = emc.id AND emr.recipinent_type = 3', 'left');
        $this->db->order_by('emc.created', 'asc');
        $this->db->group_by('emc.id');
        $this->db->where('emc.messageId', $messageId);
        $this->db->where("((emc.sender_type = 3 AND emc.userId = " . $memberId . ") Or (emr.recipinent_type = 3 AND emr.recipinentId = " . $memberId . "))");
        $this->db->where('emc.is_draft', 0);

        $query = $this->db->get();
        $result = $query->result();

        $ext_msg = array();
        $lastContentId = '';

        if (!empty($result)) {
            foreach ($result as $key => $val) {
                $x['id'] = $val->id;
                $x['mail_date'] = $val->created;
                $x['content'] = $val->content;
                $x['is_priority'] = $val->is_priority;
                $x['sender_type'] = $val->sender_type;

                $user_data = explode('||', $val->user_data);

                $sender_type = $val->sender_type;

                if (count($user_data) > 1) {
                    // here 0 = user name 
                    $x['user_name'] = $user_data[0];

                    if ($sender_type == 1) {
                        $lastContentId = $val->id;
                        // here 3 = user id // 1 = profile img name // 2 = gender
                        $x['user_img'] = get_admin_img($user_data[3], $user_data[1], $user_data[2]);
                    } else if ($sender_type == 3) {

                        // here 3 = user id  // 1 = profile img name  // 2 = gender
                        $x['user_img'] = get_admin_img($user_data[3], $user_data[1], $user_data[2]);
                    }
                }

                // get all attachments
                $x['attachments'] = $this->get_mail_attachment($val->id);

                $ext_msg[] = $x;
            }
        } else {
            return false;
        }

        return array('ext_msg' => $ext_msg, 'lastContentId' => $lastContentId);
    }

    function get_mail_attachment($messageContentId) {
        $this->db->select(array('id', 'filename'));

        $this->db->from('tbl_external_message_attachment');
        $this->db->where('tbl_external_message_attachment.messageContentId', $messageContentId);

        $query = $this->db->get();
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $val) {
                $val->file_path = $this->config->item('adminUrl') . EXTERNAL_IMAIL_PATH . $messageContentId . '/' . $val->filename;
                $val->contentId = $messageContentId;
            }
        }

        return $result;
    }

    function mark_read_unread($messageId, $userId, $status) {
        if ($status == 0 || $status == 1) {

            $where = array('messageId' => $messageId, 'recipinent_type' => 3, 'recipinentId' => $userId);
            $data = array('is_read' => $status);

            if ($status == 1) {
                $data['is_notify'] = 1;
            }
            $this->basic_model->update_records('external_message_recipient', $data, $where);
        }
    }

    public function push_notification_setting($objMember) {
        $member_id = $objMember->getMemberid();
        $push_notification_enable = $objMember->getPushNotificationEnable();
        $this->db->set('push_notification_enable', $push_notification_enable);
        $this->db->where('id', $member_id);
        $this->db->update(TBL_PREFIX . 'member');
        return true;
    }

    public function create_message($reqData) {

        // here intsert content data
        $contentId = $this->set_replay_content($reqData);

        // upload attachment if have any attachement
        $this->upload_content_attachement($reqData->messageId, $contentId);

        // set here relicant data
        $this->set_recipient_user($reqData, $contentId);

        return true;
    }

    function set_replay_content($reqData) {
        $content_data = array(
            'messageId' => $reqData->messageId,
            'sender_type' => 3,
            'userId' => $reqData->memberid,
            'created' => DATE_TIME,
            'content' => $reqData->text,
            'is_priority' => 0,
            'is_reply' => 1,
            'is_draft' => 0,
        );

        return $this->basic_model->insert_records('external_message_content', $content_data, FALSE);
    }

    function set_recipient_user($reqData, $contentId) {
        $where_mc = ['id' => $reqData->lastContentId, 'messageId' => $reqData->messageId];
        $sender_user = $rec = $this->basic_model->get_record_where('external_message_content', ['userId as adminId'], $where_mc);

        $where_rc = ['recipinent_type' => 1, 'messageContentId' => $reqData->lastContentId, 'messageId' => $reqData->messageId];
        $recipient_user = $this->basic_model->get_record_where('external_message_recipient', ['recipinentId as adminId'], $where_rc);

        $recipient_user = array_merge($sender_user, $recipient_user);

        $recipient_data = array();

        $userIds = array();
        if (!empty($recipient_user)) {
            foreach ($recipient_user as $val) {
                $x['messageContentId'] = $contentId;
                $x['messageId'] = $reqData->messageId;
                $x['recipinent_type'] = 1;
                $x['recipinentId'] = $val->adminId;
                $x['is_read'] = 0;
                $x['is_notify'] = 0;

                $recipient_data[] = $x;

                $userIds[1][] = $val->adminId;
            }
        }

        if (!empty($recipient_data)) {
            $this->basic_model->insert_records('external_message_recipient', $recipient_data, true);
        }
    }

    function check_messageId_and_contentId($reqData) {
        $where_rc = ['recipinent_type' => 3, 'messageContentId' => $reqData->lastContentId, 'messageId' => $reqData->messageId];
        return $this->basic_model->get_record_where('external_message_recipient', ['recipinentId as adminId'], $where_rc);
    }

    public function get_shift_goal($obj) {
        $row = $this->basic_model->get_row('shift_participant', $column = array('participantId'), $where = array('shiftId' => $obj->getShiftId()));
        $participantId = $row->participantId;
        $where_array = array('participantId' => $participantId, 'status' => '1', 'date(end_date) > ' => date('Y-m-d'));
        $columns = array('id,title');
        $x = $this->Basic_model->get_result('participant_goal', $where_array, $columns, array('id', 'DESC'));
        return $x;
    }

    public function get_shift_for_review($obj) {
        $member_id = $obj->getShiftMember();

        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_member = TBL_PREFIX . 'shift_member';

        $where = '';
        $where = $tbl_shift_member . ".memberId = " . $member_id . " AND " . $tbl_shift . ".status = 7 AND " . $tbl_shift_member . ".status = 3 AND (NOW( ) BETWEEN tbl_shift.start_time AND tbl_shift.end_time)";

        #$where = $tbl_shift_member.".memberId = ".$member_id." AND ". $tbl_shift . ".status = 7 AND ".$tbl_shift_member . ".status = 3 AND ". $tbl_shift . ".id = 45";
        $sWhere = $where;

        $select_column = array($tbl_shift . ".id as shift_id", $tbl_shift . ".shift_date as start_date", $tbl_shift . ".status", $tbl_shift . ".start_time", $tbl_shift . ".end_time");

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_shift);
        $this->db->join($tbl_shift_member, $tbl_shift_member . '.shiftId = ' . $tbl_shift . '.id ', 'left');

        $this->db->order_by($tbl_shift . ".shift_date");
        $this->db->order_by($tbl_shift . ".start_time");
        $this->db->where($sWhere, null, false);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        #@file_put_contents($_SERVER['DOCUMENT_ROOT'].'/development/application/modules/api/controllers/response.txt', $this->db->last_query().PHP_EOL , FILE_APPEND | LOCK_EX);
        $dataResult = $query->result_array();

        $main_ary = array();
        if (!empty($dataResult)) {
            foreach ($dataResult as $key => $value) {
                $temp['shift_id'] = $value['shift_id'];
                $temp['start_time'] = $value['start_time'];
                $main_ary['shift_data'][] = $temp;
            }
        }
        //$return = array( 'data' => $main_ary,'status'=>true);
        return $main_ary;
    }

    function get_external_messages_badgechcount($member_id) {
        $select_colown = array('em.id', 'em.title', 'emc.content');

        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_colown)), false);

        $this->db->from('tbl_external_message as em');
        $this->db->join('tbl_external_message_content as emc', 'em.id = emc.messageId', 'left');
        $this->db->join('tbl_external_message_recipient as emr', 'emr.messageContentId = emc.id AND emr.recipinent_type = 3 AND emr.recipinentId = ' . $member_id, 'inner');

        $this->db->where(array('emr.is_read' => 0, 'emr.is_notify' => 0, 'emr.recipinentId' => $member_id, 'emc.is_draft' => 0));

        $this->db->order_by('emc.created', 'desc');
        $this->db->group_by('emc.messageId');
        $this->db->limit(1);

        $query = $this->db->get();
        $query->row();

        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        return $dt_filtered_total;
    }

    function upload_content_attachement($messageId, $contentId) {
        $error = array();

        if (!empty($_FILES)) {
            $config['upload_path'] = 'uploads/temp/';
            $config['input_name'] = 'attachment';
            $config['directory_name'] = '';
            $config['allowed_types'] = 'jpg|jpeg|png|xlx|xls|doc|docx|pdf|csv|odt|rtf';

            $response = do_muliple_upload($config);
//            print_r($response);

            $attachments = array();

            if (!empty($response)) {
                foreach ($response as $key => $val) {
                    if (isset($val['error'])) {
                        $error[]['file_error'] = $val['error'];
                    } else {
                        $attachments[$key]['filename'] = $val['upload_data']['file_name'];
                        $attachments[$key]['created'] = DATE_TIME;
                        $attachments[$key]['messageContentId'] = $contentId;

                        $sended_data[$key]['filename'] = $val['upload_data']['file_name'];
                        $sended_data[$key]['messageContentId'] = $contentId;
                        $sended_data[$key]['path'] = base_url() . 'uploads/temp/' . $val['upload_data']['file_name'];
                    }
                }


                $this->send_attachment_to_ocs_admin($sended_data);


                if (!empty($attachments)) {
                    $this->basic_model->insert_records('external_message_attachment', $attachments, true);
                }
            }
        }

        return $error;
    }

    function send_attachment_to_ocs_admin($data) {
        $url = $this->config->item('adminUrl') . 'imail/External_imail/recieve_attachment_from_mobile_site';

        $ch = curl_init();

        //Get the response from cURL
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //Set the Url
        curl_setopt($ch, CURLOPT_URL, $url);

        $post_data = ['files' => json_encode($data)];
        //Create a POST array with the file in it
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

        // Execute the request
        return $response = curl_exec($ch);
    }

    public function check_availibility_already_exist($rosObj) {
        $start_date = $rosObj->getStartDate();
        $end_date = $rosObj->getEndDate();


        $this->db->select(array("tbl_member_availability.id"));
        $this->db->from('tbl_member_availability');

        $where = "is_default = 2 AND memberId = " . $rosObj->getMemberid() . " AND 
			(('" . $start_date . "' BETWEEN date(start_date) AND date(end_date))  or ('" . $end_date . "' BETWEEN date(start_date) AND date(end_date)) OR
			(date(start_date) BETWEEN '" . $start_date . "' AND '" . $end_date . "')  or (date(end_date) BETWEEN '" . $start_date . "' AND '" . $end_date . "')) ";
        $this->db->where($where);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        
        return $query->row();
    }

}
