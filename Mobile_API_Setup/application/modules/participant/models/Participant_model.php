<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Participant_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function participant_list($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';


        $colowmn = array('tbl_participant.id', 'tbl_participant.ndis_num', 'tbl_participant.preferredname', 'tbl_participant.firstname', 'tbl_participant.lastname', 'tbl_participant.status', 'tbl_participant_phone.phone', 'tbl_participant.gender,tbl_participant_address.street,tbl_participant_address.city,tbl_participant_address.postal,tbl_participant_address.state');

        $src_columns = array('tbl_participant.id', 'tbl_participant.ndis_num', 'tbl_participant.preferredname', 'tbl_participant.firstname', 'tbl_participant.lastname', 'tbl_participant.status', 'tbl_participant_phone.phone', 'tbl_participant_address.street', ' tbl_participant_address.postal', 'tbl_participant_address.state');
        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'tbl_participant.id';
            $direction = 'ASC';
        }

        $where = '';
        $where = "tbl_participant.archive = 0";

        $sWhere = $where;


        if (!empty($filter)) {
            if ($filter->include_inactive == 'true') {
                $status_con = "0,1";
                $this->db->where_in(array('tbl_participant.status', $status_con));
            } else {
                $this->db->where(array('tbl_participant.status' => 1));

            }




            if (!empty($filter->srch_box)) {
                $search_value = $filter->srch_box;
                $where = $where . " AND (";
                $sWhere = " (" . $where;

                for ($i = 0; $i < count($src_columns); $i++) {
                    $column_search = $src_columns[$i];
                    if (strstr($column_search, "as") !== false) {
                        $serch_column = explode(" as ", $column_search);
                        $sWhere .= $serch_column[0] . " LIKE '%" . $this->db->escape_like_str($search_value) . "%' OR ";
                    } else {
                        $sWhere .= $column_search . " LIKE '%" . $this->db->escape_like_str($search_value) . "%' OR ";
                    }
                }
                $sWhere = substr_replace($sWhere, "", -3);
                $sWhere .= '))';
            }
        } else {
            $status_con = 1;
            $this->db->where(array('tbl_participant.status' => $status_con));
        }

        $select_column = array('tbl_participant.id', 'tbl_participant.ndis_num', 'tbl_participant.preferredname', 'tbl_participant.firstname', 'tbl_participant.lastname', 'tbl_participant.status', 'tbl_participant_phone.phone', 'tbl_participant.gender,tbl_participant_address.street,tbl_participant_address.city,tbl_participant_address.postal,tbl_state.name as state');

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from(TBL_PREFIX . 'participant');

        $this->db->join('tbl_participant_address', 'tbl_participant_address.participantId = tbl_participant.id AND tbl_participant_address.primary_address = 1', 'left');
        $this->db->join('tbl_participant_phone', 'tbl_participant_phone.participantId = tbl_participant.id AND tbl_participant_phone.primary_phone = 1', 'left');
        $this->db->join('tbl_state', 'tbl_state.id = tbl_participant_address.state', 'left');


        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        $this->db->where($sWhere, null, false);

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = array();

        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $row = array();
                $row['id'] = $val->id;
                $row['ndis_num'] = $val->ndis_num;
                $row['name'] = $val->preferredname . ' ' . $val->firstname . ' ' . $val->lastname;
                $row['gender'] = isset($val->gender) && $val->gender == 1 ? 'M' : 'F';
                $row['street'] = $val->street . ', ' . $val->state . ', ' . $val->postal;
                $row['phone'] = $val->phone;
                $row['status'] = $val->status;
                $dataResult[] = $row;
            }
        }
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult);
        return $return;
    }

    public function Check_UserName($objparticipant) {
        $username = $objparticipant->getUserName();
        $this->db->select(array('count(*) as count'));
        $this->db->from(TBL_PREFIX . 'participant');
        $this->db->where(array('username' => $username));
        //return $this->db->last_query();
        return $this->db->get()->row()->count;
    }

    public function create_participant($objparticipant) {

        $arr_participant = array();
        $arr_participant['username'] = $objparticipant->getUserName();
        $arr_participant['firstname'] = $objparticipant->getFirstname();
        $arr_participant['middlename'] = $objparticipant->getMiddlename();
        $arr_participant['lastname'] = $objparticipant->getLastname();
        $arr_participant['gender'] = $objparticipant->getGender();
        $arr_participant['dob'] = $objparticipant->getDob();
        $arr_participant['ndis_num'] = $objparticipant->getNdisNum();
        $arr_participant['medicare_num'] = $objparticipant->getMedicareNum();
        $arr_participant['crn_num'] = $objparticipant->getCrnNum();
        $arr_participant['referral'] = $objparticipant->getReferral();

        if ($objparticipant->getReferral() == 1) {
            $arr_participant['relation'] = $objparticipant->getParticipantRelation();
            $arr_participant['preferredname'] = $objparticipant->getPreferredname();
            $arr_participant['referral_firstname'] = $objparticipant->getReferralFirstName();
            $arr_participant['referral_lastname'] = $objparticipant->getReferralLastName();
            $arr_participant['referral_email'] = $objparticipant->getReferralEmail();
            $arr_participant['referral_phone'] = $objparticipant->getReferralPhone();
        }

        $arr_participant['living_situation'] = $objparticipant->getLivingSituation();
        $arr_participant['aboriginal_tsi'] = $objparticipant->getAboriginalTsi();
        $arr_participant['oc_departments'] = $objparticipant->getOcDepartments();
        $arr_participant['created'] = $objparticipant->getCreated();
        $arr_participant['status'] = $objparticipant->getStatus();
        $arr_participant['portal_access'] = $objparticipant->getPortalAccess();
        $arr_participant['archive'] = 0;
        $arr_participant['houseId'] = $objparticipant->getHouseid();


        $insert_query = $this->db->insert(TBL_PREFIX . 'participant', $arr_participant);
        $participant_id = $this->db->insert_id();

        if ($participant_id > 0) {
            //Here to start insert all Emails
            $participant_email = array();
            $all_part_emails = $objparticipant->getParticipantEmail();
            if (is_array($all_part_emails)) {
                foreach ($all_part_emails as $emails) {
                    $participant_email[] = array('participantId' => $participant_id, 'email' => $emails['email'], 'primary_email' => $emails['type']);
                }
                $this->db->insert_batch(TBL_PREFIX . 'participant_email', $participant_email);
            }

            //Here to start insert all Phone no.
            $participant_phone = array();
            $all_part_phone = $objparticipant->getParticipantPhone();
            if (is_array($all_part_phone)) {
                foreach ($all_part_phone as $phone) {
                    $participant_phone[] = array('participantId' => $participant_id, 'phone' => $phone['phone'], 'primary_phone' => $phone['type']);
                }
                $this->db->insert_batch(TBL_PREFIX . 'participant_phone', $participant_phone);
            }
        }
        return $participant_id;
    }

    public function participant_profile($participantID) {

        $colown = array("gender", "status", "portal_access", "firstname");
        $this->db->select($colown);
        $this->db->select(array("concat(firstname,' ',middlename,' ',lastname) as full_name", false));
        $this->db->from(TBL_PREFIX . 'participant');
        $this->db->where(array('id' => $participantID));
        $query = $this->db->get();

        return $query->result();
    }

    public function participant_about($participantId) {

        if (!empty($participantId)) {

            $tbl_1 = TBL_PREFIX . 'participant';
            $tbl_2 = TBL_PREFIX . 'participant_phone';
            $tbl_3 = TBL_PREFIX . 'participant_email';
            $tbl_4 = TBL_PREFIX . 'participant_address';
            $tbl_5 = TBL_PREFIX . 'participant_kin';

            $sWhere = array($tbl_1 . '.id' => $participantId);

            $this->db->select("CONCAT($tbl_1.firstname,' ',$tbl_1.middlename, ' ', $tbl_1.lastname) AS fullname", FALSE);

            $dt_query = $this->db->select(array($tbl_1 . '.id as ocs_id', $tbl_1 . '.firstname', $tbl_1 . '.username', $tbl_1 . '.gender', $tbl_1 . '.lastname', $tbl_1 . '.middlename', $tbl_1 . '.ndis_num', $tbl_1 . '.medicare_num', $tbl_1 . '.crn_num', $tbl_1 . '.preferredname', $tbl_1 . '.prefer_contact', $tbl_1 . '.dob', $tbl_2 . '.primary_phone', $tbl_3 . '.email', $tbl_4 . '.street', $tbl_4 . '.city', $tbl_4 . '.state', $tbl_4 . '.postal', $tbl_4 . '.primary_address', $tbl_4 . '.site_category', $tbl_5 . '.relation', $tbl_5 . '.phone', $tbl_5 . '.email', $tbl_5 . '.primary_kin'));
            $this->db->from($tbl_1);
            $this->db->join($tbl_2, 'tbl_participant_phone.participantId = tbl_participant.id AND tbl_participant_phone.primary_phone = 1', 'left');
            $this->db->join($tbl_3, 'tbl_participant_email.participantId = tbl_participant.id AND tbl_participant_email.primary_email = 1', 'left');
            $this->db->join($tbl_4, 'tbl_participant_address.participantId = tbl_participant.id AND tbl_participant_address.primary_address = 1', 'left');
            $this->db->join($tbl_5, 'tbl_participant_kin.participantId = tbl_participant.id AND tbl_participant_kin.primary_kin = 1', 'left');
            $this->db->where($sWhere);
            $query = $this->db->get();
            $x = $query->row_array();

            $arr = $x;

            $dob = $arr['dob'];
            $x = date('Y', strtotime($dob));
            $y = date('Y');
            $arr['age'] = $y - $x;


            $dt_query = $this->db->select(array($tbl_4 . '.street', $tbl_4 . '.city', $tbl_4 . '.postal', $tbl_4 . '.state as state', 'tbl_state.name as stateName', $tbl_4 . '.primary_address', $tbl_4 . '.site_category'));
            $this->db->from($tbl_4);
            $this->db->join('tbl_state', 'tbl_state.id = ' . $tbl_4 . '.state', 'inner');
            $sWhere = array($tbl_4 . '.participantId' => $participantId);
            $this->db->where($sWhere, null, false);
            $query = $this->db->get();
            $arr['address'] = $query->result_array();

            $dt_query = $this->db->select(array($tbl_2 . '.phone', $tbl_2 . '.primary_phone'));
            $this->db->from($tbl_2);
            $sWhere = array($tbl_2 . '.participantId' => $participantId);
            $this->db->where($sWhere, null, false);
            $query = $this->db->get();
            $arr['phones'] = $query->result_array();

            $dt_query = $this->db->select(array($tbl_3 . '.email', $tbl_3 . '.primary_email'));
            $this->db->from($tbl_3);
            $sWhere = array($tbl_3 . '.participantId' => $participantId);
            $this->db->where($sWhere, null, false);
            $query = $this->db->get();
            $arr['emails'] = $query->result_array();

            $dt_query = $this->db->select(array($tbl_5 . '.relation', $tbl_5 . '.phone', $tbl_5 . '.email', $tbl_5 . '.primary_kin', $tbl_5 . '.firstname', $tbl_5 . '.lastname'));
            $this->db->select("CONCAT($tbl_5.firstname, ' ', $tbl_5.lastname) AS kinfullname", FALSE);
            $this->db->from($tbl_5);
            $sWhere = array($tbl_5 . '.participantId' => $participantId);
            $this->db->where($sWhere, null, false);
            $query = $this->db->get();
            $arr['kin_detials'] = $query->result_array();

            $tbl_6 = TBL_PREFIX . 'participant_oc_services';
            $tbl_7 = TBL_PREFIX . 'participant_support_required';
            $tbl_8 = TBL_PREFIX . 'participant_assistance';
            $tbl_9 = TBL_PREFIX . 'participant_genral';
            $genral_Where = array($tbl_9 . '.status' => 1);

            $dt_query = $this->db->select(array("GROUP_CONCAT(" . $tbl_9 . ".name SEPARATOR ', ') as name"));
            $this->db->from($tbl_9);
            $this->db->join($tbl_6, $tbl_9 . '.id = tbl_participant_oc_services.oc_service', 'inner');
            $this->db->where(array('participantId' => $participantId));
            $this->db->where($genral_Where);
            $query = $this->db->get();
            $arr['oc_service'] = $query->result_array();
            $arr['oc_service'] = $query->result_array();

            if (!empty($arr['oc_service'])) {
                $arr['oc_service'] = $arr['oc_service'][0]['name'];
            }

            $dt_query = $this->db->select(array("GROUP_CONCAT(" . $tbl_9 . ".name SEPARATOR ', ') as name"));
            $this->db->from($tbl_9);
            $this->db->join($tbl_7, $tbl_9 . '.id = tbl_participant_support_required.support_required', 'inner');
            $this->db->where(array('participantId' => $participantId));
            $this->db->where($genral_Where);
            $query = $this->db->get();
            $arr['support_required'] = $query->result_array();
            if (!empty($arr['support_required'])) {
                $arr['support_required'] = $arr['support_required'][0]['name'];
            }

            $dt_query = $this->db->select(array("GROUP_CONCAT(" . $tbl_9 . ".name SEPARATOR ', ') as name"));
            $this->db->from($tbl_9);
            $this->db->join($tbl_8, $tbl_9 . '.id = tbl_participant_assistance.assistanceId', 'inner');
            $this->db->where(array('participantId' => $participantId));
            $this->db->where($genral_Where);
            $query = $this->db->get();
            $arr['assistanceId'] = $query->result_array();
            if (!empty($arr['assistanceId'])) {
                $arr['assistanceId'] = $arr['assistanceId'][0]['name'];
            }

            $dt_query = $this->db->select(array("gender", "ethnicity", "religious"));
            $this->db->from('tbl_participant_care_not_tobook');
            $this->db->where(array('participantId' => $participantId));
            $query = $this->db->get();
            $arr['care_not_tobook'] = $query->result_array();

            $dt_query = $this->db->select(array("cognition", "communication", "english", "preferred_language", "linguistic_interpreter"));
            $this->db->from('tbl_participant_care_requirement');
            $this->db->where(array('participantId' => $participantId));
            $query = $this->db->get();
            $care_requirement = $query->row_array();
            if (!empty($care_requirement)) {
                $arr = array_merge($care_requirement, $arr);
            }

            return $arr;
        }
    }

    public function participant_sites($participantID) {
        $tb1 = TBL_PREFIX . 'participant_address';
        $tb2 = TBL_PREFIX . 'state';

        $colown = array($tb1 . ".id", $tb1 . ".street", $tb1 . ".city", $tb1 . ".postal", $tb2 . ".name", $tb1 . ".site_category");
        $this->db->select($colown);
        $this->db->from($tb1);
        $this->db->join($tb2, $tb2 . '.id = ' . $tb1 . '.state', 'inner');
        $this->db->where(array($tb1 . '.participantId' => $participantID, $tb1 . '.archive' => 0));
        $query = $this->db->get();

        return $query->result();
    }

    public function strategies_care_notes($participantID, $type) {
        $tb1 = TBL_PREFIX . 'participant_about_care';
        $categories = array('care_notes', 'strategies');
        $arhive = ($type == 'current') ? 0 : 1;

        $colown = array('id', 'title', 'content', 'created', 'categories');
        $this->db->select($colown);
        $this->db->from($tb1);
        $this->db->where(array('participantId' => $participantID, 'archive' => $arhive));
        $this->db->where_in('categories', $categories);
        $this->db->order_by("updated", "asc");
        $query = $this->db->get();

        return $query->result();
    }

    public function participant_update($participant_data) {
//        print_r($participant_data);
        // for address update
        $this->db->delete(TBL_PREFIX . 'participant_address', $where = array('participantId' => $participant_data['ocs_id']));
        if (!empty($participant_data['address'])) {
            foreach ($participant_data['address'] as $val) {
                unset($val->stateName);
                $val->participantId = $participant_data['ocs_id'];
                if (empty($val->primary_address))
                    $val->primary_address = 2;

                $addrees_data[] = $val;
            }
            $this->db->insert_batch(TBL_PREFIX . 'participant_address', $addrees_data);
        }

        // for phone number update
        $this->db->delete(TBL_PREFIX . 'participant_phone', $where = array('participantId' => $participant_data['ocs_id']));
        if (!empty($participant_data['phones'])) {
            foreach ($participant_data['phones'] as $val) {
                $val->participantId = $participant_data['ocs_id'];
                $phones[] = $val;
            }

            $this->db->insert_batch(TBL_PREFIX . 'participant_phone', $phones);
        }

        // for email number update
        $this->db->delete(TBL_PREFIX . 'participant_email', $where = array('participantId' => $participant_data['ocs_id']));
        if (!empty($participant_data['emails'])) {
            foreach ($participant_data['emails'] as $val) {
                $val->participantId = $participant_data['ocs_id'];
                $emails[] = $val;
            }
            $this->db->insert_batch(TBL_PREFIX . 'participant_email', $emails);
        }

        // for kin details number update
        $this->db->delete(TBL_PREFIX . 'participant_kin', $where = array('participantId' => $participant_data['ocs_id']));
        if (!empty($participant_data['emails'])) {
            foreach ($participant_data['kin_detials'] as $val) {
                if (!empty(array_filter((array) $val))) {
                    unset($val->kinfullname);
                    $val->participantId = $participant_data['ocs_id'];
                    $kin_details[] = $val;
                }
            }
            $this->db->insert_batch(TBL_PREFIX . 'participant_kin', $kin_details);
        }

        $participant = array('firstname' => $participant_data['firstname'], 'middlename' => $participant_data['middlename'], 'lastname' => $participant_data['lastname'], 'username' => $participant_data['username'], 'gender' => $participant_data['gender'], 'ndis_num' => $participant_data['ndis_num'], 'medicare_num' => $participant_data['medicare_num'], 'crn_num' => $participant_data['crn_num'], 'preferredname' => $participant_data['preferredname'], 'prefer_contact' => $participant_data['prefer_contact'], 'dob' => $participant_data['firstname'], 'dob' => $participant_data['dob']);

        $this->db->where($where = array('id' => $participant_data['ocs_id']));
        $this->db->update(TBL_PREFIX . 'participant', $participant);

        return true;
    }

}
