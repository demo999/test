<?php

use classRoles as adminRoles;
use AdminClass as AdminClass;

defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller  
class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin_model');
    }

    public function index() {
        
    }
    
    public function get_user_details(){
        echo json_encode(array('status' => true));
    }

    public function create_edit_user() {
        // get request data
        $reqData = request_handler(1,1);
        $this->load->library('form_validation');

        require_once APPPATH . 'Classes/admin/admin.php';
        $objAdmin = new AdminClass\Admin();

        if (!empty($reqData->data)) {
            $data = (array) $reqData->data;

            $this->form_validation->set_data($data);
            $validation_rules = array(
                array('field' => 'username', 'label' => 'UserName', 'rules' => 'callback_check_username_already_exist'),
                array('field' => 'firstname', 'label' => 'First Name', 'rules' => 'required'),
                array('field' => 'lastname', 'label' => 'Last Name', 'rules' => 'required'),
                array('field' => 'password', 'label' => 'Preferred Name', 'rules' => 'required'),
                array('field' => 'position', 'label' => 'Date Of Birth', 'rules' => 'required'),
                array('field' => 'EmailInput[]', 'label' => 'Email address', 'rules' => 'callback_check_user_emailaddress_already_exist'),
                array('field' => 'PhoneInput[]', 'label' => 'phone number', 'rules' => 'callback_check_phone_number_validation'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {

                $objAdmin->setUsername($data['username']);
                $objAdmin->setPassword($data['password']);
                $objAdmin->setFirstname($data['firstname']);
                $objAdmin->setLastname($data['lastname']);
                $objAdmin->setDepartment($data['department']);
                $objAdmin->setPosition($data['position']);
                $objAdmin->setPrimaryEmail($data['EmailInput'][0]->name);
                $objAdmin->setPrimaryPhone($data['PhoneInput'][0]->name);
                $objAdmin->setSecondaryEmails($data['EmailInput']);
                $objAdmin->setSecondaryPhone($data['PhoneInput']);
                $objAdmin->setRoles($data['rolesList']);
                // create user
                $objAdmin->createUser();

                // insert secondary email
                $objAdmin->insertSecondyEmail();

                // insert secondary email
                $objAdmin->insertSecondyPhone();

                $objAdmin->insertRoleToAdmin();

                $response = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    public function check_phone_number_validation($phone_numbers) {
        foreach ($phone_numbers as $val) {
            if (empty($val)) {
                $this->form_validation->set_message('phone number can not empty', 'phone number can not be empty');
                return false;
            }
        }
        return true;
    }

    // this function used for add and edit role
    public function add_role() {
        require_once APPPATH . 'Classes/admin/role.php';
        $objRoles = new adminRoles\Roles();

        if ($this->input->post()) {
            $role_id = $this->input->post('role_id');
            $objRoles->setrolename($this->input->post('role_name'));
            $objRoles->setroleid($role_id);
            $res = $objRoles->checkAlreadyExist();

            if ($res) {
                if ($role_id > 0) {
                    $objRoles->UpdateAllRoles();
                } else {
                    $objRoles->CreateRole();
                }
                $response = array('status' => true);
            } else {
                $response = array('status' => false, 'error' => 'This role already exist');
            }

            echo json_encode($response);
        }
    }

    public function list_role() {
        $this->load->model('admin_model');

        if ($this->input->post()) {
            $reqData = json_decode($this->input->post('request'));
            $response = $this->admin_model->list_role_dataquery($reqData);

            echo json_encode($response);
        }
    }

    public function delete_role() {
        require_once APPPATH . 'Classes/admin/role.php';
        $objRoles = new adminRoles\Roles();

        if ($this->input->post()) {
            $objRoles->setArchive(1);
            $objRoles->setroleid($this->input->post('role_id'));

            $objRoles->UpdateRoles();

            echo json_encode(array('status' => true));
        }
    }

    public function active_inactive_role() {
        if ($this->input->post()) {
            $role_id = $this->input->post('role_id');
            $status = $this->input->post('status');

            $result = $this->basic_model->update_records('role', $role_data = array('status' => $status), $where = array('id' => $role_id));

            echo json_encode(array('status' => true));
        }
    }

    // this method use for check user email already exist
    public function check_user_emailaddress_already_exist($sever_emailAddress = array()) {
        $this->load->model('admin_model');

        if ($this->input->get() || $sever_emailAddress) {
            $emails = ($this->input->get()) ? $this->input->get() : $sever_emailAddress;

            foreach ($emails as $val) {
                $result = $this->admin_model->check_dublicate_email($val);

                if ($sever_emailAddress) {
                    if (!empty($result)) {
                        $this->form_validation->set_message('check_user_emailaddress_already_exist', 'this ' . $result[0]->email . ' Email address Allready Exist');
                        return false;
                    }
                }

                if ($this->input->get()) {
                    if (!empty($result)) {
                        echo 'false';
                    } else {
                        echo 'true';
                    }
                }

                return true;
            }
        }
    }

    // this method use for check username already exist
    public function check_username_already_exist($sever_end_username = false) {
        if ($this->input->get('username') || $sever_end_username) {
            $username = ($this->input->get('username')) ? $this->input->get('username') : $sever_end_username;

            $result = $this->basic_model->get_row('admin', array('username'), $where = array('username' => $username));

            if ($sever_end_username) {
                if (!empty($result)) {
                    $this->form_validation->set_message('check_username_already_exist', 'User Name Allready Exist');
                    return false;
                }
                return true;
            }

            if (!empty($result)) {
                echo 'false';
            } else {
                echo 'true';
            }
        }
    }

    // using this method get admin list
    public function list_admins() {

        $reqData = request_handler(1,1);

        if (!empty($reqData->data)) {
            $reqData = $reqData->data;
            $reqData = json_decode($reqData);
            $response = $this->admin_model->list_admins_dataquery($reqData);

            echo json_encode($response);
        }
    }

    // this mehtod use for delete user
    public function delete_user() {
        require_once APPPATH . 'Classes/admin/role.php';
        $objRoles = new adminRoles\Roles();
        $reqData = request_handler(1,1);

        if (!empty($reqData->data)) {
            $reqData = $reqData->data;
            $role_id = $reqData->role_id;

            $result = $this->basic_model->update_records('admin', $role_data = array('archive' => 1), $where = array('id' => $role_id));

            echo json_encode(array('status' => true));
        }
    }

    // this method use for active and inactive user
    public function active_inactive_user() {
        $reqData = request_handler(1,1);
        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            $role_id = $reqData->role_id;
            $status = $reqData->status;

            $result = $this->basic_model->update_records('admin', $role_data = array('status' => $status), $where = array('id' => $role_id));

            echo json_encode(array('status' => true));
        }
    }

    public function gell_all_roles() {
        require_once APPPATH . 'Classes/admin/role.php';
        $objRoles = new adminRoles\Roles();

        // get request data
        $reqData = request_handler(1,1);

        $roles = $objRoles->getAllRoles();
        echo json_encode(array('status' => true, 'data' => $roles));
    }

}
