<?php

use Admin\Auth;

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        
    }

    public function check_login() {
        $this->load->helper('cookie');
        require_once APPPATH . 'Classes/admin/auth.php';

        $adminAuth = new Admin\Auth\Auth();

        $reqData = request_handler(0);

        if (!empty($reqData)) {
            $adminAuth->setUsername($reqData->username);
            $adminAuth->setPassword($reqData->password);

            $response = $adminAuth->check_auth();
            if (!empty($response['status'])) {
                $presmission = get_all_permission($adminAuth->getOcsToken());
                $response['permission'] = $presmission;
            }
            echo json_encode($response);
        }
    }

    public function logout() {
        require_once APPPATH . 'Classes/admin/auth.php';
        $adminAuth = new Admin\Auth\Auth();

        // get request data
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $adminAuth->unsetAdminLogin($reqData->data);
            $response = array('status' => true);
        } else {
            $response = array('status' => false);
        }
        echo json_encode($response);
    }

    public function request_reset_password() {
        $this->load->helper('email_template_helper');

        // get request data
        $reqData = request_handler(0);

        if (!empty($reqData)) {
            $email = $reqData->email;

            $where = array('email' => $email);

            $result = $this->basic_model->get_row('admin', $column = array('id', 'firstname', 'lastname', 'status'), $where);
            if (!empty($result)) {
                $rand = mt_rand(10, 100000);
                $token = encrypt_decrypt('encrypt', $rand);

                $where = array('id' => $result->id);
                $this->basic_model->update_records('admin', $data = array('token' => $token), $where);

                $userdata = array(
                    'firstname' => $result->firstname,
                    'lastname' => $result->lastname,
                    'email' => $email,
                    'url' => base_url("reset_password/" . encrypt_decrypt('encrypt', $result->id) . '/' . $token),
                );

                forgot_password_mail($userdata, $cc_email_address = null);
                $response = array('status' => true, 'success' => system_msgs('forgot_password_send_mail_succefully'));
            } else {
                $response = array('status' => false, 'error' => system_msgs('This_email_not_exist_oversystem'));
            }

            echo json_encode($response);
        }
    }

    function reset_password() {
        require_once APPPATH . 'Classes/admin/auth.php';
        // get request data
        $reqData = request_handler(0);
        $adminAuth = new Admin\Auth\Auth();

        if ($reqData) {
            $user_id = encrypt_decrypt('decrypt', $reqData->id);
            $adminAuth->setAdminid($user_id);
            $adminAuth->setOcsToken($reqData->token);

            $result = $adminAuth->verify_token();

            if (!empty($result)) {
                $adminAuth->setPassword($reqData->password);
                $adminAuth->reset_password();
                echo json_encode(array('status' => true, 'success' => system_msgs('password_reset_successfully')));
            } else {
                echo json_encode(array('status' => false, 'error' => system_msgs('verfiy_password_error')));
            }
        } else {
            echo json_encode(array('status' => false, 'error' => system_msgs('verfiy_password_error')));
        }
    }

    public function verify_reset_password_token() {
        require_once APPPATH . 'Classes/admin/auth.php';
        // get request data
        $reqData = request_handler(0);

        if ($reqData) {

            $user_id = encrypt_decrypt('decrypt', $reqData->id);

            $adminAuth = new Admin\Auth\Auth();

            $adminAuth->setAdminid($user_id);
            $adminAuth->setOcsToken($reqData->token);

            $result = $adminAuth->verify_token();
            if (!empty($result)) {
                $response = array('status' => true);
            } else {
                $response = array('status' => false, 'error' => system_msgs('verfiy_password_error'));
            }

            echo json_encode($response);
        }
    }

    public function check_pin() {
        require_once APPPATH . 'Classes/admin/auth.php';
        $reqData = request_handler();

        if (!empty($reqData)) {
            $adminAuth = new Admin\Auth\Auth();
            $adminAuth->setPin($reqData->data);
            $adminAuth->setAdminid($reqData->adminId);

            $response = $adminAuth->verfiy_pin();
            echo json_encode($response);
        }
    }

}
