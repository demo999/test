<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class Common extends MX_Controller {

    function __construct() {

        parent::__construct();
    }

    public function index() {
        
    }

    public function archive_all() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            $result = $this->basic_model->update_records($reqData->table, array('archive' => 1), $where = array('id' => $reqData->id));
            echo json_encode(array('status' => true));
        } else {
            echo json_encode(array('status' => false, 'error' => 'Invalid Request'));
        }
    }

}
