<?php

function send_mail_smtp($to_email, $subject, $body, $cc_email_address = null) {
    $obj = & get_instance();
    $msg = $body;
    $from_email = 'developer@yourdevelopmentteam.com.au';
    $obj->load->library('email');
    $config['protocol'] = "smtp";
    $config['smtp_host'] = 'ssl://smtp.gmail.com';
    //$config['smtp_host'] = 'tls://smtp.gmail.com';
    $config['smtp_port'] = 465;  //25
    $config['smtp_user'] = 'developer@yourdevelopmentteam.com.au';
    $config['smtp_pass'] = 'NPRBpXEtUf';
    $config['charset'] = "utf-8";
    $config['mailtype'] = "html";
    $config['newline'] = "\r\n";
    $config['priority'] = "1";
    $obj->email->initialize($config);
    $obj->email->from($from_email, APPLICATION_NAME);
    $obj->email->to($to_email);
    $obj->email->subject($subject);
    $obj->email->message($msg);
    $obj->email->send();
    $output = $obj->email->print_debugger();

    return true;
}

function send_mail($to_email, $subject, $body, $cc_email_address = null) {
    //mail send using ci library
    $obj = & get_instance();
    $obj->load->library('email');
    $obj->email->set_mailtype('html');
    $obj->email->from('developer@yourdevelopmentteam.com.au', APPLICATION_NAME);
    $obj->email->to($to_email);
    $obj->email->subject($subject);
    $obj->email->message($body);
    $obj->email->send();
    return true;
}

function send_otp_on_mail($userdata, $cc_email_address = null) {
    $obj = & get_instance();
    $subject = 'OCS: One time verfication password';

    $msg = '<table style="max-width:40%; min-width:280px; margin: 0px auto; border: 1px solid #cdcdcd; border-collapse:collapse; font-family:sans-serif;" cellpadding="0" cellspaceing="0">
  

  <tr><td style="padding:15px 30px 0px; font-size:20px; font-weight:600; color:#443d3d;">Password reset information</td></tr>
  <tr><td style="padding:0px 30px 15px"><hr style="margin: 5px 0px; background: #00A63F; height: 2px; border: 0px; width: 127px;"></td></tr>
  <tr><td style="font-size:15px; font-weight:normal; padding:15px 30px; color:#443d3d;">Thanks for contacting us. Follow the directions below to reset your password.</td></tr>
  <tr><td style="padding:15px 30px; color:#fff;"><font style="padding:10px 15px; background:#1882d6; border-radius:2px; color:#fff;">
  <a style="text-decoration:none; color:#fff;">' . $userdata['otp'] . '</a></font></td></tr>
  <tr><td style="font-size:15px; font-weight:normal; padding:15px 30px 10px; color:#443d3d;">After you click the button above, you\'ll be prompted to complete the following steps: </td></tr>
  <tr>
  <td>
    <table style="font-size:15px; font-weight:normal; color:#443d3d; padding:0px 30px;">
        <tr><td>1. </td><td>Enter and confirm your new password.</td></tr>
        <tr><td>2. </td><td>Click "Submit"</td></tr>
    </table>
  </td>
  </tr>
  <tr><td style="font-size:15px; font-weight:normal; padding:15px 30px 10px; color:#443d3d;">
  If you didn\'t request a password reset or you feel you\'ve received this message in error, please call our 24/7 support team right away at 00 000 000. If you take no action, don\'t worry — nobody will be able to change your password without access to this email. 
  </td></tr>
  <tr>

  <tr>
  <td width="100%" style="margin-bottom: 30px; float: left; width: 100%;">
  </td>
  </tr>
  </table>

  <table style="width: 41%; margin: 0px auto; padding: 20px;">
  <tr style="margin-bottom: 20px; font-size: 12px; float: left;  width: 100%;">
  <td style="line-height: 20px; width: 100%; text-align: center; display: block; color: #909090;">&#169;2018-All Righ  Reserved <b>ONCALL</b></td>
  </tr>
  </table>
<style>
a:hover, a:active{
  color:#fff !important;
  text-decoration:none !important;
}
</style>
  ';

    $output = send_mail($userdata['email'], $subject, $msg);

    return $output;
}
