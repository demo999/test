<?php

function system_msgs($msg_key) {
    $global_ary = array(
        'something_went_wrong' => 'Something went wrong.',
        'wrong_username_password' => 'Invalid username and password.',
        'account_not_active' => 'Your account not active.',
        'success_login' => 'Login successfully.',
        'This_email_not_exist_oversystem' => 'This email not exist over system.',
        'forgot_password_send_mail_succefully' => 'Please visit your mail inbox to reset your password.',
        'verfiy_token_error' => 'Invalid request',
        'password_reset_successfully' => 'Password reset succeffully',
		'verfiy_password_error' => 'Invalid request',
		'login_attempt' => 'Login attempt failed please reset password',
		'invalid_json'=>'Not a valid JSON',
		'insert_success' => 'Insert successfully',
		'is_required' => 'is required',
		'feedback_rating' => 'Rating only between 1 to 5 accepted',
		 'encorrect_pin' => 'Incorrect pin',
        'token_verfied' => 'Pin verfied',
        'empty_token_member' => 'empty token or member id not accepted.',
        'mismatch_token_member' => 'member id does not match with token.',
        'blank_member_id' => 'Member id can\'t be blank.',
        'blank_shift_id' => 'Shift id can\'t be blank.',
        'INVALID_JSON' => 'Given Json is not valid.',
        'INVALID_INPUT' => 'Given input is not valid.',
    );
    return $global_ary[$msg_key];
}
function is_required($msg_key){
	
	$msg_key= $msg_key.' is required';
	$response = array('status' => false, 'error' => $msg_key);
	echo json_encode($response);
	exit;
}
function check_valid_email($email){
	if(!valid_email($email)){
		$msg_key= $email.' not a valid email';
		$response = array('status' => false, 'error' => $msg_key);
		echo json_encode($response);
		exit;
	}
}
?>
