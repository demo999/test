<?php

use classPermission\Permission;

function encrypt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is my secret key';
    $secret_iv = 'This is my secret iv';
    $key = hash('sha256', $secret_key);
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ($action == 'encrypt') {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if ($action == 'decrypt') {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}

function pr($data, $die = 1) {
    print_r("<pre>");
    print_r($data);
    if ($die == 1) {
        die;
    }
}

function last_query($die = 0) {
    $ci = & get_instance();
    echo $ci->db->last_query();
    if ($die == 1) {
        die;
    }
}

function all_user_data() {
    $ci = & get_instance();
    echo '<pre>';
    print_r($ci->session->all_userdata());
    exit;
}

function request_handler($check_token = 1, $pin = false) {
    $request_body = file_get_contents('php://input');
    $request_body = json_decode($request_body);

    if ($check_token) {
        $adminId = verifyAdminToken($request_body->request_data, $pin);

        if (!empty($adminId) && $adminId > 0) {
            $request_body->request_data->adminId = $adminId;
            return $request_body->request_data;
        } else {
            echo json_encode(array('status' => false, 'token_status' => true, 'error' => system_msgs('verfiy_token_error')));
            exit();
        }
    } else {
        return $request_body->request_data->data;
    }
}

/*
 *  verfiy token work on both option parameter and private varibale $ocs_token 
 *  return true if token verify and return false if expire and on time out
 */

function verifyAdminToken($token_data, $pin) {
    $CI = & get_instance();
    $return = false;


    if (!empty($token_data)) {
        $response = $CI->basic_model->get_row('admin_login', $columns = array('updated', 'adminId', 'token', 'pin'), $where = array('token' => $token_data->token));

        if (!empty($response)) {
            $diff = strtotime(DATE_TIME) - strtotime($response->updated);
            if ($diff > $CI->config->item('jwt_token_time')) {
                // $return alredy set false
            } else {
                // update time of ocs token
                $CI->basic_model->update_records('admin_login', $columns = array('updated' => DATE_TIME), $where = array('token' => $token_data->token));
                $return = $response->adminId;


                // check restic area pin 
                if ($pin) {
                    if ($response->pin != $token_data->pin) {
                        echo json_encode(array('status' => false, 'pin_status' => true, 'error' => system_msgs('verfiy_token_error')));
                        exit();
                    }
                }
            }
        }
    }
    return $return;
}

function check_permission($token, $pemission_key) {
    require_once APPPATH . 'Classes/admin/permission.php';

    $obj_permission = new classPermission\Permission();
    $obj_permission->check_permission($token, $pemission_key);
}

function get_all_permission($token) {
    require_once APPPATH . 'Classes/admin/permission.php';
    $obj_permission = new classPermission\Permission();
    return $obj_permission->get_all_permission($token);
}

if (!function_exists('getTestimonial')) {

    function getTestimonial() {
        $CI = & get_instance();
        $CI->load->model('testimonial/testimonial_model');
        return $CI->testimonial_model->getTestimonials();
    }

}

function time_ago_in_php($timestamp) {
    $time_ago = strtotime($timestamp);
    $current_time = time();
    $time_difference = $current_time - $time_ago;
    $seconds = $time_difference;

    $minutes = round($seconds / 60); // value 60 is seconds  
    $hours = round($seconds / 3600); //value 3600 is 60 minutes * 60 sec  
    $days = round($seconds / 86400); //86400 = 24 * 60 * 60;  
    $weeks = round($seconds / 604800); // 7*24*60*60;  
    $months = round($seconds / 2629440); //((365+365+365+365+366)/5/12)*24*60*60  
    $years = round($seconds / 31553280); //(365+365+365+365+366)/5 * 24 * 60 * 60

    if ($seconds <= 60) {

        return "Just Now";
    } else if ($minutes <= 60) {

        if ($minutes == 1) {

            return "one minute ago";
        } else {

            return "$minutes minutes ago";
        }
    } else if ($hours <= 24) {

        if ($hours == 1) {

            return "an hour ago";
        } else {

            return "$hours hrs ago";
        }
    } else if ($days <= 7) {

        if ($days == 1) {

            return "yesterday";
        } else {

            return "$days days ago";
        }
    } else if ($weeks <= 4.3) {

        if ($weeks == 1) {

            return "a week ago";
        } else {

            return "$weeks weeks ago";
        }
    } else if ($months <= 12) {

        if ($months == 1) {

            return "a month ago";
        } else {

            return "$months months ago";
        }
    } else {

        if ($years == 1) {

            return "one year ago";
        } else {

            return "$years years ago";
        }
    }
}

function setting_length($x, $length) {
    if (strlen($x) <= $length) {
        return $x;
    } else {
        $y = substr($x, 0, $length) . '...';
        return $y;
    }
}

function get_admin_img($userId, $img, $gender) {
//    $CI = & get_instance();

    if ($gender == 2) {
        return base_url() . '/assets/user/girls.png';
    } else {
        return base_url() . '/assets/user/boy.png';
    }
}

function do_muliple_upload($config_ary) {
    $CI = & get_instance();
    $CI->load->library('upload');
    $response = array();

    if (!empty($config_ary)) {
        $directory_path = $config_ary['upload_path'] . $config_ary['directory_name'];

        $config['upload_path'] = $directory_path;

        $config['allowed_types'] = isset($config_ary['allowed_types']) ? $config_ary['allowed_types'] : '';
        $config['max_size'] = isset($config_ary['max_size']) ? $config_ary['max_size'] : '';
        $config['max_width'] = isset($config_ary['max_width']) ? $config_ary['max_width'] : '';
        $config['max_height'] = isset($config_ary['max_height']) ? $config_ary['max_height'] : '';

        create_directory($directory_path);

        $input_name = $config_ary['input_name'];

        $files = $_FILES;
        $cpt = count($_FILES[$input_name]['name']);

        for ($i = 0; $i < $cpt; $i++) {
            $_FILES[$input_name]['name'] = $files[$input_name]['name'][$i];
            $_FILES[$input_name]['type'] = $files[$input_name]['type'][$i];
            $_FILES[$input_name]['tmp_name'] = $files[$input_name]['tmp_name'][$i];
            $_FILES[$input_name]['error'] = $files[$input_name]['error'][$i];
            $_FILES[$input_name]['size'] = $files[$input_name]['size'][$i];

            $CI->upload->initialize($config);

            if (!$CI->upload->do_upload($input_name)) {
                $response[] = array('error' => $CI->upload->display_errors());
            } else {
                $response[] = array('upload_data' => $CI->upload->data());
            }
        }

        return $response;
    }
}

function create_directory($directoryName) {
    if (!is_dir($directoryName)) {
        mkdir($directoryName, 0755);
        fopen($directoryName . "/index.html", "w");
    }
}

function is_json($string) {
    return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}

function dateRangeBetweenDate($start_date, $end_date) {
    $first = $start_date;
    $last = $end_date;

    $dates = array();
    $step = '+1 day';
    $format = 'Y-m-d';
    $current = strtotime($first);
    $last = strtotime($last);
    while ($current <= $last) {
        $date1 = date($format, $current);
        $dates[$date1] = date('D', $current);
        $current = strtotime($step, $current);
    }
    return $dates;
}

function dayDifferenceBetweenDate($fromDate, $toDate) {
    $now = strtotime($toDate); //current date 
    $your_date = strtotime($fromDate);
    $datediff = $now - $your_date;
    return round($datediff / (60 * 60 * 24));
}

function DateFormate($date, $formate) {
    return date($formate, strtotime($date));
}