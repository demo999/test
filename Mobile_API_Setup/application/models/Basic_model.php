<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Basic_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->load->database();    // Load database
    }

    public function get_row($table_name = '', $columns = array(), $id_array = array()) {
        if (!empty($columns)):
            $all_columns = implode(",", $columns);
            $this->db->select($all_columns);
        endif;
        if (!empty($id_array)):
            foreach ($id_array as $key => $value) {
                $this->db->where($key, $value);
            }
        endif;
        $query = $this->db->get(TBL_PREFIX.$table_name);

        //echo last_query();
        if ($query->num_rows() > 0)
            return $query->row();
        else
            return FALSE;
    }

    // Function for getting records from table with where condition
    function get_record_where($table, $column = '', $where = '') {
        if ($column != '') {
            $this->db->select($column);
        } else {
            $this->db->select('*');
        }
        $this->db->from(TBL_PREFIX.$table);
        if ($where != '') {
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->result();
    }

    // Function for getting records from table with where condition and order by
    function get_record_where_orderby($table, $column = '', $where = '', $orderby = '', $direction = 'ASC') {
        if ($column != '') {
            $this->db->select($column);
        } else {
            $this->db->select('*');
        }
        $this->db->from(TBL_PREFIX.$table);
        if ($where != '') {
            $this->db->where($where);
        }
        if ($orderby != '') {
            $this->db->order_by($orderby, $direction);
        }

        $query = $this->db->get();
        return $query->result();
    }

    // Function for inserting records
    function insert_records($table, $data, $multiple = FALSE) {
        if ($multiple) {
            $this->db->insert_batch(TBL_PREFIX.$table, $data);
        } else {
            $this->db->insert(TBL_PREFIX.$table, $data);
        }

        return $this->db->insert_id();
    }

    // Function for updating records
    function update_records($table, $data, $where) {
        $this->db->where($where);
        $this->db->update(TBL_PREFIX.$table, $data);
        return $this->db->affected_rows();
    }

    // Function for deleting records
    function delete_records($table, $where) {
        $this->db->delete(TBL_PREFIX.$table, $where);
        if (!$this->db->affected_rows()) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function get_result($table_name='', $id_array='',$columns=array(),$order_by=array())
    {
        if(!empty($columns)):
            $all_columns = implode(",", $columns);
            $this->db->select($all_columns);
        endif; 
        if(!empty($order_by)):          
            $this->db->order_by($order_by[0], $order_by[1]);
        endif; 
        
        if(!empty($id_array)):      
            foreach ($id_array as $key => $value){
                $this->db->where($key, $value);
            }
        endif;      
        $query=$this->db->get(TBL_PREFIX.$table_name);
        if($query->num_rows()>0)
            return $query->result();
        else
            return FALSE;
    }
}
