<?php
/**
 *  Class name : Auth
 *  Create date : 18-07-2018,
 *  author : Corner stone solution
 *  Description : this class used for set cookie, check authentication, and set session
 * 
 */

namespace MemberLoginClass;

include APPPATH . 'Classes/API/member/Member.php';

use MemberClass;

class MemberLogin extends MemberClass\Member  {

    public function __construct() {
        $memberClass = new MemberClass\member();		
    }
	
	private $membertoken;
	private $loginupdateddate;
	private $logincreateddate;
	  
    public function getMemberToken(){ return $this->membertoken; }
    public function setMemberToken($membertoken){$this->membertoken = $membertoken;}

	public function getLoginUpdate(){ return $this->loginupdateddate; }
    public function setLoginUpdate($loginupdateddate){$this->loginupdateddate = $loginupdateddate;}
	
	public function getLoginCreated(){ return $this->logincreateddate; }
    public function setLoginCreated($logincreateddate){$this->logincreateddate = $logincreateddate;}
	
	
	public function check_auth() {        
		$CI = & get_instance();
		$CI->load->model('Api_model');
		$login_attempt=3;
		
		$memberrecord=$CI->Api_model->valid_member($this);
		
		//return $memberpin;
		$getPassword=$this->getPin();
	//	return $user_details = password_encrpt($getPassword);
		$response=array();			
		if(count($memberrecord)>0){
			if($memberrecord->loginattempt<$login_attempt){
				if (!$memberrecord->archive){
					if ($memberrecord->status){
					    //echo $getPassword;
					   // echo "<br>";
					   // echo $memberrecord->pin;
						if(password_validate($getPassword,$memberrecord->pin)){
						// Member Id Exist please check password					
						$token = array();
						$token['id'] = $this->getMemberid();						
						$JWT_Token=generate_token($token);
						$this->setMemberToken($JWT_Token);						
						$responseToken=$CI->Api_model->insert_member_token($this);							
						if($responseToken>0){
						    $this->setloginattempt(-1);
							$responseLoginAttempt=$CI->Api_model->update_login_attempt($this);
							$response = array('token' => $JWT_Token, 'status' => true, 'success' => system_msgs('success_login'));
						}else{
							// token not insert in tbl_member_login
							$response = array('status' => false,'data'=>'inactive token'.$responseToken, 'error' => system_msgs('wrong_username_password'));
						}
						
						}else{
							// status inactive member
							
							$this->setloginattempt($memberrecord->loginattempt);
							$responseLoginAttempt=$CI->Api_model->update_login_attempt($this);
														
							$response = array('status' => false, 'data'=>'inactive member', 'error' => system_msgs('wrong_username_password'));
						}				
					}else{
						// status inactive member
						$response = array('status' => false,'data'=>'inactive', 'error' => system_msgs('wrong_username_password'));
					}
				}else{
						// Archive delete user inactive member
						$response = array('status' => false,'data'=>'archive', 'error' => system_msgs('wrong_username_password'));
					}
			}else{
					$responsePrimaryPhone=$CI->Api_model->get_member_primary_phone($this);
					$response_json=json_encode($responsePrimaryPhone);
					// Login attempt failed greater then 3
					$response = array('status' => false,'error' => system_msgs('login_attempt'),'data'=>$response_json);
				}
			
		}else{
			// Member Not Exist
			$response = array('status' => false,'data'=>'Member Not Exist', 'error' => system_msgs('wrong_username_password'));
		}
		return $response;		
    }
}
