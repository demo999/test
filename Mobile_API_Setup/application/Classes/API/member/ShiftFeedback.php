<?php
/**
 *  Class name : Auth
 *  Create date : 17-10-2018,
 *  author : Corner stone solution
 *  Description : used to save shift feedback
 * 
 */

namespace ShiftFeedbackClass;

class ShiftFeedback {
    public $CI;
    function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->model('Basic_model');
        $this->CI->load->model('api_model');
    }
	
	private $shift_id;
	private $member_id;
	private $incident_type;
	private $what_happen;
	  
    public function getShiftId(){ return $this->shift_id; }
    public function setShiftId($shift_id){$this->shift_id = $shift_id;} 

    public function getMemberId(){ return $this->member_id; }
    public function setMemberId($member_id){$this->member_id = $member_id;}

	public function getIncidentType(){ return $this->incident_type; }
    public function setIncidentType($incident_type){$this->incident_type = $incident_type;}
	
	public function getWhatHappen(){ return $this->what_happen; }
    public function setWhatHappen($what_happen){$this->what_happen = $what_happen;}
	
	public function save_shift_feedback() {        
		$CI = & get_instance();
		return $CI->api_model->save_shift_feedback($this);		
    }
}
