<?php

namespace ShiftClass;

/*
 * Filename: Shift.php
 * Desc: Shift of Members, end and start time of shift
 * @author YDT <yourdevelopmentteam.com.au>
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * Class: Shift
 * Desc: Class Has 5 Arrays, variables ans setter and getter methods of shifts
 * Created: 06-08-2018
 */

class Shift {

    public $CI;

    function __construct() {
        $this->CI = & get_instance();
        //$this->CI->load->model('Schedule_model');
        //$this->CI->load->model('Listing_model');
        $this->CI->load->model('Basic_model');
        $this->CI->load->model('api_model');
    }

    /**
     * @var shiftid
     * @access private
     * @vartype: integer
     */
    private $shiftId;

    /**
     * @var booked_by
     * @access private
     * @vartype: tinyint
     */
    private $booked_by;

    /**
     * @var shift_date
     * @access private
     * @vartype: timestamp
     */
    private $shift_date;

    /**
     * @var start_time
     * @access private
     * @vartype: timestamp
     */
    private $start_time;

    /**
     * @var end_time
     * @access private
     * @vartype: timestamp
     */
    private $end_time;

    /**
     * @var so
     * @access private
     * @vartype: tinyint
     */
    private $so;

    /**
     * @var an
     * @access private
     * @vartype: tinyint
     */
    private $an;

    /**
     * @var eco
     * @access private
     * @vartype: tinyint
     */
    private $eco;

    /**
     * @var price
     * @access private
     * @vartype: double
     */
    private $price;

    /**
     * @var allocate_pre_member
     * @access private
     * @vartype: tinyint
     */
    private $allocate_pre_member;

    /**
     * @var autofill_shift
     * @access private
     * @vartype: tinyint
     */
    private $autofill_shift;

    /**
     * @var push_to_app
     * @access private
     * @vartype: tinyint
     */
    private $push_to_app;

    /**
     * @var status
     * @access private
     * @vartype: tinyint
     */
    private $status;

    /**
     * @var created
     * @access private
     * @vartype: timestamp
     */
    private $created;

    /**
     * @array shift_member
     * @access private
     * @vartype: integer|tinyint|timestamp
     */
    private $shift_member = array();

    /**
     * @array shift_org_requirements
     * @access private
     * @vartype: integer|tinyint
     */
    private $shift_org_requirements = array();

    /**
     * @array shift_participant
     * @access private
     * @vartype: integer|tinyint|timestamp
     */
    private $shift_participant = array();

    /**
     * @array shift_preferred_member
     * @access private
     * @vartype: integer|tinyint|timestamp
     */
    private $shift_preferred_member = array();

    /**
     * @array shift_requirements
     * @access private
     * @vartype: integer|tinyint
     */
    private $shift_requirements = array();

    /**
     * @function getShiftid
     * @access public
     * @returns $shiftid integer
     * Get Shift Id
     */
    public function getShiftId() {
        return $this->shiftId;
    }

    /**
     * @function setShiftid
     * @access public
     * @param $shiftid integer 
     * Set Shift Id
     */
    public function setShiftId($shiftId) {
        $this->shiftId = $shiftId;
    }

    /**
     * @function getBookedBy
     * @access public
     * @returns $booked_by tinyint
     * Get BookedBy 
     */
    public function getBookedBy() {
        return $this->booked_by;
    }

    /**
     * @function setBookedBy
     * @access public
     * @param $bookedBy tinyint 
     * Set BookedBy
     */
    public function setBookedBy($bookedBy) {
        $this->booked_by = $bookedBy;
    }

    /**
     * @function getShiftDate
     * @access public
     * @returns $shift_date timestamp
     * Get Shift_date 
     */
    public function getShiftDate() {
        return $this->shift_date;
    }

    /**
     * @function setShiftDate
     * @access public
     * @param $shiftDate timestamp 
     * Set Shift_date
     */
    public function setShiftDate($shiftDate) {
        $this->shift_date = $shiftDate;
    }

    /**
     * @function getStartTime
     * @access public
     * @returns $start_time timestamp
     * Get StartTime 
     */
    public function getStartTime() {
        return $this->start_time;
    }

    /**
     * @function setStartTime
     * @access public
     * @param $startTime timestamp 
     * Set StartTime
     */
    public function setStartTime($startTime) {
        $this->start_time = $startTime;
    }

    /**
     * @function getEndTime
     * @access public
     * @returns $end_time timestamp
     * Get EndTime 
     */
    public function getEndTime() {
        return $this->end_time;
    }

    /**
     * @function setEndTime
     * @access public
     * @param $endTime timestamp 
     * Set EndTime
     */
    public function setEndTime($endTime) {
        $this->end_time = $endTime;
    }

    /**
     * @function getSo
     * @access public
     * @returns $so tinyint
     * Get So 
     */
    public function getSo() {
        return $this->so;
    }

    /**
     * @function setSo
     * @access public
     * @param $so tinyint 
     * Set So
     */
    public function setSo($so) {
        $this->so = $so;
    }

    /**
     * @function getAn
     * @access public
     * @returns $an tinyint
     * Get An
     */
    public function getAo() {
        return $this->ao;
    }

    /**
     * @function setAn
     * @access public
     * @param $an tinyint 
     * Set An
     */
    public function setAo($ao) {
        $this->ao = $ao;
    }

    /**
     * @function getEco
     * @access public
     * @returns $eco tinyint
     * Get Eco 
     */
    public function getEco() {
        return $this->eco;
    }

    /**
     * @function setEco
     * @access public
     * @param $eco tinyint 
     * Set Eco
     */
    public function setEco($eco) {
        $this->eco = $eco;
    }

    /**
     * @function getPrice
     * @access public
     * @returns $price double
     * Get Price 
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * @function setPrice
     * @access public
     * @param $price double 
     * Set Price
     */
    public function setPrice($price) {
        $this->price = $price;
    }

    /**
     * @function getAllocatePreMember
     * @access public
     * @returns $allocate_pre_member tinyint
     * Get AllocatePreMember 
     */
    public function getAllocatePreMember() {
        return $this->allocate_pre_member;
    }

    /**
     * @function setAllocatePreMember
     * @access public
     * @param $allocatePreMember tinyint 
     * Set AllocatePreMember
     */
    public function setAllocatePreMember($allocatePreMember) {
        $this->allocate_pre_member = $allocatePreMember;
    }

    /**
     * @function getAutofillShift
     * @access public
     * @returns $autofill_shift tinyint
     * Get AutofillShift 
     */
    public function getAutofillShift() {
        return $this->autofill_shift;
    }

    /**
     * @function setAutofillShift
     * @access public
     * @param $autofillShift tinyint 
     * Set AutofillShift
     */
    public function setAutofillShift($autofillShift) {
        $this->autofill_shift = $autofillShift;
    }

    /**
     * @function getPushToApp
     * @access public
     * @returns $push_to_app tinyint
     * Get PushToApp 
     */
    public function getPushToApp() {
        return $this->push_to_app;
    }

    /**
     * @function PushToApp
     * @access public
     * @param $pushToApp tinyint 
     * Set PushToApp
     */
    public function setPushToApp($pushToApp) {
        $this->push_to_app = $pushToApp;
    }

    /**
     * @function getStatus
     * @access public
     * @returns $status tinyint
     * Get Status 
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @function setStatus
     * @access public
     * @param $status tinyint 
     * Set Status
     */
    public function setStatus($status) {
        $this->status = $status;
    }

    /**
     * @function getCreated
     * @access public
     * @returns $created tinyint
     * Get Created 
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * @function setCreated
     * @access public
     * @param $created tinyint 
     * Set Created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * @function getShiftMember
     * @access public
     * @returns $shift_member integer|tinyint|timestamp
     * Get ShiftMember 
     */
    public function getShiftMember() {
        return $this->shift_member;
    }

    /**
     * @function setShiftMember
     * @access public
     * @param $shiftMember integer|tinyint|timestamp 
     * Set ShiftMember
     */
    public function setShiftMember($shiftMember) {
        $this->shift_member = $shiftMember;
    }

    /**
     * @function getShiftOrgRequirements
     * @access public
     * @returns $shift_org_requirements integer|tinyint
     * Get ShiftOrgRequirements 
     */
    public function getShiftOrgRequirements() {
        return $this->shift_org_requirements;
    }

    /**
     * @function setShiftOrgRequirements
     * @access public
     * @param $shiftOrgRequirements integer|tinyint 
     * Set ShiftOrgRequirements
     */
    public function setShiftOrgRequirements($shiftOrgRequirements) {
        $this->shift_org_requirements = $shiftOrgRequirements;
    }

    /**
     * @function getShiftParticipant
     * @access public
     * @returns $shift_participant integer|tinyint|timestamp
     * Get ShiftParticipant 
     */
    public function getShiftParticipant() {
        return $this->shift_participant;
    }

    /**
     * @function setShiftParticipant
     * @access public
     * @param $shiftParticipant integer|tinyint|timestamp
     * Set ShiftParticipant
     */
    public function setShiftParticipant($shiftParticipant) {
        $this->shift_participant = $shiftParticipant;
    }

    /**
     * @function getShiftPreferredMember
     * @access public
     * @returns $shift_preferred_member integer|tinyint|timestamp
     * Get ShiftPreferredMember 
     */
    public function getShiftPreferredMember() {
        return $this->shift_preferred_member;
    }

    /**
     * @function setShiftPreferredMember
     * @access public
     * @param $shiftPreferredMember integer|tinyint|timestamp 
     * Set ShiftPreferredMember
     */
    public function setShiftPreferredMember($shiftPreferredMember) {
        $this->shift_preferred_member = $shiftPreferredMember;
    }

    /**
     * @function ShiftRequirements
     * @access public
     * @returns $shift_requirements integer|tinyint
     * Get ShiftRequirements 
     */
    public function getShiftRequirements() {
        return $this->shift_requirements;
    }

    public function get_member_upcoming_shift($where_ary)
    {
        $CI = & get_instance();
        #$CI->load->model('api_model');
        return $CI->api_model->get_member_upcoming_shift($this,$where_ary);
    }  

    public function save_app_feedback()
    {
        $CI = & get_instance();
        $CI->load->model('api_model');
        return $CI->api_model->save_app_feedback($this);  
    }

    public function get_shift_record()
    {
        $CI = & get_instance();
        #$CI->load->model('api_model');
        return $CI->api_model->get_shift_record($this);
    }

    //confirm shift from shift to confirm view
    public function save_shift()
    {
        $CI = & get_instance();
        #$CI->load->model('api_model');
        return $CI->api_model->save_shift($this);   
    }

    public function member_shifts_to_confirm()
    {
        $CI = & get_instance();
        return $CI->api_model->get_member_shift_to_confirm($this);
    }


    public function shift_sign_off($shift_info)
    {
        $CI = & get_instance();
        return $CI->api_model->shift_sign_off($this,$shift_info);   
    }

    public function get_all_shift_by_date($selected_date)
    {
        $CI = & get_instance();
        return $CI->api_model->get_all_shift_by_date($this,$selected_date);
    }

    public function get_shift_for_review()
    {
        $CI = & get_instance();
        return $CI->api_model->get_shift_for_review($this);
    }

    public function get_shift_goal()
    {
        $CI = & get_instance();
        $CI->load->model('api_model');
        return $CI->api_model->get_shift_goal($this);  
    }
}
