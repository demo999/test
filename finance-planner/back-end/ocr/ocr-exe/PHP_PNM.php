<?php
//License: MIT
//Author: de77
//Version: 0.1

function imagepnm($im, $fname)
{
	list($wid, $hei) = array(imagesx($im), imagesy($im));
	$str = "P6\n{$wid} {$hei}\n255\n";
	for ($y=0; $y<$hei; $y++)
	{
		for ($x=0; $x<$wid; $x++)
		{
			$rgb = imagecolorat($im, $x, $y);
			$r = ($rgb >> 16) & 0xFF;
			$g = ($rgb >> 8) & 0xFF;
			$b = $rgb & 0xFF;
	
			$str .= chr($r) . chr($g) . chr($b);
		}
	}
	file_put_contents($fname, $str);
}
