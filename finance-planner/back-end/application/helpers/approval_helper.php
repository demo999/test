<?php

function participant_approval_msgs($msg_key) {
    $global_ary = array(
		'ProfileUpdate' =>array( 'title'=>'Profile','section'=>'Update Profile'),
		'KinUpdate' =>array( 'title'=>'Profile','section'=>'Update Kin Detail'),
		'PhoneUpdate' =>array( 'title'=>'Profile','section'=>'Update Phone Detail'),
		'EmailUpdate' =>array( 'title'=>'Profile','section'=>'Update Email Detail'),
		'AddressUpdate' =>array( 'title'=>'Profile','section'=>'Update Address Detail')        
    );
    return $global_ary[$msg_key];
}

?>
