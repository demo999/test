<?php

use classRoles as adminRoles;
use Gufy\PdfToHtml\Config;
defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class Invoice extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Invoice_model');
        $this->load->helper(array('common', 'auth'));
        if (!$this->session->userdata('username')) {
            // redirect('');
        }
    }
    function list_invoice() {
        $reqData = request_handler();
        $reqData = $reqData->data;
        if (!empty($reqData)) {
            $reqData = $reqData;
            $response = $this->Invoice_model->invoice_list($reqData);
            echo json_encode($response);
        }
    }
    public function list_audit_logs() {
        $reqData = request_handler();
        $reqData = $reqData->data;
        if (!empty($reqData)) {
            $reqData = json_decode($reqData);
            $response = $this->Invoice_model->list_audit_logs($reqData);
            echo json_encode($response);
        }
    }
    public function get_participant_name() {
        $post_data = $this->input->get('query');
        $rows = $this->Invoice_model->get_participant_name($post_data);
        echo json_encode($rows);
    }
    public function get_company_name() {
        $post_data = $this->input->get('query');
        $rows = $this->Invoice_model->get_company_name($post_data);
        echo json_encode($rows);
    }
    public function get_participant_id() {
        $rows = $this->Invoice_model->get_participant_id();
        echo json_encode($rows);
    }
    function participant_invoices() {
        $reqData = request_handler();
        $reqData = $reqData->data;
        if (!empty($reqData)) {
            $reqData = $reqData;
            $response = $this->Invoice_model->participant_invoices($reqData);
            echo json_encode($response);
        }
    }


    function invoice_details(){
      $reqData = request_handler();
      $reqData = $reqData->data;
      if (!empty($reqData)) {
        $invoice_id = $reqData->id;

          // get about Invoice details
          $result = $this->Invoice_model->invoice_details($invoice_id);
          if (!empty($result)) {
            echo json_encode(array('status' => true, 'data' => $result));
          } else {
              echo json_encode(array('status' => false, 'error' => 'Sorry no data found'));
          }
      }
   }
   function status_change(){
    $reqData = request_handler();
     $adminId =$reqData->adminId;
    $reqData = $reqData->data;
    if (!empty($reqData)) {
      $status = $reqData->status;
      $id = $reqData->id;
      $invoicedetails = $reqData->invoicedetails;
      $result = $this->Invoice_model->status_change($status,$id,$invoicedetails,$adminId);
      if (!empty($result)) {
        echo json_encode(array('status' => true, 'data' => $result));
      } else {
          echo json_encode(array('status' => false, 'error' => 'Sorry no data found'));
      }
    }
   }

   function create_dispute_note(){
      $reqData = request_handler();
      $reqData = $reqData->data;
        if (!empty($reqData)) {
          if($reqData->data->method){
            $reqData->data->method = $reqData->data->method->value;
          }
          $noteData = (array) $reqData->data;
          //var_dump($reqData->data); exit;
          $validation_rules = array(
                array('field' => 'notes', 'label' => 'Dispute Notes', 'rules' => 'required|min_length[8]|max_length[1000]'),
                array('field' => 'method', 'label' => 'Contact Method', 'rules' => 'required'),
                array('field' => 'provider', 'label' => 'Contact Name', 'rules' => 'required|max_length[30]'),
                array('field' => 'raised', 'label' => 'Dispute Raised', 'rules' => 'required'),
                array('field' => 'reason', 'label' => 'Dispute Reason', 'rules' => 'required|max_length[30]'),
            );
            $this->form_validation->set_data($noteData);
            $this->form_validation->set_rules($validation_rules);
          if (!$this->form_validation->run() == FALSE){
          $disputeData= array(
            'reason' => $reqData->data->reason,
            'raised' => $reqData->data->raised,
            'contact_name' => $reqData->data->provider,
            'contact_method' => $reqData->data->method,
            'notes' => $reqData->data->notes,
            'invoice_id'=>$reqData->data->invoice_id
          );

          $where =array('invoice_id'=> $reqData->data->invoice_id);
          $count = $this->basic_model->get_record_where('finance_dispute_note','count(*) as total_dispute', $where);
          if($count[0]->total_dispute>10){
            echo json_encode(array('status' => true, 'data' => "Sorry, More than 10 Notes Cannot be created"));
          }else{
              $result = $this->Invoice_model->add_dispute_note($disputeData);
              if (!empty($result)) {
                echo json_encode(array('status' => true, 'data' => "Dispute Note Added Successful"));
              } else {
                  echo json_encode(array('status' => false, 'error' => 'Sorry,Not Created Dispute Note'));
              }

          }

          } else {
             $errors = $this->form_validation->error_array();
              echo json_encode(array('status' => false, 'data' => '', 'error' => $errors));
          }
        }
   }

   function list_dispute_note(){
      $reqData = request_handler();
      $reqData = $reqData->data;
        if (!empty($reqData)) {
          $result = $this->Invoice_model->get_list_dispute_note();
          if (!empty($result)) {
            echo json_encode(array('status' => true, 'data' => $result));
          } else {
              echo json_encode(array('status' => false, 'error' => 'Sorry no data found'));
          }
        }
   }



 function fetch_invoice(){
   $reqData = request_handler();
   $reqData = $reqData->data;
   $last_email_date = "2018-01-15";
   $dateemail = date('d M Y', strtotime($last_email_date));
   $dateemailcheck = date('m/d/Y H:i:s', strtotime($last_email_date));
   $hostname = '{imap.gmail.com:993/imap/ssl}INBOX';
   $username = 'developertest60@gmail.com';
   $password = 'umealone';

   $inbox = imap_open($hostname, $username, $password) or die('Cannot connect to Gmail: ' . imap_last_error());
   $emails=imap_search($inbox, 'SINCE "12 aug 2017"');
   $max_emails = 3;
    if ($emails) {
        $count = 1;
        $cnt = 1;
        $arrcnt = 1;
        sort($emails);
        $arr_email = array();
        foreach ($emails as $email_number) {
            $arr_att = array();

            $namefrom = '';
            $dateemail = '';
            $emailid = '';
            $subject = '';

            $overview = imap_fetch_overview($inbox, $email_number, 0);
            $message = imap_fetchbody($inbox, $email_number, 1.2);
            foreach ($overview as $details) {
                $namefrom = $details->from;
                $dateemail = $details->date;
                $emailid = $details->uid;
                $pdf_date = date('m/d/Y H:i:s', strtotime($details->date));
                $subject = $details->subject;
                $mass = $message;
            }
            if ($dateemailcheck < $pdf_date) {

                $arr_email[$arrcnt]['name'] = $namefrom;
                $arr_email[$arrcnt]['date'] = $dateemail;
                $arr_email[$arrcnt]['mailid'] = $emailid;
                $arr_email[$arrcnt]['subject'] = $subject;


                $structure = imap_fetchstructure($inbox, $email_number);
                $attachments = array();
                if (isset($structure->parts) && count($structure->parts)) {
                    for ($i = 0; $i < count($structure->parts); $i++) {
                        $attachments[$i] = array(
                            'is_attachment' => false,
                            'filename' => '',
                            'name' => '',
                            'attachment' => ''
                        );
                        if ($structure->parts[$i]->ifdparameters) {
                            foreach ($structure->parts[$i]->dparameters as $object) {
                                if (strtolower($object->attribute) == 'filename') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['filename'] = $object->value;
                                }
                            }
                        }
                        if ($structure->parts[$i]->ifparameters) {
                            foreach ($structure->parts[$i]->parameters as $object) {
                                if (strtolower($object->attribute) == 'name') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['name'] = $object->value;
                                }
                            }
                        }

                        if ($attachments[$i]['is_attachment']) {
                            $attachments[$i]['attachment'] = imap_fetchbody($inbox, $email_number, $i + 1);


                            if ($structure->parts[$i]->encoding == 3) {
                                $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                            } elseif ($structure->parts[$i]->encoding == 4) {
                                $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                            }
                        }
                    }
                }

                foreach ($attachments as $attachment) {
                    if ($attachment['is_attachment'] == 1) {
                        $filename = $attachment['name'];


                        $file_name[] = $email_number . "-" . $attachment['name'];

                        if (empty($filename))
                            $filename = $attachment['filename'];

                        if (empty($filename))
                            $filename = time() . ".dat";

                        $filename = strtolower($filename);
                        $fp = fopen(FCPATH."assets/pdf/" . $filename, "w+");
                        fwrite($fp, $attachment['attachment']);
                        fclose($fp);

                        $filename1 = $filename;
                        $arr_att[] = $filename1;
                        $fname[] = $filename1;
                    }
                }
                $arr_email[$arrcnt]['attachment'] = $arr_att;
                $arrcnt++;
                if ($count++ >= $max_emails)
                    break;
            }
            $cnt++;
        }

      }
      foreach($arr_email as $value){
          if(!empty($value['attachment'])){
          include FCPATH.'pdf_to_html_con/vendor/autoload.php';
          $url = base_url()."assets/pdf/".$value['attachment'][0];
          $path = FCPATH."assets/pdf/".$value['attachment'][0];
          Config::set('pdftohtml.bin', FCPATH.'pdf_to_html_con/poppler-0.51/bin/pdftohtml.exe');
          Config::set('pdfinfo.bin', FCPATH.'pdf_to_html_con/poppler-0.51/bin/pdfinfo.exe');
          $pdf = new Gufy\PdfToHtml\Pdf($path);
          $total_pages = $pdf->getPages();
          $html = $pdf->html();
          $html_files =array();
          $filename = str_replace('.pdf', '', $value['attachment'][0]);

          for($i=1;$i<=$total_pages;$i++){
            array_push($html_files,$filename.'-'.$i.'.html');
            rename(FCPATH."pdf_to_html_con/vendor/gufy/pdftohtml-php/src/output/".$filename.'-'.$i.'.html',FCPATH."assets/pdf/".$filename.'-'.$i.'.html');
            rename(FCPATH."pdf_to_html_con/vendor/gufy/pdftohtml-php/src/output/".$filename.'00'.$i.'.png',FCPATH."assets/pdf/".$filename.'00'.$i.'.png');
          }
          $html_files = implode(",",$html_files);
          $data = array('participant_email'=>$username,'pdf_url'=>$value['attachment'][0],'html_url'=>$html_files);
          $table = "finance_invoice";
          $this->basic_model->insert_records($table, $data);
        }
      }
      echo json_encode(array('status' => true, 'data' => 'Successful'));
    }
  }
