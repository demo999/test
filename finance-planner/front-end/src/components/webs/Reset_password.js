import React, { Component } from 'react';

import { Link, Redirect } from 'react-router-dom';
// import { connect } from 'react-redux';
import { ROUTER_PATH, BASE_URL } from '../../config.js';
import jQuery from "jquery";
import axios from 'axios';
import moment from 'moment-timezone';
import '../../service/jquery.validate.js';
import { setRemeber, getRemeber, setLoginTIme } from '../../service/common.js';
import { connect } from 'react-redux';
import { reset_password,verify_token } from '../../store/actions';


class Reset_password extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect:false,loading: false,error:'',success:'',password:'',resetTrue:false
        }
    }


    handleChange = (e) => {
        var state = {};
        this.setState({ error: '' });
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }


    onSubmit=(e) =>{
          e.preventDefault();

          jQuery('#reset_password').validate();
          if ( jQuery('#reset_password').valid()) {
              this.setState({loading: true, error: '', success: ''});

              var resetData = {password: this.state.password, id: this.props.match.params.id, token: this.props.match.params.token};
              this.props.reset_password(resetData).then((result) => {
                this.setState({loading: false});
                  if (result.status) {
                      this.setState({success: result.success});
                      setTimeout(() => this.setState({resetTrue: true}), 2000);
                  } else {
                      this.setState({error: result.error});
                  }
              });

          }
      }

      componentDidMount() {
         this.setState({validateMassage: <span><h2 className="text-center">Please wait..</h2><p className="text-center">we are validating your request</p></span>})
          this.props.verify_token(this.props.match.params).then((result) => {
            this.setState({loading: false});
              if (result.status) {
                  this.setState({validating: true});
                  this.setState({error: ''});
                  this.setState({success: result.success});
              } else {
                  this.setState({validateMassage: <h2 className="text-center">{result.error}</h2>});
              }
          });
      }




    render() {
    if(this.state.resetTrue){
      return(<Redirect to='/'  />);
    }
        return (

            <React.Fragment>
            {//this.state.redirect ? <Redirect to="/dashboard" /> : ''
            }

               <div className='Login_page row '>
                    <div className='col-12' >

                        <div className='log_faceIc'><i className='icon icon-userm1-ie'></i></div>
                        <h1 className='cmn_clr1 loginHdng1__'>Hello there,<br /> Welcome to the Plan Management Module</h1>

                        <div className='login_box '>
                        {(this.state.validating) ?
                            <form id="reset_password" className="login-form"    onSubmit={this.onSubmit}>

                                <div className='row'>

                                    <div className='col-sm-12'>

                                        <div className="csform-group">
                                            <label className='inpLabel1'>Password:</label>
                                            <input type="password" className="csForm_control" id="password" name="password" placeholder="New password" onChange={this.handleChange} value={this.state.password} data-rule-strongPassword="true" min-length="6" data-rule-required="true" data-placement="right" />
                                        </div>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Password:</label>
                                            <input type="password" className="csForm_control" name="confirm_password" placeholder="Confirm password" onChange={this.handleChange} value={this.state.confirm_password} data-rule-required="true" data-rule-equalto="#password" data-msg-equalto="Please enter same password as above" data-placement="right" />
                                        </div>

                                    </div>
                                    <div className='col-sm-12'><br/>
                                        <input type='submit' className='cmn-btn1 btn btn-block' value='Reset password' />
                                    </div>

                                    <div className='col-sm-12'>
                                        {this.state.success? <div className='validMsgBox__ successMsg1'><i className="icon icon-input-type-check"></i><span>{this.state.success}</span></div>:''}
                                        {this.state.error?<div className='validMsgBox__ errorMsg1'><i className="icon icon-alert"></i><span>{this.state.error}</span></div>:''}

                                    </div>
                                    <div className='col-sm-12 text-center'>
                                        <p className='forgot_lnk__'>
                                            <Link to={ROUTER_PATH + 'forgot_password'}>Forgot Password?</Link>
                                        </p>
                                    </div>

                                </div>
                            </form>
                            : <div>{this.state.validateMassage} </div>
                                                    }
                        </div>
                    </div>

                </div>

            </React.Fragment>

        );
    }
}
const mapStateToProps = (state) => {
return  {
  
  }
};
export default connect(mapStateToProps,{reset_password,verify_token})(Reset_password);
