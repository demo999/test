import React, { Component } from 'react';

import { Link, Redirect } from 'react-router-dom';
// import { connect } from 'react-redux';
import { ROUTER_PATH, BASE_URL } from '../../config.js';
import jQuery from "jquery";
import axios from 'axios';
import moment from 'moment-timezone';
import '../../service/jquery.validate.js';
import { setRemeber, getRemeber, setLoginTIme } from '../../service/common.js';
import { connect } from 'react-redux';
import { forgot_password } from '../../store/actions';


class Forgot_password extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect:false,
            error: '',
            success: '',email:''
        }
    }


    handleChange = (e) => {
        var state = {};
        this.setState({success: ''});
        this.setState({ error: '' });
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }


  forgotSubmit = (e) => {
       e.preventDefault();
      jQuery('#forget_password').validate()
      if (jQuery('#forget_password').valid() && !this.state.loading) {

        let { email } = this.state;
        this.props.forgot_password(this.state)
        .then((result) => {
          if (result.status) {
                   this.setState({success: result.success, email : ''});
                   setTimeout(() => this.setState({redirect:true}), 2000);
              } else {
                  this.setState({error: result.error});
              }
        });

    }
  }




    render() {
    if(this.state.redirect){
        return (<Redirect to="/dashboard" />);
     }
        return (

            <React.Fragment>
            {//this.state.redirect ? <Redirect to="/dashboard" /> : ''
            }

               <div className='Login_page row '>
                    <div className='col-12' >

                        <div className='log_faceIc'><i className='icon icon-userm1-ie'></i></div>
                        <h1 className='cmn_clr1 loginHdng1__'>Hello there,<br /> Welcome to the Plan Management Module</h1>

                        <div className='login_box '>
                            <form id="forget_password" className="login-form"  method="POST"  onSubmit={this.forgotSubmit}>

                                <div className='row'>

                                    <div className='col-sm-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Email Address:</label>
                                            <input type="text" className="csForm_control" name="email" placeholder="Email Address" value={this.state.email || ''} onChange={this.handleChange} data-rule-required="true" />
                                        </div>


                                    </div>
                                    {/* <div className='col-sm-12'>
                                        <label className='d-flex align-items-center rmbrDv__ mr_t_15'>
                                            <label className='cstmChekie clrTpe2'>
                                                <input type="checkbox" className=" " defaultChecked={false} />
                                                <div className="chkie"></div>
                                            </label>
                                            <span> Remember Me</span>
                                        </label>
                                    </div> */}
                                    <div className='col-sm-12'><br/>
                                        <input type='submit' className='cmn-btn1 btn btn-block' value='Request' />
                                    </div>

                                    <div className='col-sm-12'>
                                        {this.state.success? <div className='validMsgBox__ successMsg1'><i className="icon icon-input-type-check"></i><span>{this.state.success}</span></div>:''}
                                        {this.state.error?<div className='validMsgBox__ errorMsg1'><i className="icon icon-alert"></i><span>{this.state.error}</span></div>:''}

                                    </div>

                                </div>
                            </form>
                            <h5 className="col-md-12 text-center P_30_T text-center for_text button_small"><Link to={ROUTER_PATH} >Login here</Link></h5>
                        </div>
                    </div>

                </div>

            </React.Fragment>

        );
    }
}

const mapStateToProps = (state) => {
return  {
  forgot_password: state.Reducer1.forgot_password,

  }
};
export default connect(mapStateToProps,{forgot_password})(Forgot_password);
