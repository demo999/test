import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ReactTable from "react-table";
import AuthWrapper from '../hoc/AuthWrapper';
import { connect } from 'react-redux';
import  {participantInvoice} from '../store/actions';


class AllInvoicesDetail extends Component {

  constructor(props) {
      super(props);


  }

  componentWillMount() {
    this.props.participantInvoice(this.props.match.params.id);
   }



  render() {
    const { participantInvoiceDetails } =   this.props;

        const hdrColumn = [
            {

                Header: (props) =>
                    <label className='cstmChekie clrTpe2'>
                        <input type="checkbox" className=" " />
                        <div className="chkie"></div>
                    </label>,
                accessor: "id",
                width: 70,
                sortable: false,
                filterable: false,
                Cell: (props) =>
                    <label className='cstmChekie clrTpe3'>
                        <input type="checkbox" className=" " />
                        <div className="chkie"></div>
                    </label>,

            },
            { Header: 'Due Date', accessor: 'due_date' },
            { Header: 'Biller', accessor: 'biller' },
            { Header: 'Invoice Amount', accessor: 'invoice_amount' },
            { Header: 'Follow Up', accessor: 'invoice_amount' },
            {
                Header: 'Status',
                accessor: 'status',
                sortable: false,
                filterable: false,
                Cell: (props) =>
                    <div className='stat_grp'>
                    {
                        (props.original.status === 'Duplicate') ?<Link to={'/duplicateinvoices/viewduplicateinvoice/'+props.original.id}> <span className='status duplicate'>Duplicate</span></Link>
                        :
                        (props.original.status === 'inQueue') ? <Link to={'/pendinginvoices/viewpendinginvoice/'+props.original.id}><span className='status inQueue'>In Queue</span></Link>
                        :
                        (props.original.status === 'followUp')? <Link to={'/followup/viewfollowup/'+props.original.id}><span className='status followUp'>Follow Up</span></Link>
                        :(props.original.status === 'Paid')? <Link to={'/duplicateinvoices'}><span className='status paid'>Paid</span></Link>
                        :' '
                    }

                    </div>
            }

        ]

        const dashboardData = [
            {
                date: '12/06/19',
                biller: 'Telestra',
                amount: '$15256',
                number: '$15256'
            },
            {
                date: '12/06/19',
                biller: 'Telestra',
                amount: '$15256',
                number: '$15256'
            },
            {
                date: '12/06/19',
                biller: 'Telestra',
                amount: '$15256',
                number: '$15256'
            },
            {
                date: '12/06/19',
                biller: 'Telestra',
                amount: '$15256',
                number: '$15256'
            }


        ]


        return (

            <AuthWrapper>

                        <div className=" back_col_cmn-">
                            <Link to={'/allinvoices'}><span className="icon icon-back1-ie"></span></Link>
                        </div>

                        <div className="main_heading_cmn-">
                            <h1>
                                <span>All Invoices: {typeof(participantInvoiceDetails[0])!='undefined'?participantInvoiceDetails[0].full_name:'N/A'}</span>
                                <Link to='#'>
                                    <button className="btn hdng_btn cmn-btn1">Run Planner</button>
                                </Link>
                            </h1>
                        </div>

                        <div className='mini_profile__ d-flex bor_bot1'>

                            <div className='pro_ic1'>
                                <i className='icon icon-userm1-ie'></i>
                            </div>

                            <div className='prf_cntPart'>
                                <h4 className='cmn_clr1'><strong>{typeof(participantInvoiceDetails[0])!='undefined'?participantInvoiceDetails[0].full_name:'N/A'}</strong></h4>
                                <ul>
                                    <li><strong>OCS ID:</strong>{typeof(participantInvoiceDetails[0])!='undefined'?participantInvoiceDetails[0].id:'N/A'}</li>
                                    <li><strong>Mail ID:</strong>{typeof(participantInvoiceDetails[0])!='undefined'?participantInvoiceDetails[0].participant_email:'N/A'}</li>
                                </ul>
                                <div className='d-flex'>
                                    <div>
                                        <select className='cstmSlctHt stats_slct' >
                                            <option>Active</option>
                                            <option>Inactive</option>
                                        </select>
                                    </div>
                                    <div>
                                        <h5 className='avl_fnd'>
                                            <strong>Avail Funds :</strong>
                                            <span>$15246</span>
                                        </h5>
                                    </div>
                                </div>
                            </div>

                        </div>




                        <div className="data_table_cmn dashboard_Table text-center">
                            <ReactTable
                                data={participantInvoiceDetails}
                                defaultPageSize={10}
                                minRows={1}
                                className="-striped -highlight"
                                previousText={<span className="icon icon-arrow-1-left previous"></span>}
                                nextText={<span className="icon icon-arrow-1-right next"></span>}
                                columns={hdrColumn}
                            />
                        </div>
                        {/* table comp ends */}

            </AuthWrapper>

        );
    }
}



const mapStateToProps = state => {
return {  participantInvoiceDetails: state.InvoiceReducer.participantInvoiceDetails

  }
};

const mapDispatchToProps = {
  participantInvoice
}
export default connect(mapStateToProps,mapDispatchToProps)(AllInvoicesDetail);

// export default AllInvoicesDetail;
