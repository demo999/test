import React, { Component } from 'react';
import Select from 'react-select-plus';
import ReactTable from "react-table";
import AuthWrapper from '../hoc/AuthWrapper';
import DatePicker from "react-datepicker";

class SyncLogs extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filterVal: '',
            startDate: null
        }
    }


    render() {

        const hdrColumn = [
            {

                Header: (props) =>
                    <label className='cstmChekie clrTpe2'>
                        <input type="checkbox" className=" " />
                        <div className="chkie"></div>
                    </label>,
                accessor: "",
                width: 70,
                sortable: false,
                filterable: false,
                Cell: (props) =>
                    <label className='cstmChekie clrTpe3'>
                        <input type="checkbox" className=" " />
                        <div className="chkie"></div>
                    </label>,

            },
            { Header: 'Sync Type', accessor: 'type' },
            { Header: 'Date Completed', accessor: 'completed' },
            { Header: 'Records Processed', accessor: 'processed' },
            {
                Header: 'Status',
                accessor: 'status',
                sortable: false,
                filterable: false,
                Cell: (props) =>
                    <div className='stat_grp'>
                        {
                            (props.index % 2 === 0) ? <span className='status duplicate'>Failed</span>
                            : <span className='status paid'>Successful</span>
                        }

                    </div>
            },
            {
                expander: true,
                Header: () => <strong></strong>,
                width: 55,
                headerStyle: { border: "0px solid #fff" },
                Expander: ({ isExpanded, ...rest }) =>
                    <div className="rec-table-icon">
                        {isExpanded
                            ? <i className="icon icon-arrow-down icn_ar1"></i>
                            : <i className="icon icon-arrow-right icn_ar1"></i>}
                    </div>,
                style: {
                    cursor: "pointer",
                    fontSize: 25,
                    padding: "0",
                    textAlign: "center",
                    userSelect: "none"
                },

            }


        ]

        const dashboardData = [
            {
                type: 'input 1',
                completed: 'input 2',
                processed: 'input 3',

            },
            {
                type: 'input 1',
                completed: 'input 2',
                processed: 'input 3'
            },
            {
                type: 'input 1',
                completed: 'input 2',
                processed: 'input 3'
            },
            {
                type: 'input 1',
                completed: 'input 2',
                processed: 'input 3'
            },
            {
                type: 'input 1',
                completed: 'input 2',
                processed: 'input 3'
            }


        ]

        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];


        return (

            <AuthWrapper>

                <div className=" back_col_cmn-">
                    <span onClick={() => window.history.back()} className="icon icon-back1-ie"></span>
                </div>

                <div className="main_heading_cmn-">
                    <h1>
                        <span>Sync Logs</span>

                    </h1>
                </div>

                <div className='row pd_t_15'>
                    <div className='col-xl-5 col-lg-7 col-7 mr_b_15_min'>

                        <div className='cstm_select search_filter brdr_slct'>

                            <Select name="view_by_status "
                                required={true} simpleValue={true}
                                searchable={true} Clearable={false}
                                placeholder="| Names, numbers, dates & status'"
                                options={options}
                                onChange={(e) => this.setState({ filterVal: e })}
                                value={this.state.filterVal}

                            />

                        </div>

                    </div>

                    <div className='col-xl-3 col-lg-5 col-5 mr_b_15_min'>

                        <div className='cstm_select  slct_cstm2 bg_grey'>

                            <Select name="view_by_status "
                                simpleValue={true}
                                searchable={false} Clearable={false}
                                placeholder="Status"
                                options={options}
                                onChange={(e) => this.setState({ categoryVal: e })}
                                value={this.state.categoryVal}

                            />

                        </div>

                    </div>

                    <div className='col-xl-2 col-lg-4 col-4'>
                        <div className='d-flex align-items-center'>
                            <label className='pd_r_10'>From</label>
                            <DatePicker

                                className="csForm_control"
                                selected={this.state.fromDate}
                                onChange={(e) => this.setState({ fromDate: e })}
                                dateFormat="DD/MM/YYYY"
                                name="tempDueDate"
                                placeholderText={'DD/MM/YY'}
                            />
                        </div>
                    </div>

                    <div className='col-xl-2 col-lg-4 col-4'>
                        <div className='d-flex align-items-center'>
                            <label className='pd_r_10'>To</label>
                            <DatePicker

                                className="csForm_control"
                                selected={this.state.toDate}
                                onChange={(e) => this.setState({ toDate: e })}
                                dateFormat="DD/MM/YYYY"
                                name="tempDueDate"
                                placeholderText={'DD/MM/YY'}
                            />
                        </div>
                    </div>

                </div>


                <div className="data_table_cmn dashboard_Table pending_invTable text-center bor_top1 pd_tb_15" >
                    <ReactTable
                        data={dashboardData}
                        defaultPageSize={dashboardData.length}
                        className="-striped -highlight"
                        previousText={<span className="icon icon-arrow-1-left previous"></span>}
                        nextText={<span className="icon icon-arrow-1-right next"></span>}
                        columns={hdrColumn}
                        SubComponent={(ev) =>
                            <div className='cmn_sbComp'>Sub component</div>
                        }
                    />
                </div>
                {/* table comp ends */}

            </AuthWrapper>

        );
    }
}





export default SyncLogs;
