import React, { Component } from 'react';

import Header from '../components/utils/Header';
import Sidebar from '../components/utils/Sidebar';
import { checkLoginWithReturnTrueFalse } from '../service/common.js';
import { Redirect } from 'react-router-dom';
class AuthWrapper extends Component {


    render() {
        if(!checkLoginWithReturnTrueFalse()){return <Redirect to="/" />}
        return (

            <React.Fragment>

                <Header />

                <section className='asideSect__'>

                    <Sidebar />

                    <div className='container-fluid'>

                        <div className='row justify-content-center '>

                            <div className='col-xl-11 col-lg-12 col-md-12 col-12'>
                                {this.props.children}
                            </div>
                        </div>

                    </div>

                </section>

            </React.Fragment>

        );
    }
}


export default AuthWrapper;
