import {postData} from '../../service/common.js';
import moment from 'moment-timezone';
export const SIDEBAROPEN = 'sidebarOpen';
export const SIDEBARCLOSE = 'closeSidebar';
export const LOGIN = 'logIn';
export const LOGOUT = 'logOut';

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';

export const COUNT_SUCCESS = 'COUNT_SUCCESS';
export const MEMBERS_SUMMARY = 'MEMBERS_SUMMARY';

export const LOADERSTATE = 'LOADERSTATE';
export const PROCESSED_INVOICE = 'PROCESSED_INVOICE';
export const GRAPH_DATA = 'GRAPH_DATA';
export const FORGOT_PASSWORD = 'FORGOT_PASSWORD';
export  function login(data) {
  return dispatch => {
     dispatch(request({ data }));
    return postData('finance_planner/login/check_login',  data).then((json) => {
      if(json.status){
        localStorage.setItem("finance_membertoken", json.token);
        localStorage.setItem("finance_memberid", json.ocs_id);
        localStorage.setItem("dateTime",moment());
        dispatch(success(json.success));
        return json;
      } else {
         dispatch(failure(json.error));
      }

      })
      .catch(error => dispatch(failure(error.toString())));
  };
    function request(request) { return { type: LOGIN,request: request } }
    function success(success) { return { type: LOGIN_SUCCESS,success: success } }
    function failure(error) { return { type: LOGIN_FAILURE,error: error } }
}
export function forgot_password(data) {
  return dispatch => {
  //    dispatch(request({ data }));
    return postData('finance_planner/login/request_reset_password',  data);

  };
  // function request(request) { return { type: FORGOT_PASSWORD,request: request } }
}
export function reset_password(data) {
  return dispatch => {
    return postData('finance_planner/login/reset_password', data);
};

}
export function verify_token(data) {
  return dispatch => {
    return postData('finance_planner/login/verify_reset_password_token',  data);
  };

}
export function dashboardCountData(){
  return dispatch => {
    dispatch(loader(true));
    postData('finance_planner/dashboard/all_typeofinvoice_count',  {}).then((json) => {
      if(json.status){
        dispatch(success(json.data));
        dispatch(loader(false));
        return json.data;
      }
    });
  };
  function success(countData) { return { type: COUNT_SUCCESS,countData: countData } }
}
export function get_members_summary(){
  return dispatch => {
    dispatch(loader(true));
    return postData('finance_planner/dashboard/get_members_summary',  {}).then((json) => {
      if(json.status){
        dispatch(success(json.data));
        dispatch(loader(false));
        return json.data;
      }
    });
  };
  function success(data) { return { type: MEMBERS_SUMMARY,summary: data } }
}
export function get_processed_invoice(){
  return dispatch => {
    dispatch(loader(true));
    return postData('finance_planner/dashboard/get_processed_invoice',  {}).then((json) => {
      if(json.status){
        dispatch(success(json.data));
        dispatch(loader(false));
        return json.data;
      }
    });
  };
  function success(data) { return { type: PROCESSED_INVOICE,processedInvoice: data } }
}

export function get_graph_data(){
  return dispatch => {
    dispatch(loader(true));
    return postData('finance_planner/dashboard/get_graph_data',  {}).then((json) => {
      if(json.status){
        console.log(json.data);
        dispatch(success(json.data));
        dispatch(loader(false));
        return json.data;
      }
    });
  };
  function success(data) { return { type: GRAPH_DATA,graphdata: data } }
}


export function loader(loadState){return {type:LOADERSTATE, loadState:loadState}}
