import {postData} from '../../service/common.js';
import {loader} from './default.actions.js';
export const FETCH_ALL_INVOICE = 'FETCH_ALL_INVOICE';
export const FETCH_PENDING_INVOICE = 'FETCH_PENDING_INVOICE';
export const FETCH_DUPLICATE_INVOICE = 'FETCH_DUPLICATE_INVOICE';
export const FETCH_FOLLOW_UP_INVOICE = 'FETCH_FOLLOW_UP_INVOICE';
export const FETCH_PARTICIPANT_INVOICE = 'FETCH_PARTICIPANT_INVOICE';
export const INVOICE_DETAILS = 'INVOICE_DETAILS';
export const PDF_DATA = 'PDF_DATA';
export const INVOICE_STATUS = 'INVOICE_STATUS';
export const FETCH_INVOICE = 'FETCH_INVOICE';
export const DISPUTE_NOTE = "DISPUTE NOTE CREATE";
export const GET_DISPUTE_NOTES = "GET ALL DISPUTE NOTES";
export const PARTICIPANT_ID = 'PARTICIPANT_ID';
export const AUDIT_LOGS = 'AUDIT_LOGS';
export const pdfData = (data) =>({
  type:PDF_DATA,
  data:data
})
export function invoice_status(status,id,invoicedetails={}){
  let request_data = {status:status,id:id,invoicedetails:invoicedetails};
  return  dispatch=>{
     dispatch(loader(true));
      postData('finance_planner/invoice/status_change',request_data)
    // .then(res => res.json())
      .then(json => {
        dispatch(fetchInvoiceStatus(json.data));
        dispatch(loader(false));
        return json.data;
      })
};

    function fetchInvoiceStatus(status) { return { type: INVOICE_STATUS,InvoiceStatus: status } }
  }
    export function fetch_invoice(){
      let request_data = {};
      return  dispatch=>{
         dispatch(loader(true));
          postData('finance_planner/invoice/fetch_invoice',request_data)
        // .then(res => res.json())
          .then(json => {
            dispatch(fetchInvoice(json.data));
            dispatch(loader(false));
            return json.data;
          })
    };

    function fetchInvoice(status) { return { type: FETCH_INVOICE,FetchInvoice: status } }
  }
export function allInvoice(){
  let request_data = {data:0};
  return  dispatch=>{
     dispatch(loader(true));
      postData('finance_planner/invoice/list_invoice',request_data)
    // .then(res => res.json())
      .then(json => {
        dispatch(fetchAllInvoice(json.data));
        dispatch(loader(false));
        return json.data;
      })



  };

    function fetchAllInvoice(allInvoice) { return { type: FETCH_ALL_INVOICE,allInvoice: allInvoice } }
  }

export function pendingInvoice(){
  let request_data = {data:4 };
  return  dispatch=>{
    dispatch(loader(true));
    postData('finance_planner/invoice/list_invoice',request_data)
      // .then(res => res.json())
      .then(json => {
        dispatch(fetchPendingInvoice(json.data));
        dispatch(loader(false));
        return json.data;
      })
      .catch(error => console.log(error));
  };
  function fetchPendingInvoice(pendingInvoice) { return { type: FETCH_PENDING_INVOICE,pendingInvoice: pendingInvoice } }


}
export function duplicateInvoice(){
  let request_data = {data:2 };
  return  dispatch=>{
    dispatch(loader(true));
    postData('finance_planner/invoice/list_invoice',request_data)
      // .then(res => res.json())
      .then(json => {
        dispatch(fetchDuplicateInvoice(json.data));
        dispatch(loader(false));
        return json.data;
      })
      .catch(error => console.log(error));
  };
  function fetchDuplicateInvoice(duplicateInvoice) { return { type: FETCH_DUPLICATE_INVOICE,duplicateInvoice: duplicateInvoice } }


}
export function followUpInvoices(){
  let request_data = {data:3};
  return  dispatch=>{
    dispatch(loader(true));
    postData('finance_planner/invoice/list_invoice',request_data)
      // .then(res => res.json())
      .then(json => {
        dispatch(followUpInvoice(json.data));
        dispatch(loader(false));
        return json.data;
      })
      .catch(error => console.log(error));
  };
  function followUpInvoice(followUpInvoice) { return { type: FETCH_FOLLOW_UP_INVOICE,followUpInvoice: followUpInvoice } }

  }
export function participantInvoice(id){
  let request_data = {data:id};
  return  dispatch=>{
    dispatch(loader(true));
    postData('finance_planner/invoice/participant_invoices',request_data)
      // .then(res => res.json())
      .then(json => {
        dispatch(participant_invoice(json.data));
        dispatch(loader(false));
        return json.data;
      })
      .catch(error => console.log(error));
  };
  function participant_invoice(participant_invoice) { return { type: FETCH_PARTICIPANT_INVOICE,participant_invoice: participant_invoice } }

  }

  export function invoiceDetails(id){
    let request_data = {'id':id};
    return  dispatch=>{
      return  postData( 'finance_planner/invoice/invoice_details',request_data)
        // .then(res => res.json())
        // .then(json => {
        //   dispatch(fetchInvoiceDetails(json.data));
        //   return json.data;
        // })
        // .catch(error => console.log(error));
    };
    function fetchInvoiceDetails(invoicedetails) { return { type: INVOICE_DETAILS,invoicedetails: invoicedetails } }
    }
  // Handle HTTP errors since fetch won't.
  function getLoginToken() {
      return localStorage.getItem("finance_membertoken");
  }


export  function createDisputeNotes(data){
    let request_data ={data}
    return  dispatch=>{
      dispatch(loader(true));
    return postData('finance_planner/invoice/create_dispute_note',request_data)
        // .then(res => res.json())
        .then(json => {
          if(json.status==true){
            dispatch(disputeSuccess(json.data));
          } else {
            dispatch(disputeFailure(json.error));
          }

          dispatch(loader(false));
          return json;
        })
        .catch(error => console.log(error));
    };
    function disputeSuccess(success) { return { type: DISPUTE_NOTE,disputeNoteSuccess: success } }
    function disputeFailure(error) { return { type: DISPUTE_NOTE,disputeNoteError: error } }
  }

  export  function getAllDisputeNotes(){
    let request_data ={data:'getalldispute'}
      return  dispatch=>{
        dispatch(loader(true));
        postData('finance_planner/invoice/list_dispute_note',request_data)
          // .then(res => res.json())
          .then(json => {
            dispatch(alldisputenotes(json.data));
            dispatch(loader(false));
            return json.data;
          })
          .catch(error => console.log(error));
      };
      function alldisputenotes(allnotes) { return { type: GET_DISPUTE_NOTES,disputeAllNotes: allnotes } }
    }

    export function getOptionsCrmParticipantId() {
    let request_data = {};
    return  dispatch=>{
      return  postData( 'finance_planner/invoice/get_participant_id',request_data)

      // .then(res => console.log(res))
      .then(res => {
        dispatch(fetchOptionsCrmParticipantId(res));
        return res;
      })
      .catch(error => console.log(error));
    };
    function fetchOptionsCrmParticipantId(CrmParticipantId) { return { type: PARTICIPANT_ID,CrmParticipantId: CrmParticipantId } }
  }
  export function list_audit_logs(pageSize,page,sorted,filtered){
    return dispatch => {
      // dispatch(loader(true));

        var Request = JSON.stringify({ pageSize: pageSize, page: page, sorted: sorted, filtered: filtered });
        return postData('finance_planner/invoice/list_audit_logs',  Request)
        .then(res => {
          dispatch(auditLogs(res.data));
          return res.data;
        })
    };
    function auditLogs(auditLogs) { return { type: AUDIT_LOGS,auditLogs: auditLogs } }
  }
