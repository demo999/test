ALTER TABLE `tbl_crm_participant_stage_docs` ADD PRIMARY KEY(`id`);
ALTER TABLE `tbl_crm_participant_stage_docs` ADD `title` VARCHAR(100) NOT NULL AFTER `stage_id`;
ALTER TABLE `tbl_crm_participant_stage_docs` CHANGE `status` `archive` TINYINT(1) NULL DEFAULT NULL COMMENT '1- Active, 0- Inactive';