DROP TABLE `tbl_crm_staff_address`, `tbl_crm_staff_email`, `tbl_crm_staff_phone`;

DROP TABLE IF EXISTS `tbl_crm_staff`;
CREATE TABLE `tbl_crm_staff` (
  `id` int(8) NOT NULL,
  `department_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_crm_staff` (`id`, `department_id`, `admin_id`) VALUES
(1, 1, 56),
(2, 2, 55),
(11, 3, 57);

DROP TABLE IF EXISTS `tbl_crm_staff_disable`;
CREATE TABLE `tbl_crm_staff_disable` (
  `id` int(10) NOT NULL,
  `crm_staff_id` int(10) NOT NULL,
  `disable_account` varchar(30) NOT NULL,
  `account_allocated` varchar(30) NOT NULL,
  `account_allocated_to` int(10) NOT NULL,
  `relevant_note` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tbl_crm_staff_department_allocations`;
CREATE TABLE `tbl_crm_staff_department_allocations` (
  `id` tinyint(3) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `allocated_department` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `tbl_crm_staff_department_allocations` (`id`, `admin_id`, `allocated_department`, `status`, `created`) VALUES
(1, 56, 2, 1, '2019-02-25 17:31:37'),
(2, 56, 1, 1, '2019-02-25 05:00:00'),
(3, 55, 4, 1, '2019-02-25 17:31:37'),
(4, 57, 3, 1, '2019-02-25 05:00:00');

ALTER TABLE `tbl_crm_staff`
ADD PRIMARY KEY (`id`);
  
ALTER TABLE `tbl_crm_staff_department_allocations`
 ADD PRIMARY KEY (`id`);
 
ALTER TABLE `tbl_crm_staff_disable`
  ADD PRIMARY KEY (`id`);
  
 ALTER TABLE `tbl_crm_staff`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
 
ALTER TABLE `tbl_crm_staff_department_allocations`
  MODIFY `id` tinyint(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
  
ALTER TABLE `tbl_crm_participant_stage` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

DROP TABLE IF EXISTS `tbl_crm_department`;
CREATE TABLE `tbl_crm_department` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `hcmgr_id` varchar(20) NOT NULL,
  `archive` tinyint(4) NOT NULL COMMENT '0- Not / 1 - Delete',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `tbl_crm_department` (`id`, `name`, `hcmgr_id`, `archive`, `created`) VALUES
(1, 'Accommodation & Client Services', '00120812', 0, '2019-02-22 02:11:09'),
(2, 'Casual Staff Services', '00120813', 0, '2019-02-22 03:16:43'),
(3, 'People & Culture', '00120814', 0, '2019-02-22 17:11:34'),
(4, 'Business Systems', '00120815', 0, '2019-02-22 06:38:39'),
(5, 'Marketing', '00120816', 0, '2019-02-21 14:09:14'),
(6, 'Finance', '00120817', 0, '2019-02-21 04:17:12');

ALTER TABLE `tbl_crm_department`
ADD PRIMARY KEY (`id`);
  
 ALTER TABLE `tbl_crm_department`
 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

 ALTER TABLE `tbl_crm_participant_schedule_task` CHANGE `due_date` `due_date` DATE NOT NULL;