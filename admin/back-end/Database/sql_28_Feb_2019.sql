ALTER TABLE `tbl_internal_message` ADD `updated` DATETIME NOT NULL AFTER `created`;

ALTER TABLE `tbl_internal_message` CHANGE `created` `created` DATETIME NOT NULL;


////////////////////****************//////////////////////////
-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 23, 2019 at 09:28 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ocs`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_recruitment_applicant`
--

CREATE TABLE `tbl_recruitment_applicant` (
  `id` int(10) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `applicant_code` varchar(30) NOT NULL,
  `application_category` tinyint(3) NOT NULL,
  `applicant_classification` tinyint(1) NOT NULL COMMENT 'skilled , non skilled etc',
  `date_applide` datetime NOT NULL,
  `job_exeperiance` tinyint(1) NOT NULL COMMENT '0=No/1=Yes',
  `created` datetime NOT NULL,
  `lastupdate` datetime NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1- Active, 0- Inactive',
  `currunt_stage` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_recruitment_applicant_address`
--

CREATE TABLE `tbl_recruitment_applicant_address` (
  `id` int(11) NOT NULL,
  `applicant_id` int(10) NOT NULL,
  `street` varchar(128) NOT NULL,
  `city` varchar(64) NOT NULL,
  `postal` varchar(10) NOT NULL,
  `state` tinyint(2) NOT NULL,
  `latitude` varchar(30) NOT NULL,
  `longitude` varchar(30) NOT NULL,
  `created` datetime NOT NULL,
  `primary_address` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_recruitment_applicant_doc`
--

CREATE TABLE `tbl_recruitment_applicant_doc` (
  `id` int(11) NOT NULL,
  `document_name` varchar(100) NOT NULL,
  `document_path` varchar(100) NOT NULL,
  `applicant_id` int(10) NOT NULL,
  `document_type` tinyint(1) NOT NULL COMMENT '1=Resume/2=cover letter/3=Qualification/4=First Aid',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_recruitment_applicant_email`
--

CREATE TABLE `tbl_recruitment_applicant_email` (
  `id` int(11) NOT NULL,
  `applicant_id` int(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `primary_email` tinyint(1) NOT NULL COMMENT '1- Primary, 2- Secondary'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_recruitment_applicant_phone`
--

CREATE TABLE `tbl_recruitment_applicant_phone` (
  `id` int(11) NOT NULL,
  `applicant_id` int(10) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `created` datetime NOT NULL,
  `primary_phone` tinyint(1) NOT NULL COMMENT '1- Primary, 2- Secondary'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_recruitment_applicant_stage_attachment`
--

CREATE TABLE `tbl_recruitment_applicant_stage_attachment` (
  `id` int(11) NOT NULL,
  `applicant_id` int(10) NOT NULL,
  `attachment_title` varchar(30) NOT NULL,
  `attachment` int(50) NOT NULL,
  `stage` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_recruitment_applicant_stage_notes`
--

CREATE TABLE `tbl_recruitment_applicant_stage_notes` (
  `id` int(11) NOT NULL,
  `applicant_id` int(10) NOT NULL,
  `notes` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  `stage` tinyint(1) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1- Active, 0- Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_recruitment_application`
--

CREATE TABLE `tbl_recruitment_application` (
  `id` tinyint(3) NOT NULL,
  `application_category` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1- Active, 0- Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_recruitment_applicant`
--
ALTER TABLE `tbl_recruitment_applicant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_recruitment_applicant_address`
--
ALTER TABLE `tbl_recruitment_applicant_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_recruitment_applicant_doc`
--
ALTER TABLE `tbl_recruitment_applicant_doc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_recruitment_applicant_email`
--
ALTER TABLE `tbl_recruitment_applicant_email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_recruitment_applicant_phone`
--
ALTER TABLE `tbl_recruitment_applicant_phone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_recruitment_applicant_stage_attachment`
--
ALTER TABLE `tbl_recruitment_applicant_stage_attachment`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_recruitment_applicant`
--
ALTER TABLE `tbl_recruitment_applicant`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_recruitment_applicant_address`
--
ALTER TABLE `tbl_recruitment_applicant_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_recruitment_applicant_doc`
--
ALTER TABLE `tbl_recruitment_applicant_doc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_recruitment_applicant_email`
--
ALTER TABLE `tbl_recruitment_applicant_email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_recruitment_applicant_phone`
--
ALTER TABLE `tbl_recruitment_applicant_phone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_recruitment_applicant_stage_attachment`
--
ALTER TABLE `tbl_recruitment_applicant_stage_attachment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;



ALTER TABLE `tbl_recruitment_applicant`
ADD `archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1-archieved';

ALTER TABLE `tbl_recruitment_applicant_phone`
ADD `archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1-archieved';

ALTER TABLE `tbl_recruitment_applicant_email`
ADD `archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1-archieved';

DROP TABLE IF EXISTS `tbl_recruitment_action`;
CREATE TABLE `tbl_recruitment_action` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `action_name` varchar(100) NOT NULL,
  `user` int(10) NOT NULL,
  `action_type` tinyint(1) NOT NULL COMMENT '0=group interview ,1= single',
  `start_datetime` datetime NOT NULL,
  `end_datetime` datetime NOT NULL,
  `training_location` tinyint(3) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1- Active, 0- Inactive',
  `mail_status` tinyint(1) NOT NULL COMMENT '0=No , 1= Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_recruitment_action_applicant`;
CREATE TABLE `tbl_recruitment_action_applicant` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(10) NOT NULL,
  `applicant_message` varchar(200) NOT NULL COMMENT 'reply by applicant end',
  `email_status` tinyint(1) NOT NULL COMMENT '0=No , 1= Yes',
  `status` tinyint(1) NOT NULL COMMENT '0- Pending, 1- Approved,  2=Declined',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_recruitment_action_subtask`;
CREATE TABLE `tbl_recruitment_action_subtask` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `action_id` int(7) NOT NULL,
  `subtask` varchar(50) NOT NULL,
  `subtask_detail` text NOT NULL,
  `notes` varchar(200) NOT NULL,
  `assigned_to` int(10) NOT NULL,
  `due_date` datetime NOT NULL,
  `attachment` varchar(50) NOT NULL,
  `task_completed` tinyint(4) NOT NULL COMMENT '0=Not Completed ,1=Completed ',
  `archive` tinyint(1) NOT NULL COMMENT '1- Delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `tbl_recruitment_action`
ADD `created` datetime NOT NULL;

ALTER TABLE `tbl_recruitment_action_applicant`
ADD `action_id` int(10) NOT NULL AFTER `id`;

ALTER TABLE `tbl_recruitment_action_subtask`
CHANGE `archive` `archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1- Delete' AFTER `task_completed`;
