<?php

trait formCustomValidation {

    public function phone_number_check($sourceData, $fieldData = 'phone') {
        $data = $sourceData;
        $fieldExport = explode(',', $fieldData);

        $field = $fieldExport[0];
        $empty_check_required = isset($fieldExport[1]) && !empty($fieldExport[1]) ? true : false;
        $msg_show = isset($fieldExport[2]) && !empty($fieldExport[2]) && gettype($fieldExport[2] == 'string') ? $fieldExport[2] : 'Please enter valid phone number';
        if (!empty($data)) {
            $data = gettype($data) == 'object' || gettype($data) == 'array' ? (array) $data : [$field => $data];
            if ($empty_check_required && isset($data[$field]) && empty($data[$field])) {
                $this->form_validation->set_message('phone_number_check', 'Phone number can not be empty');
                return false;
            }
            if (isset($data[$field]) && !empty($data[$field]) && !preg_match(PHONE_REGEX_KEY, $data[$field], $match)) {
                $this->form_validation->set_message('phone_number_check', $msg_show);
                return false;
            }
        }
        return true;
    }

    public function suburb_check($sourceData, $fieldData = 'value') {
        $data = $sourceData;
        $fieldExport = explode(',', $fieldData);
        $field = $fieldExport[0];
        $empty_check_required = isset($fieldExport[1]) && !empty($fieldExport[1]) ? true : false;
        $msg_show = isset($fieldExport[2]) && !empty($fieldExport[2]) && gettype($fieldExport[2] == 'string') ? $fieldExport[2] : 'Suburb can not be empty';
        $data = gettype($data) == 'object' || gettype($data) == 'array' ? (array) $data : [$field => $data];

        if ($empty_check_required && (!isset($data[$field]) || empty($data[$field]))) {
            $this->form_validation->set_message('suburb_check', $msg_show);
            return false;
        }
        return true;
    }

    public function check_site_not_alloted($sourceData, $fieldData = 'value') {
        $data = $sourceData;
        $fieldExport = explode(',', $fieldData);
        $field = $fieldExport[0];
        $option_label = isset($fieldExport[1]) && !empty($fieldExport[1]) && gettype($fieldExport[1] == 'string') ? $fieldExport[1] : 'label';
        $empty_check_required = isset($fieldExport[2]) && !empty($fieldExport[2]) ? true : false;
        $msg_show = isset($fieldExport[3]) && !empty($fieldExport[3]) && gettype($fieldExport[3] == 'string') ? $fieldExport[3] : 'Please Select site again it alerady assign to other organization.';
        if (!empty($data)) {

            $data = gettype($data) == 'object' || gettype($data) == 'array' ? (array) $data : [$field => $data];
            $msg = isset($data[$option_label]) ? $data[$option_label] . ', ' : '';
            if ((!isset($data[$field])) || (isset($data[$field]) && empty($data[$field]))) {
                $this->form_validation->set_message('check_site_not_alloted', $msg . $msg_show);
                return false;
            }
            if (!is_numeric($data[$field])) {
                $this->form_validation->set_message('check_site_not_alloted', 'Something went wrong.');
                return false;
            }
            $res = $this->Basic_model->get_row('organisation_site', array('organisationId', 'id'), array('organisationId' => '0', 'archive' => '0', 'id' => $data[$field]));
            if (!$res) {
                $this->form_validation->set_message('check_site_not_alloted', $msg . $msg_show);
                return false;
            }
        } else if ($empty_check_required && (empty($data) || count($data) <= 0)) {
            $this->form_validation->set_message('check_site_not_alloted', 'Please Select atleast one site to attach.');
            return false;
        }
        return true;
    }

    public function multiple_phone_number_check($sourceData, $fieldData = 'sitePh,phone') {
        $data = $sourceData;
        $fieldExport = explode(',', $fieldData);
        $field = $fieldExport[0];
        $fieldInsideIndex = $fieldExport[1];
        $empty_check_required = isset($fieldExport[2]) && !empty($fieldExport[2]) ? true : false;
        $msg_show = isset($fieldExport[3]) && !empty($fieldExport[3]) && gettype($fieldExport[3] == 'string') ? $fieldExport[3] : 'Please enter valid phone number';
        if (!empty($data)) {
            $data = gettype($data) == 'object' || gettype($data) == 'array' ? json_decode(json_encode($data), TRUE) : [];
            if ($empty_check_required && empty($data)) {
                $this->form_validation->set_message('multiple_phone_number_check', 'Phone number can not be empty.');
                return false;
            }
            if ($empty_check_required && isset($data[$field]) && empty($data[$field])) {
                $this->form_validation->set_message('multiple_phone_number_check', 'Phone number can not be empty.');
                return false;
            }
            $sts = false;
            foreach ($data[$field] as $row) {

                if (!isset($row[$fieldInsideIndex])) {
                    $this->form_validation->set_message('multiple_phone_number_check', 'Phone number can not be empty.');
                    $sts = true;
                    break;
                }
                if ($empty_check_required && empty($row[$fieldInsideIndex])) {
                    $this->form_validation->set_message('multiple_phone_number_check', 'Phone number can not be empty');
                    $sts = true;
                    break;
                }


                if (isset($row[$fieldInsideIndex]) && !empty($row[$fieldInsideIndex]) && !preg_match(PHONE_REGEX_KEY, $row[$fieldInsideIndex], $match)) {
                    $this->form_validation->set_message('multiple_phone_number_check', $msg_show);
                    $sts = true;
                    break;
                }
            }
            if ($sts) {
                return false;
            }
        }
        return true;
    }

    public function multiple_email_check($sourceData, $fieldData = 'siteMail,email') {
        $data = $sourceData;
        $fieldExport = explode(',', $fieldData);
        $field = $fieldExport[0];
        $fieldInsideIndex = $fieldExport[1];
        $empty_check_required = isset($fieldExport[2]) && !empty($fieldExport[2]) ? true : false;
        $msg_show = isset($fieldExport[3]) && !empty($fieldExport[3]) && gettype($fieldExport[3] == 'string') ? $fieldExport[3] : 'Please enter valid phone number';
        if (!empty($data)) {
            $data = gettype($data) == 'object' || gettype($data) == 'array' ? json_decode(json_encode($data), TRUE) : [];
            if ($empty_check_required && empty($data)) {
                $this->form_validation->set_message('multiple_email_check', 'Email can not empty');
                return false;
            }
            if ($empty_check_required && isset($data[$field]) && empty($data[$field])) {
                $this->form_validation->set_message('multiple_email_check', 'Email can not empty');
                return false;
            }
            foreach ($data[$field] as $row) {
                if (!isset($row[$fieldInsideIndex])) {
                    $this->form_validation->set_message('multiple_email_check', 'Email can not empty');
                    return false;
                }
                if ($empty_check_required && empty($row[$fieldInsideIndex])) {
                    $this->form_validation->set_message('multiple_email_check', 'Email can not empty');
                    return false;
                }

                if (isset($row[$fieldInsideIndex]) && !empty($row[$fieldInsideIndex]) && !filter_var($row[$fieldInsideIndex], FILTER_VALIDATE_EMAIL)) {
                    $this->form_validation->set_message('multiple_email_check', $msg_show);
                    return false;
                }
            }
        }
        return true;
    }

    public function site_details_check($sourceData) {
        $data = $sourceData;
        if (!empty($data)) {
            $msg = ' can not empty';
            $data = gettype($data) == 'object' || gettype($data) == 'array' ? json_decode(json_encode($data), TRUE) : [];
            if ((isset($data['firstname']) && empty($data['firstname'])) || !isset($data['firstname'])) {
                $msg_show = 'First name';
                $this->form_validation->set_message('site_details_check', $msg_show . $msg);
                return false;
            }

            if ((isset($data['lastname']) && empty($data['lastname'])) || !isset($data['lastname'])) {
                $msg_show = 'Last name';
                $this->form_validation->set_message('site_details_check', $msg_show . $msg);
                return false;
            }

            if ((isset($data['position']) && empty($data['position'])) || !isset($data['position'])) {
                $msg_show = 'Position';
                $this->form_validation->set_message('site_details_check', $msg_show . $msg);
                return false;
            }

            if ((isset($data['department']) && empty($data['department'])) || !isset($data['department'])) {
                $msg_show = 'Department';
                $this->form_validation->set_message('site_details_check', $msg_show . $msg);
                return false;
            }
        } else {
            $msg_show = 'Required field ';
            $this->form_validation->set_message('site_details_check', $msg_show . $msg);
            return false;
        }
        return true;
    }

    /**
     * check site_name is already exists or not in the database table "tbl_organisation_site" 
     * @param	string	$sourceData	the site name.
     * @param	int	$siteIdValue the site id. Can be intager
     * @return	bool
     */
    public function check_unique_site_name($sourceData, $siteIdValue = 0) {
        $sourceData = trim($sourceData);
        if (empty($sourceData)) {
            $msg = 'Title can not empty.';
            $this->form_validation->set_message('check_unique_site_name', $msg);
            return false;
        }
        if (!is_string($sourceData)) {
            $msg = 'Title Only allow alphanumeric characters.';
            $this->form_validation->set_message('check_unique_site_name', $msg);
            return false;
        }

        $selectColumn = ['id', 'organisationId'];
        $siteIdValue = (int) $siteIdValue;
        $whereCondition = ['site_name' => $sourceData, 'id !=' => $siteIdValue];
        $table = 'organisation_site';
        $res = $this->Basic_model->get_row($table, $selectColumn, $whereCondition);
        if ($res) {
            $msg = 'Site title already exists.';
            $this->form_validation->set_message('check_unique_site_name', $msg);
            return false;
        }
        return true;
    }

    public function kin_details_check($sourceData, $msgStart = 'Next of kin ') {
        $data = $sourceData;
        if (!empty($data)) {
            $msg = ' can not empty.';
            $data = gettype($data) == 'object' || gettype($data) == 'array' ? json_decode(json_encode($data), TRUE) : [];
            if ((isset($data['firstname']) && empty($data['firstname'])) || !isset($data['firstname'])) {
                $msg_show = !empty($msgStart) ? '' : 'name';
                $this->form_validation->set_message('kin_details_check', ucfirst($msgStart . $msg_show . $msg));
                return false;
            }

            /* if((isset($data['lastname']) && empty($data['lastname']))) {
              $msg_show = 'Last name';
              $this->form_validation->set_message('kin_details_check', ucfirst($msgStart.$msg_show.$msg));
              return false;
              } */

            if ((isset($data['relation']) && empty($data['relation'])) || !isset($data['relation'])) {
                $msg_show = 'relation';
                $this->form_validation->set_message('kin_details_check', ucfirst($msgStart . $msg_show . $msg));
                return false;
            }

            if ((isset($data['email']) && empty($data['email'])) || !isset($data['email'])) {
                $msg_show = 'email';
                $this->form_validation->set_message('kin_details_check', ucfirst($msgStart . $msg_show . $msg));
                return false;
            } else if (isset($data['email']) && !empty($data['email']) && !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $msg_show = 'Email is not valid.';
                $this->form_validation->set_message('kin_details_check', ucfirst($msgStart . $msg_show));
                return false;
            }

            if ((isset($data['phone']) && empty($data['phone'])) || !isset($data['phone'])) {
                $msg_show = 'phone';
                $this->form_validation->set_message('kin_details_check', ucfirst($msgStart . $msg_show . $msg));
                return false;
            }
        } else {
            $msg_show = 'Required field ';
            $this->form_validation->set_message('kin_details_check', ucfirst($msg_show . $msg));
            return false;
        }
        return true;
    }

    public function postal_code_check($sourceData, $fieldData = 'postal') {
        $data = $sourceData;
        $fieldExport = explode(',', $fieldData);
        $field = $fieldExport[0];
        $msgShow = isset($fieldExport[1]) && !empty($fieldExport[1]) && gettype($fieldExport[1] == 'string') ? $fieldExport[1] : 'Please enter valid 4 digit postcode number.';
        $data = gettype($data) == 'object' || gettype($data) == 'array' ? (array) $data : [$field => $data];

        if (isset($data[$field]) && !empty($data[$field])) {
            if (!preg_match(POSTCODE_AU_REGEX_KEY, $data[$field], $match)) {
                $this->form_validation->set_message('postal_code_check', ucfirst($msgShow));
                return false;
            }
        } elseif (!isset($data[$field]) || empty($data[$field])) {
            $msg_show = 'Postcode can not be empty.';
            $this->form_validation->set_message('postal_code_check', ucfirst($msg_show));
            return false;
        } else {
            $msg_show = 'Postcode can not be empty.';
            $this->form_validation->set_message('postal_code_check', ucfirst($msg_show));
            return false;
        }
        return true;
    }

    public function date_check($sourceData, $fieldData = 'dob') {
        $data = $sourceData;
        $allowDateCheck = ['current','before','after'];
        $fieldExport = explode(',', $fieldData);
        $field = $fieldExport[0];
        $msgShow = isset($fieldExport[2]) && !empty($fieldExport[2]) && gettype($fieldExport[2] == 'string') ? $fieldExport[2] : 'Date is not grater than current date.';
        $type = isset($fieldExport[1]) && !empty($fieldExport[1]) && gettype($fieldExport[1] == 'string') && in_array(strtolower($fieldExport[1]),$allowDateCheck) ? trim(strtolower($fieldExport[1])) : 'current';
        $data = gettype($data) == 'object' || gettype($data) == 'array' ? (array) $data : [$field => $data];
        if (isset($data[$field]) && !empty($data[$field])) {
            
           if($type=='current' && date('Y-m-d') < date('Y-m-d',strtotime($data[$field]))){
            $this->form_validation->set_message('date_check', ucfirst($msgShow));
            return false;
           }elseif($type=='before' && date('Y-m-d') <= date('Y-m-d',strtotime($data[$field]))){
            $this->form_validation->set_message('date_check', ucfirst($msgShow));
            return false;

           }elseif($type=='after' && date('Y-m-d') >= date('Y-m-d',strtotime($data[$field]))){
            $this->form_validation->set_message('date_check', ucfirst($msgShow));
            return false;
           }
        } elseif (!isset($data[$field]) || empty($data[$field])) {
            $msg_show = 'date can not be empty.';
            $this->form_validation->set_message('date_check', ucfirst($msg_show));
            return false;
        } else {
            $msg_show = 'date can not be empty.';
            $this->form_validation->set_message('date_check', ucfirst($msg_show));
            return false;
        }
        return true;
    }

}
