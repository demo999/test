<?php
namespace DisableUserClass; // if namespacce is not made in Classes(Member.php,MemberAddress.php [files]) folder, then it will give error of Cannot redeclare class
/*
 * Filename: Member.php
 * Desc: The member file defines a module which checks the details and updates of members, upcoming shifts, create new availability, cases(FMS), create cases.
 * @author YDT <yourdevelopmentteam.com.au>
*/

if (!defined("BASEPATH")) exit("No direct script access allowed");

/*
 * Class: Member
 * Desc: The Member Class is a class which holds infomation about members like memberid, firstname, lastname etc.
 * The class includes variables and some methods. The methods are used to get and store information of members.
 * The visibility mode of this variables are private and the methods are made public.
 * Created: 01-08-2018
*/

class DisableUser
{
    public $CI;
    function __construct()
    {
        $this->CI = & get_instance();
        //$this->CI->load->model('Org_model');
        $this->CI->load->model('Basic_model');
    }

    private $id;
    private $crm_staff_id;
    private $disable_account;
    private $account_allocated;
    private $account_allocated_to;
    private $relevant_note;


    function setId($id) { $this->id = $id; }
    function getId() { return $this->id; }

    function setCrmStaffId($crm_staff_id) { $this->crm_staff_id = $crm_staff_id; }
    function getCrmStaffId() { return $this->crm_staff_id; }


    function setDisableAccount($disable_account) { $this->disable_account = $disable_account; }
    function getDisableAccount() { return $this->disable_account; }

    function setAccountAllocated($account_allocated) { $this->account_allocated = $account_allocated; }
    function getAccountAllocated() { return $this->account_allocated; }

    function setAccountAllocatedTo($account_allocated_to) { $this->account_allocated_to = $account_allocated_to; }
    function getAccountAllocatedTo() { return $this->account_allocated_to; }

    function setRelevantNote($relevant_note) { $this->relevant_note = $relevant_note; }
    function getRelevantNote() { return $this->relevant_note; }



    public function get_organisation_profile()
    {
        return $this->CI->Org_model->get_organisation_profile($this);
    }

    public function get_organisation_about()
    {
        return $this->CI->Org_model->get_organisation_about($this);
    }

    public function get_sub_org()
    {
        return $this->CI->Org_model->get_sub_org($this);
    }

    function check_exist_disable_recruiter()
    {
        $where = array('crm_staff_id'=>$this->getCrmStaffId());
        $result = $this->CI->Basic_model->get_record_where('crm_staff_disable', 'id,crm_staff_id', $where);
        if($result){
            return $result[0]->id;
        } else {
            return false;
        }
    }

    function add_disable_recruiter()
    {
        $bool = false;
        $res="";
        $data = array('crm_staff_id'=>$this->getCrmStaffId(),
            'disable_account'=>$this->getDisableAccount(),
            'account_allocated'=>$this->getAccountAllocated(),
            'relevant_note'=>$this->getRelevantNote(),
        );
        // $data2  = array('account_status'=>0);
        // $c = $this->CI->Basic_model->update_records('crm_staff', $data2, array('id'=>$this->getCrmStaffId()));
        $tbl_1 = TBL_PREFIX . 'admin';
        $tbl_2 = TBL_PREFIX . 'crm_participant';
        $tbl_3 = TBL_PREFIX . 'crm_staff';

        $query = $this->CI->db->select(array($tbl_2.'.id as participant_id'));
        $this->CI->db->from($tbl_1);
        $this->CI->db->join('tbl_crm_participant', 'tbl_crm_participant.assigned_to = tbl_admin.id', 'inner');
        $this->CI->db->where($tbl_1.'.id=',$this->getCrmStaffId());
        $participant_data = $this->CI->db->get();
        foreach($participant_data->result_array() as $value){
              if($this->getAccountAllocated() == '1'){
                $assigned_counts =  $this->assign_participant($this->getCrmStaffId());
                  $min = min($assigned_counts);
                  $key = array_search($min,$assigned_counts);
                  $where = array($tbl_2.".id"=>$value['participant_id']);
                  $data = array($tbl_2.".assigned_to"=>$key);
                  $res = $this->CI->Basic_model->update_records('crm_participant', $data, $where);
                }
                else{
                  $where = array($tbl_2.".id"=>$value['participant_id']);
                  $data = array($tbl_2.".assigned_to"=>$this->getAccountAllocatedTo());
                  $res = $this->CI->Basic_model->update_records('crm_participant', $data, $where);
                }
        }
        $add_disable = $this->CI->Basic_model->insert_records('crm_staff_disable', $data);
        $update_user = $this->CI->Basic_model->update_records('admin', array('status'=>0),array('id'=>$this->getCrmStaffId()));
        return ($res)?true:false;

    }

    function assign_participant($staffId){
      $tbl_1 = TBL_PREFIX . 'admin';
      $tbl_2 = TBL_PREFIX . 'crm_participant';
      $tbl_3 = TBL_PREFIX . 'crm_staff';
      $dt_query = $this->CI->db->select(array($tbl_1.'.id as ocs_id',$tbl_1.'.status',"CONCAT(tbl_admin.firstname,' ',tbl_admin.lastname) AS name","count(tbl_admin.id) as count"));

      $this->CI->db->from($tbl_3);
    $this->CI->db->join('tbl_admin', 'tbl_crm_staff.admin_id = tbl_admin.id', 'left');
      $this->CI->db->where($tbl_1.'.archive=',"0");
      $this->CI->db->where($tbl_1.'.id<>',$staffId);
      $this->CI->db->where($tbl_1.'.status=',"1");
      $this->CI->db->group_by($tbl_1.'.id');
      $data = $this->CI->db->get();
      $assigned_counts= array();
      foreach($data->result_array() as $value){
        $assigned_counts = array($value['ocs_id'] => $value['count']);

      }
      return $assigned_counts;
    }
    function update_disable_recruiter()
    {
        $data = array('crm_staff_id'=>$this->getCrmStaffId(),
        'disable_account'=>$this->getDisableAccount(),
        'account_allocated'=>$this->getAccountAllocated(),
        'account_allocated_to'=>$this->getAccountAllocatedTo(),
        'relevant_note'=>$this->getRelevantNote(),
        );
        $c = $this->CI->Basic_model->update_records('crm_staff_disable', $data,array('id'=>$this->getId()));
        return $c;
    }

}
