<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_StageOne extends CI_Migration {
    public function up()
    {
        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "activity` (
                `id` mediumint(5) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `companyId` int(8) NOT NULL,
                `name` varchar(50) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        // Batch insert into activity from activity.json
        $this->insert_from_json(TBL_PREFIX . 'activity');

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "admin` (
                `id` int(5) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `companyId` smallint(3) NOT NULL DEFAULT '0',
                `username` varchar(20) NOT NULL,
                `password` varchar(64) NOT NULL,
                `pin` text NOT NULL,
                `firstname` varchar(32) NOT NULL,
                `lastname` varchar(32) NOT NULL,
                `profile` varchar(100) NOT NULL,
                `position` varchar(50) NOT NULL,
                `department` varchar(50) NOT NULL,
                `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1- Active, 0- Inactive',
                `background` varchar(64) NOT NULL,
                `gender` tinyint(1) NOT NULL COMMENT '1- Male, 2- Female',
                `token` varchar(255) NOT NULL,
                `archive` tinyint(4) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "admin_email` (
                `adminId` int(5) NOT NULL,
                `email` varchar(64) NOT NULL,
                `primary_email` tinyint(1) NOT NULL COMMENT '1- Primary, 2- Secondary'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "admin_login` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `adminId` int(11) NOT NULL,
                `token` text NOT NULL,
                `ip_address` varchar(100) NOT NULL,
                `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                `pin` text NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "admin_login_history` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `adminId` int(11) NOT NULL,
                `ip_address` varchar(100) NOT NULL,
                `details` text NOT NULL,
                `login_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                `last_access` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                `status` int(11) NOT NULL COMMENT '1- active / 2 - deactive'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "admin_phone` (
                `adminId` int(5) NOT NULL,
                `phone` varchar(20) NOT NULL,
                `primary_phone` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1- Primary, 2- Secondary'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "admin_role` (
                `id` int(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `roleId` smallint(4) NOT NULL,
                `adminId` int(5) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "approval` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `userId` int(11) NOT NULL,
                `user_type` int(11) NOT NULL COMMENT '1- Member, 2- Participant',
                `approval_area` varchar(200) NOT NULL COMMENT 'like UpdateProfile,Care Requirement',
                `approval_content` text NOT NULL COMMENT 'JSON',
                `created` datetime NOT NULL,
                `approval_date` datetime NOT NULL,
                `approved_by` int(11) NOT NULL,
                `status` int(11) NOT NULL COMMENT '0 Not Approved(Request by Member/Participant) ,1 Approved ,2 Cancel',
                `pin` int(11) NOT NULL COMMENT '0- not / 1 - yes',
                `updated` datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "app_feedback` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `memberid` int(11) NOT NULL,
                `rating` smallint(6) NOT NULL,
                `feedback` varchar(1000) NOT NULL,
                `created` datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "call_log` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `receiver_number` varchar(25) NOT NULL,
                `caller_number` varchar(25) NOT NULL,
                `audio_url` varchar(200) NOT NULL,
                `duration` varchar(20) NOT NULL,
                `created` datetime NOT NULL,
                `api_data` text NOT NULL
            ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "company` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `name` varchar(128) NOT NULL,
                `address` varchar(128) NOT NULL,
                `city` varchar(64) NOT NULL,
                `state` varchar(64) NOT NULL,
                `email` varchar(64) NOT NULL,
                `phone` varchar(20) NOT NULL,
                `contact_person` varchar(64) NOT NULL,
                `status` tinyint(1) DEFAULT '1'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "department` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `name` varchar(100) NOT NULL,
                `archive` tinyint(4) NOT NULL COMMENT '0- Not / 1 - Delete'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        // Batch insert into department from department.json
        $this->insert_from_json(TBL_PREFIX . 'department');

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "external_message` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `companyId` smallint(5) NOT NULL,
                `title` varchar(228) NOT NULL,
                `is_block` int(11) NOT NULL COMMENT '0- not / 1 - yes',
                `created` datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "external_message_action` (
                `messageId` int(11) NOT NULL,
                `user_type` int(11) NOT NULL COMMENT '1 - admin / 2 - participant / 3 - member / 4 - organisation',
                `userId` int(11) NOT NULL,
                `is_fav` int(11) NOT NULL COMMENT '0- not / 1 - yes',
                `is_flage` int(11) NOT NULL COMMENT '0- not / 1 - yes',
                `archive` int(11) NOT NULL COMMENT '0- not / 1 - yes'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "external_message_attachment` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `messageContentId` int(11) NOT NULL,
                `filename` varchar(200) NOT NULL,
                `created` datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "external_message_content` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `messageId` int(10) NOT NULL,
                `sender_type` tinyint(1) NOT NULL COMMENT '1 - admin / 2 - participant / 3 - member / 4 - organisation',
                `userId` int(11) NOT NULL,
                `created` datetime NOT NULL,
                `content` text NOT NULL,
                `is_priority` int(11) NOT NULL COMMENT '0 - No / 1 - Yes',
                `is_reply` int(11) NOT NULL COMMENT '0- not / 1 - yes',
                `is_draft` int(11) NOT NULL COMMENT '0- not / 1 - yes'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "external_message_recipient` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `messageContentId` int(8) NOT NULL,
                `messageId` int(8) NOT NULL,
                `recipinent_type` int(11) NOT NULL COMMENT '1 - admin / 2 - participant / 3 - member / 4 - organisation',
                `recipinentId` int(11) NOT NULL,
                `is_read` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1- Yes, 0- No'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "fms_address_book` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `companyId` int(8) NOT NULL,
                `caseId` int(11) NOT NULL,
                `type` tinyint(4) NOT NULL COMMENT '1- Member, 2- Participant',
                `ocs_id` int(8) NOT NULL COMMENT 'Member or Participant id',
                `created` datetime NOT NULL
            ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "fms_case` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `companyId` smallint(5) NOT NULL,
                `event_date` date NOT NULL,
                `shiftId` int(8) NOT NULL DEFAULT '0',
                `initiated_by` int(8) NOT NULL,
                `initiated_type` tinyint(1) NOT NULL COMMENT '1- Member, 2- Participant, 3- ORG, 4- House, 5- member of public, 6- ONCALL (General), 7- ONCALL User/Admin',
                `created` varchar(20) NOT NULL,
                `escalate_to_incident` tinyint(1) NOT NULL DEFAULT '0',
                `Initiator_first_name` varchar(150) DEFAULT NULL COMMENT 'in case of initiated_by public',
                `Initiator_last_name` varchar(150) DEFAULT NULL COMMENT 'in case of initiated_by public',
                `Initiator_email` varchar(150) DEFAULT NULL COMMENT 'in case of initiated_by public',
                `Initiator_phone` varchar(150) DEFAULT NULL COMMENT 'in case of initiated_by public',
                `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0- Ongoing, 1- Complete',
                `fms_type` enum('1','0') NOT NULL DEFAULT '0' COMMENT '0-case, 1- incident'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "fms_case_against_detail` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `caseId` int(8) NOT NULL,
                `against_category` int(11) NOT NULL COMMENT '1- member of public,2- Member, 3- Participant, 4- ONCALL (General), 5- ONCALL User/Admin',
                `against_by` int(11) NOT NULL COMMENT 'Member or Participant id',
                `against_first_name` varchar(150) DEFAULT NULL COMMENT 'when against_category = 1',
                `against_last_name` varchar(150) DEFAULT NULL COMMENT 'when against_category = 1',
                `against_email` varchar(150) DEFAULT NULL COMMENT 'when against_category = 1',
                `against_phone` varchar(150) DEFAULT NULL COMMENT 'when against_category = 1',
                `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
            ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "fms_case_all_category` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `name` varchar(150) NOT NULL,
                `archive` tinyint(4) NOT NULL,
                `created` datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Predefined requirement of shift organisation from OCS';
        ");

        // Batch insert into fms_case_all_category from fms_case_all_category.json
        $this->insert_from_json(TBL_PREFIX . 'fms_case_all_category');

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "fms_case_category` (
                `caseId` int(8) NOT NULL,
                `categoryId` tinyint(2) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "fms_case_docs` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `caseId` int(8) NOT NULL,
                `title` varchar(64) NOT NULL,
                `filename` varchar(255) NOT NULL,
                `created` datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "fms_case_link` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `caseId` int(8) NOT NULL,
                `link_case` int(8) NOT NULL,
                `archive` tinyint(1) NOT NULL COMMENT '1- Delete',
                `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "fms_case_location` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `caseId` int(10) NOT NULL,
                `address` varchar(128) NOT NULL,
                `suburb` varchar(100) NOT NULL COMMENT 'city',
                `state` tinyint(2) NOT NULL,
                `postal` varchar(10) NOT NULL,
                `lat` varchar(100) DEFAULT NULL,
                `long` varchar(100) DEFAULT NULL,
                `categoryId` int(10) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "fms_case_log` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `caseId` int(8) NOT NULL,
                `title` varchar(128) NOT NULL,
                `created_by` int(8) DEFAULT NULL,
                `created_type` tinyint(1) NOT NULL COMMENT '1- Member, 2- Admin ',
                `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "fms_case_notes` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `caseId` int(8) NOT NULL,
                `title` varchar(228) DEFAULT NULL,
                `description` text NOT NULL,
                `created_by` int(8) NOT NULL,
                `created_type` int(11) NOT NULL COMMENT '1- Member, 2- Participant, 3- ORG, 4- House, 5- member of public',
                `created` datetime NOT NULL,
                `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "fms_case_party` (
                `caseId` int(8) NOT NULL,
                `againstId` int(8) NOT NULL,
                `against_type` tinyint(1) NOT NULL COMMENT '1- Member, 2- Participant, 3- ORG, 4- House'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "fms_case_reason` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `caseId` int(8) NOT NULL,
                `title` varchar(228) NOT NULL,
                `description` text NOT NULL,
                `created_by` int(8) NOT NULL,
                `created_type` tinyint(1) NOT NULL COMMENT '1- Member, 2- Admin',
                `created` datetime NOT NULL,
                `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "goal_rating` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `rating` int(11) NOT NULL,
                `name` varchar(200) NOT NULL,
                `archive` tinyint(4) NOT NULL
            ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
        ");

        // Batch insert into goal_rating from goal_rating.json
        $this->insert_from_json(TBL_PREFIX . 'goal_rating');

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "group_message` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `teamId` int(11) NOT NULL,
                `type` int(11) NOT NULL COMMENT '1 - Team / 2 - Department',
                `message_type` int(11) NOT NULL COMMENT '1 - text/ 2- file',
                `message` longtext NOT NULL,
                `senderId` int(11) NOT NULL,
                `created` datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "group_message_action` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `messageId` int(11) NOT NULL,
                `senderId` int(11) NOT NULL,
                `status` int(11) NOT NULL COMMENT '0 - unread/ 2 - read / 3 - archive'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "house` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `companyId` smallint(5) NOT NULL,
                `name` varchar(64) NOT NULL,
                `address` varchar(128) NOT NULL,
                `postal` varchar(10) NOT NULL,
                `state` tinyint(2) NOT NULL,
                `created` varchar(20) NOT NULL,
                `status` tinyint(1) NOT NULL COMMENT '1- Active, 0- Inactive'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "house_all_contact` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `houseId` int(6) NOT NULL,
                `name` varchar(64) NOT NULL,
                `phone` varchar(20) NOT NULL,
                `email` varchar(64) NOT NULL,
                `position` varchar(20) NOT NULL,
                `type` tinyint(1) NOT NULL COMMENT '1- Support Coordinator, 2- Member, 3- Key Contact',
                `archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1- Yes, 0- No'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "house_docs` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `houseId` int(6) NOT NULL,
                `filename` varchar(64) NOT NULL,
                `title` varchar(64) NOT NULL,
                `expiry` date NOT NULL,
                `created` varchar(20) NOT NULL,
                `archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1- Yes, 0- No'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "house_email` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `houseId` int(6) NOT NULL,
                `email` varchar(64) NOT NULL,
                `primary_email` tinyint(1) NOT NULL COMMENT '1- Primary, 2- Secondary'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "house_phone` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `houseId` int(6) NOT NULL,
                `phone` varchar(20) NOT NULL,
                `primary_phone` tinyint(1) NOT NULL COMMENT '1- Primary, 2- Secondary'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "house_requirements` (
                `houseId` int(6) NOT NULL,
                `requirementId` tinyint(2) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "house_resident` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `houseId` int(6) NOT NULL,
                `name` varchar(64) NOT NULL,
                `phone` varchar(20) NOT NULL,
                `email` varchar(64) NOT NULL,
                `dob` varchar(15) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "internal_message` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `companyId` smallint(5) NOT NULL,
                `title` varchar(200) NOT NULL,
                `is_block` int(11) NOT NULL COMMENT '0- not / 1 - yes',
                `created` varchar(20) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "internal_message_action` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `messageId` int(11) NOT NULL,
                `userId` int(11) NOT NULL COMMENT '0- not / 1 - yes',
                `is_fav` int(11) NOT NULL COMMENT '0- not / 1 - yes',
                `archive` int(11) NOT NULL COMMENT '0- not / 1 - yes',
                `is_flage` int(11) NOT NULL COMMENT '0- not / 1 - yes'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "internal_message_attachment` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `messageContentId` int(11) NOT NULL,
                `filename` varchar(200) NOT NULL,
                `created` datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "internal_message_content` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `messageId` int(10) NOT NULL,
                `senderId` int(8) NOT NULL,
                `content` text NOT NULL,
                `is_priority` int(11) NOT NULL COMMENT '0- not / 1 - yes',
                `is_draft` int(11) NOT NULL COMMENT '0- not / 1 - yes',
                `is_reply` int(11) NOT NULL COMMENT '0- not / 1 - yes',
                `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "internal_message_recipient` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `messageContentId` int(11) NOT NULL,
                `messageId` int(10) NOT NULL,
                `recipientId` int(8) NOT NULL,
                `cc` int(11) NOT NULL COMMENT '0- not / 1 - yes',
                `is_read` int(11) NOT NULL COMMENT '0- not / 1 - yes'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "internal_message_team` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `adminId` int(5) NOT NULL,
                `team_name` varchar(20) NOT NULL,
                `team_color` varchar(50) NOT NULL,
                `created` varchar(20) NOT NULL,
                `archive` int(11) NOT NULL COMMENT '0- not/ 1 - archive'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "internal_message_team_admin` (
                `teamId` mediumint(5) NOT NULL,
                `adminId` int(8) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "logs` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `companyId` smallint(3) DEFAULT NULL,
                `userId` int(6) DEFAULT NULL,
                `module` int(11) DEFAULT NULL COMMENT '1- Admin/ 2- Participant / 3 - Member/ 4 - Schedule / 5 - FSM / 6 - House / 7 - Organization / 8 - Imail',
                `sub_module` int(11) DEFAULT NULL COMMENT 'if schedule (1 - Shift / 2 - Roster) , if Imail (1 - Externam mail/ 2 - Internal Mail) ',
                `title` text,
                `description` text,
                `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `created_by` int(8) DEFAULT '0',
                `created_type` int(11) NOT NULL COMMENT '1 - admin / 2 - participant'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "member` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `companyId` smallint(3) NOT NULL,
                `firstname` varchar(32) NOT NULL,
                `lastname` varchar(32) NOT NULL,
                `middlename` varchar(32) NOT NULL,
                `preferredname` varchar(64) NOT NULL,
                `pin` varchar(64) NOT NULL,
                `profile_image` varchar(255) NOT NULL,
                `user_type` tinyint(1) NOT NULL COMMENT '0- Support Coordinator, 1- Member',
                `deviceId` varchar(64) NOT NULL,
                `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1- Active, 0- Inactive',
                `prefer_contact` varchar(6) NOT NULL,
                `dob` date NOT NULL,
                `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1- Male, 2- Female',
                `push_notification_enable` tinyint(1) NOT NULL COMMENT '1- Enable, 0- Disable',
                `enable_app_access` tinyint(1) NOT NULL COMMENT '1- Enable, 0- Disable',
                `dwes_confirm` tinyint(1) NOT NULL COMMENT '1- Confirm, 0- not Confirm',
                `archive` tinyint(1) NOT NULL COMMENT '1- Delete',
                `created` datetime NOT NULL,
                `loginattempt` tinyint(4) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "member_activity` (
                `activityId` smallint(5) NOT NULL,
                `memberId` int(8) NOT NULL,
                `type` tinyint(1) NOT NULL COMMENT '1- Favourite, 2- Least Favourite'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "member_address` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `memberId` int(8) NOT NULL DEFAULT '0',
                `street` varchar(128) NOT NULL,
                `city` varchar(64) NOT NULL,
                `postal` varchar(10) NOT NULL,
                `state` tinyint(1) NOT NULL,
                `primary_address` tinyint(1) NOT NULL COMMENT '1- Primary, 2- Secondary',
                `lat` varchar(200) NOT NULL,
                `long` varchar(200) NOT NULL,
                `archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1- Delete'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "member_availability` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `memberId` int(8) NOT NULL,
                `title` varchar(50) NOT NULL,
                `is_default` tinyint(1) NOT NULL COMMENT '1- Yes, 2- No',
                `status` tinyint(1) NOT NULL,
                `start_date` timestamp NULL DEFAULT NULL,
                `end_date` timestamp NULL DEFAULT NULL,
                `first_week` text NOT NULL,
                `second_week` text NOT NULL,
                `flexible_availability` tinyint(1) NOT NULL COMMENT '1- Yes, 0- No',
                `flexible_km` smallint(4) NOT NULL,
                `travel_km` smallint(4) NOT NULL,
                `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
                `archive` tinyint(1) NOT NULL COMMENT '1- Delete'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "member_availability_list` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `member_availability_id` int(10) NOT NULL,
                `memberId` int(8) NOT NULL,
                `is_default` tinyint(4) NOT NULL COMMENT '1- Yes, 2- No',
                `status` tinyint(1) NOT NULL COMMENT 'is_open or filled',
                `availability_type` varchar(255) NOT NULL,
                `availability_date` date DEFAULT NULL,
                `availability_week` varchar(100) DEFAULT NULL,
                `availability_start_time` time NOT NULL,
                `flexible_availability` tinyint(1) NOT NULL COMMENT '1- Yes, 0- No',
                `flexible_km` smallint(4) NOT NULL,
                `travel_km` smallint(4) NOT NULL,
                `created` datetime NOT NULL,
                `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
                `archive` tinyint(1) NOT NULL COMMENT '1- Delete',
                `run_mode` tinyint(1) NOT NULL COMMENT '1- Cron, 0- OCS'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "member_contact` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `memberId` int(8) NOT NULL,
                `created` varchar(20) NOT NULL,
                `type` char(1) NOT NULL COMMENT 'P- Phone, M- InMessage',
                `record_file` varchar(64) NOT NULL,
                `messageId` int(10) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "member_email` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `memberId` int(8) NOT NULL,
                `email` varchar(64) NOT NULL,
                `primary_email` tinyint(1) NOT NULL COMMENT '1- Primary, 2- Secondary',
                `archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1- Delete'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "member_kin` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `memberId` int(8) NOT NULL,
                `firstname` varchar(32) NOT NULL,
                `lastname` varchar(32) NOT NULL,
                `relation` varchar(24) NOT NULL,
                `phone` varchar(20) NOT NULL,
                `email` varchar(64) NOT NULL,
                `primary_kin` tinyint(1) NOT NULL COMMENT '1- Primary, 2- Secondary',
                `archive` tinyint(1) NOT NULL COMMENT '1- Delete'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "member_login` (
                `memberId` int(8) NOT NULL,
                `token` text NOT NULL,
                `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "member_phone` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `phone` varchar(20) NOT NULL,
                `primary_phone` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1- Primary, 2- Secondary',
                `memberId` int(8) NOT NULL,
                `archive` tinyint(1) NOT NULL COMMENT '1- Delete'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "member_place` (
                `placeId` mediumint(5) NOT NULL,
                `memberId` int(8) NOT NULL,
                `type` tinyint(1) NOT NULL COMMENT '1- Favourite, 2- Least Favourite'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "member_position_award` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `companyId` int(8) NOT NULL,
                `memberId` int(8) NOT NULL,
                `position` tinyint(4) NOT NULL,
                `award` tinyint(4) NOT NULL,
                `level` tinyint(4) NOT NULL,
                `created` datetime NOT NULL,
                `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
                `archive` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1- Delete'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "member_qualification` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `memberId` int(8) NOT NULL,
                `expiry` date NOT NULL,
                `title` varchar(64) NOT NULL,
                `type` int(8) NOT NULL COMMENT '1- Police Check, 2- WWCC, 3- First Aid, 4- Fire Safety,5- CPR, 6- Anaphylaxis',
                `created` datetime NOT NULL,
                `can_delete` tinyint(1) NOT NULL COMMENT '1- Yes, 0- No',
                `filename` varchar(64) NOT NULL,
                `archive` tinyint(4) NOT NULL COMMENT '1- Delete'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "member_special_agreement` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `memberId` int(8) NOT NULL,
                `title` varchar(200) NOT NULL,
                `expiry` date NOT NULL,
                `created` datetime NOT NULL,
                `filename` varchar(100) NOT NULL,
                `archive` tinyint(4) NOT NULL COMMENT '1- Delete'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "member_work_area` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `companyId` int(8) NOT NULL,
                `memberId` int(8) NOT NULL,
                `work_area` tinyint(4) NOT NULL COMMENT '1 = Client & NDIS Services, 2 = Out Of Home Care 3 = Disability Accommodation 4 = Casual Staff Service -Disability 5 = Casual Staff Service -Welfare',
                `work_status` tinyint(4) NOT NULL COMMENT '1 = Yes, 2 = Yes, Not Preffered, 3 = No, Inexperienced, 4 = No, ONCALL Request, 5 = Registered',
                `created` datetime NOT NULL,
                `archive` tinyint(4) NOT NULL COMMENT '1- Delete'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "notification` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `userId` int(11) NOT NULL,
                `user_type` int(11) NOT NULL COMMENT '1- Member, 2- Participant',
                `title` varchar(200) NOT NULL COMMENT 'like UpdateProfile,Care Requirement',
                `shortdescription` text NOT NULL,
                `created` datetime NOT NULL,
                `status` int(11) NOT NULL COMMENT '0 Not read / 1 Read',
                `sender_type` int(11) NOT NULL DEFAULT '1' COMMENT '1- user / 2- admin'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "organisation` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `companyId` smallint(5) NOT NULL,
                `name` varchar(128) NOT NULL,
                `abn` varchar(20) NOT NULL,
                `logo_file` varchar(64) NOT NULL,
                `parent_org` smallint(5) NOT NULL DEFAULT '0',
                `website` varchar(228) NOT NULL,
                `payroll_tax` enum('1','0') NOT NULL,
                `gst` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1- Yes, 0- No',
                `enable_portal_access` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1- Enable, 0- Disable',
                `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1- Yes, 0- No',
                `booking_status` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1- Yes, 0- No',
                `booking_date` date NOT NULL,
                `archive` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1- Yes, 0- No'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "organisation_address` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `organisationId` int(8) NOT NULL DEFAULT '0',
                `street` varchar(128) NOT NULL,
                `city` varchar(64) NOT NULL,
                `postal` varchar(10) NOT NULL,
                `state` tinyint(1) NOT NULL,
                `category` tinyint(1) NOT NULL,
                `primary_address` tinyint(1) NOT NULL COMMENT '1- Primary, 2- Secondary'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "organisation_all_contact` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `organisationId` int(6) NOT NULL,
                `name` varchar(64) NOT NULL,
                `lastname` varchar(64) NOT NULL,
                `position` varchar(20) NOT NULL,
                `department` varchar(32) NOT NULL,
                `type` tinyint(1) NOT NULL COMMENT '1- Support Coordinator, 2- Member, 3- Key Contact, 4-Billing',
                `archive` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1- Yes, 0- No'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "organisation_all_contact_email` (
                `contactId` int(5) NOT NULL,
                `email` varchar(64) NOT NULL,
                `primary_email` enum('1','2') NOT NULL COMMENT '1- Primary, 2- Secondary',
                `archive` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1- Delete'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "organisation_all_contact_phone` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `contactId` int(5) NOT NULL,
                `phone` varchar(20) NOT NULL,
                `primary_phone` enum('1','2') NOT NULL COMMENT '1- Primary, 2- Secondary',
                `archive` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1- Delete'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "organisation_docs` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `organisationId` int(6) NOT NULL,
                `filename` varchar(64) NOT NULL,
                `title` varchar(64) NOT NULL,
                `expiry` date NOT NULL,
                `created` datetime NOT NULL,
                `archive` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1- delete'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "organisation_email` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `organisationId` int(5) NOT NULL,
                `email` varchar(64) NOT NULL,
                `primary_email` enum('1','2') NOT NULL COMMENT '1- Primary, 2- Secondary',
                `archive` enum('1','0') NOT NULL DEFAULT '0'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "organisation_phone` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `organisationId` int(5) NOT NULL,
                `phone` varchar(20) NOT NULL,
                `primary_phone` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1- Primary, 2- Secondary',
                `archive` enum('1','0') NOT NULL DEFAULT '0'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "organisation_requirement` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `name` varchar(100) NOT NULL,
                `archive` enum('1','0') NOT NULL COMMENT '1- delete',
                `created` datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Predefined requirement of shift from OCS';
        ");

        // Batch insert into organisation_requirement from organisation_requirement.json
        $this->insert_from_json(TBL_PREFIX . 'organisation_requirement');

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "organisation_requirements` (
                `organisationId` int(6) NOT NULL,
                `requirementId` tinyint(2) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "organisation_site` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `organisationId` int(8) NOT NULL DEFAULT '0',
                `site_name` varchar(150) NOT NULL,
                `street` varchar(128) NOT NULL,
                `city` varchar(64) NOT NULL,
                `postal` varchar(10) NOT NULL,
                `state` tinyint(1) NOT NULL,
                `archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1- Yes, 0- No',
                `status` enum('1','0') NOT NULL COMMENT '1- Active, 0-No',
                `enable_portal_access` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1- Yes, 0- No',
                `logo_file` varbinary(200) NOT NULL,
                `created` datetime NOT NULL,
                `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "organisation_site_email` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `siteId` int(5) NOT NULL,
                `email` varchar(64) NOT NULL,
                `primary_email` enum('1','2') NOT NULL COMMENT '1- Primary, 2- Secondary',
                `archive` enum('1','0') NOT NULL DEFAULT '0'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "organisation_site_key_contact` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `siteId` int(8) NOT NULL,
                `firstname` varchar(150) NOT NULL,
                `lastname` varchar(150) NOT NULL,
                `position` varchar(32) NOT NULL,
                `department` varchar(32) NOT NULL,
                `type` int(11) NOT NULL COMMENT '1- Support Coordinator, 2- Member, 3- Key Contact, 4-Billing',
                `street` varchar(128) NOT NULL,
                `city` varchar(64) NOT NULL,
                `postal` varchar(10) NOT NULL,
                `state` tinyint(4) NOT NULL,
                `archive` enum('1','0') NOT NULL DEFAULT '0',
                `created` datetime NOT NULL,
                `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
            ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "organisation_site_key_contact_email` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `contactId` int(11) NOT NULL,
                `email` varchar(64) NOT NULL,
                `primary_email` enum('1','2') NOT NULL COMMENT '1- Primary, 2- Secondary',
                `archive` enum('1','0') NOT NULL COMMENT '0- COMMENT, 1- Delete'
            ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "organisation_site_key_contact_phone` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `contactId` int(5) NOT NULL,
                `phone` varchar(20) NOT NULL,
                `primary_phone` enum('1','0') NOT NULL COMMENT '1- Primary, 2- Secondary',
                `archive` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1- Delete'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "organisation_site_phone` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `siteId` int(5) NOT NULL,
                `phone` varchar(20) NOT NULL,
                `primary_phone` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1- Primary, 2- Secondary',
                `archive` enum('1','0') NOT NULL DEFAULT '0'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `username` varchar(200) NOT NULL,
                `firstname` varchar(32) NOT NULL,
                `middlename` varchar(32) NOT NULL,
                `lastname` varchar(32) NOT NULL,
                `gender` tinyint(1) NOT NULL COMMENT '1- Male, 2- Female',
                `profile_image` varchar(255) NOT NULL,
                `relation` varchar(50) NOT NULL,
                `preferredname` varchar(32) NOT NULL,
                `prefer_contact` int(11) NOT NULL COMMENT '1-for contact/2-for-email',
                `dob` date NOT NULL,
                `ndis_num` varchar(15) NOT NULL,
                `medicare_num` varchar(15) NOT NULL,
                `crn_num` varchar(15) NOT NULL,
                `referral` tinyint(4) NOT NULL COMMENT '1- Yes, 0- No',
                `referral_firstname` varchar(200) NOT NULL,
                `referral_lastname` varchar(200) NOT NULL,
                `referral_email` varchar(100) NOT NULL,
                `referral_phone` varchar(20) NOT NULL,
                `living_situation` varchar(20) NOT NULL,
                `aboriginal_tsi` varchar(20) NOT NULL,
                `oc_departments` tinyint(2) NOT NULL,
                `houseId` int(5) NOT NULL DEFAULT '0',
                `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `status` tinyint(1) NOT NULL COMMENT '1- Active, 0- Inactive',
                `portal_access` tinyint(1) NOT NULL COMMENT '1- Yes, 0- No',
                `archive` tinyint(4) NOT NULL,
                `password` text NOT NULL,
                `loginattempt` tinyint(4) NOT NULL,
                `token` varchar(200) NOT NULL,
                `booking_status` int(11) NOT NULL COMMENT '1 - open/ 2 - close',
                `booking_date` date NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_about_care` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `participantId` int(11) NOT NULL,
                `title` text NOT NULL,
                `content` text NOT NULL,
                `primary_key` int(11) NOT NULL COMMENT '1-primary/2-secondary',
                `categories` varchar(200) NOT NULL,
                `archive` int(11) NOT NULL COMMENT '0-for-not/1-for deleted',
                `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_activity` (
                `activityId` smallint(6) NOT NULL,
                `participantId` int(11) NOT NULL,
                `type` smallint(1) NOT NULL COMMENT '1- Favourite, 2- Least Favourite'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_address` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `participantId` int(8) NOT NULL DEFAULT '0',
                `street` varchar(128) NOT NULL,
                `city` varchar(64) NOT NULL,
                `postal` varchar(10) NOT NULL,
                `state` tinyint(1) NOT NULL,
                `lat` varchar(200) DEFAULT NULL,
                `long` varchar(200) DEFAULT NULL,
                `site_category` tinyint(1) NOT NULL,
                `primary_address` tinyint(1) NOT NULL COMMENT '1- Primary, 2- Secondary',
                `archive` int(11) NOT NULL COMMENT '0-for-not-deleted/1-for delete'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_assistance` (
                `participantId` int(8) NOT NULL DEFAULT '0',
                `assistanceId` smallint(2) NOT NULL,
                `type` varchar(100) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_booking_list` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `participantId` int(11) NOT NULL,
                `firstname` varchar(200) NOT NULL,
                `lastname` varchar(200) NOT NULL,
                `relation` varchar(200) NOT NULL,
                `phone` varchar(100) NOT NULL,
                `email` varchar(200) NOT NULL,
                `created` datetime NOT NULL,
                `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `archive` enum('0','1') NOT NULL COMMENT '0- not /1 - archive'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_care_not_tobook` (
                `participantId` int(8) NOT NULL,
                `gender` tinyint(1) NOT NULL,
                `ethnicity` varchar(20) NOT NULL,
                `religious` varchar(20) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_care_requirement` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `participantId` int(8) NOT NULL,
                `diagnosis_primary` text NOT NULL,
                `diagnosis_secondary` text NOT NULL,
                `participant_care` text NOT NULL,
                `cognition` varchar(20) NOT NULL,
                `communication` varchar(20) NOT NULL,
                `english` tinyint(1) NOT NULL COMMENT '1- Yes, 0- No, 2- Yes but not preferred',
                `preferred_language` varchar(16) NOT NULL,
                `preferred_language_other` varchar(200) NOT NULL,
                `linguistic_interpreter` tinyint(1) NOT NULL COMMENT '1- Yes, 0- No',
                `hearing_interpreter` tinyint(1) NOT NULL COMMENT '1- Yes, 0- No',
                `require_assistance_other` varchar(256) NOT NULL,
                `support_require_other` varchar(256) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_docs` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `participantId` int(8) NOT NULL,
                `type` tinyint(1) NOT NULL COMMENT '1- Service, 2- SIL Doc',
                `title` varchar(32) NOT NULL,
                `filename` varchar(64) NOT NULL,
                `created` datetime NOT NULL,
                `archive` tinyint(4) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_email` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `participantId` int(8) NOT NULL,
                `email` varchar(64) NOT NULL,
                `primary_email` tinyint(1) NOT NULL COMMENT '1- Primary, 2- Secondary'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_genral` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `name` varchar(255) NOT NULL,
                `type` varchar(100) NOT NULL,
                `status` tinyint(4) NOT NULL COMMENT '0-for inactive/ 1-for-active',
                `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        // Batch insert into participant_genral from participant_genral.json
        $this->insert_from_json(TBL_PREFIX . 'participant_genral');

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_goal` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `participantId` int(8) NOT NULL,
                `title` varchar(228) NOT NULL,
                `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `end_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                `status` int(11) NOT NULL COMMENT '1- Active / 2 - Archive',
                `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_goal_result` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `participantId` int(8) NOT NULL DEFAULT '0',
                `goalId` int(8) NOT NULL DEFAULT '0',
                `shiftId` int(8) NOT NULL,
                `created` datetime NOT NULL,
                `rating` smallint(2) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_kin` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `participantId` int(8) NOT NULL,
                `firstname` varchar(32) NOT NULL,
                `lastname` varchar(32) NOT NULL,
                `relation` varchar(24) NOT NULL,
                `phone` varchar(20) NOT NULL,
                `email` varchar(64) NOT NULL,
                `primary_kin` tinyint(1) NOT NULL COMMENT '1- Primary, 2- Secondary',
                `archive` int(11) NOT NULL COMMENT '0 - Not / - archive',
                `updated` datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_login` (
                `participantId` int(8) NOT NULL,
                `token` text NOT NULL,
                `ip_address` varchar(100) NOT NULL,
                `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_oc_services` (
                `participantId` int(8) NOT NULL DEFAULT '0',
                `oc_service` smallint(2) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_phone` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `participantId` int(8) NOT NULL DEFAULT '0',
                `phone` varchar(20) NOT NULL,
                `primary_phone` tinyint(1) NOT NULL COMMENT '1- Primary, 2- Secondary'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_place` (
                `placeId` mediumint(9) NOT NULL,
                `participantId` int(11) NOT NULL,
                `type` tinyint(1) NOT NULL COMMENT '1- Favourite, 2- Least Favourite'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_plan` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `participantId` int(8) NOT NULL,
                `plan_type` tinyint(1) NOT NULL,
                `plan_id` varchar(20) NOT NULL,
                `start_date` varchar(20) NOT NULL,
                `end_date` varchar(20) NOT NULL,
                `total_funding` double(10,2) NOT NULL DEFAULT '0.00',
                `fund_used` double(10,2) NOT NULL DEFAULT '0.00',
                `remaing_fund` double(10,2) NOT NULL DEFAULT '0.00'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_plan_site` (
                `planId` int(8) NOT NULL DEFAULT '0',
                `participantId` int(8) NOT NULL,
                `address` varchar(128) NOT NULL,
                `city` varchar(32) NOT NULL,
                `postal` varchar(10) NOT NULL,
                `state` tinyint(1) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_remove_account` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `participantId` int(8) NOT NULL DEFAULT '0',
                `reason` varchar(20) NOT NULL,
                `contact` tinyint(1) NOT NULL COMMENT '1- Yes, 0- No'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_roster` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `participantId` int(10) NOT NULL,
                `title` varchar(50) NOT NULL,
                `is_default` tinyint(1) NOT NULL COMMENT '1- No, 0- Yes',
                `start_date` timestamp NULL DEFAULT NULL,
                `end_date` timestamp NULL DEFAULT NULL,
                `shift_round` varchar(20) DEFAULT NULL,
                `status` tinyint(1) NOT NULL COMMENT '1 - Active / 2 - Pending /3 - Inactive /4 - Reject / 5 - Archive',
                `created` datetime NOT NULL,
                `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_roster_data` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `rosterId` int(10) NOT NULL,
                `week_day` tinyint(1) NOT NULL COMMENT '1- Mon, 2- Tue... 7-Sun',
                `start_time` timestamp NULL DEFAULT NULL,
                `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                `week_number` tinyint(1) NOT NULL COMMENT '1- First, 2- second, 3- Third, 4- Four'
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_roster_temp_data` (
                `rosterId` int(11) NOT NULL,
                `rosterData` longtext NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "participant_support_required` (
                `participantId` int(11) NOT NULL,
                `support_required` int(11) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "permission` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `companyId` smallint(3) NOT NULL DEFAULT '0',
                `permission` varchar(64) NOT NULL,
                `title` varchar(64) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "place` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `companyId` smallint(3) NOT NULL,
                `name` varchar(50) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        // Batch insert into place from place.json
        $this->insert_from_json(TBL_PREFIX . 'place');

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "role` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `companyId` smallint(3) NOT NULL DEFAULT '0',
                `name` varchar(32) NOT NULL,
                `status` tinyint(1) NOT NULL DEFAULT '1',
                `archive` tinyint(4) NOT NULL,
                `created` datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        // Batch insert into role from role.json
        $this->insert_from_json(TBL_PREFIX . 'role');

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "role_permission` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `roleId` smallint(4) NOT NULL,
                `permission` int(6) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "shift` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `booked_by` tinyint(1) NOT NULL COMMENT '1= Site / Home, 2= Participant, 3 =Location',
                `shift_date` date DEFAULT NULL,
                `start_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
                `end_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
                `expenses` varchar(100) NOT NULL,
                `so` tinyint(1) NOT NULL COMMENT '1- Yes, 0- No',
                `ao` tinyint(1) NOT NULL COMMENT '1- Yes, 0- No',
                `eco` tinyint(1) NOT NULL COMMENT '1- Yes, 0- No',
                `price` double(10,2) NOT NULL DEFAULT '0.00',
                `allocate_pre_member` tinyint(1) NOT NULL COMMENT '1- Yes, 0- No',
                `autofill_shift` tinyint(1) NOT NULL COMMENT '1- Yes, 0- No',
                `push_to_app` tinyint(4) NOT NULL COMMENT '1- Yes, 0- No, 2 Return from App',
                `status` int(11) NOT NULL COMMENT '1-Unfilled/ 2- Unconfirmed / 3- Quote / 4 -Rejected / 5 -Cancelled / 6 - Completed / 7 - Confirmed / 8 - Archive',
                `shift_amendment` tinyint(1) NOT NULL COMMENT '1- shift has amendment / 2- no amendment',
                `created` timestamp NOT NULL,
                `updated` timestamp NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "shift_amendment` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `shiftId` int(11) NOT NULL,
                `start_time` time NOT NULL,
                `end_time` time NOT NULL,
                `total_break_time` int(11) NOT NULL,
                `shift_km` varchar(150) NOT NULL,
                `additional_expenses` tinyint(4) NOT NULL COMMENT '1- Yes, 0- No',
                `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "shift_caller` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `shiftId` int(10) NOT NULL,
                `booker_id` int(11) NOT NULL,
                `booking_method` int(11) NOT NULL,
                `firstname` varchar(32) NOT NULL,
                `lastname` varchar(32) NOT NULL,
                `email` varchar(64) NOT NULL,
                `phone` varchar(20) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "shift_cancelled` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `shiftId` int(11) NOT NULL,
                `cancel_type` varchar(100) NOT NULL COMMENT 'member / participant / kin / org / site / booker	',
                `cancel_by` int(11) NOT NULL COMMENT 'MemberId / ParticipantId',
                `cancel_method` int(11) NOT NULL COMMENT '1 - Email / 2 - Call / 3 -SMS',
                `person_name` varchar(200) NOT NULL,
                `reason` text NOT NULL COMMENT 't',
                `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "shift_confirmation` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `shiftId` int(10) NOT NULL,
                `confirm_with` tinyint(1) NOT NULL COMMENT '1-Booker, 2-Other, 3-Key Contacts, 4-Billing Contact',
                `confirm_by` tinyint(1) NOT NULL COMMENT '1- Phone, 2- Email',
                `firstname` varchar(32) NOT NULL,
                `lastname` varchar(32) NOT NULL,
                `email` varchar(64) NOT NULL,
                `phone` varchar(20) NOT NULL,
                `confirmed_with_allocated` datetime NOT NULL,
                `confirmed_on` datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "shift_expenses_attachment` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `shiftId` int(8) NOT NULL,
                `shift_amendment_id` int(8) NOT NULL,
                `receipt` varchar(200) NOT NULL,
                `receipt_value` varchar(200) NOT NULL,
                `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "shift_feedback` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `shift_id` int(8) NOT NULL,
                `member_id` int(8) NOT NULL,
                `incident_type` tinyint(1) NOT NULL,
                `what_happen` text NOT NULL,
                `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "shift_incident_type` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `name` varchar(255) NOT NULL,
                `created` datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "shift_location` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `shiftId` int(10) NOT NULL,
                `address` varchar(128) NOT NULL,
                `suburb` varchar(100) DEFAULT NULL COMMENT 'city',
                `state` tinyint(2) NOT NULL,
                `postal` varchar(10) NOT NULL,
                `lat` varchar(100) DEFAULT NULL,
                `long` varchar(100) DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "shift_member` (
                `shiftId` int(10) NOT NULL,
                `memberId` int(8) NOT NULL,
                `status` tinyint(1) NOT NULL COMMENT '1-Pending/ 2 - Rejected / 3 - Accepted / 4 - Cancelled',
                `created` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='use for member allocation for particular shift';
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "shift_notes` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `shiftId` int(11) NOT NULL,
                `adminId` int(11) NOT NULL,
                `title` varchar(200) NOT NULL,
                `notes` text NOT NULL,
                `archive` int(11) NOT NULL COMMENT '0- not/ 1 - archive',
                `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "shift_org_requirement` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `name` varchar(150) NOT NULL,
                `archive` tinyint(4) NOT NULL,
                `created` datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Predefined requirement of shift organisation from OCS';
        ");

        // Batch insert into shift_org_requirement from shift_org_requirement.json
        $this->insert_from_json(TBL_PREFIX . 'shift_org_requirement');

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "shift_org_requirements` (
                `shiftId` int(10) NOT NULL,
                `requirementId` tinyint(2) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='requirement of shift(site)';
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "shift_participant` (
                `shiftId` int(10) NOT NULL,
                `participantId` int(8) NOT NULL,
                `status` tinyint(1) NOT NULL,
                `created` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "shift_preferred_member` (
                `shiftId` int(10) NOT NULL,
                `memberId` int(8) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "shift_requirement` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `name` varchar(100) NOT NULL,
                `archive` tinyint(4) NOT NULL,
                `created` datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Predefined requirement of shift from OCS';
        ");

        // Batch insert into shift_requirement from shift_requirement.json
        $this->insert_from_json(TBL_PREFIX . 'shift_requirement');

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "shift_requirements` (
                `shiftId` int(10) NOT NULL,
                `requirementId` tinyint(2) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='requirement of shift(guardian)';
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "shift_sign_off` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `shiftId` int(11) NOT NULL,
                `member_signature` varchar(200) NOT NULL,
                `approval_signature` varchar(200) NOT NULL,
                `created` datetime NOT NULL,
                `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "shift_site` (
                `shiftId` int(10) NOT NULL,
                `siteId` int(8) NOT NULL,
                `created` timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "state` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `country_id` int(11) DEFAULT NULL,
                `name` varchar(100) NOT NULL,
                `archive` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1- Delete',
                `created` datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        // Batch insert into state from state.json
        $this->insert_from_json(TBL_PREFIX . 'state');

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "suburb_state` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `suburb` varchar(100) NOT NULL,
                `state` varchar(50) NOT NULL,
                `stateId` int(11) NOT NULL,
                `postcode` int(11) NOT NULL,
                `latitude` varchar(200) NOT NULL,
                `longitude` varchar(200) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        // Batch insert into suburb_state from suburb_state.json
        $this->insert_from_json(TBL_PREFIX . 'suburb_state');

        $this->db->query("
            CREATE TABLE `" . TBL_PREFIX . "testimonial` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `full_name` varchar(100) NOT NULL,
                `module_type` int(11) NOT NULL COMMENT '1 - member/ 2 - participant',
                `title` varchar(200) NOT NULL,
                `testimonial` text NOT NULL,
                `status` tinyint(1) NOT NULL,
                `created` datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");
    }
    
    public function down()
    {
        $this->dbforge->drop_table(TBL_PREFIX . 'testimonial');
        $this->dbforge->drop_table(TBL_PREFIX . 'suburb_state');
        $this->dbforge->drop_table(TBL_PREFIX . 'state');
        $this->dbforge->drop_table(TBL_PREFIX . 'shift_site');
        $this->dbforge->drop_table(TBL_PREFIX . 'shift_sign_off');
        $this->dbforge->drop_table(TBL_PREFIX . 'shift_requirements');
        $this->dbforge->drop_table(TBL_PREFIX . 'shift_requirement');
        $this->dbforge->drop_table(TBL_PREFIX . 'shift_preferred_member');
        $this->dbforge->drop_table(TBL_PREFIX . 'shift_participant');
        $this->dbforge->drop_table(TBL_PREFIX . 'shift_org_requirements');
        $this->dbforge->drop_table(TBL_PREFIX . 'shift_org_requirement');
        $this->dbforge->drop_table(TBL_PREFIX . 'shift_notes');
        $this->dbforge->drop_table(TBL_PREFIX . 'shift_member');
        $this->dbforge->drop_table(TBL_PREFIX . 'shift_location');
        $this->dbforge->drop_table(TBL_PREFIX . 'shift_incident_type');
        $this->dbforge->drop_table(TBL_PREFIX . 'shift_feedback');
        $this->dbforge->drop_table(TBL_PREFIX . 'shift_expenses_attachment');
        $this->dbforge->drop_table(TBL_PREFIX . 'shift_confirmation');
        $this->dbforge->drop_table(TBL_PREFIX . 'shift_cancelled');
        $this->dbforge->drop_table(TBL_PREFIX . 'shift_caller');
        $this->dbforge->drop_table(TBL_PREFIX . 'shift_amendment');
        $this->dbforge->drop_table(TBL_PREFIX . 'shift');
        $this->dbforge->drop_table(TBL_PREFIX . 'role_permission');
        $this->dbforge->drop_table(TBL_PREFIX . 'role');
        $this->dbforge->drop_table(TBL_PREFIX . 'place');
        $this->dbforge->drop_table(TBL_PREFIX . 'permission');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_support_required');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_roster_temp_data');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_roster_data');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_roster');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_remove_account');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_plan_site');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_plan');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_place');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_phone');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_oc_services');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_login');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_kin');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_goal_result');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_goal');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_genral');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_email');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_docs');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_care_requirement');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_care_not_tobook');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_booking_list');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_assistance');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_address');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_activity');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant_about_care');
        $this->dbforge->drop_table(TBL_PREFIX . 'participant');
        $this->dbforge->drop_table(TBL_PREFIX . 'organisation_site_phone');
        $this->dbforge->drop_table(TBL_PREFIX . 'organisation_site_key_contact_phone');
        $this->dbforge->drop_table(TBL_PREFIX . 'organisation_site_key_contact_email');
        $this->dbforge->drop_table(TBL_PREFIX . 'organisation_site_key_contact');
        $this->dbforge->drop_table(TBL_PREFIX . 'organisation_site_email');
        $this->dbforge->drop_table(TBL_PREFIX . 'organisation_site');
        $this->dbforge->drop_table(TBL_PREFIX . 'organisation_requirements');
        $this->dbforge->drop_table(TBL_PREFIX . 'organisation_requirement');
        $this->dbforge->drop_table(TBL_PREFIX . 'organisation_phone');
        $this->dbforge->drop_table(TBL_PREFIX . 'organisation_email');
        $this->dbforge->drop_table(TBL_PREFIX . 'organisation_docs');
        $this->dbforge->drop_table(TBL_PREFIX . 'organisation_all_contact_phone');
        $this->dbforge->drop_table(TBL_PREFIX . 'organisation_all_contact_email');
        $this->dbforge->drop_table(TBL_PREFIX . 'organisation_all_contact');
        $this->dbforge->drop_table(TBL_PREFIX . 'organisation_address');
        $this->dbforge->drop_table(TBL_PREFIX . 'organisation');
        $this->dbforge->drop_table(TBL_PREFIX . 'notification');
        $this->dbforge->drop_table(TBL_PREFIX . 'member_work_area');
        $this->dbforge->drop_table(TBL_PREFIX . 'member_special_agreement');
        $this->dbforge->drop_table(TBL_PREFIX . 'member_qualification');
        $this->dbforge->drop_table(TBL_PREFIX . 'member_position_award');
        $this->dbforge->drop_table(TBL_PREFIX . 'member_place');
        $this->dbforge->drop_table(TBL_PREFIX . 'member_phone');
        $this->dbforge->drop_table(TBL_PREFIX . 'member_login');
        $this->dbforge->drop_table(TBL_PREFIX . 'member_kin');
        $this->dbforge->drop_table(TBL_PREFIX . 'member_email');
        $this->dbforge->drop_table(TBL_PREFIX . 'member_contact');
        $this->dbforge->drop_table(TBL_PREFIX . 'member_availability_list');
        $this->dbforge->drop_table(TBL_PREFIX . 'member_availability');
        $this->dbforge->drop_table(TBL_PREFIX . 'member_address');
        $this->dbforge->drop_table(TBL_PREFIX . 'member_activity');
        $this->dbforge->drop_table(TBL_PREFIX . 'member');
        $this->dbforge->drop_table(TBL_PREFIX . 'logs');
        $this->dbforge->drop_table(TBL_PREFIX . 'internal_message_team_admin');
        $this->dbforge->drop_table(TBL_PREFIX . 'internal_message_team');
        $this->dbforge->drop_table(TBL_PREFIX . 'internal_message_recipient');
        $this->dbforge->drop_table(TBL_PREFIX . 'internal_message_content');
        $this->dbforge->drop_table(TBL_PREFIX . 'internal_message_attachment');
        $this->dbforge->drop_table(TBL_PREFIX . 'internal_message_action');
        $this->dbforge->drop_table(TBL_PREFIX . 'internal_message');
        $this->dbforge->drop_table(TBL_PREFIX . 'house_resident');
        $this->dbforge->drop_table(TBL_PREFIX . 'house_requirements');
        $this->dbforge->drop_table(TBL_PREFIX . 'house_phone');
        $this->dbforge->drop_table(TBL_PREFIX . 'house_email');
        $this->dbforge->drop_table(TBL_PREFIX . 'house_docs');
        $this->dbforge->drop_table(TBL_PREFIX . 'house_all_contact');
        $this->dbforge->drop_table(TBL_PREFIX . 'house');
        $this->dbforge->drop_table(TBL_PREFIX . 'group_message_action');
        $this->dbforge->drop_table(TBL_PREFIX . 'group_message');
        $this->dbforge->drop_table(TBL_PREFIX . 'goal_rating');
        $this->dbforge->drop_table(TBL_PREFIX . 'fms_case_reason');
        $this->dbforge->drop_table(TBL_PREFIX . 'fms_case_party');
        $this->dbforge->drop_table(TBL_PREFIX . 'fms_case_notes');
        $this->dbforge->drop_table(TBL_PREFIX . 'fms_case_log');
        $this->dbforge->drop_table(TBL_PREFIX . 'fms_case_location');
        $this->dbforge->drop_table(TBL_PREFIX . 'fms_case_link');
        $this->dbforge->drop_table(TBL_PREFIX . 'fms_case_docs');
        $this->dbforge->drop_table(TBL_PREFIX . 'fms_case_category');
        $this->dbforge->drop_table(TBL_PREFIX . 'fms_case_all_category');
        $this->dbforge->drop_table(TBL_PREFIX . 'fms_case_against_detail');
        $this->dbforge->drop_table(TBL_PREFIX . 'fms_case');
        $this->dbforge->drop_table(TBL_PREFIX . 'fms_address_book');
        $this->dbforge->drop_table(TBL_PREFIX . 'external_message_recipient');
        $this->dbforge->drop_table(TBL_PREFIX . 'external_message_content');
        $this->dbforge->drop_table(TBL_PREFIX . 'external_message_attachment');
        $this->dbforge->drop_table(TBL_PREFIX . 'external_message_action');
        $this->dbforge->drop_table(TBL_PREFIX . 'external_message');
        $this->dbforge->drop_table(TBL_PREFIX . 'department');
        $this->dbforge->drop_table(TBL_PREFIX . 'company');
        $this->dbforge->drop_table(TBL_PREFIX . 'call_log');
        $this->dbforge->drop_table(TBL_PREFIX . 'app_feedback');
        $this->dbforge->drop_table(TBL_PREFIX . 'approval');
        $this->dbforge->drop_table(TBL_PREFIX . 'admin_role');
        $this->dbforge->drop_table(TBL_PREFIX . 'admin_phone');
        $this->dbforge->drop_table(TBL_PREFIX . 'admin_login_history');
        $this->dbforge->drop_table(TBL_PREFIX . 'admin_login');
        $this->dbforge->drop_table(TBL_PREFIX . 'admin_email');
        $this->dbforge->drop_table(TBL_PREFIX . 'admin');
        $this->dbforge->drop_table(TBL_PREFIX . 'activity');
    }
}
