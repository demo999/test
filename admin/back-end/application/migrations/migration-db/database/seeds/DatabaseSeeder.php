<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $this->call([
            ActivitySeeder::class,
            GoalRatingSeeder::class,
            CompanySeeder::class,
            StateSeeder::class,
            ShiftIncidentTypeSeeder::class,
            PlaceSeeder::class,
            FmsCaseAllCategorySeeder::class,
            OrganisationRequirementSeeder::class,
            ParticipantGenralSeeder::class,
            PermissionSeeder::class,
            AdminEmailSeeder::class,
            AdminPhoneSeeder::class,
            AdminSeeder::class,
            RoleSeeder::class,
            DepartmentSeeder::class,
            RolePermissionSeeder::class,
            MemberSeeder::class,
            ShiftRequirementSeeder::class,
            ShiftRequirementOrgSeeder::class,
            SuburbStateSeeder::class,
            FinanceInvoiceSeeder::class,
            ClassificationLevelSeeder::class,
            ClassificationPointSeeder::class,
            SupportTypeSeeder::class,
            FinanceInvoiceDocsSeeder::class,
            CrmDepartmentSeeder::class,
            CrmStageSeeder::class,
            VendorSeeder::class,
            CrmtaskprioritySeeder::class //Make this in last
        ]);
    }

}
