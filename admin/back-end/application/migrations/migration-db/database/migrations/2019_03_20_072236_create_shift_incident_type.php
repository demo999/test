<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShiftIncidentType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tbl_shift_incident_type')) {
            Schema::create('tbl_shift_incident_type', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name',255);
                $table->datetime('created')->default('0000-00-00 00:00:00');

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_shift_incident_type');
    }
}
