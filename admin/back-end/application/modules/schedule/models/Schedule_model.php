<?php

class Schedule_model extends CI_Model {

    public function get_participant_name($post_data) {
        $this->db->select("CONCAT(firstname,' ',middlename,' ',lastname) as label");
        $this->db->select('id as value');
        $this->db->where('archive', 0);
        $this->db->where('status', 1);
        $this->db->group_start();
        $this->db->or_like('firstname', $post_data);
        $this->db->or_like('lastname', $post_data);
        $this->db->group_end();
        $query = $this->db->get(TBL_PREFIX . 'participant');
        $query->result();
        return $query->result();
    }

    public function get_site_name($post_data) {
        $this->db->like('site_name', $post_data, 'both');
        $this->db->where('archive =', 0);
        $this->db->where('organisationId !=', 0);
        $this->db->select('site_name, id');
        $query = $this->db->get(TBL_PREFIX . 'organisation_site');
        $query->result();
        $participant_rows = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $participant_rows[] = array('label' => $val->site_name, 'value' => $val->id);
            }
        }
        return $participant_rows;
    }

    public function create_shift($reqData) {
        $responseAry = $reqData->data;
        $address_ary = $responseAry->completeAddress;
        $shift_requirement_ary = $responseAry->shift_requirement;
        $org_shift_requirement_ary = $responseAry->shift_org_requirement;
        $preferred_member_ary = $responseAry->preferred_member_ary;
        $participant_member_ary = $responseAry->participant_member_ary;
        $site_lookup_ary = $responseAry->site_lookup_ary;
        $booked_by = $responseAry->booked_by;
        $shift_ary = array('booked_by' => $booked_by,
            'shift_date' => isset($responseAry->start_time) ? DateFormate($responseAry->start_time, 'Y-m-d') : '',
            'start_time' => isset($responseAry->start_time) ? DateFormate($responseAry->start_time, "Y-m-d H:i:s") : '',
            'end_time' => isset($responseAry->end_time) ? DateFormate($responseAry->end_time, "Y-m-d H:i:s") : '',
            'so' => isset($responseAry->so) ? $responseAry->so : 0,
            'ao' => isset($responseAry->ao) ? $responseAry->ao : 0,
            'eco' => isset($responseAry->eco) ? $responseAry->eco : 0,
            'allocate_pre_member' => $responseAry->allocate_pre_member,
            'push_to_app' => ($responseAry->push_to_app == 2) ? 0 : 1,
            'autofill_shift' => $responseAry->autofill_shift,
            'status' => isset($responseAry->status) ? $responseAry->status : 1,
            'created' => DATE_TIME,
        );
        $shift_id = $this->Basic_model->insert_records('shift', $shift_ary, $multiple = FALSE);
        // make shift caller
        $this->make_shift_caller($responseAry, $shift_id);
        // make shift address
        $this->make_shift_address($address_ary, $shift_id);
        // make shift requirement
        $this->make_shift_requirement($shift_requirement_ary, $shift_id);
        // if booked by organization then inset organization requirement
        if ($booked_by == 1) {
            $this->make_org_shift_requirement($org_shift_requirement_ary, $shift_id);
        }
        // make shift preferred member
        $this->make_shift_preferred_member_and_allocate($preferred_member_ary, $responseAry->allocate_pre_member, $shift_id);
        // make shift confirmation details
        $this->make_shift_confirmation_details($responseAry, $shift_id);
        // if booked by organization then inset organization information
        if ($booked_by == 1) {
            $this->make_org_shift_user($site_lookup_ary, $shift_id);
        }
        // if booked by participant and location then inset participant information
        if ($booked_by != 1) {
            $this->make_shift_participant_user($participant_member_ary, $shift_id);
        }
        $allocation_result = false;
        if ($responseAry->autofill_shift == 1) {
            $allocation_result = $this->make_auto_fill_shift($responseAry, $shift_id);
        }
        return array('shiftId' => $shift_id, 'allocation' => $responseAry->autofill_shift, 'allocation_res' => $allocation_result);
    }

    function make_shift_caller($responseAry, $shift_id) {
        $shift_caller = array('shiftId' => $shift_id,
            'firstname' => $responseAry->caller_name,
            'lastname' => isset($responseAry->caller_lastname) ? $responseAry->caller_lastname : '',
            'email' => $responseAry->caller_email,
            'phone' => $responseAry->caller_phone,
            'booker_id' => isset($responseAry->booker_id) ? $responseAry->booker_id : '',
            'booking_method' => $responseAry->booking_method
        );
        // insert caller data
        $this->Basic_model->insert_records('shift_caller', $shift_caller, $multiple = FALSE);
    }

    function make_shift_address($address_ary, $shift_id) {
        if (!empty($address_ary)) {
            foreach ($address_ary as $value) {
                $address = $value->address . ' ' . $value->suburb->label . ' ' . $value->state . ' ' . $value->postal;
                $lat_long = getLatLong($address);
                if (!empty($lat_long)) {
                    $lat = $lat_long['lat'];
                    $long = $lat_long['long'];
                }
                $shift_addr[] = array('shiftId' => $shift_id,
                    'address' => $value->address,
                    'suburb' => $value->suburb->label,
                    'state' => $value->state,
                    'postal' => $value->postal,
                    'lat' => isset($lat) ? $lat : '',
                    'long' => isset($long) ? $long : '',
                );
            }
            $this->Basic_model->insert_records('shift_location', $shift_addr, $multiple = TRUE);
        }
    }

    function make_shift_requirement($shift_requirement_ary, $shift_id) {
        if (!empty($shift_requirement_ary)) {
            $shift_requirements = array();
            foreach ($shift_requirement_ary as $key => $val) {
                if (isset($val->checked)) {
                    $shift_requirements[] = array('shiftId' => $shift_id,
                        'requirementId' => $val->value,
                    );
                }
            }
            if (!empty($shift_requirements))
                $this->Basic_model->insert_records('shift_requirements', $shift_requirements, $multiple = TRUE);
        }
    }

    function make_org_shift_requirement($org_shift_requirement_ary, $shift_id) {
        if (!empty($org_shift_requirement_ary)) {
            $org_shift_requirements = array();
            foreach ($org_shift_requirement_ary as $val) {
                if (isset($val->checked)) {
                    $org_shift_requirements[] = array('shiftId' => $shift_id,
                        'requirementId' => $val->value,
                    );
                }
            }
            if (!empty($org_shift_requirements)) {
                $this->Basic_model->insert_records('shift_org_requirements', $org_shift_requirements, $multiple = TRUE);
            }
        }
    }

    function make_shift_preferred_member_and_allocate($preferred_member_ary, $allocate_preferred, $shift_id) {
        if (!empty($preferred_member_ary)) {
            $shift_member = array();
            $allocated_member = array();
            foreach ($preferred_member_ary as $key => $vall) {
                if (isset($vall->name->value)) {
                    $shift_member[] = array('shiftId' => $shift_id, 'memberId' => $vall->name->value);
                    $allocated_member[] = array('shiftId' => $shift_id, 'memberId' => $vall->name->value, 'status' => 1, 'created' => DATE_TIME);
                }
            }
            if (!empty($shift_member)) {
                $this->Basic_model->insert_records('shift_preferred_member', $shift_member, $multiple = TRUE);
            }
            if ($allocate_preferred == 1 && !empty($allocated_member)) {
                $this->move_shift_to_unconfirmed($shift_id);
                $this->Basic_model->insert_records('shift_member', $allocated_member, $multiple = TRUE);
            }
        }
    }

    function make_shift_confirmation_details($responseAry, $shift_id) {
        $shift_confirmation = array('shiftId' => $shift_id,
            'confirm_with' => $responseAry->confirm_with,
            'confirm_by' => $responseAry->confirm_by,
            'firstname' => '',
            'lastname' => '',
            'email' => '',
            'phone' => '',
        );
        $shift_confirmation['firstname'] = $responseAry->confirm_with_f_name;
        $shift_confirmation['lastname'] = $responseAry->confirm_with_l_name;
        $shift_confirmation['email'] = $responseAry->confirm_with_email;
        $shift_confirmation['phone'] = $responseAry->confirm_with_mobile;
        // insert confirmation details
        $this->Basic_model->insert_records('shift_confirmation', $shift_confirmation, $multiple = FALSE);
    }

    function make_org_shift_user($site_lookup_ary, $shift_id) {
        if (!empty($site_lookup_ary)) {
            $shift_site = array();
            foreach ($site_lookup_ary as $key => $value) {
                if (isset($value->name->value)) {
                    $shift_site[] = array('shiftId' => $shift_id,
                        'siteId' => $value->name->value,
                        'created' => DATE_TIME,
                    );
                }
            }
            if (!empty($shift_site)) {
                $this->Basic_model->insert_records('shift_site', $shift_site, $multiple = true);
            }
        }
    }

    function make_shift_participant_user($participant_member_ary, $shift_id) {
        if (!empty($participant_member_ary)) {
            $shift_participant = array();
            foreach ($participant_member_ary as $value) {
                if (isset($value->name->value)) {
                    $shift_participant[] = array('shiftId' => $shift_id,
                        'participantId' => $value->name->value,
                        'status' => 1,
                        'created' => DATE_TIME,
                    );
                }
            }
            if (!empty($shift_participant)) {
                $this->Basic_model->insert_records('shift_participant', $shift_participant, $multiple = true);
            }
        }
    }

    public function get_booking_list($reqData) {
        $srchId = $reqData->data->srchId;
        $tbl_participant_booking_list = 'tbl_participant_booking_list';
        $this->db->select(array("CONCAT(" . $tbl_participant_booking_list . ".firstname,' '," . $tbl_participant_booking_list . ".lastname ) as name", "id", "phone", "email", "firstname", "lastname"));
        $this->db->where('participantId', $srchId);
        $this->db->where('archive', '0');
        $query = $this->db->get($tbl_participant_booking_list);
        $state = array();
        $query->result();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $state[] = array('label' => $val->name, 'value' => $val->id, 'firstname' => $val->firstname, 'lastname' => $val->lastname, 'email' => $val->email, 'phone' => $val->phone);
            }
        }
//        $state[] = array('label' => 'Other', 'value' => 'value');
        return $state;
    }

    function move_shift_to_unconfirmed($shiftId) {
        $this->basic_model->update_records('shift', array('status' => 2), $where = array('id' => $shiftId));
    }

    public function make_auto_fill_shift($s_details, $shiftId) {
        $allocation = false;
        $shift_date = date('Y-m-d', strtotime($s_details->start_time));
        $shift_time = $s_details->start_time;
        $records = $this->Listing_model->get_available_member_by_city($shiftId, $shift_date, $shift_time, array());
        if (!empty($records) && !empty($records['available_members'])) {
            $memberids = $records['available_members'];
            $participantId = $s_details->participant_member_ary[0]->name->value;
            if (($s_details->booked_by == 2 || $s_details->booked_by == 3) && count($memberids) > 0) {
      
                $shared_preference = $this->get_available_member_by_preferences($memberids, $participantId, 1);
 
                $memberids = (!empty($shared_preference) && (count($shared_preference) == 1)) ? $shared_preference : $memberids;
            }
            if (!empty($memberids)) {
                foreach ($memberids as $key => $memberId) {
                    $name = $this->get_member_name1($memberId);
                    $allocate_member[] = array('memberId' => $memberId, 'shiftId' => $shiftId, 'status' => 1, 'created' => DATE_TIME);
                    $this->loges->setUserId($shiftId);
                    $this->loges->setDescription(json_encode($allocate_member));
                    $this->loges->setTitle('Assign shift to member ' . $name->memberName . ' : Shift Id ' . $shiftId);
                    $this->loges->createLog();
                    $allocation = true;
                }
                if (!empty($allocate_member)) {
                    $allocation = true;
                    $this->basic_model->insert_records('shift_member', $allocate_member, $multiple = true);
                    $this->move_shift_to_unconfirmed($shiftId);
                }
            }
        }
        return $allocation;
    }

    public function get_nearest_shift_member($data_ary) {
        $shift_id = $data_ary['shift_id'];
        $shift_time = $data_ary['start_time'];
        $limit = $data_ary['memberLimit'];
        $member_lookup = $data_ary['member_lookup'];
        $shift_date = date('Y-m-d', strtotime($data_ary['shift_date']));
        $booked_by = $data_ary['booked_by'];
        if ($booked_by == 1)
            $participantId = 0;
        else
            $participantId = $data_ary['participant_data'][0]->participantId;
        $records = $this->Listing_model->get_available_member_by_city($shift_id, $shift_date, $shift_time, array());
        $memberids = $records['available_members'];
        $temp = $memberids;
        if ($member_lookup == 2 && count($memberids) > 0 && $booked_by != 1) {
            $most_contact = $this->get_available_member_by_previous_work($memberids, $participantId, $limit);
            $temp = isset($most_contact) && count($most_contact) > 0 ? $temp : array();
        } elseif ($member_lookup == 3 && count($memberids) > 0 && $booked_by != 1) {
  
            $shared_preference = $this->get_available_member_by_preferences($memberids, $participantId, $limit);

            $temp = (!empty($shared_preference) && count($shared_preference) > 0) ? $temp : array();
        }
        $available_members = array();
        if (!empty($temp)) {
            foreach ($temp as $key => $value) {
                $name = $this->get_member_name1($value);
                $avail_temp['memberId'] = $value;
                $avail_temp['memberName'] = $name->memberName;
                $available_members[] = $avail_temp;
                if (($key + 1) == $limit)
                    break;
            }
        }
        return $available_members;
    }

    //most contact memeber
    public function get_available_member_by_previous_work($memberIds, $participantId, $limit) {
        $tbl_shift_participant = TBL_PREFIX . 'shift_participant';
        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_member = TBL_PREFIX . 'shift_member';
        $this->db->select(array($tbl_shift_member . '.memberId', 'count(tbl_shift_member.memberId) as count'));
        $this->db->from($tbl_shift_member);
        $this->db->join($tbl_shift, $tbl_shift_member . '.shiftId = ' . $tbl_shift . '.id', 'inner');
        $this->db->join($tbl_shift_participant, $tbl_shift_participant . '.shiftId = ' . $tbl_shift . '.id', 'inner');
        $this->db->where_in($tbl_shift_member . '.memberId', $memberIds);
        $this->db->where(array($tbl_shift . '.status' => 6, $tbl_shift_member . '.status' => 3, $tbl_shift_participant . '.participantId' => $participantId));
        $this->db->order_by('count', 'Asc');
        $this->db->limit($limit);
        $query = $this->db->get();
        $result = $query->result_array();
        if (!empty($result)) {
            $memberIds = array_column($result, 'memberId');
            return $memberIds;
        } else {
            return false;
        }
    }

    public function get_available_member_by_preferences($memberIds, $participantIds, $limit) {
        $particpantID = $participantIds[0];
        $tbl_participant_place = TBL_PREFIX . 'participant_place';
        $tbl_member_place = TBL_PREFIX . 'member_place';
        $resultPlace = array();
        $this->db->select(array("count(tbl_participant_place.placeId) as cnt", "tbl_member_place.memberId"));
        $this->db->from($tbl_participant_place);
        $this->db->where('participantId', $particpantID);
        $this->db->join($tbl_member_place, $tbl_member_place . '.placeId = ' . $tbl_participant_place . '.placeId AND ' . $tbl_member_place . '.memberId IN (' . implode(', ', $memberIds) . ')', 'inner');
        // $this->db->join($tbl_place, $tbl_member_place . '.placeId = ' . $tbl_place . '.id', 'inner');
        $this->db->group_by("tbl_member_place.memberId");
        $this->db->order_by("cnt", 'DESC');
        $this->db->limit($limit);
        $query = $this->db->get();
        $place_member = $query->result_array();

        if (!empty($place_member)) {
            return array_column($place_member, 'memberId');
        } else {
            return $memberIds;
        }
    }

    public function get_member_name1($memberId) {
        $tbl_member = 'tbl_member';
        $this->db->select("CONCAT(" . $tbl_member . ".firstname,' '," . $tbl_member . ".middlename,' '," . $tbl_member . ".lastname ) as memberName");
        $this->db->from($tbl_member);
        $this->db->where($tbl_member . '.id', $memberId);
        $query = $this->db->get();
        return $result = $query->row();
    }

    public function get_roster_data($rosterId) {
        $tbl_participant_roster_data = 'tbl_participant_roster_data';
        $colown_r = array('week_day', 'start_time', 'end_time', 'week_number');
        $this->db->select($colown_r);
        $this->db->from($tbl_participant_roster_data);
        $this->db->where(array('rosterId' => $rosterId));
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_previous_shifts($participantID, $shiftIds) {
        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_participant = TBL_PREFIX . 'shift_participant';
        $select_column = array($tbl_shift . ".id", $tbl_shift . ".shift_date", $tbl_shift . ".start_time", $tbl_shift . ".end_time", $tbl_shift . ".status");
        $this->db->select("CONCAT(MOD( TIMESTAMPDIFF(hour," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 24), ':',MOD( TIMESTAMPDIFF(minute," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 60), ' hrs') as duration");
        $this->db->select($select_column);
        $this->db->from($tbl_shift_participant);
        $this->db->join($tbl_shift, $tbl_shift_participant . '.shiftId = ' . $tbl_shift . '.id', 'inner');
        $this->db->where_in('shift_date', $shiftIds);
        $this->db->where_in($tbl_shift . '.status', array(1, 2, 3, 4, 5, 6, 7));
        $this->db->where('participantId', $participantID);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $response = $query->result_array();
        if (!empty($response)) {
            foreach ($response as $key => $val) {
                $response[$key] = $val;
                $response[$key]['duration'] = ($val['duration']);
            }
        }
        return $response;
    }

    function get_participant_shift_related_information($participantId) {
        $tbl_participant = TBL_PREFIX . 'participant';
        $tbl_participant_address = TBL_PREFIX . 'participant_address';
        $tbl_participant_email = TBL_PREFIX . 'participant_email';
        $tbl_participant_phone = TBL_PREFIX . 'participant_phone';
        $this->db->select(array($tbl_participant_address . '.street as address', $tbl_participant_address . '.city as suburb', $tbl_participant_address . '.postal', $tbl_participant_address . '.state', $tbl_participant_address . '.lat', $tbl_participant_address . '.long'));
        $this->db->select(array($tbl_participant . '.firstname', $tbl_participant . '.lastname', $tbl_participant . '.prefer_contact'));
        $this->db->select(array($tbl_participant_email . '.email', $tbl_participant_phone . '.phone'));
        $this->db->from($tbl_participant);
        $this->db->join($tbl_participant_address, $tbl_participant_address . '.participantId = ' . $tbl_participant . '.id', 'inner');
        $this->db->join($tbl_participant_email, $tbl_participant_email . '.participantId = ' . $tbl_participant . '.id AND ' . $tbl_participant_email . '.primary_email = 1', 'inner');
        $this->db->join($tbl_participant_phone, $tbl_participant_phone . '.participantId = ' . $tbl_participant . '.id AND ' . $tbl_participant_phone . '.primary_phone = 1', 'inner');
        $this->db->where($tbl_participant . '.id', $participantId);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $response = $query->row();
        $response->requirement = $this->basic_model->get_record_where('participant_assistance', array('assistanceId'), $where = array('participantId' => $participantId, 'type' => 'assistance'));
        return $response;
    }

    function check_shift_exist($objShift) {
        $responseAry = $objShift;
        $participantId = $responseAry->getShiftParticipant();
        $shift_date = date('Y-m-d', strtotime($responseAry->getShiftDate()));
        $start_time = date('Y-m-d H:i:s', strtotime($responseAry->getStartTime()));
        $end_time = date('Y-m-d H:i:s', strtotime($responseAry->getEndTime()));
        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_participant = TBL_PREFIX . 'shift_participant';
        $this->db->select(array($tbl_shift . ".id"));
        $this->db->from($tbl_shift);
        $this->db->join($tbl_shift_participant, $tbl_shift_participant . '.shiftId = ' . $tbl_shift . '.id', 'inner');
        $where = "participantId = " . $participantId . " AND
        (('" . $start_time . "' BETWEEN (start_time) AND (end_time))  or ('" . $end_time . "' BETWEEN (start_time) AND (end_time)) OR
        ((start_time) BETWEEN '" . $start_time . "' AND '" . $end_time . "')  or ((end_time) BETWEEN '" . $start_time . "' AND '" . $end_time . "')) ";
        $this->db->where($where);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        #last_query();
        return $query->result();
    }

    public function get_count_filled_shift($view_by, $filled_type) {
        $tb1 = TBL_PREFIX . 'shift';
        $this->db->from($tb1);
        $this->db->where('status', 7);
        if ($view_by == 'month') {
            $where['MONTH(created)'] = date('m');
            $this->db->where($where);
        } else {
            $this->db->where('shift_date BETWEEN (DATE_SUB(NOW(), INTERVAL 1 WEEK)) AND NOW()');
        }
        if ($filled_type == 'by_app') {
            $this->db->where('push_to_app', 1);
        } else {
            $this->db->where('push_to_app', 0);
        }
        $query = $this->db->get();
        $result = $query->num_rows();
        return $result;
    }

    public function get_count_shift_graph($view_by) {
        $tb1 = TBL_PREFIX . 'shift';
        $this->db->from($tb1);
        $this->db->where_in('status', array(1, 2, 4, 7));
        if ($view_by == 'month') {
            $where['MONTH(created)'] = date('m');
            $this->db->where($where);
        } elseif ($view_by == 'week') {
            $this->db->where('shift_date BETWEEN (DATE_SUB(NOW(), INTERVAL 1 WEEK)) AND NOW()');
        } else {
            $this->db->where('shift_date', date("Y-m-d"));
        }
        $query = $this->db->get();
        $result['sub_count'] = $query->num_rows();
//        last_query();
        // get all shift count
        $this->db->from($tb1);
        $this->db->where_in('status', array(1, 2, 4, 7));
        $query = $this->db->get();
        $result['all_shift'] = $query->num_rows();
        return $result;
    }

    public function get_key_billing_persion($siteId, $confirmType) {
        $colown = array('tbl_organisation_site_key_contact.id as value', 'tbl_organisation_site_key_contact.lastname', 'tbl_organisation_site_key_contact.firstname', 'concat(tbl_organisation_site_key_contact.firstname," ",tbl_organisation_site_key_contact.lastname) as label', 'tbl_organisation_site_key_contact_email.email', 'tbl_organisation_site_key_contact_phone.phone');
        $this->db->select($colown);
        $this->db->from('tbl_organisation_site_key_contact');
        $this->db->join('tbl_organisation_site_key_contact_email', 'tbl_organisation_site_key_contact_email.contactId = tbl_organisation_site_key_contact.id and tbl_organisation_site_key_contact_email.primary_email = "1"', 'left');
        $this->db->join('tbl_organisation_site_key_contact_phone', 'tbl_organisation_site_key_contact_phone.contactId = tbl_organisation_site_key_contact.id AND tbl_organisation_site_key_contact_phone.primary_phone = "1"', 'left');
        $this->db->where('tbl_organisation_site_key_contact.archive', "0");
        $this->db->where('tbl_organisation_site_key_contact.type', $confirmType);
        $this->db->where('tbl_organisation_site_key_contact.siteId', $siteId);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function get_member_name($post_data) {
        $pre_selected = array();
//        print_r($post_data);
        if (!empty($post_data['pre_selected'])) {
            foreach ($post_data['pre_selected'] as $val) {
                if (!empty($val->member)) {
                    $pre_selected[] = $val->member->value;
                }
            }
        }
        if (!empty($post_data['pre_assign'])) {
            foreach ($post_data['pre_assign'] as $val) {
                $pre_selected[] = $val->memberId;
            }
        }
        $search = $post_data['search'];
        $this->db->group_start();
        $this->db->or_where("(MATCH (firstname) AGAINST ('$search *'))", NULL, FALSE);
        $this->db->or_where("(MATCH (middlename) AGAINST ('$search *'))", NULL, FALSE);
        $this->db->or_where("(MATCH (lastname) AGAINST ('$search *'))", NULL, FALSE);
        $this->db->group_end();
        $this->db->where('archive', 0);
        $this->db->where('status', 1);
        if (!empty($pre_selected)) {
            $this->db->where_not_in('id', implode(', ', $pre_selected));
        }
        $this->db->select("CONCAT(firstname,' ',middlename,' ',lastname) as label");
        $this->db->select(array('id as value'));
        $query = $this->db->get(TBL_PREFIX . 'member');
        return $query->result();
    }

    function get_canceler_list($reqData) {
        $reqData->shiftId;
        $reqData->cancel_type;
        $shift_participant = $this->Listing_model->get_shift_participant($reqData->shiftId);
        if (!empty($shift_participant)) {
            $participatnId = $shift_participant[0]['participantId'];
            if ($reqData->cancel_type == 'kin') {
                $where = array('participantId' => $participatnId);
                $column = array("concat(firstname,' ',lastname) as  label", "id as value");
                $result = $this->basic_model->get_record_where('participant_kin', $column, $where);
            } else if ($reqData->cancel_type == 'booker') {
                $where = array('participantId' => $participatnId, 'archive' => '0');
                $column = array("concat(firstname,' ',lastname) as  label", "id as value");
                $result = $this->basic_model->get_record_where('participant_booking_list', $column, $where);
            }
            return $result;
        }
    }

    function cancel_shift($reqData) {
        $cancel_by = '';
        $response = $this->Listing_model->get_accepted_shift_member($reqData->shiftId);
        // set member cancelled shift
        if (!empty($response)) {
            $cancel_by = $response[0]->memberId;
        }
        $this->basic_model->update_records('shift_member', $data = array('status' => 4), $where = array('shiftId' => $reqData->shiftId));
        if ($reqData->cancel_type == 'member') {
            $cancel_by = $response[0]->memberId;
        } else if ($reqData->cancel_type == 'participant') {
            $response = $this->Listing_model->get_shift_participant($reqData->shiftId);
            if (!empty($response))
                $cancel_by = $response[0]['participantId'];
        }else if ($reqData->cancel_type == 'kin') {
            $cancel_by = $reqData->cancel_person;
        } else if ($reqData->cancel_type == 'booker') {
            $cancel_by = $reqData->cancel_person;
        } else if ($reqData->cancel_type == 'org') {
            $result = $this->Listing_model->get_shift_oganization($reqData->shiftId);
            if (!empty($result)) {
                $cancel_by = $result[0]->siteId;
            }
        } else if ($reqData->cancel_type == 'site') {
            $result = $this->Listing_model->get_shift_oganization($reqData->shiftId);
            if (!empty($result)) {
                $cancel_by = $result[0]->siteId;
            }
        }
        $this->loges->setUserId($reqData->shiftId);
        $this->loges->setDescription(json_encode($reqData));
        $this->loges->setTitle('Cancel shift : Shift Id ' . $reqData->shiftId);
        $this->loges->createLog();
        $cancel_array = array('shiftId' => $reqData->shiftId, 'reason' => $reqData->reason, 'cancel_type' => $reqData->cancel_type, 'cancel_by' => $cancel_by, 'cancel_method' => $reqData->cancel_method, 'person_name' => '');
        $this->basic_model->insert_records('shift_cancelled', $cancel_array);
        $this->basic_model->update_records('shift', $data = array('status' => 5), $where = array('id' => $reqData->shiftId));
        return $return = array('status' => true);
    }

    public function get_member_allocation_by_pay_point($memberId = ['0'], $extra_parameter = []) {
        $supportType = isset($extra_parameter['support_type']) && !empty($extra_parameter['support_type']) ? $extra_parameter['support_type'] : '';
        $memberId = is_array($memberId) ? array_filter($memberId) : ['0'];
        $priortyOrderColumnConcat = "concat(tcl.level_priority,tcp.point_priority)";
        $priortyOrderColumnConcatMin = "min(" . $priortyOrderColumnConcat . ") as min_priorty_order";
        $priortyOrderColumnConcatAs = $priortyOrderColumnConcat . " as priorty_order";
        $this->db->select($priortyOrderColumnConcatMin, false);
        $this->db->from('tbl_member_position_award as tmpa');
        $this->db->join('tbl_classification_level as tcl', 'tmpa.level=tcl.id', 'inner');
        $this->db->join('tbl_classification_point as tcp', 'tmpa.award=tcp.id', 'inner');
        if (!empty($supportType)) {
            $this->db->where('tmpa.support_type', $supportType);
        }
        $this->db->where('tmpa.archive', 0);
        $this->db->where_in('tmpa.memberId', $memberId);
        $whereClause = $this->db->get_compiled_select();
        $this->db->select($priortyOrderColumnConcatAs, false);
        $this->db->select('tmpa.memberId, tcl.level_name,tcp.point_name,tcl.level_priority,tcp.point_priority');
        $this->db->from('tbl_member_position_award as tmpa');
        $this->db->join('tbl_classification_level as tcl', 'tmpa.level=tcl.id', 'inner');
        $this->db->join('tbl_classification_point as tcp', 'tmpa.award=tcp.id', 'inner');
        if (!empty($supportType)) {
            $this->db->where('tmpa.support_type', $supportType);
        }
        $this->db->where('tmpa.archive', 0);
        $this->db->where_in('tmpa.memberId', $memberId);
        $this->db->where($priortyOrderColumnConcat . "=(" . $whereClause . ")", null, false);
        $this->db->group_by('tmpa.memberId');
        $this->db->order_by('tcl.level_priority asc,tcp.point_priority asc');
        $query = $this->db->get();
        return $query->result_array();
    }

}