<?php

class Listing_model extends CI_Model {

// get unfilled shift
    public function unfilled_shift($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';


        $src_columns = array("sf.id", "sf.start_time", "sf.end_time", 'pr.firstname', 'pr.middlename', 'pr.lastname', "CONCAT(pr.firstname,' ',pr.middlename,' ',pr.lastname)", "CONCAT(pr.firstname,' ',pr.lastname)", 'org_st.site_name');


        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'sf.shift_date';
            $direction = 'ASC';
        }



        if (isset($filter->search_box) && $filter->search_box != "") {
            $this->db->group_start();

            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    if ($serch_column[0] != 'null')
                        $this->db->or_like($serch_column[0], $filter->search_box);
                }
                else if ($column_search != 'null') {
                    $this->db->or_like($column_search, $filter->search_box);
                }
            }

            $this->db->group_end();
        }


        if (!empty($filter->shift_date)) {
            $this->db->where(array("DATE(sf.start_time)" => date('Y-m-d', strtotime($filter->shift_date))));
        } else {
            if (!empty($filter->start_date)) {
                $this->db->where("DATE(sf.start_time) >= '" . date('Y-m-d', strtotime($filter->start_date)) . "'");
            }
            if (!empty($filter->end_date)) {
                $this->db->where("DATE(sf.start_time) <= '" . date('Y-m-d', strtotime($filter->end_date)) . "'");
            }
        }
        if (!empty($filter->push_to_app)) {
            $this->db->where_in('push_to_app', ($filter->push_to_app == 'unfilled') ? array('0', '2') : array(1));
        }

        if (!empty($filter->shift_type)) {
            if ($filter->shift_type == 'am' || $filter->shift_type == 'pm' || $filter->shift_type == 'so') {
                $insterval = shift_type_interval($filter->shift_type);
                $this->db->group_start();

                $this->db->where("DATE_FORMAT(sf.start_time,'%H:%i') >=", date($insterval['start_time']));

                if ($insterval['spacial']) {
                    $this->db->where("DATE_FORMAT(sf.end_time,'%H:%i') <=", "DATE_FORMAT(DATE_ADD(sf.end_time, INTERVAL 1 DAY), '%Y-%m-%d 06:00'");
                } else {
                    $this->db->where("DATE_FORMAT(sf.end_time,'%H:%i') <=", date($insterval['end_time']));
                }

                $this->db->group_end();
            }
        }


        $select_column = array("sf.id", "sf.shift_date", "sf.start_time", "sf.end_time", "sf.expenses", 'sf.booked_by', 'org_st.site_name', 'sf.push_to_app');

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from('tbl_shift as sf');

        $this->db->select("CONCAT(pr.firstname,' ',pr.middlename,' ',pr.lastname) as participantName");
        $this->db->select("CONCAT(MOD( TIMESTAMPDIFF(hour,sf.start_time,sf.end_time), 24), ':',MOD( TIMESTAMPDIFF(minute,sf.start_time,sf.end_time), 60), ' hrs') as duration");


        // join with participant
        $this->db->join('tbl_shift_participant as sf_pr', 'sf_pr.shiftId = sf.id AND sf_pr.status = 1', 'left');
        $this->db->join('tbl_participant as pr', 'pr.id = sf_pr.participantId AND pr.archive = 0', 'left');

        // join with organization site
        $this->db->join('tbl_shift_site as sf_st', 'sf_st.shiftId = sf.id', 'left');
        $this->db->join('tbl_organisation_site as org_st', 'org_st.id = sf_st.siteId', 'left');

        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        $this->db->where('sf.shift_date >=', date('Y-m-d'));

        $this->db->where('sf.status', 1);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

//        last_query();
        $total_count = $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        if (!empty($dataResult)) {
            foreach ($dataResult as $val) {
                if ($val->booked_by == 1) {
                    $val->participantName = $val->site_name;
                }

                $val->memberNames = $this->get_preferred_member($val->id);
                $val->address = $this->get_shift_location($val->id);

                $val->diff = (strtotime(date('Y-m-d H:i:s', strtotime($val->start_time))) - strtotime(date('Y-m-d H:i:s'))) * 1000;

                $val->duration = ($val->duration);
            }
        }

        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'total_count' => $total_count);
        return $return;
    }

// get unconfirmed shift
    public function unconfirmed_shift($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_participnt = TBL_PREFIX . 'shift_participant';
        $tbl_participnt = TBL_PREFIX . 'participant';
        $tbl_shift_site = TBL_PREFIX . 'shift_site';
        $tbl_organisation_site = TBL_PREFIX . 'organisation_site';

        $src_columns = array($tbl_shift . ".id", $tbl_shift . ".start_time", $tbl_shift . ".end_time", $tbl_participnt . '.firstname', $tbl_participnt . '.middlename', $tbl_participnt . '.lastname', "CONCAT(" . $tbl_participnt . ".firstname,' '," . $tbl_participnt . ".middlename,' '," . $tbl_participnt . ".lastname)", "CONCAT(" . $tbl_participnt . ".firstname,' '," . $tbl_participnt . ".lastname)", $tbl_organisation_site . '.site_name');


        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = $tbl_shift . '.id';
            $direction = 'ASC';
        }

       
        if (!empty($filter->search_box)) {
            $this->db->group_start();

            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    if ($serch_column[0] != 'null')
                        $this->db->or_like($serch_column[0], $filter->search_box);
                }
                else if ($column_search != 'null') {
                    $this->db->or_like($column_search, $filter->search_box);
                }
            }

            $this->db->group_end();
        }

        if (!empty($filter->shift_date)) {
            $this->db->where(array("DATE(tbl_shift.start_time)" => date('Y-m-d', strtotime($filter->shift_date))));
        } else {
            if (!empty($filter->start_date)) {
                $this->db->where("DATE(tbl_shift.start_time) >= '" . date('Y-m-d', strtotime($filter->start_date)) . "'");
            }
            if (!empty($filter->end_date)) {
                $this->db->where("DATE(tbl_shift.start_time) <= '" . date('Y-m-d', strtotime($filter->end_date)) . "'");
            }
        }

        if (!empty($filter->shift_type)) {
            if ($filter->shift_type == 'am' || $filter->shift_type == 'pm' || $filter->shift_type == 'so') {
                $insterval = shift_type_interval($filter->shift_type);
                $this->db->group_start();

                $this->db->where("DATE_FORMAT(tbl_shift.start_time,'%H:%i') >=", date($insterval['start_time']));

                if ($insterval['spacial']) {
                    $this->db->where("DATE_FORMAT(tbl_shift.end_time,'%H:%i') <=", "DATE_FORMAT(DATE_ADD(tbl_shift.end_time, INTERVAL 1 DAY), '%Y-%m-%d 06:00'");
                } else {
                    $this->db->where("DATE_FORMAT(tbl_shift.end_time,'%H:%i') <=", date($insterval['end_time']));
                }

                $this->db->group_end();
            }
        }

        $select_column = array($tbl_shift . ".id", $tbl_shift . ".shift_date", $tbl_shift . ".start_time", $tbl_shift . ".end_time", $tbl_shift . ".expenses", $tbl_shift . '.booked_by', $tbl_organisation_site . '.site_name');

        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_shift);

        $this->db->select("CONCAT(" . $tbl_participnt . ".firstname,' '," . $tbl_participnt . ".middlename,' '," . $tbl_participnt . ".lastname ) as participantName");
        $this->db->select("CONCAT(MOD( TIMESTAMPDIFF(hour," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 24), ':',MOD( TIMESTAMPDIFF(minute," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 60), ' hrs') as duration");

        $this->db->join($tbl_shift_participnt, $tbl_shift_participnt . '.shiftId = ' . $tbl_shift . '.id AND ' . $tbl_shift_participnt . '.status = 1', 'left');
        $this->db->join($tbl_participnt, $tbl_participnt . '.id = ' . $tbl_shift_participnt . '.participantId AND tbl_participant.archive = 0', 'left');

        // join with organization site
        $this->db->join($tbl_shift_site, $tbl_shift_site . '.shiftId = ' . $tbl_shift . '.id', 'left');
        $this->db->join($tbl_organisation_site, $tbl_organisation_site . '.id = ' . $tbl_shift_site . '.siteId', 'left');

        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        $this->db->where($tbl_shift . ".status", (($filter->status === 'unconfirmed') ? 2 : 3));
        $this->db->where($tbl_shift . '.shift_date >=', date('Y-m-d'));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
//         last_query(); die;
        $total_count = $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        if (!empty($dataResult)) {
            foreach ($dataResult as $val) {
                if ($val->booked_by == 1) {
                    $val->participantName = $val->site_name;
                }

                $val->address = $this->get_shift_location($val->id);
                $val->memberName = $this->get_allocated_member($val->id);

                $val->diff = (strtotime(date('Y-m-d H:i:s', strtotime($val->start_time))) - strtotime(date('Y-m-d H:i:s'))) * 1000;

                $val->duration = ($val->duration);
            }
        }

        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'total_count' => $total_count);
        return $return;
    }

// get cancelled and cancelled shift    
    public function rejected_and_cancelled_shift($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_participnt = TBL_PREFIX . 'shift_participant';
        $tbl_participnt = TBL_PREFIX . 'participant';
        $tbl_shift_site = TBL_PREFIX . 'shift_site';
        $tbl_organisation_site = TBL_PREFIX . 'organisation_site';

        $src_columns = array($tbl_shift . ".id", $tbl_shift . ".start_time", $tbl_shift . ".end_time", $tbl_participnt . '.firstname', $tbl_participnt . '.middlename', $tbl_participnt . '.lastname', "CONCAT(" . $tbl_participnt . ".firstname,' '," . $tbl_participnt . ".middlename,' '," . $tbl_participnt . ".lastname)", "CONCAT(" . $tbl_participnt . ".firstname,' '," . $tbl_participnt . ".lastname)", $tbl_organisation_site . '.site_name');


        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = $tbl_shift . '.id';
            $direction = 'ASC';
        }

        $where = '';
        $where = $tbl_shift . ".status = " . (($filter->status == 'Rejected') ? 4 : 5);
        $sWhere = $where;


        if (!empty($filter->search_box)) {
            $where = $where . " AND (";
            $sWhere = " (" . $where;
            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    $sWhere .= $serch_column[0] . " LIKE '%" . $this->db->escape_like_str($filter->search_box) . "%' OR ";
                } else {
                    $sWhere .= $column_search . " LIKE '%" . $this->db->escape_like_str($filter->search_box) . "%' OR ";
                }
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= '))';
        }

        if (!empty($filter->shift_date)) {
            $this->db->where(array($tbl_shift . ".shift_date" => date('Y-m-d', strtotime($filter->shift_date))));
        } else {
            if (!empty($filter->start_date)) {
                $this->db->where($tbl_shift . ".shift_date >= '" . date('Y-m-d', strtotime($filter->start_date)) . "'");
            }
            if (!empty($filter->end_date)) {
                $this->db->where($tbl_shift . ".shift_date <= '" . date('Y-m-d', strtotime($filter->end_date)) . "'");
            }
        }

        if (!empty($filter->shift_type)) {
            if ($filter->shift_type == 'am' || $filter->shift_type == 'pm' || $filter->shift_type == 'so') {
                $insterval = shift_type_interval($filter->shift_type);
                $this->db->group_start();

                $this->db->where("DATE_FORMAT(tbl_shift.start_time,'%H:%i') >=", date($insterval['start_time']));

                if ($insterval['spacial']) {
                    $this->db->where("DATE_FORMAT(tbl_shift.end_time,'%H:%i') <=", "DATE_FORMAT(DATE_ADD(tbl_shift.end_time, INTERVAL 1 DAY), '%Y-%m-%d 06:00'");
                } else {
                    $this->db->where("DATE_FORMAT(tbl_shift.end_time,'%H:%i') <=", date($insterval['end_time']));
                }

                $this->db->group_end();
            }
        }

        $select_column = array($tbl_shift . ".id", $tbl_shift . ".shift_date", $tbl_shift . ".start_time", $tbl_shift . ".end_time", $tbl_shift . ".expenses", $tbl_shift . '.booked_by', $tbl_organisation_site . '.site_name');

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_shift);

        $this->db->select("CONCAT(" . $tbl_participnt . ".firstname,' '," . $tbl_participnt . ".middlename,' '," . $tbl_participnt . ".lastname ) as participantName");
        $this->db->select("CONCAT(MOD( TIMESTAMPDIFF(hour," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 24), ':',MOD( TIMESTAMPDIFF(minute," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 60), ' hrs') as duration");

        $this->db->join($tbl_shift_participnt, $tbl_shift_participnt . '.shiftId = ' . $tbl_shift . '.id AND ' . $tbl_shift_participnt . '.status = 1', 'left');
        $this->db->join($tbl_participnt, $tbl_participnt . '.id = ' . $tbl_shift_participnt . '.participantId AND tbl_participant.archive = 0', 'left');

        // join with organization site
        $this->db->join($tbl_shift_site, $tbl_shift_site . '.shiftId = ' . $tbl_shift . '.id', 'left');
        $this->db->join($tbl_organisation_site, $tbl_organisation_site . '.id = ' . $tbl_shift_site . '.siteId', 'left');

        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        $this->db->where($sWhere, null, false);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
//        last_query(); die;
        $total_count = $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        if (!empty($dataResult)) {
            foreach ($dataResult as $val) {
                $val->address = $this->get_shift_location($val->id);

                if ($val->booked_by == 1) {
                    $val->participantName = $val->site_name;
                }

                if ($filter->status == 'Rejected') {
                    $val->memberName = $this->get_rejected_member($val->id);
                } else {
                    $val->cancelled_data = $this->get_cancelled_details($val->id);
                }

                $val->diff = (strtotime(date('Y-m-d H:i:s', strtotime($val->start_time))) - strtotime(date('Y-m-d H:i:s'))) * 1000;


                $val->duration = ($val->duration);
            }
        }

        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'total_count' => $total_count);
        return $return;
    }

    // get filled shift
    public function get_completed_shifts($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_participnt = TBL_PREFIX . 'shift_participant';
        $tbl_participnt = TBL_PREFIX . 'participant';
        $tbl_shift_site = TBL_PREFIX . 'shift_site';
        $tbl_organisation_site = TBL_PREFIX . 'organisation_site';

        $src_columns = array($tbl_shift . ".id", $tbl_shift . ".start_time", $tbl_shift . ".end_time", $tbl_participnt . '.firstname', $tbl_participnt . '.middlename', $tbl_participnt . '.lastname', "CONCAT(" . $tbl_participnt . ".firstname,' '," . $tbl_participnt . ".middlename,' '," . $tbl_participnt . ".lastname)", "CONCAT(" . $tbl_participnt . ".firstname,' '," . $tbl_participnt . ".lastname)", $tbl_organisation_site . '.site_name');


        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = $tbl_shift . '.id';
            $direction = 'ASC';
        }

        $where = '';
        $where = $tbl_shift . ".status = 6";
        $sWhere = $where;


        if (!empty($filter->search_box)) {
            $where = $where . " AND (";
            $sWhere = " (" . $where;
            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    $sWhere .= $serch_column[0] . " LIKE '%" . $this->db->escape_like_str($filter->search_box) . "%' OR ";
                } else {
                    $sWhere .= $column_search . " LIKE '%" . $this->db->escape_like_str($filter->search_box) . "%' OR ";
                }
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= '))';
        }

        if (!empty($filter->shift_date)) {
            $this->db->where(array($tbl_shift . ".shift_date" => date('Y-m-d', strtotime($filter->shift_date))));
        } else {
            if (!empty($filter->start_date)) {
                $this->db->where($tbl_shift . ".shift_date >= '" . date('Y-m-d', strtotime($filter->start_date)) . "'");
            }
            if (!empty($filter->end_date)) {
                $this->db->where($tbl_shift . ".shift_date <= '" . date('Y-m-d', strtotime($filter->end_date)) . "'");
            }
        }

        if (!empty($filter->shift_type)) {
            if ($filter->shift_type == 'am' || $filter->shift_type == 'pm' || $filter->shift_type == 'so') {
                $insterval = shift_type_interval($filter->shift_type);
                $this->db->group_start();

                $this->db->where("DATE_FORMAT(tbl_shift.start_time,'%H:%i') >=", date($insterval['start_time']));

                if ($insterval['spacial']) {
                    $this->db->where("DATE_FORMAT(tbl_shift.end_time,'%H:%i') <=", "DATE_FORMAT(DATE_ADD(tbl_shift.end_time, INTERVAL 1 DAY), '%Y-%m-%d 06:00'");
                } else {
                    $this->db->where("DATE_FORMAT(tbl_shift.end_time,'%H:%i') <=", date($insterval['end_time']));
                }

                $this->db->group_end();
            }
        }

        $select_column = array($tbl_shift . ".id", $tbl_shift . ".shift_date", $tbl_shift . ".start_time", $tbl_shift . ".end_time", $tbl_shift . ".expenses", $tbl_shift . '.booked_by', $tbl_organisation_site . '.site_name');

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_shift);

        $this->db->select("CONCAT(" . $tbl_participnt . ".firstname,' '," . $tbl_participnt . ".middlename,' '," . $tbl_participnt . ".lastname ) as participantName");
        $this->db->select("CONCAT(MOD( TIMESTAMPDIFF(hour," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 24), ':',MOD( TIMESTAMPDIFF(minute," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 60), ' hrs') as duration");

        $this->db->join($tbl_shift_participnt, $tbl_shift_participnt . '.shiftId = ' . $tbl_shift . '.id AND ' . $tbl_shift_participnt . '.status = 1', 'left');
        $this->db->join($tbl_participnt, $tbl_participnt . '.id = ' . $tbl_shift_participnt . '.participantId', 'left');

        // join with organization site
        $this->db->join($tbl_shift_site, $tbl_shift_site . '.shiftId = ' . $tbl_shift . '.id', 'left');
        $this->db->join($tbl_organisation_site, $tbl_organisation_site . '.id = ' . $tbl_shift_site . '.siteId', 'left');

        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        $this->db->where($sWhere, null, false);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
//         last_query(); die;
        $total_count = $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        if (!empty($dataResult)) {
            foreach ($dataResult as $val) {
                if ($val->booked_by == 1) {
                    $val->participantName = $val->site_name;
                }

                $val->address = $this->get_shift_location($val->id);
                $val->memberNames = $this->get_accepted_shift_member($val->id);

                $val->diff = (strtotime(date('Y-m-d H:i:s', strtotime($val->start_time))) - strtotime(date('Y-m-d H:i:s'))) * 1000;

                $val->duration = ($val->duration);
            }
        }

        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'total_count' => $total_count);
        return $return;
    }

    // get filled shift
    public function get_filled_shifts($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_participnt = TBL_PREFIX . 'shift_participant';
        $tbl_participnt = TBL_PREFIX . 'participant';
        $tbl_shift_site = TBL_PREFIX . 'shift_site';
        $tbl_organisation_site = TBL_PREFIX . 'organisation_site';

        $src_columns = array($tbl_shift . ".id", $tbl_shift . ".start_time", $tbl_shift . ".end_time", $tbl_participnt . '.firstname', $tbl_participnt . '.middlename', $tbl_participnt . '.lastname', "CONCAT(" . $tbl_participnt . ".firstname,' '," . $tbl_participnt . ".middlename,' '," . $tbl_participnt . ".lastname)", "CONCAT(" . $tbl_participnt . ".firstname,' '," . $tbl_participnt . ".lastname)", $tbl_organisation_site . '.site_name');


        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = $tbl_shift . '.id';
            $direction = 'ASC';
        }

        $where = '';
        $where = $tbl_shift . ".status = 7";
        $sWhere = $where;


        if (!empty($filter->search_box)) {
            $where = $where . " AND (";
            $sWhere = " (" . $where;
            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    $sWhere .= $serch_column[0] . " LIKE '%" . $this->db->escape_like_str($filter->search_box) . "%' OR ";
                } else {
                    $sWhere .= $column_search . " LIKE '%" . $this->db->escape_like_str($filter->search_box) . "%' OR ";
                }
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= '))';
        }

        if (!empty($filter->shift_date)) {
            $this->db->where(array($tbl_shift . ".shift_date" => date('Y-m-d', strtotime($filter->shift_date))));
        } else {
            if (!empty($filter->start_date)) {
                $this->db->where($tbl_shift . ".shift_date >= '" . date('Y-m-d', strtotime($filter->start_date)) . "'");
            }
            if (!empty($filter->end_date)) {
                $this->db->where($tbl_shift . ".shift_date <= '" . date('Y-m-d', strtotime($filter->end_date)) . "'");
            }
        }

        if (!empty($filter->shift_type)) {
            if ($filter->shift_type == 'am' || $filter->shift_type == 'pm' || $filter->shift_type == 'so') {
                $insterval = shift_type_interval($filter->shift_type);
                $this->db->group_start();

                $this->db->where("DATE_FORMAT(tbl_shift.start_time,'%H:%i') >=", date($insterval['start_time']));

                if ($insterval['spacial']) {
                    $this->db->where("DATE_FORMAT(tbl_shift.end_time,'%H:%i') <=", "DATE_FORMAT(DATE_ADD(tbl_shift.end_time, INTERVAL 1 DAY), '%Y-%m-%d 06:00'");
                } else {
                    $this->db->where("DATE_FORMAT(tbl_shift.end_time,'%H:%i') <=", date($insterval['end_time']));
                }

                $this->db->group_end();
            }
        }

        $select_column = array($tbl_shift . ".id", $tbl_shift . ".shift_date", $tbl_shift . ".start_time", $tbl_shift . ".end_time", $tbl_shift . ".expenses", $tbl_shift . '.booked_by', $tbl_organisation_site . '.site_name');

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_shift);

        $this->db->select("CONCAT(" . $tbl_participnt . ".firstname,' '," . $tbl_participnt . ".middlename,' '," . $tbl_participnt . ".lastname ) as participantName");
        $this->db->select("CONCAT(MOD( TIMESTAMPDIFF(hour," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 24), ':',MOD( TIMESTAMPDIFF(minute," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 60), ' hrs') as duration");

        $this->db->join($tbl_shift_participnt, $tbl_shift_participnt . '.shiftId = ' . $tbl_shift . '.id AND ' . $tbl_shift_participnt . '.status = 1', 'left');
        $this->db->join($tbl_participnt, $tbl_participnt . '.id = ' . $tbl_shift_participnt . '.participantId AND tbl_participant.archive = 0', 'left');

        // join with organization site
        $this->db->join($tbl_shift_site, $tbl_shift_site . '.shiftId = ' . $tbl_shift . '.id', 'left');
        $this->db->join($tbl_organisation_site, $tbl_organisation_site . '.id = ' . $tbl_shift_site . '.siteId', 'left');

        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        $this->db->where($sWhere, null, false);
        $this->db->where($tbl_shift . '.shift_date >=', date('Y-m-d'));
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
//         last_query(); die;
        $total_count = $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        if (!empty($dataResult)) {
            foreach ($dataResult as $val) {
                if ($val->booked_by == 1) {
                    $val->participantName = $val->site_name;
                }

                $val->address = $this->get_shift_location($val->id);
                $val->memberName = $this->get_accepted_shift_member($val->id);

                $val->diff = (strtotime(date('Y-m-d H:i:s', strtotime($val->start_time))) - strtotime(date('Y-m-d H:i:s'))) * 1000;

                $val->duration = ($val->duration);
            }
        }

        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'total_count' => $total_count);
        return $return;
    }

    public function get_preferred_member($shiftId) {
        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_preferred_member = TBL_PREFIX . 'shift_preferred_member';
        $tbl_member = TBL_PREFIX . 'member';

        $this->db->select("CONCAT(" . $tbl_member . ".firstname,' '," . $tbl_member . ".middlename,' '," . $tbl_member . ".lastname ) as memberName");
        $this->db->select(array($tbl_shift_preferred_member . '.memberId'));
        $this->db->from($tbl_shift_preferred_member);
        $this->db->join($tbl_shift, $tbl_shift_preferred_member . '.shiftId = ' . $tbl_shift . '.id', 'left');
        $this->db->join($tbl_member, $tbl_member . '.id = ' . $tbl_shift_preferred_member . '.memberId', 'left');
        $this->db->where(array($tbl_shift_preferred_member . '.shiftId' => $shiftId));


        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result = $query->result();
//        last_query();
        return $query->result();
    }

    public function get_shift_location($shiftId) {
        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_location = TBL_PREFIX . 'shift_location';

        $this->db->select("CONCAT(" . $tbl_shift_location . ".address,', ', " . $tbl_shift_location . ".suburb,', ', " . $tbl_shift_location . ".postal,', ', tbl_state.name) as site");
        $this->db->select(array($tbl_shift_location . ".suburb", $tbl_shift_location . '.address', $tbl_shift_location . '.state', $tbl_shift_location . '.postal'));
        $this->db->from($tbl_shift_location);
        $this->db->join('tbl_state', 'tbl_state.id = ' . $tbl_shift_location . '.state', 'left');
        $this->db->where(array($tbl_shift_location . '.shiftId' => $shiftId));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        return $query->result();
    }

    public function get_shift_participant($shiftId) {
        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_participnt = TBL_PREFIX . 'shift_participant';
        $tbl_participnt = TBL_PREFIX . 'participant';

        $this->db->select('CONCAT("\'",tbl_participant.firstname,"\' ",tbl_participant.middlename, " ", tbl_participant.lastname ) as participantName');
        $this->db->select(array($tbl_participnt . '.firstname', $tbl_participnt . '.id as participantId'));

        $this->db->from($tbl_shift_participnt);
        $this->db->join($tbl_participnt, $tbl_shift_participnt . '.participantId = ' . $tbl_participnt . '.id', 'left');
        $this->db->where(array($tbl_shift_participnt . '.shiftId' => $shiftId));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
//        last_query();
        return $query->result_array();
    }

    public function get_shift_oganization($shiftId) {
        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_site = TBL_PREFIX . 'shift_site';
        $tbl_organisation_site = TBL_PREFIX . 'organisation_site';

        $this->db->select(array($tbl_organisation_site . '.site_name', $tbl_organisation_site . '.id as siteId'));

        $this->db->from($tbl_shift_site);
        $this->db->join($tbl_organisation_site, $tbl_organisation_site . '.id = ' . $tbl_shift_site . '.siteId', 'inner');
        $this->db->where(array($tbl_shift_site . '.shiftId' => $shiftId));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
//        last_query();
        return $query->result();
    }

    public function get_shift_requirement($shiftId) {
        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_requirement = TBL_PREFIX . 'shift_requirement';
        $tbl_shift_requirements = TBL_PREFIX . 'shift_requirements';

        $arr['shift_requirement'] = '';

        $dt_query = $this->db->select(array($tbl_requirement . ".id", $tbl_requirement . ".name", $tbl_shift_requirements . ".requirementId as active"));
        $this->db->from($tbl_requirement);
        $this->db->join($tbl_shift_requirements, $tbl_shift_requirements . '.requirementId = ' . $tbl_requirement . '.id AND shiftId = ' . $shiftId, 'left');
        $query = $this->db->get();
        $arr['requirement'] = $query->result();

        if (!empty($arr['requirement'])) {
            $requirement = [];
            foreach ($arr['requirement'] as $val) {
                if ($val->active > 0) {
                    $requirement[] = $val->name;
                }
            }
            $arr['shift_requirement'] = implode(', ', $requirement);
        }

        return $arr;
    }

    public function get_allocated_member($shiftId) {
        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_member = TBL_PREFIX . 'shift_member';
        $tbl_shift_preferred_member = TBL_PREFIX . 'shift_preferred_member';
        $tbl_member = TBL_PREFIX . 'member';

        $this->db->select("CONCAT(" . $tbl_member . ".firstname,' '," . $tbl_member . ".middlename,' '," . $tbl_member . ".lastname ) as memberName");
        $this->db->select(array($tbl_shift_member . '.created', $tbl_shift_preferred_member . '.memberId as preferred', $tbl_shift_preferred_member . '.memberId'));
        $this->db->from($tbl_shift_member);
        $this->db->join($tbl_shift_preferred_member, $tbl_shift_preferred_member . '.memberId = ' . $tbl_shift_member . '.memberId AND ' . $tbl_shift_preferred_member . '.shiftId =' . $shiftId, 'left');
        $this->db->join($tbl_member, $tbl_shift_member . '.memberId = ' . $tbl_member . '.id', 'left');
        $this->db->where(array($tbl_shift_member . '.shiftId' => $shiftId));
        $this->db->where(array($tbl_shift_member . '.status' => 1));
        $this->db->group_by($tbl_shift_member . '.shiftId');

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $val) {
                $val->preferred = ($val->preferred > 0) ? true : false;
                $val->allocate_on = date('d/m/y - h:ia', strtotime($val->created));
                $val->remaning_time = ((strtotime($val->created) + $this->config->item('member_allocated_time')) - strtotime(DATE_TIME)) * 1000;
            }
        }
//        last_query();
        return $query->result();
    }

    public function get_accepted_shift_member($shiftId) {
        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_member = TBL_PREFIX . 'shift_member';
        $tbl_shift_preferred_member = TBL_PREFIX . 'shift_preferred_member';
        $tbl_member = TBL_PREFIX . 'member';

        $this->db->select("CONCAT(" . $tbl_member . ".firstname,' '," . $tbl_member . ".middlename,' '," . $tbl_member . ".lastname ) as memberName");
        $this->db->select(array($tbl_shift_member . '.created', $tbl_shift_preferred_member . '.memberId as preferred', $tbl_member . ".id as memberId"));
        $this->db->from($tbl_shift_member);
        $this->db->join($tbl_shift_preferred_member, $tbl_shift_preferred_member . '.shiftId = ' . $tbl_shift_member . '.memberId', 'left');
        $this->db->join($tbl_member, $tbl_shift_member . '.memberId = ' . $tbl_member . '.id', 'left');
        $this->db->where(array($tbl_shift_member . '.shiftId' => $shiftId));
        $this->db->where(array($tbl_shift_member . '.status' => 3));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $val) {
                $val->allocate_on = date('d/m/y - h:ia', strtotime($val->created));
                $val->remaning_time = ((strtotime($val->created) + $this->config->item('member_allocated_time')) - strtotime(DATE_TIME)) * 1000;
            }
        }
        return $query->result();
    }

    public function get_rejected_member($shiftId) {
        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_member = TBL_PREFIX . 'shift_member';
        $tbl_shift_preferred_member = TBL_PREFIX . 'shift_preferred_member';
        $tbl_member = TBL_PREFIX . 'member';

        $this->db->select("CONCAT(" . $tbl_member . ".firstname,' '," . $tbl_member . ".middlename,' '," . $tbl_member . ".lastname ) as memberName");
        $this->db->from($tbl_shift_member);
        $this->db->join($tbl_member, $tbl_shift_member . '.memberId = ' . $tbl_member . '.id', 'left');
        $this->db->where(array($tbl_shift_member . '.shiftId' => $shiftId));
        $this->db->where(array($tbl_shift_member . '.status' => 2));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result = $query->result();
        return $result;
    }

    public function get_shift_details($shiftId, $mutiple = false) {
        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_preferred_member = TBL_PREFIX . 'shift_preferred_member';

        $select_column = array("id", "shift_date", "start_time", "end_time", "expenses", "created", "status", "booked_by");

        $this->db->select($select_column);
        $this->db->select("CONCAT(MOD( TIMESTAMPDIFF(hour," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 24), ':',MOD( TIMESTAMPDIFF(minute," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 60), ' hrs') as duration");

        $this->db->from($tbl_shift);


        if ($mutiple) {
            $this->db->where_in($tbl_shift . '.id', $shiftId);
        } else {
            $this->db->where(array($tbl_shift . '.id' => $shiftId));
        }

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $response = $query->result();

        $result = $mutliple_result = array();
        if (!empty($response)) {
            foreach ($response as $key => $val) {
                $result = (array) $val;


                $result['diff'] = (strtotime(date('Y-m-d H:i:s', strtotime($val->start_time))) - strtotime(date('Y-m-d H:i:s'))) * 1000;

                $result['duration'] = ($val->duration);
                if ($mutiple) {
                    $mutliple_result[] = $result;
                }
            }
        }

        if ($mutiple) {
            return $mutliple_result;
        } else {
            return $result;
        }
    }

    public function get_cancelled_details($shiftId) {
        $tbl_shift_cancelled = TBL_PREFIX . 'shift_cancelled';

        $this->db->select(array('reason', 'cancel_type', 'cancel_by', 'person_name'));
        $this->db->from($tbl_shift_cancelled);
        $this->db->where(array($tbl_shift_cancelled . '.shiftId' => $shiftId));

        $this->db->order_by($tbl_shift_cancelled . '.id', 'desc');
        $this->db->limit(1);

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $val) {
                $val->reinitiate = false;

                if ($val->cancel_type == 'member') { // 1 = member
                    $this->load->model('Schedule_model');
                    $val->reinitiate = true;
                    $x = $this->Schedule_model->get_member_name1($val->cancel_by);
                    $val->cancel_by = $x->memberName . ' (Member)';
                } elseif ($val->cancel_type == 'participant') {
                    $tbl_participant = TBL_PREFIX . 'participant';
                    $this->db->select("CONCAT(" . $tbl_participant . ".firstname,' '," . $tbl_participant . ".middlename,' '," . $tbl_participant . ".lastname ) as participantName");
                    $this->db->from($tbl_participant);
                    $this->db->where(array($tbl_participant . '.id' => $val->cancel_by));
                    $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
                    $participant_result = $query->row();

                    if (!empty($participant_result)) {
                        $val->cancel_by = $participant_result->participantName;
                    }
                } elseif ($val->cancel_type == 'kin') {
                    $clwn = array("concat(firstname,' ',lastname) as kinName");
                    $kin_res = $this->basic_model->get_row('participant_kin', $clwn, $where = array('id' => $val->cancel_by));

                    if (!empty($kin_res)) {
                        $val->cancel_by = $kin_res->kinName;
                    }
                } elseif ($val->cancel_type == 'booker') {
                    $clwn = array("concat(firstname,' ',lastname) as bookerName");
                    $kin_res = $this->basic_model->get_row('participant_booking_list', $clwn, $where = array('id' => $val->cancel_by));
                    if (!empty($kin_res)) {
                        $val->cancel_by = $kin_res->bookerName;
                    }
                } elseif ($val->cancel_type == 'org') {
                    $val->cancel_by = 'Oranization';
                } elseif ($val->cancel_type == 'site') {
                    $val->cancel_by = 'Site';
                } else {
                    $val->cancel_by = 'N/A';
                }
            }
        }

        return $result;
    }

    public function get_shift_confirmation_details($shiftId) {
        $tbl_shift_confirmation = TBL_PREFIX . 'shift_confirmation';

        $this->db->select(array('confirm_with', 'confirm_by', 'firstname', 'lastname', 'confirmed_with_allocated', 'confirmed_on', 'phone', 'email'));
        $this->db->from($tbl_shift_confirmation);
        $this->db->where(array('shiftId' => $shiftId));
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $response = $query->row();


        if (!empty($response)) {
            $response->confirmed_with_allocated = ($response->confirmed_with_allocated == '0000-00-00 00:00:00') ? '' : $response->confirmed_with_allocated;
            $response->confirmed_on = ($response->confirmed_on == '0000-00-00 00:00:00') ? '' : $response->confirmed_on;
        }

        return $response;
    }

    public function get_shift_caller($shiftId) {
        $tbl_shift_caller = TBL_PREFIX . 'shift_caller';

        $this->db->select(array('firstname', 'lastname', 'phone', 'booking_method', 'email'));
        $this->db->from($tbl_shift_caller);
        $this->db->where(array('shiftId' => $shiftId));
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $response = $query->row();

        return (array) $response;
    }

    public function get_available_member_by_city($shiftId, $shift_date, $shift_time, $pre_selected_member) {

        $shift_time = getAvailabilityType($shift_time);
        $date = date('Y-m-d', strtotime($shift_date));

        $this->db->select(array("tbl_member_address.memberId", "tbl_member_address.lat", "tbl_member_address.long", "tbl_shift_location.lat", "tbl_shift_location.long",
            "tbl_member_availability_list.flexible_availability", "tbl_member_availability_list.flexible_km", "tbl_member_availability_list.travel_km"));

        $this->db->select("(6371 * acos( 
                cos( radians(tbl_shift_location.lat) ) 
              * cos( radians(tbl_member_address.lat) ) 
              * cos( radians(tbl_member_address.long ) - radians(tbl_shift_location.long) ) 
              + sin( radians(tbl_shift_location.lat) ) 
              * sin( radians(tbl_member_address.lat ) )
                ) ) as distance_km");

        $this->db->select("(select tbl_shift_member.memberId from tbl_shift 
left join tbl_shift_member on tbl_shift.id = tbl_shift_member.shiftId 
where tbl_shift.shift_date = '" . $date . "'  and  tbl_shift_member.memberId = tbl_member_address.memberId and tbl_shift_member.status IN (1,2,3) limit 1) as reflect");


        $this->db->from('tbl_shift_location');
        $this->db->join('tbl_member_address', 'tbl_member_address.city = tbl_shift_location.suburb', 'left');
        $this->db->join('tbl_member', 'tbl_member.id = tbl_member_address.memberId', 'left');
        $this->db->join('tbl_member_availability_list', 'tbl_member_availability_list.memberId = tbl_member_address.memberId', 'left');

        $this->db->where(array("tbl_shift_location.shiftId" => $shiftId));
        $this->db->where(array("tbl_member.status" => 1));
        $this->db->where(array("tbl_member.archive" => 0));

        $this->db->where(array("tbl_member_availability_list.availability_date" => $date));
        $this->db->where_in("tbl_member_availability_list.availability_type", $shift_time);

        $this->db->group_by("tbl_member_availability_list.memberId");


        if (!empty($pre_selected_member) && array_key_exists($shift_date, $pre_selected_member)) {
            $this->db->where_not_in('tbl_member_availability_list.memberId', $pre_selected_member[$shift_date]);
        }

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $available_members = array();
        $result = $query->result();

        $memberIdWithDistance = array();
        if (!empty($result)) {
            foreach ($result as $val) {
                $traval_distance = $val->travel_km;

                if ($val->flexible_availability)
                    $traval_distance = $val->travel_km + $val->flexible_km;

                if ($val->travel_km >= $val->distance_km && !$val->reflect) {

                    $memberIdWithDistance[$val->memberId] = number_format($val->distance_km, 2);
                    $available_members[] = $val->memberId;
                }
            }
        }


        return array('available_members' => $available_members, 'memberIdWithDistance' => $memberIdWithDistance);
    }

    public function get_available_member_by_preferences($memberIds, $participantIds) {
        $particpantID = $participantIds[0];

        $tbl_participant_place = TBL_PREFIX . 'participant_place';
        $tbl_member_place = TBL_PREFIX . 'member_place';
        //$tbl_place = TBL_PREFIX . 'place';

        $resultPlace = array();

        $this->db->select(array("count(tbl_participant_place.placeId) as cnt", "tbl_member_place.memberId"));
        $this->db->from($tbl_participant_place);

        $this->db->where('participantId', $particpantID);
        $this->db->join($tbl_member_place, $tbl_member_place . '.placeId = ' . $tbl_participant_place . '.placeId AND ' . $tbl_member_place . '.memberId IN (' . implode(', ', $memberIds) . ')', 'inner');
        // $this->db->join($tbl_place, $tbl_member_place . '.placeId = ' . $tbl_place . '.id', 'inner');
        $this->db->group_by("tbl_member_place.memberId");
        $this->db->order_by("cnt", 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        $place_member = $query->row();

        if (!empty($place_member)) {
            return $place_member->memberId;
        } else {
            return false;
        }
    }

    public function get_prefference_activity_places($memberId, $participantId) {
        $tbl_place = TBL_PREFIX . 'place';
        $tbl_participant_place = TBL_PREFIX . 'participant_place';
        $tbl_member_place = TBL_PREFIX . 'member_place';

        $this->db->select(array($tbl_place . '.name'));
        $this->db->from($tbl_participant_place);
        $this->db->join($tbl_member_place, $tbl_member_place . '.placeId = ' . $tbl_participant_place . '.placeId', 'inner');
        $this->db->join($tbl_place, $tbl_member_place . '.placeId = ' . $tbl_place . '.id', 'inner');
        $this->db->where($tbl_member_place . '.memberId', $memberId);
        $this->db->where('participantId', $participantId);
        $query = $this->db->get();

        $result['shared_place'] = $query->result_array();


        $tbl_activity = TBL_PREFIX . 'activity';
        $tbl_participant_activity = TBL_PREFIX . 'participant_activity';
        $tbl_member_activity = TBL_PREFIX . 'member_activity';

        $this->db->select(array($tbl_activity . '.name'));
        $this->db->from($tbl_participant_activity);
        $this->db->join($tbl_member_activity, $tbl_member_activity . '.activityId = ' . $tbl_participant_activity . '.activityId', 'inner');
        $this->db->join($tbl_activity, $tbl_participant_activity . '.activityId = ' . $tbl_activity . '.id', 'inner');
        $this->db->where($tbl_member_activity . '.memberId', $memberId);
        $this->db->where('participantId', $participantId);
        $query = $this->db->get();

        $result['shared_activity'] = $query->result_array();

        return $result;
    }

    function get_member_details($memberId) {
        $tbl_member = TBL_PREFIX . 'member';
        $tbl_member_address = TBL_PREFIX . 'member_address';

        $this->db->select("CONCAT(" . $tbl_member . ".firstname,' '," . $tbl_member . ".middlename,' '," . $tbl_member . ".lastname ) as memberName");
        $this->db->select(array($tbl_member . '.id as memberId', $tbl_member_address . '.city as suburb'));
        $this->db->from($tbl_member);
        $this->db->join($tbl_member_address, $tbl_member_address . '.memberId = ' . $tbl_member . '.id', 'left');
        $this->db->where($tbl_member . '.id', $memberId);
        $query = $this->db->get();

        return $result = $query->row();
    }


    public function get_available_member_by_previous_work($memberIds, $participantId) {
        $tbl_shift_participant = TBL_PREFIX . 'shift_participant';
        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_member = TBL_PREFIX . 'shift_member';

        $this->db->select(array($tbl_shift_member . '.memberId', 'count(tbl_shift_member.memberId) as count'));
        $this->db->from($tbl_shift_member);
        $this->db->join($tbl_shift, $tbl_shift_member . '.shiftId = ' . $tbl_shift . '.id', 'inner');
        $this->db->join($tbl_shift_participant, $tbl_shift_participant . '.shiftId = ' . $tbl_shift . '.id', 'inner');

        $this->db->where_in($tbl_shift_member . '.memberId', $memberIds);
        $this->db->where(array($tbl_shift . '.status' => 6, $tbl_shift_member . '.status' => 3, $tbl_shift_participant . '.participantId' => $participantId));

        $this->db->order_by('count', 'Asc');
        $this->db->limit(1);

        $query = $this->db->get();
        $result = $query->row();


        if (!empty($result->memberId > 0 && $result->count > 0)) {
            $memberIds[] = $result->memberId;
            return $memberIds;
        } else {
            return false;
        }
    }

}
