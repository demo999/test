<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ScheduleDashboard extends MX_Controller {

    use formCustomValidation;

    function __construct() {
        parent::__construct();
        $this->load->model('Schedule_model');
        $this->load->model('Listing_model');
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;

        $this->loges->setModule(4);
        $this->loges->setSubModule(1);
    }

    public function create_shift() {
        $reqData = request_handler('create_schedule');
        $this->loges->setCreatedBy($reqData->adminId);

        $shift_data = (array) $reqData->data;
        $reqestData = $reqData->data;
//        print_r($shift_data);

        $validation_rules = array(
            array('field' => 'participant_member_ary', 'label' => 'Participant', 'rules' => 'callback_check_selected_val[' . json_encode($shift_data) . ']'),
            array('field' => 'completeAddress[]', 'label' => 'Address', 'rules' => 'callback_check_shift_address|callback_postal_code_check[postal]'),
            array('field' => 'shift_requirement', 'label' => 'Participant', 'rules' => 'callback_check_shift_requirement[' . json_encode($shift_data) . ']'),
            array('field' => 'caller_name', 'label' => 'booker firstname', 'rules' => 'required'),
            array('field' => 'caller_lastname', 'label' => 'booker lastname', 'rules' => 'required'),
            array('field' => 'caller_phone', 'label' => 'booker phone', 'rules' => 'required|callback_phone_number_check[caller_phone,required,Booking Details contact should be enter valid phone number.]'),
            array('field' => 'caller_email', 'label' => 'booker name', 'rules' => 'required|valid_email'),
            array('field' => 'start_time', 'label' => 'start date time', 'rules' => 'callback_check_shift_calendar_date_time[' . json_encode($shift_data) . ']'),
            array('field' => 'end_time', 'label' => 'end date time', 'rules' => 'required'),
            array('field' => 'booking_method', 'label' => 'booking for', 'rules' => 'required'),
            array('field' => 'confirm_with_f_name', 'label' => 'confimation firstname', 'rules' => 'required'),
            array('field' => 'confirm_with_l_name', 'label' => 'confimation lastname', 'rules' => 'required'),
            array('field' => 'confirm_with_email', 'label' => 'confimation email', 'rules' => 'required|valid_email'),
            array('field' => 'confirm_with_mobile', 'label' => 'confimation mobile number', 'rules' => 'required|callback_phone_number_check[confirm_with_mobile,required,Confimation Details contact should be enter valid phone number.]'),
            array('field' => 'push_to_app', 'label' => 'push on app', 'rules' => 'required'),
            array('field' => 'autofill_shift', 'label' => 'auto fill shift', 'rules' => 'required'),
        );

        $this->form_validation->set_data($shift_data);
        $this->form_validation->set_rules($validation_rules);


        if ($this->form_validation->run()) {

            require_once APPPATH . 'Classes/shift/Shift.php';
            $objShift = new ShiftClass\Shift();

            $objShift->setBookedBy($reqestData->booked_by);
            $objShift->setShiftDate($reqestData->start_time);
            $objShift->setStartTime($reqestData->start_time);
            $objShift->setEndTime($reqestData->end_time);

            $check_shift_exist = array();
            if ($reqestData->booked_by == 2) {
                $objShift->setShiftParticipant($reqestData->participant_member_ary[0]->name->value);
                $check_shift_exist = $objShift->check_shift_exist();

                if (!empty($check_shift_exist)) {
                    $shift_exist = array('status' => false, 'error' => 'Shift is already exist.');
                    echo json_encode($shift_exist);
                    exit();
                }
            } elseif ($reqestData->booked_by == 2) {
                #$objShift->setShiftSite($reqestData->site_lookup_ary[0]->name->value);
            }

            $result = $objShift->create_shift($reqData);

            if (!empty($result['shiftId'])) {
                $objShift->shiftCreateMail();
                // create log
                $this->loges->setTitle('New shift created : Shift Id ' . $result['shiftId']);
                $this->loges->setUserId($result['shiftId']);
                $this->loges->setDescription(json_encode($reqestData));
                $this->loges->createLog();
                $redirect = 1;
                $msg = '';
                if ($reqData->data->autofill_shift == 1 && !$result['allocation_res']) {
                    $msg = 'Autofill Shift Member not found.';
                } else if ($reqData->data->autofill_shift == 1 && $result['allocation_res']) {
                    $redirect = 2;
                } else if (isset($reqData->data->allocate_pre_member) && !empty($reqData->data->allocate_pre_member)) {
                    $redirect = 2;
                }

                $return = array('status' => true, 'shift_id' => $result['shiftId'], 'redirect_type' => $redirect, 'msg' => $msg);
            } else {
                $return = array('status' => false);
            }
        } else {
            $errors = $this->form_validation->error_array();
            $return = array('status' => false, 'error' => implode(', ', $errors));
        }
        echo json_encode($return);
        exit();
    }

    public function check_selected_val($name_ary, $param_type) {
        $return = false;

        $param_type = json_decode($param_type);
        $booked_by = $param_type->booked_by;
        if ($booked_by == 1) {
            $message = 'Please select Site Look-up.';
            if (isset($param_type->site_lookup_ary[0]->name->value)) {
                $return = true;
            }
        } else if ($booked_by == 2) {
            $message = 'Please select Participant Name.';
            if (isset($param_type->participant_member_ary[0]->name->value)) {
                $return = true;
            }
        } else {
            $message = '';
            $return = true;
        }

        $this->form_validation->set_message('check_selected_val', $message);
        return $return;
    }

    public function check_shift_address($address, $param_type) {
        if (!empty($address)) {
            if (empty($address->address)) {
                $this->form_validation->set_message('check_shift_address', 'Street can not be empty');
                return false;
            } elseif (empty((array) $address->suburb)) {
                $this->form_validation->set_message('check_shift_address', 'Suburb can not be empty');
                return false;
            } elseif (empty($address->state)) {
                $this->form_validation->set_message('check_shift_address', 'State can not be empty');
                return false;
            } elseif (empty($address->postal)) {
                $this->form_validation->set_message('check_shift_address', 'Postal can not be empty');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_shift_address', 'Address can not empty');
            return false;
        }
    }

    public function check_shift_requirement($shift_requirement, $param_type) {
        $param_type = json_decode($param_type);
        $status = false;

        if (!empty($param_type->shift_requirement)) {
            foreach ($param_type->shift_requirement as $val) {
                if (!empty($val->checked) || !empty($val->active)) {
                    $status = true;
                }
            }
        }
        if (!$status) {
            $this->form_validation->set_message('check_shift_requirement', 'Please chosse at least one shift requirement');
            return false;
        }
    }

    function check_shift_calendar_date_time($start_time, $shiftData) {

        $shiftData = json_decode($shiftData);

        if (!empty($shiftData)) {

            /* time calculation */
            $strTime = strtotime(DateFormate($shiftData->start_time, 'Y-m-d H:i:s'));
            $endTime = strtotime(DateFormate($shiftData->end_time, 'Y-m-d H:i:s'));
            $next_day_time = strtotime('+24 hours', $strTime);

            $strTimeCurrunt = strtotime(DATE_TIME);
            if (!empty($strTime)) {
                if ($strTime > $strTimeCurrunt) {
                    
                } else {
                    $this->form_validation->set_message('check_shift_calendar_date_time', 'Shift start time can not less then current time');
                    return false;
                }
            } else {
                $this->form_validation->set_message('check_shift_calendar_date_time', 'Shift start date time required can not empty');
                return false;
            }


            if (!empty($endTime)) {
                if ($endTime < $next_day_time) {
                    
                } else {
                    $this->form_validation->set_message('check_shift_calendar_date_time', 'Shift end time not greater then 24 hours');
                    return false;
                }
            } else {
                $this->form_validation->set_message('check_shift_calendar_date_time', 'Shift end date time required can not empty');
                return false;
            }

            if ($strTime > $endTime) {
                $this->form_validation->set_message('check_shift_calendar_date_time', 'Shift end time not less then start time');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_shift_calendar_date_time', 'Shift data required can not empty');
            return false;
        }
        return true;
    }

    public function get_participant_name() {
        $reqData = request_handler();
        $reqData->data = json_decode($reqData->data);
        $post_data = isset($reqData->data->query) ? $reqData->data->query : '';
        $rows = $this->Schedule_model->get_participant_name($post_data);
        echo json_encode($rows);
    }

    public function get_site_name() {
        $reqData = request_handler();
        $reqData->data = json_decode($reqData->data);
        $post_data = isset($reqData->data->query) ? $reqData->data->query : '';
        $rows = $this->Schedule_model->get_site_name($post_data);
        echo json_encode($rows);
    }

    public function get_shift_details() {
        $reqData = request_handler('access_schedule');

        require_once APPPATH . 'Classes/shift/Shift.php';
        $objShift = new ShiftClass\Shift();

        if (!empty($reqData->data)) {
            $shiftId = $reqData->data->id;

            $objShift->setShiftId($shiftId);

            // get shift details
            $result = $objShift->get_shift_details();

            // if booked by 1 mean this booking by organization so get shift relation details get form organization
            if ($result['booked_by'] == 1) {
                // get shift organization
                $result['shift_organiztion_site'] = $objShift->get_shift_oganization();
            } else {
                // get shift participant
                $result['shift_participant'] = $objShift->get_shift_participant();
            }

            // get shift location
            $result['location'] = $objShift->get_shift_location();

            if (!empty($result['location'])) {
                foreach ($result['location'] as $key => $val) {
                    $result['location'][$key]->suburb = array('value' => $val->suburb, 'label' => $val->suburb);
                }
            }

            // if status 7 that mean shift is confirmed then get confirmed member details
            if ($result['status'] == 7) {
                $result['allocated_member'] = $objShift->get_accepted_shift_member();
            } elseif ($result['status'] == 2) {
                $result['allocated_member'] = $objShift->get_allocated_member();
            } else if ($result['status'] == 5) {
                $result['cancel_data'] = $objShift->get_cancelled_details();
            } else if ($result['status'] == 4) {
                $result['rejected_data'] = $objShift->get_rejected_member();
            } else {
                $preffered_member = $objShift->get_preferred_member();
                if (!empty($preffered_member)) {
                    foreach ($preffered_member as $val) {
                        $val->select = array('value' => $val->memberId, 'label' => $val->memberName);
                    }
                }
                $result['preferred_member'] = $preffered_member;
            }

            // get shift caller
            $result['shift_caller'] = (array) $objShift->get_shift_caller();

            // get shift confirmation details
            $result['confirmation_details'] = (array) $objShift->get_shift_confirmation_details();

            // get shift requirement
            $response = $objShift->get_shift_requirement();

            $result = array_merge($response, $result);
            echo json_encode(array('status' => true, 'data' => $result));
        }
    }

    public function cancel_shift() {
        $reqData = request_handler('update_schedule');
        $this->loges->setCreatedBy($reqData->adminId);

        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            $this->form_validation->set_data((array) $reqData);

            $validation_rules = array(
                array('field' => 'shiftId', 'label' => 'shift Id', 'rules' => 'required'),
                array('field' => 'cancel_method', 'label' => 'cancel method', 'rules' => 'required'),
                array('field' => 'cancel_type', 'label' => 'who cancel', 'rules' => 'required'),
                array('field' => 'reason', 'label' => 'reason', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {

                $return = $this->Schedule_model->cancel_shift($reqData);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($return);
        }
    }

    public function get_shift_notes() {
        $reqData = request_handler('access_schedule');

        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            $result = $this->basic_model->get_record_where('shift_notes', $column = array('id', 'adminId', 'title', 'notes', "DATE_FORMAT(created,'%d/%m/%Y') as created"), $where = array('shiftId' => $reqData->shiftId, 'archive' => 0));

            echo json_encode(array('status' => true, 'data' => $result));
        }
    }

    public function create_shift_notes() {
        $reqestData = request_handler();
        $this->loges->setCreatedBy($reqestData->adminId);

        if (!empty($reqestData->data)) {
            $reqData = $reqestData->data;

            $this->form_validation->set_data((array) $reqData);

            $validation_rules = array(
                array('field' => 'shiftId', 'label' => 'shift Id', 'rules' => 'required'),
                array('field' => 'notes', 'label' => 'notes', 'rules' => 'required'),
                array('field' => 'title', 'label' => 'title', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $this->loges->setUserId($reqData->shiftId);
                $this->loges->setDescription(json_encode($reqData));

                $data = array('title' => $reqData->title, 'notes' => $reqData->notes, 'shiftId' => $reqData->shiftId, 'adminId' => $reqestData->adminId);

                if (!empty($reqData->id)) {
                    // check permission
                    check_permission($reqestData->adminId, 'update_schedule');

                    $this->loges->setTitle('Update notes: Shift Id ' . $reqData->shiftId);
                    $this->basic_model->update_records('shift_notes', $data, $where = array('id' => $reqData->id));
                } else {
                    // check permission
                    check_permission($reqestData->adminId, 'create_schedule');

                    $this->loges->setTitle('Added new notes: Shift Id ' . $reqData->shiftId);
                    $data['created'] = DATE_TIME;
                    $this->basic_model->insert_records('shift_notes', $data);
                }

                $this->loges->createLog();
                $response = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    public function update_shift_date_time() {
        $reqestData = request_handler('update_schedule');
        $this->loges->setCreatedBy($reqestData->adminId);

        if (!empty($reqestData->data)) {
            $reqData = $reqestData->data;

            $this->form_validation->set_data((array) $reqData);

            $validation_rules = array(
                array('field' => 'shiftId', 'label' => 'shift Id', 'rules' => 'required'),
                array('field' => 'start_time', 'label' => 'start date time', 'rules' => 'required'),
                array('field' => 'end_time', 'label' => 'end date time', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $this->loges->setUserId($reqData->shiftId);
                $this->loges->setDescription(json_encode($reqData));
                $this->loges->setTitle('Update shift date time: Shift Id ' . $reqData->shiftId);
                $this->loges->createLog();

                $shfit_date = date('Y-m-d H:i:s', strtotime($reqData->start_time));

                $start_time = DateFormate($reqData->start_time, 'Y-m-d H:i:s');
                $end_time = DateFormate($reqData->end_time, 'Y-m-d H:i:s');

                $where = array('id' => $reqData->shiftId);
                $data = array('shift_date' => $shfit_date, 'start_time' => $start_time, 'end_time' => $end_time);
                $this->basic_model->update_records('shift', $data, $where);

                $response = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    public function update_shift_address() {
        $reqestData = request_handler('create_schedule');

        $this->loges->setCreatedBy($reqestData->adminId);

        if (!empty($reqestData->data)) {
            $reqData = $reqestData->data;
            $location = array();

            $this->form_validation->set_data((array) $reqData);

            $validation_rules = array(
                array('field' => 'location[]', 'label' => 'Address', 'rules' => 'callback_check_shift_address|callback_postal_code_check[postal]'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                if ($reqData->location) {
                    foreach ($reqData->location as $key => $val) {

                        $val->suburb = $val->suburb->value;
                        $state = getStateById($val->state);
                        $adds = $val->address . ' ' . $val->suburb . ' ' . $state;
                        $lat_long = getLatLong($adds);

                        $location[$key] = (array) $val;

                        if (!empty($lat_long)) {
                            $location[$key] = array_merge($location[$key], $lat_long);
                        }

                        unset($location[$key]['site']);
                        $location[$key]['shiftId'] = $reqData->shiftId;
                    }


                    $this->loges->setUserId($reqData->shiftId);
                    $this->loges->setDescription(json_encode($reqData));
                    $this->loges->setTitle('Update address : Shift Id ' . $reqData->shiftId);
                    $this->loges->createLog();
                    $this->basic_model->delete_records('shift_location', $where = array('shiftId' => $reqData->shiftId));
                    $this->basic_model->insert_records('shift_location', $location, $multiple = true);
                }

                $response = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    public function update_shift_requirement() {
        $reqestData = request_handler('update_schedule');
        $this->loges->setCreatedBy($reqestData->adminId);

        if (!empty($reqestData->data)) {
            $reqData = $reqestData->data;
            $reqData->shift_requirement = $reqData->requirement;

            $this->form_validation->set_data((array) $reqData);

            $validation_rules = array(
                array('field' => 'requirement', 'label' => 'Participant', 'rules' => 'callback_check_shift_requirement[' . json_encode($reqData) . ']')
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $requirement = $reqData->requirement;

                $updated_requirement = array();

                $this->basic_model->delete_records('shift_requirements', $where = array('shiftId' => $reqData->shiftId));

                if (!empty($requirement)) {
                    foreach ($requirement as $key => $val) {
                        if ($val->active) {
                            $updated_requirement[$key]['shiftId'] = $reqData->shiftId;
                            $updated_requirement[$key]['requirementId'] = $val->id;
                        }
                    }

                    $this->loges->setUserId($reqData->shiftId);
                    $this->loges->setDescription(json_encode($reqData));
                    $this->loges->setTitle('Update shift requirement : Shift Id ' . $reqData->shiftId);
                    $this->loges->createLog();

                    $this->basic_model->insert_records('shift_requirements', $updated_requirement, $multiple = true);

                    $response = array('status' => true);
                }
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    function update_preffered_member() {
        $reqestData = request_handler('update_schedule');
        $this->loges->setCreatedBy($reqestData->adminId);

        if (!empty($reqestData->data)) {
            $reqData = $reqestData->data;
            $preffer_member = array();

            $this->basic_model->delete_records('shift_preferred_member', $where = array('shiftId' => $reqData->shiftId));
            if (!empty($reqData->preferred_members)) {
                foreach ($reqData->preferred_members as $val) {
                    if (!empty($val->select->value)) {
                        $preffer_member[] = array('memberId' => $val->select->value, 'shiftId' => $reqData->shiftId);
                    }
                }
            }
            if (!empty($preffer_member)) {
                $this->basic_model->insert_records('shift_preferred_member', $preffer_member, $multiple = true);

                $this->loges->setUserId($reqData->shiftId);
                $this->loges->setDescription(json_encode($reqData));
                $this->loges->setTitle('Update prefered member : Shift Id ' . $reqData->shiftId);
                $this->loges->createLog();
            }

            echo json_encode(array('status' => true));
        }
    }

    public function call_allocated() {
        $reqestData = request_handler('access_schedule');
        $this->loges->setCreatedBy($reqestData->adminId);

        if (!empty($reqestData->data)) {
            $shiftId = $reqestData->data->shiftId;

            $this->loges->setUserId($shiftId);
            $this->loges->setDescription(json_encode($reqestData));
            $this->loges->setTitle('Call to allocater : Shift Id ' . $shiftId);
            $this->loges->createLog();

            $data = array('confirmed_on' => DATE_TIME);
            $this->basic_model->update_records('shift_confirmation', $data, $where = array('shiftId' => $shiftId));

            echo json_encode(array('status' => true));
        }
    }

    public function call_booker() {
        $reqestData = request_handler('access_schedule');
        $this->loges->setCreatedBy($reqestData->adminId);

        if (!empty($reqestData->data)) {
            $shiftId = $reqestData->data->shiftId;

            $this->loges->setUserId($shiftId);
            $this->loges->setDescription(json_encode($reqestData));
            $this->loges->setTitle('Call to booker : Shift Id ' . $shiftId);
            $this->loges->createLog();

            $data = array('confirmed_with_allocated' => DATE_TIME);
            $this->basic_model->update_records('shift_confirmation', $data, $where = array('shiftId' => $shiftId));
            echo json_encode(array('status' => true));
        }
    }

    public function get_booking_list() {
        $reqestData = request_handler('access_schedule');
        $rows = $this->Schedule_model->get_booking_list($reqestData);
        echo json_encode(array('status' => true, 'shift_data' => $rows));
    }

    public function get_auto_fill_shift_member() {
        $reqestData = request_handler('access_schedule');
        require_once APPPATH . 'Classes/shift/Shift.php';
        $objShift = new ShiftClass\Shift();

        $pre_selected_member = array();

        if ($reqestData->data) {
            $reqData = $reqestData->data;


            if ($reqData->listShiftsIDs) {
                $listShifts = array_keys((array) $reqData->listShiftsIDs, true);

                $objShift->setShiftId($listShifts);

                // true for multiple shift id details
                $result = $objShift->get_shift_details(true);
                if (!empty($result)) {
                    foreach ($result as $key => $val) {
                        $objShift->setShiftId($val['id']);

                        // if booked by 1 mean this booking by organization so get shift relation details get form organization
                        if ($val['booked_by'] == 1) {
                            // get shift organization
                            $result[$key]['shift_organiztion_site'] = $val['shift_organiztion_site'] = $objShift->get_shift_oganization();
                        } else {
                            // get shift participant
                            $result[$key]['shift_participant'] = $val['shift_participant'] = $objShift->get_shift_participant();
                        }


                        $result[$key]['preferred_member'] = $val['preferred_member'] = $participants = $objShift->get_preferred_member();
                        $result[$key]['location'] = $val['location'] = $objShift->get_shift_location();

                        $result[$key]['available_member'] = $this->get_available_member($val, $participants, $pre_selected_member);

                        if (!empty($result[$key]['available_member']['memberId'])) {
                            $pre_selected_member[$val['shift_date']][] = $result[$key]['available_member']['memberId'];
                        }

                        $result[$key]['accepted'] = false;
                    }
                }
            }
        }
        echo json_encode(array('status' => true, 'data' => $result));
    }

    public function get_available_member($shiftDetails, $participants, $pre_selected_member) {
        require_once APPPATH . 'Classes/shift/Shift.php';
        $objShift = new ShiftClass\Shift();


        // check all available member according city
        $near_city_members = $objShift->get_available_member_by_city($shiftDetails['id'], $shiftDetails['shift_date'], $shiftDetails['start_time'], $pre_selected_member);
        // check and filter memeber accroding of minimum pay level and pay point skill  
        if (!empty($near_city_members['available_members']) && count($near_city_members['available_members']) > 0) {
            $memberPayPoint = $this->Schedule_model->get_member_allocation_by_pay_point($near_city_members['available_members'], ['support_type' => 1]);
            if (count($memberPayPoint) > 0) {
                $memberIds = array_column($memberPayPoint, 'memberId');
                $near_city_members['available_members'] = $memberIds;
            }
        }
        $available_member = $near_city_members['available_members'];
        // check here preffer member if availble then alot
        if (!empty($shiftDetails['preferred_member']) && count($available_member) > 1) {
            foreach ($shiftDetails['preferred_member'] as $val) {
//                print_r($available_member);
                if (in_array($val->memberId, $available_member)) {
                    //array_column('memberId', $available_member)
                    $available_member = array($val->memberId);
                }
            }
        }


        // check that highest match mean how many time previous work
        if (count($available_member) > 1 && $shiftDetails['booked_by'] == 2) {
            $participantIds = array_column($shiftDetails['shift_participant'], 'participantId');

            $result = $objShift->get_available_member_by_previous_work($available_member, $participantIds[0]);

            $available_member = (!empty($result)) ? $result : $available_member;
        }



        if (empty($available_member)) {
            return array('shared_activity' => [], 'shared_place' => []);
        }

        // get member details accoding to organization
        if ($shiftDetails['booked_by'] == 1) {
            // get participant ids
            $siteId = array_column($shiftDetails['shift_organiztion_site'], 'siteId');

            // get member details
            $result = (array) $objShift->get_member_details($available_member[0]);
            $result['shared_activity'] = array();
            $result['shared_place'] = array();

            return $result;
        } elseif ($shiftDetails['booked_by'] == 2 || $shiftDetails['booked_by'] == 3) { // get member details accoding to participants and location
            // get participant ids
            $participantIds = array_column($shiftDetails['shift_participant'], 'participantId');

            // if get more member then check its shared places and activity
            if (count($available_member) > 0) {
                $available_member[] = $objShift->get_available_member_by_preferences($available_member, $participantIds);
            }


            if (!empty($available_member)) {
                // get member details
                $result = (array) $objShift->get_member_details($available_member[0]);

                //get all perfered places and activity
                $shared = $objShift->get_prefference_activity_places($available_member[0], $participantIds[0]);

                $result = array_merge($result, $shared);
                $result['distance_km'] = $near_city_members['memberIdWithDistance'][$result['memberId']];

                return $result;
            }
        }
    }

    public function assign_autofill_member() {
        $reqestData = request_handler('update_schedule');
        $this->loges->setCreatedBy($reqestData->adminId);
        $memberAssign = $updateShift = array();

        if ($reqestData->data) {
            $shift_details = $reqestData->data;

            if (!empty($shift_details)) {
                foreach ($shift_details as $val) {
                    if ($val->accepted) {
                        $updateShift = array('updated' => DATE_TIME, 'status' => 2);
                        $this->basic_model->update_records('shift', $updateShift, $where = array('id' => $val->id));

                        $memberAssign[] = array('shiftId' => $val->id, 'memberId' => $val->available_member->memberId, 'status' => 1, 'created' => DATE_TIME);

                        $this->loges->setUserId($val->id);
                        $this->loges->setDescription(json_encode($val));
                        $this->loges->setTitle('Assign shift to member ' . $val->available_member->memberName . ' : Shift Id ' . $val->id);
                        $this->loges->createLog();
                    }
                }
            }

            if (!empty($memberAssign)) {
                $this->basic_model->insert_records('shift_member', $memberAssign, $multiple = true);

                echo json_encode(array('status' => true));
            } else {
                echo json_encode(array('status' => false, 'error' => 'Please assign member to at least one shift'));
            }
        }
    }

    public function get_shift_loges() {
        $reqestData = request_handler('access_schedule');
        if ($reqestData->data) {
            $reqData = $reqestData->data;
            $colowmn = array('title', "DATE_FORMAT(created, '%d/%m/%Y') as created", "DATE_FORMAT(created, '%H:%i %p') as time");
            $result = $this->basic_model->get_record_where('logs', $colowmn, $where = array('userId' => $reqData->shiftId, 'module' => 4));

            echo json_encode(array('status' => true, 'data' => $result));
        }
    }

    public function make_roster_data($data, $rosterId = false) {
        foreach ($data as $key => $weekData) {
            $dayCounter = 1;
            foreach ($weekData as $day => $multipleShift) {
                foreach ($multipleShift as $val) {
                    if ($val->is_active) {

                        $start_time = date('Y-m-d H:i:s', strtotime($val->start_time));
                        $end_time = date('Y-m-d H:i:s', strtotime($val->end_time));

                        $roster_data = array('week_day' => $dayCounter, 'start_time' => $start_time, 'end_time' => $end_time, 'week_number' => ($key + 1));

                        if (!empty($rosterId))
                            $roster_data['rosterId'] = $rosterId;

                        $rosters[] = $roster_data;
                    }
                }
                $dayCounter++;
            }
        }

        return $rosters;
    }

    public function create_roster() {
        require_once APPPATH . 'Classes/shift/Roster.php';
        $objRoster = new RosterClass\Roster();

        $reqestData = request_handler('create_schedule');
        $this->loges->setCreatedBy($reqestData->adminId);
        $this->loges->setSubModule(2);

        $this->loges->setDescription(json_encode($reqestData));
        if ($reqestData->data) {
            $reqData = $reqestData->data;

            $roster_listing = $reqData->rosterList;

            if (!empty($roster_listing)) {

                // get shift round
                $shift_round = json_encode($this->get_shift_round($roster_listing));

                $objRoster->setParticipantId($reqData->participant->value);
                $objRoster->setStart_date(DateFormate($reqData->start_date, 'Y-m-d'));
                $objRoster->setCreated(DATE_TIME);
                $objRoster->setStatus(1);
                $objRoster->setShift_round($shift_round);

                $is_default = ($reqData->is_default == 1) ? 1 : 0;
                $objRoster->setIs_default($is_default);

                if ($is_default == 1) {
                    $objRoster->setTitle($reqData->title);
                    $objRoster->setEnd_date(DateFormate($reqData->end_date, 'Y-m-d'));
                }

                // start for rollback
                $this->db->trans_begin();

                if (!empty($reqData->rosterId)) {
                    // update roster
                    $rosterId = $reqData->rosterId;
                    $objRoster->setRosterId($rosterId);
                    $objRoster->updateRoster();

                    $this->loges->setTitle('Update Roster : Roster Id ' . $rosterId);
                } else {
                    if ($objRoster->getIs_default() == 0) {
                        // old defualt roster remove if new roster is default roster
                        $this->basic_model->update_records('participant_roster', array('status' => 5, 'updated' => DATE_TIME), $where = array('participantId' => $objRoster->getParticipantId(), 'is_default' => 0));
                    }

                    // create roster
                    $rosterId = $objRoster->createRoster();

                    $objRoster->rosterCreateMail();

                    $this->loges->setTitle('Added New Roster : Roster Id ' . $rosterId);
                }

                $this->loges->setUserId($rosterId);
                $this->loges->createLog();

                // make roster data
                $roster_data = $this->make_roster_data($roster_listing, $rosterId);

                if (!empty($roster_data)) {
                    $this->db->trans_commit();

                    // first delete all data and then insert
                    $this->basic_model->delete_records('participant_roster_data', $where = array('id' => $rosterId));

                    $this->basic_model->insert_records('participant_roster_data', $roster_data, $multiple = true);

                    // check if collapse shift found
                    if (!empty($reqData->collapse_shift)) {
                        $this->resolve_collapse_shift_insert($reqData->collapse_shift, $objRoster->getParticipantId());
                    }

                    echo json_encode(array('status' => true));
                } else {
                    // for previous data remove from roster table
                    $this->db->trans_rollback();

                    echo json_encode(array('status' => false, 'error' => 'Please select at least one day'));
                }
            }
        }
    }

    function resolve_collapse_shift_insert($rosterShift, $participantId) {
        $participantInfo = $this->Schedule_model->get_participant_shift_related_information($participantId);

        if (!empty($rosterShift)) {
            foreach ($rosterShift as $key => $val) {
                $shift = array();

                $rost = $val->new;
                $rosterCancel = false;

                if ($val->is_collapse == 'true') {
                    if ($val->status == 2) {
                        if (!empty($val->old)) {
                            $this->archiveShifts($val->old);
                        }
                    } elseif (!empty($val->old)) {

                        $rosterCancel = true;
                        $this->archiveShifts($val->old);
                    }
                }


                if ($rosterCancel == false) {
                    $shift = array('booked_by' => 2, 'shift_date' => DateFormate($rost->shift_date, "Y-m-d H:i:s"), 'start_time' => concatDateTime($rost->shift_date, $rost->start_time), 'end_time' => concatDateTime($rost->shift_date, $rost->end_time), 'status' => 1, 'created' => DATE_TIME, 'updated' => DATE_TIME);

                    // create shift
                    $shiftId = $this->basic_model->insert_records('shift', $shift, $multiple = FALSE);

                    // insert participant data
                    $shift_participant = array('shiftId' => $shiftId, 'participantId' => $participantId, 'status' => 1, 'created' => DATE_TIME);
                    $this->basic_model->insert_records('shift_participant', $shift_participant, $multiple = FALSE);

                    // insert shift location
                    $shift_location = array('address' => $participantInfo->address, 'suburb' => $participantInfo->suburb, 'state' => $participantInfo->state, 'postal' => $participantInfo->postal, 'lat' => $participantInfo->lat, 'long' => $participantInfo->long, 'shiftId' => $shiftId);
                    $this->basic_model->insert_records('shift_location', $shift_location, $multiple = FALSE);

                    // insert confirmation detials
                    $shift_confirmation = array('firstname' => $participantInfo->firstname, 'lastname' => $participantInfo->lastname, 'email' => $participantInfo->email, 'phone' => $participantInfo->phone, 'confirm_by' => $participantInfo->prefer_contact, 'confirm_with' => 1, 'shiftId' => $shiftId);
                    $this->basic_model->insert_records('shift_confirmation', $shift_confirmation, $multiple = FALSE);

                    // insert shift requirement
                    $shiftRequirement = array();
                    if (!empty($participantInfo->requirement)) {
                        foreach ($participantInfo->requirement as $val) {
                            $shiftRequirement[] = array('requirementId' => $val->assistanceId, 'shiftId' => $shiftId);
                        }

                        $this->basic_model->insert_records('shift_requirements', $shiftRequirement, $multiple = true);
                    }

                    $this->basic_model->insert_records('shift_confirmation', $shift_confirmation, $multiple = FALSE);
                }
            }
        }
    }

    function create_shift_from_roster($rosterId = false, $rosterData = false, $rosterDayData = false, $totalWeek = false) {
        $shiftRoster = array();

        /*
         *  here all parameter is optional
         *  but here roster id or rosterData , rosterDayData and totlWeek is mandatory to create shift
         *  if give roster only roster id mean also want insert in database
         *  and if given data mean want shift data
         */


        // if roster id is absent mean data send manually for only get shifts
        if (!empty($rosterId)) {
            $colown = array('is_default', 'start_date', 'end_date', 'status', 'participantId', 'shift_round');
            // get roster data
            $rosterData = $this->basic_model->get_row('participant_roster', $colown, $where = array('id' => $rosterId));

            // get participant information
            $participantInfo = $this->Schedule_model->get_participant_shift_related_information($rosterData->participantId);

            // get roster data of time
            $rosterDayData = $this->Schedule_model->get_roster_data($rosterId);

            $totalWeek = json_decode($rosterData->shift_round);
        }


        $newRosterData = array();
        if (!empty($rosterDayData)) {
            foreach ($rosterDayData as $val) {
                $newRosterData[$val['week_number']][numberToDay($val['week_day'])][] = array('start_time' => $val['start_time'], 'end_time' => $val['end_time']);
            }
        }


        $shiftCreateEndDate = date('Y-m-d', strtotime(date("Y-m-d") . ' + 57 days'));

        $date_range = dateRangeBetweenDateWithWeek($rosterData->start_date, $shiftCreateEndDate, $totalWeek);

        $shifts = array();

        if (!empty($date_range)) {
            foreach ($date_range as $week_number => $week) {
                foreach ($week as $days) {
                    foreach ($days as $date => $day) {

                        if (array_key_exists($week_number, $newRosterData)) {
                            if (array_key_exists($day, $newRosterData[$week_number])) {

                                for ($i = 0; $i < count($newRosterData[$week_number][$day]); $i++) {
                                    $start_time = $newRosterData[$week_number][$day][$i]['start_time'];
                                    $end_time = $newRosterData[$week_number][$day][$i]['end_time'];

                                    $shift = array('booked_by' => 2, 'shift_date' => $date, 'start_time' => $start_time, 'end_time' => $end_time, 'status' => 1, 'created' => DATE_TIME, 'updated' => DATE_TIME);


                                    if (!empty($rosterId)) {
                                        // create shift
                                        $shiftId = $this->basic_model->insert_records('shift', $shift, $multiple = FALSE);

                                        // insert participant data
                                        $shift_participant = array('shiftId' => $shiftId, 'participantId' => $rosterData->participantId, 'status' => 1, 'created' => DATE_TIME);
                                        $this->basic_model->insert_records('shift_participant', $shift_participant, $multiple = FALSE);



                                        // insert shift location
                                        $shift_location = array('address' => $participantInfo->address, 'suburb' => $participantInfo->suburb, 'state' => $participantInfo->state, 'postal' => $participantInfo->postal, 'lat' => $participantInfo->lat, 'long' => $participantInfo->long, 'shiftId' => $shiftId);
                                        $this->basic_model->insert_records('shift_location', $shift_location, $multiple = FALSE);

                                        // insert confirmation detials
                                        $shift_confirmation = array('firstname' => $participantInfo->firstname, 'lastname' => $participantInfo->lastname, 'email' => $participantInfo->email, 'phone' => $participantInfo->phone, 'confirm_by' => $participantInfo->prefer_contact, 'confirm_with' => 1, 'shiftId' => $shiftId);

                                        $this->basic_model->insert_records('shift_confirmation', $shift_confirmation, $multiple = FALSE);
                                    } else {
                                        $shiftRoster[] = $shift;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $shiftRoster;
    }

    function archiveShifts($shifts) {
        if (!empty($shifts)) {
            foreach ($shifts as $oldShift) {
                if (!$oldShift->active) {
                    $this->basic_model->update_records('shift', array('status' => 8), $where = array('id' => $oldShift->id));
                }
            }
        }
    }

    function mappingDate($shiftDates, $singleDate) {
        $matchDateKey = array();
        if (!empty($shiftDates)) {
            foreach ($shiftDates as $key => $val) {
                if ($val == $singleDate) {
                    $matchDateKey[] = $key;
                }
            }

            return $matchDateKey;
        }
    }

    function check_shift_collapse() {
        require_once APPPATH . 'Classes/shift/Roster.php';
        $objRoster = new RosterClass\Roster();

        $reqestData = request_handler('access_schedule');
        $collapse_shifts_count = 0;

        if ($reqestData->data) {
            $reqData = $reqestData->data;

            $this->form_validation->set_data((array) $reqData);

            $validation_rules = array(
                array('field' => 'start_date', 'label' => 'start date', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                if (!empty($reqData->rosterId)) {
                    $objRoster->setRosterId($reqData->rosterId);
                }

                $objRoster->setStart_date($reqData->start_date);
                $objRoster->setParticipantId($reqData->participant->value);

                $roster = array('start_date' => $reqData->start_date, 'participantId' => $reqData->participant->value, 'status' => 1, 'created' => DATE_TIME);

                $roster['is_default'] = ($reqData->is_default == 1) ? 1 : 0;

                $objRoster->setIs_default($roster['is_default']);

                if ($roster['is_default'] == 1) {
                    $objRoster->setEnd_date($reqData->end_date);
                    $roster['end_date'] = $reqData->end_date;
                } else {
                    $roster['end_date'] = date('Y-m-d', strtotime($roster['start_date'] . ' + 56 days'));
                }

                if ($objRoster->getIs_default() == 1) {
                    $result = $objRoster->checkRosterAleadyExistThisDate();
                }

                if (!empty($result)) {

                    echo json_encode(array('status' => false, 'error' => 'In this date another roster already exist'));
                    exit();
                } else {
                    // get shift round
                    $shift_round = $this->get_shift_round($reqData->rosterList);

                    // make roster data
                    $roster_data = $this->make_roster_data($reqData->rosterList, false);

                    $new_roster_shifts = $this->create_shift_from_roster($rosterId = false, (object) $roster, $roster_data, $shift_round);
                  
                    $new_shiftDate = array_column($new_roster_shifts, 'shift_date');
                    $old_shifts = array();
                    if (!empty($new_shiftDate)) {
                        $old_shifts = $this->Schedule_model->get_previous_shifts($roster['participantId'], $new_shiftDate);
                    }

                    $collapse_shifts = array();
                    $oldShiftDates = array_column($old_shifts, 'shift_date');

                    $array_status = array(1 => 'Unfilled', 2 => 'Unconfirmed', 3 => 'Quote', 4 => 'Rejected', 5 => 'Cancelled', 6 => 'Confirmed');

                    if (!empty($new_roster_shifts)) {

                        foreach ($new_roster_shifts as $key => $val) {
                            $collapse_shifts[$key]['is_collapse'] = false;

                            $responseMatchedDate = $this->mappingDate($oldShiftDates, $val['shift_date']);

                            if (!empty($responseMatchedDate)) {
                                $indexKey = 0;
                                foreach ($responseMatchedDate as $index => $old_shift_key) {

                                    $o_str_time = $old_shifts[$old_shift_key]['start_time'];
                                    $o_end_time = $old_shifts[$old_shift_key]['end_time'];

                                    $condtion1 = (((strTime($val['start_time']) < strTime($o_str_time)) && (strTime($o_str_time) < strTime($val['end_time']))));
                                    $condtion2 = (((strTime($val['start_time']) < strTime($o_end_time)) && (strTime($o_end_time) < strTime($val['end_time']))));


                                    $condtion3 = (((strTime($o_str_time) < strTime($val['start_time'])) && (strTime($val['end_time']) < strTime($o_str_time))));
                                    $condtion4 = (((strTime($o_end_time) < strTime($val['start_time'])) && (strTime($val['end_time']) < strTime($o_end_time))));

                                    if ($condtion1 || $condtion2 || $condtion3 || $condtion4) {
                                        $collapse_shifts_count++;
                                        $collapse_shifts[$key]['is_collapse'] = true;
                                        $collapse_shifts[$key]['status'] = 2;

                                        $collapse_shifts[$key]['old'][$indexKey] = $old_shifts[$old_shift_key];
                                        $collapse_shifts[$key]['old'][$indexKey]['status'] = $array_status[$old_shifts[$old_shift_key]['status']];
                                        $collapse_shifts[$key]['old'][$indexKey]['active'] = false;
                                        $indexKey++;
                                    }
                                }
                            }

                            $collapse_shifts[$key]['new'] = $val;

                            $startTime = new DateTime($val['start_time']);
                            $endTime = new DateTime($val['end_time']);
                            $interval = $endTime->diff($startTime);
                            $difference = $interval->format('%H.%i hrs.');


                            $collapse_shifts[$key]['new']['start_time'] = $val['start_time'];
                            $collapse_shifts[$key]['new']['end_time'] = $val['end_time'];
                            $collapse_shifts[$key]['new']['shift_date'] = $val['shift_date'];
                            $collapse_shifts[$key]['new']['duration'] = $difference;
                            $collapse_shifts[$key]['new']['status'] = 'Unfilled';
                        }
                    }

                    $response = array('status' => true, 'data' => $collapse_shifts, 'count' => $collapse_shifts_count);
                }
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    function get_shift_round($rosterList) {
        $shift_round = array();
        if (!empty($rosterList)) {
            foreach ($rosterList as $key => $val) {
                $shift_round[] = $key + 1;
            }
        }
        return $shift_round;
    }

    function approve_and_deny_roster() {
        $reqestData = request_handler('update_schedule');
        $this->loges->setCreatedBy($reqestData->adminId);
        $this->loges->setSubModule(2);
        $this->load->model('Roster_model');

        $this->loges->setDescription(json_encode($reqestData));
        if ($reqestData->data) {
            $reqData = $reqestData->data;

            if ($reqData->status == 4) {

                $this->basic_model->update_records('participant_roster', array('status' => 4), $where = array('id' => $reqData->rosterId));
                $message = 'Roster reject successfully';
                $this->loges->setTitle('Roster rejected : Roster Id ' . $reqData->rosterId);
            } else {

                // check that its tempary collapse shift
                $result = $this->Roster_model->getRosterTempData($reqData->rosterId);

                if (!empty($result)) {
                    $rosterData = json_decode($result->rosterData);
                    $this->resolve_collapse_shift_insert($rosterData, $result->participantId);
                }

                $this->loges->setTitle('Roster approve : Roster Id ' . $reqData->rosterId);
                $message = 'Roster approve successfully';
                if ($result->is_default == 0) {
                    // old defualt roster remove if new roster is default roster
                    $this->basic_model->update_records('participant_roster', array('status' => 5), $where = array('participantId' => $result->participantId, 'is_default' => 0));
                }
                $this->basic_model->update_records('participant_roster', array('status' => 1, 'updated' => DATE_TIME), $where = array('id' => $reqData->rosterId));
            }

            $this->loges->setUserId($reqData->rosterId);
            $this->loges->createLog();

            echo json_encode(array('status' => true, 'message' => $message));
        }
    }

    public function get_shift_dashboard_count() {
        $reqData = request_handler('access_schedule');
        if (!empty($reqData->data)) {
            $post_data = $reqData->data;

            $data = $this->Schedule_model->get_count_shift_graph($post_data->view_type_graph);
            $data['count_fill_by_admin'] = $this->Schedule_model->get_count_filled_shift($post_data->view_type_by_admin, 'by_admin');
            $data['count_fill_by_app'] = $this->Schedule_model->get_count_filled_shift($post_data->view_type_by_app, 'by_app');

            echo json_encode(array('data' => $data, 'status' => TRUE));
            exit();
        }
    }

    public function get_key_billing_person() {
        $reqData = request_handler();

        if (!empty($reqData->data)) {
            $post_data = $reqData->data;

            if (!empty($post_data->site_lookup_ary[0]->name)) {
                $siteId = $post_data->site_lookup_ary[0]->name->value;

                $data = $this->Schedule_model->get_key_billing_persion($siteId, $post_data->confirm_type);
                echo json_encode(array('data' => $data, 'status' => TRUE));
            } else {
                echo json_encode(array('data' => 'Site not found', 'status' => false));
            }
        }
    }

}
