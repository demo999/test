<?php



defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class CrmParticipant extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
        $this->load->model('CrmParticipant_model');
        $this->load->model('Dashboard_model');
        $this->load->model('Basic_model');
        $this->loges->setModule(10);
    }

    public function list_prospective_participant()
    {
        $this->load->model('CrmParticipant_model');
        $reqData = request_handler();
        $adminId = $reqData->adminId;
        if (!empty($reqData->data)) {
            $reqData  = json_decode($reqData->data);
            $response = $this->CrmParticipant_model->prospective_participant_list($reqData, $adminId);
            echo json_encode($response);
        }
    }
    public function get_prospective_participant_details()
    {
        $this->load->model('CrmParticipant_model');
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $participantId = $reqData->data->id;
            $where         = array(
                'participantId' => $participantId
            );
            $response      = $this->CrmParticipant_model->prospective_participant_details($participantId);
            if (!empty($response)) {
                echo json_encode(array(
                    'status' => true,
                    'data' => $response
                ));
            } else {
                echo json_encode(array(
                    'status' => false,
                    'error' => 'Sorry no data found'
                ));
            }
        }
    }


    public function get_prospective_participant_ability()
    {
        $this->load->model('CrmParticipant_model');
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $participantId = $reqData->data->id;
            $where         = array(
                'participantId' => $participantId
            );
            $response      = $this->CrmParticipant_model->prospective_participant_ability($participantId);
            if (!empty($response)) {
                echo json_encode(array(
                    'status' => true,
                    'data' => $response
                ));
            } else {
                echo json_encode(array(
                    'status' => false,
                    'error' => 'Sorry no data found'
                ));
            }
        }
    }


    public function create_crm_participant()
    {
        $reqData          = request_handlerFile();
        // if (!empty($reqData->data)) {
        $participant_data = json_decode($reqData->data);
        // var_dump(($reqData->participantDetails->provide_phone_number));

        $this->loges->setCreatedBy($reqData->adminId);

        try {
            global $globalparticipant;
            require_once APPPATH . 'Classes/crm/CrmParticipant.php';
            $objparticipant = new CrmParticipantClass\CrmParticipant();
            $response       = $this->validate_participant_data((array) $participant_data->participantDetails);
            if (!empty($response['status'])) {
                // set particicpant
                $temp = $this->setParticipantvalues($objparticipant, $participant_data);
                if ($temp) {
                    $globalparticipant = $objparticipant;
                    // generate password
                    $objparticipant->genratePassword();
                    // password send to mail
                    //   $objparticipant->WelcomeMailParticipant();
                    // encrypt password
                    $objparticipant->encryptPassword();
                    //ndis no checking
                    if ($objparticipant->checkNdisNumber()) {
                        $response = array(
                            'status' => false,
                            'error' => system_msgs('ndis_exist')
                        );
                        echo json_encode($response);
                        exit;
                    }
                    // create participant
                    $participant_id = $objparticipant->AddCrmParticipant();
                    if (!$participant_id) {
                        $response = array(
                            'status' => false,
                            'error' => system_msgs('no_staff')
                        );
                        echo json_encode($response);
                        exit;
                    }
                    $participant_ability                       = (array) $participant_data->participantAbility;
                    $participant_ability['crm_participant_id'] = $participant_id;
                    // for participantability
                    if (!empty($participant_ability)) {
                        $this->CrmParticipant_model->participant_ability_disability_crm_update($participant_ability);
                    }
                    $participant_shift                       = (array) $participant_data->participantShift;
                    $participant_shift['crm_participant_id'] = $participant_id;
                    if (!empty($participant_shift)) {
                        $this->CrmParticipant_model->participant_shift($participant_shift);
                    }

                    $this->loges->setUserId($participant_id);
                    $this->loges->setDescription(json_encode($participant_data));
                    $this->loges->setTitle('Added new participant :' . trim($objparticipant->getFirstname() . ' ' . $objparticipant->getLastname()));
                    $this->loges->createLog();
                    // $assignTo=(array)$objparticipant->getAssignTo();

                    $response = array(
                        'status' => true
                    );

                } else {
                    $response = array(
                        'status' => false,
                        'error' => system_msgs('something_went_wrong')
                    );
                }
            } else {
                $response = array(
                    'status' => false,
                    'error' => $response['error']
                );
            }
        }
        catch (Exception $ex) {
            $response = array(
                'status' => false,
                'error' => $ex->getMessage()
            );
        }

        echo json_encode($response);
    }

    // set class properties values in Participant class
    function setParticipantvalues($objparticipant, $participant_data)
    {
        $participant_data = (array) $participant_data;
        try {

            $objparticipant->setFirstname($participant_data['participantDetails']->firstname);
            $objparticipant->setLastname($participant_data['participantDetails']->lastname);
            $objparticipant->setPreferredname(!empty($participant_data['participantDetails']->Preferredname) ? $participant_data['participantDetails']->Preferredname : '');
            $objparticipant->setDob($participant_data['participantDetails']->Dob);
            $objparticipant->setNdisNum(!empty($participant_data['participantDetails']->ndisno) ? $participant_data['participantDetails']->ndisno : '');
            $objparticipant->setLivingSituation($participant_data['participantDetails']->livingsituation);
            $objparticipant->setRelevantPlan($participant_data['participantDetails']->other_relevent_plans);
            $objparticipant->setBehavioural($participant_data['participantDetails']->current_behavioural);
            $objparticipant->setMaritalStatus($participant_data['participantDetails']->martialstatus);
            $objparticipant->setParticipantPlan($participant_data['participantDetails']->plan_management);
            $objparticipant->setCreated(DATE_TIME);
            $objparticipant->setStatus(1);
            // $objparticipant->setReferral(($participant_data['referral'] == 2) ? 0 : 1);
            //
            // if ($participant_data['referral'] == 1) {

            $objparticipant->setParticipantRelation($participant_data['refererDetails']->relation);
            $objparticipant->setReferralFirstName($participant_data['refererDetails']->first_name);
            $objparticipant->setReferralLastName($participant_data['refererDetails']->last_name);
            $objparticipant->setReferralEmail($participant_data['refererDetails']->email);
            $objparticipant->setReferralPhone($participant_data['refererDetails']->phone_number);
            $objparticipant->setReferralOrg($participant_data['refererDetails']->organisation);
            // }

            $objparticipant->setArchive(1);
            $objparticipant->setPortalAccess(1);
            $objparticipant->setParticipantEmail($participant_data['participantDetails']->email);
            $objparticipant->setParticipantPhone($participant_data['participantDetails']->phonenumber);
        }
        catch (Exception $e) {
            return false;
        }
        return $objparticipant;
    }

    function validate_participant_data($participant_data)
    {
        try {
            $validation_rules = array(
                // array('field' => 'assign_to', 'label' => 'Assign Department', 'rules' => 'required'),
                array(
                    'field' => 'firstname',
                    'label' => 'Firs name',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'lastname',
                    'label' => 'Last name',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'Dob',
                    'label' => 'Dob',
                    'rules' => 'required'
                )
                // array('field' => 'Preferredcontacttype', 'label' => 'Preferred contact type', 'rules' => 'required'),
                // array('field' => 'ParticipantSituation', 'label' => 'Participant Situation', 'rules' => 'required'),

            );

            $this->form_validation->set_data($participant_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $return = array(
                    'status' => true
                );
            } else {
                $errors = $this->form_validation->error_array();
                $return = array(
                    'status' => false,
                    'error' => implode(', ', $errors)
                );
            }
        }
        catch (Exception $e) {
            $return = array(
                'status' => false,
                'error' => system_msgs('something_went_wrong')
            );
        }

        return $return;
    }
    public function get_document_by_status()
    {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData  = (array) $reqData->data;
            $table    = 'crm_participant_docs';
            $column   = "*";
            $where    = array(
                "archive" => $reqData['crm_doc_status']
            );
            $response = $this->Basic_model->get_record_where($table, $column, $where);

            echo json_encode($response);
        }
    }





    public function update_crm_participant()
    {

        $request     = request_handlerFile();
        $member_data = (array) $request;
        //  var_dump($member_data);exit;
        $this->loges->setCreatedBy($request->adminId);

        if (!empty($member_data)) {
            // $participant_crm_data = (array) $reqData->data;

            $validation_rules = array(
                // array('field' => 'phones[]', 'label' => 'participant Phone', 'rules' => 'callback_check_participant_number'),
                // array('field' => 'emails[]', 'label' => 'participant Email', 'rules' => 'callback_check_participant_email_address'),
                // array('field' => 'address[]', 'label' => 'Address', 'rules' => 'callback_check_participant_address'),
                // array('field' => 'username', 'label' => 'username', 'rules' => 'callback_check_participant_username_already_exist[' . $participant_data['ocs_id'] . ']'),
                //array('field' => 'kin_detials[]', 'label' => 'Address', 'rules' => 'callback_check_kin_detials'),
                array(
                    'field' => 'firstname',
                    'label' => 'Firs name',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'lastname',
                    'label' => 'Last name',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'gender',
                    'label' => 'gender',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'dob',
                    'label' => 'dob',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'prefer_contact',
                    'label' => 'Preferred contact type',
                    'rules' => 'required'
                )
            );

            $this->form_validation->set_data($member_data);
            $this->form_validation->set_rules($validation_rules);

            // $this->loges->setUserId($participant_data['ocs_id']);
            // $this->loges->setTitle("Update profile: " . trim($participant_data['fullname']));
            $this->loges->setDescription(json_encode($member_data));
            $this->loges->createLog();

            if ($this->form_validation->run()) {
                if (!empty($_FILES)) {
                    $config['upload_path']    = CRM_UPLOAD_PATH;
                    $config['input_name']     = 'crmParticipantFiles';
                    $config['directory_name'] = $member_data['crm_participant_id'];
                    $config['allowed_types']  = 'jpg|jpeg|png|xlx|xls|doc|docx|pdf';

                    $is_upload = do_muliple_upload($config);


                    if (isset($is_upload['error'])) {
                        echo json_encode(array(
                            'status' => false,
                            'error' => strip_tags($is_upload['error'])
                        ));
                        exit();
                    } else {

                        for ($i = 0; $i < count($is_upload); $i++) {
                            // $image[$i] = $is_upload[$i]['upload_data']['file_name'];
                            $insert_ary = array(
                                'filename' => $is_upload[$i]['upload_data']['file_name'],
                                'crm_participant_id' => $member_data['crm_participant_id'],
                                'title' => $this->input->post('docsTitle'),
                                'created' => DATE_TIME,
                                'archive' => 0
                            );
                            $rows       = $this->Basic_model->insert_records('crm_participant_docs', $insert_ary, $multiple = FALSE);

                        }

                    }
                } else {
                    echo json_encode(array(
                        'status' => false,
                        'error' => 'Error in uploading file.'
                    ));
                    exit();
                }
                $this->CrmParticipant_model->participant_crm_update($member_data);
                $return = array(
                    'status' => true
                );
            } else {
                $errors = $this->form_validation->error_array();
                $return = array(
                    'status' => false,
                    'error' => implode(', ', $errors)
                );
            }
        }
        echo json_encode($return);
    }

    public function crm_inteck_info()
    {
        $reqData = request_handler();
        $this->loges->setCreatedBy($reqData->adminId);

        try {

            $requestData     = $reqData->data;
            $first_step_data = (array) json_decode($requestData);

            $intake_data = array_merge($first_step_data, (array) $requestData);


            global $globalparticipant;
            require_once APPPATH . 'Classes/crm/CrmStage.php';
            $intake = new CrmIntakeClass\CrmStage();



            $response = $this->validate_intake_data($intake_data);


            if (!empty($response['status'])) {
                // set particicpant
                $temp = $this->setIntakeValues($intake, $intake_data);
                if ($temp) {
                    // create Intake
                    $intakeID = $intake->AddIntakeInformation();





                } else {
                    $response = array(
                        'status' => false,
                        'error' => system_msgs('something_went_wrong')
                    );
                }
            } else {
                $response = array(
                    'status' => false,
                    'error' => $response['error']
                );
            }
        }
        catch (Exception $ex) {
            $response = array(
                'status' => false,
                'error' => $ex->getMessage()
            );
        }

        echo json_encode($response);

    }

    public function get_intake_percent()
    {
        $this->load->model('CrmParticipant_model');
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData  = json_decode($reqData->data);
            $response = $this->CrmParticipant_model->intake_percent($reqData->crm_participant_id);
            if (!empty($response)) {
                echo json_encode(array(
                    'status' => true,
                    'data' => $response
                ));
            } else {
                echo json_encode(array(
                    'status' => false,
                    'error' => 'Sorry no data found'
                ));
            }
        }
    }

    function setIntakeValues($intake, $intake_data)
    {
        try {

            $intake->setNote($intake_data['notes']);
            $intake->setParticipantId($intake_data['crm_participant_id']);
            $intake->setStage($intake_data['stage']);
            $intake->setArchive(1);
            $intake->setPortalAccess(1);

        }
        catch (Exception $e) {
            return false;
        }
        return $intake;
    }
    function validate_intake_data($intake_data)
    {
        try {
            $validation_rules = array(
                array(
                    'field' => 'notes',
                    'label' => 'Note',
                    'rules' => 'required'
                )

            );

            $this->form_validation->set_data($intake_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $return = array(
                    'status' => true
                );
            } else {
                $errors = $this->form_validation->error_array();
                $return = array(
                    'status' => false,
                    'error' => implode(', ', $errors)
                );
            }
        }
        catch (Exception $e) {
            $return = array(
                'status' => false,
                'error' => system_msgs('something_went_wrong')
            );
        }

        return $return;
    }
    public function uploading_crm_paricipant_docs()
    {
        $request          = request_handlerFile();
        $member_data      = (array) $request;
        $crmParticipantId = $this->input->post("crmParticipantId");
        $category         = ($this->input->post("category")) ? $this->input->post("category") : 2;
        if (!empty($member_data)) {
            $validation_rules = array(

                array(
                    'field' => 'docsTitle',
                    'label' => 'Title',
                    'rules' => 'required'
                )
            );

            $this->form_validation->set_data($member_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                if (!empty($_FILES) && $_FILES['crmParticipantFiles']['error'] == 0) {
                    $config['upload_path']    = CRM_UPLOAD_PATH;
                    $config['input_name']     = 'crmParticipantFiles';
                    $config['directory_name'] = $crmParticipantId;
                    $config['allowed_types']  = 'jpg|jpeg|png|xlx|xls|doc|docx|pdf';

                    $is_upload = do_upload($config);

                    if (isset($is_upload['error'])) {
                        echo json_encode(array(
                            'status' => false,
                            'error' => strip_tags($is_upload['error'])
                        ));
                        exit();
                    } else {
                        if ($category == 1) {
                            $insert_ary = array(
                                'filename' => $is_upload['upload_data']['file_name'],
                                'crm_participant_id' => $crmParticipantId,
                                'title' => $this->input->post('docsTitle'),
                                'created' => DATE_TIME,
                                'archive' => 0,
                                'type' => 1
                            );
                            $rows       = $this->Basic_model->insert_records('crm_participant_docs', $insert_ary, $multiple = FALSE);
                        } else {
                            $insert_ary        = array(
                                'file_path' => $is_upload['upload_data']['file_name'],
                                'crm_participant_id' => $crmParticipantId,
                                'title' => $this->input->post('docsTitle'),
                                'created' => DATE_TIME,
                                'archive' => 0,
                                "stage_id" => $category

                            );
                            $rows              = $this->Basic_model->insert_records('crm_participant_stage_docs', $insert_ary, $multiple = FALSE);
                            // for crm_participant_stage
                            $participant_stage = array(
                                "stage_id" => $category,
                                "crm_participant_id" => $crmParticipantId,
                                "crm_member_id" => $request->adminId,
                                "status" => 1
                            );
                            $this->Basic_model->insert_records('crm_participant_stage', $participant_stage, $multiple = false);
                        }
                        /*logs*/
                        $this->loges->setCreatedBy($request->adminId);
                        $this->loges->setUserId($crmParticipantId);
                        $this->loges->setDescription(json_encode($request));
                        $this->loges->setTitle('Added CRM Participant DOCS : ' . $crmParticipantId);
                        $this->loges->createLog();
                        if (!empty($rows)) {
                            echo json_encode(array(
                                'status' => true
                            ));
                            exit();
                        } else {
                            echo json_encode(array(
                                'status' => false
                            ));
                            exit();
                        }
                    }
                } else {
                    echo json_encode(array(
                        'status' => false,
                        'error' => 'Error in uploading file.'
                    ));
                    exit();
                }
                $return = array(
                    'status' => true
                );
            } else {
                $errors = $this->form_validation->error_array();
                $return = array(
                    'status' => false,
                    'error' => implode("\n", $errors)
                );
            }
            echo json_encode($return);
        }
    }
    public function update_crm_participant_ability_disability()
    {
        $request          = request_handlerFile();
        $member_data      = (array) $request;
        // var_dump($member_data);exit;
        $crmParticipantId = $this->input->post("crm_participant_id");
        if (!empty($member_data)) {
            $validation_rules = array(
                array(
                    'field' => 'cognitive_level',
                    'label' => 'Cognitive level',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'communication',
                    'label' => 'Communication',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'primary_fomal_diagnosis_desc',
                    'label' => 'Primary Fomal Diagnosis Description',
                    'rules' => 'required'
                )
                // array('field' => 'require_assistance[]', 'label' => 'Require Assistance', 'rules' => 'required'),
                // array('field' => 'require_mobility[]', 'label' => 'Require Mobility', 'rules' => 'required'),
                // array('field' => 'linguistic_diverse', 'label' => 'Linguistic Diverse', 'rules' => 'required'),
                // array('field' => 'language_interpreter', 'label' => 'Language Interpreter', 'rules' => 'required'),
                // array('field' => 'languages_spoken[]', 'label' => 'Languages Spoken', 'rules' => 'required'),
                // array('field' => 'hearing_interpreter', 'label' => 'Hearing Interpreter', 'rules' => 'required'),
                // array('field' => 'secondary_fomal_diagnosis_desc', 'label' => 'Secondary Primary Fomal Diagnosis Description', 'rules' => 'required'),
                // array('field' => 'legal_issues', 'label' => 'Legal Issues', 'rules' => 'required'),

            );

            $this->form_validation->set_data($member_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {

                $data  = array();
                $image = array();
                if (!empty($_FILES)) {
                    $config['upload_path']    = CRM_UPLOAD_PATH;
                    $config['input_name']     = 'crmParticipantFiles';
                    $config['directory_name'] = $crmParticipantId;
                    $config['allowed_types']  = 'jpg|jpeg|png|xlx|xls|doc|docx|pdf';

                    $is_upload = do_muliple_upload($config);

                    if (isset($is_upload['error'])) {
                        echo json_encode(array(
                            'status' => false,
                            'error' => strip_tags($is_upload['error'])
                        ));
                        exit();
                    } else {
                        foreach ($is_upload as $val) {
                            $data .= $val['upload_data']['file_name'] . ',';
                        }
                        for ($i = 0; $i < count($is_upload); $i++) {
                            // $image[$i] = $is_upload[$i]['upload_data']['file_name'];
                            $insert_ary = array(
                                'filename' => $is_upload[$i]['upload_data']['file_name'],
                                'crm_participant_id' => $crmParticipantId,
                                'title' => $this->input->post('docsTitle'),
                                'created' => DATE_TIME,
                                'archive' => 0
                            );
                            $rows       = $this->Basic_model->insert_records('tbl_crm_participant_docs', $insert_ary, $multiple = FALSE);
                            array_push($data, $rows);
                        }
                    }
                }


                $data     = implode(',', $data);
                $image1[] = $image;
                $status   = $this->CrmParticipant_model->participant_ability_disability_crm_update($member_data, $data);

                /*logs*/
                $this->loges->setCreatedBy($request->adminId);
                $this->loges->setUserId($crmParticipantId);
                $this->loges->setDescription(json_encode($request));
                $this->loges->setTitle('Update CRM Participant Ability/Disability Info : ' . $crmParticipantId);
                $this->loges->createLog();
                if ($status) {
                    echo json_encode(array(
                        'status' => true
                    ));
                    exit();
                } else {
                    echo json_encode(array(
                        'status' => false
                    ));
                    exit();
                }

                $return = array(
                    'status' => true
                );
            } else {
                $errors = $this->form_validation->error_array();
                $return = array(
                    'status' => false,
                    'error' => implode("\n", $errors)
                );
            }
            echo json_encode($return);
        }
        $reqData = request_handler();
        $this->loges->setCreatedBy($reqData->adminId);

        if (!empty($reqData->data)) {
            $participant_ability_crm_data = (array) $reqData->data->data; //  $data['data'];
            $validation_rules             = array(
                array(
                    'field' => 'cognitive_level',
                    'label' => 'Cognitive level',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'communication',
                    'label' => 'Communication',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'primary_fomal_diagnosis_desc',
                    'label' => 'Primary Fomal Diagnosis Description',
                    'rules' => 'required'
                )
                // array('field' => 'require_assistance[]', 'label' => 'Require Assistance', 'rules' => 'required'),
                // array('field' => 'require_mobility[]', 'label' => 'Require Mobility', 'rules' => 'required'),
                // array('field' => 'linguistic_diverse', 'label' => 'Linguistic Diverse', 'rules' => 'required'),
                // array('field' => 'language_interpreter', 'label' => 'Language Interpreter', 'rules' => 'required'),
                // array('field' => 'languages_spoken[]', 'label' => 'Languages Spoken', 'rules' => 'required'),
                // array('field' => 'hearing_interpreter', 'label' => 'Hearing Interpreter', 'rules' => 'required'),
                // array('field' => 'secondary_fomal_diagnosis_desc', 'label' => 'Secondary Primary Fomal Diagnosis Description', 'rules' => 'required'),
                // array('field' => 'legal_issues', 'label' => 'Legal Issues', 'rules' => 'required'),

            );
            $this->form_validation->set_data($participant_ability_crm_data);
            $this->form_validation->set_rules($validation_rules);
            $this->loges->setTitle('Participant Ability, Disability Updated');
            $this->loges->setDescription(json_encode($participant_ability_crm_data));
            $this->loges->createLog();
            if ($this->form_validation->run()) {
                $this->CrmParticipant_model->participant_ability_disability_crm_update($participant_ability_crm_data);
                $return = array(
                    'status' => true
                );
            } else {
                $errors = $this->form_validation->error_array();
                $return = array(
                    'status' => false,
                    'error' => implode(', ', $errors)
                );
            }
        }
        echo json_encode($return);
    }

    public function change_participant_state()
    {
        $reqData = request_handler();
        $this->loges->setCreatedBy($reqData->adminId);
        // var_dump($reqData->data);exit;
        if (!empty($reqData->data)) {
            $reqData  = json_decode($reqData->data);
            // var_dump($reqData->crm_participant_id);exit;
            $response = $this->CrmParticipant_model->set_participant_state($reqData->crm_participant_id, $reqData->state);
            if (!empty($response)) {
                echo json_encode(array(
                    'status' => true,
                    'data' => $response
                ));
            } else {
                echo json_encode(array(
                    'status' => false,
                    'error' => 'Sorry no data found'
                ));
            }
        }
    }
    public function uploading_crm_paricipant_stage_docs()
    {
        $request          = request_handlerFile();
        $member_data      = (array) $request;
        // var_dump($member_data);exit;
        $crmParticipantId = $this->input->post("crmParticipantId");
        $category         = ($this->input->post("category")) ? $this->input->post("category") : 2;
        if (!empty($member_data)) {
            $validation_rules = array(

                array(
                    'field' => 'docsTitle',
                    'label' => 'Title',
                    'rules' => 'required'
                )
            );

            $this->form_validation->set_data($member_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {

                if (!empty($_FILES)) {
                    $config['upload_path']    = CRM_UPLOAD_PATH;
                    $config['input_name']     = 'crmParticipantFiles';
                    $config['directory_name'] = $crmParticipantId;
                    $config['allowed_types']  = 'jpg|jpeg|png|xlx|xls|doc|docx|pdf';

                    $is_upload = do_muliple_upload($config);

                    if (isset($is_upload['error'])) {
                        echo json_encode(array(
                            'status' => false,
                            'error' => strip_tags($is_upload['error'])
                        ));
                        exit();
                    } else {
                        $image = array();
                        if ($category == 1) {
                            $insert_ary = array(
                                'filename' => $is_upload['upload_data']['file_name'],
                                'crm_participant_id' => $crmParticipantId,
                                'title' => $this->input->post('docsTitle'),
                                'created' => DATE_TIME,
                                'archive' => 0,
                                'type' => 1
                            );
                            $rows       = $this->Basic_model->insert_records('crm_participant_docs', $insert_ary, $multiple = FALSE);
                        } else {
                            $data = '';
                            // foreach($is_upload as $val){
                            //   $data .= $val['upload_data']['file_name'].',';
                            // }
                            for ($i = 0; $i < count($is_upload); $i++) {
                                // $image[$i] = $is_upload[$i]['upload_data']['file_name'];
                                $insert_ary = array(
                                    'file_path' => $is_upload[$i]['upload_data']['file_name'],
                                    'crm_participant_id' => $crmParticipantId,
                                    'title' => $this->input->post('docsTitle'),
                                    'created' => DATE_TIME,
                                    'archive' => 0,
                                    "stage_id" => $category
                                );
                                $rows       = $this->Basic_model->insert_records('crm_participant_stage_docs', $insert_ary, $multiple = FALSE);
                            }
                            $image1[] = $image;
                            // $insert_ary = array('file_path'=>$is_upload['upload_data']['file_name'],

                            // for crm_participant_stage
                            $participant_stage = array(
                                "stage_id" => $category,
                                "crm_participant_id" => $crmParticipantId,
                                "crm_member_id" => $request->adminId,
                                "status" => 1
                            );
                            $this->Basic_model->insert_records('crm_participant_stage', $participant_stage, $multiple = false);
                        }
                        /*logs*/
                        $this->loges->setCreatedBy($request->adminId);
                        $this->loges->setUserId($crmParticipantId);
                        $this->loges->setDescription(json_encode($request));
                        $this->loges->setTitle('Added CRM Participant DOCS : ' . $crmParticipantId);
                        $this->loges->createLog();
                        if (!empty($rows)) {
                            echo json_encode(array(
                                'status' => true
                            ));
                            exit();
                        } else {
                            echo json_encode(array(
                                'status' => false
                            ));
                            exit();
                        }
                    }
                } else {
                    echo json_encode(array(
                        'status' => false,
                        'error' => 'Error in uploading file.'
                    ));
                    exit();
                }
                $return = array(
                    'status' => true
                );
            } else {
                $errors = $this->form_validation->error_array();
                $return = array(
                    'status' => false,
                    'error' => implode("\n", $errors)
                );
            }
            echo json_encode($return);
        }
    }


    public function get_participant_stage_docs()
    {
        $this->load->model('CrmParticipant_model');
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData  = json_decode($reqData->data);
            $response = $this->CrmParticipant_model->participant_stage_docs($reqData->crm_participant_id);
            if (!empty($response)) {
                echo json_encode(array(
                    'status' => true,
                    'data' => $response
                ));
            } else {
                echo json_encode(array(
                    'status' => false,
                    'error' => 'Sorry no data found'
                ));
            }
        }
    }
    public function archive_partcipant_stage_docs()
    {
        $reqData = request_handler();
        $where   = array();
        $rows    = array();
        if (!empty($reqData->data)) {
            $reqData = json_decode($reqData->data);
            // var_dump($reqData);
            for ($i = 0; $i < count($reqData->ids); $i++) {
                $data  = array(
                    'archive' => 0
                );
                $where = array(
                    'id' => $reqData->ids[$i]
                );
                $rows  = $this->Basic_model->update_records('crm_participant_stage_docs', $data, $where);
            }
            // var_dump($where);
            if (!empty($rows)) {
                echo json_encode(array(
                    'status' => true,
                    'data' => $rows
                ));
            } else {
                echo json_encode(array(
                    'status' => false,
                    'error' => 'Sorry no data found'
                ));
            }
        }
    }

    public function download_participant_stage_docs()
    {
        $reqData   = request_handlerFile();
        $response  = array();
        $response1 = array();
        $file_name = array();
        if (!empty($reqData)) {
            $reqData = json_decode($reqData->rows);
            $table   = TBL_PREFIX . 'crm_participant_stage_docs';
            $column  = array(
                $table . ".file_path",
                $table . ".crm_participant_id",
                $table . ".file_path as name"
            );
            $this->db->select($column);
            $this->db->from($table);
            $this->db->where_in('id', $reqData->ids);
            $response = $this->db->get()->result_array();

            // $results=array();
            // $contents = array();

            $fielDoc = 'uploads/crmparticipant/' . $response[0]['crm_participant_id'] . '/' . $response[0]['file_path'];
            // header("Content-Type: 'image/jpg'");
            //header('Content-Length: ' . filesize(FCPATH.$fielDoc));
            header("Access-Control-Allow-Origin: *");
            header("Access-Control-Allow-Methods: 'PUT,GET,POST,DELETE,OPTIONS'");
            $allow_headers = "Referer,Accept,Origin,User-Agent,Content-Type";
            header("Access-Control-Allow-Headers: $allow_headers");


            header("Content-Type: application/octet-stream");
            header('Content-Disposition: attachment; filename=' . filesize(FCPATH . $fielDoc));
            $file     = fopen(FCPATH . $fielDoc, "r");
            $contents = fread($file, filesize(FCPATH . $fielDoc));
            // $results = 'data:image/jpg'  . ';base64,' . base64_encode($contents);
            // $results = utf8_encode($contents);
            //  $results = json_decode($contents);
            //$fileName = $response[0]['name'];.
            $response = array(
                'status' => true,
                'file_content' => base_url() . $fielDoc,
                'file_name' => FCPATH . $response[0]['name']
            );
            echo json_encode($response);

            exit;







            // $response['file_path'] = base_url().'/uploads/crmparticipant/'.$response[0]->crm_participant_id.'/'.$rows[0]->file_path;
            for ($i = 0; $i < count($response); $i++) {
                $doc = 'uploads/crmparticipant/' . $response[$i]['crm_participant_id'] . '/' . $response[$i]['file_path'];
                if (file_exists(FCPATH . $doc)) {

                    $response1[$i] = base_url() . $doc;
                    $file_name[$i] = $response[$i]['name'];
                    header("Content-Description: File Transfer");
                    // header("Content-Disposition: attachment; filename=$file_name[$i]");
                    header("Content-Disposition: attachment; filename=\"" . $file_name[$i] . "\"");
                    // header("Content-Type: 'application/octet-stream'");
                    // header("Content-Transfer-Encoding: binary");
                    // header('Expires: 0');
                    // header('Pragma: no-cache');
                    // header('Content-Length: ' . filesize(FCPATH.$doc));
                    // $contents[$i]=    file_get_contents(FCPATH.$doc);
                    //
                    // $contents[$i] = utf8_encode($contents[$i]);
                    // $results[$i] = json_decode($contents[$i]);
                } else {
                    $response1[$i] = '';
                    $file_name[$i] = '';
                }
            }

            $response = array(
                'status' => true,
                'file_content' => $response1,
                'file_name' => $file_name
            );

        } else {
            $response = array(
                'status' => false,
                'msg' => 'You Do not have permission'
            );

        }
        echo json_encode($response);
    }

    public function get_fms_cases()
    {
        $this->load->model('CrmParticipant_model');
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData  = ($reqData->data);
            $response = $this->CrmParticipant_model->fms_cases($reqData->crm_participant_id);

            if (!empty($response)) {
                echo json_encode(array(
                    'status' => true,
                    'data' => $response
                ));
            } else {
                echo json_encode(array(
                    'status' => false,
                    'error' => 'Sorry no data found'
                ));
            }
        }
    }

    public function get_state()
    {
        request_handler();

        // get all state as value and lable form according to fron-end requirement
        $response = $this->basic_model->get_record_where('state', $column = array(
            'name as label',
            'id as value'
        ), $where = array(
            'archive' => 0
        ));

        echo json_encode(array(
            'status' => true,
            'data' => $response
        ));
    }
}
