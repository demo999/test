<?php


defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class CrmStage extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $this->load->model('CrmStage_model');

        $this->load->model('Basic_model');
        $this->loges->setModule(10);
    }


    public function list_intake_info() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = (array) json_decode($reqData->data);
            $table='crm_participant_stage_notes';
            $column="*";
            $condition=array("stage_id like"=>$reqData['stage_id'],"crm_participant_id"=>$reqData['crm_participant_id'],"status"=>1);
            $stage_id = $this->Basic_model->get_record_where('crm_participant_stage','id',$condition);
            $response = array();
            $stage_ids = array();
            if(!empty($stage_id)){
               foreach($stage_id as $ids){
                 $stage_ids[] = $ids->id;
               }
            $all_stage_id = implode(',',$stage_ids);
            $where = "stage_id IN (".$all_stage_id.") AND crm_participant_id = ".$reqData['crm_participant_id']." AND status = 1";
            $orderby="id";
            $direction="desc";
            $response = $this->Basic_model->get_record_where_orderby($table, $column, $where, $orderby, $direction);

            }
            else{
              $response = array();
            }
            echo json_encode($response);
       }
    }
    public function notes_list_with_staff_member_and_stage_id() {
        $reqData = request_handler();

        if (!empty($reqData->data)) {
            $reqData = (array)$reqData->data;
            $response=array();
            $a = array();
            $a['notes'] = $this->CrmStage_model->get_note_record_where($reqData);
           // $a['docs'] = $this->CrmStage_model->get_docs_record_where($reqData);
            $response= array('allupdates' => $a, 'status' => true);
            echo json_encode($response);
       }
    }
    public function get_latest_stage() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = (array) json_decode($reqData->data);
            $table='crm_participant_stage';
            $column="(select name from tbl_crm_stage where id=max(stage_id)) as latest_stage_name,(select level from tbl_crm_stage where level=max(stage_id)) as latest_stage";
            $where=array("crm_participant_id"=>$reqData['crm_participant_id'],"status"=>1);
            $orderby="id";
            $direction="desc";
            $response = $this->Basic_model->get_record_where_orderby($table, $column, $where, $orderby, $direction);

            echo json_encode($response);
       }
    }
    public function get_stage_info_by_id() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = (array) json_decode($reqData->data);
            $table='crm_stage';
            $column="name";
            $where=array("id"=>$reqData['id'],"status"=>1);

            $response = $this->Basic_model->get_record_where($table, $column, $where);

            echo json_encode($response);
       }
    }
    public function get_all_stage() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = (array) json_decode($reqData->data);
            $table='crm_stage';
            $column="id as value,name as label";
            $response = $this->Basic_model->get_record_where($table, $column);

            echo json_encode($response);
       }
    }
        public function delete_intake(){
            $reqData = request_handler();
                if (!empty($reqData->data)) {

                    $reqData = (array)$reqData->data;
                    $table='crm_participant_stage_notes';
                    $where=array("id"=> $reqData['intake_id']);
                    $data=array("status"=>0);
                    $response = $this->Basic_model->update_records($table, $data, $where);
                    echo json_encode($response);
                }
        }
        public function create_intake_info(){
                $reqData = request_handler();
                $this->loges->setCreatedBy($reqData->adminId);

                try {

                    $requestData =  $reqData->data;
                    $first_step_data = (array) json_decode($requestData);

                    $intake_data = array_merge($first_step_data, (array)$requestData);


                    global $globalparticipant;
                    require_once APPPATH . 'Classes/crm/CrmStage.php';
                    $intake = new CrmIntakeClass\CrmStage();



                    $response = $this->validate_intake_data($intake_data);


                    if (!empty($response['status'])) {
                        // set particicpant
                        $temp = $this->setIntakeValues($intake, $intake_data);
                        $crm_participant_id=$intake->getParticipantid();
                        if ($temp) {
                          // for crm_participant_stage
                          $stage_id = $intake->getSatgeId();
                          $participant_stage=array("stage_id"=>$stage_id,"crm_participant_id"=>$crm_participant_id,"crm_member_id"=> $reqData->adminId,"status"=>0);
                          $insert_id = $this->basic_model->insert_records('crm_participant_stage', $participant_stage, $multiple = false);
                          $intake->setParticipantStageId($insert_id);
                            // create Intake
                        $intakeID = $intake->AddIntakeInformation();



						//Logs Added
						$stageName = $intake->getStageName($stage_id);
                        $participantName = $intake->getParticipantName($crm_participant_id);
						$this->loges->setUserId($reqData->adminId);
                        $this->loges->setDescription(json_encode('Added New Note for Stage -'.$intake_data['stage_id'].'('.$stageName.') for participant ('.$participantName.')'));
                        $this->loges->setTitle('For Participant ('.$participantName.') added a New Note for Stage '.$intake_data['stage_id']);
                        $this->loges->createLog();






                        } else {
                            $response = array('status' => false, 'error' => system_msgs('something_went_wrong'));
                        }
                    } else {
                        $response = array('status' => false, 'error' => $response['error']);
                    }
                } catch (Exception $ex) {
                    $response = array('status' => false, 'error' => $ex->getMessage());
                }

                echo json_encode($response);

            }

            function setIntakeValues($intake, $intake_data) {
                try {

                    $intake->setNote($intake_data['notes']);
                    $intake->setParticipantId($intake_data['crm_participant_id']);
                    $intake->setStageId($intake_data['stage_id']);
                    $intake->setArchive(1);
                    $intake->setPortalAccess(1);

                } catch (Exception $e) {
                    return false;
                }
                return $intake;
            }
            function validate_intake_data($intake_data) {
                try {
                    $validation_rules = array(
                        array('field' => 'notes', 'label' => 'Note', 'rules' => 'required'),
                        array('field' => 'crm_participant_id', 'label' => 'Participant ID', 'rules' => 'required'),
                        array('field' => 'stage_id', 'label' => 'Stage ID', 'rules' => 'required')
                    );

                    $this->form_validation->set_data($intake_data);
                    $this->form_validation->set_rules($validation_rules);

                    if ($this->form_validation->run()) {
                        $return = array('status' => true);
                    } else {
                        $errors = $this->form_validation->error_array();
                        $return = array('status' => false, 'error' => implode(', ', $errors));
                    }
                } catch (Exception $e) {
                    $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
                }

                return $return;
            }
            public function update_stage_status(){
                $reqData = request_handler();
                if (!empty($reqData->data)) {
                    $reqData = (array) json_decode($reqData->data,true);
                    $response = $this->CrmStage_model->update_stage_status($reqData);
                }
              echo json_encode($response);
            }
            public function get_stage_option() {
                $reqData = request_handler();
                if (!empty($reqData->data)) {
                    $reqData = (array) json_decode($reqData->data);
                    $response = $this->CrmStage_model->get_state_options($reqData);
                    echo json_encode($response);
               }
            }
}
