<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class CrmTask extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $this->load->model('CrmTask_model');
        $this->load->model('Dashboard_model');
        $this->loges->setModule(10);
    }
    public function list_task() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = json_decode($reqData->data);
            $response = $this->CrmTask_model->task_list($reqData);
            echo json_encode($response);
        }
    }
    public function archive_task() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = ($reqData->data);
            $response = $this->CrmTask_model->archive_task($reqData->task_id);
            echo json_encode($response);
        }
    }
    public function complete_task() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = ($reqData->data);
            $response = $this->CrmTask_model->complete_task($reqData->task_id);
            echo json_encode($response);
        }
    }
    public function list_sub_task() {
       $reqData = request_handler();
       if (!empty($reqData->data)) {
           $reqData = ($reqData->data);
           $response = $this->CrmTask_model->list_sub_task($reqData);
           if(!empty($response)){
             echo json_encode(array('status' => true,'data' =>$response));
           }
           else{
             echo json_encode(array('status' => false ));
           }

       }
   }
    public function get_task_priority_list(){
        $this->load->model('CrmTask_model');
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = ($reqData->data);
            $response = $this->CrmTask_model->task_priority_list($reqData);
            echo json_encode($response);
        }
    }
    public function get_participant_name() {
        $post_data = $this->input->get('query');
        $rows = $this->CrmTask_model->get_participant_name($post_data);
        echo json_encode($rows);
    }
    public function create_task(){
      $reqData = request_handler();
      $this->loges->setCreatedBy($reqData->adminId);
      $data = (array) json_decode($reqData->data);
      if($data['action'] == 2){
        $response = $this->create_sub_task($data['task_id'],$data);
      }
      else{

        try {
            $task_data = (array) json_decode($reqData->data);
            require_once APPPATH . 'Classes/crm/CrmTask.php';
            $objtask = new TaskClass\Task();
            $response = $this->validate_task_data($task_data);
            if (!empty($response['status'])) {
              $temp = $this->setTaskvalues($objtask,$task_data);
              if ($temp) {
                  $task_id = $objtask->AddTask();
                  $this->loges->setUserId($objtask->getParticipant()->assigned_to);
                  $this->loges->setDescription(json_encode($objtask));
                  $this->loges->setTitle('Added new Task :' . trim($objtask->getParticipant()->assigned_to));
                  $this->loges->createLog();

              }
              else {
                  $response = array('status' => false, 'error' => system_msgs('something_went_wrong'));
              }
            }
            else {
                $response = array('status' => false, 'error' => $response['error']);
            }
          }
          catch (Exception $ex) {
              $response = array('status' => false, 'error' => $ex->getMessage());
          }
        }

        echo json_encode($response);
      }

      function validate_task_data($task_data) {
          try {
              $validation_rules = array(

                  array('field' => 'task_name', 'label' => 'Task name', 'rules' => 'required'),
                  array('field' => 'priority', 'label' => 'Priority', 'rules' => 'required'),
                  array('field' => 'due_date', 'label' => 'Due Date', 'rules' => 'required'),
                  array('field' => 'crm_participant_id', 'label' => 'Participant', 'rules' => 'callback_check_selected_participant_val[' . json_encode($task_data) . ']'),
                  array('field' => 'relevant_task_note', 'label' => 'Task Note', 'rules' => 'required'),
                  //array('field' => 'assign_to', 'label' => 'Assign to', 'rules' => 'callback_check_selected_member_val[' . json_encode($task_data) . ']'),

              );

              $this->form_validation->set_data($task_data);
              $this->form_validation->set_rules($validation_rules);

              if ($this->form_validation->run()) {
                  $return = array('status' => true);
              } else {
                  $errors = $this->form_validation->error_array();
                  $return = array('status' => false, 'error' => implode(', ', $errors));
              }
          } catch (Exception $e) {
              $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
          }

          return $return;
      }
      public function check_selected_participant_val($name_ary, $param_type) {
          $return = false;

          $param_type = json_decode($param_type);
          $crm_participant_id = $param_type->crm_participant_id->value;
          if(empty($crm_participant_id)){
            $message = 'Participant is required';
              $return = false;
          }
          else{
            $message = '';
              $return = true;
            }
            $this->form_validation->set_message('check_selected_participant_val', $message);
            return $return;
      }
      public function check_selected_member_val($name_ary, $param_type) {
          $return = false;

          $param_type = json_decode($param_type);
          $assign_to = $param_type->assign_to->value;
          if(empty($assign_to)){
            $message = 'Assign to is required';
              $return = false;
          }
          else{
            $message = '';
              $return = true;
            }
            $this->form_validation->set_message('check_selected_participant_val', $message);
            return $return;
      }

      // set class properties values in task class
      function setTaskvalues($objtask, $task_data) {
        try {
          $objtask->setParticipant($task_data['crm_participant_id']);
          $objtask->setTaskName($task_data['task_name']);
          $objtask->setPriority($task_data['priority']);
          $objtask->setDuedate($task_data['due_date']);
          $objtask->setTasknote($task_data['relevant_task_note']);
          $objtask->setMember($task_data['assign_to']);

        }
        catch (Exception $e) {
          return false;
        }
        return $objtask;
      }
      public function create_sub_task($task_id,$data){

        try {
            $task_data =(array)$data['subTask'];
            require_once APPPATH . 'Classes/crm/CrmTask.php';
            $objtask = new TaskClass\Task();
            $response = $this->validate_sub_task_data($task_data);
            if (!empty($response['status'])) {
              $temp = $this->setSubTaskvalues($objtask,$task_data,$task_id);
              if ($temp) {
                  $task_id = $objtask->AddsubTask();
                  $this->loges->setUserId($objtask->getMember()->value);
                  $this->loges->setDescription(json_encode($objtask));
                  $this->loges->setTitle('Added new Sub Task :' . trim($objtask->getMember()->value ));
                  $this->loges->createLog();

                  if ($task_id > 0) {
                      $response = array('status' => true);
                  } else {
                      $response = array('status' => false, 'error' => system_msgs('something_went_wrong'));
                  }
              }
              else {
                  $response = array('status' => false, 'error' => system_msgs('something_went_wrong'));
              }
            }
            else {
                $response = array('status' => false, 'error' => $response['error']);
            }
          }
          catch (Exception $ex) {
              $response = array('status' => false, 'error' => $ex->getMessage());
          }

          return $response;
        }



      function getTaskinfo(){
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $task_id = $reqData->data->id;

            // get about participant details
            $result = $this->CrmTask_model->get_task_details($task_id);
            if (!empty($result)) {
              echo json_encode(array('status' => true, 'data' => $result));
            } else {
                echo json_encode(array('status' => false, 'error' => 'Sorry no data found'));
            }
        }
     }

     public function task_update() {
         $reqData = request_handler();
         $this->loges->setCreatedBy($reqData->adminId);

         if (!empty($reqData->data)) {
             $task_data = (array) $reqData->data;
             require_once APPPATH . 'Classes/crm/CrmTask.php';
             $objtask = new TaskClass\Task();
             $response = $this->validate_task_data($task_data);
             if (!empty($response['status'])) {
               $this->loges->setUserId($task_data['crm_participant_id']);
               $this->loges->setDescription(json_encode($task_data));
               $this->loges->setTitle('Updated Task :' . trim($task_data['crm_participant_id'] ));
               $this->loges->createLog();
               $this->CrmTask_model->task_update($task_data);
               $return = array('status' => true);
               } else {
                   $return = array('status' => false, 'error' => implode(', ', $response['error']));
               }
        }
        echo json_encode($return);

    }
}
