<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CrmParticipant_model extends CI_Model
{

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->load->model('Basic_model');
    }

    public function Check_UserName($objparticipant)
    {
        $username = $objparticipant->getUserName();
        $this->db->select(array(
            'count(*) as count'
        ));
        $this->db->from(TBL_PREFIX . 'participant');
        $this->db->where(array(
            'username' => $username
        ));
        //return $this->db->last_query();
        return $this->db->get()->row()->count;
    }

    public function prospective_participant_list($reqData, $loginId)
    {
        $limit      = $reqData->pageSize;
        $page       = $reqData->page;
        $sorted     = $reqData->sorted;
        $filter     = $reqData->filtered;
        $orderBy    = '';
        $direction  = '';
        $status_con = array(
            1
        );

        $src_columns = array(
            'tbl_crm_participant.id',
            'tbl_crm_participant.ndis_num',
            'concat(tbl_crm_participant.preferredname," ",tbl_crm_participant.firstname," ",tbl_crm_participant.lastname) as FullName',
            'tbl_crm_participant_phone.phone',
            'tbl_crm_participant_address.street',
            'tbl_crm_participant_address.postal',
            'tbl_crm_participant_address.state',
            'tbl_crm_participant_email.email',
            'tbl_crm_participant.updated'
        );
        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;

                if ($orderBy == 'FullName') {
                    $orderBy = 'tbl_crm_participant.firstname';
                }
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy   = 'tbl_crm_participant.id';
            $direction = 'DESC';
        }

        $where = '';
        $where = "tbl_crm_participant.archive = 0";

        $sWhere = $where;

        if (!empty($filter)) {
            // if ($filter->inactive) {
            //     $status_con = array(0, 1);
            // }

            if (!empty($filter->search)) {
                $this->db->group_start();

                for ($i = 0; $i < count($src_columns); $i++) {
                    $column_search = $src_columns[$i];
                    if (strstr($column_search, "as") !== false) {
                        $serch_column = explode(" as ", $column_search);
                        if ($serch_column[0] != 'null')
                            $this->db->or_like($serch_column[0], $filter->search);
                    } else if ($column_search != 'null') {
                        $this->db->or_like($column_search, $filter->search);
                    }
                }

                $this->db->group_end();
            }

            if (!empty($filter->filterVal)) {
                $this->db->where('tbl_crm_participant.booking_status', $filter->filterVal);
            }

            if (!empty($filter->search_by)) {
                $this->db->where('tbl_crm_participant.action_status', $filter->search_by);
            }

        }

        require_once APPPATH . 'Classes/crm/CrmRole.php';
        $permission = new classRoles\Roles();
        $role = $permission->getRole($loginId);
        $role_array = array();
        foreach($role as $value){
          array_push($role_array,$value->permission);
        }
        $select_column = array(
            'tbl_crm_participant.id',
            'tbl_crm_participant.ndis_num',
            'concat(tbl_crm_participant.preferredname," ",tbl_crm_participant.firstname," ",tbl_crm_participant.lastname) as FullName',
            'tbl_crm_participant.status',
            'tbl_crm_participant_email.email',
            'concat(tbl_crm_participant.referral_firstname," ",tbl_crm_participant.referral_lastname) as ref_fullName',
            '(CASE
            WHEN tbl_crm_participant.intake_type =1 THEN "New"
            WHEN tbl_crm_participant.intake_type =2 THEN "Rejected"
            WHEN tbl_crm_participant.intake_type =3 THEN "Renewed"
            END) as intake_type',
            'tbl_crm_participant.referral_email',
            'tbl_crm_participant.referral_phone',
            'tbl_crm_participant.referral_org',
            'tbl_crm_participant.referral_relation',
            '(select concat(firstname," ",lastname) from tbl_admin where id=tbl_crm_participant.assigned_to) as assigned_to',
            'tbl_department.name as participant_department',
            'tbl_crm_participant_phone.phone',
            'tbl_crm_participant.gender,
             tbl_crm_participant_address.street,
             tbl_crm_participant_address.city,
             tbl_crm_participant_address.postal',
            'tbl_crm_participant_address.lat',
            'tbl_crm_participant.updated',
            'tbl_crm_participant_address.long',
            'tbl_crm_participant.booking_date',
            'tbl_crm_participant.booking_status',
            'tbl_crm_participant.action_status'
        );
        $dt_query      = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from(TBL_PREFIX . 'crm_participant');

        $this->db->join('tbl_crm_participant_address', 'tbl_crm_participant_address.crm_participant_id = tbl_crm_participant.id AND tbl_crm_participant_address.primary_address = 1', 'left');
        $this->db->join('tbl_crm_participant_email', 'tbl_crm_participant_email.crm_participant_id = tbl_crm_participant.id AND tbl_crm_participant_email.primary_email = 1', 'left');
        $this->db->join('tbl_crm_participant_phone', 'tbl_crm_participant_phone.crm_participant_id = tbl_crm_participant.id AND tbl_crm_participant_phone.primary_phone = 1', 'left');
        //  $this->db->join('tbl_state', 'tbl_state.id = tbl_crm_participant_address.state', 'left');
        $this->db->join('tbl_department', 'tbl_department.id = tbl_crm_participant.oc_departments', 'left');

        $this->db->where_in('tbl_crm_participant.status', $status_con);

        if(in_array('access_crm',$role_array) && !in_array('access_crm_admin',$role_array)){
            $this->db->where_in('tbl_crm_participant.assigned_to', $loginId);
        }
        $this->db->order_by($orderBy, $direction);
        $this->db->group_by('tbl_crm_participant.id');
        $this->db->limit($limit, ($page * $limit));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        //        last_query();
        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = array();

        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $row = array();

                // if (!empty($filter->incomplete)) {
                //     $percent = get_profile_complete('PARTICIPANT', $val->id);
                //     if ($percent == 100) {
                //         continue;
                //     }
                // }

                $row['id']                = $val->id;
                $row['ndis_num']          = $val->ndis_num;
                $row['FullName']          = $val->FullName;
                $row['gender']            = isset($val->gender) && $val->gender == 1 ? 'Male' : 'Female';
                //   $row['street'] = $val->street . ', ' . $val->state . ', ' . $val->postal;
                $row['address']           = $val->street;
                $row['phone']             = $val->phone;
                $row['email']             = $val->email;
                $row['status']            = isset($val->booking_status) && $val->booking_status == 1 ? 'Pending Contact' : 'Unassigned';
                $row['long']              = $val->long;
                $row['lat']               = $val->lat;
                $row['date']              = $val->booking_date;
                $row['assigned_to']       = $val->assigned_to;
                $row['intake_type']       = $val->intake_type;
                $row['latest_stage_name'] = 'In-Progress  : ' . $this->get_latest_stage($val->id);
                $row['ref_name']          = $val->ref_fullName;
                $row['ref_email']         = $val->referral_email;
                $row['ref_phone']         = $val->referral_phone;
                $row['ref_org']           = $val->referral_org;
                $row['ref_relation']      = $val->referral_relation;
                $row['department']        = $val->participant_department;
                $updated                  = $val->updated;
                $row['updated']           = $this->get_time_ago(strtotime($updated));
                $row['action']            = isset($val->action_status) && $val->action_status == 1 ? 'Phone Screening' : 'Call';
                $dataResult[]             = $row;
            }
        }
        $return = array(
            'count' => $dt_filtered_total,
            'data' => $dataResult
        );
        return $return;
    }

    function get_time_ago($time)
    {
        $time_difference = time() - $time;

        if ($time_difference < 1) {
            return 'less than 1 second ago';
        }
        $condition = array(
            12 * 30 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'month',
            24 * 60 * 60 => 'day',
            60 * 60 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        foreach ($condition as $secs => $str) {
            $d = $time_difference / $secs;

            if ($d >= 1) {
                $t = round($d);
                return 'about ' . $t . ' ' . $str . ($t > 1 ? 's' : '') . ' ago';
            }
        }
    }

    function get_latest_stage($crm_participant_id)
    {

        $table     = 'crm_participant_stage';
        $column    = "(select name from tbl_crm_stage where id=max(stage_id)) as latest_stage_name";
        $where     = array(
            "crm_participant_id" => $crm_participant_id,
            "status" => 1
        );
        $orderby   = "id";
        $direction = "desc";
        $response  = $this->Basic_model->get_record_where_orderby($table, $column, $where, $orderby, $direction);
        if ($response) {
            $result = $response[0]->latest_stage_name;
        } else {
            $result = '';
        }
        return $result;
    }
    public function prospective_participant_details($participantId)
    {
        if (!empty($participantId)) {
            $tbl_1         = TBL_PREFIX . 'crm_participant';
            $tbl_2         = TBL_PREFIX . 'crm_participant_phone';
            $tbl_3         = TBL_PREFIX . 'crm_participant_email';
            $tbl_4         = TBL_PREFIX . 'crm_participant_address';
            $tbl_5         = TBL_PREFIX . 'department';
            $tbl_6         = TBL_PREFIX . 'crm_participant_kin';
            $tbl_7         = TBL_PREFIX . 'crm_participant_stage';
            $tbl_8         = TBL_PREFIX . 'admin';
            $select_column = array(
                $tbl_1 . '.*',
                'concat(' . $tbl_1 . '.preferredname," ",' . $tbl_1 . '.firstname," ",' . $tbl_1 . '.lastname) as FullName',
                $tbl_2 . '.phone',
                $tbl_3 . '.crm_participant_id',
                $tbl_3 . '.email',
                $tbl_3 . '.primary_email',
                'concat(' . $tbl_1 . '.referral_firstname," ",' . $tbl_1 . '.referral_lastname) as ref_fullName',
                'a1.street as primary_address',
                'a2.street as secondary_address',
                'a1.address_type  as address_primary_type',
                'a2.address_type  as secondary_address_type',
                $tbl_5 . '.name as participant_department',
                'concat(' . $tbl_6 . '.firstname," ",' . $tbl_6 . '.lastname) as kin_fullname',
                $tbl_6 . '.relation as kin_relation',
                $tbl_6 . '.phone as kin_phone',
                $tbl_6 . '.firstname as kin_firstname',
                $tbl_6 . '.lastname as kin_lastname',
                $tbl_6 . '.email as kin_email',
                $tbl_7 . '.stage_id',
                $tbl_7 . '.crm_participant_id',
                $tbl_7 . '.crm_member_id',
                $tbl_7 . '.status',
                '(select concat(firstname," ",lastname) from ' . $tbl_8 . ' where id=' . $tbl_1 . '.assigned_to) as assigned_to'


            );
            $sWhere        = array(
                $tbl_1 . '.id' => $participantId
            );
            $this->db->select($select_column);
            $this->db->from($tbl_1);
            $this->db->join($tbl_2, $tbl_2 . '.crm_participant_id = ' . $tbl_1 . '.id AND ' . $tbl_2 . '.primary_phone = 1', 'left');
            $this->db->join($tbl_3, $tbl_3 . '.crm_participant_id = ' . $tbl_1 . '.id AND ' . $tbl_3 . '.primary_email = 1', 'left');
            $this->db->join($tbl_4 . ' as a1', 'a1.crm_participant_id = ' . $tbl_1 . '.id AND a1.primary_address = 1', 'left');
            $this->db->join($tbl_4 . ' as a2', 'a2.crm_participant_id = ' . $tbl_1 . '.id AND a2.primary_address = 2', 'left');
            $this->db->join($tbl_5, $tbl_5 . '.id = ' . $tbl_1 . '.oc_departments', 'left');
            $this->db->join($tbl_6, $tbl_6 . '.crm_participant_id = ' . $tbl_1 . '.id', 'left');
            $this->db->join($tbl_7, $tbl_7 . '.crm_participant_id = ' . $tbl_1 . '.id', 'left');
            $this->db->where($sWhere);
            $query = $this->db->get();
            return $query->row_array();
        }
    }
    public function prospective_participant_ability($participantId)
    {
        if (!empty($participantId)) {
            $tbl_1 = TBL_PREFIX . 'crm_participant';
            $tbl_2 = TBL_PREFIX . 'crm_participant_ability';
            $tbl_3 = TBL_PREFIX . 'crm_participant_disability';

            $select_column = array(
                $tbl_2 . '.*',
                $tbl_3 . '.*'
            );
            $sWhere        = array(
                $tbl_1 . '.id' => $participantId
            );
            $this->db->select($select_column);
            $this->db->from($tbl_1);
            $this->db->join($tbl_2, $tbl_2 . '.crm_participant_id = ' . $tbl_1 . '.id ', 'left');
            $this->db->join($tbl_3, $tbl_3 . '.crm_participant_id = ' . $tbl_1 . '.id ', 'left');
            $this->db->where($sWhere);
            $query      = $this->db->get();
            $dataResult = array();
            if (!empty($query->result())) {
                foreach ($query->result() as $val) {
                    $row = array();

                    $row['cognitive_level']                = $val->cognitive_level;
                    $row['communication']                  = $val->communication;
                    $row['crm_participant_id']             = $val->crm_participant_id;
                    $row['docs']                           = $val->docs;
                    $row['primary_fomal_diagnosis_desc']   = $val->primary_fomal_diagnosis_desc;
                    $row['secondary_fomal_diagnosis_desc'] = $val->secondary_fomal_diagnosis_desc;
                    $row['hearing_interpreter']            = $val->hearing_interpreter;
                    $row['id']                             = $val->id;
                    $row['language_interpreter']           = $val->language_interpreter;
                    $row['languages_spoken']               = (!empty($val->languages_spoken)) ? explode(",", $val->languages_spoken) : $val->languages_spoken;
                    $row['linguistic_diverse']             = $val->linguistic_diverse;
                    $row['linked_fms_case_id']             = $val->linked_fms_case_id;
                    $row['other_relevant_information']     = $val->other_relevant_information;
                    $row['require_assistance']             = (!empty($val->require_assistance)) ? explode(",", $val->require_assistance) : $val->require_assistance;
                    $row['require_mobility']               = (!empty($val->require_mobility)) ? explode(",", $val->require_mobility) : $val->require_mobility;
                    $row['legal_issues']                   = $val->legal_issues;
                    $row['status']                         = $val->status;

                    $dataResult[] = $row;
                }

            }
            return $dataResult;

        }
    }


    public function create_intake_info($intakeInfo)
    {
        $arr_intake              = array();
        $arr_intake['firstname'] = $intakeInfo->getFirstname();
        $insert_query            = $this->db->insert(TBL_PREFIX . 'crm_participant', $arr_intake);
        $intake_id               = $this->db->insert_id();
        return $intake_id;

    }
    public function create_crm_participant($objparticipant)
    {

        $arr_participant = array();
        // $arr_participant['username'] = $objparticipant->getUserName();

        $arr_participant['firstname']      = $objparticipant->getFirstname();
        //  $arr_participant['middlename'] = $objparticipant->getMiddlename();
        $arr_participant['lastname']       = $objparticipant->getLastname();
        //$arr_participant['gender'] = $objparticipant->getGender();
        $arr_participant['dob']            = $objparticipant->getDob();
        $arr_participant['ndis_num']       = $objparticipant->getNdisNum();
        $arr_participant['medicare_num']   = $objparticipant->getMedicareNum();
        $arr_participant['action_status']  = 2;
        $arr_participant['booking_status'] = 2;
        $departmentID                      = "";
        //  $staffs=$this->basic_model->get_record_where('crm_staff_department_allocations', 'admin_id', array('allocated_department'=>$departmentID));
        //  $staff_count=count($staffs);
        $assigned_to                       = '';
        // if($staff_count<1)
        // {
        //     return false;
        // }
        //
        //  foreach( $staffs as $key=>$value){
        //    $where = "assigned_to='$value->admin_id' AND booking_status in(4,1)";
        //    $check_assign=$this->basic_model->get_record_where('crm_participant', 'id', $where);
        //
        //    if( count($check_assign)==0)
        //    {
        //
        //       $assigned_to=$value->admin_id;
        //
        //    }
        //    continue;
        //  }
        //  if( $assigned_to=='')
        //  {
        //      $arr_participant['booking_status'] =   2;
        //  }
        //  else{
        //      $arr_participant['booking_status'] =   4;
        //  }


        $arr_participant['assigned_to'] = '';
        $arr_participant['referral']    = 1;

        // if ($objparticipant->getReferral() == 1) {
        $arr_participant['referral_relation']  = $objparticipant->getParticipantRelation();
        //   $arr_participant['preferredname'] = $objparticipant->getPreferredname();
        $arr_participant['referral_firstname'] = $objparticipant->getReferralFirstName();
        $arr_participant['referral_lastname']  = $objparticipant->getReferralLastName();
        $arr_participant['referral_email']     = $objparticipant->getReferralEmail();
        $arr_participant['referral_phone']     = $objparticipant->getReferralPhone();
        $arr_participant['referral_org']       = $objparticipant->getReferralOrg();
        // }

        $arr_participant['living_situation'] = $objparticipant->getLivingSituation();
        // $arr_participant['aboriginal_tsi'] = $objparticipant->getAboriginalTsi();
        $arr_participant['oc_departments']   = $departmentID;
        $arr_participant['created']          = $objparticipant->getCreated();
        $arr_participant['status']           = $objparticipant->getStatus();
        $arr_participant['booking_date']     = date($objparticipant->getCreated());
        $arr_participant['archive']          = 0;
        // $arr_participant['houseId'] = $objparticipant->getHouseid();


        $insert_query   = $this->db->insert(TBL_PREFIX . 'crm_participant', $arr_participant);
        $participant_id = $this->db->insert_id();

        if ($participant_id > 0) {
            //Here to start insert all Emails
            $participant_email = array();
            $all_part_emails   = $objparticipant->getParticipantEmail();
            if (is_array($all_part_emails)) {
                foreach ($all_part_emails as $emails) {
                    $participant_email[] = array(
                        'crm_participant_id' => $participant_id,
                        'email' => $emails['email'],
                        'primary_email' => $emails['type']
                    );
                }
                $this->db->insert_batch(TBL_PREFIX . 'crm_participant_email', $participant_email);
            }

            //Here to start insert all Phone no.
            $participant_phone = array();
            $all_part_phone    = $objparticipant->getParticipantPhone();
            if (is_array($all_part_phone)) {
                foreach ($all_part_phone as $phone) {
                    $participant_phone[] = array(
                        'crm_participant_id' => $participant_id,
                        'phone' => $phone['phone'],
                        'primary_phone' => $phone['type']
                    );
                }
                $this->db->insert_batch(TBL_PREFIX . 'crm_participant_phone', $participant_phone);
            }
            // for crm_participant_schedule_task
            //     $participant_schedule = array('task_name' => "New participant",'crm_participant_id' => $participant_id, 'priority' => 1, 'assign_to' => $assigned_to,"parent_id"=>0,"note"=>"","task_status"=>2);
            //     $this->db->insert(TBL_PREFIX . 'crm_participant_schedule_task', $participant_schedule);
            // }
            $tbl_2    = TBL_PREFIX . 'crm_participant';
            $assigned_counts = $this->assign_participant();
            $min             = min($assigned_counts);
            $key             = array_search($min, $assigned_counts);
            $where           = array(
                $tbl_2 . ".id" => $participant_id
            );
            $data            = array(
                $tbl_2 . ".assigned_to" => $key
            );
            $res             = $this->Basic_model->update_records('crm_participant', $data, $where);

            return $participant_id;
        }
    }
    public function assign_participant()
    {
        $tbl_1    = TBL_PREFIX . 'admin';
        $tbl_2    = TBL_PREFIX . 'crm_participant';
        $tbl_3    = TBL_PREFIX . 'crm_staff';
        $dt_query = $this->db->select(array(
            $tbl_1 . '.id as ocs_id',
            $tbl_1 . '.status',
            "CONCAT(tbl_admin.firstname,' ',tbl_admin.lastname) AS name",
            "count(tbl_admin.id) as count"
        ));

        $this->db->from($tbl_3);
        $this->db->join('tbl_admin', 'tbl_crm_staff.admin_id = tbl_admin.id', 'left');
        $this->db->where($tbl_1 . '.archive=', "0");
        $this->db->where($tbl_1 . '.status=', "1");
        $this->db->group_by($tbl_1 . '.id');
        $data            = $this->db->get();
        $assigned_counts = array();
        foreach ($data->result_array() as $value) {
            $assigned_counts = array(
                $value['ocs_id'] => $value['count']
            );

        }
        return $assigned_counts;
    }

    public function participant_crm_update($participant_crm_data)
    {

        // for address update
        $this->update_participant_crm_address($participant_crm_data);

        // // update phone
        // $this->update_participant_phone($participant_data);

        // // update email
        // $this->update_participant_email($participant_data);

        // update kin detials
        // $this->update_crm_kin_details($participant_crm_data['kin_detials'], $participant_crm_data['crm_participant_id']);

        $participant_crm = array(
            'firstname' => $participant_crm_data['firstname'],
            'middlename' => $participant_crm_data['middlename'],
            'lastname' => $participant_crm_data['lastname'],
            'username' => $participant_crm_data['username'],
            'gender' => $participant_crm_data['gender'],
            'ndis_num' => $participant_crm_data['ndis_num'],
            'medicare_num' => $participant_crm_data['medicare_num'],
            'preferredname' => $participant_crm_data['preferredname'],
            'prefer_contact' => $participant_crm_data['prefer_contact'],
            'dob' => $participant_crm_data['firstname'],
            'dob' => $participant_crm_data['dob']
        );

        $this->db->where($where = array(
            'id' => $participant_crm_data['crm_participant_id']
        ));
        $this->db->update(TBL_PREFIX . 'crm_participant', $participant_crm);

        return true;
    }

    function update_crm_kin_details($participant_crm_kin, $participant_crm_id)
    {
        $previous_kin = json_decode(json_encode($this->get_participant_crm_kin($participant_crm_id)), true);
        $new_kin      = json_decode(json_encode($participant_crm_kin), true);

        $previous_kin_ids = array_column($previous_kin, 'id');

        if (!empty($new_kin)) {
            foreach ($new_kin as $key => $val) {
                if (!empty($val['id'])) {
                    if (in_array($val['id'], $previous_kin_ids)) {

                        $key = array_search($val['id'], $previous_kin_ids);

                        // those value on done action remove from array
                        unset($previous_kin[$key]);

                        $update_kin = $this->mapping_kin_array($val, $participant_crm_id);
                        $this->basic_model->update_records('crm_participant_kin', $update_kin, $where = array(
                            'id' => $val['id']
                        ));
                    }
                } else {

                    // collect new data
                    $inset_kin[] = $this->mapping_kin_array($val, $participant_crm_id);
                }
            }

            if (!empty($inset_kin)) {
                $this->basic_model->insert_records('crm_participant_kin', $inset_kin, true);
            }

            // archive those member who remove
            if (!empty($previous_kin)) {
                foreach ($previous_kin as $val) {
                    $archive_kin = array(
                        'archive' => 1
                    );
                    $this->basic_model->update_records('crm_participant_kin', $archive_kin, $where = array(
                        'id' => $val['id']
                    ));
                }
            }
        }
    }

    function mapping_kin_array($data, $participant_crm_id)
    {
        $update_kin = array(
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'relation' => $data['relation'],
            'primary_kin' => (!empty($data['primary_kin']) && ($data['primary_kin'] == 1) ? 1 : 2),
            'crm_participant_id' => $participant_crm_id
        );

        return $update_kin;
    }
    function update_participant_crm_address($participant_crm_data)
    {
        $this->db->delete(TBL_PREFIX . 'crm_participant_address', $where = array(
            'crm_participant_id' => $participant_crm_data['crm_participant_id']
        ));
        if (!empty($participant_crm_data['address'])) {
            foreach ($participant_crm_data['address'] as $val) {

                $val->city = $val->city->value;

                $state   = getStateById($val->state);
                $address = $val->street . ' ' . $val->city . ' ' . $state;

                $latLong = getLatLong($address);
                $val     = (object) array_merge((array) $latLong, (array) $val);
                unset($val->stateName);
                $val->participantId = $participant_crm_data['crm_participant_id'];
                if (empty($val->primary_address))
                    $val->primary_address = 2;

                $addrees_data[] = $val;
            }


            $this->db->insert_batch(TBL_PREFIX . 'crm_participant_address', $addrees_data);
        }
    }
    function get_participant_crm_kin($participantId)
    {
        $where  = array(
            'crm_participant_id' => $participantId,
            'archive' => 0
        );
        $colown = array(
            'id',
            'relation',
            'phone',
            'email',
            'primary_kin',
            'firstname',
            'lastname',
            "CONCAT(firstname, ' ', lastname) AS kinfullname"
        );
        return $this->basic_model->get_record_where('crm_participant_kin', $colown, $where);
    }
    public function participant_shift($data)
    {
        $start_date        = $data['start_date'];
        $shiftsvalues      = (array) $data['shiftsvalues'];
        $shift_requirement = $data['shift_requirement'];
        $participant_id    = $data['crm_participant_id'];
        $roster_data       = array(
            'participantId' => $participant_id,
            'start_date' => $start_date,
            'shift_requirement' => implode(',', $shift_requirement),
            'status' => 2,
            'archived' => 0
        );
        $this->db->insert(TBL_PREFIX . 'crm_participant_roster', $roster_data);
        $roster_id = $this->db->insert_id();
        foreach ($shiftsvalues as $key => $value) {

            $shift_data = array(
                'crmRosterId' => $roster_id,
                'day' => $key,
                'shift_type' => implode(',', $value),
                'archived' => 0
            );
            $result     = $this->db->insert(TBL_PREFIX . 'crm_participant_shift', $shift_data);
        }

    }
    public function participant_ability_disability_crm_update($data)
    {
        $require_assistance = implode(',', $data['require_assistance']);
        $require_mobility   = implode(',', $data['require_mobility']);
        $languages_spoken   = implode(',', $data['languages_spoken']);

        $participant_ability_crm = array(
            'cognitive_level' => $data['cognitive_level'],
            'communication' => $data['communication'],
            'hearing_interpreter' => $data['hearing_interpreter'],
            'language_interpreter' => $data['language_interpreter'],
            'languages_spoken' => $languages_spoken,
            'linguistic_diverse' => $data['linguistic_diverse'],
            'require_assistance' => $require_assistance,
            'require_mobility' => $require_mobility
        );
        $abiity_data             = $this->db->query("select cognitive_level from tbl_crm_participant_ability where crm_participant_id=" . $data['crm_participant_id']);
        if (!empty($abiity_data->result_array())) {
            $this->db->where($where = array(
                'crm_participant_id' => $data['crm_participant_id']
            ));
            $ability = $this->db->update(TBL_PREFIX . 'crm_participant_ability', $participant_ability_crm);

        } else {
            $participant_ability_crm['crm_participant_id'] = $data['crm_participant_id'];
            $ability                                       = $this->db->insert(TBL_PREFIX . 'crm_participant_ability', $participant_ability_crm);
        }
        $participant_disability_crm = array(
            'other_relevant_information' => $data['other_relevant_conformation'],
            'secondary_fomal_diagnosis_desc' => $data['secondary_fomal_diagnosis_desc'],
            'primary_fomal_diagnosis_desc' => $data['primary_fomal_diagnosis_desc'],
            'legal_issues' => $data['legal_issues']
        );
        $disability_data            = $this->db->query("select * from tbl_crm_participant_disability where crm_participant_id=" . $data['crm_participant_id']);
        if (!empty($disability_data->result_array())) {
            $this->db->where($where = array(
                'crm_participant_id' => $data['crm_participant_id']
            ));
            $disability = $this->db->update(TBL_PREFIX . 'crm_participant_disability', $participant_disability_crm);
        } else {
            $participant_disability_crm['crm_participant_id'] = $data['crm_participant_id'];
            $disability                                       = $this->db->insert(TBL_PREFIX . 'crm_participant_disability', $participant_disability_crm);
        }

        if ($ability && $disability)
            return true;
        else
            return false;
    }
    public function intake_percent($crmParticipantId)
    {
        $table         = TBL_PREFIX . 'crm_participant_stage';
        $column        = $table . ".stage_id";
        $tbl_2         = TBL_PREFIX . "crm_stage";
        $select_column = $tbl_2 . ".level";
        $this->db->select($select_column);
        $this->db->from($tbl_2);
        $this->db->join($table, $table . '.stage_id = ' . $tbl_2 . '.id', 'left');
        $where = array(
            $table . ".crm_participant_id" => $crmParticipantId
        );
        $this->db->where($where);
        $this->db->order_by($table . '.id', 'DESC');
        $this->db->limit(1);
        $response          = $this->db->get()->row_array();
        $response['level'] = (int) (($response['level']) / 0.09);
        return $response;
    }

    public function set_participant_state($crmParticipantId, $state)
    {
        $tbl_1         = TBL_PREFIX . 'crm_participant_stage';
        $tbl_2         = 'crm_participant';
        $tbl_3         = 'participant';
        $select_column = array(
            $tbl_1 . ".stage_id",
            $tbl_1 . ".status"
        );
        $this->db->select($select_column);
        $this->db->from($tbl_1);
        $where = array(
            $tbl_1 . ".crm_participant_id" => $crmParticipantId
        );
        $this->db->where($where);
        $this->db->order_by($tbl_1 . '.id', 'DESC');
        $this->db->limit(1);
        $response = $this->db->get()->row_array();
        $data     = array();
        if (!empty($response)) {
            switch ($state) {
                case '4':
                    if ($response['status'] == 1 && $response['stage_id'] == '11') {
                        $update = array(
                            "status" => 4
                        );
                        $result = $this->basic_model->update_records($tbl_2, $update, $where = array(
                            'id' => $crmParticipantId
                        ));
                        var_dump($result);
                        if (!empty($result)) {
                            $this->db->select('*');
                            $this->db->from(TBL_PREFIX . $tbl_2);
                            $where = array(
                                TBL_PREFIX . $tbl_2 . ".id" => $crmParticipantId
                            );
                            $this->db->where($where);
                            $objparticipant                        = $this->db->get()->row_array();
                            $objparticipant                        = (object) $objparticipant;
                            $arr_participant                       = array();
                            $arr_participant['username']           = $objparticipant->username;
                            $arr_participant['firstname']          = $objparticipant->firstname;
                            $arr_participant['middlename']         = $objparticipant->middlename;
                            $arr_participant['lastname']           = $objparticipant->lastname;
                            $arr_participant['gender']             = $objparticipant->gender;
                            $arr_participant['dob']                = $objparticipant->dob;
                            $arr_participant['ndis_num']           = $objparticipant->ndis_num;
                            $arr_participant['medicare_num']       = $objparticipant->medicare_num;
                            $arr_participant['referral']           = $objparticipant->referral;
                            $arr_participant['preferredname']      = $objparticipant->preferredname;
                            $arr_participant['relation']           = $objparticipant->relation;
                            $arr_participant['referral_firstname'] = $objparticipant->referral_firstname;
                            $arr_participant['referral_lastname']  = $objparticipant->referral_lastname;
                            $arr_participant['referral_email']     = $objparticipant->referral_email;
                            $arr_participant['referral_phone']     = $objparticipant->referral_phone;
                            $arr_participant['living_situation']   = $objparticipant->living_situation;
                            $arr_participant['aboriginal_tsi']     = $objparticipant->aboriginal_tsi;
                            $arr_participant['oc_departments']     = $objparticipant->oc_departments;
                            $arr_participant['created']            = date("Y-m-d H:i:s");
                            $arr_participant['status']             = '1';
                            $arr_participant['archive']            = 0;
                            $arr_participant['houseId']            = $objparticipant->houseId;
                            $insert_query                          = $this->db->insert(TBL_PREFIX . $tbl_3, $arr_participant);
                            $participant_id                        = $this->db->insert_id();
                            if ($participant_id > 0) {
                                $data = array(
                                    'msg' => "Participant state changed to Active"
                                );
                            } else {
                                $data = array(
                                    'msg' => "Cannot change state"
                                );
                            }
                        }
                    } else {
                        $data = array(
                            'msg' => "Cannot change state"
                        );
                    }
                    break;
                case '3':
                    $update = array(
                        "status" => 3
                    );
                    $result = $this->basic_model->update_records($tbl_2, $update, $where = array(
                        'id' => $crmParticipantId
                    ));
                    if (!empty($result)) {
                        $data = array(
                            'msg' => "Participant state changed to Rejected"
                        );
                    } else
                        $data = array(
                            'msg' => "Cannot change state"
                        );
                    break;
                case '2':
                    $update = array(
                        "status" => 2
                    );
                    $result = $this->basic_model->update_records($tbl_2, $update, $where = array(
                        'id' => $crmParticipantId
                    ));
                    if (!empty($result)) {
                        $data = array(
                            'msg' => "Participant state changed to Parked"
                        );
                    } else
                        $data = array(
                            'msg' => "Cannot change state"
                        );
                    break;
                case '1':
                    $update = array(
                        "status" => 1
                    );
                    $result = $this->basic_model->update_records($tbl_2, $update, $where = array(
                        'id' => $crmParticipantId
                    ));
                    if (!empty($result)) {
                        $data = array(
                            'msg' => "Participant state changed to Prospective"
                        );
                    } else
                        $data = array(
                            'msg' => "Cannot change state"
                        );
                    break;
                default:
                    break;
            }
            return $data;
        }
    }
    public function participant_stage_docs($crmParticipantId)
    {
        $table    = 'crm_participant_stage_docs';
        $column   = "*";
        $where    = array(
            "crm_participant_id" => $crmParticipantId,
            "archive" => 1
        );
        $response = $this->Basic_model->get_record_where($table, $column, $where);
        // var_dump($response);
        return $response;
    }

    public function fms_cases($crmParticipantemail)
    {
        $tbl_1         = TBL_PREFIX . 'participant_email';
        $tbl_2         = TBL_PREFIX . 'fms_case_against_detail';
        $tbl_3         = TBL_PREFIX . 'fms_case';
        $tbl_4         = TBL_PREFIX . 'fms_case_notes';
        $tbl_5         = TBL_PREFIX . 'fms_case_category';
        $tbl_6         = TBL_PREFIX . 'fms_case_all_category';
        $select_column = array(
            $tbl_1 . '.participantId',
            $tbl_2 . '.caseId',
            $tbl_2 . '.created',
            $tbl_4 . '.title',
            $tbl_6 . '.name'
        );
        $sWhere        = array(
            $tbl_1 . '.email' => $crmParticipantemail
        );
        $this->db->select($select_column);
        $this->db->from($tbl_1);
        $this->db->join($tbl_2, $tbl_2 . '.against_by = ' . $tbl_1 . '.participantId AND ' . $tbl_2 . '.against_category = 3', 'left');
        $this->db->join($tbl_4, $tbl_4 . '.caseId = ' . $tbl_2 . '.caseId ', 'left');
        $this->db->join($tbl_5, $tbl_5 . '.caseId = ' . $tbl_2 . '.caseId ', 'left');
        $this->db->join($tbl_6, $tbl_6 . '.id = ' . $tbl_5 . '.categoryId ', 'left');
        $this->db->where($sWhere);
        $query = $this->db->get();
        return $query->result_array();
    }


}
