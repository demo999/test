<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CrmDepartment_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_department($reqData)
    {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';
        $tbl_rec_dep = TBL_PREFIX . 'crm_department';

        $src_columns = array();

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = $tbl_rec_dep . '.id';
            $direction = 'DESC';
        }

        if (!empty($filter->search))
        {
            $this->db->group_start();
            $src_columns = array($tbl_rec_dep . ".id", $tbl_rec_dep . ".name");

            for ($i = 0; $i < count($src_columns); $i++)
            {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    $this->db->or_like($serch_column[0],$filter->search);
                } else {
                    $this->db->or_like($column_search,$filter->search);
                }
            }
            $this->db->group_end();
        }

        if (!empty($filter->filterVal)) {
            $this->db->where('tbl_crm_department.id', $filter->filterVal);
        }

        $select_column = array($tbl_rec_dep . ".id",$tbl_rec_dep . ".name",$tbl_rec_dep . ".created");
        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_rec_dep);
        $this->db->where('archive=','0');
        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        //last_query();
        $dt_filtered_total = $all_count =  $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();
        $data = [];

        if (!empty($dataResult))
        {
            foreach ($dataResult as $val)
            {
                $members_list = $this->db->query('select
                concat(tbl_admin.id,"-",tbl_crm_staff_department_allocations.allocated_department) as hcmgr_id,
                tbl_crm_department.name  as department_name,tbl_crm_department.created, concat(tbl_admin.firstname," ",tbl_admin.lastname) as FullName from tbl_crm_staff_department_allocations
                LEFT JOIN tbl_crm_department ON tbl_crm_department.id = tbl_crm_staff_department_allocations.allocated_department
                LEFT JOIN tbl_admin ON tbl_admin.id = tbl_crm_staff_department_allocations.admin_id
                WHERE  tbl_crm_staff_department_allocations.allocated_department='.$val->id)->result_array();
                $val->created = $val->created!='0000-00-00 00:00:00'?date('d/m/Y',strtotime($val->created)):'';
                $val->value = $val->id;
                $val->label = $val->name;
                $val->hcmgr_id = $val->id;
                $val->members = $members_list;

            }
            $data = array_merge(array(array('demo'=>[])),$dataResult);
            #$data = array_merge(array(array('demo'=>[])),$dataResult);
        }
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult,'all_count'=>$all_count);
        return $return;
    }

    public function get_all_department(){
        $query = $this->db->query('select id, name as deptList from tbl_crm_department');
        foreach ($query->result() as $val) {
            $row = array();
            $row['value'] = $val->id;
            $row['label'] = $val->deptList;
            $dataResult[] = $row;
          }
        $return = array('count' => empty($dataResult)? 0 :count($dataResult), 'data' => $dataResult);
        return $return;
      }


    }
