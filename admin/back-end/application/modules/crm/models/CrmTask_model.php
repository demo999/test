<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CrmTask_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    public function task_list($reqData)
    {
        $limit      = $reqData->pageSize;
        $page       = $reqData->page;
        $sorted     = $reqData->sorted;
        $filter     = $reqData->filtered;
        $orderBy    = '';
        $direction  = '';
        $status_con = array(
            1
        );

        $src_columns = array(
            'tbl_crm_participant.id',
            'tbl_crm_participant.ndis_num',
            'concat(tbl_crm_participant.preferredname," ",tbl_crm_participant.firstname," ",tbl_crm_participant.lastname) as FullName',
            'tbl_crm_participant.status'
        );
        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;

                if ($orderBy == 'FullName') {
                    $orderBy = 'tbl_crm_participant.firstname';
                }
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy   = 'tbl_crm_participant.id';
            $direction = 'DESC';
        }
        $where = '';
        $where = "tbl_crm_participant.archive = 0";
        $sWhere = $where;
        if (!empty($filter)) {

            if (!empty($filter->search)) {
                $this->db->group_start();
                for ($i = 0; $i < count($src_columns); $i++) {
                    $column_search = $src_columns[$i];
                    if (strstr($column_search, "as") !== false) {
                        $serch_column = explode(" as ", $column_search);
                        if ($serch_column[0] != 'null')
                            $this->db->or_like($serch_column[0], $filter->search);
                    } else if ($column_search != 'null') {
                        $this->db->or_like($column_search, $filter->search);
                    }
                }
                $this->db->group_end();
            }
            if (!empty($filter->filterVal)) {
                $this->db->where('tbl_crm_participant.id', $filter->filterVal);
            }

        }

        $select_column = array(
            'tbl_crm_participant.id','tbl_crm_participant.ndis_num','concat(tbl_crm_participant.preferredname," ",tbl_crm_participant.firstname," ",tbl_crm_participant.lastname) as FullName',
            'tbl_crm_participant.status','concat(tbl_crm_participant.referral_firstname," ",tbl_crm_participant.referral_lastname) as ref_fullName',
            'tbl_crm_participant.assigned_to','tbl_crm_participant.gender','tbl_crm_participant_schedule_task.id as task_id','tbl_crm_participant_schedule_task.*',
            'tbl_crm_participant.booking_date','tbl_crm_participant.booking_status','tbl_crm_participant.action_status');

        $dt_query  = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from(TBL_PREFIX . 'crm_participant_schedule_task');
        $this->db->join('tbl_crm_participant', 'tbl_crm_participant_schedule_task.crm_participant_id = tbl_crm_participant.id', 'left');
      //  $this->db->join('tbl_admin', 'tbl_admin.id = tbl_crm_participant.assigned_to', 'left');
        $this->db->where_in('tbl_crm_participant.status', $status_con);
        $this->db->where('tbl_crm_participant_schedule_task.parent_id', '0');
        $this->db->order_by($orderBy, $direction);
        $this->db->group_by('tbl_crm_participant_schedule_task.id');
        $this->db->limit($limit, ($page * $limit));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
        $dataResult = array();
        if (!empty($query->result())) {
            $row = array(); $task= array();
            foreach ($query->result() as $val) {

                $row['id']                 = $val->id;
                $row['ndis']               = $val->ndis_num;
                $row['name']               = $val->FullName;
                $row['gender']             = isset($val->gender) && $val->gender == 1 ? 'Male' : 'Female';
                $row['taskname']           = $val->task_name;
              //  $row['assigned']           = $val->task_name;
                $row['assigned']           = $val->assigned_to;
                $row['duedate']            = $val->due_date;
                $row['notes']              = $val->note;
                $row['task_id']            = $val->task_id;
                $row['crm_participant_id'] = $val->crm_participant_id;
                $row['task_status']             = isset($val->task_status) && $val->task_status == 1 ? 'Completed' : 'Assigned';
                $row['date']               = $val->booking_date;
                $row['assigned_to']        = $val->assigned_to;
                $row['priority']        = $val->priority;
              //  $row['assigned_person']        = $val->f.' '.$val->l;
                $row['action']             = '';//isset($val->action_status) && $val->action_status == 1 ? 'Phone Screening' : 'Call';
                $row['taskLists']       =  array('id'=>$val->id,'taskname'=>$val->task_name,'assigned'=>$val->assigned_to,'duedate'=>$val->due_date,'notes'=>$val->note);
                $dataResult[]              = $row;
            }
        }
        $return = array(
            'count' => $dt_filtered_total,
            'data' => $dataResult
        );
        return $return;
    }
    public function task_priority_list(){
        $this->db->select('*');
        $this->db->from(TBL_PREFIX .'crm_task_priority');
        $sWhere = array(TBL_PREFIX .'crm_task_priority.status' => '1');
        $this->db->where($sWhere);
        $query = $this->db->get();
        $dataResult = array();
        if (!empty($query)) {
            foreach ($query->result() as $val) {
                $row = array();
                $row['value'] = $val->id;
                $row['label'] = $val->name;
               // $row['code'] = $val->status;
                $dataResult[] = $row;
            }
            $data = array("status" => "true","data" => $dataResult);
        }else{
            $data = array("status" => "false");
        }
        return $data;
    }
    public function create_task($objtask){

      $arr_task = array();
      $arr_task['crm_participant_id'] = $objtask->getParticipant()->value;
      $arr_task['assign_to'] = $objtask->getParticipant()->assigned_to;
      $arr_task['priority'] = $objtask->getPriority();
      $arr_task['due_date'] = $objtask->getDuedate();
      $arr_task['task_name'] = $objtask->getTaskName();
      $arr_task['relevant_task_note'] = $objtask->getTasknote();
      $insert_query = $this->db->insert(TBL_PREFIX . 'crm_participant_schedule_task', $arr_task);
      $task_id = $this->db->insert_id();
      return $task_id;
    }


      public function get_task_details($taskId) {
        if(!empty($taskId)){
          $tbl = TBL_PREFIX . 'crm_participant_schedule_task';
          $Where = array($tbl . '.id' => $taskId);
          $this->db->select(array($tbl . '.id as task_id',$tbl.'.crm_participant_id',$tbl.'.task_name',$tbl.'.priority',$tbl.'.due_date',$tbl.'.relevant_task_note',$tbl.'.assign_to'));
          $this->db->from($tbl);
          $this->db->where($Where);
          $query = $this->db->get();
          $result = $query->row_array();
          return $result;
        }
      }
      public function task_update($task_data){
          $task_details = array('crm_participant_id'=>$task_data['crm_participant_id'],'task_name'=>$task_data['task_name'],'priority'=>$task_data['priority'],
                          'due_date'=>$task_data['due_date'],'relevant_task_note'=>$task_data['relevant_task_note'],'assign_to'=>$task_data['assign_to']);
          $this->db->where($where = array('id' => $task_data['task_id']));
          $this->db->update(TBL_PREFIX . 'crm_participant_schedule_task', $task_details);

          return true;
      }
      public function get_participant_name($post_data) {
          // $this->db->or_where("(MATCH (firstname) AGAINST ('$post_data *'))", NULL, FALSE);
         // $this->db->or_where("(MATCH (lastname) AGAINST ('$post_data *'))", NULL, FALSE);
         $this->db->or_where("firstname LIKE  '%$post_data%' ");
         $this->db->or_where("lastname LIKE  '%$post_data%' ");
          $this->db->where('archive =', 0);
          $this->db->where('status=', 1);

          $this->db->select("CONCAT(firstname,' ',lastname) as participantName");
          $this->db->select('firstname, id,assigned_to');

          $query = $this->db->get(TBL_PREFIX . 'crm_participant');
          $query->result();
          $participant_rows = array();
          if (!empty($query->result())) {
              foreach ($query->result() as $val) {
                $this->db->select("CONCAT(tbl_admin.firstname,' ',tbl_admin.lastname) as userName");
                $this->db->where('id =', $val->assigned_to);
                $query2 = $this->db->get(TBL_PREFIX . 'admin')->row();
                $participant_rows[] = array('value' => $val->id,'label' => $val->participantName,'assigned_person'=>$query2->userName,'assigned_to'=>$val->assigned_to);
                //
              }
          }
          return $participant_rows;
      }
      public function archive_task($task_id){
        $where = array();
        $rows =array();
        if (!empty($task_id)) {
              // for ($i=0; $i < count($task_id->ids); $i++) {
              $data = array('archive'=>0);
              $where =array('id'=>$task_id);
            $rows = $this->basic_model->update_records('crm_participant_schedule_task', $data, $where);
            // }
            if (!empty($rows)) {
             return (array('status' => true, 'data' => $rows));
           }
           else {
               return (array('status' => false, 'error' => 'Sorry no data found'));
           }
       }
      }
      public function complete_task($task_id){
        $where = array();
        $rows =array();
        if (!empty($task_id)) {
              // for ($i=0; $i < count($task_id->ids); $i++) {
              $data = array('task_status'=>1);
              $where =array('id'=>$task_id);
            $rows = $this->basic_model->update_records('crm_participant_schedule_task', $data, $where);
            // }
            if (!empty($rows)) {
             return (array('status' => true, 'data' => $rows));
           }
           else {
               return (array('status' => false, 'error' => 'Sorry no data found'));
           }
       }
      }
}
