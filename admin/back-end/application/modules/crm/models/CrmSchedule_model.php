<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CrmSchedule_model extends CI_Model
{

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function Check_UserName($objparticipant)
    {
        $username = $objparticipant->getUserName();
        $this->db->select(array(
            'count(*) as count'
        ));
        $this->db->from(TBL_PREFIX . 'participant');
        $this->db->where(array(
            'username' => $username
        ));
        //return $this->db->last_query();
        return $this->db->get()->row()->count;
    }


    public function schedules_calendar_data($reqData){
     $tbl = 'tbl_crm_participant_schedule_task';
     $select_column = 'COUNT(*) as count ,'.$tbl.'.due_date';
     $where_data = "task_status <> 1 ";
     $this->db->select($select_column);
     $this->db->from($tbl);
     // $this->db->where($where_data);
     $this->db->group_by($tbl.'.due_date');
     $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
     $this->db->select('COUNT(*) as completedcount ,'.$tbl.'.updated_at');
     $this->db->from($tbl);
     $where_data = "task_status = 1 ";
     $this->db->where($where_data);
     $this->db->group_by($tbl.'.updated_at');
     $completed_query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
     $dataResult = array();
     if (!empty($query->result())) {
        foreach ($query->result() as $val) {
            $row = array();
            $row['title']         = $val->count.' Due Task';
            $row['start']      = date("Y, n, j",strtotime($val->due_date));
            $row['end']      = date("Y, n, j",strtotime($val->due_date));
            $row['type']      = 'due';
            $dataResult[]         = $row;
       }
     }
     if (!empty($completed_query->result())) {
        foreach ($completed_query->result() as $val) {
            $completed = array();
            $completed['title']         = $val->completedcount.' Completed Task';
            $completed['start']      = date("Y, n, j",strtotime($val->updated_at));
            $completed['end']      = date("Y, n, j",strtotime($val->updated_at));
            $completed['type']      ='completed';
            $dataResult[]         = $completed;
       }
     }
     return $dataResult;
   }
}
