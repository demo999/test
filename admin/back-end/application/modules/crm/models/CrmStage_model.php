<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CrmStage_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function create_intake_info($intakeInfo) {
        $arr_intake = array();
        $arr_intake['crm_participant_id'] = $intakeInfo->getParticipantid();
        $arr_intake['notes'] = $intakeInfo->getNote();
        $arr_intake['stage_id'] = $intakeInfo->getParticipantStageId();
        $arr_intake['status'] = 1;
        $insert_query = $this->db->insert(TBL_PREFIX . 'crm_participant_stage_notes', $arr_intake);

        $intake_id = $this->db->insert_id();
        return $intake_id;

    }
    public function get_note_record_where($reqData)
    {
        if (!empty($reqData)) {
           $tbl_1 = TBL_PREFIX . 'crm_participant_stage';
           $tbl_2 = TBL_PREFIX . 'crm_participant_stage_notes';
           $select_column = array();
           $sWhere = array();
           if(!empty($reqData['staff_id'])){


             $staff_id = $this->db->query("select id from tbl_crm_staff where admin_id=".$reqData['staff_id'])->row_array();
             
            $sWhere[$tbl_1.'.crm_member_id'] = $staff_id['id'];
           }
          if(!empty($reqData['stage_id'])){
             $sWhere[$tbl_1.'.stage_id'] = $reqData['stage_id'];

           }
           $select_column = array($tbl_2.'.*');
            $this->db->select($select_column);
            $this->db->from($tbl_1);
            $this->db->join($tbl_2, $tbl_2.'.crm_participant_id = '.$tbl_1.'.crm_participant_id AND '.$tbl_2.'.stage_id = '.$tbl_1.'.id', 'right');

            $this->db->where($sWhere);
            $query = $this->db->get()->result_array();
            return $query;
          }

    }
    public function get_docs_record_where($reqData)
    {

        if (!empty($reqData)) {
            $tbl_1 = TBL_PREFIX . 'crm_participant_stage';
            $tbl_2 = TBL_PREFIX . 'crm_participant_stage_docs';

            $select_column = array(

                $tbl_2.'.*',
                '(select name from tbl_crm_stage where id='.$reqData['stage_id'].') as stage_name',
                '(select concat(firstname," ",lastname) from tbl_admin where id='.$reqData['staff_id'].') as member_name'


              );
            $sWhere = array($tbl_1.'.crm_member_id' => $reqData['staff_id'],$tbl_1.'.stage_id' => $reqData['stage_id']);
            $this->db->select($select_column);
            $this->db->from($tbl_1);
            $this->db->join($tbl_2, $tbl_2.'.crm_participant_id = '.$tbl_1.'.crm_participant_id AND '.$tbl_2.'.stage_id = '.$tbl_1.'.stage_id', 'right');

            $this->db->where($sWhere);
            $query = $this->db->get()->result_array();
            return $query;
          }
    }
    public function get_state_options($reqData){
      $table='crm_participant_stage';
      $column="stage_id,status";  
      $where=array("crm_participant_id"=>$reqData['participant_id']);
      $resullt = $this->Basic_model->get_record_where($table, $column, $where);
      return $resullt ;
    }

    public function update_stage_status($reqData){
       $response = array();
       $stage_where=array('stage_id'=>$reqData['stage_id'],'crm_participant_id'=>$reqData['crm_participant_id']);
       $stage_data=array('status'=>$reqData['status']);
       $crm_participant_stage="crm_participant_stage";
       $this->basic_model->update_records($crm_participant_stage, $stage_data, $stage_where);
	   if($reqData['stage_id']<11)
		  {
			$participant_where=array('id'=>$reqData['crm_participant_id']);
			$participant_data=array('booking_status'=> 1);
			$crm_participant="crm_participant";
			$this->basic_model->update_records($crm_participant, $participant_data, $participant_where);
		  }
        
      $response = array('status' => true, 'msg'=>'Updated Data Successfully');
      return $response;
  }
}
