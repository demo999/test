<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Participant_care_not_tobook_model extends CI_Model
{
    private $table = TBL_PREFIX . 'participant_care_not_tobook';

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    /**
     * Create a carer not to book record for a participant
     */
    public function create(ParticipantCareNotToBook $participantCareNotToBook)
    {
        $arr_participant_care_not_tobook = [
            'participantId' => $participantCareNotToBook->getParticipantId(),
            'gender' => $participantCareNotToBook->getGender(),
            'ethnicity' => $participantCareNotToBook->getEthnicity(),
            'religious' => $participantCareNotToBook->getReligious(),
        ];

        $this->db->insert($this->table, $arr_participant_care_not_tobook);
    }

    /**
     * Delete carers not to book for a participant id
     */
    public function delete_where(int $participantId)
    {
        $this->db->where('participantId', $participantId);
        $this->db->delete($this->table);
    }
}
