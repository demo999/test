<?php

use Admin\Auth;

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
    }

    public function index() {
        
    }

    public function check_login() {
        $this->load->helper('cookie');
        require_once APPPATH . 'Classes/admin/auth.php';
        $adminAuth = new Admin\Auth\Auth();

        $reqData = request_handler(0, 0);

        if (!empty($reqData)) {
            $this->form_validation->set_data((array) $reqData);

            $validation_rules = array(
                array('field' => 'username', 'label' => 'UserName', 'rules' => 'required'),
                array('field' => 'password', 'label' => 'First Name', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {
                $adminAuth->setUsername($reqData->username);
                $adminAuth->setPassword($reqData->password);

                $response = $adminAuth->check_auth();

                if (!empty($response['status'])) {
                    // make history of login
                    add_login_history($adminAuth->getAdminid(), $adminAuth->getOcsToken());

                    // get all permission
                    $presmission = get_all_permission($adminAuth->getAdminid());
                    $response['permission'] = $presmission;
                    $response['type'] = ($adminAuth->getAdminid() == 1) ? 1 : 2;
                }
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }


            echo json_encode($response);
        }
    }

    public function logout() {
        require_once APPPATH . 'Classes/admin/auth.php';
        $adminAuth = new Admin\Auth\Auth();

        // get request data
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $adminAuth->unsetAdminLogin($reqData->data);

            // make logout histry
            logout_login_history($reqData->adminId);
            $response = array('status' => true);
        } else {
            $response = array('status' => false);
        }
        echo json_encode($response);
    }

    public function request_reset_password() {
        $this->load->helper('email_template_helper');

        // get request data
        $reqData = request_handler(0, 0);

        if (!empty($reqData)) {
            $email = $reqData->email;
            $this->form_validation->set_data((array) $reqData);

            $validation_rules = array(
                array('field' => 'email', 'label' => 'email', 'rules' => 'required|valid_email'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {

                $where = array('email' => $email, 'primary_email' => 1);
                $result = $this->basic_model->get_row('admin_email', $column = array('adminId'), $where);

                if (!empty($result)) {
                    $where = array('id' => $result->adminId, 'archive' => 0, 'status' => 1);
                    $result = $this->basic_model->get_row('admin', $column = array('id', 'firstname', 'lastname', 'status'), $where);

                    if (empty($result)) {
                        $response = array('status' => false, 'error' => system_msgs('account_not_active'));
                    } else {
                        $rand = mt_rand(10, 100000);
                        $token = encrypt_decrypt('encrypt', $rand);

                        $where = array('id' => $result->id);
                        $this->basic_model->update_records('admin', $data = array('token' => $token), $where);

                        $userdata = array(
                            'firstname' => $result->firstname,
                            'lastname' => $result->lastname,
                            'email' => $email,
                            'url' => $this->config->item('server_url') . "reset_password/" . encrypt_decrypt('encrypt', $result->id) . '/' . $token . '/' . encrypt_decrypt('encrypt', strtotime(DATE_TIME)),
                        );

                        forgot_password_mail($userdata, $cc_email_address = null);
                        $response = array('status' => true, 'success' => system_msgs('forgot_password_send_mail_succefully'));
                    }
                } else {
                    $response = array('status' => false, 'error' => system_msgs('This_email_not_exist_oversystem'));
                }
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    public function is_password_strong($password) {
        if (preg_match('#[0-9]#', $password) && preg_match('#.*^(?=.{6,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$#', $password)) {
            
            return TRUE;
        }
        $this->form_validation->set_message('is_password_strong', 'Password must be contain alphanumeric, number and spacial character');
        return FALSE;
    }

    function reset_password() {
        require_once APPPATH . 'Classes/admin/auth.php';
        // get request data
        $reqData = request_handler(0, 0);
        $adminAuth = new Admin\Auth\Auth();

        $this->form_validation->set_data((array) $reqData);
        $validation_rules = array(
            array('field' => 'id', 'label' => 'missing something', 'rules' => 'required'),
            array('field' => 'token', 'label' => 'token', 'rules' => 'required'),
            array('field' => 'password', 'label' => 'password', 'rules' => 'required|min_length[6]|max_length[25]|callback_is_password_strong'),
        );

        // set rules form validation
        $this->form_validation->set_rules($validation_rules);

        if ($this->form_validation->run()) {
            $user_id = encrypt_decrypt('decrypt', $reqData->id);
            $adminAuth->setAdminid($user_id);
            $adminAuth->setOcsToken($reqData->token);

            $result = $adminAuth->verify_token();

            if (!empty($result)) {
                $adminAuth->setPassword($reqData->password);
                $adminAuth->reset_password();
                $response = array('status' => true, 'success' => system_msgs('password_reset_successfully'));
            } else {
                $response = array('status' => false, 'error' => system_msgs('verfiy_password_error'));
            }
        } else {
            $errors = $this->form_validation->error_array();
            $response = array('status' => false, 'error' => implode(', ', $errors));
        }

        echo json_encode($response);
    }

    public function verify_reset_password_token() {
        require_once APPPATH . 'Classes/admin/auth.php';
        // get request data
        $reqData = request_handler(0, 0);
        
        if ($reqData) {
            $this->form_validation->set_data((array) $reqData);
            $validation_rules = array(
                array('field' => 'id', 'label' => 'missing something', 'rules' => 'required'),
                array('field' => 'token', 'label' => 'token', 'rules' => 'required'),
                array('field' => 'dateTime', 'label' => 'missing something', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $user_id = encrypt_decrypt('decrypt', $reqData->id);

                $adminAuth = new Admin\Auth\Auth();
                $adminAuth->setAdminid($user_id);
                $adminAuth->setOcsToken($reqData->token);
                $recieve_date_time = encrypt_decrypt('decrypt', $reqData->dateTime);
                $diff = strtotime(DATE_TIME) - $recieve_date_time;

                if ($diff > 3600) {
                    $response = array('status' => false, 'error' => system_msgs('link_exprire'));
                } else {
                    $result = $adminAuth->verify_token();
                    if (!empty($result)) {
                        $response = array('status' => true);
                    } else {
                        $response = array('status' => false, 'error' => system_msgs('verfiy_password_error'));
                    }
                }
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    public function check_pin() {
        require_once APPPATH . 'Classes/admin/auth.php';
        $reqData = request_handler();

        if (!empty($reqData)) {

            $this->form_validation->set_data((array) $reqData->data);
            $validation_rules = array(
                array('field' => 'pinData', 'label' => 'pin', 'rules' => 'required'),
                array('field' => 'pinType', 'label' => 'token', 'rules' => 'required'),
            );
            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {

                $adminAuth = new Admin\Auth\Auth();

                $adminAuth->setPin($reqData->data->pinData);
                $adminAuth->setAdminid($reqData->adminId);
                $adminAuth->setPinType($reqData->data->pinType);
                $adminAuth->setToken($reqData->token);
                $response = $adminAuth->verfiy_pin();
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    public function verify_email_update() {
        require_once APPPATH . 'Classes/admin/auth.php';
        $reqData = request_handler('', 0, 0);

        if (!empty($reqData->token)) {
            $adminAuth = new Admin\Auth\Auth();
            $token = $reqData->token;

            $adminAuth->setToken($token);
            $data = json_decode(encrypt_decrypt('decrypt', $token));

            $auth_res = '';

            if ($data->adminId) {
                $adminAuth->setAdminid($data->adminId);
                $auth_res = $adminAuth->checkAuthToken();
            }

            if (!empty($auth_res)) {
                $adminAuth->setPrimaryEmail($data->email);
                $adminAuth->UpdatePrimaryEmail($data->adminId);

                $return = array('status' => true, 'success' => 'Your email address changed');
            } else {
                $return = array('status' => false, 'error' => 'Invalid Request');
            }
        } else {
            $return = array('status' => false, 'error' => 'Invalid Request');
        }

        echo json_encode($return);
    }

}
