<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Recruitment_applicant_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_requirement_applicants($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';
        $status_con = array(1);


        $src_columns = array('ra.id', 'ra.date_applide', 'concat(ra.preferredname," ",ra.firstname," ",ra.lastname) as FullName', 'ra.status', 'ra.lastupdate');

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'ra.id';
            $direction = 'DESC';
        }



        if (!empty($filter->search)) {
            $this->db->group_start();
            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    if ($serch_column[0] != 'null')
                        $this->db->or_like($serch_column[0], $filter->search);
                }
                else if ($column_search != 'null') {
                    $this->db->or_like($column_search, $filter->search);
                }
            }
            $this->db->group_end();
        }


        $select_column = array('ra.id', 'ra.date_applide', 'concat(ra.firstname," ",ra.middlename," ",ra.lastname) as FullName', 'ra.status', 'ra.lastupdate', 'ra_em.email', 'ra_ph.phone');

        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from('tbl_recruitment_applicant as ra');
        $this->db->join('tbl_recruitment_applicant_email as ra_em', 'ra_em.applicant_id = ra.id AND ra_em.primary_email = 1', 'left');
        $this->db->join('tbl_recruitment_applicant_phone as ra_ph', 'ra_ph.applicant_id = ra.id AND ra_ph.primary_phone = 1', 'left');

//        $this->db->where_in('pr.status', $status_con);
        $this->db->where_in('ra.archive', 0);

        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $return = array('count' => $dt_filtered_total, 'data' => $query->result());
        return $return;
    }

}
