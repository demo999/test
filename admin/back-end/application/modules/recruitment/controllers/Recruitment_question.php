<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class Recruitment_question extends MX_Controller {

    function __construct() {

        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $this->loges->setModule(2);
    }

    public function insert_update_question() {

        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            $response_valid = $this->validate_question_data((array) $reqData);
            if ($response_valid['status']) {

                require_once APPPATH . 'Classes/recruitment/Questions.php';
                $objQuestions = new QuestionsClass\Questions();
                if ($reqData->mode == 'add') {
                    $objQuestions->setId(0);
                } else {
                    $objQuestions->setId($reqData->question_id);
                }
                $objQuestions->setQuestion($reqData->question);
                $objQuestions->setStatus($reqData->question_status);
                $objQuestions->setCreated(DATE_TIME);
                $objQuestions->setCreated_by(1);
                $objQuestions->setQuestionTopic($reqData->question_topic);
                $objQuestions->setTrainingCategory($reqData->question_category);
                $objQuestions->setQuestion_Type($reqData->answer_type);
                $objQuestions->setAnswer($reqData->answers);
                $response = $objQuestions->create_Questions();
                echo json_encode(array('status' => true, 'data' => $response));
            } else {
                echo json_encode($response_valid);
            }
        } else {
            echo json_encode(array('status' => false));
        }
    }

    public function update_question() {

        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = $reqData->data;
            require_once APPPATH . 'Classes/recruitment/Questions.php';
            $objQuestions = new QuestionsClass\Questions();

            $objQuestions->setId(3);
            $objQuestions->setQuestion($reqData->question);
            $objQuestions->setStatus($reqData->question_status);
            $objQuestions->setCreated(DATE_TIME);
            $objQuestions->setCreated_by(1);
            $objQuestions->setQuestionTopic($reqData->question_topic);
            $objQuestions->setTrainingCategory($reqData->question_category);
            $objQuestions->setQuestion_Type($reqData->answer_type);
            $anser_array = array();
            foreach ($reqData->answers as $answer) {
                if ($answer->type == 'answer_a') {
                    $objQuestions->setAnswer_A($answer->value);
                }
                if ($answer->type == 'answer_b') {
                    $objQuestions->setAnswer_B($answer->value);
                }
                if ($answer->type == 'answer_c') {
                    $objQuestions->setAnswer_C($answer->value);
                }
                if ($answer->type == 'answer_d') {
                    $objQuestions->setAnswer_D($answer->value);
                }

                if ($answer->checked) {
                    $anser_array[] = $answer->lebel;
                }
            }
            $objQuestions->setAnswer($anser_array);
            $response = $objQuestions->update_Questions();

            echo json_encode(array('status' => true, 'data' => $response));
        } else {
            echo json_encode(array('status' => false));
        }
    }

    public function delete_Question() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            require_once APPPATH . 'Classes/recruitment/Questions.php';
            $objQuestions = new QuestionsClass\Questions();
            $objQuestions->setId($reqData->id);
            $objQuestions->delete_Question();
            echo json_encode(array('status' => true));
        } else {
            echo json_encode(array('status' => false));
        }
    }

    public function get_recruitment_topic_list() {
        require_once APPPATH . 'Classes/recruitment/Recruitment.php';
        $objRequirment = new RecruitmentClass\Recruitment();
        $requirment = $objRequirment->recruitment_topic_list();
        echo json_encode(array('status' => true, 'data' => $requirment));
    }

    public function get_questions_list() {
        require_once APPPATH . 'Classes/recruitment/Questions.php';
        $objQuestions = new QuestionsClass\Questions();
        $question = $objQuestions->Questions_list();
        echo json_encode($question);
    }

    /* Question Validation start */

    function validate_question_data($rosterData) {
        try {
            $validation_rules = array(
                array('field' => 'start_date', 'label' => 'validate_question_data', 'rules' => 'callback_check_question_data[' . json_encode($rosterData) . ']')
            );
            $this->form_validation->set_data($rosterData);
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return;
    }

    function check_question_data($data, $questionData) {

        $question_Data = json_decode($questionData);
        if (!empty($question_Data)) {
            //echo "<pre>";
            //print_r($question_Data);

            if (empty($question_Data->question)) {
                $this->form_validation->set_message('check_question_data', 'Question can not empty');
                return false;
            }
            if (empty($question_Data->question_topic)) {
                $this->form_validation->set_message('check_question_data', 'Question topic can not empty');
                return false;
            }
            if (empty($question_Data->question_category)) {
                $this->form_validation->set_message('check_question_data', 'Question category can not empty');
                return false;
            }

            // single 0 multiple 1
            //answer_type


            if (empty($question_Data->answers)) {
                $this->form_validation->set_message('check_question_data', 'Answer can not empty');
                return false;
            }
            $cntAnswer = 0;
            foreach ($question_Data->answers as $answer) {
                if ($answer->checked) {
                    $cntAnswer++;
                }
            }




            /*
              // check is default
              if($roster_Data->question==1){
              //$title=trim($roster_Data->title);

              if(!array_key_exists('title', $roster_Data)) {
              $this->form_validation->set_message('check_question_data', 'Roster title can not empty');
              return false;
              }

              if(empty($roster_Data->title)){
              $this->form_validation->set_message('check_question_data', 'Roster title can not empty');
              return false;
              }

              $endDate=strtotime(DateFormate($roster_Data->end_date, 'Y-m-d'));
              if(empty($endDate)){
              $this->form_validation->set_message('check_roster_data', 'Roster end date can not empty');
              return false;
              }

              if($strDate > $endDate){
              $this->form_validation->set_message('check_roster_data', 'Roster end date not less then start date');
              return false;
              }
              if($strDate == $endDate){
              $this->form_validation->set_message('check_roster_data', 'Roster start and end same date not accepted');
              return false;
              }
              }
             */
        } else {
            $this->form_validation->set_message('check_roster_data', 'Roster details can not empty');
            return false;
        }
        return true;
    }

    /* Question Validation end */
}
