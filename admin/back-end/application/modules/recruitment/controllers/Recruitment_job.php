<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class Recruitment_job extends MX_Controller {

    function __construct() {

        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $this->loges->setModule(2);
    }
	
	public function insert_update_job() {
		
		$reqData = request_handler();		
		if (!empty($reqData->data)) {
            $reqData = $reqData->data;
			
			//$response_valid = $this->validate_jobs_data((array)$reqData);
			//if($response_valid['status']){
			
				require_once APPPATH . 'Classes/recruitment/Jobs.php';
				$objJobs = new JobsClass\Jobs();				
				
				if($reqData->mode=='add'){	$objJobs->setId(0); }else{ $objJobs->setId($reqData->job_id);	}				
					$objJobs->setJobName($reqData->job_name);
					$objJobs->setStatus(0);
					$objJobs->setCreated(DATE_TIME);
				//$objJobs->setRecruiter(1);
					$objJobs->setJobPosition(1); //$reqData->job_position
					$objJobs->setPhone($reqData->job_phone);
					$objJobs->setEmail($reqData->job_email);			
					$objJobs->setWebLink($reqData->job_weblink);
					$objJobs->setJobDescription($reqData->job_content);
				//$objJobs->setJobCategory($reqData->answers);
				//$objJobs->setJobTemplate($reqData->answers);
					$objJobs->setJobStartDate(date("Y-m-d", strtotime($reqData->PostDate)));
					$objJobs->setJobEndDate(date("Y-m-d", strtotime($reqData->EndDate)));
				//$objJobs->setPublish($reqData->answers);
				//$objJobs->setJobStatus($reqData->answers);
								
				
				$response=$objJobs->create_Jobs();				
				echo json_encode(array('status' => true, 'data' => $response));
			//}else{
			//	echo json_encode($response_valid);
			//}
		}else{
			echo json_encode(array('status' => false));
		}
    }
	

	public function delete_job(){
		$reqData = request_handler();		
		if(!empty($reqData->data)){
            $reqData = $reqData->data;			
			
			require_once APPPATH . 'Classes/recruitment/Questions.php';
			$objQuestions = new QuestionsClass\Questions();			
			$objQuestions->setId($reqData->id);
			$objQuestions->delete_Question();
			echo json_encode(array('status' => true));	
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
    public function get_recruitment_job_position_list() {
       
    }
	
	public function get_job_questions_list() {
        
    }
	
	/* Question Validation start */
	
	function validate_jobs_data($rosterData){
		try {
            $validation_rules = array(
				array('field' => 'start_date', 'label' => 'validate_jobs_data', 'rules' => 'callback_check_jobs_data['.json_encode($rosterData).']')
            );
            $this->form_validation->set_data($rosterData);
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return;
	}
	function check_jobs_data($data,$questionData){	
		
		$question_Data=json_decode($questionData);			
		if (!empty($question_Data)){						
				if(empty($question_Data->question)){	
					$this->form_validation->set_message('check_question_data', 'Question can not empty');
					return false;
				}
				if(empty($question_Data->question_topic)){	
					$this->form_validation->set_message('check_question_data', 'Question topic can not empty');
					return false;
				}
				if(empty($question_Data->question_category)){	
					$this->form_validation->set_message('check_question_data', 'Question category can not empty');
					return false;
				}				
				if(empty($question_Data->answers)){
					$this->form_validation->set_message('check_question_data', 'Answer can not empty');
					return false;
				}
				$cntAnswer=0;
				foreach($question_Data->answers as $answer){					
					if($answer->checked){
						$cntAnswer++;
					}
				}				
		}else {
				$this->form_validation->set_message('check_roster_data', 'Roster details can not empty');
				return false;
			}
		return true;
	}
	
	/* Question Validation end */
	
}