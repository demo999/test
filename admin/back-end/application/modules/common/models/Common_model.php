<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class Common_model extends CI_Model {

    function __construct() {

        parent::__construct();
    }

    public function get_suburb($post_data, $state) {
        $tbl_suburb_state = TBL_PREFIX . 'suburb_state';

        $this->db->select(array('suburb as value', 'suburb as label', 'postcode'));
        $this->db->from($tbl_suburb_state);
        $this->db->where(array('stateId' => $state));
        $this->db->group_by('suburb');
        $this->db->like('suburb', $post_data);
        $query = $this->db->get();

        return $result = $query->result();
    }

    public function get_user_for_compose_mail($reqData) {

        $name = $this->db->escape_str($reqData->search);
        $sql = array();


        $sql[] = "select concat(firstname, ' ', middlename, ' ', lastname) as  label, '2'  as type , id as value from tbl_participant WHERE archive = 0 and concat(firstname, ' ', middlename, '', lastname) LIKE '%" . $name . "%'";

        $sql[] = "select concat(firstname, ' ', middlename, ' ', lastname) as  label, '3'  as type , id as value from tbl_member WHERE archive = 0 and concat(firstname, ' ', middlename, '', lastname)  LIKE '%" . $name . "%' ";

        $sql[] = "select name as  label, '4'  as type , id as value from tbl_organisation WHERE archive = 0 and name  LIKE '%" . $name . "%' ";

        $sql = implode(' union ', $sql);
        $query = $this->db->query($sql);
//        last_query();
        return $result = $query->result();
    }

    public function get_admin_name($reqData, $currentAdminId) {
        $tbl_admin = TBL_PREFIX . 'admin';
        $this->db->select("concat(firstname,' ', lastname) as label");
        $this->db->select("id as value");
        $this->db->from($tbl_admin);
        $this->db->like('firstname', $reqData->search);

        $query = $this->db->get();
        return $query->result();
    }

    public function get_admin_team_department($name, $currentAdminId) {
        $name = $this->db->escape_str($name);

        $query = $this->db->query("select concat(firstname,' ', lastname,' - (Staff)') as  label, '1'  as type, id as value from tbl_admin 
            WHERE archive = 0 and concat(firstname, ' ', lastname) LIKE '%" . $name . "%' and id != " . $currentAdminId . "
            union 

            select concat(team_name,' - (My Team)') as  label, '2'  as type, id as value from tbl_internal_message_team 
            WHERE archive = 0 and team_name LIKE '%" . $name . "%' and adminId =" . $currentAdminId . "
            union 

            select concat(name,' - (Department)') as  label, '3'  as type, id as value from tbl_department 
            WHERE archive = 0 and name LIKE '%" . $name . "%'");

        return $query->result();
    }

    public function get_global_search_data($search, $adminId) {
        $all_permission = get_all_permission($adminId);


        $search = $this->db->escape_str($search);

        $sql = array();
        if (array_key_exists('access_admin', $all_permission)) {
            $sql[] = "select concat(firstname,' ', lastname) as  label, 'a' as type, tbl_admin.id as value, concat('/admin/user/update/', tbl_admin.id) as url from tbl_admin left join tbl_admin_email on tbl_admin_email.adminId = tbl_admin.id
            WHERE tbl_admin.archive = 0 and (concat(firstname, ' ', lastname) LIKE '%" . $search . "%' OR tbl_admin.id = '" . $search . "' OR tbl_admin_email.email LIKE '%" . $search . "%')";
        }

        if (array_key_exists('access_participant', $all_permission)) {
            $sql[] = "select concat(firstname, ' ',middlename,' ',lastname) as label, 'p'  as type, tbl_participant.id as value, concat('/admin/participant/about/', tbl_participant.id) as url from tbl_participant 
            left join tbl_participant_email on tbl_participant_email.participantId = tbl_participant.id
            WHERE archive = 0 and (concat(firstname, ' ',middlename,' ',lastname) LIKE '%" . $search . "%' OR tbl_participant.id = '" . $search . "' OR tbl_participant_email.email LIKE '%" . $search . "%')";
        }

        if (array_key_exists('access_member', $all_permission)) {
            $sql[] = "select concat(firstname, ' ',middlename,' ',lastname) as label, 'm'  as type, tbl_member.id as value, concat('/admin/member/about/', tbl_member.id) as url from tbl_member left join tbl_member_email on tbl_member_email.memberId = tbl_member.id
            WHERE tbl_member.archive = 0 and (concat(firstname, ' ',middlename,' ',lastname) LIKE '%" . $search . "%' OR tbl_member.id = '" . $search . "' OR tbl_member_email.email LIKE '%" . $search . "%')";
        }

        if (array_key_exists('access_schedule', $all_permission)) {
            $sql[] = "select 'Shift' as label, 's'  as type, id as value, concat('/admin/schedule/details/', id) as url from tbl_shift 
            WHERE status != 8 and (id = '" . $search . "')";
        }

        if (array_key_exists('access_fms', $all_permission)) {
            $sql[] = "select 'Fms' as label, 'f'  as type, id as value, concat('/admin/fms/case/', id) as url from tbl_fms_case 
            WHERE (id = '" . $search . "') ";
        }

        if (array_key_exists('access_organization', $all_permission)) {
            $sql[] = "select name as label, 'o'  as type, id as value, concat('/admin/organisation/about/', id) as url from tbl_organisation WHERE archive = '0' AND parent_org = 0  AND (name LIKE '%" . $search . "%' OR id = '" . $search . "' OR abn = '" . $search . "') ";
        }

        $sql = implode(' union ', $sql);
        $query = $this->db->query($sql);


        $result = $query->result();

        return $result;
    }

    public function get_member_name($post_data) {
        $this->db->group_start();
        $this->db->or_where("(MATCH (firstname) AGAINST ('$post_data *'))", NULL, FALSE);
        $this->db->or_where("(MATCH (middlename) AGAINST ('$post_data *'))", NULL, FALSE);
        $this->db->or_where("(MATCH (lastname) AGAINST ('$post_data *'))", NULL, FALSE);
        $this->db->group_end();

        $this->db->where('archive', 0);
        $this->db->where('status', 1);
        $this->db->select("CONCAT(firstname,' ',middlename,' ',lastname) as memberName");
        $this->db->select(array('id'));
        $query = $this->db->get(TBL_PREFIX . 'member');
        //last_query();
        $query->result();
        $participant_rows = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $participant_rows[] = array('label' => $val->memberName, 'value' => $val->id);
            }
        }
        return $participant_rows;
    }

    public function get_org_name($post_data) {
        $this->db->like('name', $post_data, 'both');    
        $this->db->where('archive','0');
        $this->db->select(array('name', 'id', 'abn'));
        $query = $this->db->get(TBL_PREFIX . 'organisation');
        #last_query();
        #$query->result();
        $org_rows = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $org_rows[] = array('label' => $val->name, 'value' => $val->id, 'abn'=> $val->abn);
            }
        }
        return $org_rows;
    }

    public function get_recruitment_staff($post_data) {

        $tbl_1 = TBL_PREFIX . 'admin';
        $dt_query = $this->db->select(array($tbl_1.'.id',"CONCAT(tbl_admin.firstname,' ',tbl_admin.lastname) AS name"));
        $this->db->join('tbl_recruitment_staff', 'tbl_recruitment_staff.adminId = tbl_admin.id', 'inner');
        $this->db->where($tbl_1.'.archive=',"0");
        $this->db->like("CONCAT(tbl_admin.firstname,' ',tbl_admin.lastname)", $post_data);
        $query = $this->db->get($tbl_1);   
        $staff_rows = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $staff_rows[] = array('label' => $val->name, 'value' => $val->id);
            }
        }
        return $staff_rows;
    }
}
