
import React from 'react';

const CounterShowOnBox = (props) => {
  
    return (
        <div className={"counter_wrapper "+props.classNameAdd}>
                            <div className="counter_div">
                                <div className="counter_text">
                                    {props.checkLengthData(props.counterTitle,props.minNumber)}
                                </div>
                                <span className="counter_lines"></span>
                            </div>
                        </div>
    );
}
CounterShowOnBox.defaultProps = {
     counterTitle: 0,
    minNumber: 3,
    checkLengthData :  (number, width) =>{
        return new Array(width + 1 - (number + '').length).join('0') + number;
    },
    classNameAdd:''
        

};

export { CounterShowOnBox };