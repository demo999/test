
import React from 'react';

const ToastUndo = (props) => {
  let icon = 'icon';
  if(props.showType =='e'){
      icon += ' icon-error-icons';
  }else if(props.showType =='s'){
       icon += ' icon-accept-approve1-ie';
  }
    return (
        <div className="Toastify_content__">
        <h3>
          <p>{props.message} </p>
          <i className={icon}></i>
        </h3>
      </div>
    );
}
ToastUndo.defaultProps = {
     message: '',
    showType: 'e'
};

export { ToastUndo };


