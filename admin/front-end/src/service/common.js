import React, { Component } from 'react';
import jQuery from "jquery";
import moment from 'moment-timezone';
import axios from 'axios';
import { ROUTER_PATH, BASE_URL, LOGIN_DIFFERENCE, PIN_DATA } from '../config.js';
import { confirmAlert, createElementReconfirm } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
        import _ from 'lodash';
import './jquery.validate.js';
import "./custom_script.js";
import {NavLink} from 'react-router-dom';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

//check user login or not
export function checkItsLoggedIn() {
    if (getLoginToken()) {
        window.location = ROUTER_PATH + 'admin/dashboard';
    }
}

export function checkItsNotLoggedIn() {
    check_loginTime();

    if (!getLoginToken()) {
        window.location = ROUTER_PATH;
    }
}

export function checkItsJson(text) {
    try {
        JSON.parse(text);
    } catch (e) {
        return false;
    }
    return true;
}

export function checkLoginWithReturnTrueFalse() {
    if (!getLoginToken()) {
        return false
    } else {
        return true
    }
}

export function checkPin(typeData = '') {
    let pinTypeData = getPinToken();
    let pinData = PIN_DATA;
    let type = typeData != '' && pinData.hasOwnProperty(typeData) ? pinData[typeData] : '';
    pinTypeData = pinTypeData != null && pinTypeData != undefined && pinTypeData != '' ? JSON.parse(pinTypeData) : {};
    if (type != '' && pinTypeData.hasOwnProperty(type) && pinTypeData[type] != '') {
        return true;
    } else {
        return false;
}
}

export function setRemeber(data) {
    var d = new Date();
    var cookieDays = 7;
    d.setTime(d.getTime() + (cookieDays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();

    setCookie('adminUsername', data.username, cookieDays);
    setCookie('adminPassword', data.password, cookieDays);
}

export function getRemeber() {
    var username = getCookie('adminUsername');
    var password = getCookie('adminPassword');
    var data = {username: ((username != undefined) ? username : ''), password: ((password != undefined) ? password : '')}
    return data;
}

export function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

export function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

export function getLoginToken() {
    return localStorage.getItem("ocs_token603560");
}

export function setLoginToken(token) {
    localStorage.setItem("ocs_token603560", token);
}

export function getFullName() {
    return localStorage.getItem("user_name");
}

export function setFullName(full_name) {
    localStorage.setItem("user_name", full_name);
}

export function removeLogoutRequiredItem() {
    localStorage.removeItem('ocs_token603560');
    localStorage.removeItem('ocs_pin_token');
    localStorage.removeItem('dateTime');
    localStorage.removeItem('permission');
    localStorage.removeItem('user_name');
}

export function logout() {
    var ss = postData('admin/Login/logout', getLoginToken(ROUTER_PATH));
    removeLogoutRequiredItem();
    if (typeof (ss) == 'object' && ss.hasOwnProperty('status')) {
        window.location = ROUTER_PATH
    } else {
        setTimeout(function () {
            window.location = ROUTER_PATH;
        }, 300);
    }
}

export function getPinToken() {
    return localStorage.getItem("ocs_pin_token")
}

export function getAllPinToken() {
    return localStorage.getItem("ocs_pin_token")
}

/*
 * check login time if time
 * is more then given
 * time than it will
 * logout diectally
 *
 */
export function getLoginTIme() {
    return localStorage.getItem("dateTime")
}

export function setLoginTIme(token) {
    localStorage.setItem("dateTime", token);
}


export function check_loginTime() {
    var DATE_TIME = getLoginTIme();
    var server = moment(DATE_TIME)

    var currentDateTime = moment()

    const diff = currentDateTime.diff(server);
    const diffDuration = moment.duration(diff);

    if (diffDuration.days() > 0) {

        logout();
    } else if (diffDuration.hours() > 0) {
        logout();
    } else if (diffDuration.minutes() > LOGIN_DIFFERENCE) {
        logout();
    }
}

/*
 *
 */

export function setPinToken(token) {
    localStorage.setItem("ocs_pin_token", token);
}

export function destroyPinToken(typeData) {
    if (typeof (typeData) == undefined || typeData == '') {
        localStorage.removeItem('ocs_pin_token');
        window.location = '/admin/dashboard';
    } else {
        let pinTypeData = getPinToken();
        let pinData = PIN_DATA;
        let type = typeData != '' && pinData.hasOwnProperty(typeData) ? pinData[typeData] : '';
        if (type == '') {
            let pinDataValue = _.invert(pinData);
            let typeKey = typeData != '' && pinDataValue.hasOwnProperty(typeData) ? pinDataValue[typeData] : '';
            type = typeKey != '' ? pinData[typeKey] : '';
        }
        pinTypeData = pinTypeData != null && pinTypeData != undefined && pinTypeData != '' ? JSON.parse(pinTypeData) : {};
        if (type != '' && pinTypeData.hasOwnProperty(type)) {
            delete pinTypeData[type];
            setPinToken(JSON.stringify(pinTypeData));

        }
        let urlRedirect = (type == '3' && pinTypeData.hasOwnProperty('1')) ? '/admin/fms/dashboard/new/case_ongoing' : '/admin/dashboard';
        window.location = urlRedirect;
    }

}

export function getPermission() {
    var AES = require("crypto-js/aes");
    var SHA256 = require("crypto-js/sha256");
    var CryptoJS = require("crypto-js");
    var ciphertext = localStorage.getItem("permission");
    // Decrypt

    try {
        var bytes = CryptoJS.AES.decrypt(ciphertext.toString(), 'secret key 123');
        var plaintext = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    } catch (e) {
        var plaintext = undefined;
    }
    return plaintext;
}

export function setPermission(permission) {
    var AES = require("crypto-js/aes");
    var SHA256 = require("crypto-js/sha256");
    var CryptoJS = require("crypto-js");
    // Encrypt
    var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(permission), 'secret key 123');
    localStorage.setItem("permission", ciphertext);
}

export function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}



export function postData(url, data) {
    var request_data = {'pin': getPinToken(), 'token': getLoginToken(), 'data': data}
    return new Promise((resolve, reject) => {
        fetch(BASE_URL + url, {
            method: 'POST',
            body: JSON.stringify({request_data})
        }).then((response) => response.json())
                .then((responseJson) => {


                    if (url == 'admin/Login/logout') {
                        resolve(responseJson);
                        return true;
                    }

                    // if same account open on another location
                    if (responseJson.another_location_opened) {
                        LogoutAccountOpenedAnotherLocation();
                    }

                    // if jwt token status true mean token not verified
                    if (responseJson.token_status) {
                        logout();
                    }

                    // if token is verified then update date client side time
                    if (responseJson.status) {
                        setLoginTIme(moment());
                    }

                    // if pin status true mean pin token not verified
                    if (responseJson.pin_status) {
                        let type = responseJson.hasOwnProperty('pin_type') ? responseJson.pin_type : '';
                        destroyPinToken(type);
                    }

                    // if ip status true mean ip address change of current user
                    if (responseJson.ip_address_status) {
                        logout();
                    }

                    // if server status true mean request not came at over server
                    if (responseJson.server_status) {
                        logout();
                    }

                    // if permission status true mean not have permission to access this
                    if (responseJson.permission_status) {
                        window.location = '/admin/no_access';
                    }

                    if (!responseJson.another_location_opened) {
                        resolve(responseJson);
                    }

                }).catch((error) => {
            reject(error);
            console.error(error);
        });
    });
}

// arhive anything form table
export function LogoutAccountOpenedAnotherLocation() {
    var msg = <span>This account is opened at another location, you are being logged off.</span>;

    return new Promise((resolve, reject) => {
        confirmAlert({
            customUI: ({ onClose }) => {
                removeLogoutRequiredItem();
                return (
                        <div className='custom-ui'>
                            <div className="confi_header_div">
                                <h3>System message</h3>
                            </div>
                            <p>{msg}</p>
                            <div className="confi_but_div">
                                <a href="/" className="Confirm_btn_Conf"> Ok</a>
                            </div>
                        </div>
                        )
            }
        })
    });
}

export function postImageData(url, data) {
    data.append('pin', getPinToken());
    data.append('token', getLoginToken());


    return new Promise((resolve, reject) => {
        axios.post(BASE_URL + url, data, {
            onUploadProgress: progressEvent => {
                // console.log(progressEvent.loaded / progressEvent.total)
            }
        }).then((response) => {
            response = response.data;

            // if jwt token status true mean token not verified
            if (response.token_status) {
                logout();
            }

            // if token is verified then update date client side time
            if (response.status) {
                setLoginTIme(moment())
            }

            // if pin status true mean pin token not verified
            if (response.pin_status) {
                //destroyPinToken();
                let type = response.hasOwnProperty('pin_type') ? response.pin_type : '';
                destroyPinToken(type);
            }

            // if ip status true mean ip address change of current user
            if (response.ip_address_status) {
                logout();

            }

            // if server status true mean request not came at over server
            if (response.server_status) {
                logout();
            }

            // if permission status true mean not have permission to access this
            if (response.permission_status) {
                window.location = '/admin/no_access';
            }

            resolve(response);
        })
                .catch((error) => {
                    reject(error);
                    console.error(error);
                });
    });
}

export function IsValidJson() {
    return true;
}

export function checkPinVerified(type) {
    if (!checkPin(type)) {
        destroyPinToken(type);
    }
}

// arhive anything form table
export function archiveALL(data, msg, url) {
    if (msg) {
        msg = msg;
    } else {
        msg = <span>Are you sure you want to archive this item? <br /> Once archived, this action can not be undone.</span>;
    }

    return new Promise((resolve, reject) => {
        confirmAlert({
            customUI: ({ onClose }) => {
                return (
                        <div className='custom-ui'>
                            <div className="confi_header_div">
                                <h3>Confirmation</h3>
                                <span className="icon icon-cross-icons" onClick={() => {
                                onClose();
                                resolve({status: false })
                                      }}></span>
                            </div>
                            <p>{
                                            msg}</p>
                            <div className="confi_but_div">
                                <button className="Confirm_btn_Conf" onClick={
                                    () => {
                                        postData(url, data).then((result) => {
                                            resolve(result);
                                            onClose();
                                                                                    })
                                        }}>Confirm</button>
                                <button className="Cancel_btn_Conf" onClick={
                                            () => {
                                                onClose();
                                                resolve({status: false });
                                        }}> Cancel</button>
                            </div>
                        </div>
                                        )
                    }
                })
            });
        }

        export function handleShareholderNameChange(obj, stateName, index, fieldName, value) {
            console.log(value);
            var state = {};
            var tempField = {};
            var List = obj.state[stateName];
            List[index][fieldName] = value

            state[stateName] = List;
            obj.setState(state);
        }

        export function handleAddShareholder(obj, e, stateName, object_array) {
            e.preventDefault();
            var state = {};
            var temp = object_array
            var list = obj.state[stateName];

            state[stateName] = list.concat([reInitializeObject(temp)]);
            obj.setState(state);
        }


        export function reInitializeObject(object_array) {
            var state = {}
            Object.keys(object_array).forEach(function (key) {
                state[key] = '';
            });
            return state;
        }

        export function handleRemoveShareholder(obj, e, index, stateName) {
            e.preventDefault();
            var state = {};
            var List = obj.state[stateName];

            state[stateName] = List.filter((s, sidx) => index !== sidx);
            obj.setState(state);
        }

        export function handleCheckboxValue(obj, stateName, index, fieldName) {
            var List = obj.state[stateName];
            var state = {};

            if (List[index][fieldName] == undefined || List[index][fieldName] == false) {
                List[index][fieldName] = true
            } else {
                List[index][fieldName] = false
            }
            state[stateName] = List;
            obj.setState(state);
        }

        export function handleChange(Obj, e) {
            var state = {};
            state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
            Obj.setState(state);
        }
        /*used for checkbox,input field,*/
        export function handleChangeChkboxInput(Obj, e) {
            var state = {};
            state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
            Obj.setState(state);
        }

        export function handleChangeSelectDatepicker(Obj, selectedOption, fieldname) {
            var state = {};
            state[fieldname] = selectedOption;
            state[fieldname + '_error'] = false;
            if (fieldname == 'state') {
                state['city'] = {};
                state['postal'] = '';
            }
            Obj.setState(state);
        }

        export function calendarColorCode(givenKey) {
            var myArray = [
                {value: '7', label: '#89e0a9'}, //confirmed light green
                {value: '2', label: '#e0da8c'}, //unconfirmed yellow
                {value: '5', label: '#ff7a7b'}, //cancelled
                {value: '1', label: '#a3b5c7'}, //unfilled
            ];
            if (givenKey == 0) {
                return myArray;
            } else {
                var index = myArray.findIndex(x => x.value == givenKey)
                return myArray[index].label;
            }
        }

        export function getOptionsSuburb(input, state) {
            return queryOptionData(input, 'common/Common/get_suburb', {query: input, state: state}, 3);
        }

        export function getOptionsParticipant(input) {
            return queryOptionData(input, 'schedule/ScheduleDashboard/get_participant_name', {query: input});
        }

        export function getOptionsSiteName(e) {
            return queryOptionData(e, 'schedule/ScheduleDashboard/get_site_name', {query: e});
        }

        export function getOptionsMember(e, memberArray) {
            return queryOptionData(e, 'common/Common/get_member_name', {query: e});
        }

        export function getOptionsAdmin(e) {
            if (!e) {
                return Promise.resolve({options: []});
            }

            return postData('common/Common/get_admin_name', {search: e}).then((json) => {
                return {options: json};
            });
        }

        export function getAdminTeamDepartment(e) {
            if (!e) {
                return Promise.resolve({options: []});
            }
            return postData('common/Common/get_admin_team_department', {search: e}).then((json) => {
                return {options: json};
            });
        }

        export function getOptionsParticipantMember(e, previous) {
            if (!e || e.length < 3) {
                return Promise.resolve({options: []});
            }

            return postData('common/Common/get_user_for_compose_mail', {search: e, previous: previous}).then((json) => {
                return {options: json};
            });
        }

        export function getTimeAgoMessage(DATE_TIME) {
            var messageTime = moment(DATE_TIME)

            var currentDateTime = moment()

            const diff = currentDateTime.diff(messageTime);
            const diffDuration = moment.duration(diff);


            if (diffDuration.days() > 0) {
                return diffDuration.days() + ' Days.'
            } else if (diffDuration.hours() > 0) {
                return diffDuration.hours() + ' hr.'
            } else if (diffDuration.minutes() > 0) {
                return diffDuration.minutes() + ' Min.'
            } else {
                return 'Just now'
            }
        }

        export function reFreashReactTable(obj, fetchDataMethod) {
            var ReactOption = obj.reactTable.current.state;
            var state = {pageSize: ReactOption.pageSize, page: ReactOption.page, sorted: ReactOption.sorted, filtered: ReactOption.filtered}
            obj[fetchDataMethod](state);
        }

        export function handleDateChangeRaw(e) {
            e.preventDefault();
        }

        export function changeTimeZone(dateTIme, FORMATE, returnType) {
            if (dateTIme) {
                if (!FORMATE) {
                    FORMATE = 'DD/MM/YYYY';
                }

                //                var temp = moment.utc(dateTIme);
                ////    console.log(moment(dateTIme, 'YYYY-MM-DD').format());
                //                var local = temp.local().format(FORMATE);

                var local = moment(dateTIme).format(FORMATE);

                if (returnType == true) {
                    local = moment(local);
                }

                return local;
            }
        }

        export function getStateList() {
            return new Promise((resolve, reject) => {
                postData('common/common/get_state', {}).then((result) => {
                    if (result.status) {
                        resolve(result.data);
                    }
                })
            });
        }

        export function getFmscasePrimaryCategory() {
            return new Promise((resolve, reject) => {
                postData('common/common/get_case_primary_cat', {}).then((result) => {
                    if (result.status) {
                        resolve(result.data);
                    }
                })
            });
        }

        export const selectFilterOptions = (options, filterValue, excludeOptions, props) => {
            if (excludeOptions)
                excludeOptions = excludeOptions.map(function (i) {
                    return i[props.valueKey];
                });

            return options.filter(function (option) {
                if (excludeOptions && excludeOptions.indexOf(option[props.valueKey]) > -1)
                    return false;
                if (props.filterOption)
                    return props.filterOption.call(undefined, option, filterValue);
                if (!filterValue)
                    return true;

                var value = option[props.valueKey];
                var label = option[props.labelKey];


                if (!value && !label) {
                    return false;
                }

                var valueTest = value ? String(value) : null;
                var labelTest = label ? String(label) : null;

                return (props.matchPos === 'start') ? (valueTest) : (valueTest);
            });
        };

        /*export function checkLoginModule (obj,moduleType,returnUrlIfLogin)
         {
         if(checkPin())
         {
         window.location.href=returnUrlIfLogin;
         }
         else
         {
         if(moduleType == 'fms')
         obj.setState({pinModalOpen:true,moduleHed:'FMS Module',color:'Red_fms'})
         else if(moduleType == 'admin')
         obj.setState({pinModalOpen:true,moduleHed:'Admin Module',color:'Blue'})
         }
         }*/

        export function checkLoginModule(obj, moduleType, returnUrldefine) {
            let urlDefault = {fms: '/admin/fms/dashboard/new/case_ongoing', admin: '/admin/user/dashboard', incident: 'admin/fms/dashboard/incidents/incident_ongoing'};
            let url = '';
            url = (returnUrldefine != undefined && typeof (returnUrldefine) == 'string' && returnUrldefine.trim() != '' ? returnUrldefine.trim() : (urlDefault.hasOwnProperty(moduleType) ? urlDefault[moduleType] : '/admin/dashboard'));
            console.log('here', url);
            if (checkPin(moduleType)) {
                //move to desired llocation
                if (moduleType == 'fms') {
                    window.location.href = url;
                } else if (moduleType == 'admin') {
                    window.location.href = url;
                } else if (moduleType == 'incident') {
                    window.location.href = url;
                }
            } else {
                if (moduleType == 'fms') {
                    obj.setState({pinModalOpen: true, moduleHed: 'FMS Module', color: 'Red_fms', pinType: 1, returnUrl: url});
                } else if (moduleType == 'admin') {
                    obj.setState({pinModalOpen: true, moduleHed: 'Admin Module', color: 'Blue', pinType: 2, returnUrl: url})
                } else if (moduleType == 'incident') {
                    obj.setState({pinModalOpen: true, moduleHed: 'Incident Module', color: 'Red_fms', pinType: 3, returnUrl: url})
                }

            }
        }

        export const Aux = (props) => props.children;

        export function getOptionsCrmParticipant(input) {
            if (!input) {
                return Promise.resolve({options: []});
            }
            return fetch(BASE_URL + 'crm/CrmTask/get_participant_name?query=' + input)
                    .then((response) => {
                        return response.json();
                    }).then((json) => {
                return {options: json};
            });
        }

        export function getOptionsCrmMembers(input) {
            if (!input) {
                return Promise.resolve({options: []});
            }
            return fetch(BASE_URL + 'crm/CrmStaff/get_staff_name?query=' + input)
                    .then((response) => {
                        return response.json();
                    }).then((json) => {
                return {options: json};
            });
        }


        export function getOptionsallUsers(input) {
            if (!input) {
                return Promise.resolve({options: []});
            }
            return fetch(BASE_URL + 'crm/CrmStaff/get_all_users?query=' + input)
                    .then((response) => {
                        return response.json();
                    }).then((json) => {
                return {options: json};
            });
        }

        export function getOptionsRecruitmentStaff(e, memberArray) {
            return queryOptionData(e, 'common/Common/get_recruitment_staff', {query: e});
        }

        export function queryOptionData(e, urlData, requestData, checkLengthData = 0) {
            if (!e || e.length < parseInt(checkLengthData)) {
                let blankData = Promise.resolve({options: []});
                return blankData.then((res) => {
                    return res;
                });
            }

            var Request = JSON.stringify(requestData);
            return postData(urlData, Request).then((response) => {
                return {options: response};
            });
        }

        export function pinHtml(obj, moduleType, pageshow, redirectUrl, labelShow) {
            let lableFirstLatter = '';
            let lable = '';
            let pagesTitleShow = false;
            let pagesTitleIcon = true;
            let iconColor = 'a-colr';
            if (pageshow != undefined && pageshow != '' && pageshow == 'dashboard') {
                pagesTitleShow = true;
            }

            if (pageshow != undefined && pageshow != '' && pageshow == 'menu') {
                pagesTitleIcon = false;
            }
            let defaultUrl = ROUTER_PATH;
            if (moduleType == 'fms') {
                lableFirstLatter = 'F';
                lable = 'FMS';
                iconColor = 'f-colr';
                defaultUrl = ROUTER_PATH + 'admin/fms/dashboard/new/case_ongoing';
            } else if (moduleType == 'admin') {
                lableFirstLatter = 'A';
                lable = 'Admin';
                iconColor = 'a-colr';
                defaultUrl = ROUTER_PATH + 'admin/user/dashboard';
            } else if (moduleType == 'incident') {
                lableFirstLatter = 'I';
                lable = 'Incident';
                iconColor = 'f-colr';
                defaultUrl = ROUTER_PATH + 'admin/fms/dashboard/incidents/incident_ongoing';
            }

            if (redirectUrl != undefined && redirectUrl != '' && pageshow == 'menu') {
                defaultUrl = redirectUrl;
            }
            if (labelShow != undefined && labelShow != '' && pageshow == 'menu') {
                lable = labelShow;
            }

            const htlmlLable = pagesTitleShow ? <p>{lable}</p> : <React.Fragment />;
            const htlmlIcon = pagesTitleIcon ? <span className={"add_access " + iconColor}>{lableFirstLatter}</span> : <React.Fragment >{lable}</React.Fragment>;

            if (!checkPin(moduleType)) {
                return <React.Fragment><a onClick={() => checkLoginModule(obj, moduleType, defaultUrl)}>{htlmlIcon}{htlmlLable}</a ></React.Fragment>
            } else {
                return <React.Fragment><NavLink to={defaultUrl}>{htlmlIcon}{htlmlLable}</NavLink ></React.Fragment>
            }
        }
