import React from 'react';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import {ShiftCheckbox,ShiftDays,ShiftRequirement} from  '../../../dropdown/CrmDropdown.js';

import moment from 'moment';
export const ParticipantShiftPopup = (props) => {
    return (
        <React.Fragment>
        <form id="partcipant_shift">
            <div className="row">
                <div className="col-md-12 py-4 title_sub_modal">Shift</div>
                </div>
                <div className="row">
                    <div className="col-md-3 mb-4">
                      <label className="title_input">What is your Preferred Start Date?: </label>
                      <div className="required"><DatePicker showYearDropdown scrollableYearDropdown yearDropdownItemNumber={110} dateFormat="DD/MM/YYYY" required={true} data-placement={'bottom'} minDate={moment()}
                               name="start_date" onChange={(e) => props.updateSelect(e, 'start_date','participantShift')}  className="text-center px-0" placeholderText="DD/MM/YYYY"  selected={props.sts.start_date ? moment(props.sts.start_date, 'DD-MM-YYYY') : null}/></div>
                    </div>
                  </div>

                  <div className="row">
                <div className="col-md-12">
                <div className="Sched_shift-02__">
                    <div className="Sched_shift-02-1__">
                    <div></div>
                    <div>Am</div>
                    <div>PM</div>
                    <div>Sleep Over</div>
                    <div>Ative Night</div>
                    </div>

                  {ShiftDays(props.sts.shiftsvalues).map((value, idxx) => (
                  <div className="Sched_shift-02-2__">
                      <div><span>{value.label}</span></div>

                     {  ShiftCheckbox(value.data).map((value2, idx) => (
                      <div>
                         <span><label class="c-custom-checkbox CH_010">
                         <input type="checkbox" class="checkbox1" id={value2.value} name="shiftsvalues" value={value2.value }checked={value2.checked} onChange={(e)=>props.checkboxHandlerShift(e,'participantShift',value.value)}/><i class="c-custom-checkbox__img"></i></label> </span>
                      </div>
                      ))
                      }

                  </div>
                  ))}


                </div>
                </div>
            </div>

            <div className="row mt-5">

            <div className="col-md-12"> <label className="title_input pl-0">What are the Participants Shift Requirements?</label></div>
                <div className="col-md-4">
                <div className="Scroll_div_parents">
                    <div className="Scroll_div">
                        <div className="row">
                            <span>
                            {
                                ShiftRequirement(props.sts.shift_requirement).map((value, idxx) => (
                                   <span key={idxx}>
                                     <div className="col-md-12 mb-2">
                                       <label className="c-custom-checkbox CH_010">
                                           <input type="checkbox" className="checkbox1" id={value.value} name="shift_requirement" value={value.value }checked={value.checked} onChange={(e)=>props.checkboxHandler(e,'participantShift')}  />
                                           <i className="c-custom-checkbox__img"></i>
                                           <div>{value.label}</div>
                                       </label>
                                     </div>
                                   </span>
                               ))
                             }

                            </span>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div className="row d-flex justify-content-end">
              <div className="col-md-3"><a className="btn-1" onClick={props.submitParticipantShift}>Save And Changes</a></div>
            </div>
            </form>
        </React.Fragment>

    );

}
