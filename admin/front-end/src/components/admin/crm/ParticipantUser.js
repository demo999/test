import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Doughnut, Bar } from 'react-chartjs-2';
import { Tabs, Tab, ProgressBar } from 'react-bootstrap';
import ReactTable from "react-table";
import 'react-table/react-table.css';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { checkItsNotLoggedIn, postData,getPermission} from '../../../service/common.js';
import { ROUTER_PATH } from '../../../config.js';
import { allLatestUpdate } from './actions/DashboardAction.js';
import { CounterShowOnBox } from 'service/CounterShowOnBox.js';
import { connect } from 'react-redux';
import CrmPage from './CrmPage';


class ProspectiveParticipantStatus extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      crmparticipantCount: 0,
      view_type: 'month',
      grapheachdata: '',
    };

  }

  componentDidMount() {
    this.crmParticipantAjax();
  }

  crmParticipantStatusBy = (type) => {
    this.setState({ view_type: type }, function () {
      this.crmParticipantAjax();
    });
  }

  crmParticipantAjax = () => {
    postData('crm/Dashboard/crm_participant_status', this.state).then((result) => {
      if (result.status) {
        this.setState({ crmparticipantCount: result.participant_count });
        this.setState({ grapheachdata: result.grapheachdata });

      } else {
        this.setState({ error: result.error });
      }
    });
  }


  render() {
    const Graphdata = {
      labels: ['Successful', 'Processing', 'Rejected'],
      datasets: [{
        data: [this.state.grapheachdata.successful, this.state.grapheachdata.processing, this.state.grapheachdata.rejected],
        backgroundColor: ['#be77ff', '#7c00ef', '#5300a7'],
      }],
    };
    return (
      <li className="radi_2">
        <h2 className="text-center  cl_black">Participant Status:</h2>
        <div className="row  pb-3 align-self-center w-100 mx-auto Graph_flex">
          <div className="text-center col-md-5 col-xs-12">
            <div className="myChart12 mx-auto" >
              <Doughnut data={Graphdata} height={250} className="myDoughnut" legend={""} />
            </div>
          </div>

          <div className="col-md-4 col-xs-12 text-center d-inline-flex align-self-center">
            <div className="myLegend mx-auto">
              <div className="chart_txt_1"><b>Successful:</b> {this.state.grapheachdata.successful}</div>
              <div className="chart_txt_2"><b>Processing:</b> {this.state.grapheachdata.processing}</div>
              <div className="chart_txt_3"><b>Rejected:</b> {this.state.grapheachdata.rejected}</div>
            </div>
          </div>
          <div className="W_M_Y_box P_15_T col-md-3 col-xs-12 pb-3 d-inline-flex align-self-center">
                        <div className="vw_bx12 mx-auto">
            <h5><b>View by:</b></h5>
            <span onClick={() => this.crmParticipantStatusBy('week')} className={this.state.view_type == 'week' ? 'color' : ''}>Week</span><br/>
            <span onClick={() => this.crmParticipantStatusBy('month')} className={this.state.view_type == 'month' ? 'color' : ''}>Month </span><br/>
            <span onClick={() => this.crmParticipantStatusBy('year')} className={this.state.view_type == 'year' ? 'color' : ''}> Year</span><br/>
          </div>
          </div>
        </div>
      </li>
    );
  }
}


class ProspectiveParticipantCount extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      crmparticipantCount: 0,
      view_type: 'month',
      all_crmparticipant_count: '',
    };

  }

  componentDidMount() {
    this.crmParticipantAjax();
  }

  crmParticipantCountBy = (type) => {
    this.setState({ view_type: type }, function () {
      this.crmParticipantAjax();
    });
  }
  //
  crmParticipantAjax = () => {
    postData('crm/Dashboard/crm_participant_count', this.state).then((result) => {
      if (result.status) {
        this.setState({ crmparticipantCount: result.crm_participant_count });
        this.setState({ all_crmparticipant_count: result.all_crmparticipant_count });

      } else {
        this.setState({ error: result.error });
      }
    });
  }

  render() {

    return (
      <li className="radi_2">

        <h2 className="text-center  cl_black">Prospective Participants:</h2>
        <div className="row pb-3 align-self-center w-100 mx-auto Graph_flex">
          <div className="col-md-8 col-xs-12 d-inline-flex align-self-center justify-content-center mb-3">
            <CounterShowOnBox counterTitle={this.state.crmparticipantCount} classNameAdd="" />
          </div>



          <div className="W_M_Y_box  col-md-4 col-xs-12 d-inline-flex align-self-center">
            <div className="vw_bx12 mx-auto">
              <h5><b>View by:</b></h5>


              <span onClick={() => this.crmParticipantCountBy('week')} className={this.state.view_type == 'week' ? 'color' : ''}>Week</span><br />
              <span onClick={() => this.crmParticipantCountBy('month')} className={this.state.view_type == 'month' ? 'color' : ''}>Month </span><br />
              <span onClick={() => this.crmParticipantCountBy('year')} className={this.state.view_type == 'year' ? 'color' : ''}> Year</span>
            </div>
          </div>
        </div>

      </li>
    );
  }
}



class Participantadmin extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleSelect = this.handleSelect.bind(this);
    this.state = { filterVal: '', key: 1,
      permissions : (getPermission() == undefined)? [] : JSON.parse(getPermission()),
    };

  }

  handleSelect(key) {
    this.setState({ key });
  }


  render() {
    var options = [
      { value: 'one', label: 'One' },
      { value: 'two', label: 'Two' }
    ];
    const Participantsavailable = {
      labels: ['', '', ''],
      datasets: [{
        data: ['250', '333', '250'],
        backgroundColor: ['#be77ff', '#7c00ef', '#5300a0'],

      }],

    };
    return (
      <div>
        <CrmPage pageTypeParms={'crm_dashboard'} />
        <div className="container-fluid">


          <div className="row">
            <div className="col-lg-10 col-lg-offset-1">
              <div className="back_arrow py-4 bb-1">
                <a href={ROUTER_PATH + 'admin/dashboard'}><span className="icon icon-back1-ie"></span></a>
              </div>
            </div>
          </div>

          <div className="row _Common_He_a">
            <div className="col-lg-8 col-lg-offset-1 col-xs-9"><h1 className="my-0 color"> CRM Dashboard (User)</h1></div>
            <div className="col-lg-2 col-xs-3">
              <a className="Plus_button" href="#"><i className="icon icon-add-icons create_add_but"></i><span>Create New Participant</span></a>
            </div>
          </div>
          <div className="row "><div className="col-lg-10 col-lg-offset-1"><div className="bt-1"></div></div></div>


          <div className="row">
            <div className="col-lg-10 col-lg-offset-1">
              <ul className="landing_graph landing_crm__ mt-5">
                <ProspectiveParticipantCount />
                <ProspectiveParticipantStatus />
              </ul>
              <div className="bt-1 my-5"></div>
            </div>
          </div>
          {/* 2. Start Tab Concept Start */}
        {(this.state.permissions.access_crm_admin)?
          <div>
        <div className="row">
          <div className="col-lg-10 col-lg-offset-1">
            <div className="row py-5">
              {// <div className="col-lg-4"><a onClick={() => this.showModal_PE('showModal_PE')} className="btn_g__">Reporting</a></div>
              }
              <div className="col-lg-4"><div className="btn_g__"><Link to={ROUTER_PATH + 'admin/crm/reporting'}>Reporting</Link></div></div>
              <div className="col-lg-4"><div ><Link className="btn-1" to={ROUTER_PATH + 'admin/crm/prospectiveparticipants'}>Prospective Participant</Link></div></div>
              <div className="col-lg-4"><Link to="#" className="btn_g__"><Link to={ROUTER_PATH + 'admin/crm/schedules'}>Schedules</Link></Link></div>
            </div>
          </div>
        </div>
        <div className="row "><div className="col-lg-10 col-lg-offset-1"><div className="bt-1"></div></div></div>

        <div className="row">
          <ul className="nav nav-tabs Category_tap Nav_ui__ col-lg-10 col-lg-offset-1 col-sm-12 P_20_TB">
            <li className="col-lg-4 col-sm-4 active"><a href="#1a" data-toggle="tab">Personal View</a>
            </li>
            <li className="col-lg-4 col-sm-4 "><Link to={ROUTER_PATH + 'admin/crm/participantadmin'} >CRM Department View</Link>
            </li>
          </ul>
        </div>
        </div>
      :''}
          {/* <div className="row">
            <div className="col-lg-10 col-lg-offset-1 px-0">
              <div className="tabs_1">
                <ul>
                  <li><Link to={ROUTER_PATH + 'admin/crm/prospectiveparticipants'}>Prospective Participants</Link></li>
                  <li><Link to={ROUTER_PATH + 'admin/crm/schedules'}>Schedules</Link></li>
                </ul>
              </div>
              <div className="col-md-12"> <div className=" bt-1"></div></div>
            </div>
          </div> */}

          {// <div className="row">
            //     <Filterdiv />
            //     {/* <Tablereact addclass="re-table re-table2 re-table-progrees mt-4" /> */}
            // </div>
          }

          <div className="row">
            <div className="col-lg-5 col-lg-offset-1 col-md-6">
              <LatestActions />
            </div>
            <div className="col-lg-5 col-md-6">
              <DueTasks />
            </div>
          </div>

        </div>
      </div>

    );
  }
}


class LatestActions extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      activeCol: '',
      data: []
    }
  }
  crmParticipantLatestAction = () => {
    postData('crm/Dashboard/crm_latest_action', this.state).then((result) => {
      if (result.status) {
        this.setState({ data: result.data.data });

      } else {
        this.setState({ error: result.error });
      }
    });
  }
  componentWillMount() {
    this.crmParticipantLatestAction();
  }
  render() {
    return (
      <div className="PD_Al_div">
        <div className="PD_Al_h_txt pt-3">New assigned Participants</div>
        <div className="re-table re_tab_D">
          <ReactTable
            data={this.state.data}
            columns={[
              {
                Header: "Participant Name", accessor: "FullName",
                headerClassName: 'hdrCls', className: (this.state.activeCol === 'name') ? 'borderCellCls' : 'T_align_m1'
              },
              {
                Header: "Intake Type", accessor: "stage_name",
                headerClassName: 'hdrCls', className: (this.state.activeCol === 'overdueaction') ? 'borderCellCls' : 'T_align_m1'
              },
              {
                Header: "Intake Date", accessor: "duedate",
                headerClassName: 'hdrCls', className: (this.state.activeCol === 'date') ? 'borderCellCls' : 'T_align_m1',
                maxWidth: 120,
              },
              {
                Header: "", headerStyle: { border: '0px solid #000' },
                maxWidth: 100,
                Cell: row => (<span className="PD_Al_icon">  <Link to={ROUTER_PATH + 'admin/crm/tasks'}><a><i className="icon icon-view2-ie LA_i1"></i></a></Link>
                <Link to={ROUTER_PATH + 'admin/crm/tasks'}><a><i className="icon icon-notes2-ie LA_i2"></i></a></Link></span>)
              },
            ]}
            defaultPageSize={9}
            showPagination={false}
            sortable={false}
            className="-striped -highlight"
          />
        </div>
        <Link className="btn-1" to={ROUTER_PATH + 'admin/crm/prospectiveparticipants'}>View All</Link>
      </div>
    )
  }
}

class DueTasks extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      activeCol: '',
      data: [],
    }
  }
  crmParticipantDueTask = () => {
    postData('crm/Dashboard/crm_task_list', this.state).then((result) => {
      if (result.status) {
        this.setState({ data: result.data.data });

      } else {
        this.setState({ error: result.error });
      }
    });
  }
  componentWillMount() {
    this.crmParticipantDueTask();
  }
  render() {
    return (
      <div className="PD_Al_div">
        <div className="PD_Al_h_txt pt-3">Due Tasks: (Next 5 day)</div>
        <div className="re-table re_tab_D">
          <ReactTable
            data={this.state.data}
            columns={[
              {
                Header: "Task Name", accessor: "taskname",
                headerClassName: 'hdrCls', className: (this.state.activeCol === 'name') ? 'borderCellCls' : 'T_align_m1'
              },
              {
                Header: "Participant", accessor: "fullname",
                headerClassName: 'hdrCls', className: (this.state.activeCol === 'status') ? 'borderCellCls' : 'T_align_m1'
              },
              {
                Header: "Due Date", accessor: "duedate",
                headerClassName: 'hdrCls', className: (this.state.activeCol === 'date') ? 'borderCellCls' : 'T_align_m1',
                maxWidth: 120,
              },
              {
                Header: "", headerStyle: { border: '0px solid #000' },
                maxWidth: 100,
                Cell: row => (<span className="PD_Al_icon"><a><i className="icon icon-view2-ie LA_i1"></i></a> <a><i className="icon icon-notes2-ie LA_i2"></i></a></span>)
              },
            ]}
            defaultPageSize={9}
            showPagination={false}
            sortable={false}
            className="-striped -highlight"
          />
        </div>
        <Link className="btn-1" to={ROUTER_PATH + 'admin/crm/tasks'}>View All</Link>
      </div>
    )
  }
}







const mapStateToProps = state => {
  return {
    showPageTitle: state.DepartmentReducer.activePage.pageTitle,
    showTypePage: state.DepartmentReducer.activePage.pageType,

  }
};
const mapDispatchtoProps = (dispach) => {
  return {
    allLatestUpdate: () => dispach(allLatestUpdate()),
  }
}
export default connect(mapStateToProps, mapDispatchtoProps)(Participantadmin);
