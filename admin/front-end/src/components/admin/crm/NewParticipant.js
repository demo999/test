import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import 'react-table/react-table.css';
import 'react-select-plus/dist/react-select-plus.css';
import { checkItsNotLoggedIn, postData } from '../../../service/common.js';
import { ROUTER_PATH } from '../../../config.js';
import { allLatestUpdate } from './actions/DashboardAction.js';
import { connect } from 'react-redux';
import CrmPage from './CrmPage';

import {RefererData} from './RefererData';
import {ParticipantAbilityPopup} from './ParticipantAbilityPopup';
import {ParticipantDetailsPopup} from './ParticipantDetailsPopup';
import {ParticipantShiftPopup} from './ParticipantShiftPopup';





class NewParticipant extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleSelect = this.handleSelect.bind(this);
    this.state = { filterVal: '', key: 1 };

  }

  handleSelect(key) {
    this.setState({ key });
  }


  render() {
    var options = [
      { value: 'one', label: 'One' },
      { value: 'two', label: 'Two' }
    ];
    const Participantsavailable = {
      labels: ['', '', ''],
      datasets: [{
        data: ['250', '333', '250'],
        backgroundColor: ['#ed97fa', '#b968c7', '#702f75'],

      }],

    };
    return (
      <div>
        <CrmPage pageTypeParms={'crm_dashboard'} />
        <div className="container-fluid">


          <div className="row">
            <div className="col-lg-10 col-lg-offset-1">
              <div className="back_arrow py-4 bb-1">
                <a href={ROUTER_PATH + 'admin/dashboard'}><span className="icon icon-back1-ie"></span></a>
              </div>
            </div>
          </div>

          <div className="row _Common_He_a">
            <div className="col-lg-8 col-lg-offset-1 col-xs-9"><h1 className="my-0 color"> Create New Participant</h1></div>
            
          </div>
          <div className="row "><div className="col-lg-10 col-lg-offset-1"><div className="bt-1"></div></div></div>


          <div className="row">
            <div className="col-lg-10 col-lg-offset-1">
            <ul className="nav nav-tabs create-new-par__ bb-1">
              <li className="active"><a data-toggle="tab" href="#RefererDetails">Referer Details</a></li>
              <li><a data-toggle="tab" href="#ParticipantDetails">Participant Details</a></li>
              <li><a data-toggle="tab" href="#ParticipantAbility">Participant Ability</a></li>
              <li><a data-toggle="tab" href="#Shift">Shift</a></li>
            </ul>

            <div className="w-100">
              <div className="tab-content">
                {/* 1. Start Referer Details from */}
                <div id="RefererDetails" className="tab-pane active">
                  <RefererData sts={this.state} updateSelect={this.updateSelect} options={options} />
                </div>
                {/* End Referer Details from */}

                {/* 1. Start Participant Details from */}
                <div id="ParticipantDetails" className="tab-pane">
                   <ParticipantDetailsPopup sts={this.state} updateSelect={this.updateSelect} options={options} />
                   </div>
                {/* End Participant Details from */}


                {/* 1. Start Participant Details from */}
                <div id="ParticipantAbility" className="tab-pane">
                  <ParticipantAbilityPopup sts={this.state} updateSelect={this.updateSelect} options={options} />
                </div>
                <div id="Shift" className="tab-pane">
                <ParticipantShiftPopup sts={this.state} updateSelect={this.updateSelect} options={options} />
                </div>
              </div>

              <div className="row d-flex justify-content-end">
                <div className="col-md-3"><a className="btn-1">Save And Changes</a></div>
              </div>

            </div>

            </div>
          </div>

        </div>
      </div>

    );
  }
}

const mapStateToProps = state => {
  return {
    showPageTitle: state.DepartmentReducer.activePage.pageTitle,
    showTypePage: state.DepartmentReducer.activePage.pageType,

  }
};
const mapDispatchtoProps = (dispach) => {
  return {
    allLatestUpdate: () => dispach(allLatestUpdate()),
  }
}
export default connect(mapStateToProps, mapDispatchtoProps)(NewParticipant);
