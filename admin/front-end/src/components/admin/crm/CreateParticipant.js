import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import 'react-table/react-table.css';
import 'react-select-plus/dist/react-select-plus.css';
import { allLatestUpdate } from './actions/DashboardAction.js';
import { crmParticipant,states,crmParticipantSubmit } from './actions/CrmParticipantAction.js';
import { connect } from 'react-redux';
import jQuery from "jquery";
import '../../../service/jquery.validate.js';
import { ROUTER_PATH, BASE_URL }from '../../../config.js';
import { postData, getOptionsSuburb, handleAddShareholder ,checkItsNotLoggedIn} from '../../../service/common.js';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import CrmPage from './CrmPage';
import { listViewSitesOption, relationDropdown, sitCategoryListDropdown, ocDepartmentDropdown, getAboriginalOrTSI, LivingSituationOption} from '../../../dropdown/CrmDropdown.js';
import {RefererData} from './RefererData';
import {ParticipantAbilityPopup} from './ParticipantAbilityPopup';
import {ParticipantDetailsPopup} from './ParticipantDetailsPopup';
import {ParticipantShiftPopup} from './ParticipantShiftPopup';
import { ToastContainer, toast } from 'react-toastify';
import { ToastUndo } from 'service/ToastUndo.js'
import ProspectiveParticipants from './ProspectiveParticipants.js';


class NewParticipant extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleSelect = this.handleSelect.bind(this);
    this.state = {
      filterVal: '',
      key: 1,
      refer:false,
      tab_active:true,
      participant_details:false,
      ability:false,
      refererDetails:{'first_name':'','last_name':'','organisation':'','email':'','phone_number':'','relation':''},
      participantDetails: {'Dob':'','aboriginal':'','current_behavioural':'','email':'','firstname':'','lastname':'','livingsituation':'','martialstatus':'','ndisno':'','plan_management':3, 'provide_phone_number': '', 'provide_email': '', 'provide_address': '','current_behavioural':0,'other_relevent_plans':0 ,'aboriginal':0},
      participantAbility:{'legal_issues':0,'other_relevant_conformation':'','secondary_fomal_diagnosis_desc':'','primary_fomal_diagnosis_desc':'','languages_spoken':[],'language_interpreter':0,'linguistic_diverse':0,'require_assistance':[],'require_mobility':[],'hearing_interpreter':0,'communication':'','cognitive_level':' '},
      participantShift:{'start_date':'','shiftsvalues':{'1':[],'2':[],'3':[],'4':[],'5':[],'6':[],'7':[]},'shifts':[],'shift_requirement':[]}
   };


  }
  handleShiftCheckbox=(e,array_name,week)=>{
    let details =this.state[array_name];

    let data = (details[e.target.name][week]!=null)?details[e.target.name][week]:[];

    if(data.includes(e.target.value)){
      var index = data.indexOf(e.target.value);
      if(index!=-1){
        data.splice(index, 1);
     }
    }else{
      data.push(e.target.value);
    }
    details[e.target.name][week] = data;
    this.setState({[array_name]:details});
  }
  handleCheckboxValue=(e,array_name)=> {
    let details =this.state[array_name];
    let data = (details[e.target.name]!=null)?details[e.target.name]:[];
    if(data.includes(e.target.value)){
      var index = data.indexOf(e.target.value);
      if(index!=-1){
        data.splice(index, 1);
     }
    }else{
      data.push(e.target.value);
    }
    details[e.target.name] = data;
    this.setState({[array_name]:details});
   }
  selectChange = (selectedOption, fieldname,array_name) => {
       var state = this.state[array_name];
       state[fieldname] = selectedOption;
       state[fieldname + '_error'] = false;
       this.setState({[array_name]:state});
   }
   handleChange = (e,array_name) => {
     var inputFields = this.state[array_name];
     if(e.target.type == 'file'){
       inputFields[e.target.name] = e.target.files[0];
       inputFields[e.target.name+'_name'] =  e.target.files[0].name;
       this.setState({[array_name]:inputFields});
     }else{

       this.setState({error: ''});
       inputFields[e.target.name] = e.target.value;
       this.setState({[array_name]:inputFields});
     }

   }
   custom_validation_details = () => {
       var return_var = true;
       var state = this.state['participantDetails'];
       var List = [{ key: 'state' },{ key: 'provide_state' },{ key: 'martialstatus' },{ key: 'livingsituation' }];
       List.map((object, sidx) => {
      if (state[object.key] == null || state[object.key] == '') {
               state[object.key + '_error'] = true;
               this.setState(state);
               return_var = false;
           }
       });
       return return_var;return return_var;
     }
   custom_validation_ability = () => {
       var return_var = true;
       var state = this.state['participantAbility'];
       var List = [{ key: 'cognitive_level' },{ key: 'communication' }];
       List.map((object, sidx) => {
      if (state[object.key] == null || state[object.key] == '') {
               state[object.key + '_error'] = true;
               this.setState(state);
               return_var = false;
           }
       });
       return return_var;
     }
   custom_validation_refer = () => {
       var return_var = true;
       var state = this.state['refererDetails'];
       var List = [{ key: 'relation' }];
       List.map((object, sidx) => {
      if (state[object.key] == null || state[object.key] == '') {
               state[object.key + '_error'] = true;
               this.setState(state);
               return_var = false;
           }
       });
       return return_var;
     }

   submitReferDetails = (e) => {
       e.preventDefault();
       var custom_validate = this.custom_validation_refer({ errorClass: 'tooltip-default' });
       var validator = jQuery("#referral_details").validate({
           // rules: {
           //   firstname: { required: true},
           //   lastname: { required : true},
           //   organisation: { required: true},
           //   email: { required : true},
           //   phonenumber: { required: true},
           //   relation: { required : true},
           // }
        });


        if (jQuery("#referral_details").valid() && custom_validate ) {
          console.log(this.state);
        var str = JSON.stringify(this.state);
       this.props.crmParticipant(str);
         this.setState({ refer: true,   tab_active:false });

    } else {
        validator.focusInvalid();
    }

   }
   submitParticipant=(e)=>{
      e.preventDefault();
   }
   submitParticipantDetails = (e) => {
       e.preventDefault();

       var custom_validate = this.custom_validation_details({ errorClass: 'tooltip-default' });

       var validator = jQuery("#partcipant_details").validate();


       if (!this.state.loading && jQuery("#partcipant_details").valid()  && custom_validate) {
           var str = JSON.stringify(this.state);
           this.props.crmParticipant(str);
           this.setState({ participant_details: true, refer: false,  tab_active:false })
         } else {
             validator.focusInvalid();
         }

   }
   submitParticipantAbility = (e) => {
       e.preventDefault();

       var custom_validate = this.custom_validation_ability({ errorClass: 'tooltip-default' });

        var validator = jQuery("#partcipant_ability").validate();

          if (jQuery("#partcipant_ability").valid()  && custom_validate) {
           console.log('1');
           var str = JSON.stringify(this.state);
           this.props.crmParticipant(str);

           this.setState({ ability: true, refer: false,  tab_active:false,participant_details: false  })
         } else {
             validator.focusInvalid();
         }

   }
   submitParticipantShift = (e) => {
       e.preventDefault();
        var str = JSON.stringify(this.state);


       var validator = jQuery("#partcipant_shift").validate();


       if (!this.state.loading && jQuery("#partcipant_shift").valid()  ) {
           var str = JSON.stringify(this.state);
           console.log(this.state);
           const formData = new FormData();

           formData.append('ndis[]', this.state.participantDetails.ndis_file)
           formData.append('hearing[]', this.state.participantDetails.hearing_file)
           formData.append('data', str)
           this.props.crmParticipantSubmit(formData);
           this.setState({ success: true })
           toast.success(<ToastUndo message={"Participant created successfully"} showType={'s'} />, {
           // toast.success("Participant created successfully", {
             position: toast.POSITION.TOP_CENTER,
             hideProgressBar: true
           });
         } else {
             validator.focusInvalid();
         }

   }

   handleSelect(key) {
     this.setState({ key });
   }

   errorShowInTooltip = ($key, msg,array) => {
     var state = this.state[array];
       return (state[$key + '_error']) ? <div className={'tooltip custom-tooltip fade top in' + ((state[$key + '_error']) ? ' select-validation-error' : '')} role="tooltip">
           <div className="tooltip-arrow"></div><div className="tooltip-inner">{msg}.</div></div> : '';

   }
  handleSelect(key) {
    this.setState({ key });
  }

componentWillMount(){
  this.props.states();
}

  render() {
    if (this.state.success) {
      return (<ProspectiveParticipants />)
    }
console.log(this.state);
      var options = [
      { value: 'one', label: 'One' },
      { value: 'two', label: 'Two' }
    ];
    const Participantsavailable = {
      labels: ['', '', ''],
      datasets: [{
        data: ['250', '333', '250'],
        backgroundColor: ['#ed97fa', '#b968c7', '#702f75'],

      }],

    };
    return (
      <div>
        <CrmPage pageTypeParms={'crm_dashboard'} />
        <div className="container-fluid">


          <div className="row">
            <div className="col-lg-10 col-lg-offset-1">
              <div className="back_arrow py-4 bb-1">
                <a href={ROUTER_PATH + 'admin/dashboard'}><span className="icon icon-back1-ie"></span></a>
              </div>
            </div>
          </div>

          <div className="row _Common_He_a">
            <div className="col-lg-8 col-lg-offset-1 col-xs-9"><h1 className="my-0 color"> Create New Participant</h1></div>

          </div>
          <div className="row "><div className="col-lg-10 col-lg-offset-1"><div className="bt-1"></div></div></div>


          <div className="row">
            <div className="col-lg-10 col-lg-offset-1">
            <ul className="nav nav-tabs create-new-par__ bb-1">
              <li className={(this.state.tab_active) ? 'active' : ''}><a data-toggle="tab" href="#RefererDetails">Referer Details</a></li>
              <li className={(this.state.refer) ? 'active' : ''} ><a data-toggle="tab" href="#ParticipantDetails">Participant Details</a></li>
              <li className={(this.state.participant_details) ? 'active' : ''} ><a data-toggle="tab" href="#ParticipantAbility">Participant Ability</a></li>
              <li className={(this.state.ability) ? 'active' : ''}><a data-toggle="tab" href="#Shift">Shift</a></li>
            </ul>

            <div className="w-100">
              <div className="tab-content">
                {/* 1. Start Referer Details from */}
                <div id="RefererDetails" className= {(this.state.tab_active) ? 'tab-pane active' : 'tab-pane '}>
                  <RefererData sts={this.state.refererDetails}  errorTooltip={this.errorShowInTooltip} updateSelect={this.selectChange} formIds={"referral_details"} options={options} submitReferDetail={this.submitReferDetails} handleChanges={this.handleChange} />
                </div>
                {/* End Referer Details from */}

                {/* 1. Start Participant Details from */}
                <div id="ParticipantDetails" className={(this.state.refer) ? 'tab-pane active' : 'tab-pane'} >
                   <ParticipantDetailsPopup sts={this.state.participantDetails} fileChangedHandlers={this.fileChangedHandler} propData={this.props} errorTooltip={this.errorShowInTooltip} updateSelect={this.selectChange} submitParticipantDetail={this.submitParticipantDetails} handleChanges={this.handleChange}/>
                   </div>
                {/* End Participant Details from */}


                {/* 1. Start Participant Details from */}
                <div id="ParticipantAbility" className={(this.state.participant_details) ? 'tab-pane active' : 'tab-pane'}>
                  <ParticipantAbilityPopup sts={this.state.participantAbility} fileChangedHandlers={this.fileChangedHandler} propData={this.props} errorTooltip={this.errorShowInTooltip} updateSelect={this.selectChange}  submitParticipantAbility={this.submitParticipantAbility} handleChanges={this.handleChange} checkboxHandler={this.handleCheckboxValue}/>
                </div>
                <div id="Shift" className={(this.state.ability) ? 'tab-pane active' : 'tab-pane'}>
                <ParticipantShiftPopup sts={this.state.participantShift} fileChangedHandlers={this.fileChangedHandler} propData={this.props} errorTooltip={this.errorShowInTooltip} updateSelect={this.selectChange}  submitParticipantShift={this.submitParticipantShift} handleChanges={this.handleChange} checkboxHandlerShift={this.handleShiftCheckbox}  checkboxHandler={this.handleCheckboxValue}/>
                </div>
              </div>



            </div>

            </div>
          </div>

        </div>
      </div>

    );
  }
}

const mapStateToProps = state => {
  return {
    showPageTitle: state.DepartmentReducer.activePage.pageTitle,
    showTypePage: state.DepartmentReducer.activePage.pageType,
    participantDatas:state.CrmParticipantReducer.participantData,
    statesValue:state.CrmParticipantReducer.states2
  }
};
const mapDispatchtoProps = (dispach) => {
  return {
    allLatestUpdate: () => dispach(allLatestUpdate()),
    crmParticipant: (data) => dispach(crmParticipant(data)),
    crmParticipantSubmit: (data) => dispach(crmParticipantSubmit(data)),
    states: () => dispach(states()),
  }
}
export default connect(mapStateToProps, mapDispatchtoProps)(NewParticipant);
