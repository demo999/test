import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import 'react-table/react-table.css';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { ROUTER_PATH } from '../../../config.js';
import { connect } from 'react-redux';
import CrmPage from './CrmPage';


class LocationAnalytics extends Component {
    constructor(props, context) {
        super(props, context);

        this.handleSelect = this.handleSelect.bind(this);

        this.state = {
            key: 1, filterVal: ''
        };
    }

    handleSelect(key) {
        this.setState({ key });
    }

    render() {
        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];
        return (

            <div className="container-fluid">
            <CrmPage pageTypeParms={'report_location_analytics'}/>
                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1">
                        <div className="back_arrow py-4 bb-1">
                        <Link to={ROUTER_PATH + 'admin/crm/participantadmin'}><span className="icon icon-back1-ie"></span></Link>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1">
                        <div className="row d-flex py-4">
                            <div className="col-md-9">
                                <div className="h-h1">
                                    {this.props.showPageTitle}
                                    </div>
                            </div>
                        </div>
                        <div className="row"><div className="col-md-12"><div className="bt-1"></div></div></div>
                    </div>
                </div>
              
                <div className="row mt-5">
                   

                    <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                        <div className="row d-flex">
                            <div className="col-md-12">
                                <div className="big-search l-search">
                                    <input />
                                    <button><span className="icon icon-search1-ie"></span></button>
                                </div>
                            </div>
                        </div>
                        <div className="row my-3">
                            <div className="col-md-3">
                                <div className="s-def1  my-3">
                                    <Select
                                        name="view_by_status"
                                        options={options}
                                        required={true}
                                        simpleValue={true}
                                        searchable={false}
                                        clearable={false}
                                        placeholder="Filter by: Unread"
                                        onChange={(e) => this.setState({ filterVal: e })}
                                        value={this.state.filterVal}
                                    />
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="s-def1 my-3">
                                    <Select
                                        name="view_by_status"
                                        options={options}
                                        required={true}
                                        simpleValue={true}
                                        searchable={false}
                                        clearable={false}
                                        placeholder="Filter by: Unread"
                                        onChange={(e) => this.setState({ filterVal: e })}
                                        value={this.state.filterVal}
                                    />
                                </div>
                            </div>
                            <div className="col-md-3 col-md-offset-3">
                                <div className="s-def1  my-3">
                                    <Select
                                        name="view_by_status"
                                        options={options}
                                        required={true}
                                        simpleValue={true}
                                        searchable={false}
                                        clearable={false}
                                        placeholder="Filter by: Unread"
                                        onChange={(e) => this.setState({ filterVal: e })}
                                        value={this.state.filterVal}
                                    />
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <img src="/assets/images/map.jpg" alt="map" className="w-100" />
                            </div>
                        </div>


                    </div>

                </div>




            </div>

        );
    }
}

const mapStateToProps = state => {
      return {
        showPageTitle: state.DepartmentReducer.activePage.pageTitle,
        showTypePage: state.DepartmentReducer.activePage.pageType,
  
      }
  };
export default connect(mapStateToProps)(LocationAnalytics);
