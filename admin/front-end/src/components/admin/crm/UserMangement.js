import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ReactTable from "react-table";
import 'react-table/react-table.css';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import ReactResponsiveSelect from 'react-responsive-select';
import { checkItsNotLoggedIn, postData, getQueryStringValue, getOptionsCrmParticipant, getOptionsCrmMembers, handleChangeChkboxInput,reFreashReactTable} from '../../../service/common.js';
import { staffDisableAccount,staffAllocatedAccount } from '../../../dropdown/CrmDropdown.js';
import Modal from 'react-bootstrap/lib/Modal';
import jQuery from "jquery";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import AddStaffMember from './AddStaffMember';
import {BASE_URL, ROUTER_PATH} from '../../../config';
import CrmPage from './CrmPage';
import { connect } from 'react-redux';
import Pagination from "../../../service/Pagination.js";
import { ToastUndo } from 'service/ToastUndo.js'
import { confirmAlert, createElementReconfirm } from 'react-confirm-alert';


const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {

        // request json
        var Request = JSON.stringify({ pageSize: pageSize, page: page, sorted: sorted, filtered: filtered });
        postData('crm/CrmStaff/list_user_management', Request).then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.count)
            };
            resolve(res);
        });
    });
};

const getDepartmentList = () => {
    return new Promise((resolve, reject) => {

        // request json
        var Request = JSON.stringify({});
        postData('crm/CrmDepartment/get_all_department', Request).then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.count)
            };
            setTimeout(() => resolve(res), 10);
        });
    });
};


const multiSelectOptionMarkup = (text) => (
    <div>
        <span className="rrs_select"> {text}</span>
        <span className="checkbox">
            <i className="icon icon-star2-ie"></i>
        </span>

    </div>
);

// By default no caret icon is supplied - any valid jsx markup will do
const caretIcon = (
    <i className="icon icon-edit1-ie"></i>
);

class UserMangement extends Component {
    constructor(props, context) {
        super(props, context);
        this.handleSelect = this.handleSelect.bind(this);

        this.state = {
            key: 1, filterVal: '',
            showModal: false,
            usersList:[],
            searching: false,
            inactive: false,
            incomplete: false,
            depList:[],showCreateModal:false
        };
        this.reactTable = React.createRef();
    }



    componentWillMount(){
       getDepartmentList().then(res => {
           this.setState({
               depList: res.rows,
               pages: res.pages,
               loading: false
           });
       });
    }


    fetchData = (state, instance) => {
        // function for fetch data from database
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {

            this.setState({
                usersList: res.rows,
                pages: res.pages,
                loading: false
            });
        });
    }
    closeCreateModal = (param) => {
        this.setState({ showCreateModal: false });

        if(param){

            reFreashReactTable(this, 'fetchData');
          }
    }
    handleSelect(key) {
        this.setState({ key });
    }
    options = (data)=>{
      let opt =[];
      opt.push({ text: 'Select Allocations:', optHeader: true });
      data.map((key,i) =>(
          opt.push({
            value: data[i].value,
            text: data[i].label,
            markup: multiSelectOptionMarkup(data[i].label),
        })


      ))

      return opt;
    }

    handleSelectedDropdown(value) {
        this.setState({ allocatedTo: value });
    }


    showModal = (a) => {
        this.setState({ showModal: true,a:a })
    }
    closeModal = () => {

        this.setState({ showModal: false })
    }



    submitSearch = (e) => {
        e.preventDefault();

        var srch_ary = {search: this.state.search, inactive: this.state.inactive, incomplete: this.state.incomplete , filterVal: this.state.filterVal }
        this.setState({filtered: srch_ary});

    }

    searchData = (key, value) => {
        var srch_ary = {search: this.state.search,inactive: this.state.inactive, filterVal: this.state.filterVal };

        srch_ary[key] = value;
        this.setState(srch_ary);
        this.setState({filtered: srch_ary});
    }
    EnableRecruiter=(id)=> {
          this.setState({
              staff_disable: {
                  staff_id: id,
              }
          });
          let msg = <span>Are you sure you want to enable this account?</span>;
          return new Promise((resolve, reject) => {
              confirmAlert({
                  customUI: ({ onClose }) => {
                      return (
                          <div className='custom-ui'>
                              <div className="confi_header_div">
                                  <h3>Confirmation</h3>
                                  <span className="icon icon-cross-icons" onClick={() => {
                                      onClose();
                                      resolve({ status: false })
                                  }}></span>
                              </div>
                              <p>{
                                  msg}</p>
                              <div className="confi_but_div">
                                  <button className="Confirm_btn_Conf" onClick={
                                      () => {
                                          postData('crm/CrmStaff/enable_crm_user', id).then((result) => {
                                              if (result) {
                                                  toast.success(<ToastUndo message='Enabled successfully'  showType={'s'} />, {
                                                      // toast.success("Note Deleted successfully", {
                                                      position: toast.POSITION.TOP_CENTER,
                                                      hideProgressBar: true
                                                  });
                                                  onClose();
                                                  this.setState({ success: true })
                                              } else {
                                                  toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                                                      // toast.error(result.error, {
                                                      position: toast.POSITION.TOP_CENTER,
                                                      hideProgressBar: true
                                                  });

                                              }

                                          })
                                      }}>Confirm</button>
                                  <button className="Cancel_btn_Conf" onClick={
                                      () => {
                                          onClose();
                                          resolve({ status: false });
                                      }}> Cancel</button>
                              </div>
                          </div>
                      )
                  }
              })
          });
      }

    render() {
    const { data, pages, loading } = this.state;
        var userStatus = [
            { value: '1', label: 'Active' },
            { value: '0', label: 'Disabled' },
            { value: 'All', label: 'All' }
        ];

      const columns = [
          { Header: "Name:", accessor: "name", },
          { Header: "HCMGR ID:", accessor: "ocs_id", },
          { Header: "Service Area:", accessor: "service_area", },
          { Header: "Start Date:", accessor: "created", },
        //  { Header: "End Date:", accessor: "updated", headerStyle: { border: "0px solid #fff" } },
          {
              expander: true,
              Header: () => <strong></strong>,
              width: 55,
              headerStyle: { border: "0px solid #fff" },
              Expander: ({ isExpanded, ...rest }) =>
                  <div className="rec-table-icon">
                      {isExpanded
                          ? <i className="icon icon-arrow-up"></i>
                          : <i className="icon icon-arrow-down"></i>}
                  </div>,
              style: {
                  cursor: "pointer",
                  fontSize: 25,
                  padding: "0",
                  textAlign: "center",
                  userSelect: "none"
              },
          }
      ];


        const options2 = [
            { text: 'Select Allocations:', optHeader: true },
            {
                value: 'Alfa Romeo Alfa RomeoAlfa RomeoAlfa RomeoAlfa RomeoAlfa Romeo',
                text: 'Alfa Romeo Alfa RomeoAlfa RomeoAlfa RomeoAlfa RomeoAlfa Romeo',
                markup: multiSelectOptionMarkup('Alfa Romeo Alfa RomeoAlfa RomeoAlfa RomeoAlfa RomeoAlfa Romeo'),
            },
            {
                value: 'fiat',
                text: 'Fiat',
                markup: multiSelectOptionMarkup('Fiat'),
            },
            {
                value: 'ferrari',
                text: 'Ferrari',
                markup: multiSelectOptionMarkup('Ferrari'),
            },
            {
                value: 'mercedes',
                text: 'Mercedes',
                markup: multiSelectOptionMarkup('Mercedes'),
            },
            {
                value: 'tesla',
                text: 'Tesla',
                markup: multiSelectOptionMarkup('Tesla'),
            },
            {
                value: 'volvo',
                text: 'Volvo',
                markup: multiSelectOptionMarkup('Volvo'),
            },
            {
                value: 'zonda',
                text: 'Zonda',
                markup: multiSelectOptionMarkup('Zonda'),
            },
        ]
          const pageSize = (typeof(this.state.usersList)=='undefined'?0:this.state.usersList.length)
          const subComponentDataMapper = row => {
            let data = row.row._original;
            return(
              <div className="col-md-12 task_table">


                  <div className="row d-flex by-1">
                      <div className="col-md-6  col-lg-5 br-1 py-3 d-flex my-3">
                          <div className="mx-4">
                              <div className="Staff_U_img"><img src="/assets/images/admin/dummy.png" /></div>
                              <div className="mt-3 Partt_d1_txt_2">User Status: <b>{(data.user_status==1)?'Active':'Disabled'}</b></div>
                              {// <a className="btn-3 mt-3" onClick={() => this.setState({ showCreateModal: true,staffData :data,id:data.ocs_id,pagetitile:'Update Staff Member' })}>Edit</a>
                              }
                          </div>
                          <div>
                              <div className="Partt_d1_txt_1"><strong>{data.name}</strong></div>
                              <div className="Partt_d1_txt_2">{data.position}</div>
                              <div className="Partt_d1_txt_2">HCMGR-ID:  <strong>{data.ocs_id}</strong></div>

                              <div className="Partt_d1_txt_2 mt-4"><b>Contact:</b></div>
                              <div className="Partt_d1_txt_2"> Phone:  <strong>{(data.PhoneInput.length > 0) ? <span>
                                  {data.PhoneInput.map((value, idx) => (
                                      <div key={idx + 2}> <b>   {value.name}</b></div>
                                  ))} </span> : ''
                              }</strong></div>
                              <div className="Partt_d1_txt_2"> Email:  <strong>{(data.EmailInput.length > 0) ? <span>
                                  {data.EmailInput.map((value, idx) => (
                                      <div key={idx + 5}> <b> {value.name}</b></div>
                                  ))} </span> : ''
                              }</strong></div>
                          </div>

                      </div>

                      <div className="col-md-6  col-lg-7 pb-3 my-3">
                          <div className="row">
                              <div className="col-lg-6 col-md-12 px-5">
                                  <div className="my-3">Access Permissions:</div>
                                  <div className="mb-2 Partt_d1_txt_2"><b>All</b></div>
                              </div>
                              <div className="col-lg-6 col-md-12 px-5">
                                  <div className="my-3">Allocated Serive Area:</div>
                                  <div className="mb-2 Partt_d1_txt_2"><b>NDIS</b></div>
                                  <div className="my-3">Preferred Service Area:</div>
                                  <div className="mb-2 Partt_d1_txt_2"><b>NDIS</b></div>
                              </div>

                          </div>
                      </div>

                  </div>

                  <div className="row pb-4 mt-3">
                      <div className="col-md-2"><Link className="btn-3" to={ROUTER_PATH + 'admin/crm/StaffDetails/'+data.ocs_id} >Staff Details</Link></div>
                      <div className="col-md-10 task_table_footer pt-2">
                          {(data.user_status==1)?<a onClick={()=>this.showModal(data.ocs_id)}><u>Disable / Pause User Account</u></a>
                          :<a onClick={()=>this.EnableRecruiter(data.ocs_id)}><u> Enable User Account</u></a>}

                      </div>
                  </div>
              </div>
            );

          }
        return (
            <div className="container-fluid">
            <CrmPage pageTypeParms={'user_staff_members'} />

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1 col-md-12">
                        <div className="back_arrow py-4 bb-1">
                            <Link to={ROUTER_PATH + 'admin/crm/participantadmin'}><span className="icon icon-back1-ie"></span></Link>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1  col-md-12">
                        <div className="row d-flex">
                            <div className="col-md-12 align-self-center py-4 bb-1">
                                <div className="h-h1 ">{this.props.showPageTitle}</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row pt-4 pb-3">
                    <div className="col-lg-2 col-lg-offset-9 col-md-3  col-md-offset-9">
                        <a className="v-c-btn1" onClick={() => this.setState({ showCreateModal: true,staffData:'',pagetitile:'Create New Staff Member' })}><span>Add Staff Member</span> <i className="icon icon-add3-ie"></i></a>
                    </div>
                    <AddStaffMember
                        showModal={this.state.showCreateModal}
                        closeModal={this.closeCreateModal}
                        staffData={this.state.staffData}
                        title={this.state.pagetitile}
                    />
                </div>

                <div className="row d-flex">


                    <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                      <form onSubmit={this.submitSearch}>
                        <div className="row d-flex">
                            <div className="col-md-8">
                                <div className="big-search l-search">
                                    <input type="text" name="search" value={this.state.search || ''} onChange={(e) => this.setState({'search': e.target.value})}  />
                                    <button  type="submit"><span className="icon icon-search1-ie"></span></button>
                                </div>
                            </div>
                            <div className="col-md-1 d-inline-flex align-self-center justify-content-end">Filter by:</div>
                            <div className="col-md-3 d-inline-flex align-self-center">
                                <div className="s-def1 w-100">
                                    <Select
                                        name="view_by_status"
                                        options={userStatus}
                                        required={true}
                                        simpleValue={true}
                                        searchable={false}
                                        clearable={false}
                                        placeholder="Filter by: Unread"
                                        onChange={(e) => this.searchData('filterVal', e)}
                                      //  onChange={(e) => this.setState({ filterVal: e })}
                                        value={this.state.filterVal}
                                    />
                                </div>
                            </div>
                        </div>
                            </form>


                        <div className="row">
                            <div className="col-md-12 re-table re-table_select mt-4">
                                <ReactTable
                                PaginationComponent={Pagination}
                                    ref={this.reactTable}
                                    columns={columns}
                                    data={this.state.usersList}
                                    onFetchData={this.fetchData}
                                    pages={this.state.pages}
                                    loading={this.state.loading}
                                    defaultPageSize={10}
                                    filtered={this.state.filtered}
                                    className="-striped -highlight"
                                    noDataText= 'No Users found'
                                    minRows={1}
                                    previousText={<span className="icon icon-arrow-left privious"></span>}
                                    nextText={<span className="icon icon-arrow-right next"></span>}
                                    showPagination={true}
                                    SubComponent={subComponentDataMapper}

                                />

                            </div>
                        </div>


                        <DisableStaff showModal={this.state.showModal} staffId={this.state.a}  closeModal={this.closeModal}  />

                </div>
            </div>


            </div >


        );
    }
}
const mapStateToProps = state => {
    return {
      showPageTitle: state.DepartmentReducer.activePage.pageTitle,
      showTypePage: state.DepartmentReducer.activePage.pageType
    }
};
export default connect(mapStateToProps)(UserMangement);

export class DisableStaff extends Component{
  constructor(props, context) {
      super(props, context);
       this.state = {
          key: 1,
          filterVal: '',
          showModal:  false,
          staff_detail:[],
          loading: false,
          staff_disable_note:'',
          account_allocated_staff_to:'',
          disableAccount:'',
          account_allocated_staff_to:'',
          allocatedAccount:'',
          staff_id:'',
          staff_name:'',
          staff_disable:{},
          custom_search:false

          //staff_disable:[{disableAccount:'', account_allocated_staff_to:'', allocatedAccount:'',staff_id:'',staff_name:'',staff_disable_note:''}]

      };



  }

  componentWillReceiveProps(newProps) {
     this.setState({newProps}, () => {
          if(newProps.showModal){
              this.get_staff_detail();
              this.setState({staff_detail: []});
            //  this.setState({staff_id:this.state.staff_detail.id});
            //  this.get_staff_disable();
            //  this.setState({staff_disable: []});
          }
     });
  }

  get_staff_detail = () => {
    this.setState({loading: true}, () => {
        postData('crm/CrmStaff/get_staff_details', {id:this.props.staffId}).then((result) => {
            if (result.status) {
              this.setState({staff_detail: result.data, loading: false})
            }
        });
    });
  }



submit = (e) => {
      this.setState({staff_disable: {
        disableAccount:this.state.disableAccount,
        account_allocated_staff_to: this.state.account_allocated_staff_to.value,
        allocatedAccount: this.state.allocatedAccount,
        staff_id:this.props.staffId,
        staff_disable_note:this.state.staff_disable_note,
        update_mode:'1'} });
      e.preventDefault()
       jQuery('#updateDisableStaff').validate({ignore: []});
       if(jQuery('#updateDisableStaff').valid()){
          this.setState({loading: true},() => {
          postData('crm/CrmStaff/add_update_disable_recruiter_staff', this.state.staff_disable).then((result) => {

          if (result.status) {
            toast.success(<ToastUndo message={result.msg} showType={'s'} />, {
            //   toast.success(result.msg, {
                  position: toast.POSITION.TOP_CENTER,
                  hideProgressBar: true
              });
              this.props.closeModal(true);
          }
          else
          {
            toast.error(<ToastUndo message={result.error} showType={'e'} />, {
            //   toast.error(result.error, {
                  position: toast.POSITION.TOP_CENTER,
                  hideProgressBar: true
              });
          }
          this.setState({loading: false})
      });
          });
     }
  }



  render(){
    const columns = [
        { Header: "Name:", accessor: "FullName", },
        { Header: "Stage:", accessor: "stage_id", },
       { Header: "Allocated to:", accessor: "user", },

      //  { Header: "End Date:", accessor: "updated", headerStyle: { border: "0px solid #fff" } },

    ];
    let assigned = (typeof(this.state.staff_detail.assigned_participant)=='undefined')?[]:this.state.staff_detail.assigned_participant;
    console.log(assigned);
     return(
        <div className={this.props.showModal ? 'customModal show' : 'customModal'}>
        <div className="custom-modal-dialog Information_modal task_modal">
        <form id="updateDisableStaff">
            <div className="custom-modal-header by-1">
                <div className="Modal_title">Disable Recruiter</div>

                <i className="icon icon-close1-ie Modal_close_i" onClick={this.props.closeModal}></i>
            </div>
            <div className="custom-modal-body w-80 mx-auto">
                <div className="row">
                    <div className="col-md-12 my-3 mt-5">
                        <h4 className="my-0">Disable <b>{this.state.staff_detail.FullName}</b> account:</h4>
                    </div>
                </div>
                <div className="row">

                    <div className="Dis_R_Mdiv">

                        <div className="col-md-6">
                            <div className="row d-flex">
                                <div className="col-md-8 px-0">
                                    <div className="s-def1 w-100">

                                        <Select data-rule-required={true}
                                            name="disableAccount"
                                            options={staffDisableAccount(0)}
                                            required={true}
                                            simpleValue={true}
                                            searchable={false}
                                            clearable={false}
                                            placeholder="Filter by: Unread"
                                            onChange={(e) => this.setState({ disableAccount: e })}
                                            value={this.state.disableAccount}
                                        />

                                    </div>
                                </div>
                              {  // <div className="col-md-4 d-inline-flex align-self-center px-0">
                                //     <div className="Dis_R_div1"></div>
                                // </div>
                              }
                            </div>
                        </div>

                      {  // <div className="col-md-6 px-0">
                        //     <div className="Dis_R_div2">
                        //         <p>- Temp Pause User Account</p>
                        //         <p>- Permanently Disable</p>
                        //         <p>- Schedule Pause or Disable</p>
                        //     </div>
                        // </div>
                      }

                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12 my-3">
                        <h4 className="my-0">Account Allocations:</h4>
                        <label className="title_input pl-0">Where would you like to Re-allocate all recruiters current assigned Incomplete participants to:</label>
                    </div>
                </div>

                <div className="row">

                    <div className="Dis_R_Mdiv">

                        <div className="col-md-6">
                            <div className="row d-flex">
                                <div className="col-md-8 px-0">
                                    <div className="s-def1 w-100">
                                        <Select
                                            name="allocatedAccount"
                                            options={staffAllocatedAccount(0)}
                                            required={true}
                                            simpleValue={true}
                                            searchable={false}
                                            clearable={false}
                                            placeholder="Filter by: Unread"
                                            onChange={(e) => this.setState({ allocatedAccount: e },()=>(e==2)?this.setState({custom_search:true}):this.setState({custom_search:false}))}
                                            value={this.state.allocatedAccount}
                                        />
                                    </div>
                                </div>
                              {  // <div className="col-md-4 d-inline-flex align-self-center px-0">
                                //     <div className="Dis_R_div1"></div>
                                // </div>
                              }
                            </div>
                        </div>

                      {  // <div className="col-md-6 px-0">
                        //     <div className="Dis_R_div2">
                        //         <p>- Next Available Staff Member</p>
                        //         <p>- By Department Preference</p>
                        //         <p>- Custom Selection (Search) </p>
                        //         <p>- Lorem Ipsum is simply dummy text of the printing </p>
                        //     </div>
                        // </div>
                      }

                    </div>
                </div>

                <div className="row">
                <div className="col-md-12"><h4 className="my-2">Allocated Participant:</h4></div>
                  <div className="">
                  {(this.state.custom_search)?
                   <div className="search_icons_right modify_select">
                      <Select.Async
                          cache={false}
                          clearable={false}
                          name="account_allocated_staff_to"
                          required={true}
                          value={this.state.account_allocated_staff_to}
                          loadOptions={getOptionsCrmMembers}
                          placeholder='Search'
                          onChange={(e) => this.setState({ account_allocated_staff_to: e })}
                      />

                      </div>
                    :''}
                    </div>
                    <div className="col-md-12 task_table">

                        <ReactTable
                            columns={columns}
                            defaultPageSize={3}
                            data={assigned}
                            minRows={2}
                            showPagination={false}
                        />
                    </div>
                    </div>



                <div className="row pt-5">
                <div class="col-md-12 pb-4"><div class="border-das_line"></div></div>
                <div className="col-md-12 my-3">
                    <h4 className="my-0">Add Relevant Notes:</h4>
                </div>
                <div className="col-md-12 task_N_txt">
                    <textarea className="form-control" value={this.state.staff_disable_note} onChange={(e) => handleChangeChkboxInput(this, e)}     name="staff_disable_note" data-rule-required="true"  wrap="soft"></textarea>
                </div>

            </div>
            </div>

        <div className="custom-modal-footer bt-1 mt-5 px-0 pb-4">
            <div className="row d-flex justify-content-end">
            {//    <div className="col-md-2"><a className="btn-1">Disable</a></div>
            }
                <div className="col-md-3"><button disabled={this.state.loading} onClick={this.submit} className="btn-1 w-100">Disable</button></div>
            </div>
        </div>
        </form>

        </div>


      </div>

    )
  }
}
