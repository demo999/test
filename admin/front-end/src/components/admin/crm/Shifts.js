import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {ProgressBar} from 'react-bootstrap';
import {ROUTER_PATH} from '../../../config.js';
import {PartShiftModal} from './ParticipantShiftModal';
import CrmPage from './CrmPage';
import { connect } from 'react-redux'


class Shifts extends Component {
  constructor(props, context) {
      super(props, context);
      this.participantDetailsRef = React.createRef();
      this.state = { participant_id:'',
      PartShiftModal: false};
  }
  componentDidMount(){
    this.setState({participant_id:this.props.props.match.params.id});
    this.participantDetailsRef.current.wrappedInstance.getParticipantDetails(this.props.props.match.params.id);
  }

    closePartShiftModal(){
        this.setState({PartShiftModal:false});
    }

    showPartShiftModal(){
        this.setState({PartShiftModal:true});
    }

    render() {
        const now = 60;
        return (
            <div className="container-fluid">
            <CrmPage ref={this.participantDetailsRef} pageTypeParms={'participant_shift'} />
                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1 col-md-12">
                        <div className="back_arrow py-4 bb-1">
                        <Link to={ROUTER_PATH + 'admin/crm/prospectiveparticipants'}><span className="icon icon-back1-ie"></span></Link>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1  col-md-12">
                        <div className="row d-flex py-4">
                            <div className="col-md-6 align-self-center br-1">
                                <div className="h-h1 ">
                                   {this.props.showPageTitle}
                            </div>
                            </div>
                            <div className="col-md-6">
                                <div className="Lates_up_1">
                                    <div className="Lates_up_a col-md-3 align-self-center">
                                        Latest
                                        Update:
                                    </div>
                                    <div className="col-md-9 justify-content-between pr-0">
                                        <div className="Lates_up_b">
                                            <div className="Lates_up_txt"><b>Stage 2:</b> Attachment added- Service Agreement Doc</div>
                                            <div className="Lates_up_btn br-1 bl-1"><i className="icon icon-view1-ie"></i><span>View Attachment</span></div>
                                            <div className="Lates_up_btn"><i className="icon icon-view1-ie"></i><span>View all Updates</span></div>
                                        </div>
                                        <div className="Lates_up_2">
                                            <div className="Lates_up_txt2 btn-1">Susan McDonald (Recruiter)</div>
                                            <div className="Lates_up_time_date"> Date: 01/01/01 - 11:32AM</div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div className="row"><div className="col-md-12"><div className="bt-1"></div></div></div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1 col-md-12 mt-5">
                        <div className="progress-b1">
                        <ProgressBar className="progress-b2" now={now} label={'Intake Progress: ' + `${now}%`+ 'Complete'} />
                        </div>
                    </div>
                </div>

                <div className="row">

              
                    <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="shift_h1 py-2 by-1">Shifts</div>
                            </div>
                        </div>
                        <div className="row">

                            <div className="col-md-12">
                                <div className="Partt_d1_txt_3 my-2"><strong>Shift Start Date:  </strong><span>01/01/01</span></div>
                            </div>
                            <div className="col-md-12">
                                <div className="Partt_d1_txt_3 mt-4"><strong>Required Shifts:</strong></div>
                                <div className="shift_week_1">
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Sun</div>
                                        <div className="Shift_day_blank am_shift"></div>
                                        <div className="Shift_day_blank pm_shift"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Mon</div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank so_shift"></div>
                                        <div className="Shift_day_blank na_shift"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Tue</div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank am_shift"></div>
                                        <div className="Shift_day_blank pm_shift"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Wed</div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank so_shift"></div>
                                        <div className="Shift_day_blank na_shift"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Thur</div>
                                        <div className="Shift_day_blank am_shift"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank so_shift"></div>
                                        <div className="Shift_day_blank"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Fri</div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank na_shift"></div>
                                        <div className="Shift_day_blank"></div>
                                    </div>
                                    <div className="shift_days_1">
                                        <div className="Shift_day_n">Sat</div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank"></div>
                                        <div className="Shift_day_blank na_shift"></div>
                                        <div className="Shift_day_blank"></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-12 d-flex Shift_times_div my-4">
                                <div><span className="am_shift"></span>AM (6am - 12pm)</div>
                                <div><span className="pm_shift"></span>PM (12pm - 10pm)</div>
                                <div><span className="so_shift"></span>S/O (10pm - 6am, non active)</div>
                                <div><span className="na_shift"></span>A/N (10pm - 6am)</div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12 text-right">
                                <span className="add_btn1"><i className="icon icon-ios-plus-empty"></i></span>
                                <span className="add_btn1"><i className="icon icon-ios-minus-empty"></i></span>
                            </div>
                        </div>
                        <div className="row mt-4">
                            <div className="col-md-12">
                                <div className="shift_h1 py-2 by-1">Shift Requirements</div>
                            </div>
                            <div className="col-md-12">
                                <div className="Partt_d1_txt_3 py-4 bb-1">
                                    In Home Suppport, Community Access, Personla Care, Other: [Open Text box]</div>
                            </div>
                        </div>
                        <div className="row d-flex justify-content-end">
                            <div className="col-md-3 mt-3"> <span onClick={()=>{this.showPartShiftModal()}} className="btn-3">Edit Participants Shifts</span></div>
                        </div>

                    </div>


                </div>
                <PartShiftModal showModal={this.state.PartShiftModal} handleClose={()=>this.closePartShiftModal()} />
            </div>
        );
    }
}
const mapStateToProps = state => {
      return {
        showPageTitle: state.DepartmentReducer.activePage.pageTitle,
        showTypePage: state.DepartmentReducer.activePage.pageType,
  
      }
  };
export default connect(mapStateToProps)(Shifts);
