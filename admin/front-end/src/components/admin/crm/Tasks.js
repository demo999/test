import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import ReactTable from "react-table";
import 'react-table/react-table.css';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { checkItsNotLoggedIn, postData, getQueryStringValue,getOptionsCrmParticipant,getOptionsCrmMembers,reFreashReactTable } from '../../../service/common.js';
import jQuery from "jquery";
import DatePicker from 'react-datepicker';
import moment from 'moment';
import {BASE_URL, ROUTER_PATH} from '../../../config';
import { ToastContainer, toast } from 'react-toastify';
import { connect } from 'react-redux';
import CrmPage from './CrmPage';
import Pagination from "../../../service/Pagination.js";
import { ToastUndo } from 'service/ToastUndo.js'

const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {
      // request json
        var Request = JSON.stringify({ pageSize: pageSize, page: page, sorted: sorted, filtered: filtered });
        postData('crm/CrmTask/list_task', Request).then((result) => {
            let filteredData = result.data;

            const res = {
                rows: filteredData,
                pages: (result.count)
            };
            resolve(res);
        });

    });
};

const requestTaskPriority = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {

        // request json
        var Request = JSON.stringify({ pageSize: pageSize, page: page, sorted: sorted, filtered: filtered });
        postData('crm/CrmTask/get_task_priority_list', Request).then((result) => {
            let filteredData2 = result.data;

            const res = {
                rows: filteredData2,
                pages: (result.count)
            };
            setTimeout(() => resolve(res), 10);
        });

    });
};


class CreateTask extends Component{
  constructor(props, context) {
      super(props, context);
      this.child = React.createRef();
      this.state = {

          crm_participant_id:'',
          task_name:'',
          due_date:'',
          task_note:'',
          priority:'',
          task_id:'',
          assign_to:'1',
          action:'1',
      };
  }

  componentWillMount(){
      requestTaskPriority(
          this.state.pageSize,
          this.state.page,
          this.state.sorted,
          this.state.filtered
      ).then(res => {
        //console.log(res.rows);
          this.setState({
              task_priority: res.rows,
              pages: res.pages,
              loading: false
          });
      });
    }




    submit = (e) => {
        e.preventDefault();

          var custom_validate = this.custom_validation({  errorClass: 'tooltip-default'});


        var validator = jQuery("#create_task").validate({ignore: []});

        if (jQuery("#create_task").valid() && custom_validate) {
            var str = JSON.stringify(this.state);
            postData('crm/CrmTask/create_task', str).then((result) => {
                  if (result.status) {
                    toast.success(<ToastUndo message={"Task created successfully"} showType={'s'} />, {
                    // toast.success("Task created successfully", {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                      });

                      this.setState({success: true})
                      this.props.closeModal(true);
                      } else{
                        toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                        //   toast.error(result.error, {
                              position: toast.POSITION.TOP_CENTER,
                              hideProgressBar: true
                            });
                         this.closeModal();
                          }
                      this.setState({loading: false})
              });


        } else {
            validator.focusInvalid();
        }
    }
    closeModal = ()=>{
      this.props.closeModal();

    }
    handleChange = (e) => {
        var state = {};
        this.setState({error: ''});
        state[e.target.name] =  e.target.value;
        this.setState(state);
    }
    custom_validation = () => {
    var return_var = true;
    var state = {};
    var List = [ {key:'due_date'},{key:'crm_participant_id'}];
    List.map((object, sidx) => {
            if(object.key == 'crm_participant_id'){

            if((this.state['due_date'] == undefined || this.state['due_date'] == '')){
                    state[object.key+'_error'] = true;
                    this.setState(state);
                    return_var = false;
                }
            } else if(this.state[object.key] == null || this.state[object.key] == undefined || this.state[object.key] == ''){
                 state[object.key+'_error'] = true;
                 this.setState(state);
                 return_var = false;
            }
        });
        return return_var;
      }
      selectChange = (selectedOption, fieldname) => {
          var state = {};
          state[fieldname] = selectedOption;
          state[fieldname+'_error'] = false;

          this.setState(state);
      }
      errorShowInTooltip = ($key, msg) => {
        //alert($key);
       return (this.state[$key + '_error'])? <div className={'tooltip custom-tooltip fade top in'+ ((this.state[$key + '_error'])? ' select-validation-error': '')} role="tooltip">
        <div className="tooltip-arrow"></div><div className="tooltip-inner">{msg}.</div></div> : '';

      }

  render(){


    var priority = this.state.task_priority;
    var options="";

    return(
      <div className="custom-modal-body w-100 mx-auto">
      <div className="custom-modal-header by-1">
          <div className="Modal_title">Create Task</div>
          <i className="icon icon-close1-ie Modal_close_i" onClick={this.closeModal}></i>
      </div>
      <form id="create_task">

          <div className="row">
          <div className="row my-3">
                        <div className="col-md-12"><h4 className="my-2">Search for a Participant:</h4></div>
                                <div className="col-md-8">
                                <div className="search_icons_right modify_select">
                                <Select.Async
                                    cache={false}
                                    clearable={false}
                                    name="crm_participant_id" required={true}
                                    value={this.state['crm_participant_id']}
                                    loadOptions={getOptionsCrmParticipant}
                                    placeholder='Search'
                                    onChange={(e)=>this.selectChange(e,'crm_participant_id')}
                                    className="custom_select"
                                />
                                {this.errorShowInTooltip('crm_participant_id', 'Select Participant')}
                                {
                                  // <button><span className="icon icon-search1-ie"></span></button>
                                }
                                </div>
                                </div>
                        </div>
              <div className="col-md-4 my-3">
                  <h4 className="my-2">Task Name:</h4>
                  <input   type="text" name="task_name" data-rule-required='true' data-msg-required="Add Task Name" value={this.state['task_name'] || ''}   onChange={this.handleChange} className="default-input" />
              </div>
              <div className="col-md-3 my-3">
              <h4 className="my-2">Priority:</h4>
                  <div className="s-def1 w-100">
                      <Select
                          name="view_by_status"
                          className="custom_select"
                          options={priority}
                        //  onFetchData = {this.fetchData2}
                          required={true}
                          name="priority"
                          simpleValue={true}
                          searchable={false}
                          clearable={false}
                          value={this.state['priority']}
                          placeholder="Priority"
                          onChange={(e)=>this.selectChange(e,'priority')}

                      />


                      {this.errorShowInTooltip('priority', 'Select Priority')}
                  </div>
              </div>
              <div className="col-md-2 my-3">
                  <h4 className="my-2">Due Date:</h4>
                  <div className="s-def1 w-100">
                  <DatePicker showYearDropdown scrollableYearDropdown
                  yearDropdownItemNumber={110} dateFormat="DD/MM/YYYY"
                   required={true} data-placement={'bottom'} utcOffset={0}
                   minDate={moment()}
                    name="due_date"
                    onChange={(e)=>this.selectChange(e,'due_date')}
                    selected={this.state['due_date'] ? moment(this.state['due_date'], 'DD-MM-YYYY') : null}
                    className="text-center " placeholderText="DD/MM/YYYY" maxLength="30"/>

                  </div>
              </div>
              <div className="col-md-3 my-3">
                  <h4 className="my-2">Assigned To :</h4>
                  <div className="search_icons_right modify_select">
                    <p>{this.state['crm_participant_id'].assigned_person}</p>
                  </div>
              </div>
          </div>

          <div className="row d-flex">
              <div className="col-md-7">
                  <h4 className="my-2">Relevant Task Notes:</h4>
              </div>

          </div>

          <div className="row d-flex mb-3">
              <div className="col-md-7  task_N_txt">
                  <textarea name="relevant_task_note" data-rule-required='true' data-msg-required="Add Task Note" value={this.state['relevant_task_note'] || ''}   onChange={this.handleChange} className="form-control textarea-max-size " wrap="soft"></textarea>
              </div>
          </div>



         <div className="custom-modal-footer bt-1 mt-5 px-0 pb-4">
              <div className="row d-flex justify-content-end">
                  <div className="col-md-2"><a className="btn-1" onClick={this.submit}>Create Task</a></div>
              </div>
          </div>

          </form>

      </div>

    )
  }

}


class Tasks extends Component {
    constructor(props, context) {
        super(props, context);
        this.child = React.createRef();
        this.handleSelect = this.handleSelect.bind(this);

        this.state = {
            key: 1, filterVal: '',
            showModal: false,
            taskList:[],
            loading: false,
            counter: 0,
            action:2,task_id:1

        };
        this.reactTable = React.createRef();
    }

    fetchData = (state, instance) => {
        // function for fetch data from database
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
          //console.log(res.rows);
            this.setState({
                taskLists: res.rows,
                pages: res.pages,
                loading: false
            });
        });
    }

    handleSelect=(key)=> {
        this.setState({ key });
    }

    showModal = () => {
        this.setState({ showModal: true })

    }

    closeModal = (param) => {
        this.setState({ showModal: false });
        if(param)
            reFreashReactTable(this, 'fetchData');
    }

    submitSearch = (e) => {
        e.preventDefault();

        var srch_ary = {search: this.state.search, filterVal: this.state.filterVal }
        this.setState({filtered: srch_ary});

    }

    searchData = (key, value) => {
        var srch_ary = {search: this.state.search, filterVal: this.state.filterVal };

        srch_ary[key] = value;
        this.setState(srch_ary);
        this.setState({filtered: srch_ary});
    }


    render() {
        const classname = ['none', 'priority_high_task', 'priority_medium_task', 'priority_low_task'];
        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];
        const columns = [
            { Header: "Task Name:", accessor: "taskname",  Cell: row => (<div className={classname[row.original.priority]}><span className="d-flex align-items-center tsk_sp"><i className="icon icon-circle1-ie cir_1"></i>  <span>{ row.value}</span></span></div>  ), headerStyle: { border: '0px solid #000', },  },
            { Header: "Participant Name:", accessor: "name", },
            { Header: "Date:", accessor: "duedate", },
            { Header: "Status:", accessor: "task_status", },
            {Cell: (props) => <span className="action_ix__ action_ix_1__">
                <i className="icon icon-view2-ie icon_h-1 mr-2"></i>
                <Link to={{}}><i className="icon icon-views"></i></Link>
                <i className="icon icon-accept-approve2-ie icon_h-1 mr-2"></i>
                <i className="icon icon-archive2-ie icon_h-1 mr-2"></i>
            
                </span>, Header: <div className="">Action</div>, 
                headerStyle: {border:"0px solid #fff" }, sortable: false}, 


        ];


        return (
            <div className="container-fluid">
            <CrmPage pageTypeParms={'schedule_user_task'}/>
                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1">
                        <div className="back_arrow py-4 bb-1">
                        <Link to={ROUTER_PATH + 'admin/crm/participantadmin'}><span className="icon icon-back1-ie"></span></Link>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1">
                        <div className="row d-flex py-4">
                            <div className="col-md-9">
                                <div className="h-h1">
                                    {this.props.showPageTitle}
                                </div>
                            </div>
                        </div>
                        <div className="row"><div className="col-md-12"><div className="bt-1"></div></div></div>
                    </div>
                </div>


                <div className="row mt-5">


                    <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                    <form onSubmit={this.submitSearch}>
                        <div className="row d-flex">
                            <div className="col-md-9">
                                <div className="big-search l-search">
                                  <input type="text" name="search" value={this.state.search || ''} onChange={(e) => this.setState({'search': e.target.value})}  />
                                  <button  type="submit"><span className="icon icon-search1-ie"></span></button>
                                </div>
                            </div>
                            <div className="col-md-3 d-inline-flex align-self-center">
                                <div className="s-def1 w-100">
                                    <Select
                                        name="view_by_status"
                                        options={options}
                                        required={true}
                                        simpleValue={true}
                                        searchable={false}
                                        clearable={false}
                                        placeholder="Filter by: Unread"
                                        onChange={(e) => this.searchData('filterVal', e)}
                                        value={this.state.filterVal}
                                    />
                                </div>
                            </div>
                        </div>
                        </form>
                        <div className="row d-flex justify-content-end mt-5 mb-3">
                            <div className="col-md-3"><a className="btn-1" onClick={this.showModal}>Create New Task</a></div>
                        </div>

                        <div className="row">
                            <div className="col-md-12 re-table mt-4">
                                <ReactTable
                                PaginationComponent={Pagination}
                                    ref={this.reactTable}
                                    columns={columns}
                                    data={this.state.taskLists}
                                    pages={this.state.pages}
                                    loading={this.state.loading}
                                    defaultPageSize={10}
                                    onFetchData={this.fetchData}
                                    filtered={this.state.filtered}
                                    showPagination={true}
                                    minRows={2}
                                    className="-striped -highlight"
                                    previousText={<span className="icon icon-arrow-left privious"></span>}
nextText={<span className="icon icon-arrow-right next"></span>}
                                  //  SubComponent={subComponentTaskMapper}

                                />

                            </div>
                        </div>



                      


                        <div className={this.state.showModal ? 'customModal show' : 'customModal'}>
                            <div className="custom-modal-dialog Information_modal task_modal">

                                <CreateTask closeModal={this.closeModal}/>



                            </div>
                        </div>




                    </div>

                </div>





            </div>
        );
    }
}


const mapStateToProps = state => {
      return {
        showPageTitle: state.DepartmentReducer.activePage.pageTitle,
        showTypePage: state.DepartmentReducer.activePage.pageType,

      }
  };

export default connect(mapStateToProps)(Tasks);
