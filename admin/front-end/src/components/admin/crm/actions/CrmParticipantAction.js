import {postData,postImageData} from '../../../../service/common.js';
export const CRM_PARTICIPANT_DATA = 'CRM_PARTICIPANT_DATA';
export const STATES = 'STATES';
export const SUBMIT = 'SUBMIT';

export function crmParticipant(data){
  return  dispatch=>{
        dispatch(fetchAllLatestUpdate(data));
  };
    function fetchAllLatestUpdate(participantData) { return { type: CRM_PARTICIPANT_DATA,participantData: participantData } }
  }
export function crmParticipantSubmit(data){
  const config = {
                      headers: {
                          'content-type': 'multipart/form-data'
                      }
  }
  return  dispatch=>{
    postImageData('crm/CrmParticipant/create_crm_participant',data,config)
    .then(json => {

      dispatch(submit(json.data));
      return json.data;
    })
  };
    function submit(participantData) { return { type: SUBMIT,response: participantData } }
  }


  export function states(){
    let request_data = {data:''};
    return  dispatch=>{
        postData('crm/CrmParticipant/get_state',request_data)
        .then(json => {

          dispatch(states(json.data));
          return json.data;
        })
        .catch(error => console.log(error));
    };
      function states(state) { return { type: STATES,states: state } }
  }
