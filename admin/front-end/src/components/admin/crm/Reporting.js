import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Doughnut } from 'react-chartjs-2';
import { ROUTER_PATH } from '../../../config.js';
import { checkItsNotLoggedIn, postData, handleChangeSelectDatepicker } from '../../../service/common.js';
import { CounterShowOnBox } from 'service/CounterShowOnBox.js';
import { connect } from 'react-redux';
import CrmPage from './CrmPage';


class ProspectiveParticipantStatus extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            crmparticipantCount: 0,
            view_type: 'month',
            grapheachdata: {},
        };

    }

    componentDidMount() {
        this.crmParticipantAjax();
    }

    crmParticipantStatusBy = (type) => {
        this.setState({ view_type: type }, function () {
            this.crmParticipantAjax();
        });
    }

    crmParticipantAjax = () => {
        postData('crm/Dashboard/crm_participant_status', this.state).then((result) => {
            if (result.status) {
                this.setState({ crmparticipantCount: result.participant_count });
                this.setState({ grapheachdata: result.grapheachdata });

            } else {
                this.setState({ error: result.error });
            }
        });
    }

    render() {

        const Graphdata = {
            labels: ['Successful', 'Processing', 'Rejected'],
            datasets: [{
                data: [
                    (this.state.grapheachdata.successful) ? this.state.grapheachdata.successful : 0,
                    (this.state.grapheachdata.processing) ? this.state.grapheachdata.processing : 0,
                    (this.state.grapheachdata.rejected) ? this.state.grapheachdata.rejected : 0],
                backgroundColor: ['#be77ff', '#7c00ef', '#5300a7'],

            }],

        };
        return (
          <li className="radi_2">
                    <h2 className="text-center cl_black">Participant Status:</h2>
                    <div className="row  pb-3 align-self-center w-100 mx-auto Graph_flex">
                        <div className="text-center col-md-5 col-xs-12">
                            <div className="myChart12 mx-auto" >
                                <Doughnut data={Graphdata} height={250} className="myDoughnut" legend={""} />
                            </div>
                        </div>

                        <div className="col-md-4 col-xs-12 text-center d-inline-flex align-self-center">
                            <div className="myLegend mx-auto">
                                <div className="chart_txt_1"><b>Successful:</b> {this.state.grapheachdata.successful}</div>
                                <div className="chart_txt_2"><b>Processing:</b> {this.state.grapheachdata.processing}</div>
                                <div className="chart_txt_3"><b>Rejected:</b> {this.state.grapheachdata.rejected}</div>
                            </div>
                        </div>
                        <div className="W_M_Y_box P_15_T col-md-3 col-xs-12 pb-3 d-inline-flex align-self-center">
                            <div className="vw_bx12 mx-auto">
                                <h5><b>View by:</b></h5>
                                <span onClick={() => this.crmParticipantStatusBy('week')} className={this.state.view_type == 'week' ? 'color' : ''}>Week</span><br />
                                <span onClick={() => this.crmParticipantStatusBy('month')} className={this.state.view_type == 'month' ? 'color' : ''}>Month </span><br />
                                <span onClick={() => this.crmParticipantStatusBy('year')} className={this.state.view_type == 'year' ? 'color' : ''}> Year</span><br />
                            </div>
                        </div>
                    </div>
                </li>
        

        );
    }
}

class ProspectiveParticipantCount extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            crmparticipantCount: 0,
            view_type: 'month',
            all_crmparticipant_count: '',
        };

    }

    componentDidMount() {
        this.crmParticipantAjax();
    }

    crmParticipantCountBy = (type) => {
        this.setState({ view_type: type }, function () {
            this.crmParticipantAjax();
        });
    }
    //
    crmParticipantAjax = () => {
        postData('crm/Dashboard/crm_participant_count', this.state).then((result) => {
            if (result.status) {
                this.setState({ crmparticipantCount: result.crm_participant_count });
                this.setState({ all_crmparticipant_count: result.all_crmparticipant_count });

            } else {
                this.setState({ error: result.error });
            }
        });
    }

    render() {

        return (


   
                <li className="radi_2">
                    <h2 className="text-center cl_black">Prospective Participants:</h2>

                    <div className="row pb-3 align-self-center w-100 mx-auto Graph_flex">
                        <div className="col-md-8 col-xs-12 d-inline-flex align-self-center justify-content-center mb-3">
                            <CounterShowOnBox counterTitle={this.state.crmparticipantCount} classNameAdd="" />
                        </div>



                        <div className="W_M_Y_box  col-md-4 col-xs-12 d-inline-flex align-self-center">
                            <div className="vw_bx12 mx-auto">
                                <h5><b>View by:</b></h5>


                                <span onClick={() => this.crmParticipantCountBy('week')} className={this.state.view_type == 'week' ? 'color' : ''}>Week</span><br />
                                <span onClick={() => this.crmParticipantCountBy('month')} className={this.state.view_type == 'month' ? 'color' : ''}>Month </span><br />
                                <span onClick={() => this.crmParticipantCountBy('year')} className={this.state.view_type == 'year' ? 'color' : ''}> Year</span>
                            </div>
                        </div>
                    </div>


                </li>
           


        );
    }
}

class Reporting extends Component {
    constructor(props, context) {
        super(props, context);
        checkItsNotLoggedIn(ROUTER_PATH);
        this.handleSelect = this.handleSelect.bind(this);

        this.state = {
            key: 1, filterVal: ''
        };
    }

    handleSelect(key) {
        this.setState({ key });
    }

    render() {
        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];
        const Applicantdata = {
            labels: ['Successful', 'Processing', 'Rejected'],
            datasets: [{
                data: ['250', '333', '250'],
                backgroundColor: ['#ed97fa', '#b968c7', '#702f75'],

            }],

        };
        const Participantsavailable = {
            labels: ['', '', ''],
            datasets: [{
                data: ['250', '333', '250'],
                backgroundColor: ['#ed97fa', '#b968c7', '#702f75'],

            }],

        };
        return (

            <div className="container-fluid">
                <CrmPage pageTypeParms={'report_onbording_analytics'} />
                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1">
                        <div className="back_arrow py-4 bb-1">
                            <Link to={ROUTER_PATH + 'admin/crm/participantadmin'}><span className="icon icon-back1-ie"></span></Link>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1">
                        <div className="row d-flex py-4">
                            <div className="col-md-9">
                                <div className="h-h1">
                                    {this.props.showPageTitle}
                                </div>
                            </div>
                        </div>
                        <div className="row"><div className="col-md-12"><div className="bt-1"></div></div></div>
                    </div>
                </div>

                <div className="row mt-5">

                    <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                        <div className="row d-flex">
                            <div className="col-md-12">
                                <div className="big-search l-search">
                                    <input />
                                    <button><span className="icon icon-search1-ie"></span></button>
                                </div>
                            </div>
                        </div>

                        <ul className="landing_graph landing_graph_item_2 mt-5">
                            <ProspectiveParticipantStatus />
                            <ProspectiveParticipantCount />
                        </ul>
                    </div>

                </div>


                {/* <div className="row">
                    <div className="col-lg-10 col-lg-offset-1">
                        <div className="row">
                            <div className="col-md-12"><h3>Acceptance of Onboarding Status:</h3></div>
                            <div className="col-md-12">
                                <img src="/assets/images/charts.jpg" alt="charts" className="w-100" />
                            </div>
                        </div>
                    </div>
                </div> */}


            </div>

        );
    }
}

const mapStateToProps = state => {
    return {
        showPageTitle: state.DepartmentReducer.activePage.pageTitle,
        showTypePage: state.DepartmentReducer.activePage.pageType,

    }
};

export default connect(mapStateToProps)(Reporting);
