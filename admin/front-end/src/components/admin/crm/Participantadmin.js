import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Doughnut, Bar } from 'react-chartjs-2';
import { Tabs, Tab, ProgressBar } from 'react-bootstrap';
import ReactTable from "react-table";
import 'react-table/react-table.css';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { checkItsNotLoggedIn, postData } from '../../../service/common.js';
import { ROUTER_PATH } from '../../../config.js';
import { allLatestUpdate } from './actions/DashboardAction.js';
import { connect } from 'react-redux';
import CrmPage from './CrmPage';
import { CounterShowOnBox } from 'service/CounterShowOnBox.js';


import ParticipantDetails from './ParticipantDetails.js';


class ProspectiveParticipantStatus extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      crmparticipantCount: 0,
      view_type: 'month',
      grapheachdata: '',
    };

  }

  componentDidMount() {
    this.crmParticipantAjax();
  }

  crmParticipantStatusBy = (type) => {
    this.setState({ view_type: type }, function () {
      this.crmParticipantAjax();
    });
  }

  crmParticipantAjax = () => {
    postData('crm/Dashboard/crm_participant_status', this.state).then((result) => {
      if (result.status) {
        this.setState({ crmparticipantCount: result.participant_count });
        this.setState({ grapheachdata: result.grapheachdata });

      } else {
        this.setState({ error: result.error });
      }
    });
  }


  render() {
    // console.log(this.state.grapheachdata.successful);
    const Graphdata = {
      labels: ['Successful', 'Processing', 'Rejected'],
      datasets: [{
        data: [this.state.grapheachdata.successful, this.state.grapheachdata.processing, this.state.grapheachdata.rejected],
        backgroundColor: ['#be77ff', '#7c00ef', '#5300a7'],

      }],

    };
    return (
      <li className="radi_2" style={{ marginRight: '0px' }}>

        <h2 className="text-center  cl_black">Participant Status:</h2>

        <div className="row  pb-3 align-self-center w-100 mx-auto Graph_flex">
          <div className="text-center col-md-5 col-xs-12">
            <div className="myChart12 mx-auto" >
              <Doughnut data={Graphdata} height={250} className="myDoughnut" legend={""} />
            </div>
          </div>

          <div className="col-md-4 col-xs-12 text-center d-inline-flex align-self-center">
            <div className="myLegend mx-auto">
              <div className="chart_txt_1"><b>Successful:</b> {this.state.grapheachdata.successful}</div>
              <div className="chart_txt_2"><b>Processing:</b> {this.state.grapheachdata.processing}</div>
              <div className="chart_txt_3"><b>Rejected:</b> {this.state.grapheachdata.rejected}</div>
            </div>
          </div>
          <div className="W_M_Y_box P_15_T col-md-3 col-xs-12 pb-3 d-inline-flex align-self-center">
                        <div className="vw_bx12 mx-auto">
            <h5><b>View by:</b></h5>
            <span onClick={() => this.crmParticipantStatusBy('week')} className={this.state.view_type == 'week' ? 'color' : ''}>Week</span><br/>
            <span onClick={() => this.crmParticipantStatusBy('month')} className={this.state.view_type == 'month' ? 'color' : ''}>Month </span><br/>
            <span onClick={() => this.crmParticipantStatusBy('year')} className={this.state.view_type == 'year' ? 'color' : ''}> Year</span><br/>
          </div>
          </div>
        </div>


      </li>
    );
  }
}

class ProspectiveParticipantVsMember extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
      view_type: 'month',

    };

  }

  componentDidMount() {
    this.crmParticipantAjax();
  }

  crmParticipantStatusBy = (type) => {
    this.setState({ view_type: type }, function () {
      this.crmParticipantAjax();
    });
  }

  crmParticipantAjax = () => {
    postData('crm/Dashboard/crm_participant_member', this.state).then((result) => {
      if (result.status) {
        this.setState({ count: result.count });

      } else {
        this.setState({ error: result.error });
      }
    });
  }


  render() {
    const BarGraphdata = {
      labels: ['Participants', 'Members', 'In Progress'],
      datasets: [{
        data: [this.state.count.participant, this.state.count.member, this.state.count.processing],
        backgroundColor: ['#be77ff', '#7c00ef', '#5300a0'],

      }],

    };
    const data = {
      labels: ['Participant Other', 'Participant New'],
      datasets: [{
        data: [(this.state.all_participant_count - this.state.participantCount), this.state.participantCount],
        backgroundColor: ['#7c00ef', '#5300a0'],
        hoverBackgroundColor: ['#7c00ef', '#5300a0'],
      }],
    };
    return (

      <li className="radi_2">

        <h2 className="text-center  cl_black">Participants vs Members available:</h2>
        <div className="col-md-5 pr-0">
          <Bar data={BarGraphdata} width={120} legend={""}
            options={{
              maintainAspectRatio: false
            }}
          />
        </div>
        <div className="col-md-7 text-left mt-3">
          <div className="chart_txt_1"><b>Participants:</b> {this.state.count.participant}</div>
          <div className="chart_txt_2"><b>Members:</b> {this.state.count.member}</div>
          <div className="chart_txt_3"><b>In Progress:</b> {this.state.count.processing}</div>
          <div className="chart_txt_4">
            <div className="mt-5"><b>View by:</b></div>
            <span onClick={() => this.crmParticipantStatusBy('week')} className={this.state.view_type == 'week' ? 'active' : ''}>Week</span>
            <span onClick={() => this.crmParticipantStatusBy('month')} className={this.state.view_type == 'month' ? 'active' : ''}>Month </span>
            <span onClick={() => this.crmParticipantStatusBy('year')} className={this.state.view_type == 'year' ? 'active' : ''}> Year</span>
          </div>
        </div>

      </li>


    );
  }
}

class ProspectiveParticipantCount extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      crmparticipantCount: 0,
      view_type: 'month',
      all_crmparticipant_count: '',
    };

  }

  componentDidMount() {
    this.crmParticipantAjax();
  }

  crmParticipantCountBy = (type) => {
    this.setState({ view_type: type }, function () {
      this.crmParticipantAjax();
    });
  }
  //
  crmParticipantAjax = () => {
    postData('crm/Dashboard/crm_participant_count', this.state).then((result) => {
      if (result.status) {
        this.setState({ crmparticipantCount: result.crm_participant_count });
        this.setState({ all_crmparticipant_count: result.all_crmparticipant_count });

      } else {
        this.setState({ error: result.error });
      }
    });
  }

  render() {

    return (
      <li className="radi_2">

        <h2 className="text-center cl_black">Prospective Participants:</h2>
        <div className="row  pb-3 align-self-center w-100 mx-auto Graph_flex">
          <div className="col-md-8 col-xs-12 d-inline-flex align-self-center justify-content-center mb-3">
            <CounterShowOnBox counterTitle={this.state.crmparticipantCount} classNameAdd="" />
          </div>



          <div className="W_M_Y_box  col-md-4 col-xs-12 d-inline-flex align-self-center">
            <div className="vw_bx12 mx-auto">
              <h5><b>View by:</b></h5>


              <span onClick={() => this.crmParticipantCountBy('week')} className={this.state.view_type == 'week' ? 'color' : ''}>Week</span><br />
              <span onClick={() => this.crmParticipantCountBy('month')} className={this.state.view_type == 'month' ? 'color' : ''}>Month </span><br />
              <span onClick={() => this.crmParticipantCountBy('year')} className={this.state.view_type == 'year' ? 'color' : ''}> Year</span>
            </div>
          </div>
        </div>

      </li>
    );
  }
}



class Participantadmin extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleSelect = this.handleSelect.bind(this);
    this.state = {
      filterVal: '', key: 1,
      showModal_PE: false,
    };

  }

  handleSelect(key) {
    this.setState({ key });
  }
  showModal_PE = (id) => {
    let state = {};
    state[id] = true;

    this.setState(state)
  }
  closeModal_PE = (id) => {
    let state = {};
    state[id] = false;

    this.setState(state)
  }

  updateSelect = (e) => {
    this.setState({ filterVal: e });
  }

  render() {
    var options = [
      { value: 'one', label: 'One' },
      { value: 'two', label: 'Two' }
    ];

    // const Applicantdata = {
    //   labels: ['Successful', 'Processing', 'Rejected'],
    //   datasets: [{
    //     data: ['20', '33', '20'],
    //     backgroundColor: ['#ed97fa', '#b968c7', '#702f75'],
    //   }],
    //
    // };
    const Participantsavailable = {
      labels: ['', '', ''],
      datasets: [{
        data: ['250', '333', '250'],
        backgroundColor: ['#ed97fa', '#b968c7', '#702f75'],

      }],

    };
    return (
      <div>
      <CrmPage pageTypeParms={'crm_dashboard'} />
        <div className="container-fluid">
          <div className="row">
            <div className="col-lg-10 col-lg-offset-1">
              <div className="back_arrow py-4 bb-1">
                <a href={ROUTER_PATH + 'admin/dashboard'}><span className="icon icon-back1-ie"></span></a>
              </div>
            </div>
          </div>

          <div className="row _Common_He_a">
            <div className="col-lg-8 col-lg-offset-1 col-xs-9"><h1 className="my-0 color"> CRM Dashboard (Admin)</h1></div>
            <div className="col-lg-2 col-xs-3">
              <a className="Plus_button" href={ROUTER_PATH + 'admin/crm/createParticipant'}><i className="icon icon-add-icons create_add_but"></i><span>Create New Participant</span></a>
            </div>
          </div>
          <div className="row "><div className="col-lg-10 col-lg-offset-1"><div className="bt-1"></div></div></div>

          <div className="row">
            <div className="col-lg-10 col-lg-offset-1">
              <ul className="landing_graph landing_graph_item_2 mt-5">
                <ProspectiveParticipantCount />
                <ProspectiveParticipantStatus />
                {/* <ProspectiveParticipantVsMember/> */}
              </ul>
              <div className="bt-1 mt-5"></div>
            </div>
          </div>


          <div className="row">
            {/* <Filterdiv /> */}
            {/* <Tablereact addclassName="re-table re-table2 re-table-progrees mt-4" /> */}
          </div>


          {/* 2. Start Tab Concept Start */}

          <div className="row">
            <div className="col-lg-10 col-lg-offset-1">
              <div className="row py-5">
                {// <div className="col-lg-4"><a onClick={() => this.showModal_PE('showModal_PE')} className="btn_g__">Reporting</a></div>
                }
                <div className="col-md-4 col-sm-12"><div className="btn_g__"><Link to={ROUTER_PATH + 'admin/crm/reporting'}>Reporting</Link></div></div>
                <div className="col-md-4 col-sm-12"><div><Link className="btn-1" to={ROUTER_PATH + 'admin/crm/prospectiveparticipants'}>Prospective Participant</Link></div></div>
                <div className="col-md-4 col-sm-12"><Link to="#" className="btn_g__"><Link to={ROUTER_PATH + 'admin/crm/schedules'}>Schedules</Link></Link></div>
              </div>
            </div>
          </div>
          <div className="row "><div className="col-lg-10 col-lg-offset-1"><div className="bt-1"></div></div></div>

          <div className="row">
            <ul className="nav nav-tabs Category_tap Nav_ui__ col-lg-10 col-lg-offset-1 col-sm-12 P_20_TB">
              <li className="col-lg-4 col-sm-4 "><Link to={ROUTER_PATH + 'admin/crm/participantuser'} >Personal View</Link>
              </li>
              <li className="col-lg-4 col-sm-4 active"><a href="#1a" data-toggle="tab">CRM Department View</a>
              </li>
            </ul>
          </div>

          <div className="tab-content clearfix">
            <div className="tab-pane active" id="1a">
              <div className="row">
                <div className="col-lg-4 col-lg-offset-1 col-md-5">
                  <LatestActions />
                </div>
                <div className="col-lg-4 col-md-5">
                  <DueTasks />
                </div>
                <div className="col-lg-2 col-md-2">
                  <LatestUpdates props={this.props} />
                </div>
              </div>
            </div>
            <div className="tab-pane" id="2a">
              <div className="row">

              </div>
            </div>
          </div>

          {/* Start Tab Concept End */}


        </div>

      </div >


    );
  }
}


class Filterdiv extends React.Component {
  constructor(props) {
    super(props);
    this.state = { filterVal: '' };
  }
  render() {
    var options = [
      { value: 'one', label: 'One' },
      { value: 'two', label: 'Two' }
    ];
    return (

      <div className="row pt-5 mb-5">

        <div className="col-lg-6 col-lg-offset-1 col-md-6">
          <div className="big-search l-search">
            <input />
            <button><span className="icon icon-search1-ie"></span></button>
          </div>
        </div>
        <div className="col-lg-2 col-md-3">
          <div className="s-def1">
            <Select
              name="view_by_status"
              options={options}
              required={true}
              simpleValue={true}
              searchable={false}
              clearable={false}
              placeholder="Filter by: Unread"
              onChange={(e) => this.setState({ filterVal: e })}
              value={this.state.filterVal}
            />
          </div>
        </div>
        <div className="col-lg-2 col-md-3">
          <div className="s-def1">
            <Select
              name="view_by_status"
              options={options}
              required={true}
              simpleValue={true}
              searchable={false}
              clearable={false}
              placeholder="Filter by: Unread"
              onChange={(e) => this.setState({ filterVal: e })}
              value={this.state.filterVal}
            />
          </div>
        </div>
      </div>
    );
  }
};

// class Tablereact extends React.Component {
//   render() {
//     const columns = [
//       { Header: 'NDIS', accessor: 'ndis' }
//       , { Header: 'Name', accessor: 'name' }
//       , { Header: 'Status', accessor: 'status' }
//       , { Header: 'Date', accessor: 'date' },
//       { Header: 'Action', accessor: 'action' }]

//     const tabledata = [{
//       ndis: '000 000 000', name: 'Roy Agasthyan', status: 'Pending Contact', date: '26/10/2019', action: "Phone Screening"}, {
//       ndis: '000 000 000', name: 'Sam Thomason', status: 'Pending Contact', date: '22/10/2019', action: "Phone Screening"}, {
//       ndis: '000 000 000', name: 'Michael Jackson', status: 'Pending Contact', date: '36/10/2019', action: "Phone Screening"}, {
//       ndis: '000 000 000', name: 'Samuel Roy', status: 'Pending Contact', date: '56/10/2019', action: "Phone Screening"}, {
//       ndis: '000 000 000', name: 'Rima Soy', status: 'Pending Contact', date: '28/10/2019', action: "Phone Screening"}, {
//       ndis: '000 000 000', name: 'Suzi Eliamma', status: 'Pending Contact', date: '28/10/2019', action: "Phone Screening"}, {
//       ndis: '000 000 000', name: 'Roy Agasthyan', status: 'Pending Contact', date: '26/10/2019', action: "Phone Screening"}, {
//       ndis: '000 000 000', name: 'Sam Thomason', status: 'Unassigned', date: '22/10/2019', action: "Phone Screening"}, {
//       ndis: '000 000 000', name: 'Michael Jackson', status: 'Unassigned', date: '36/10/2019', action: "Phone Screening"}, {
//       ndis: '000 000 000', name: 'Samuel Roy', status: 'Unassigned', date: '56/10/2019', action: "Phone Screening"}, {
//       ndis: '000 000 000', name: 'Rima Soy', status: 'Unassigned', date: '28/10/2019', action: "Phone Screening"}, {
//       ndis: '000 000 000', name: 'Suzi Eliamma', status: 'Unassigned', date: '28/10/2019', action: "Phone Screening"
//     }]
//     const now = 60;


//     return (
//       <div className={this.props.addclass}>

//         <ReactTable

//           columns={[
//             { Header: "NDIS No:", accessor: "ndis", },
//             { Header: "Participant Name:", accessor: "name", },
//             { Header: "Status:", accessor: "status", },
//             { Header: "Intake Submisson Date:", accessor: "date", },
//             {
//               Header: "Next Actions:", accessor: "action",
//               headerStyle: { border: "0px solid #fff" },
//             },
//             {
//               expander: true,
//               Header: () => <strong></strong>,
//               width: 55,
//               headerStyle: { border: "0px solid #fff" },
//               Expander: ({ isExpanded, ...rest }) =>
//                 <div className="rec-table-icon">
//                   {isExpanded
//                     ? <i className="icon icon-arrow-up"></i>
//                     : <i className="icon icon-arrow-down"></i>}
//                 </div>,
//               style: {
//                 cursor: "pointer",
//                 fontSize: 25,
//                 padding: "0",
//                 textAlign: "center",
//                 userSelect: "none"
//               },

//             }

//           ]}
//           data={tabledata}
//           defaultPageSize={10}
//           className="-striped -highlight"
//           previousText={<span className="icon icon-arrow-1-left privious"></span>}
//           nextText={<span className="icon icon-arrow-1-right next"></span>}
//           SubComponent={() =>

//             <div className="col-md-12 re-table-in">
//               <div className="d-flex  bt-1 bb-1">
//                 <div className="text-left py-4 pr-4">
//                   <div className="txt_t1"><b>Anthony Johnston</b></div>
//                   <div className="txt_t2"><b>NDIS No.:</b> <u>000 000 000</u></div>
//                   <div className="txt_t2"><b>Phone:</b> <u>04214 79713</u></div>
//                   <div className="txt_t2"><b>Email:</b> <u>ajohnston@gmail.com</u></div>
//                 </div>
//                 <div className="text-left py-4 px-4 br-1">
//                   <div className="txt_t2"><b>Address:</b> <u>972 John Street, South Melbourne, VIC, 3000</u></div>
//                 </div>
//                 <div className="text-left py-4 px-4 br-1">
//                   <div className="txt_t2"><b> Status:</b> Pending Contact</div>
//                   <div className="txt_t2"><b>ONCALL Assignee:</b><u>Jane Goldebrg</u></div>
//                   <div className="txt_t2"><b> Department:</b> XXXX</div>
//                 </div>
//                 <div className="text-left py-4 pl-4">
//                   <div className="txt_t2"><b>Reference:</b> Tom Smith</div>
//                   <div className="txt_t2"><b>Email:</b> tomsmith@gmail.com</div>
//                   <div className="txt_t2"><b>Phone:</b> 04879 99431</div>
//                   <div className="txt_t2"><b>Organisation:</b> XXXX</div>
//                   <div className="txt_t2"><b>Relationship to Participant:</b> Legal Guardian</div>
//                 </div>
//                 <div className="d-flex align-items-end table_view_icon">
//                   <div>
//                     <Link to='./ParticipantDetails'>
//                       <span className="icon icon-view1-ie"></span>
//                       More Info
//                                 </Link></div>
//                 </div>
//               </div>
//               <div className="progress-img"></div>
//               <div className="progress-b1">
//                 <ProgressBar className="progress-b2" now={now} label={'Intake Progress: ' + `${now}%` + 'Complete'} />
//               </div>
//             </div>


//           }
//         />

//       </div>
//     );
//   }
// }


class LatestActions extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      activeCol: '',
      data: []
    }
  }
  crmParticipantLatestAction = () => {
    postData('crm/Dashboard/crm_latest_action_admin', this.state).then((result) => {
      // console.log(result);
      if (result.status) {
        this.setState({ data: result.data.data });

      } else {
        this.setState({ error: result.error });
      }
    });
  }
  componentWillMount() {
    this.crmParticipantLatestAction();
  }
  render() {
    const tabledata = [
      { ndis: '000 000 000', name: 'Roy Agasthyan', overdueaction: 'Plan Delegation', Status: 'Pending Phone Contact', date: '26/10/2019', action: "Phone Screening" },
      { ndis: '000 000 000', name: 'Sam Thomason', overdueaction: 'Plan Delegation', Status: 'Pending Phone Contact', date: '22/10/2019', action: "Phone Screening" },
      {
        ndis: '000 000 000', name: 'Michael Jackson', overdueaction: 'Plan Delegation', Status: 'Pending Phone Contact', date: '36/10/2019', action: "Phone Screening"
      }]

    return (
      <div className="PD_Al_div">
        <div className="PD_Al_h_txt pt-3">Latest Action</div>
        <div className="re-table re_tab_D">
          <ReactTable
            data={this.state.data}
            columns={[
              {
                Header: "User", accessor: "user",
                headerClassName: 'hdrCls', className: (this.state.activeCol === 'name') ? 'borderCellCls' : 'T_align_m1'
              },
              {
                Header: "Task ", accessor: "taskname",
                headerClassName: 'hdrCls', className: (this.state.activeCol === 'name') ? 'borderCellCls' : 'T_align_m1'
              },
              {
                Header: "Action", accessor: "action",
                headerClassName: 'hdrCls', className: (this.state.activeCol === 'status') ? 'borderCellCls' : 'T_align_m1'
              },
              {
                Header: "Action Date", accessor: "duedate",
                headerClassName: 'hdrCls', className: (this.state.activeCol === 'date') ? 'borderCellCls' : 'T_align_m1',
                maxWidth: 120,
              },
              {
                Header: "", headerStyle: { border: '0px solid #000' },
                maxWidth: 100,
                Cell: row => (<span className="PD_Al_icon"><a><i className="icon icon-view2-ie LA_i1"></i></a> <a><i className="icon icon-notes2-ie LA_i2"></i></a></span>)
              },
            ]}
            defaultPageSize={10}
            pageSize={this.state.data.length}
            showPagination={false}
            sortable={false}
            className="-striped -highlight"
          />
        </div>
        <Link className="btn-1" to={ROUTER_PATH + 'admin/crm/tasks'}>View All</Link>
      </div>
    )
  }
}

class DueTasks extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      activeCol: '',
      data: [],
    }
  }
  crmParticipantDueTask = () => {
    postData('crm/Dashboard/crm_task_list', this.state).then((result) => {
      // console.log(result);
      if (result.status) {
        this.setState({ data: result.data.data });

      } else {
        this.setState({ error: result.error });
      }
    });
  }
  componentWillMount() {
    this.crmParticipantDueTask();
  }
  render() {
    const tabledata = [
      { ndis: '000 000 000', name: 'Roy Agasthyan', overdueaction: 'Plan Delegation', status: 'Pending Phone Contact', date: '26/10/2019', action: "Phone Screening" },
      { ndis: '000 000 000', name: 'Sam Thomason', overdueaction: 'Plan Delegation', status: 'Pending Phone Contact', date: '22/10/2019', action: "Phone Screening" },
      {
        ndis: '000 000 000', name: 'Michael Jackson', overdueaction: 'Plan Delegation', status: 'Pending Phone Contact', date: '36/10/2019', action: "Phone Screening"
      }]

    return (
      <div className="PD_Al_div">
        <div className="PD_Al_h_txt pt-3">Due Tasks:</div>
        <div className="re-table re_tab_D">
          <ReactTable
            data={this.state.data}
            columns={[
              {
                Header: "Task", accessor: "taskname",
                headerClassName: 'hdrCls', className: (this.state.activeCol === 'name') ? 'borderCellCls' : 'T_align_m1'
              },
              {
                Header: "Participant", accessor: "fullname",
                headerClassName: 'hdrCls', className: (this.state.activeCol === 'status') ? 'borderCellCls' : 'T_align_m1'
              },
              {
                Header: "Due Date", accessor: "duedate",
                headerClassName: 'hdrCls', className: (this.state.activeCol === 'date') ? 'borderCellCls' : 'T_align_m1',
                maxWidth: 120,
              },
              {
                Header: "", headerStyle: { border: '0px solid #000' },
                maxWidth: 100,
                Cell: row => (<span className="PD_Al_icon">  <Link to={ROUTER_PATH + 'admin/crm/tasks'}><a><i className="icon icon-view2-ie LA_i1"></i></a></Link>
                <Link to={ROUTER_PATH + 'admin/crm/tasks'}><a><i className="icon icon-notes2-ie LA_i2"></i></a></Link></span>)
              },
            ]}
            defaultPageSize={10}
            pageSize={this.state.data.length}
            showPagination={false}
            sortable={false}
            className="-striped -highlight"
          />
        </div>
        <Link className="btn-1" to={ROUTER_PATH + 'admin/crm/tasks'}>View All</Link>
      </div>
    )
  }
}


// For Latest Updates
var rawData = [];
const requestData = (pageSize, page, sorted, filtered) => {
  return new Promise((resolve, reject) => {

    // request json
    var Request = JSON.stringify({ pageSize: pageSize, page: page, sorted: sorted, filtered: filtered });
    postData('crm/Dashboard/latest_updates', Request).then((result) => {
      let filteredData = result.data;
      const res = {
        rows: filteredData,
        pages: (result.count)
      };
      resolve(res);
    });

  });
};

class LatestUpdates extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      activeCol: '',
      crmlogsList: [],
      filterVal: '',
      allLatestUpdates: []
    }
  }

  // fetchData = (state, instance) => {
  //     // function for fetch data from database
  //     this.setState({ loading: true });
  //     requestData(
  //         state.pageSize,
  //         state.page,
  //         state.sorted,
  //         state.filtered
  //     ).then(res => {
  //         this.setState({
  //             crmlogsList: res.rows,
  //             pages: res.pages,
  //             loading: false
  //         });
  //     });
  // }

  componentWillMount() {
    this.props.props.allLatestUpdate().then((result) => {
      this.setState({ allLatestUpdates: result })
      let filteredData = result;
      const res = {
        rows: filteredData,
        // pages: (result.count)
      };
    });
  }
  render() {
    const { data, pages, loading } = this.state;
    const columns = [{
      Header: '', accessor: 'name', filterable: false, Cell: row => (
        <div className="col-md-12" key={row.original.id}>
          <div className="Not_msg_div">
            <div className="Not_m_d3" style={{ whiteSpace: 'pre-line', wordBreak: 'break-all' }}>{row.original.title}</div>
            <p>Completed by:{row.original.FullName}</p>
            <span><p>{row.original.created}</p>
            <Link to={ROUTER_PATH + 'admin/crm/participantdetails/'+row.original.userId}>
              <i className="icon icon-view2-ie LA_i1"></i>
            </Link>
            </span>
          </div>
        </div>

      )
    }]
    return (
      <div className="PD_Al_div">
        <div className="PD_Al_h_txt pt-3">Latest Updates</div>
        {
          <div className="re-table re_tab_D second_tab_D">
            <ReactTable
              columns={columns}
              manual
              data={this.state.allLatestUpdates}
              // pages={this.state.pages}
              loading={this.state.loading}
              // onFetchData={this.fetchData}
              defaultPageSize={10}
              noDataText="No Updates"
              className="-striped -highlight "
              minRows={2}
              showPagination={false}

            />
            <br />
          </div>
        }
        <Link className="btn-1" to={ROUTER_PATH + 'admin/crm/prospectiveparticipants'}>View All</Link>
      </div>
    )
  }
}


const mapStateToProps = state => {
  return {
    showPageTitle: state.DepartmentReducer.activePage.pageTitle,
    showTypePage: state.DepartmentReducer.activePage.pageType,

  }
};
const mapDispatchtoProps = (dispach) => {
  return {
    allLatestUpdate: () => dispach(allLatestUpdate()),
  }
}
export default connect(mapStateToProps, mapDispatchtoProps)(Participantadmin);
