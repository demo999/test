import React from 'react';
import jQuery from "jquery";
import { ROUTER_PATH, BASE_URL } from '../../../config.js';
import { checkItsNotLoggedIn, postData, archiveALL, postImageData, handleChangeSelectDatepicker,handleChangeChkboxInput } from '../../../service/common.js';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import {memberQualViewBy,qualsTitleDrpDwn} from '../../../dropdown/memberdropdown.js';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Modal from 'react-bootstrap/lib/Modal';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { customProfile } from '../../../service/CustomContentLoader.js';
import BlockUi from 'react-block-ui';
import MemberProfile from './MemberProfile';
import { connect } from 'react-redux'
import { ToastUndo } from 'service/ToastUndo.js'


class MembersQuals extends React.Component {
	constructor(props) {
	super(props);
        this.state = {
            loading:true,
            qual:[],
            view_by_state:'Current',
            start_date: '',
            end_date: '',
        };
        
        checkItsNotLoggedIn(ROUTER_PATH);
        this.urlParam = this.props.props.match.params.id;
        this.qualViewBy = memberQualViewBy();
    }

    selectChange(selectedOption, fieldname) { 
        var member_id = this.props.props.match.params.id;
        var state = {}
        state[fieldname] = selectedOption;
        this.setState(state, () => {
              this.getMemberQualification();  
        });
    }  

    componentDidMount() {
        var member_id = this.props.props.match.params.id;
        var view_by = this.state.view_by_state;    
        this.getMemberQualification();       
    }

    componentWillReceiveProps(nextProps)
    {
        this.setState({dwes_confirm:(nextProps.dwes_confirm && nextProps.dwes_confirm == 1)?true:false});
    }

   archived(index, id){
        var member_id = this.props.props.match.params.id;
        archiveALL({id: id,member_id:member_id},'', 'member/MemberDashboard/archive_member_qualification').then((result) => {
            if(result.status){
                var state = {}
                state['qual'] = this.state.qual.filter((s, sidx) => index !== sidx);
                this.setState(state);
            }
        })
    }

    getMemberQualification =()=>{
        var member_id = this.props.props.match.params.id;
        var view_by = this.state.view_by_state; 
        
        var requestData = {member_id: member_id,view_by:view_by, start_date: this.state.start_date, end_date: this.state.end_date};
        postData('member/MemberDashboard/member_qualification', requestData).then((result) => {
            if (result.status) {
                this.setState({qual:result.data},()=>{ });  

            } else {
                this.setState({qual:result.data});
            }
             this.setState({loading: false});
        });
    }

    closeDocsPopUp=()=>
    {
        this.setState({openDocsModal:false});
    }

    clickToDownloadFile =(key) =>{
        var activeDownload = this.state.qual;
        if(activeDownload[key]['is_active'] == true)
        activeDownload[key]['is_active'] = false;
        else 
        activeDownload[key]['is_active'] = true;
        this.setState({qual:activeDownload}); 
    }

   
    downloadSelectedFile =()=>
    {
       
        var member_id = this.props.props.match.params.id;
        var requestData = {member_id: member_id,downloadData:this.state.qual};
         
        postData('member/MemberDashboard/download_selected_file', requestData).then((result) => {
        if (result.status) {
            window.location.href=BASE_URL+"archieve/"+result.zip_name;                          
               
            var removeDownload = this.state.qual;
            removeDownload.map((value, idx) => {
                removeDownload[idx]['is_active'] = false;
            })
            this.setState({qual:removeDownload});
        } else {
            toast.dismiss();
            toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                position: toast.POSITION.TOP_CENTER,
                hideProgressBar: true
            });
        }
        });           
    }

    dwesChecked=(e)=>
    {
        var member_id = this.props.props.match.params.id;
        this.setState({dwes_confirm: e.target.checked});
        var requestData = {status: (e.target.checked) ? 1 : 0, member_id: member_id};
        postData('member/MemberDashboard/dwes_checked', requestData).then((result) => {
        });
    }

    render() {	
       return (
          <div>

            <MemberProfile MemberData={this.state} ProfileId={this.props.props.match.params.id} pageTypeParms={this.props.props.match.params.page} />
           <div className="row">
                    <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                        <div className="tab-content">
                            <div role="tabpanel" className={this.props.showTypePage=='current_docs' ? "tab-pane active" :"tab-pane"} id="Barry_details">
                                <div className="row mt-3">
                                    <div className="col-lg-9 col-md-9">
                                        <div className="row">
                                    
                                            <div className="col-md-12">
                                            <div className="bor_T"></div>
                                            <h3 className="P_7_TB"><b>
                                            {/* {this.props.firstName} */}
                                             Qualifications</b></h3></div>
                                            <div className="col-md-12"><div className="bor_T"></div></div>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 col-md-3">
                                        <div className="box">
                                            <Select name="view_by_state" required={true} simpleValue={true} searchable={false} clearable={false} value={this.state.view_by_state} onChange={(e) => this.selectChange(e, 'view_by_state')}
                                                options={this.qualViewBy} placeholder="View By" />
                                        </div>
                                    </div>
                                </div>
                                <div className="row flex set_alignment ">
                                    <div className="col-lg-9 col-md-9">
                                        <ReactPlaceholder showLoadingAnimation type='textRow' customPlaceholder={customProfile} ready={!this.state.loading}>

                                        <div className="quali_table___">
                                        {(this.state.qual.length > 0) ?
                                                        this.state.qual.map((value, idx) => (
                                            <div key={idx} className={((idx % 2) == 0 ? 'quali_tr___' : 'quali_tr___') + ' ' + value.class_name}>
                                                <div className="quali_td1___ bl-0">Expiry: <b>{(value.expiry) && value.expiry != '0000-00-00' ? value.expiry : 'N/A'}</b></div>
                                                <div className="quali_td2___"><div className="ellipsis_line__"><b>{(value.type > 0) ? qualsTitleDrpDwn(value.type) : ''}</b></div></div>
                                                <div className="quali_td3___"><div className="ellipsis_line__"><b>{value.title}</b></div></div>
                                                {(this.state.view_by_state == 'Current')?<div className="quali_td4___"><span onClick={()=> this.archived(idx,value.id)}><i className="icon icon-email-pending"></i></span></div>:''}
                                            </div>
                                               )) : <div>
                                                   <div className="no_record py-2 mt-3">No record found.</div>
                                               </div> }
                                            </div>
                                    </ReactPlaceholder>
                                </div>
                                
                                <div className="col-lg-3 col-md-3 align-self-end">
                                    <div className="equl_menu_hight">
                                        <span className="pull-right">
                                            <p className="select_all">
                                                <input className="input_c" id="dwes" type="checkbox" name="dwes_confirm" checked={this.state.dwes_confirm || ''} value={this.state.dwes_confirm} onChange={this.dwesChecked} />
                                                <label htmlFor="dwes"></label> <span>DWES Confirmed</span>
                                            </p>
                                        </span> 
                                    </div>
                                </div>                           
                                </div>

                            </div>
                            <div role="tabpanel" className={this.props.showTypePage=='qual_docs' ? "tab-pane active" :"tab-pane"} id="Barry_availabitity">
                                <div className="row mt-3">
                                    <div className="col-lg-9 col-md-9">
                                        <div className="row">
                                       
                                            <div className="col-lg-12">
                                            <div className="bor_T"></div>
                                            <h3 className="P_7_TB"><b>
                                            {/* {this.props.firstName}  */}
                                            Qualifications Documents </b></h3></div>
                                            <div className="col-lg-12"><div className="bor_T"></div></div>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 col-md-3">
                                        <div className="box">
                                           <Select name="view_by_state" required={true} simpleValue={true} searchable={false} clearable={false} value={this.state.view_by_state} onChange={(e)=> this.selectChange(e,'view_by_state')} 
                                        options={this.qualViewBy} placeholder="View By" /> 
                                        </div>
                                        <div className="row">
                                            <div className="col-lg-6 col-sm-6">
                                                <label>From</label>
                                                <DatePicker isClearable={true} name="start_date" onChange={(e) => this.selectChange(e, 'start_date')} selected={this.state['start_date'] ? moment(this.state['start_date'], 'DD-MM-YYYY') : null} className="text-center px-0" placeholderText="00-00-0000" />
                                            </div>

                                            <div className="col-lg-6 col-sm-6">
                                                <label>To</label>
                                                <DatePicker isClearable={true} name="end_date" onChange={(e) => this.selectChange(e, 'end_date')} selected={this.state['end_date'] ? moment(this.state['end_date'], 'DD-MM-YYYY') : null} className="text-center px-0" placeholderText="00-00-0000" minDate={moment(this.state.start_date)} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row flex set_alignment">
                                    <div className="col-lg-9 col-md-9">
                                        <ul className="file_down quali_width P_15_TB">
                                            { (this.state.qual.length > 0)?                               
                                                this.state.qual.map((value, idx) => (
                                                    <li key={idx} onClick={() => this.clickToDownloadFile(idx)} className={(value.is_active) ? 'active' : ''}>
                                                         <div className="path_file mt-0 mb-4"><b>{value.title}</b></div>
                                                        <span className="icon icon-file-icons d-block"></span>
                                                        <div className="path_file">{value.filename}</div>
                                                    </li>
                                                )) : <div className="no_record py-2 mt-3 w-100">No record found.</div>  
                                            }                                                                                           
                                        </ul>
                                    </div>
                                 
                                    <div className="col-lg-3 col-md-3 align-self-end">
                                        <div className="equl_menu_hight">
                                            <span>
                                                <p><button className="but"  onClick={()=> this.downloadSelectedFile()}>Download Selected </button></p>
                                                <button className="but"onClick={()=> this.setState({openDocsModal:true})}>Upload New Qual(s)</button>
                                               
                                            </span>
                                        </div>
                                    </div>
                                    <MembersUploadQualDocsModal isDocsModalShow={this.state.openDocsModal} closeUploadPopup={this.closeDocsPopUp} firstName={this.props.firstName} full_name={this.props.full_name} memberId={this.props.props.match.params.id} getMemberQualification={this.getMemberQualification}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
          </div>
          );
      }
    }
    const mapStateToProps = state => ({
        showTypePage: state.MemberReducer.activePage.pageType
    })

    const mapDispatchtoProps = (dispach) => {
        return {
            
        }
    }
  export default connect(mapStateToProps, mapDispatchtoProps)(MembersQuals);

 //
  class MembersUploadQualDocsModal extends React.Component {
    constructor(props) {
        super(props);
        this.uploadDocsInitialState = {
            selectedFile: null,
            expiry_date: null,
            submit_form: true,
            loading: false,
            docsTitle:''
        }
        this.state =  this.uploadDocsInitialState;

    } 
    fileChangedHandler = (event) => {
         this.setState({selectedFile: event.target.files[0], filename: event.target.files[0].name})
    }

    uploadHandler = (e) => {
        var isSubmit = 1;
        var state = {};
        /*if (!this.state.docsCategory) {
            isSubmit = 0;
            state['docsCategory' + '_error'] = true;
        }*/
        this.setState(state);
        e.preventDefault();
        jQuery("#special_agreement_form").validate({ /* */ });
        if (!this.state.loading && jQuery("#special_agreement_form").valid() && isSubmit) {
            this.setState({ submit_form: false });
            const formData = new FormData()
            formData.append('myFile', this.state.selectedFile, this.state.selectedFile.name)
            formData.append('memberId', this.props.memberId)
            formData.append('expiry', this.state.expiry_date)
            formData.append('docsCategory', this.state.docsCategory)
            formData.append('docsTitle', this.state.docsTitle)
            formData.append('memberName', this.props.full_name)
            postImageData('member/MemberDashboard/upload_member_qualification', formData).then((result) => {
            if (result.status) {
                this.props.getMemberQualification();
                this.props.closeUploadPopup();
                this.setState( this.uploadDocsInitialState);
                toast.success(<ToastUndo message={'Uploaded successfully.'} showType={'s'} />, {
                // toast.success("uploaded successfully.", {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
                this.closeClear();
            } else {
                    // toast.error(result.error, {
                        toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }
                this.setState({ loading: false });
                this.setState({ submit_form: true });
            });
        }
    }

    selectChange(selectedOption, fieldname) {
            var state = {};
            state[fieldname] = selectedOption;        
            this.setState(state);        
    } 

    closeClear = () => {
        this.setState({ docsCategory: '', expiry_date: '', filename: '', docsCategory_error: '',docsTitle: '' });
        this.props.closeUploadPopup();
    }

    callHtmlForValidation = (currentState) => {
        return (this.state[currentState + '_error']) ? <div className={'tooltip fade top in' + ((this.state[currentState + '_error']) ? ' select-validation-error' : '')} role="tooltip">
            <div className="tooltip-arrow"></div><div className="tooltip-inner">This field is required.</div></div> : '';
    }

    render() {
        return (
            <div>
                <BlockUi tag="div" blocking={this.state.loading}>
                <Modal className="modal fade-scale Modal_A  Modal_B" show={this.props.isDocsModalShow} onHide={this.handleHide} container={this} aria-labelledby="myModalLabel" id="modal_1" tabIndex="-1" role="dialog" >
                    <form id="special_agreement_form" method="post" autoComplete="off">
                        <Modal.Body>
                            <div className="dis_cell">
                                <div className="text text-left">Upload New Qual(s):
                            <div data-dismiss="modal" aria-label="Close" className="close_i pull-right mt-1" onClick={() => this.closeClear()}><i className="icon icon-cross-icons"></i></div>
                                </div>

                                <div className="row P_15_T">
                                
                                    <div className="col-md-12 mb-5 mt-5">
                                        <label>Title</label>
                                        <span className="required">
                                            <input type="text" value={this.state.docsTitle || ''} onChange={(e) => handleChangeChkboxInput(this, e)}  data-rule-required="true" className="text-left" name="docsTitle" placeholder="Title" autoComplete="new-password" />
                                        </span>
                                    </div>
                                    
                                    <div className="col-md-6">
                                        <label>Qualification Category</label>
                                        <span className="required">
                                            <Select className='custom_select' name="docsCategory"  simpleValue={true} searchable={false} clearable={false} value={this.state.docsCategory} onChange={(e) => handleChangeSelectDatepicker(this, e, 'docsCategory')} options={qualsTitleDrpDwn(0)}  />
                                            {this.callHtmlForValidation('docsCategory')}
                                        </span>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="row">
                                            <div className="col-md-12"><label>Qualification Expiry Date</label></div>
                                            <div className="col-md-12">
                                                <span className="">
                                                    <DatePicker className="text-center" selected={this.state.expiry_date} name="expiry_date" onChange={(e) => this.selectChange(e, 'expiry_date')} minDate={moment()} dateFormat="DD-MM-YYYY" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="row P_15_T my-5">
                                    <div className="col-md-12 text-center">
                                        <label>Please Select a File to Upload</label>
                                    </div>
                                    <div className="col-md-6 col-md-offset-3">
                                        <span className="required upload_btn">
                                            <label className="btn btn-default btn-sm center-block btn-file">
                                                <i className="but" aria-hidden="true">Browse Document(s)</i>
                                                <input className="p-hidden" type="file" name="special_agreement_file" onChange={this.fileChangedHandler} data-rule-required="true" date-rule-extension="jpg|jpeg|png|xlx|xls|doc|docx|pdf" />
                                            </label>
                                        </span>
                                        {(this.state.filename) ? <p>File Name: <small>{this.state.filename}</small></p> : ''}
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-7"></div>
                                    <div className="col-md-5">
                                        <input type="submit" className="but" value={'Save'} name="content" disabled={(this.state.submit_form) ? false : true} onClick={(e) => this.uploadHandler(e)} />
                                    </div>
                                </div>
                            </div>
                        </Modal.Body>
                    </form>
                </Modal>
                </BlockUi>
            </div>
        );
    }
}

