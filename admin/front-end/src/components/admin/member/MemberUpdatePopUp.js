import React, { Component } from 'react';
import jQuery from "jquery";
import { postData,getOptionsSuburb,handleChangeSelectDatepicker,handleShareholderNameChange,handleAddShareholder,handleRemoveShareholder } from '../../../service/common.js';
import {memberRelationDropdown,memberPreferContactDropdown} from '../../../dropdown/memberdropdown.js';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import Modal from 'react-bootstrap/lib/Modal'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux'
import { updateMemberProfileData } from './actions/MemberAction.js';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { ToastUndo } from 'service/ToastUndo.js'

class MemberUpdatePopUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
           phone_ary:[{'phone':'','primary_phone':''}],
           email_ary:[{'email':'','primary_email':''}], 
           completeAddress:[{'location':'','city':'','state':'','postal':''}],          
           kin_ary:[{'firstname':'','email':'','phone':'','relation':''}],          
        }
    }
    
    componentWillReceiveProps(newProps) {
        this.setState(newProps.MemberProfile,()=>{
            if(this.state.phone_ary.length == 0)
                this.setState({phone_ary:[{'phone':'','primary_phone':''}]});
            
            if(this.state.email_ary.length == 0)
                this.setState({email_ary:[{'email':'','primary_email':''}]});
            
            if(this.state.completeAddress.length == 0)
                this.setState({completeAddress:[{'location':'','city':'','state':'','postal':''}]});
        
            if(this.state.kin_ary.length == 0)
               this.setState({ kin_ary:[{'firstname':'','email':'','phone':'','relation':''}] });
        });
        this.setState({pageTitle:newProps.pageTitle});
    }

    selectChange=(selectedOption, fieldname) => {
        var state = {};
        state[fieldname] = selectedOption; 
        if(fieldname == 'city' && selectedOption)       
            state['postal'] = state.city.postcode; 

        this.setState(state);        
    } 
 
    componentDidMount() {
        postData('common/Common/get_state', {}).then((result) => {
            if (result.status) {
                var details = result.data
                this.setState({stateList: details});
            }
        });
    }

    handleAddressChange =(idx, value, fieldName, fieldType ) =>{
        var state = {};
        var List = this.state['completeAddress'];
        List[idx][fieldName] = value; 
        if(fieldName == 'state'){

            let stateListData = this.state.stateList;
            let checkActiveRecord = stateListData.findIndex(x => x.value == value );
            if(checkActiveRecord>=0 && this.state.stateList!= undefined ){
                List[idx]['statename'] = typeof this.state.stateList[checkActiveRecord] != 'undefined' && this.state.stateList[checkActiveRecord].hasOwnProperty('label') ? this.state.stateList[checkActiveRecord].label:'';
            }
            List[idx]['city'] = {}
            List[idx]['postal'] = ''
        }

        if(fieldName == 'city' && value){
             List[idx]['postal'] = value.postcode
        }         
        state['completeAddress'] = List;
        this.setState(state);
    }
    
    onSubmit = (e) => {
        e.preventDefault()
         jQuery('#updateMemberProfileForm').validate({ });
         if(jQuery('#updateMemberProfileForm').valid()){
            this.setState({loading: true});
            var member_id = this.state.ocs_id; 
            const dataUpdate  = this.state;
            const dataupdateDate = this.state.dob;  
            if(dataupdateDate!=undefined){
                let dateData =  typeof(dataupdateDate) == 'object' ?  dataupdateDate : moment(dataupdateDate,'DD-MM-YYYY');
                dataUpdate['dob'] = dateData;
            }
             
            var requestData = {member_id: member_id,member_data:dataUpdate};

            postData('member/MemberDashboard/update_Member_profile', requestData).then((result) => {  
            if (result.status) {
                toast.success(<ToastUndo message={result.msg} showType={'s'} />, {
                // toast.success(result.msg, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                }); 
              
                var NewReduxData = {
                                    phone_ary:this.state.phone_ary,
                                    email_ary:this.state.email_ary,
                                    dob:this.state.dob,
                                    prefer_contact:this.state.prefer_contact,
                                    statename:this.state.statename,
                                    completeAddress:this.state.completeAddress,
                                    kin_ary:this.state.kin_ary,
                                };
                this.props.update_member_profile(NewReduxData);
                this.props.closeMemberUpdatePopUp(true);
            }
            else{
                toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                // toast.error(result.error, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                }); 
            }
            this.setState({loading: false})
        });

       }
    }   
    
  
    render() {
        return (
             <Modal
               className="Modal fade Modal_A Modal_B UM_modal Orange M_Modal_size "
               show={this.props.member_update_modal}
               container={this}
               aria-labelledby="contained-modal-title"
             >
              <Modal.Body>
            <form id="updateMemberProfileForm" method="post">
                <div className="dis_cell-1">
                    <div className="text text-left by-1 mb-5">Update Members Details:
                        <a onClick={() => this.props.closeMemberUpdatePopUp() } className="close_i pull-right mt-1"><i className="icon icon-cross-icons"></i></a>
                    </div>
                        
                        <h3><strong>Name:</strong>{this.state.full_name}</h3>
                        <h3><strong>HCM-ID:</strong>{this.state.ocs_id}</h3>
                     
                    <div className="row" >  
                         <div className="col-md-6 mt-4">
                           {this.state.phone_ary.map((value, idx) => (
                            <div  className="mb-3" key={idx+1}>
                                <label>Phone ({(idx == 0)?'Primary':'Secondary'}):</label>
                                <span className="required">
                                    <div className="input_plus__ mb-1">
                                        <input type="text" className="input_f distinctOrgPh" placeholder="Can Include Area Code" value={value.phone} name={'org_phone'+idx} required onChange={(e) => handleShareholderNameChange(this,'phone_ary',idx,'phone',e.target.value)} autoComplete="new-password" data-rule-notequaltogroup='[".distinctOrgPh"]' data-msg-notequaltogroup="Please enter a unique Phone." data-rule-phonenumber/>
                                        {idx > 0 ? <button onClick={(e) => handleRemoveShareholder(this,e, idx , 'phone_ary')} >
                                            <i  className="icon icon-decrease-icon Add-2" ></i>
                                            </button> : (this.state.phone_ary.length == 2)? '': <button onClick={(e) =>handleAddShareholder(this,e, 'phone_ary',value)}>
                                            <i  className="icon icon-add-icons Add-1" ></i>
                                            </button>
                                        }
                                    </div>
                                </span>
                            </div>
                         ))}
                        </div>
                        
                        <div className="col-md-6 mt-4">
                            {
                            this.state.email_ary.map((value, idx) => (
                            <div className="mb-3" key={idx+1}>
                            <label>Email ({(idx == 0)?'Primary':'Secondary'}):</label>
                            <span className="required">
                                <div className="input_plus__ mb-1">
                                    <input type="email" className="input_f distinctEmail" placeholder="example@example.com" value={value.email} name={'email'+idx} required onChange={(e) => handleShareholderNameChange(this,'email_ary',idx,'email',e.target.value)} autoComplete="new-password" data-rule-notequaltogroup='[".distinctEmail"]'/>
                                    {idx > 0 ? <button onClick={(e) => handleRemoveShareholder(this,e, idx , 'email_ary')} >
                                                <i  className="icon icon-decrease-icon Add-2" ></i>
                                                </button> : (this.state.email_ary.length == 2)? '': <button onClick={(e) =>handleAddShareholder(this,e, 'email_ary',value)}>
                                                <i  className="icon icon-add-icons Add-1" ></i>
                                                </button>
                                            } 
                                        </div>
                                    </span>
                                </div>
                            ))}
                        </div> 
                    </div>

                      <div className="row P_15_T" >  
                         <div className="col-md-6 mt-4">                       
                        <label>Preferred Contact:</label>
                        <span className="required">
                            <div className="add_input">
                                <Select name="prefer_contact" required={true} simpleValue={true} searchable={false} clearable={false}  options={memberPreferContactDropdown(0)} onChange={(e)=>this.setState({'prefer_contact':e})} value={this.state.prefer_contact}/>   
                            </div>
                            </span>
                    </div>
                    
                    <div className="col-md-3  mt-4">
                                <label>Date of Birth: </label>
                                <span className="required">
                                    <DatePicker isClearable={true} name="dob" onChange={(e)=>handleChangeSelectDatepicker(this,e,'dob')} selected={this.state['dob'] ? moment(this.state['dob'], 'DD-MM-YYYY') : null} className="text-center" placeholderText="00-00-0000" dateFormat='DD-MM-YYYY'/>
                                </span>
                            </div>
                        </div>

                            {this.state.completeAddress.map((value, idx) => (
                             <div className="row P_15_T"  key={idx}>
                            <div className="col-md-4">
                               <label>Address ({(idx == 0)?'Primary':'Secondary'}):</label>
                                <span className="required">
                                    <input type="text" data-rule-required="true" onChange={(e) => this.handleAddressChange(idx,e.target.value,'street')} value={value.street} name={"street_"+idx} placeholder="" />
                                </span>
                            </div>
                            <div className="col-md-2">
                                <label>State: </label>
                                <Select clearable={false} name={"state_"+idx} className="default_validation" simpleValue={true} required={true}
                                value={value.state}  onChange={(e) => this.handleAddressChange(idx,e,'state','select')}
                                options={this.state.stateList} placeholder="Please Select" />
                            </div>
                            <div className="col-md-3">
                                <label>Suburb:</label> 
                                <span className="required modify_select">
                                    <Select.Async clearable={false} cache={false} className="default_validation" name={"city_"+idx}
                                    value={value.city} disabled={(value.state)? false: true} loadOptions={(val) => getOptionsSuburb(val, value.state)} onChange={(e) => this.handleAddressChange(idx,e,'city')}
                                     placeholder="Please Select" required />
                                </span>
                            </div>

                            <div className="col-md-2">
                                <label>Postcode: </label> 
                                <span className="required">
                                    <input type="text" value={value.postal} onChange={(e) => this.handleAddressChange(idx,e.target.value,'postal')}  data-rule-required="true" className="text-center" name={"postal"+idx} placeholder="" data-rule-number="true" minLength="4" maxLength="4" data-rule-postcodecheck="true"/>
                                </span>
                            </div>

                            <div className="col-md-1" >
                            <label className="w-100">&nbsp;</label>
                               {idx > 0 ? <button onClick={(e) => handleRemoveShareholder(this,e, idx , 'completeAddress')} className="button_plus__">
                                        <i  className="icon icon-decrease-icon Add-2-2" ></i>
                                    </button> : (this.state.completeAddress.length == 3)? '': <button className="button_plus__" onClick={(e) =>handleAddShareholder(this,e, 'completeAddress',value)}>
                                        <i  className="icon icon-add-icons Add-2-1" ></i>
                                    </button>
                            }
                            </div>
                             </div>
                            )) }
                           

                            {this.state.kin_ary.map((val, idx) => (
                             <div className="row P_15_T" key={idx}>
                                <div className="col-md-3">
                                <label>Next of kin: </label>
                                <span className="required">
                                    <input type="text" id={'kin_name_'+idx} data-rule-required="true" onChange={(e) => handleShareholderNameChange(this,'kin_ary',idx,'firstname',e.target.value)} value={val.firstname} name={"firstname"+idx} placeholder="Next of kin" />
                                </span>
                            </div>  
                             <div className="col-md-2">
                                <label>Relation: </label>
                                <span className="required">
                                     <Select name={"relation_data_"+idx} 
                                      id={'relation_data_'+idx}  
                                      className="default_validation"
                                      required
                                      simpleValue={true} 
                                      onChange={(e) => handleShareholderNameChange(this,'kin_ary',idx,'relation',e)} 
                                      searchable={false} 
                                      clearable={false} 
                                      options={memberRelationDropdown(0)} 
                                      placeholder="Select"
                                      value={val.relation} />
                                </span>
                            </div> 

                            
                             <div className="col-md-3">
                                <label>Email: </label>
                                <span className="required">
                                    <input type="text" data-rule-required="true" id={'kin_email_'+idx} onChange={(e) => handleShareholderNameChange(this,'kin_ary',idx,'email',e.target.value)} value={val.email} name={"email"+idx} placeholder="" />
                                </span>
                            </div> 
                             <div className="col-md-3">
                                <label>Phone: </label>
                                <span className="required">
                                    <input type="text" id={'kin_phone_'+idx} data-rule-required="true" onChange={(e) => handleShareholderNameChange(this,'kin_ary',idx,'phone',e.target.value)} value={val.phone} name={"phone"+idx} placeholder="" data-rule-phonenumber/>
                                </span>
                            </div>
                            
                             <div className="col-md-1" >
                             <label className="w-100">&nbsp;</label>
                               {idx > 0 ? <button onClick={(e) => handleRemoveShareholder(this,e, idx , 'kin_ary')} className="button_plus__">
                                        <i  className="icon icon-decrease-icon Add-2-2" ></i>
                                    </button> : (this.state.kin_ary.length == 2)? '': <button className="button_plus__" onClick={(e) =>handleAddShareholder(this,e, 'kin_ary',val)}>
                                        <i  className="icon icon-add-icons  Add-2-1" ></i>
                                    </button>
                            }
                            </div>
                            
                            </div>
                          )) }                
                    <div className="row">
                        <div className="col-md-8"></div>
                        <div className="col-md-4 P_15_T">
                            <button disabled={this.state.loading} onClick={this.onSubmit} className="but_submit">Save Changes</button>
                        </div>
                    </div>

                </div>
            </form>
            </Modal.Body>
        </Modal>

            )
    }
}


const mapStateToProps = state => ({
    MemberProfile : state.MemberReducer.memberProfile
})

 const mapDispatchtoProps = (dispach) => {
       return {
           //org_update_booking_comp_fun: (value) => dispach(updateMemberProfileData(value)),
            update_member_profile: (value) => dispach(updateMemberProfileData(value)),
      }
}
                
export default connect(mapStateToProps, mapDispatchtoProps)(MemberUpdatePopUp)