import React, { Component } from 'react';
import jQuery from "jquery";
import { ROUTER_PATH } from '../../../config.js';
import { checkItsNotLoggedIn, postData, handleChangeSelectDatepicker, handleChangeChkboxInput, handleShareholderNameChange, handleAddShareholder, handleRemoveShareholder, getStateList, getOptionsMember, getOptionsParticipant, getFmscasePrimaryCategory, queryOptionData } from '../../../service/common.js';
import { addressCategory, againstCategory, initiatorCategory } from '../../../dropdown/Fmsdropdown.js';
import DatePicker from 'react-datepicker';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { toast } from 'react-toastify';
import moment from 'moment-timezone';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import { ToastUndo } from 'service/ToastUndo.js'

const getOptionsSuburb = (input, state) => {
    return queryOptionData(input, 'common/Common/get_suburb', { query: input, state: state }, 3);
}

const getOnCallAdmin = (input) => {
    return queryOptionData(input, 'common/Common/get_admin_name', { search: input });
}

class CreateFmsCase extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn(ROUTER_PATH);
        ////checkPinVerified();
        this.state = {
            completeAddress: [{ 'event_location': '', 'suburb': '', 'state': '', postal: '', address_category: '' }],
            againstDetail: [{ 'againstCategory': 1, 'firstName': '', 'lastName': '', 'againstOnCallMember': '', 'againstEmail': '', 'againstPhone': '', 'againstOnCallParticipant': '', 'againstOnCallUserAdmin': '' }],
            initiatorCategory: 5,
            loading: false,
            creation_date: moment(),
        };
        moment.tz.setDefault("Australia/Melbourne");
    }

    componentDidMount() {
        getStateList().then((result) => {
            var stateList = result;
            this.setState({ userState: stateList }, () => { });
        });

        getFmscasePrimaryCategory().then((result) => {
            this.setState({ caseDetailPrimaryCategory: result }, () => { });
        });
    }



    handleAddressChange = (idx, value, fieldName, fieldType) => {
        var state = {};
        var List = this.state['completeAddress'];
        List[idx][fieldName] = value;
        if (fieldName == 'state') {
            List[idx]['suburb'] = {}
            List[idx]['postal'] = ''
        }

        if (fieldName == 'suburb' && value) {
            List[idx]['postal'] = value.postcode
        }
        state['completeAddress'] = List;
        this.setState(state);
    }

    handleAddHtmlAgainstCat(obj, e, stateName, object_array) {
        e.preventDefault();
        var state = {};
        var temp = object_array
        var list = obj.state[stateName];
        state[stateName] = list.concat([this.reInitializeObject(temp)]);
        obj.setState(state);
    }

    reInitializeObject(object_array) {
        var state = {}
        Object.keys(object_array).forEach(function (key) {
            if (key == 'againstCategory')
                state['againstCategory'] = 2;
            //state['againstCategory'] = 1;
            else
                state[key] = '';
        });
        return state;
    }

    memberOnCallChange = (e, type, key) => {
        var state = {};
        var List = this.state[type];
        List[key].name = e;
        state[type] = List;
        this.setState(state);
    }

    handleSaveCase = (e) => {
        e.preventDefault();
        var validator = jQuery("#fms_case_form").validate({ ignore: [] });
        //if(jQuery("#fms_case_form").valid())
        if (!this.state.loading && jQuery("#fms_case_form").valid()) {
            this.setState({ loading: true }, () => {
                postData('fms/FmsDashboard/create_case', this.state).then((result) => {
                    if (result.status) {
                        this.setState({ loading: false });
                        toast.success(<ToastUndo message={"FMS Case created successfully."} showType={'s'} />, {
                        // toast.success("FMS Case created successfully.", {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                        this.setState({ is_save: true });
                    } else {
                        toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                        // toast.error(result.error, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                        this.setState({ loading: false });
                    }
                });
            });
        }
        else {
            validator.focusInvalid();
        }
    }

    handleChangeSelect(Obj, selectedOption, fieldname) {
        var state = {};
        this.setState({ initiator_detail: '', 'initiator_first_name': '', 'initiator_last_name': '', 'initiator_email': '', 'initiator_phone': '' });
        state[fieldname] = selectedOption;
        Obj.setState(state);
    }

    render() {
        return (
            <div>
                {(this.state.is_save) ? this.props.props.history.goBack() : ''}
                <BlockUi tag="div" blocking={this.state.loading}>
                  
                        <form id="fms_case_form" method="post" autoComplete="off" >
                           <div className="row  _Common_back_a">
                                    <div className="col-lg-10 col-lg-offset-1 col-md-12">
                                        <a onClick={this.props.props.history.goBack}>
                                            <span className="icon icon-back-arrow back_arrow"></span>
                                        </a>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-lg-10 col-lg-offset-1 col-sm-12">
                                        <div className="bor_T"></div>
                                    </div>
                                </div>

                                <div className="row _Common_He_a">
                                    <div className="col-lg-10 col-lg-offset-1 col-sm-12">
                                        <h1 className="color">Creating Feedback Case</h1>
                                    </div>
                                </div>

                                <div className="row">
                                <div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div>
                                </div>

                                <div className="row P_25_T">
                                    <div className="col-lg-1"></div>

                                    <div className="col-lg-2 col-md-3 col-sm-12">
                                        <label>Event Date:</label>
                                        <span className="required">
                                            <DatePicker placeholderText={"00/00/0000"} className="text-center" selected={this.state.event_date ? moment(this.state.event_date, 'DD-MM-YYYY') : null} name="event_date" required dateFormat="DD/MM/YYYY" onChange={(e) => handleChangeSelectDatepicker(this, e, 'event_date')} />
                                        </span>
                                    </div>

                                    <div className="col-lg-2 col-md-3 col-sm-12 default_validation">
                                        <label>Case Category:</label>
                                        <Select name="CaseCategory" required={true} simpleValue={true} searchable={true} clearable={false}
                                            placeholder="Case Category" options={this.state.caseDetailPrimaryCategory} value={this.state.CaseCategory} onChange={(e) => this.handleChangeSelect(this, e, 'CaseCategory')} />
                                    </div>

                                </div>
                                <div className="row P_25_T">

                                    <div className="col-lg-2 col-lg-offset-1 col-md-3 col-sm-12">
                                        <label>Initiator Category:</label>
                                        <Select name="initiatorCategory" required={true} simpleValue={true} searchable={false} clearable={false} value={this.state.initiatorCategory} onChange={(e) => this.handleChangeSelect(this, e, 'initiatorCategory')} options={initiatorCategory()} placeholder="" />
                                    </div>
                                    {
                                        (this.state.initiatorCategory && this.state.initiatorCategory == 5) ?
                                            <div>
                                                <div className="col-lg-2 col-md-3 col-sm-12">
                                                    <label>Initiator Details:</label>
                                                    <span className="required">
                                                        <input placeholder="First" type="text" name="initiator_first_name" value={this.state.initiator_first_name} required onChange={(e) => handleChangeChkboxInput(this, e)} />
                                                    </span>
                                                </div>
                                                <div className="col-lg-2 col-md-3 col-sm-12">
                                                    <label>&nbsp;</label>
                                                    <input placeholder="Last" type="text" name="initiator_last_name" value={this.state.initiator_last_name} onChange={(e) => handleChangeChkboxInput(this, e)} />
                                                </div>

                                                <div className="col-lg-2 col-md-3 col-sm-12">
                                                    <label>&nbsp;</label>
                                                    <span className="required">
                                                        <input placeholder="Email" type="email" name="initiator_email" value={this.state.initiator_email} required onChange={(e) => handleChangeChkboxInput(this, e)} />
                                                    </span>
                                                </div>

                                                <div className="col-lg-2 col-md-3 col-sm-12">
                                                    <label>&nbsp;</label>
                                                    <span className="required">
                                                        <input placeholder="Phone" type="text" name="initiator_phone" value={this.state.initiator_phone} required onChange={(e) => handleChangeChkboxInput(this, e)} data-rule-phonenumber />
                                                    </span>
                                                </div>
                                            </div>
                                            :
                                            (this.state.initiatorCategory && this.state.initiatorCategory == 1) ?
                                                <div className="col-lg-4col-md-3 col-sm-12">
                                                    <label>Initiator Details:</label>
                                                    <span className="requireds modify_select">
                                                        <div className="search_icons_right modify_select default_validation">
                                                            <Select.Async
                                                                name="form-field-name"
                                                                loadOptions={(e) => getOptionsMember(e)}
                                                                clearable={false}
                                                                placeholder='Search'
                                                                cache={false}
                                                                value={this.state.initiator_detail}
                                                                onChange={(e) => handleChangeSelectDatepicker(this, e, 'initiator_detail')}
                                                                required={true}
                                                            />
                                                        </div>
                                                    </span>
                                                </div>
                                                :
                                                (this.state.initiatorCategory && this.state.initiatorCategory == 2) ?
                                                    <div className="col-lg-4 col-md-6 col-sm-12">
                                                        <label>Initiator Details:</label>
                                                        <span className="requireds modify_select">
                                                            <div className="search_icons_right modify_select default_validation">
                                                                <Select.Async
                                                                    name="form-field-name"
                                                                    loadOptions={(e) => getOptionsParticipant(e)}
                                                                    clearable={false}
                                                                    cache={false}
                                                                    placeholder='Search'
                                                                    value={this.state.initiator_detail}
                                                                    onChange={(e) => handleChangeSelectDatepicker(this, e, 'initiator_detail')}
                                                                    required={true}
                                                                />
                                                            </div>
                                                        </span>
                                                    </div>
                                                    :
                                                    (this.state.initiatorCategory && this.state.initiatorCategory == 6) ?
                                                        <div>
                                                            <div className="col-lg-2 col-md-3 col-sm-12">
                                                                <label>Initiator Details:</label>
                                                                <span className="required">
                                                                    <input placeholder="First" type="text" name="initiator_first_name" required onChange={(e) => handleChangeChkboxInput(this, e)} />
                                                                </span>
                                                            </div>
                                                            <div className="col-lg-2 col-md-3 col-sm-12">
                                                                <label>&nbsp;</label>
                                                                <input placeholder="Last" type="text" name="initiator_last_name" onChange={(e) => handleChangeChkboxInput(this, e)} />
                                                            </div>
                                                        </div>
                                                        :
                                                        (this.state.initiatorCategory && this.state.initiatorCategory == 7) ?
                                                            <div className="col-lg-4 col-md-6 col-sm-12">
                                                                <label>Initiator Details:</label>
                                                                <span className="requireds modify_select">
                                                                    <div className="search_icons_right modify_select default_validation">
                                                                        <Select.Async
                                                                            name="form-field-name"
                                                                            loadOptions={(e) => getOnCallAdmin(e)}
                                                                            clearable={false}
                                                                            cache={false}
                                                                            placeholder='Search'
                                                                            value={this.state.initiator_detail}
                                                                            onChange={(e) => handleChangeSelectDatepicker(this, e, 'initiator_detail')}
                                                                            required={true}
                                                                        />
                                                                    </div>
                                                                </span>
                                                            </div>
                                                            :
                                                            ''
                                    }
                                </div>
                                <div className="row P_25_T">
                                    <div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                                    <div className="col-lg-10 col-lg-offset-1 P_15_TB col-sm-12"><h3 className="color">Parties & Location:</h3></div><div className="col-lg-1"></div>
                                    <div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                                </div>

                                {
                                    this.state.againstDetail.map((value, idx) => (
                                        <div key={idx}>
                                            <div className="row P_25_T">
                                                <div className="col-lg-2 col-lg-offset-1 col-md-3 col-sm-12">
                                                    <label>‘Against’ Category:</label>
                                                    <Select name={'againstCategory' + idx} required={true} simpleValue={true} searchable={false} clearable={false} value={value.againstCategory} onChange={(e) => handleShareholderNameChange(this, 'againstDetail', idx, 'againstCategory', e)} options={againstCategory()} placeholder="" />
                                                </div>

                                                {(value.againstCategory && value.againstCategory == 1) ?
                                                    <div>
                                                        <div className="col-lg-2 col-md-3 col-sm-12">
                                                            <label>‘Against’ Details:</label>
                                                            <span className="required">
                                                                <input placeholder="First" type="text" name={'firstName' + idx} value={value.firstName} required onChange={(e) => handleShareholderNameChange(this, 'againstDetail', idx, 'firstName', e.target.value)} />
                                                            </span>
                                                        </div>

                                                        <div className="col-lg-2 col-md-3 col-sm-12">
                                                            <label>&nbsp;</label>
                                                            <input placeholder="Last" type="text" name={'lastName' + idx} value={value.lastName} onChange={(e) => handleShareholderNameChange(this, 'againstDetail', idx, 'lastName', e.target.value)} />
                                                        </div>

                                                        <div className="col-lg-2 col-md-3 col-sm-12">
                                                            <label>&nbsp;</label>
                                                            <input placeholder="Email" type="email" name={'againstEmail' + idx} value={value.againstEmail} onChange={(e) => handleShareholderNameChange(this, 'againstDetail', idx, 'againstEmail', e.target.value)} required />
                                                        </div>

                                                        <div className="col-lg-1 col-md-3 col-sm-12" >
                                                            <label>&nbsp;</label>
                                                            <input placeholder="Phone" type="text" name={'againstPhone' + idx} value={value.againstPhone} onChange={(e) => handleShareholderNameChange(this, 'againstDetail', idx, 'againstPhone', e.target.value)} required data-rule-phonenumber />
                                                        </div>
                                                    </div>
                                                    : (value.againstCategory && value.againstCategory == 2) ?
                                                        <div className="col-lg-4 col-md-6 col-sm-12">
                                                            <label>‘Against’ Details:</label>
                                                            <span className="requireds modify_select">
                                                                <div className="search_icons_right modify_select">
                                                                    <Select.Async
                                                                        name={'againstOnCallMember' + idx}
                                                                        loadOptions={(e) => getOptionsMember(e)}
                                                                        onChange={(e) => handleShareholderNameChange(this, 'againstDetail', idx, 'againstOnCallMember', e)}
                                                                        clearable={false}
                                                                        cache={false}
                                                                        value={value.againstOnCallMember}
                                                                        placeholder='Search member' />
                                                                </div>
                                                            </span>
                                                        </div>
                                                        : (value.againstCategory && value.againstCategory == 3) ?
                                                            <div className="col-lg-4 col-md-6 col-sm-12">
                                                                <label>‘Against’ Details:</label>
                                                                <span className="requireds modify_select">
                                                                    <div className="search_icons_right modify_select">
                                                                        <Select.Async
                                                                            name={'againstOnCallParticipant' + idx}
                                                                            loadOptions={(e) => getOptionsParticipant(e)}
                                                                            onChange={(e) => handleShareholderNameChange(this, 'againstDetail', idx, 'againstOnCallParticipant', e)}
                                                                            clearable={false}
                                                                            value={value.againstOnCallParticipant}
                                                                            cache={false}
                                                                            placeholder='Search participant' />
                                                                    </div>
                                                                </span>
                                                            </div>
                                                            : (value.againstCategory && value.againstCategory == 4) ?
                                                                <div>
                                                                    <div className="col-lg-2 col-md-3 col-sm-12">
                                                                        <label>Initiator Details:</label>
                                                                        <span className="required">
                                                                            <input placeholder="First" type="text" name="initiator_first_name" required onChange={(e) => handleChangeChkboxInput(this, e)} />
                                                                        </span>
                                                                    </div>
                                                                    <div className="col-lg-2 col-md-3 col-sm-12">
                                                                        <label>&nbsp;</label>
                                                                        <input placeholder="Last" type="text" name="initiator_last_name" onChange={(e) => handleChangeChkboxInput(this, e)} />
                                                                    </div>
                                                                </div>
                                                                : (value.againstCategory && value.againstCategory == 5) ?
                                                                    <div className="col-lg-4 col-md-6 col-sm-12">
                                                                        <label>Initiator Details:</label>
                                                                        <span className="requireds modify_select">
                                                                            <div className="search_icons_right modify_select default_validation">
                                                                                <Select.Async
                                                                                    name="form-field-name"
                                                                                    loadOptions={(e) => getOnCallAdmin(e)}
                                                                                    clearable={false}
                                                                                    placeholder='Search'
                                                                                    value={value.againstOnCallUserAdmin}
                                                                                    onChange={(e) => handleShareholderNameChange(this, 'againstDetail', idx, 'againstOnCallUserAdmin', e)}
                                                                                    cache={false}
                                                                                    required={true}
                                                                                />
                                                                            </div>
                                                                        </span>
                                                                    </div>
                                                                    :
                                                                    ''
                                                }

                                                <div className="col-lg-1 col-sm-1 P_20_T mt-1">
                                                    {idx > 0 ? <button onClick={(e) => handleRemoveShareholder(this, e, idx, 'againstDetail')} className="button_plus__">
                                                        <i className="icon icon-decrease-icon  Add-2-2" ></i>
                                                    </button> : (this.state.againstDetail.length == 3) ? '' : <button className="button_plus__" onClick={(e) => this.handleAddHtmlAgainstCat(this, e, 'againstDetail', value)}>
                                                        <i className="icon icon-add-icons  Add-2-1" ></i>
                                                    </button>}
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }


                                {this.state.completeAddress.map((value, idx) => (
                                    <div className="row P_25_T" key={idx}>
                                        <div className="col-lg-1"></div>
                                        <div className="col-lg-3 col-md-3 col-sm-12">
                                            <label>Event Location:</label>
                                            <span className="required">
                                                <input placeholder="Unit #/Street #,Street Name" type="text" name={'event_location' + idx} data-rule-required="true" value={value.event_location} onChange={(e) => this.handleAddressChange(idx, e.target.value, 'event_location')} />
                                            </span>
                                        </div>

                                        <div className="col-lg-1 col-md-2 col-sm-12">
                                            <label>State:</label>
                                            <span className="required">
                                                <Select name={'state' + idx} required={true} simpleValue={true} searchable={true} clearable={false} value={value.state} onChange={(e) => this.handleAddressChange(idx, e, 'state', 'select')} options={this.state.userState} placeholder="State" className="default_validation" />
                                            </span>

                                        </div>

                                        <div className="col-lg-2 col-md-3 col-sm-12">
                                            <label>Suburb:</label>
                                            <span className="required modify_select">
                                                <Select.Async clearable={false} cache={false} name={'suburb' + idx} className="default_validation search_icons_right modify_select" required={true}
                                                    value={value.suburb} disabled={(value.state) ? false : true} loadOptions={(val) => getOptionsSuburb(val, value.state)} onChange={(e) => this.handleAddressChange(idx, e, 'suburb')}
                                                    placeholder="Please Select" />
                                            </span>
                                        </div>

                                        <div className="col-lg-1 col-md-3 col-sm-12">
                                            <label>PostCode:</label>
                                            <span className="required">
                                                <input placeholder="0000" className="text-center" type="text" name={'postal' + idx} data-rule-required="true" value={value.postal} onChange={(e) => this.handleAddressChange(idx, e.target.value, 'postal')} data-rule-number="true" minLength="4" maxLength="4" data-rule-postcodecheck="true" />
                                            </span>
                                        </div>

                                        <div className="col-lg-2 col-md-3 col-sm-12">
                                            <label>Category:</label>
                                            <Select name="address_category" required={true} simpleValue={true} searchable={false} clearable={false} value={value.address_category} onChange={(e) => this.handleAddressChange(idx, e, 'address_category')} options={addressCategory(0)} placeholder="Please Select" />
                                        </div>

                                        <div className="col-lg-1 col-md-2 col-sm-12 P_20_T mt-1">
                                            {idx > 0 ? <button onClick={(e) => handleRemoveShareholder(this, e, idx, 'completeAddress')} className="button_plus__">
                                                <i className="icon icon-decrease-icon Add-2-2" ></i>
                                            </button> : (this.state.completeAddress.length == 3) ? '' : <button className="button_plus__" onClick={(e) => handleAddShareholder(this, e, 'completeAddress', value)}>
                                                <i className="icon icon-add-icons Add-2-1" ></i>
                                            </button>}
                                        </div>
                                    </div>
                                ))}

                                <div className="row P_25_T">
                                    <div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                                    <div className="col-lg-10 col-lg-offset-1 P_15_TB col-sm-12"><h3 className="color">Case Details:</h3></div><div className="col-lg-1"></div>
                                    <div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                                </div>
                                <div className="row P_25_TB">
                                    <div className="col-lg-1"></div>
                                    <div className="col-lg-4 col-md-5 col-sm-12">
                                        <label>Reason:</label>
                                        <span className="required">
                                            <div className="required input_and_textarea" style={{ overflow: 'inherit' }} >
                                                <input type="text" className="border_R1 bx-0 bt-0 mt-2" name="title" placeholder="Please Type a Title Here." required onChange={(e) => handleChangeChkboxInput(this, e)} />

                                                <textarea className="border_R2 bz w-100 mt-2 textarea-max-size" type="textarea" name="description" placeholder="A Short Description Can Go Here." required onChange={(e) => handleChangeChkboxInput(this, e)}></textarea>
                                            </div>
                                        </span>
                                    </div>
                                    <div className="col-lg-1 col-sm-1"></div>
                                    <div className="col-lg-4 col-md-5 col-sm-12">
                                        <label>Notes:</label>
                                        <span className="required">
                                            <div className="required input_and_textarea" style={{ overflow: 'inherit' }} >

                                                <input type="text" className="border_R1 bx-0 bt-0 mt-2" name="notes_title" placeholder="Please Type a Title Here." required onChange={(e) => handleChangeChkboxInput(this, e)} />

                                                <textarea className="border_R2 bz w-100 mt-4 textarea-max-size" name="notes" type="textarea" placeholder="Any Notes Can Go Here." data-rule-required="true" onChange={(e) => handleChangeChkboxInput(this, e)}></textarea>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-1"></div>
                                    <div className="col-lg-2 col-lg-offset-8 col-md-3 col-md-offset-8 col-sm-12">
                                        <button className="but_submit" onClick={(e) => this.handleSaveCase(e, 1)} disabled={this.state.loading}>Save Case</button>
                                    </div>
                                    <div className="col-lg-1 col-sm-1"></div>
                                </div>
                           
                        </form>
                    
                </BlockUi>
            </div>
        );
    }
}
export default CreateFmsCase;
