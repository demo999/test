import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';

import { checkItsNotLoggedIn, getPermission, checkPinVerified } from '../../../service/common.js';
import { ROUTER_PATH } from '../../../config.js';
import { setFooterColor } from '../../admin/notification/actions/NotificationAction.js';
import { connect } from 'react-redux'
import Dashboard from './FmsDashboard';
import CreateCase from './CreateFmsCase';
import CaseDetail from './CaseDetail';
import CaseMonitor from './CaseMonitor';
import Sidebar from '../Sidebar';
import {fmsJson} from 'menujson/fms_menu_json';
import {setSubmenuShow} from 'components/admin/actions/SidebarAction';


const menuJson = () => {
    let menu = fmsJson;
    return menu;
}
class AppFms extends Component {
    constructor(props) {
        super(props);
        this.permission = (getPermission() == undefined) ? [] : JSON.parse(getPermission());
        checkItsNotLoggedIn(ROUTER_PATH);
        checkPinVerified('fms');

        this.state = {
            subMenuShowStatus:true,
            menus:menuJson(),
            replaceData:{':id':0}
        }

    }

    permissionRediect() {
        checkItsNotLoggedIn();
        return <Redirect to={'/admin/no_access'} />;
    }

    componentDidMount() {
        this.props.setSubmenuShow(1);
        this.props.setFooterColor('Red_fms');
    }

    componentWillUnmount() {
        this.props.setFooterColor('');
        //this.props.setSubmenuShow(0);
    }

    componentWillReceiveProps(nextProps){
         if(this.props.case_details.id !=nextProps.case_details.id){
            let menuState = this.state.menus;
            let obj = menuState.find(x => x.id === 'fms_name_cases');
            let objIndex = menuState.indexOf(obj);
            let casDetailsDataType = ['case_details','case_respond','case_monitor'];
            if(nextProps.getSidebarMenuShow.subMenuShow && nextProps.case_details.id!='' && casDetailsDataType.indexOf(nextProps.showPageType)>-1){
                menuState[objIndex]['linkShow'] = true;
            }else{
                menuState[objIndex]['linkShow'] = false;
            }
            this.setState({menus:menuState});
            this.setState({replaceData:{':id':nextProps.case_details.id}},()=>{
                let menuState = this.state.menus;
                let obj = menuState.find(x => x.id === 'fms_name_cases');
                let objIndex = menuState.indexOf(obj);
                if(nextProps.getSidebarMenuShow.subMenuShow){
                    menuState[objIndex]['name'] = 'Case Id: '+nextProps.case_details.id;
                }
                this.setState({menus:menuState});
            });
        } 

    }

    render() {
        return (
            <section className="asideSect__ manage_top Red_fms">
            <Sidebar
                    heading={'FMS'}
                    menus={this.state.menus}
                    subMenuShowStatus={this.state.subMenuShowStatus}
                    replacePropsData={this.state.replaceData}
                />
            <React.Fragment>
               
                    <div className="container-fluid ">
                        <Switch >
                            <Route exact path={ROUTER_PATH + 'admin/fms/dashboard/:type/:page?'} render={(props) => this.permission.access_fms ? <Dashboard props={props} /> : this.permissionRediect()} />
                            <Route exact path={ROUTER_PATH + 'admin/fms/create_case'} render={(props) => this.permission.create_fms ? <CreateCase props={props} /> : this.permissionRediect()} />
                            <Route exact path={ROUTER_PATH + 'admin/fms/case/:id/:page?'} render={(props) => this.permission.access_fms ? <CaseDetail props={props} /> : this.permissionRediect()} />
                            <Route exact path={ROUTER_PATH + 'admin/fms/case/:id/log'} render={(props) => this.permission.access_fms ? <CaseMonitor props={props} /> : this.permissionRediect()} />
                        </Switch>
                    </div>
              
            </React.Fragment>
            </section>
        );
    }
}

const mapStateToProps = state => ({
    getSidebarMenuShow: state.sidebarData,
    case_details: state.FmsCaseDetailsData.case_details,
    showPageType: state.FmsCaseDetailsData.activePage.pageType,
})

const mapDispatchtoProps = (dispach) => {
    return {
        setFooterColor: (result) => dispach(setFooterColor(result)),
        setSubmenuShow: (result) => dispach(setSubmenuShow(result))
    }
}

const AppFmsData = connect(mapStateToProps, mapDispatchtoProps)(AppFms)
export { AppFmsData as AppFms };