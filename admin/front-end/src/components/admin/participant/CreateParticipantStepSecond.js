import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import jQuery from "jquery";

import '../../../service/jquery.validate.js';

import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import Header from '../../../components/admin/Header';
import Footer from '../../../components/admin/Footer';
import { postData } from '../../../service/common.js';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import { ToastUndo } from 'service/ToastUndo.js'
import { interpretertDropdown, religiousDropdown, ethnicityDropdown, prefterLanguageDropdown, cognitionDropdown, communicationDropdown } from '../../../dropdown/ParticipantDropdown.js';

class CreateParticipantStepSecond extends Component {
    constructor(props) {

        super(props);
        this.state = []
        this.initialState = {
            success: false,
            first_step: false,
            ocservices: '',
            participantReqAssistance: '',
            participantReqAssistanceOther: '',
            participantocservices: '',
            participantsupport: '',
            participantsupportother: '',
           // CarersInput: [{ gender: '', ethnicity: '', religious: '', gender_error: false, ethnicity_error: false, religious_error: false }],
            Communication_option: communicationDropdown(0),
            Cognition_option: cognitionDropdown(0),
            Hearing_option: [{ value: 1, label: 'Yes' }, { value: 2, label: 'No' }],
            Linguistic_option: interpretertDropdown(0),
            English_option: [{ value: 1, label: 'Yes preferred' }, { value: 2, label: 'Yes but not preferred' }],
            Ethnicity_male_option: ethnicityDropdown(0),
            Religious_male_option: religiousDropdown(0),
            Ethnicity_female_option: ethnicityDropdown(0),
            Religious_female_option: religiousDropdown(0),
            Gender_option: [{ value: 1, label: 'Male' }, { value: 2, label: 'Female' }],
            require_assistance: [],
            os_Services: [],
            support_required: [],
            mobality: [],
            all_option_male:false,
            all_option_female:false,
        }
        this.state = this.initialState;
        this.validator = false;
    }

    setMutipleCheckbox = (e, idx, tagType,carersFor) => {
     var List = this.state[tagType];
     if(typeof carersFor !== 'undefined' && (carersFor == 'male' || carersFor == 'female'))
     {
        if(List[idx]['active']){
            if(carersFor == 'male'){
                this.setState({all_option_male:false})
            }
            else{
                this.setState({all_option_female:false})
            }
        }
    }


        if (List[idx]['active']) {
            List[idx]['active'] = false;
        } else {
            List[idx]['active'] = true;
        }

        var state = {};
        state[tagType] = List;
        state[tagType + '_error'] = false;

        this.setState(state, () => {
            if (this.validator) {
                jQuery("#create_participant_step_second").valid();
            }
        });
    }

    getParticipantGeneral = () => {
        postData('participant/ParticipantDashboard/get_participant_general', []).then((result) => {
            if (result.status) {
                this.setState({ require_assistance: result.data.assistance });
                this.setState({ os_Services: result.data.oc_service });
                this.setState({ support_required: result.data.support });
                this.setState({ mobality: result.data.mobality });
            }
            this.setState({ loading: false });
        });
    }

    componentDidMount() {
        if (sessionStorage.getItem("participant_step_1") && this.props.props.location.state) {
            if (sessionStorage.getItem("participant_step_2") && this.props.props.location.state.second_step) {
                var secondTemp = JSON.parse(sessionStorage.getItem("participant_step_2"))
                this.setState(secondTemp);
            } else {
                this.getParticipantGeneral();
            }
            this.setState({ step_first: sessionStorage.getItem("participant_step_1") });
        } else {
            this.setState({ first_step: false });
        }
    }

    submit = (e) => {
        e.preventDefault();
        var custom_valid = this.custom_validation();
        this.validator = jQuery("#create_participant_step_second").validate({ ignore: [] });

        if (!this.state.loading && jQuery("#create_participant_step_second").valid() && custom_valid) {
            this.setState({ loading: true });
            postData('participant/ParticipantDashboard/create_participant', this.state).then((result) => {
                if (result.status) {
                    this.state = this.initialState;
                    toast.success(<ToastUndo message={'Participant created successfully'} showType={'s'} />, {
                        // toast.success("Participant created successfully", {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    setTimeout(() => this.setState({ success: true }), 100);
                } else {
                    this.setState({ error: result.error });
                    toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                        // toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }
                this.setState({ loading: false });
            });
        } else {
            this.validator.focusInvalid();
        }
    }



    handleChange = (e) => {
        var state = {};
        this.setState({ error: '' });
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }

    handleShareholderNameChange = (idx, evt, tagType, fieldtype) => {

        if (tagType == 'CarersInput') {
            const newShareholders = this.state.CarersInput.map((CarersInput, sidx) => {
                if (idx !== sidx)
                    return CarersInput;
                if (fieldtype == 'selectgender') {
                    return { ...CarersInput, gender: evt, gender_error: false };
                }
                if (fieldtype == 'selectethnicity') {
                    return { ...CarersInput, ethnicity: evt, ethnicity_error: false };
                }
                if (fieldtype == 'selectreligious') {
                    return { ...CarersInput, religious: evt, religious_error: false };
                }
            });
            this.setState({ CarersInput: newShareholders });
        }

    }
    selectChange = (selectedOption, fieldname) => {
        var state = {};

        if (fieldname == 'participantenglish' && selectedOption == 1) {
            state["participantPreferredlang"] = 1;
            state["participantPreferredlang_error"] = false;
        } else if (fieldname == 'participantenglish') {
            state["participantPreferredlang"] = '';
        }
        state[fieldname] = selectedOption;
        state[fieldname + '_error'] = false;
        this.setState(state);
    }
    handleAddShareholder = (e, tagType) => {
        e.preventDefault();
        if (tagType === 'CarersInput') {
            this.setState({ CarersInput: this.state.CarersInput.concat([{ name: '' }]) });
        }
    }

    handleRemoveShareholder = (e, idx, tagType) => {
        e.preventDefault();
        if (tagType === 'CarersInput') {
            this.setState({ CarersInput: this.state.CarersInput.filter((s, sidx) => idx !== sidx) });
        }
    }

checkCarersNotBookOption = (evt,ethnicityState,religionState,checkBoxName) =>{
    let ethnicity = this.state[ethnicityState];
    let religion = this.state[religionState];

    var tempState = {};
    tempState[checkBoxName] = evt.target.checked;
    this.setState(tempState);

        ethnicity.forEach(ethn => {
            ethn.active = evt.target.checked
        })

        religion.forEach(relgn => {
            relgn.active = evt.target.checked
        })

        this.setState({ ethnicityState: ethnicity });
        this.setState({ religionState: religion });

    }
    custom_validation = () => {
        var return_var = true;
        var state = {};
        var List2 = [{ key: 'participantCognition' }, { key: 'participantCommunication' }, { key: 'participantenglish' }, { key: 'participantPreferredlang' }];

        List2.map((list2, sidx) => {
            var state = {};
            if (this.state[list2.key] == undefined || this.state[list2.key] == '') {
                state[list2.key + '_error'] = true;
                this.setState(state);
                return_var = false;
            }
        });

    /*const newShareholders = this.state.CarersInput.map((object, sidx) => {
        var state = {};
        if (object.gender == '' || object.gender == undefined) {
            state['gender_error'] = true;
            return_var = false;
        }
        if (object.ethnicity == '' || object.ethnicity == undefined) {
            state['ethnicity_error'] = true;
            return_var = false;
        }
        if (object.religious == '' || object.religious == undefined) {
            state['religious_error'] = true;
            return_var = false;
        }

        Object.assign(object, state);
        return { ...object };
    });

    this.setState({ CarersInput: newShareholders });*/
    return return_var;
}

    backStepForm() {
        var str = JSON.stringify(this.state);
        sessionStorage.setItem("participant_step_2", str);
        this.setState({ first_step: true })
    }

    errorShowInTooltip($key, msg) {
        return (this.state[$key + '_error']) ? <div className={'tooltip custom-tooltip fade top in' + ((this.state[$key + '_error']) ? ' select-validation-error' : '')} role="tooltip">
            <div className="tooltip-arrow"></div><div className="tooltip-inner">{msg}.</div></div> : '';
    }

    errorShowInTooltipForLoop(key, msg) {
        return (key == true) ? <div className={'tooltip custom-tooltip fade top in' + ((key == true) ? ' select-validation-error' : '')} role="tooltip">
            <div className="tooltip-arrow"></div><div className="tooltip-inner">{msg}.</div></div> : '';
    }

    render() {
        // for require assitance required 
        var requireIndex = this.state.require_assistance.findIndex(x => x.label == 'Other');
        var requireOther = false;
        if (requireIndex > -1) {
            requireOther = (this.state.require_assistance[requireIndex].active) ? true : false;
        }

        // for require support required 
        var supportIndex = this.state.support_required.findIndex(x => x.label == 'Other');
        var supportOther = false;
        if (supportIndex > -1) {
            supportOther = (this.state.support_required[supportIndex].active) ? true : false;
        }

        return (<div>
            {(this.state.first_step) ? <Redirect to={{ pathname: '/admin/participant/create', state: { second_step: true } }} /> : ''}
            {(this.state.success) ? <Redirect to={'/admin/participant/dashboard'} /> : ''}
            <BlockUi tag="div" blocking={this.state.loading}>

                <form id="create_participant_step_second" onSubmit={this.submit}>


                    <div className="row  _Common_back_a">
                        <div className="col-lg-10 col-lg-offset-1 col-sm-12"><a className="d-inline-flex" href="javascript:void(0)" onClick={this.backStepForm.bind(this)}><div className="icon icon-back-arrow back_arrow"></div></a></div>
                    </div>
                    <div className="row"><div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div></div>
                    <div className="row  _Common_He_a">
                        <div className="col-lg-10 col-lg-offset-1 col-sm-12 text-left">
                            <h1 className="color">Creating New Participant  â€” Care Requirements</h1>
                        </div>
                    </div>
                    <div className="row"><div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div> </div>

                    <div className="row P_25_T">
                        <div className="col-lg-4 col-lg-offset-1 col-sm-5 textarea_height">
                            <label>Formal Diagnosis (Primary):</label>
                            <span className="required">
                                <textarea className="textarea-max-size" name="formaldiagnosisprimary" onChange={this.handleChange} placeholder="Please type the Formal Diagnosis of this new participant here" value={this.state.formaldiagnosisprimary} data-rule-maxlength="500" data-msg-required="Enter Formal Diagnosis" data-rule-required="true">{this.state['formaldiagnosisprimary']}</textarea>
                            </span>
                        </div>
                        <div className="col-lg-4 col-lg-offset-1 col-sm-6 col-sm-offset-1 textarea_height">
                            <label>Formal Diagnosis (Secondary):</label>
                            <textarea className="textarea-max-size" name="formaldiagnosissecondary" onChange={this.handleChange} placeholder="Please type the Formal Diagnosis of this new participant here  " data-rule-maxlength="500" value={this.state.formaldiagnosissecondary}>{this.state.formaldiagnosissecondary}</textarea>
                        </div>
                    </div>

                    <div className="row P_25_T">
                        <div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                        <div className="col-lg-10 col-lg-offset-1 col-sm-12 P_15_TB"><h3 className="color">Participant Care Focus Points:</h3></div><div className="col-lg-1"></div>
                        <div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                    </div>

                    <div className="row P_25_T">
                        <div className="col-lg-4 col-lg-offset-1 col-sm-5 textarea_height">
                            <label>Other Information About Participant Care: </label>
                            <span className="required">
                                <textarea classname="textarea-max-size" name="participantcareinfo" onChange={this.handleChange} data-rule-required="true" value={this.state.participantcareinfo} data-rule-maxlength="500" placeholder="Please type any other info/notes about the care of this new participant here " data-msg-required="Enter any other info">{this.state['participantcareinfo']}</textarea>
                            </span>
                        </div>
                        <div className="col-lg-4 col-lg-offset-1 col-sm-6 col-sm-offset-1 ">
                            <label className="">Requires Assistance With/Mobility:</label>
                            <span className="required">
        <label htmlFor="assistance[]" className="error CheckieError" style={{display: "block", width: "100%;"}} ></label>
                                <div className="Schedules_Multiple_checkbox overflow-hidden">
                                    <div className="scroll_active_modal">
                                        {this.state.require_assistance.map((req, idx) => (
                                            <span key={idx + 1} className="w_50 w-100 mb-2">
                                                <input type='checkbox' name="assistance[]" className="checkbox1" data-rule-required="true" onChange={(e) => this.setMutipleCheckbox(e, idx, 'require_assistance')} checked={req.active} data-rule-required="true" data-msg-required="Please select at least one Requires Assistance/Mobility" />
                                                <label>
                                                    <div className="d_table-cell"> <span onClick={(e) => this.setMutipleCheckbox(e, idx, 'require_assistance')}></span></div>
                                                    <div className="d_table-cell"> {req.label}</div></label>
                                            </span>
                                        ))}
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>


                    <div className="row P_25_T">
                        <div className="col-lg-1"></div>
                        <div className="col-lg-2 col-sm-2">
                            <label>Cognition:</label>
                            <span className="required">
                                <Select clearable={false} className="custom_select" name="participantCognition" simpleValue={true} required={true} searchable={false} value={this.state['participantCognition']} onChange={(e) => this.selectChange(e, 'participantCognition')}
                                    options={this.state.Cognition_option} placeholder="Please Select" />
                                {this.errorShowInTooltip('participantCognition', 'Select Cognition')}
                            </span>
                        </div>
                        <div className="col-lg-2 col-sm-3">
                            <label>Communication:</label>
                            <span className="required">
                                <Select clearable={false} className="custom_select" name="participantCommunication" simpleValue={true} required={true} searchable={false} value={this.state['participantCommunication']} onChange={(e) => this.selectChange(e, 'participantCommunication')}
                                    options={this.state.Communication_option} placeholder="Please Select" />
                                {this.errorShowInTooltip('participantCommunication', 'Select Communication')}
                            </span>

                        </div>

                        {requireOther ? <div className="col-lg-4 col-lg-offset-1 col-sm-4 col-sm-offset-1">
                            <label>If 'Other' Please Describe:</label>
                            <input required="true" onChange={this.handleChange} type="text" value={this.state['participantReqAssistanceOther']} name="participantReqAssistanceOther" placeholder="Short descriptions separated by commas." />
                        </div> : ''}
                    </div>


                    <div className="row P_25_T">
                        <div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                        <div className="col-lg-10 col-lg-offset-1 col-sm-12 P_15_TB"><h3 className="color">Participant Language(s):</h3></div><div className="col-lg-1"></div>
                        <div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                    </div>


                    <div className="row P_25_T">
                        <div className="col-lg-2 col-lg-offset-1 col-sm-3">
                            <label>English:</label>
                            <span className="required">
                                <Select clearable={false} className="custom_select" name="participantenglish" simpleValue={true} required={true} searchable={false} value={this.state['participantenglish']} onChange={(e) => this.selectChange(e, 'participantenglish')}
                                    options={this.state.English_option} placeholder="Please Select" />
                                {this.errorShowInTooltip('participantenglish', 'Select English')}
                            </span>
                        </div>
                        <div className="col-lg-2 col-sm-3">
                            <label>Preferred Language:</label>
                            <span className="required">
                                <Select clearable={false} className="custom_select" name="participantPreferredlang" simpleValue={true} required={true} searchable={false} value={this.state['participantPreferredlang']} onChange={(e) => this.selectChange(e, 'participantPreferredlang')}
                                    options={prefterLanguageDropdown(0)} placeholder="Please Select" disabled={this.state.participantenglish == 1 ? true : false} />
                                {this.errorShowInTooltip('participantPreferredlang', 'Select Preferred Language')}
                            </span>
                        </div>
                        {(this.state.participantPreferredlang == 11) ?
                            <div className="col-lg-2 col-sm-3">
                                <label>Other Language:</label>
                                <span className="required">
                                    <input type="text" name="preferred_language_other" value={this.state.preferred_language_other} data-rule-required="true" onChange={this.handleChange} />
                                </span>
                            </div> : ''}
                        <div className="col-lg-2 col-sm-3">
                            <label>Linguistic Interpreter:</label>
                            <span>
                                <Select clearable={false} className="custom_select" name="participantLinguistic" simpleValue={true} required={true} searchable={false} value={this.state['participantLinguistic']} onChange={(e) => this.selectChange(e, 'participantLinguistic')}
                                    options={this.state.Linguistic_option} placeholder="Please Select" />

                            </span>
                        </div>
                        <div className="col-lg-3 col-sm-3">
                            <label>Hearing Impaired Interpreter:</label>
                            <div className="col-lg-8 col-sm-12">
                                <div className="row">
                                    <span>
                                        <Select clearable={false} className="custom_select" simpleValue={true} name="participantHearing" required={true} searchable={false} value={this.state['participantHearing']} onChange={(e) => this.selectChange(e, 'participantHearing')}
                                            options={this.state.Hearing_option} placeholder="Please Select" />

                                    </span>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div className="row P_25_T">
                        <div className="col-lg-10 col-lg-offset-1"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                        <div className="col-lg-10 col-lg-offset-1 P_15_TB"><h3 className="color">Participant Shift Requirements:</h3></div><div className="col-lg-1"></div>
                        <div className="col-lg-10 col-lg-offset-1"><div className="bor_T"></div></div><div className="col-sm-1"></div>
                    </div>


                    <div className="row P_25_T">
                        <div className="col-lg-4 col-lg-offset-1 col-sm-5">

                            <label className="">Services Required:</label>

                            <span className="required">
                <label htmlFor="oc_service[]" className="error CheckieError" style={{display: "block", width: "100%;"}} ></label>
                                <div className="multiple_checkbox radi_2 px-0 py-0 overflow-hidden">
                                    <div className="scroll_active_modal" style={{ background: '#fff' }}>
                                        {this.state.os_Services.map((req, idx) => (
                                            <span key={idx + 1} className="w_50 d-inline-block mb-2">
                                                <input type='checkbox' name="oc_service[]" className="checkbox1" onChange={(e) => this.setMutipleCheckbox(e, idx, 'os_Services')} checked={req.active} data-rule-required="true" data-msg-required="Please select at least one Service Required" />
                                                <label>
                                                    <span onClick={(e) => this.setMutipleCheckbox(e, idx, 'os_Services')}></span>{req.label}</label>
                                            </span>
                                        ))}

                                    </div>
                                </div>
                            </span>

                        </div>
                        <div className="col-lg-4 col-lg-offset-1 col-sm-6 col-sm-offset-1 textarea_height">
                            <label>Support Required:</label>
                            <span className="required">
                    <label htmlFor="support[]" className="error CheckieError" style={{display: "block", width: "100%;"}} ></label>
                                <div className="multiple_checkbox radi_2 px-0 py-0 overflow-hidden">
                                    <div className="scroll_active_modal" style={{ background: '#fff' }}>
                                        {this.state.support_required.map((req, idx) => (
                                            <span key={idx + 1} className="w_50 d-inline-block mb-2">
                                                <input type='checkbox' name="support[]" className="checkbox1" onChange={(e) => this.setMutipleCheckbox(e, idx, 'support_required')} checked={req.active} data-rule-required="true" data-msg-required="Please select at least one Support Required" />
                                                <label>
                                                    <span onClick={(e) => this.setMutipleCheckbox(e, idx, 'support_required')}></span>{req.label}</label>
                                            </span>
                                        ))}
                                    </div>
                                </div>
                            </span>

                            {supportOther ? <React.Fragment><label className="P_25_T">If 'Other' Please Describe:</label>
                                <input type="text" required="true" onChange={this.handleChange} name="participantsupportother" value={this.state['participantsupportother']} placeholder="Short descriptions separated by commas." /></React.Fragment> : ''}

                        </div>
                    </div>


                    <div className="row P_25_T">
                        <div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                        <div className="col-lg-10 col-lg-offset-1 col-sm-12 P_15_TB"><h3 className="color">Carers (Members) NOT to Book</h3></div><div className="col-lg-1"></div>
                        <div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                    </div>

                    <span className="required">

                        <div className="row P_25_T">

                        <div className="col-lg-2 col-lg-offset-1 col-md-3">
                                <label><span className="gendar_txt_">Male</span></label><br />
                                <span className="pull-left">
                        <input type="checkbox" className="checkbox2" id="a3" name="all_option_male" onChange={(e) => this.checkCarersNotBookOption(e,'Ethnicity_male_option','Religious_male_option','all_option_male')} checked={this.state['all_option_male'] }  />
                                    <label htmlFor="a3"><span></span>Select all options for Males</label>
                                </span>
                            </div>
                            <div className="col-lg-7 col-md-9">
                                <div className="row">
                                <div className="col-md-5 col-md-offset-1">
                                        <label className="">Ethnicity:</label>
                                        <span className="required">
                                            <label htmlFor="select_ethnicity[]" className="error CheckieError" style={{ display: "block", width: "100%;" }} ></label>
                                            <div className="Schedules_Multiple_checkbox overflow-hidden">
                                                <div className="scroll_active_modal">
                                                    {this.state.Ethnicity_male_option.map((req, idx) => (
                                                        <span key={idx + 1} className="w_50 w-100 mb-2">
                            <input type='checkbox' name="select_ethnicity[]" className="checkbox1" data-rule-required="true" onChange={(e) => this.setMutipleCheckbox(e, idx, 'Ethnicity_male_option','male')} checked={req.active || ''} data-rule-required="true" data-msg-required="Please select at least one Ethnicity" />
                                                            <label>
                            <div className="d_table-cell"> <span onClick={(e) => this.setMutipleCheckbox(e, idx, 'Ethnicity_male_option','male')}></span></div>
                                                                <div className="d_table-cell"> {req.label}</div></label>
                                                        </span>
                                                    ))}
                                                </div>
                                            </div>
                                        </span>
                                    </div>

                                    <div className="col-md-5 col-md-offset-1">
                                        <label className="">Religion Beliefs :</label>
                                        <span className="required">
                                            <label htmlFor="select_religious[]" className="error CheckieError" style={{ display: "block", width: "100%;" }} ></label>
                                            <div className="Schedules_Multiple_checkbox overflow-hidden">
                                                <div className="scroll_active_modal">
                                                    {this.state.Religious_male_option.map((req, idx) => (
                                                        <span key={idx + 1} className="w_50 w-100 mb-2">
                                <input type='checkbox' name="select_religious[]" className="checkbox1" data-rule-required="true" onChange={(e) => this.setMutipleCheckbox(e, idx, 'Religious_male_option','male')} checked={req.active || ''} data-rule-required="true" data-msg-required="Please select at least one Religion Beliefs" />
                                                            <label>
                                <div className="d_table-cell"> <span onClick={(e) => this.setMutipleCheckbox(e, idx, 'Religious_male_option','male')}></span></div>
                                                                <div className="d_table-cell"> {req.label}</div></label>
                                                        </span>
                                                    ))}
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </span>

                    <span className="required">

                        <div className="row P_25_T">

                        <div className="col-lg-2 col-lg-offset-1 col-md-3">
                        <label><span className="gendar_txt_">Female</span></label><br />
                                <span className="pull-left">
                                <input type="checkbox" className="checkbox2" id="a4" name="all_option_female" onChange={(e) => this.checkCarersNotBookOption(e,'Ethnicity_female_option','Religious_female_option','all_option_female')} checked={this.state['all_option_female'] }  />
                                <label htmlFor="a4"><span></span>Select all options for Female</label>
                                </span>

                            </div>

                            <div className="col-lg-7  col-md-9">
                                <div className="row">
                                    <div className="col-md-5  col-md-offset-1">
                                        <label className="">Ethnicity:</label>
                                        <span className="required">
                                <label htmlFor="Ethnicity_female_option[]" className="error CheckieError" style={{display: "block", width: "100%;"}} ></label>
                                            <div className="Schedules_Multiple_checkbox overflow-hidden">
                                                <div className="scroll_active_modal">
                                {this.state.Ethnicity_female_option.map((req, idx) => (
                                                        <span key={idx + 1} className="w_50 w-100 mb-2">
                                    <input type='checkbox' name="Ethnicity_female_option[]" className="checkbox1" onChange={(e) => this.setMutipleCheckbox(e, idx, 'Ethnicity_female_option','female')} checked={req.active || ''}  data-rule-required="true" data-msg-required="Please select at least one Ethnicity" />
                                                            <label>
                                    <div className="d_table-cell"> <span onClick={(e) => this.setMutipleCheckbox(e, idx, 'Ethnicity_female_option','female')}></span></div>
                                                                <div className="d_table-cell"> {req.label}</div></label>
                                                        </span>
                                                    ))}
                                                </div>
                                            </div>
                                        </span>
                                    </div>

                                    <div className="col-md-5 col-md-offset-1">
                                        <label className="">Religion Beliefs :</label>
                                        <span className="required">
                                    <label htmlFor="Religious_female_option[]" className="error CheckieError" style={{display: "block", width: "100%;"}} ></label>
                                            <div className="Schedules_Multiple_checkbox overflow-hidden">
                                                <div className="scroll_active_modal">
                                    {this.state.Religious_female_option.map((req, idx) => (
                                                        <span key={idx + 1} className="w_50 w-100 mb-2">
                                        <input type='checkbox' name="Religious_female_option[]" className="checkbox1" data-rule-required="true" onChange={(e) => this.setMutipleCheckbox(e, idx, 'Religious_female_option','female')} checked={req.active || ''} data-rule-required="true" data-msg-required="Please select at least one Religion Beliefs" />
                                                            <label>
                                        <div className="d_table-cell"> <span onClick={(e) => this.setMutipleCheckbox(e, idx, 'Religious_female_option','female')}></span></div>
                                                                <div className="d_table-cell"> {req.label}</div></label>
                                                        </span>
                                                    ))}
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </span>


                    <div className="row P_25_T">
                        <div className="col-lg-1"></div>
                        <div className="col-lg-8 col-sm-9"></div>
                        <div className="col-lg-2 col-sm-3">
                            <button type="submit" className="but" >Save New Participant</button></div>
                        <div className="col-lg-1"></div>
                    </div>
                </form>


            </BlockUi>
        </div>
        );
    }
}
export default CreateParticipantStepSecond;