import {participaintActiveTitle} from 'menujson/participaint_menu_json';
export const setProfileData = (profileData) => ({
        type: 'set_participant_profile_data',
        profileData
    })


export const setActiveClassProfilePage = (value) => ({
        type: 'set_active_class_profile_page',
        value
    })

export const updateParticipantProfile = (profileData) => ({
        type: 'update_participant_profile_data',
        profileData
    })

export const setActiveSelectPageData= (value) => {
    return {
        type: 'set_active_page_participaint',
        value
}}


export function setActiveSelectPage(request) {
    return (dispatch, getState) => {
        let pageData =participaintActiveTitle;
        let pageType = pageData.hasOwnProperty(request) ? request: 'details';
        let pageTypeTitle = pageData[pageType];
        return dispatch(setActiveSelectPageData({pageType:pageType,pageTitle:pageTypeTitle}))
    }
}

