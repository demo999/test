import React from 'react';
import jQuery from "jquery";
import { postData } from '../../../service/common.js';
import Modal from 'react-bootstrap/lib/Modal'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import { ToastUndo } from 'service/ToastUndo.js';
import { connect } from 'react-redux'
import {updateParticipantProfile} from './actions/ParticipantAction'

class BookingDatePopup extends React.Component {
    constructor(props) {
        super(props); 
        this.state = {   
            booking_date:'',
            booking_status:''
        }
    } 

    componentWillReceiveProps(newProps){
        this.setState(newProps);
    }

    updateBookingStatus = (e) => {
        e.preventDefault();
        jQuery("#booking_form").validate({ /* */});
        if (jQuery("#booking_form").valid()){
            this.setState({loading: true});
            postData('participant/Participant_profile/update_booking_data', this.state).then((result) => {
                if (result.status) {
                    toast.success(<ToastUndo message={'Updated successfully.'} showType={'s'} />, {  
                    //  toast.success("Updated successfully.", {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                        });
                     var NewReduxData = {booking_status:this.state.booking_status,booking_date:this.state.booking_date};
                     this.props.updateProfile(NewReduxData);
                        this.props.cloaseModel();
                } else {
                    this.setState({error: result.error});
                }
                
                this.setState({loading: false});
            });
        } 
    }


    render() {
        return (
        <div>
            <Modal className="modal fade-scale Modal_A  Modal_B" show={this.props.openModel} onHide={this.handleHide} container={this} aria-labelledby="myModalLabel" id="modal_1" tabIndex="-1" role="dialog" >
               <form id="booking_form" method="post" autoComplete="off">
                <Modal.Body>
                    <div className="dis_cell">
                        <div className="text text-left">Update Booking Status:
                            <a data-dismiss="modal" aria-label="Close" className="close_i pull-right mt-1" onClick={()=> this.props.cloaseModel()}><i className="icon icon-cross-icons"></i></a>
                        </div>
                        
                        <div className="row P_15_T">
                            <div className="col-sm-4">
                            <label >Disable future booking</label>
                                    <label className="c-cust-check_1">
                                        <input type="checkbox" name="booking_status" checked={this.state.booking_status} value={this.state.booking_status} id="thing"  onChange={(e)=>this.setState({booking_status: e.target.checked},()=>(
                                            (!this.state.booking_status)?this.setState({booking_date:''}):''
                                        ))} />
                                        <i className="c-cust-check_1__img color"></i>
                                    </label>                       
                            </div>
                           <div className="col-sm-7">
                                <div className="row">
                                    <div className="col-sm-12"><label>Please set an Booking date </label></div>
                                    <div className="col-sm-6">
                                        <DatePicker className="text-center" selected={(this.state.booking_date && this.state.booking_date!='00/00/0000')?moment(this.state.booking_date) :null} name="booking_date" onChange={(e) => this.setState({booking_date : e})} minDate={moment()} required={(this.state.booking_status)? true:false} dateFormat="DD-MM-YYYY"/>
                                    </div>
                                  </div>
                             </div>
                        </div>
                                                 
                        <div className="row mt-4">
                            <div className="col-sm-7"></div>
                            <div className="col-sm-5">
                                <input type="submit" className="but" value={'Save'}  name="content" disabled={this.state.loading} onClick={(e)=>this.updateBookingStatus(e)}/>
                            </div>
                        </div>
                    </div>  
                </Modal.Body>
               </form>
            </Modal>
        </div>
        );
    }
}

const mapStateToProps = state => ({
    booking_status : state.ParticipantReducer.participantProfile.booking_status,
    booking_date : state.ParticipantReducer.participantProfile.booking_date,
    participantId : state.ParticipantReducer.participantProfile.id,
    fullName : state.ParticipantReducer.participantProfile.fullName
})

 const mapDispatchtoProps = (dispach) => {
       return {
           updateProfile: (value) => dispach(updateParticipantProfile(value)),
      }
}            
export default connect(mapStateToProps, mapDispatchtoProps)(BookingDatePopup)