import React, { Component } from 'react';
class DottedLine extends React.Component {
    render() {
        return <div className="row f_color_size py-3">
            <div className="col-lg-3 col-md-3 f align_e_2 col-sm-3 col-xs-4"></div>
            <div className="col-lg-9 col-md-9 f align_e_1 dotted_line col-sm-9 col-xs-8"></div>
        </div>;
    }
}

export default DottedLine