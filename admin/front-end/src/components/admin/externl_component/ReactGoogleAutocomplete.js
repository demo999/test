import React from 'react';
import PropTypes from 'prop-types';

export default class ReactGoogleAutocomplete extends React.Component {
    static propTypes = {
        onPlaceSelected: PropTypes.func,
        types: PropTypes.array,
        componentRestrictions: PropTypes.object,
        bounds: PropTypes.object,
    }

    constructor(props) {
        super(props);
        this.autocomplete = null;
        this.event = null;
    }

    componentDidMount() {
        const {types = ['(cities)'], componentRestrictions, bounds, } = this.props;
        const config = {
            types,
            bounds,
        };

        if (componentRestrictions) {
            config.componentRestrictions = componentRestrictions;
        }

        this.disableAutofill();

        this.autocomplete = new window.google.maps.places.Autocomplete(this.refs.input, config);

        this.event = this.autocomplete.addListener('place_changed', this.onSelected.bind(this));
    }

    disableAutofill() {
        // Autofill workaround adapted from https://stackoverflow.com/questions/29931712/chrome-autofill-covers-autocomplete-for-google-maps-api-v3/49161445#49161445
        if (window.MutationObserver) {
            const observerHack = new MutationObserver(() => {
                observerHack.disconnect();
                if (this.refs && this.refs.input) {
                    this.refs.input.autocomplete = 'disable-autofill';
                }
            });
            observerHack.observe(this.refs.input, {
                attributes: true,
                attributeFilter: ['autocomplete'],
            });
        }
    }

    componentWillUnmount() {
        this.event.remove();
    }

    onSelected() {
        if (this.props.onPlaceSelected) {
             this.props.onPlaceSelected(this.autocomplete.getPlace());
        }
    }

    render() {
        const {onPlaceSelected, types, componentRestrictions, bounds, ...rest} = this.props;

        return (
                <input
                    ref="input"
                    {...rest}
                    />
                );
    }
}