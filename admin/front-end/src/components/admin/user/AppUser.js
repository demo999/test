import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';


import PageNotFound from '../../admin/PageNotFound';

import { checkItsNotLoggedIn, getPermission, checkPinVerified } from '../../../service/common.js';
import { ROUTER_PATH } from '../../../config.js';

import Dashboard from './Dashboard';
import CreateUser from './CreateUser';
import ListUsers from './ListUsers';
import ListingLoges from './ListingLoges';

import ListingAprroval from './approval/ListingAprroval';
import SingleApproval from './approval/SingleApproval';
import { setFooterColor } from '../../admin/notification/actions/NotificationAction.js';
import Sidebar from '../Sidebar';
import { connect } from 'react-redux';
import {adminJson} from 'menujson/admin_menu_json';

const menuJson = () => {
    let menu = adminJson;
    return menu;
}


class AppUser extends Component {
    constructor(props) {
        super(props);
        this.permissions = (getPermission() == undefined) ? [] : JSON.parse(getPermission());
        checkPinVerified('admin');

        checkItsNotLoggedIn(ROUTER_PATH);
        this.props.setFooterColor('');

        this.state = {
            subMenuShowStatus:false,
            menus:menuJson(),
            replaceData:{':id':0}
          }
    }

    permissionRediect() {
        checkItsNotLoggedIn();
        return <Redirect to={'/admin/no_access'} />;
    }
    
    componentWillUnmount(){
        this.props.setFooterColor('');
    }
    getObjectValue(obj){
        let menuState = obj.state.menus;
        let objApproval = menuState.find(x => x.id === 'approval');
        let objUserManageMent = menuState.find(x => x.id === 'user_management');
        let objApprovalIndex = menuState.indexOf(objApproval);
        let objUserManageMentIndex = menuState.indexOf(objUserManageMent);
        let objIndexUsrMangeement = '';
        let objIndexApproval = '';
        let managmentLinkHide = '-1';
        let approvalLinkHide = '-1';
        if(objUserManageMentIndex > -1){
            objIndexUsrMangeement = menuState[objUserManageMentIndex]['submenus'].find(x => x.id === 'user_management_id');
           managmentLinkHide = menuState[objUserManageMentIndex]['submenus'].indexOf(objIndexUsrMangeement);
            
       }
       if(objApprovalIndex > -1){
        objIndexApproval = menuState[objApprovalIndex]['submenus'].find(x => x.id === 'approval_id');
       approvalLinkHide = menuState[objApprovalIndex]['submenus'].indexOf(objIndexApproval);
        
         }
         return {approvalIndex:objApprovalIndex,approvalLinkHide:approvalLinkHide,userManageMentIndex:objUserManageMentIndex,userManagmentLinkHide:managmentLinkHide};
    }
    componentWillReceiveProps(nextProps){
        if(this.state.subMenuShowStatus!= nextProps.getSidebarMenuShow.subMenuShow) {
            this.setState({subMenuShowStatus:nextProps.getSidebarMenuShow.subMenuShow},()=>{
                let menuState = this.state.menus;
                let dataIndex = this.getObjectValue(this);
                //console.log('sdsd',dataIndex,nextProps);
               /*  let menuState = this.state.menus;
                let objApproval = menuState.find(x => x.id === 'approval');
                let objUserManageMent = menuState.find(x => x.id === 'user_management');
                let objApprovalIndex = menuState.indexOf(objApproval);
                let objUserManageMentIndex = menuState.indexOf(objUserManageMent);
                let objIndexUsrMangeement = '';
                let objIndexApproval = '';
                let managmentLinkHide = '-1';
                let approvalLinkHide = '-1';
                if(objUserManageMentIndex > -1){
                    objIndexUsrMangeement = menuState[objUserManageMentIndex]['submenus'].find(x => x.id === 'user_management_id');
                   managmentLinkHide = menuState[objUserManageMentIndex]['submenus'].indexOf(objIndexUsrMangeement);
                    
               }
               if(objApprovalIndex > -1){
                    objIndexApproval = menuState[objApprovalIndex]['submenus'].find(x => x.id === 'approval_id');
                   approvalLinkHide = menuState[objApprovalIndex]['submenus'].indexOf(objIndexApproval);
                    
               } */
                             
                if(nextProps.getSidebarMenuShow.subMenuShow){
                    
                    if(nextProps.showTypePage=='single_approval'){
                        menuState[dataIndex.approvalIndex]['submenus'][dataIndex.approvalLinkHide]['linkOnlyHide']=false;
                        menuState[dataIndex.userManageMentIndex]['submenus'][dataIndex.userManagmentLinkHide]['linkOnlyHide']=true;
                    }
                    if(nextProps.showTypePage=='single_user_management'){
                        menuState[dataIndex.userManageMentIndex]['submenus'][dataIndex.userManagmentLinkHide]['linkOnlyHide']=false;
                        menuState[dataIndex.approvalIndex]['submenus'][dataIndex.approvalLinkHide]['linkOnlyHide']=true;
                    }
                }else{
                    menuState[dataIndex.userManageMentIndex]['submenus'][dataIndex.userManagmentLinkHide]['linkOnlyHide']=true;
                    menuState[dataIndex.approvalIndex]['submenus'][dataIndex.approvalLinkHide]['linkOnlyHide']=true;
                }
                this.setState({menus:menuState});
            });
        }

        if(this.props.approvalData.id !=nextProps.approvalData.id || this.props.userData.id !=nextProps.userData.id){
            this.setState({replaceData:{':id':nextProps.approvalData.id,':AdminId':nextProps.userData.id}},()=>{
                let menuState = this.state.menus;
                let dataIndex = this.getObjectValue(this);
                if(nextProps.showTypePage=='single_approval'){
                    menuState[dataIndex.approvalIndex]['submenus'][dataIndex.approvalLinkHide]['name']='ID-'+nextProps.approvalData.id;
                }
                if(nextProps.showTypePage=='single_user_management'){
                    menuState[dataIndex.userManageMentIndex]['submenus'][dataIndex.userManagmentLinkHide]['name']='Update - '+nextProps.userData.firstname+' '+nextProps.userData.lastname;
                }
                this.setState({menus:menuState});
            });
        }
    }

    render() {
        return (
            <section className='asideSect__ Blue' style={{background:'none'}}>
            <Sidebar
              heading={'Admin'}
              menus={this.state.menus}
              subMenuShowStatus={this.state.subMenuShowStatus}
              replacePropsData={this.state.replaceData}
            />
                    <Switch >
                        <Route exact path={ROUTER_PATH + 'admin/user/dashboard'} render={(props) => this.permissions.access_admin ? <Dashboard props={props} /> : this.permissionRediect()} />
                        <Route exact path={ROUTER_PATH + 'admin/user/create'} render={(props) => this.permissions.create_admin ? <CreateUser props={props} /> : this.permissionRediect()} />
                        <Route exact path={ROUTER_PATH + 'admin/user/update/:AdminId/:page?'} render={(props) => this.permissions.update_admin ? <CreateUser props={props} /> : this.permissionRediect()} />
                        <Route exact path={ROUTER_PATH + 'admin/user/list'} render={(props) => this.permissions.access_admin ? <ListUsers props={props} /> : this.permissionRediect()} />
                        <Route exact path={ROUTER_PATH + 'admin/user/logs'} render={(props) => this.permissions.access_admin ? <ListingLoges props={props} /> : this.permissionRediect()} />
                        <Route exact path={ROUTER_PATH + 'admin/user/logs/:module'} render={(props) => this.permissions.access_admin ? <ListingLoges props={props} /> : this.permissionRediect()} />
                        <Route exact path={ROUTER_PATH + 'admin/user/approval'} render={(props) => this.permissions.access_admin ? <ListingAprroval props={props} /> : this.permissionRediect()} />
                        <Route exact path={ROUTER_PATH + 'admin/user/approval/:id/:page?'} render={(props) => this.permissions.access_admin ? <SingleApproval props={props} /> : this.permissionRediect()} />

                        <Route path='/admin/user/' component={PageNotFound} />
                    </Switch>
                   
                </section>
        );
    }
}

const mapStateToProps = state => ({
    permissions: state.Permission.AllPermission,
    getSidebarMenuShow: state.sidebarData,
    showTypePage: state.UserDetailsData.activePage.pageType,
    approvalData:state.UserDetailsData.approval_details,
    userData:state.UserDetailsData.user_details,

})

const mapDispatchtoProps = (dispach) => {
    return {
        setFooterColor: (result) => dispach(setFooterColor(result))
        
    }
};

const AppUserData = connect(mapStateToProps, mapDispatchtoProps)(AppUser)
export {AppUserData as AppUser};

