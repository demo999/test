import React, { Component } from 'react';
import ReactTable from "react-table";
import 'react-toastify/dist/ReactToastify.css';
import DatePicker from 'react-datepicker';
import { checkItsNotLoggedIn, postData, handleDateChangeRaw, reFreashReactTable } from '../../../../service/common.js';
import { ROUTER_PATH, PAGINATION_SHOW } from '../../../../config.js';
import { Link } from 'react-router-dom';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import moment from 'moment';
import { connect } from 'react-redux';
import {setSubmenuShow} from 'components/admin/actions/SidebarAction';
import Pagination from "../../../../service/Pagination.js";

// globale varibale to stote data

const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {

        // request json
        var Request = JSON.stringify({ pageSize: pageSize, page: page, sorted: sorted, filtered: filtered });

        postData('admin/Approval/get_list_approval', Request).then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.count)
            };
            resolve(res);
        });

    });
};

/*
 * class ListUsers 
 */
class ListUsers extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn(ROUTER_PATH);
        ////checkPinVerified(); 

        this.state = {
            loading: false,
            userList: [],
            counter: 0,
            startDate: new Date(),
            search: '',
            search_by: ''
        };

        this.reactTable = React.createRef();
    }

    searchTable = (key, search_by) => {
        var state = {};
        state[key] = search_by;
        this.setState(state, () => {
            this.setState({ filtered: state });
        });
    }

    addToPin = (approvalId, status) => {
        postData('admin/Approval/add_to_pin', { approvalId: approvalId, status: status }).then((result) => {
            if (result.status) {
                reFreashReactTable(this, 'fetchData');
            }
        });
    }

    submitSearch = (e) => {
        e.preventDefault();
        // reflec value of search box
        this.searchTable('search', this.state.search);
    };

    fetchData = (state, instance) => {
        // function for fetch data from database
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
            this.setState({
                userList: res.rows,
                pages: res.pages,
                loading: false
            });
        });
    }

    componentDidMount() {
        this.props.setSubmenuShow(0);
    }


    render() {
        const columns = [{ Header: 'ID', accessor: 'id', filterable: false, },
        { Header: 'User ID', accessor: 'userId', filterable: false, },
        { Header: 'Date', accessor: 'created', filterable: false, Cell: props => <span>{moment(props.value).format("DD/MM/YYYY")}</span> },
        { Header: 'Time', accessor: 'created', filterable: false, Cell: props => <span>{moment(props.value).format("LT")}</span> },
        { Header: 'Source', accessor: 'user_type', filterable: false, Cell: props => <span>{(props.value == 2) ? 'My ONCALL' : 'ONCALL Go'}</span> },
        { Header: 'Description', accessor: 'approval_area', filterable: false, sortable: false },
        {
            Header: 'Action',
            accessor: 'id',
            sortable: false,
            filterable: false,
            Cell: props =>
                <div className="flex">
                    <span className={"dsBlck " + ((props.original.pin == 1) ? 'active' : '')} onClick={() => this.addToPin(props.value, props.original.pin)} ><i className='icon icon-pin pin_ic1'></i></span>
                    <Link className="dsBlck" to={'/admin/user/approval/' + props.value} ><i className='icon icon-views'></i></Link>
                </div>
        },
        ]


        return (
            <div>

                <section className="manage_top">
                    <div className="container-fluid Blue">
                        <div className="row  _Common_back_a">
                                <div className="col-lg-10 col-lg-offset-1 col-md-12">
                                    <Link className="d-inline-flex" to={'/admin/user/dashboard'}><div className="icon icon-back-arrow back_arrow"></div></Link>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bt-1"></div>
                                </div>
                            </div>
                            <div className="row _Common_He_a">
                                <div className="col-lg-8 col-lg-offset-1  col-md-12">
                                     <h1 className="color">Approvals</h1>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bt-1"></div>
                                </div>
                            </div>

                        <div>
                            <form onSubmit={this.submitSearch}>
                            <div className="row _Common_He_a">
                                <div className="col-lg-5 col-lg-offset-1 col-sm-6 search_bar">
                                    <div className="input_search change_b">
                                        <input type="search" id="searchRoles" onChange={(e) => this.setState({ search: e.target.value })} className="form-control" placeholder="Search" />
                                        <button type="submit">
                                            <span className="icon icon-search"></span>
                                        </button>
                                    </div>
                                </div>
                                <div className="col-lg-1  col-sm-2 col-sm-offset-1 col-lg-offset-2">
                                    <DatePicker onChangeRaw={handleDateChangeRaw} utcOffset={0} isClearable={true} name="on_date" onChange={(e) => this.searchTable('on_date', e)} selected={this.state.on_date ? moment(this.state.on_date, 'DD-MM-YYYY') : null} dateFormat="DD-MM-YYYY" className="text-center px-0" placeholderText="00/00/0000" />

                                </div>
                                <div className="col-lg-2 col-sm-3 clearable-show">
                                    <Select className="wide" clearable={false} simpleValue={true} searchable={false} options={this.filterType} value={this.state.search_by || ''} placeholder="Filter by type" onChange={(e) => this.searchTable('search_by', e)} />
                                </div>
                                </div>
                            </form>
                            <div className="row">
                            <div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bb-1"></div></div>
                            </div>
                        </div>

                        <div className="row mt-3">
                            <div className="col-lg-10 col-lg-offset-1 col-md-12 listing_table PL_site">
                                <ReactTable
                                
PaginationComponent={Pagination}
                                    columns={columns}
                                    manual
                                    ref={this.reactTable}
                                    data={this.state.userList}
                                    pages={this.state.pages}
                                    loading={this.state.loading}
                                    onFetchData={this.fetchData}
                                    filtered={this.state.filtered}
                                    defaultPageSize={10}
                                    noDataText="No roles"
                                    className="-striped -highlight"

                                    minRows={2}
                                    previousText={<span className="icon icon-arrow-left privious"></span>}
nextText={<span className="icon icon-arrow-right next"></span>}
                                    showPagination={this.state.userList.length >= PAGINATION_SHOW ? true : false}
                                />
                            </div>
                        </div>

                    </div>

                </section>

            </div>
        );
    }
}
const mapStateToProps = state => ({
    showTypePage: state.UserDetailsData.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {
        setSubmenuShow: (result) => dispach(setSubmenuShow(result))
    }
}
export default connect(mapStateToProps, mapDispatchtoProps)(ListUsers);


