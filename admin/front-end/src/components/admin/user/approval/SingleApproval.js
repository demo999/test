import React, { Component } from 'react';
import ReactTable from "react-table";
import 'react-toastify/dist/ReactToastify.css';
import { checkItsNotLoggedIn, postData } from '../../../../service/common.js';
import { ROUTER_PATH } from '../../../../config.js';
import { Link } from 'react-router-dom';
import 'react-select-plus/dist/react-select-plus.css';
import { DynamicComponet } from './DynamicMappingConstant';
import { toast } from 'react-toastify';
import { connect } from 'react-redux';
import {setApprovalDetails, setActiveSelectPage} from 'components/admin/user/actions/UserAction';
import {setSubmenuShow} from 'components/admin/actions/SidebarAction';
import { ToastUndo } from 'service/ToastUndo.js'

class SingleApproval extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn(ROUTER_PATH);


        this.state = {
            loading: false,
            approval_data: [],
        };
    }

    confirmApprova = () => {
        postData(this.state.submit_uri, this.state).then((result) => {
            if (result.status) {
                toast.success(<ToastUndo message={'Updated successfully'} showType={'s'} />, {   
                // toast.success("Updated successfully", {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
                this.getApprovalData();
            }
        });
    }

    getApprovalData = () => {
        postData('admin/Approval/get_approval_data', { aprrovalId: this.props.props.match.params.id }).then((result) => {
            if (result.status) {
                this.setState(result.data);
                this.props.setApprovalDetails(result.data);
            }
        });
    }

    componentDidMount() {
        this.props.setSubmenuShow(1);
        this.getPageSelect();
        this.getApprovalData();
    }

    approveDeny = (e, index, value) => {
        e.preventDefault();
        if (this.state.status == 0) {
            var List = this.state.approval_data
            List[index]['approve'] = value;
            this.setState(List);
            return true;
        }
    }

    getPageSelect(){
        this.props.setActivePage('single_approval');
      }

    componentDidUpdate(){
        if(this.props.showTypePage!='single_approval'){
              this.getPageSelect();
              
        }
    }



    render() {
        const columns = [{ Header: 'Description', accessor: 'description', filterable: false, },
        { Header: 'Area', accessor: 'area', filterable: false },
        { Header: 'Field', accessor: 'field', filterable: false },
        {
            Header: '',
            accessor: 'id',
            sortable: false,
            filterable: false,
            expander: true, sortable: false,
            Expander: ({ isExpanded, ...rest }) =>
                <div>{isExpanded ? <i className="icon icon-arrow-up"></i> : <i className="icon icon-arrow-down"></i>}</div>,
            headerStyle: { border: "0px solid #fff" }

        }
        ]


        return (
            <div>

                <section className="manage_top">
                    <div className="container-fluid Blue">
    
                        <div className="row  _Common_back_a">
                            <div className="col-lg-10 col-lg-offset-1 col-md-12">
                                <Link className="d-inline-flex" to={'/admin/user/approval'}><div className="icon icon-back-arrow back_arrow"></div></Link>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bt-1"></div>
                            </div>
                        </div>
                        <div className="row _Common_He_a">
                            <div className="col-lg-8 col-lg-offset-1  col-md-12">
                                <h1 className="color">Approvals</h1>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bt-1"></div>
                            </div>
                        </div>

                        <div className="row py-4">
                            <div className="col-lg-10 col-lg-offset-1">
                            <ul className="user_info ">
                                <li>User Id: <span>{this.state.userId}</span></li>
                                <li> User Name: <span>{this.state.username}</span></li>
                                <li> Source:  <span>{this.state.user_type == 2 ? 'My ONCALL' : 'ONCALL Go'} </span></li>
                                <li> Description: <span>{this.state.approval_area} </span></li>
                                <li> Status: <span>{this.state.status == 0 ? 'Pending' : ((this.state.status == 1) ? 'Approved' : "Denied")} </span></li>
                            </ul>
                           </div>
                           <div className="col-lg-10 col-lg-offset-1 col-sm-12 P_15_T"><div className="bb-1"></div></div>
                        
                        </div>

                        <div className="row">
                            <div className="col-lg-10 col-lg-offset-1 col-md-12 schedule_listings rc-listing aprvl_rcTable11">
                                <ReactTable
                                    columns={columns}
                                    manual
                                    data={this.state.approval_data}
                                    pages={this.state.pages}
                                    loading={this.state.loading}
                                    defaultPageSize={10}
                                    className="-striped -highlight"
                                    showPagination={false}
                                    collapseOnDataChange={false}
                                    minRows={2}
                                    SubComponent={(props) => {
                                        var MyComponent = DynamicComponet[props.original.subcomponet];
                                        return <MyComponent props={props.original} action={this.approveDeny} index={props.index} />
                                    }

                                    }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className='col-lg-10 col-lg-offset-1 col-sm-12'>
                                {(this.state.status == 0) ? <button onClick={this.confirmApprova} className="but cnfrm_btn pull-right">Confirm</button> : ''}
                            </div>
                        </div>
                    </div>

                </section>

            </div>
        );
    }
}
const mapStateToProps = state => ({
    showTypePage: state.UserDetailsData.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {
        setSubmenuShow: (result) => dispach(setSubmenuShow(result)),
        setActivePage:(result) => dispach(setActiveSelectPage(result)),
        setApprovalDetails:(result) => dispach(setApprovalDetails(result)),
    }
}
export default connect(mapStateToProps, mapDispatchtoProps)(SingleApproval);



