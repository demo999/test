import React, { Component } from 'react';
import jQuery from "jquery";

import { checkItsNotLoggedIn } from '../../../service/common.js';
import { ROUTER_PATH, BASE_URL } from '../../../config.js';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import 'react-dual-listbox/lib/react-dual-listbox.css';
import DualListBox from 'react-dual-listbox';


class AddRoles extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn(ROUTER_PATH);

        this.state = {
            role_id: '',
            role_name: '',
            role_error: '',
            loading: false,
        };


        this.handleChange = this.handleChange.bind(this); // this function use for reflected change in input field
        this.onSubmit = this.onSubmit.bind(this) // this function use for edit and add new role in system mean submit data
        this.resetState = this.resetState.bind(this); // this function use for reset state
        this.onChange = this.onChange.bind(this);
    }
    onChange(selectedValues) {
        // handle selected values here
    }
    resetState(e) {
        // this function only for reset update
        this.setState({ loading: false, role_id: '', role_name: '', role_error: '' });
    }

    handleChange(e) {
        var state = {};
        this.setState({ role_error: '' });
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    onSubmit(e) {
        e.preventDefault();

        if (this.state.role_name) {
            jQuery.ajax({
                url: BASE_URL + 'admin/Dashboard/add_role',
                type: 'POST',
                dataType: 'JSON',
                data: { 'role_name': this.state.role_name, 'role_id': this.state.role_id },
                beforeSend: function () {
                    this.setState({ loading: true });
                }.bind(this),
                success: function (data) {
                    if (data.status) {
                        this.resetState();
                        this.refreashReactTable();

                    } else {
                        this.setState({ 'role_error': data.error });
                    }
                    this.setState({ loading: false });
                }.bind(this),
                error: function (xhr) {
                    this.setState({ loading: false });
                }.bind(this)
            });
        } else {
            this.setState({ 'role_error': 'Please enter role name' });
        }
    }

    componentDidMount() {

    }

    render() {
        const options = [
            { value: 'luna', label: 'Moon' },
            { value: 'phobos', label: 'Phobos' },
            { value: 'deimos', label: 'Deimos' },
            { value: 'io', label: 'Io' },
            { value: 'europa', label: 'Europa' },
            { value: 'ganymede', label: 'Ganymede' },
            { value: 'callisto', label: 'Callisto' },
            { value: 'mimas', label: 'Mimas' },
            { value: 'enceladus', label: 'Enceladus' },
            { value: 'tethys', label: 'Tethys' },
            { value: 'rhea', label: 'Rhea' },
            { value: 'titan', label: 'Titan' },
            { value: 'iapetus', label: 'Iapetus' },
        ];

        const selected = [1, 2]

        return (
            <div>
              
                <section className="manage_top">
                    <div className="container-fluid">
    
                        <div className="row  _Common_back_a">
                            <div className="col-lg-10 col-lg-offset-1 col-md-12">
                            <Link to={ROUTER_PATH + 'admin/user/roles/list'}><div className="icon icon-back-arrow back_arrow"></div></Link>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-10 col-lg-offset-1 col-md-12"><div className="bt-1"></div>
                            </div>
                        </div>
                        <div className="row   row _Common_He_a">
                            <div className="col-lg-8 col-lg-offset-1  col-md-12">
                                <h1 className="color">Add Roles</h1>
                            </div>
                        </div>


                        <DualListBox options={options} onChange={this.onChange} />
                        <div className="row py-4">
                            <form>
                                <input type="text" name="role_name" placeholder="Role name" onChange={this.handleChange} value={this.state.role_name} />

                                <label id="role_name-error" className="error">{this.state.role_error}</label>


                                <a href="javascript:void(0)" onClick={this.onSubmit} className="but">{this.state.role_id > 0 ? 'Upadate' : 'Add'} role</a>
                                <div className="col-lg-10 col-lg-offset-1 col-sm-12 P_15_T"><div className="bb-1"></div></div>
                            </form>

                        </div>
                    </div>
                </section>
              
            </div>
        );
    }
}
export default AddRoles;
