import React from 'react';
import { ROUTER_PATH } from '../../../config.js';
import { checkItsNotLoggedIn} from '../../../service/common.js';
// import OrganisationNavigation from './OrganisationNavigation';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import {subOrgViewBy} from '../../../dropdown/Orgdropdown.js';
import OrganisationBookingDatePopup from './OrganisationBookingDatePopup';
import OrganisationUpdatePopUp from './OrganisationUpdatePopUp';
import moment from 'moment';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { customProfile } from '../../../service/CustomContentLoader.js';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux'
import { setActiveSelectPage } from './actions/OrganisationAction.js'

class DottedLine extends React.Component{
    render(){
        return <div className="row f_color_size py-3">
                                   <div className="col-lg-3 col-md-3 f align_e_2 col-xs-3 col-xs-3"></div>
                                   <div className="col-lg-9 col-md-9 f align_e_1 col-xs-9 dotted_line col-xs-9"></div>
                                </div>;
    }
}
//
class OrganisationOverview extends React.Component {
    constructor(props) {
    super(props);
        checkItsNotLoggedIn(ROUTER_PATH);
        var all=[];
        this.state = {
            sub_org_list:[],
            view_by_status:1,
            modal_show:false,
            org_update_modal:false,
        };
    }
    
    componentDidMount(){
        this.props.setActiveSelectPage('org_overview');
    }

    closeBookingPopUp = (param) => {
        this.setState({modal_show : false});
    }

    closeOrgUpdatePopUp=(param)=>{
        this.setState({org_update_modal:false},()=>{
            if(param){
                this.props.getOrgProfile(this.props);
            }
            
        });
    }

    render() {  
     
     return (
            <div>             
                <div className="row">
                <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                <div className="row">
                        <div className="col-sm-12"><div className="bor_T"></div></div>
                    </div>


                    <div className="tab-content">
                        <div role="tabpanel" className="tab-pane active" id="orgOverView">
                        <ReactPlaceholder showLoadingAnimation type='textRow' customPlaceholder={customProfile} ready={!this.state.loading}>
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="P_7_TB"><h3>About '{this.props.OrganisationProfile.name}':</h3></div>
                                </div>
                                <div className="col-md-12"><div className="bor_T"></div></div>
                            </div>
                            <div className="px-3 mt-4">
                            <div className="row f_color_size">
                                <div className="col-md-3 f align_e_2 col-xs-3">Name: </div>
                                <div className="col-md-9 f align_e_1 col-xs-9">{this.props.OrganisationProfile.name}</div>
                            </div>
                            <DottedLine/>

                            <div className="row f_color_size">
                                <div className="col-md-3 f align_e_2 col-xs-3">Address: </div>
                                <div className="col-md-9 f align_e_1 col-xs-9">{this.props.OrganisationProfile.primary_address || 'N/A'}</div>
                            </div>
                            <DottedLine/>

                            { (this.props.OrganisationProfile.OrganisationPh)? <span>                             
                                {this.props.OrganisationProfile.OrganisationPh.map((value, idx) => (
                                    <div className="row f_color_size" key={idx}>
                                        <div className="col-md-3 f align_e_2 col-xs-3">Office Phone ({(value.primary_phone) && value.primary_phone == 1?'Primary':'Secondary'}):</div>
                                        <div className="col-md-9 f align_e_1 col-xs-9 "><u>{value.phone.indexOf('+') == -1 ? '+':''}{value.phone}</u></div>
                                    </div>
                                ))}  </span> : ''
                            }

                            {(this.props.OrganisationProfile.OrganisationPh) && this.props.OrganisationProfile.OrganisationPh.length > 0?<DottedLine/>:''}

                            { (this.props.OrganisationProfile.OrganisationEmail)? <span>                             
                                {this.props.OrganisationProfile.OrganisationEmail.map((value, idx) => (
                                    <div className="row f_color_size" key={idx}>
                                        <div className="col-md-3 f align_e_2 col-xs-3">Office Email ({(value.primary_email) && value.primary_email == 1?'Primary':'Secondary'}):</div>
                                        <div className="col-md-9 f align_e_1 col-xs-9">{value.email}</div>
                                    </div>
                                ))}  </span> : ''
                            }

                           {(this.props.OrganisationProfile.OrganisationEmail && this.props.OrganisationProfile.OrganisationEmail.length > 0)?<DottedLine/>:''}

                            <div className="row f_color_size">
                                <div className="col-md-3 f align_e_2 col-xs-3">ABN:</div>
                                <div className="col-md-9 f align_e_1 col-xs-9">{this.props.OrganisationProfile.abn}</div>
                            </div>

                            <DottedLine/>
                            <div className="row f_color_size">
                                <div className="col-md-3 f align_e_2 col-xs-3">Payroll Tax: </div> 
                                <div className="col-md-9 f align_e_1 col-xs-9">{ (this.props.OrganisationProfile.payroll_tax && this.props.OrganisationProfile.payroll_tax == '0')?'No':(this.props.OrganisationProfile.payroll_tax && this.props.OrganisationProfile.payroll_tax == '1')?'Yes':'N/A'}</div>
                            </div>
                             <DottedLine/>
                        
                            <div className="row f_color_size">
                                <div className="col-md-3 f align_e_2 col-xs-3">GST: </div>
                                <div className="col-md-9 f align_e_1 col-xs-9">{(this.props.OrganisationProfile.gst && this.props.OrganisationProfile.gst == '1')?'Yes':'No'}</div>
                            </div>
                             <DottedLine/>

                            <div className="row f_color_size">
                                <div className="col-md-3 f align_e_2 col-xs-3">Booking Status: </div>
                                <div className="col-md-9 f align_e_1 col-xs-9">{(this.props.OrganisationProfile.booking_status)?'Do not allow future bookings from -':'Open'}  {(this.props.OrganisationProfile.booking_date && this.props.OrganisationProfile.booking_date!='00-00-0000')?moment(this.props.OrganisationProfile.booking_date).format('DD/MM/YYYY'):''}
                                </div>
                                

                                <div className="row text-left mt-2">
                                    <a><i className="icon icon-update update_button" onClick={() => this.setState({modal_show:true})}></i></a>    
                                </div>
                            </div>
                           
                           {/*<OrganisationBookingDatePopup 
                                orgId={this.props.props.match.params.id} 
                                selectedData={{'booking_status':this.props.OrganisationProfile.booking_status,'booking_date':this.props.OrganisationProfile.booking_date}} 
                                modal_show={this.state.modal_show} 
                                closeBookingPopUp={this.closeBookingPopUp} 
                            />*/}

                        </div>
                            <div className="row P_15_T">
                                <div className="col-lg-3 col-sm-4 pull-right">
                                    <a className="but" onClick={() => this.setState({org_update_modal:true})}>Update Org Details</a>
                                </div>
                            </div>

                            <OrganisationUpdatePopUp 
                             orgId={this.props.props.match.params.id}
                             org_update_modal={this.state.org_update_modal}
                             pageTitle= {'Update '+ "'"+ this.props.OrganisationProfile.name+"'"}
                             closeOrgUpdatePopUp={this.closeOrgUpdatePopUp}
                            />
                            </ReactPlaceholder>
                        </div>

                        <div role="tabpanel" className="tab-pane" id="subOrgList">
                            <div className="row">
                               <div className="col-lg-9 col-md-8">
                                    <div className="P_7_TB"><h3>Sub-Orgs Belonging to '{this.props.orgName}':</h3></div>
                                    <div className="bor_T"></div>
                                </div>

                                <div className="col-lg-3 col-md-4">
                                    <div className="box">
                                         <Select name="view_by_status" required={true} simpleValue={true} searchable={false} clearable={false} options={subOrgViewBy()} placeholder="View By" value={this.state.view_by_status} onChange={(e)=> this.viewByselectChange(e,'view_by_status')}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>            
            </div>
        );
    }
}

const mapStateToProps = state => ({
    //ActiveClass : state.OrganisationReducer.ActiveClassProfilePage,
    OrganisationProfile : state.OrganisationReducer.orgProfile
})

 const mapDispatchtoProps = (dispach) => {
       return {
           setActiveSelectPage: (value) => dispach(setActiveSelectPage(value)),
      }
}
                
export default connect(mapStateToProps, mapDispatchtoProps)(OrganisationOverview)
