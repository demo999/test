import React from 'react';
import {Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PinModal from '../PinModal';
import {checkPin,checkLoginModule} from '../../../service/common.js';

class OrganisationNavigation extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
        	pinModalOpen:false
        };
    }
    closeModal=()=>
    {
    	this.setState({pinModalOpen:false})
    }
    

    render() {	
        const fmsUrl = (
        (this.props.InnerMenu && this.props.InnerMenu == '0') ? 
            '/admin/organisation/fms/'+this.props.organisationId : 
            ((this.props.InnerMenu && this.props.InnerMenu == '1') ? 
                '/admin/organisation/fms/'+this.props.organisationId+'/'+this.props.subOrgId : 
                ((this.props.InnerMenu && this.props.InnerMenu == '2') ?
                    '/admin/organisation/house_fms/'+this.props.OrganisationProfile.ocs_id+'/'+this.props.OrganisationProfile.orgId :
                    '/admin/organisation/dashboard'
                )
            )
        );
        const fmsHtml = (
            checkPin('fms') ? 
            <Link to={fmsUrl} className={(this.props.Active) && this.props.Active == 'fms'?'active':''}>FMS</Link> : 
            <a onClick={(props) => checkLoginModule(this,'fms',fmsUrl)} className={(this.props.Active) && this.props.Active == 'fms'?'active':''}>FMS</a>
        );
       return (
          <div>
                    <aside className="col-lg-2 col-sm-3 col-lg-offset-1">
                        {
                            (this.props.InnerMenu && this.props.InnerMenu == '0')?
                            <ul className="side_menu">
                                <li><Link to={'/admin/organisation/overview/'+this.props.organisationId} className={(this.props.Active) && this.props.Active == 'about'?'active':''} >About</Link ></li>
                                <li><Link to={'/admin/organisation/contacts/'+this.props.organisationId} className={(this.props.Active) && this.props.Active == 'contacts'?'active':''}>Contacts</Link ></li>
                                <li>{fmsHtml}</li>
                            </ul>                      
                            : 
                            (this.props.InnerMenu && this.props.InnerMenu == '1')?
                                <ul className="side_menu">
                                    <li><Link to={'/admin/organisation/overview/'+this.props.organisationId+'/'+this.props.subOrgId} className={(this.props.Active) && this.props.Active == 'about'?'active':''}>About</Link></li>
                                    <li><Link to={'/admin/organisation/contacts/'+this.props.organisationId+'/'+this.props.subOrgId} className={(this.props.Active) && this.props.Active == 'contacts'?'active':''}>Contacts</Link></li>
                                    <li>{fmsHtml}</li>
                                </ul>
                            :
                            (this.props.InnerMenu && this.props.InnerMenu == '2')?
                            <ul className="side_menu">
                                <li><Link to={'/admin/organisation/house_about/'+this.props.OrganisationProfile.ocs_id+'/'+this.props.OrganisationProfile.orgId} className={(this.props.Active) && this.props.Active == 'about'?'active':''}>About</Link></li>
                                <li><Link to={'/admin/organisation/contactBilling/'+this.props.OrganisationProfile.ocs_id+'/'+this.props.OrganisationProfile.orgId} className={(this.props.Active) && this.props.Active == 'bill'?'active':''}>Contact/Billing</Link></li>
                                <li><Link to={'/admin/organisation/docs/'+this.props.OrganisationProfile.ocs_id+'/'+this.props.OrganisationProfile.orgId} className={(this.props.Active) && this.props.Active == 'docs'?'active':''}>Docs</Link></li>
                                <li>{fmsHtml}</li>
                            </ul>
                            :''
                        }                            
                    </aside>
                <PinModal 
					color={this.state.color}  
					pinType={this.state.pinType}  
					moduleHed={this.state.moduleHed} 
					modal_show={this.state.pinModalOpen} 
					returnUrl={this.state.returnUrl}
					closeModal={this.closeModal}
				/>
          </div>
          );
      }
    }


const mapStateToProps = state => ({
    OrganisationProfile : state.OrganisationReducer.orgProfile
})

const mapDispatchtoProps = (dispach) => {
       return {
           
      }
}
                
export default connect(mapStateToProps, mapDispatchtoProps)(OrganisationNavigation)