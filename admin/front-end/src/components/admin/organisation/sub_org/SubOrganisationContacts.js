import React from 'react';
import { ROUTER_PATH } from '../../../../config.js';
import { postData,checkItsNotLoggedIn,archiveALL,reFreashReactTable } from '../../../../service/common.js';
import OrganisationAddContactPopUp from '../OrganisationAddContactPopUp';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import {orgContactViewBy} from '../../../../dropdown/Orgdropdown.js';
import "react-placeholder/lib/reactPlaceholder.css";
import 'react-toastify/dist/ReactToastify.css';
import ReactTable from "react-table";
import Pagination from "../../../../service/Pagination.js";
import 'react-table/react-table.css'
import { connect } from 'react-redux'
import { setActiveSelectPage } from './../actions/OrganisationAction.js'

const requestData = (pageSize, page, sorted, filtered,orgId) => {
    return new Promise((resolve, reject) => {
        var Request = JSON.stringify({pageSize: pageSize, page: page, sorted: sorted, filtered: filtered,orgId: orgId});
        postData('organisation/OrgDashboard/get_org_contact', Request).then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.count)
            };
            resolve(res);
        });

    });
};

class OrganisationContacts extends React.Component {
    constructor(props) {
    super(props);
        checkItsNotLoggedIn(ROUTER_PATH);
        var all=[]  ;
        this.state = {
            sites_list:[],
            view_by_status:0
        };
    this.reactTable = React.createRef();
    }

    componentDidMount() { 
        this.props.setActiveSelectPage('suborg_overview');
    }

    archived(id){
        var org_id = this.props.props.match.params.subOrgId;
        archiveALL({id:id,org_id:org_id},'','organisation/OrgDashboard/archive_organisation_contact').then((result) => {
            if(result.status){
                var requestData = {archive:'0'};
                this.setState({filtered:requestData});
            }
        })
    }

    viewByselectChange(selectedOption, fieldname) { 
       var requestData = {archive:selectedOption};
       this.setState({filtered:requestData,view_by_status:selectedOption});       
    }   

    fetchData = (state, instance) => {
       this.setState({loading: true});
        requestData(
                state.pageSize,
                state.page,
                state.sorted,
                state.filtered,
                this.props.props.match.params.subOrgId
                ).then(res => {
            this.setState({
                sites_list: res.rows,
                pages: res.pages,
                loading: false
            });
        });
    }

    closeEditPopUp=(param)=>
    {
        if(param)
        reFreashReactTable (this,'fetchData');

        this.setState({modal_show:false});
    }

    render() { 
            const columns = [
            {Header: '', accessor: 'id', filterable: false,Cell: (props) => <span>{props.original.key_contact}</span>, Header: <span></span>} ,
            {Cell: (props) => <span></span>, Header: <span></span>, style: {
                    "textAlign": "right", 
                  }, headerStyle: {border:"0px solid #fff" }, headerClassName: 'margin_right_side', sortable: false}, 
                   {expander : true, sortable: false,
                       Expander: ({ isExpanded, ...rest }) => 
                       <div>{isExpanded? <i className="icon icon-arrow-up"></i> : <i className="icon icon-arrow-down"></i>}</div>,
                     headerStyle: {border:"0px solid #fff" },
              }
        ];
        const propsData = {};
        if(this.props.props.match.params.hasOwnProperty('subOrgId') && this.props.props.match.params.subOrgId!=''){
            propsData['subOrgId'] = this.props.props.match.params.subOrgId;
        }

        if(this.props.props.match.params.hasOwnProperty('id') && this.props.props.match.params.id!=''){
            propsData['organisationId'] = this.props.props.match.params.id;
        } 
     return (
            <div>             
                  
                <div className="row"> 
                <div className="col-lg-10 col-sm-12 col-lg-offset-1">
               
                    <div className="tab-content">
                       
                        <div role="tabpanel" className="tab-pane active" id="siteList">
                            <div className="row">
                               <div className="col-lg-9 col-sm-8">
                               <div className="bor_T"></div>
                                    <div className="P_7_TB"><h3>All '{this.props.OrganisationProfile.name}' Contacts:</h3></div>
                                    <div className="bor_T"></div>
                                </div>

                                <div className="col-lg-3 col-sm-4">
                                    <div className="box">
                                         <Select name="view_by_status" required={true} simpleValue={true} searchable={false} clearable={false} options={orgContactViewBy()} placeholder="View By" value={this.state.view_by_status} onChange={(e)=> this.viewByselectChange(e,'view_by_status')}/>
                                    </div>
                                </div>
                            </div>

                            <div className="row P_15_TB">
                                <div className="col-lg-12 col-sm-12">
                                   <div className="schedule_listings header_none_react_table  PL_site">
                                   <ReactTable
                                            PaginationComponent={Pagination} 
                                            columns={columns}
                                            manual 
                                            data={this.state.sites_list}
                                            pages={this.state.pages}
                                            loading={this.state.loading} 
                                            onFetchData={this.fetchData} 
                                            filtered={this.state.filtered}
                                            defaultFiltered ={{archive: 0}}
                                            defaultPageSize={10}
                                            className="-striped -highlight"  
                                            noDataText="No Record Found"
                                            minRows={2}
                                            ref={this.reactTable}
                                            showPagination={false}
                                            previousText={<span className="icon icon-arrow-left privious"></span>}
                                            nextText={<span className="icon icon-arrow-right next"></span>}
                                            SubComponent={(props) => <div className="other_conter">
                                            
                                            <div className="col-sm-10">

                                            <div className="my-0">
                                                <span className="color">{(props.original.type == 1)?'Support Coordinator':(props.original.type == 2)?'Member':(props.original.type == 3)?'Key contact':'Billing contact'}</span> 
                                            </div>
                                            
                                            <div className="my-0">
                                                <span className="color">Phone: </span>
                                                {props.original.OrganisationPh.map((value, idx) => (
                                                    <span><span className="color">{value.primary_phone == '1' ? '(Primary) ' : '(Secondary) '}</span><span key={idx+2}>{value.phone} <i className="icon icon-pending-icons color"></i></span><br/></span>
                                                ))}
                                            </div>
                                            <div className="my-0">
                                                <span className="color">Email: </span> 
                                                 {props.original.OrganisationEmail.map((value, idx) => (
                                                    <span><span className="color">{value.primary_email == '1' ? '(Primary) ' : '(Secondary) '}</span><span key={idx+5}>{value.email}</span><br/></span>
                                                ))}
                                            </div>
                                                
                                            </div>
                                            <div className="col-sm-2 text-right mt-5">
                                            { (this.state.view_by_status ==0) ? <a onClick={()=>this.setState({modal_show:true,selectedData:props.original,edit_mode:1,pageTitle:'Edit contact'})}><i className="icon icon-update update_button mr-2"></i></a>
                                                : <React.Fragment />}
                                            { (this.state.view_by_status ==0) ? <a onClick={()=> this.archived(props.original.id)}><i className="icon icon-email-pending archive_Icon"></i></a>
                                                : <React.Fragment />}
                                            </div>
                                            </div>}
                                        />
                                        </div>
                                        {   this.state.view_by_status ==1 ? <React.Fragment /> :                                          
                                            <a className="pull-right P_30_T">
                                                <button className="button_plus__" onClick={()=>this.setState({modal_show:true,pageTitle:'Adding New Contact to '+ this.props.OrganisationProfile.name,selectedData:{firstname:'',lastname:'',position:'',department:'',phone:'',position:'',email:'',edit_mode:0,OrganisationPh:[],OrganisationEmail:[]}})}><i className="icon icon-add-icons Add-2-1"></i></button>
                                            </a>
                                        }
                                        {this.state.modal_show ? 
                                        <OrganisationAddContactPopUp 
                                            orgId={this.props.props.match.params.subOrgId} 
                                            selectedData={this.state.selectedData} 
                                            orgName={this.props.orgName} 
                                            modal_show={this.state.modal_show} 
                                            closeEditPopUp={this.closeEditPopUp} 
                                            edit_mode={this.state.edit_mode}
                                            pageTitle={this.state.pageTitle}
                                        />:<React.Fragment />
                                        }
                                </div>
                            </div>
                        </div>
                                                
                    </div>
                </div>
                </div>            
            </div>
        );
    }
}
//export default OrganisationContacts;

const mapStateToProps = state => ({
    //ActiveClass : state.OrganisationReducer.ActiveClassProfilePage,
    OrganisationProfile : state.OrganisationReducer.orgProfile
})

 const mapDispatchtoProps = (dispach) => {
       return {
           setActiveSelectPage: (value) => dispach(setActiveSelectPage(value)),
      }
}
                
export default connect(mapStateToProps, mapDispatchtoProps)(OrganisationContacts)