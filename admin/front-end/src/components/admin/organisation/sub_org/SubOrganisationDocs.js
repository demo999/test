import React from 'react';
import jQuery from "jquery";
import { ROUTER_PATH, BASE_URL } from '../../../../config.js';
import { checkItsNotLoggedIn,postData,postImageData } from '../../../../service/common.js';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import {docsViewBy} from '../../../../dropdown/Orgdropdown.js';
import {  toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Modal from 'react-bootstrap/lib/Modal';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { customHeading } from '../../../../service/CustomContentLoader.js';
import { connect } from 'react-redux'
import { confirmAlert } from 'react-confirm-alert'; // Import
import { setActiveSelectPage } from './../actions/OrganisationAction.js'
import { ToastUndo } from 'service/ToastUndo.js'

class SubOrganisationDocs extends React.Component {
	constructor(props) {
		super(props);
        this.state = {
            loading:true,
            orgDocs:[],
            view_by_status:'1',
        };
        checkItsNotLoggedIn(ROUTER_PATH);
        //this.urlParam = this.props.props.match.params.orgId;
    }

    selectChange(selectedOption, fieldname) { 
        var org_id = this.props.props.match.params.houseId;
        this.setState({ view_by_status: selectedOption }, () => {
              this.getHouseDocs(org_id,this.state.view_by_status);  
        });
    }  

    componentDidMount() {
        this.props.setActiveSelectPage('suborg_house_overview');
        var org_id = this.props.props.match.params.houseId;
        var view_by = this.state.view_by_status;    
        this.getHouseDocs(org_id,view_by);       
    }

    getHouseDocs =(org_id,view_by)=>{     
        var requestData = {org_id: org_id,view_by:view_by};
        postData('organisation/OrgDashboard/get_house_docs', requestData).then((result) => {
            if (result.status) {
                this.setState({orgDocs:result.data},()=>{ });  

            } else {
                this.setState({orgDocs:result.data});
            }
             this.setState({loading: false});
        });
    }

    closeDocsPopUp=()=>
    {
        this.setState({openDocsModal:false});
    }

    clickToDownloadFile=(key)=>
    {
        var activeDownload = this.state.orgDocs;
        if(activeDownload[key]['is_active'] == true)
        activeDownload[key]['is_active'] = false;
        else 
        activeDownload[key]['is_active'] = true;
        this.setState({orgDocs:activeDownload}); 
    }

    downloadSelectedFile =()=>
    {
        var org_id = this.props.props.match.params.houseId;
        var requestData = {org_id: org_id,downloadData:this.state.orgDocs};
         
        postData('organisation/OrgDashboard/download_selected_file', requestData).then((result) => {
        if (result.status) {
            window.location.href=BASE_URL+"archieve/"+result.zip_name;                          
               
            var removeDownload = this.state.orgDocs;
            removeDownload.map((value, idx) => {
                removeDownload[idx]['is_active'] = false;
            })
            this.setState({orgDocs:removeDownload});
        } else {
            toast.dismiss();
            toast.error(<ToastUndo message={result.error} showType={'e'} />, {
            // toast.error(result.error, {
                position: toast.POSITION.TOP_CENTER,
                hideProgressBar: true
            });
        }
        });           
    }

    archieveSelectedFile =()=>
    {   
        let selectedData = this.state.orgDocs;
        let checkActiveRecord = selectedData.findIndex(x => x.is_active == true );
        if(checkActiveRecord >= 0)
        { 
            return new Promise((resolve, reject) => {
            confirmAlert({
            customUI: ({ onClose }) => {
                return (
                        <div className='custom-ui'>
                            <div className="confi_header_div">
                                <h3>Confirmation</h3>
                                <span className="icon icon-cross-icons" onClick={() => {
                                onClose();
                                resolve({status: false});}}></span>
                            </div>
                            <p>{'Are you sure you want to Archieve selected docs?'}</p>
                            <div className="confi_but_div">
                                <button className="Confirm_btn_Conf" onClick={
                                    () => {
                                        this.serverSideArchieve();
                                        onClose();
                                        }
                                    } >Confirm</button>
                                <button className="Cancel_btn_Conf" onClick={
                                            () => {
                                                onClose();
                                                    var removeDownload = this.state.orgDocs;
                                                    removeDownload.map((value, idx) => {
                                                        removeDownload[idx]['is_active'] = false;
                                                    })
                                                    this.setState({orgDocs:removeDownload});
                                                resolve({status: false});}}> Cancel</button>
                            </div>
                        </div>
                                        )
                    }
                })
            });     
        }
        else
        { 
            toast.error(<ToastUndo message={'Please select atleast one docs to continue.'} showType={'e'} />, {
            // toast.error('Please select atleast one docs to continue.', {
                position: toast.POSITION.TOP_CENTER,
                hideProgressBar: true
            });
        }    
    }

    serverSideArchieve()
    {
        var org_id = this.props.props.match.params.houseId;
        var requestData = {org_id: org_id,downloadData:this.state.orgDocs};
        postData('organisation/OrgDashboard/archieve_selected_file', requestData).then((result) => {
        if (result.status) {
            this.getHouseDocs(org_id,'1');
        } else {
            toast.error(<ToastUndo message={result.error} showType={'e'} />, {
            // toast.error(result.error, {
                position: toast.POSITION.TOP_CENTER,
                hideProgressBar: true
            });
        }
        }); 
    }

    render() {	
       return (
          <div>
           
                <div className="row">
               
                <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                
                    <div className="tab-content">
                       
                        <div role="tabpanel" className="tab-pane active" id="siteList">
                            <div className="row">
                               <div className="col-lg-9 col-sm-8">
                              <div className="bor_T"></div>
                                    <div className="P_7_TB"><h3>{this.props.OrganisationProfile.name} - Documents:</h3></div>
                                    <div className="bor_T"></div>
                                </div>

                                <div className="col-lg-3 col-sm-4">
                                    <div className="box">
                                         <Select name="view_by_status" required={true} simpleValue={true} searchable={false} clearable={false} options={docsViewBy()} placeholder="View By" value={this.state.view_by_status} onChange={(e)=> this.selectChange(e,'view_by_status')}/>
                                    </div>
                                </div>
                            </div>

                            <div className="row P_15_TB">
                                <div className="col-lg-12 col-sm-12">
                                   
                                    <div className="row flex set_alignment">
                                        <div className="col-lg-9 col-sm-8">

                                            <ul className="file_down quali_width P_15_TB">
                                                {(this.state.orgDocs.length > 0)?  
                                                this.state.orgDocs.map((value, idx) => (

                                                     <li key={idx} onClick={ () => this.clickToDownloadFile(idx)} className={(value.is_active)?'active':'' + value.class_name}>
                                                        <div class="path_file mt-0 mb-4"><b>{value.title}</b></div>
                                                        <span className="icon icon-file-icons d-block"></span>
                                                        <div className="path_file">{value.filename}</div>
                                                    </li>
                                                )) : <div className="no_record mt-2 w-100 py-2">No record found.</div>
                                                }

                                            </ul>

                                        </div>

                                        <div className="col-lg-3 col-sm-4 align-self-end">
                                            <div className="equl_menu_hight">
                                                <span>
                                                    <p><a className="but" onClick={()=> this.downloadSelectedFile()}>Download Selected </a></p>
                                                    {
                                                        (this.state.view_by_status && this.state.view_by_status == 1)?
                                                            <span>
                                                                <a className="but" onClick={()=> this.setState({openDocsModal:true})}>Upload New Docs</a>
                                                                <a className="but mt-3" onClick={()=> this.archieveSelectedFile()}>Archive Selected</a>
                                                            </span>
                                                        :''
                                                    }
                                                </span>
                                            </div>
                                        </div>

                                    </div>
                                   <HouseUploadDocsModal isDocsModalShow={this.state.openDocsModal} closeUploadPopup={this.closeDocsPopUp} houseId={this.props.props.match.params.houseId} getHouseDocs={this.getHouseDocs}/>
                                   
                                </div>
                                                     
                            </div>
                        </div>                        
                    </div>
                </div>
                </div>                   
                    
          </div>
          );
      }
    }

  const mapStateToProps = state => ({
    OrganisationProfile : state.OrganisationReducer.orgProfile
})

 const mapDispatchtoProps = (dispach) => {
       return {
            setActiveSelectPage: (value) => dispach(setActiveSelectPage(value)),
      }
}
                
export default connect(mapStateToProps, mapDispatchtoProps)(SubOrganisationDocs)

 //
  class HouseUploadDocsModal extends React.Component {
    constructor(props) {
        super(props); 
        this.uploadDocsInitialState = { 
            selectedFile: null,  
            expiry_date:null,
            submit_form:true,
            docsTitle:''
        }
        this.state =  this.uploadDocsInitialState;

    } 
    fileChangedHandler = (event) => {
         this.setState({selectedFile: event.target.files[0], filename: event.target.files[0].name})
    }

    uploadHandler = (e) => {
        e.preventDefault();
        jQuery("#upload_form").validate({ /* */});
        if (jQuery("#upload_form").valid())
        {
            const formData = new FormData()
            formData.append('myFile', this.state.selectedFile, this.state.selectedFile.name)
            formData.append('houseId', this.props.houseId)
            formData.append('expiry', this.state.expiry_date!=null && this.state.expiry_date!='' ? this.state.expiry_date:'')
            formData.append('docsTitle', this.state.docsTitle)
            postImageData('organisation/OrgDashboard/upload_house_docs', formData).then((result) => {
            if (result.status) {
                this.props.getHouseDocs(this.props.houseId,'1');
                        this.props.closeUploadPopup();
                        this.setState( this.uploadDocsInitialState);
                        toast.success(<ToastUndo message={'Uploaded successfully.'} showType={'s'} />, {
                        // toast.success("uploaded successfully.", {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                        });
                       this.closeClear(); 
            } else {
               
                toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                    // toast.error(result.error, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                    });
                
            }
             this.setState({loading: false});
        });
        } 
    }

    selectChange(selectedOption, fieldname) {
            var state = {};
            state[fieldname] = selectedOption;        
            this.setState(state);        
    } 

    closeClear = () =>
    {   
        this.setState({docsTitle:'',expiry_date:'',filename:''});
        this.props.closeUploadPopup();
    }

    render() {
        return (
        <div>
            <Modal className="modal fade-scale Modal_A  Modal_B" show={this.props.isDocsModalShow} onHide={this.handleHide} container={this} aria-labelledby="myModalLabel" id="modal_1" tabIndex="-1" role="dialog" >
               <form id="upload_form" method="post" autoComplete="off">
                <Modal.Body>
                    <div className="dis_cell">
                        <div className="text text-left">Upload New Docs:
                            <a data-dismiss="modal" aria-label="Close" className="close_i pull-right mt-1" onClick={()=> this.closeClear()}><i className="icon icon-cross-icons"></i></a>
                        </div>
                        
                        <div className="row P_15_T">
                            <div className="col-sm-5">
                            <label>Title</label>
                            <span className="required">
                                 <input type="text" onChange={(e)=>this.setState({'docsTitle':e.target.value})} value={(this.state.docsTitle)?this.state.docsTitle:''} data-rule-required="true"/>
                             </span>
                            </div>
                           <div className="col-sm-7">
                                <div className="row">
                                    <div className="col-sm-12"><label>Please set an expiry date for this Docs</label></div>
                                    <div className="col-sm-6">
                                         <span  className="required">
                                                <DatePicker className="text-center" selected={this.state.expiry_date} name="expiry_date" onChange={(e) => this.selectChange(e, 'expiry_date')} minDate={moment()} required dateFormat="DD-MM-YYYY"/>
                                         </span>
                                    </div>
                                  </div>
                             </div>
                        </div>
                         
                   
                        <div className="row P_15_T">
                            <div className="col-sm-12">
                            <label>Please select a file to upload</label>
                            </div>
                            <div className="col-sm-4">
                             <span className="required upload_btn">
                                 <label className="btn btn-default btn-sm center-block btn-file">
				    <i className="but" aria-hidden="true">Upload New Doc(s)</i>
				     <input className="p-hidden" type="file" name="special_agreement_file" onChange={this.fileChangedHandler} data-rule-required="true" date-rule-extension= "jpg|jpeg|png|xlx|xls|doc|docx|pdf"  />
		              </label>
                              </span>
                              {(this.state.filename)? <p>File Name: <small>{this.state.filename}</small></p>: ''}
                             
                            </div>
                           
                            
                        </div>

                        <div className="row">
                            <div className="col-sm-7"></div>
                            <div className="col-sm-5">
                                <input type="submit" className="but" value={'Save'}  name="content" disabled={(this.state.submit_form)?false:true} onClick={(e)=>this.uploadHandler(e)}/>
                            </div>
                        </div>
                    </div>  
                </Modal.Body>
               </form>
            </Modal>
        </div>
        );
    }
}

