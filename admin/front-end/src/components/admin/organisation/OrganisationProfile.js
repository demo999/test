import React from 'react';
import {Link, Redirect} from 'react-router-dom';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { postData } from '../../../service/common.js';
import { statusOptionProfile } from '../../../dropdown/Orgdropdown.js';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import Pagination from "../../../service/Pagination.js";
import { CustomProfileImage } from '../../../service/CustomContentLoader.js';
import { setOrgActiveClassProfilePage } from './actions/OrganisationAction.js';
import { connect } from 'react-redux'

class OrganisationProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {enable_portal_access:''};
    }

    componentWillReceiveProps (newProps)
    {
        this.setState(newProps.OrganisationProfile);
        var orgId = newProps.OrganisationProfile.ocs_id;
        this.setState({orgId:orgId});
    }

    enablePortalAccess =(e)=>
    {
        var orgId = this.state.orgId;
        this.setState({enable_portal_access: e.target.checked});
        var requestData = {status: (e.target.checked) ? 1 : 0, orgId: orgId};
        postData('organisation/OrgDashboard/organisation_portal_access', requestData).then((result) => {
            this.setState({loading: false});
            if (result.status) {

            } else {
                this.setState({error: result.error});
            }
        });
    }

    selectChange=(selectedOption, fieldname)=> {
        var orgId = this.state.orgId;
        var state = {};
        state[fieldname] = selectedOption;
        var requestData = {status: selectedOption, orgId: orgId};
        this.setState(state);
        postData('organisation/OrgDashboard/change_status', requestData).then((result) => {
            this.setState({loading: false});
            if (result.status) {

            } else {
                this.setState({error: result.error});
            }
        });
    } 

    render() {
        return (
                <div>
                    {(this.state.is_redirect)? <Redirect to={this.state.url} / >:''}
                    <div className="row">
                        <div className="col-lg-8 col-lg-offset-1 col-sm-12 P_25_TB">
                            <h1 className="color">{this.state.page_title}</h1>
                        </div>
                        <div className="col-lg-2"></div>
                        <div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div>
                    </div>
                
                    <div className="row P_20_TB">
                        <div className="col-lg-6 col-lg-offset-1 col-sm-12">
                         <ReactPlaceholder showLoadingAnimation customPlaceholder={CustomProfileImage} ready={!this.state.loading}>
                            <table width="100%" className="user_profile">
                                <tbody>
                                    <tr>
                                        <td className="width_130">
                                            <div className="user_img">
                                                <img src={this.state.logo_file}/>
                                            </div>
                                        </td>
                                        <td>
                                           
                                            <ul>
                                                <li>HCM-ID:<span> {this.state.ocs_id}</span></li>
                                                <li>Status: <span className="active color"><b>{(this.state.status) && this.state.status == 1 ? 'Active' : 'Inactive'}</b></span></li>
                                                
                                                {(this.state.type && this.state.type == 'org')?
                                                    <span>
                                                        <li>Sub-Orgs: <span>{this.state.sub_org_count}</span></li>
                                                        <li>Sites: <span> {this.state.site_count}</span></li>
                                                    </span>
                                                : (this.state.type && this.state.type == 'sub_org')?
                                                    <span>
                                                        <li>Parent-Org: <span> {this.state.parent_org}</span></li>
                                                        <li>Sites: <span> {this.state.site_count}</span></li>
                                                    </span>
                                                :
                                                <span>
                                                        <li>Parent-Org: <span> {this.state.parent_org}</span></li>
                                                        <li>Linked Sub-Org: <span>{this.state.linked_to}</span></li>
                                                    </span>
                                                }
                                                <li className="pt-2">
                
                                                    <span className="pull-left box_wide">
                                                        <Select name="status" required={true} simpleValue={true} searchable={false} clearable={false} value={this.state.status} onChange={(e) => this.selectChange(e, 'status')} 
                                                                options={statusOptionProfile()} placeholder="Change Status" />                                            
                                                    </span>
                
                                                    <span className="pull-left">
                                                        <p className="select_all">
                                                            <input className="input_c" type="checkbox" name="enable_portal_access" checked={this.state.enable_portal_access || ''} value={this.state.enable_portal_access || ''} id="thing" onChange={(e)=>this.enablePortalAccess(e)} />
                                                            <label htmlFor="thing"></label> <span>Enable Portal Access</span></p>
                                                    </span>                                   
                
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            </ReactPlaceholder>
                        </div>

                       
                    </div>
                
                    
                </div>
                );
    }
}
//export default OrganisationProfile;

const mapStateToProps = state => ({
    ActiveClass : state.OrganisationReducer.ActiveClassProfilePage,
    OrganisationProfile : state.OrganisationReducer.orgProfile
})

 const mapDispatchtoProps = (dispach) => {
       return {
           changeNavigationClass: (value) => dispach(setOrgActiveClassProfilePage(value)),
      }
}
                
export default connect(mapStateToProps, mapDispatchtoProps)(OrganisationProfile)