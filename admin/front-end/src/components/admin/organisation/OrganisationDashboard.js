import React, { Component } from 'react';
import { ROUTER_PATH, PAGINATION_SHOW } from '../../../config.js';

import OrganisationAddSitePopUp from './OrganisationAddSitePopUp';
import { checkItsNotLoggedIn, postData, handleChangeChkboxInput } from '../../../service/common.js';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import Pagination from "../../../service/Pagination.js";
import { setActiveSelectPage } from './actions/OrganisationAction.js'
import { connect } from 'react-redux'

const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {
        // request json
        var Request = JSON.stringify({ pageSize: pageSize, page: page, sorted: sorted, filtered: filtered });
        postData('organisation/OrgDashboard/get_org', Request).then((result) => {
            let filteredData = result.data;

            const res = {
                rows: filteredData,
                pages: (result.count),
                total_duration: result.total_duration
            };
            resolve(res);
        });

    });
};

class OrganisationDashboard extends Component {
    constructor(props) {
        checkItsNotLoggedIn(ROUTER_PATH);
        super(props);
        this.state = {
            caseListing: [],
            current_uri: this.props.props.match.params.type,
            search_value: '',
            include_inactive: false,
            include_incomplete: false,

        }
    }
    
    componentDidMount() { 
         this.props.setActiveSelectPage('org_dashboard_listing');
    }

    fetchData = (state, instance) => {
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
            this.setState({
                caseListing: res.rows,
                pages: res.pages,
                loading: false,
                total_duration: res.total_duration
            });
        })
    }

    searchData = (e) => {
        e.preventDefault();
        var requestData = { srch_box: this.state.search_value };
        this.setState({ filtered: requestData }); 
    }

    closeModal = () => {
        this.setState({ modal_show: false });
    }

    render() {
        var org_detail = '';
        const columns = [
            {
                Header: 'HCM-ID', accessor: 'id', filterable: false, Cell: props => <span>
                    <Link className="inherit-color" to={"/admin/organisation/overview/" + props.original.id}>{props.original.id}</Link>
                </span>, maxWidth: 150
            },
            { Header: 'Name', accessor: 'name', filterable: false, sortable: false },
            { Header: 'Type', accessor: 'a', filterable: false, sortable: false },
            { Header: 'Address ', accessor: 'address', filterable: false, },
            { Header: 'Phone ', accessor: 'phone', sortable: false },
            {
                Header: 'Sub-Orgs/Sites ', accessor: 'description', sortable: false, Cell: props => <span>
                    {org_detail = ' '}
                    {org_detail = <span>{props.original.sub_org_count}<br /></span>}
                    {org_detail = props.original.site_count}
                </span>
            },
            {
                Header: 'Action', accessor: 'id', filterable: false,

                Cell: props => <span>
                    <Link className="inherit-color" to={"/admin/organisation/overview/" + props.original.id} ><i className="icon icon-views"></i></Link>
                </span>, maxWidth: 75
            }
        ]
        const include_incomplete = 0;
        return (
            <div>
                <section className="manage_top">
                    <div className="container-fluid Green">
                        <div className="row  _Common_back_a">
                            <div className="col-lg-10 col-lg-offset-1 col-sm-12">
                            <Link className="d-inline-flex" to={'/admin/dashboard'}><span className="icon icon-back-arrow back_arrow"></span></Link></div>
                        </div>
                        <div className="row">
                            <div className="col-lg-10 col-lg-offset-1 col-sm-12">
                                <div className="bor_T"></div>
                            </div>

                        </div>
                        <div className="row _Common_He_a">
                            <div className="col-lg-6 col-lg-offset-1 col-md-6 col-sm-12">
                                <h1 className="color">Organisation</h1>
                            </div>
                            <div className="col-lg-2 col-md-3 col-sm-6">
                                <Link to={'/admin/organisation/createOrganisation'} className="Plus_button"><i className="icon icon-add-icons create_add_but"></i><span>Create New Org</span></Link>
                            </div>
                            <div className="col-lg-2 col-md-3 col-sm-6">
                                <a className="Plus_button" onClick={() => this.setState({ modal_show: true })}><i className="icon icon-add-icons create_add_but"></i><span>Create House/Site</span></a>

                                {(this.state.modal_show) ?
                                    <OrganisationAddSitePopUp
                                        modal_show={this.state.modal_show}
                                        closeModal={this.closeModal}
                                        callType="1"
                                        mode="add"
                                    />
                                    : ''}
                            </div>
                        </div>
                        <div className="row">
                        <div className="col-lg-10 col-lg-offset-1 col-sm-12">
                                <div className="bor_T"></div>
                            </div>
                        </div>
                        <div className="row _Common_Search_a">
                            <form className="col-lg-8 col-lg-offset-1 col-md-9 col-sm-12" id="srch_feedback" autoComplete="off" onSubmit={this.searchData} method="post">
                                <div className="row d-flex align-items-center my-2">
                                <div className="col-lg-7 col-md-7 col-sm-6">
                                <div className="search_bar">
                                    <div className="input_search">
                                        <input type="text" className="form-control" placeholder="Search" name="search_value" value={this.state.search_value} onChange={(e) => handleChangeChkboxInput(this, e)} />
                                        <button type="submit"><span className="icon icon-search"></span></button>
                                    </div>
                                </div>
                                </div>

                                <div className="col-lg-5 col-md-5 col-sm-6">
                                    <div className="row ">
                                        <span className="col-sm-6 include_box">
                                            <input type="checkbox" className="checkbox_big" id="Include" name="include_inactive" checked={this.state['include_inactive']} value={this.state.include_inactive} onChange={(e) => this.setState({ include_inactive: e.target.checked, filtered: { is_active: e.target.checked } })} />
                                            <label htmlFor="Include"><span></span><small className="pl-1">Include Inactive</small></label>
                                        </span>

                                        <span className="col-sm-6 include_box">
                                            <input type="checkbox" className="checkbox_big" id="incomplete" name="include_incomplete" checked={this.state.include_incomplete} value={this.state.include_incomplete} onChange={(e) => this.setState({ include_incomplete: !this.state.include_incomplete })} />
                                            <label htmlFor="incomplete"><span></span><small className="pl-1">Include Incomplete</small></label>
                                        </span>
                                    </div>
                                </div>
                                </div>
                            </form>
                            <div className="col-lg-2 col-md-3 col-xs-12">
                                <div className="box font_big">
                                    <Select name="view_by_status" required={true} simpleValue={true} searchable={false} Clearable={false} placeholder="Filter by Type" />
                                </div>
                            </div>
                          
                        </div>

                        <div className="row">
                        <div className="col-lg-10 col-lg-offset-1 col-sm-12 P_25_b">
                                <div className="bor_T"></div>
                            </div>
                        </div>


                        <div className="row">
                          <article className="col-lg-10 col-lg-offset-1">

                                <div className="row">
                                    <div className="col-sm-12 listing_table PL_site">
                                        <ReactTable
                                         PaginationComponent={Pagination}
                                            columns={columns}
                                            manual
                                            data={this.state.caseListing}
                                            pages={this.state.pages}
                                            loading={this.state.loading}
                                            onFetchData={this.fetchData}
                                            filtered={this.state.filtered}
                                            defaultFiltered={{ is_active: false }}
                                            defaultPageSize={10}
                                            className="-striped -highlight"
                                            noDataText="No Record Found"
                                            minRows={2}
                                            previousText={<span className="icon icon-arrow-left privious"></span>}
                                            nextText={<span className="icon icon-arrow-right next"></span>}

                                            getTrProps={(state, rowInfo, column, instance) => ({
                                                onClick: e => this.setState({ organisation_id: rowInfo.original.id }, () => { })
                                            })}
                                            showPagination={this.state.caseListing.length > PAGINATION_SHOW ? true : false}
                                        />

                                    </div>
                                </div>

                            </article>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}


const mapStateToProps = state => ({
    //ActiveClass : state.OrganisationReducer.ActiveClassProfilePage,
//    OrganisationProfile : state.OrganisationReducer.orgProfile
})

 const mapDispatchtoProps = (dispach) => {
       return {
            setActiveSelectPage: (value) => dispach(setActiveSelectPage(value)),
      }
}
                
export default connect(mapStateToProps, mapDispatchtoProps)(OrganisationDashboard)

