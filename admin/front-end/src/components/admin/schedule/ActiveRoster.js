import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import ScheduleNavigation from '../../admin/schedule/ScheduleNavigation';
import { checkItsNotLoggedIn, postData, changeTimeZone } from '../../../service/common.js';
import moment from 'moment';
import { RosterDropdown, AnalysisDropdown, rosterFilterOption} from '../../../dropdown/ScheduleDropdown.js';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import Countdown from 'react-countdown-now';
import DatePicker from 'react-datepicker';
import ScheduleMenu from './ScheduleMenu';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import RosterHistory from './RosterHistory';
import {PAGINATION_SHOW} from '../../../config';
import { TotalShowOnTable } from '../../../service/TotalShowOnTable';
import { connect } from 'react-redux'
import SchedulePage from './SchedulePage';
import Pagination from "../../../service/Pagination.js";


const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {

        // request json
        var Request = JSON.stringify({pageSize: pageSize, page: page, sorted: sorted, filtered: filtered, timeZone: moment().zone()});
        postData('schedule/ScheduleListing/get_roster', Request).then((result) => {
            let filteredData = result.data;

            const res = {
                rows: filteredData,
                pages: (result.count),
                total_count: (result.total_count)
            };
            resolve(res);
        });

    });
};


class ActiveRoster extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            rosterListing: [],
            counter: 0,
            start_date: '',
            end_date: '',
            active_panel: 'unfilled'
        }

    }

    fetchData = (state, instance) => {
        // function for fetch data from database
        this.setState({loading: true});
        requestData(
                state.pageSize,
                state.page,
                state.sorted,
                state.filtered
                ).then(res => {
            this.setState({
                rosterListing: res.rows,
                pages: res.pages,
                total_count: res.total_count,
                loading: false
            });
        });
    }

    searchBox = (key, value) => {
        var state = {}
        state[key] = value;
        this.setState(state, () => {
            var filter = {search_box: this.state.search_box, roster_type: this.state.roster_type, start_date: this.state.start_date, end_date: this.state.end_date, status: 1}
            this.setState({filtered: filter});
        });
    }
    
    closeHistory = () => {
        this.setState({open_history: false})
    }

    render() {
                                    
                                    
        const columns = [
            {Header: 'ID', accessor: 'id', filterable: false,  },
            {Header: 'Participant Name', accessor: 'participantName', filterable: false,  },
            {Header: 'Roster Title', accessor: 'title', filterable: false, },
            {Header: 'Start Date', accessor: 'start_date', filterable: false, Cell: props => <span>{changeTimeZone(props.original.start_date, "DD/MM/YYYY")}</span> },
            {Header: 'End Date', accessor: 'end_date', filterable: false,  Cell: props => <span>{props.original.end_date? changeTimeZone(props.original.end_date, "DD/MM/YYYY"): 'N/A'}</span>},
          
            {Cell: (props) => <span className="action_ix__"><i onClick={() => this.setState({historyId : props.original.id, open_history : true})}  className="icon icon-pending-icons icon_h-1 mr-2"></i>
                    <Link to={{ pathname: '/admin/schedule/roster_details/'+ props.original.id, state: this.props.props.location.pathname }}><i className="icon icon-views"></i></Link>
                        </span>, //Header: <div className="">Action</div>, 
                        Header:<TotalShowOnTable countData={this.state.total_count} />,
                        headerStyle: {border:"0px solid #fff" }, sortable: false}, 
        ]

        return (
                <div>
     
                 <section className="manage_top">
    <div className="container-fluid">
        <ScheduleMenu back_url={'/admin/dashboard'} roster={true} />
        <SchedulePage pageTypeParms={'active_roster'}/>

        <div className="row">
            <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                <div className="tab-content">
             

                
                    <div role="tabpanel" className="tab-pane active" id={this.state.active_panel}>
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="row">
                                    <div className="col-md-12 P_7_TB"><h3>Rosters:</h3></div>
                                    <div className="col-md-12"><div className="bor_T"></div></div>
                                </div>
                            </div>
                        </div>


                        <div className="row P_25_T">
                            <div className="col-md-4 col-sm-8">
                                <label>Search</label>
                                <div className="table_search_new">
                                    <input type="text" onChange={(e) => this.searchBox('search_box',e.target.value)} name="" value={this.state.search_box || ''} />
                                    <button type="submit">
                                        <span className="icon icon-search"></span>
                                    </button>
                                </div>
                            </div>
                            <div className="col-md-3 col-sm-4 mt-1">
                                <label></label>
                                <div className="box">
                                    <Select clearable={false} name="roster_type" simpleValue={true} searchable={false} onChange={(e) => this.searchBox('roster_type',e)} 
                            options={rosterFilterOption(0)} placeholder="Roster Type/Department" value={this.state.roster_type} />
                                   
                                </div>
                            </div>

                            <div className="col-md-5 col-sm-8">
                                <div className="row">
                                    <div className="col-md-5 col-sm-6">
                                        <label className="w-100">From</label>
                                        <DatePicker   isClearable={true} name="start_date" onChange={(e) => this.searchBox('start_date',e)} selected={this.state['start_date'] ? moment(this.state['start_date'], 'DD-MM-YYYY') : null} dateFormat="DD-MM-YYYY"  className="text-center px-0" placeholderText="00/00/0000" />
                                   
                                    </div>
                                    <div className="col-md-5 col-sm-6">
                                        <label className="w-100">To</label>
                                        <DatePicker  isClearable={true} name="end_date" onChange={(e) => this.searchBox('end_date',e)} selected={this.state['end_date'] ? moment(this.state['end_date'], 'DD-MM-YYYY') : null} dateFormat="DD-MM-YYYY"  className="text-center px-0" placeholderText="00/00/0000" />
                                   
                                    </div>
                                </div>
                            </div>

                        </div>

                                        <div className="row">
                                            <div className="col-md-12 schedule_listings p_left_table">
                                                <ReactTable
                                                 PaginationComponent={Pagination}
                                                    columns={columns}
                                                    manual
                                                    data={this.state.rosterListing}
                                                    pages={this.state.pages}
                                                    loading={this.state.loading}
                                                    onFetchData={this.fetchData}
                                                    filtered={this.state.filtered}
                                                    defaultFiltered={{ status: '1' }}
                                                    defaultPageSize={10}
                                                    className="-striped -highlight"
                                                    noDataText="No Record Found"
                                                    minRows={2}

                                                    previousText={<span className="icon icon-arrow-left privious"></span>}
                                                    nextText={<span className="icon icon-arrow-right next"></span>}
                                                    showPagination={this.state.rosterListing.length >= PAGINATION_SHOW ? true : false }
                                                />
                                                <RosterHistory open_history={this.state.open_history} rosterId={this.state.historyId} closeHistory={this.closeHistory} />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>



    </div>
</section>
                </div>
                );
    }
}

const mapStateToProps = state => ({
    showPageTitle: state.ScheduleDetailsData.activePage.pageTitle,
    showTypePage: state.ScheduleDetailsData.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {
       
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(ActiveRoster);
