import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { bookedByDropDown, shiftYesNo, confirmWith, confirmWithSites, confirmBy, bookingMethodOption } from '../../../dropdown/ScheduleDropdown.js';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import {
    checkItsNotLoggedIn, postData, handleChangeChkboxInput, handleChangeSelectDatepicker, getOptionsParticipant, getOptionsMember, getOptionsSiteName,
    getOptionsSuburb, reInitializeObject, handleDateChangeRaw
} from '../../../service/common.js';
import jQuery from "jquery";
import Autocomplete from 'react-autocomplete';
import { BASE_URL } from '../../../config.js';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import { ToastUndo } from 'service/ToastUndo.js'


class CreateShift extends Component {
    constructor(props) {
        super(props);
        this.active = this.props.active;

        this.state = {
            loading: false,
            is_save: false,
            save_redirect: 1,
            booking_list: false,
            completeAddress: [{ address: '', 'suburb': '', 'state': '', postal: '' }],
            preferred_member_ary: [{ name: '' }],
            participant_member_ary: [{ name: '' }],
            site_lookup_ary: [{ name: '' }],
            userState: [],
            shift_requirement: [],
            shift_org_requirement: [],
            autocompleteData: [],
            bookinglist: [],
            booked_by: 2,
            site_booker: 1,
            confirm_with: 1,
            confirm_by: 1,
            allocate_pre_member: 2,
            autofill_shift: 2,
            push_to_app: 2,
            booking_method: 1,
        };
    }


    participantOnChange = (e, type, key, fieldName, fieldId) => {
        var state = {};
        var List = this.state[type];
        List[key].name = e;
        state[type] = List;

        if (this.state.booked_by == 2 && type == 'participant_member_ary')
            this.getBookingList(List);

        // get key contact details
        if (this.state.booked_by == 1 && type == 'site_lookup_ary') {
            this.get_key_billing_contact(3).then((result) => {
                this.setState({ site_booker_listing: result, caller_name: '', caller_lastname: '', caller_phone: '', caller_email: '' })
            });
        }

        this.setState(state);
    }

    resetCallerData = () => {
        this.setState({ bookinglist: '', booking_list: '', caller_name: '', caller_lastname: '', caller_phone: '', caller_email: '' });
    }

    getBookingList = (obj) => {
        this.resetCallerData();

        var srchId = obj[0].name.value;
        var requestData = { srchId: srchId };
        postData('schedule/ScheduleDashboard/get_booking_list', requestData).then((result) => {
            if (result.status) {
                this.setState({ bookinglist: result.shift_data, confirmOption: result.shift_data });
            }
        });
    }
    //
    getItemValue = (item) => {
        return item.value + 'SEPARATER' + item.label;
    }

    selectChange = (selectedOption, fieldname) => {
        this.setState({
            caller_name: selectedOption.firstname,
            caller_lastname: selectedOption.lastname,
            caller_phone: selectedOption.phone,
            caller_email: selectedOption.email,
        });

        var state = {};
        state[fieldname] = selectedOption.value;
        state[fieldname + '_error'] = false;
        this.setState(state);
    }

    componentDidMount() {
        this.getState();
        this.getShiftRequirement();
    }

    getState = () => {
        postData('common/common/get_state', {}).then((result) => {
            if (result.status) {
                this.setState({ userState: result.data }, () => { });
            }
        });
    }

    getShiftRequirement = () => {
        postData('common/common/get_shift_requirement', {}).then((result) => {
            if (result.status) {
                this.setState({ shift_requirement: result.data.shift_data }, () => { });
                this.setState({ shift_org_requirement: result.data.org_shift_data }, () => { });
            }
        });
    }

    handleAddHtml = (e, tagType, object_array) => {
        e.preventDefault();
        var state = {};
        var temp = object_array
        var list = this.state[tagType];
        state[tagType] = list.concat([reInitializeObject(temp)]);
        this.setState(state);
    }

    handleRemoveHtml = (e, idx, tagType) => {
        e.preventDefault();
        var state = {};
        var List = this.state[tagType];
        state[tagType] = List.filter((s, sidx) => idx !== sidx);
        this.setState(state);
    }

    handleShareholderNameChange = (idx, value, fieldName, fieldType) => {
        var state = {};
        var tempField = {};
        var List = this.state['completeAddress'];
        List[idx][fieldName] = value;
        if (fieldName == 'state') {
            List[idx]['suburb'] = {}
            List[idx]['postal'] = ''
        }

        if (fieldName == 'suburb' && value) {
            List[idx]['postal'] = value.postcode
        }
        state['completeAddress'] = List;
        this.setState(state);
    }

    setcheckbox = (idx, checked, tagType) => {
        var List = this.state[tagType];
        var state = {};
        if (List[idx].checked == undefined || List[idx].checked == false) {
            List[idx].checked = true
        } else {
            List[idx].checked = false
        }
        state[tagType] = List;
        this.setState(state);
    }

    getTimeDifferInHours = () => {
        var start_time = moment(this.state.start_time);
        var end_time = moment(this.state.end_time);

        const diff = end_time.diff(start_time);
        const diffDuration = moment.duration(diff);
        var dayHours = diffDuration.days() * 24
        return diffDuration.hours() + dayHours;
    }

    errorMessage = (Message) => {

        toast.error(<ToastUndo message={Message} showType={'e'} />, {
        // toast.error(Message, {
            position: toast.POSITION.TOP_CENTER,
            hideProgressBar: true
        });
    }

    handleCreateShift = (e, shift_status) => {
        e.preventDefault();
        var isSubmit = 1;

        var validator = jQuery("#create_shift_form").validate({ ignore: [] });

        var difference = this.getTimeDifferInHours();
        if (difference > 24) {
            this.errorMessage("Shift duration can maximum 24 hours.");
            return false;
        } else if (difference < 0) {
            this.errorMessage("Please check start date time and end date time.");
            return false;
        }

        //if(jQuery("#create_shift_form").valid() && isSubmit)
        if (!this.state.loading && jQuery("#create_shift_form").valid() && isSubmit) {
            this.setState({ status: shift_status }, () => {
                this.setState({ loading: true });
                postData('schedule/ScheduleDashboard/create_shift', this.state).then((result) => {
                    if (result.status) {
                        toast.success(<ToastUndo message={"Shift created successfully."  + result.msg} showType={'s'} />, {   
                        // toast.success("Shift created successfully." + result.msg, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                        this.setState({ is_save: true, save_redirect: result.redirect_type });
                    } else {
                        this.errorMessage(result.error);
                    }
                    this.setState({ loading: false });
                });
            });
        }
        else {
            validator.focusInvalid();
        }
    }

    ValidationForSelect = (currentState) => {
        return (this.state[currentState + '_error']) ? <div className={'tooltip fade top in' + ((this.state[currentState + '_error']) ? ' select-validation-error' : '')} role="tooltip">
            <div className="tooltip-arrow"></div><div className="tooltip-inner">This field is required.</div></div> : '';
    }

    selectConfirmWith = (val, key) => {
        var state = {}
        state[key] = val;
        this.setState(state);

        if (this.state.booked_by == 1) {
            this.setState({ loadingConfirmPerson: true });
            var data = { site_lookup_ary: this.state.site_lookup_ary, confirm_type: val };
            this.get_key_billing_contact(val).then((result) => {
                if (result.length > 0) {
                    this.selectConfirmerShift(result[0]);
                } else {
                    var blankDetails = { firstname: '', lastname: '', phone: '', email: '' };
                    this.selectConfirmerShift(blankDetails);
                }
                this.setState({ confirmOption: result })
                this.setState({ loadingConfirmPerson: false })
            })
        }
    }

    get_key_billing_contact = (contact_type) => {
        var data = { site_lookup_ary: this.state.site_lookup_ary, confirm_type: contact_type };
        return new Promise((resolve, reject) => {
            postData('schedule/ScheduleDashboard/get_key_billing_person', data).then((result) => {
                if (result.status) {
                    resolve(result.data);
                }
            });
        });
    }

    changeDateTime = (value, key) => {
        var state = {};
        if (key == 'start_time') {
            var difference = this.getTimeDifferInHours();
            if (difference > 24 || difference < 0) {
                state['end_time'] = '';
            }
        }

        state[key] = value;
        this.setState(state);
    }

    startDateTime = () => {
        return <DatePicker dateFormat="DD/MM/YYYY h:mm a" autoComplete="new-password" onChangeRaw={handleDateChangeRaw} placeholderText={"00/00/0000 00:00"} className="text-left"
            selected={this.state.start_time} name="start_time" onChange={(e) => this.changeDateTime(e, 'start_time')}
            timeIntervals={15} showTimeSelect timeCaption="Time" required={true} name="start_time"
            maxDate={(this.state.end_time) ? moment().add(60, 'days') : moment().add(60, 'days')}
            minDate={moment()}
            minTime={(moment(this.state.start_time).format("DD-MM-YYYY") == moment().format("DD-MM-YYYY")) ? moment() : moment().hours(0).minutes(0)}
            maxTime={(this.state.end_time) ? moment(this.state.end_time) : moment().hours(23).minutes(59)}
        />
    }

    endDateTime = () => {
        return <DatePicker dateFormat="DD/MM/YYYY h:mm a" autoComplete="new-password" onChangeRaw={handleDateChangeRaw} placeholderText={"00/00/0000 00:00"} className="text-left"
            selected={this.state.end_time} name="end_time" onChange={(e) => this.changeDateTime(e, 'end_time')}
            showTimeSelect timeIntervals={15} timeCaption="Time" required={true} name="end_time"
            minDate={this.state.start_time ? moment(this.state.start_time) : moment()} maxDate={(this.state.start_time) ? moment(this.state.start_time).add(1, 'days') : moment().add(60, 'days')}
            minTime={(moment(this.state.start_time).format("DD-MM-YYYY") == moment(this.state.end_time).format("DD-MM-YYYY")) ? moment(this.state.start_time) : moment().hours(0).minutes(0)}
            maxTime={moment().hours(23).minutes(59)} />
    }



    selectConfirmerShift = (value) => {
        var state = {}

        state['confirmPerson'] = value;
        state['confirm_with_f_name'] = value.firstname;
        state['confirm_with_l_name'] = value.lastname;
        state['confirm_with_mobile'] = value.phone;
        state['confirm_with_email'] = value.email;
        state['confirm_by'] = 1;
        this.setState(state);
    }


    handleChange = (value, booked_by) => {
        var state = {}
        state['booked_by'] = value;
        this.setState(state);

        var blankDetails = { firstname: '', lastname: '', phone: '', email: '' };
        this.resetCallerData();
        this.selectConfirmerShift(blankDetails);
    }


    callHtmlBookedBy = (bookedBy) => {
        //1= 'Site' //2= 'Gaurdian' //3 =Location'

        if (bookedBy == 1) {
            return <div>
                <div className="row P_25_T">
                    <div className="col-lg-1"></div>
                    {this.state.site_lookup_ary.map((value, idx) => (
                        <div className="col-lg-4 col-md-6 col-xs-12" key={idx + 1}>
                            <label>Site Look-up:</label>
                            <span className="requireds">
                                <div className="search_icons_right modify_select">
                                    <Select.Async
                                        cache={false}
                                        name="form-field-name"
                                        clearable={false}
                                        value={value.name}
                                        loadOptions={(e) => getOptionsSiteName(e)}
                                        placeholder='Search'
                                        onChange={(e) => this.participantOnChange(e, 'site_lookup_ary', idx)}
                                    />
                                </div>
                            </span>
                        </div>
                    ))}

                    <div className="col-lg-2 col-md-3 col-xs-12">
                        <label>Site Booker </label>
                        <span >
                            <Select name="booker_id" required={true} searchable={false} clearable={false} value={this.state.booker_id} onChange={(e) => this.selectChange(e, 'booker_id')}
                                options={this.state.site_booker_listing} placeholder="Please select" />
                        </span>
                    </div>

                    <div className="col-lg-2 col-md-3 col-xs-12">
                        <label>Booking Method </label>
                        <span className="required">
                            <Select name="booking_method" required={true} simpleValue={true} searchable={false} clearable={false} value={this.state.booking_method || 1} onChange={(e) => handleChangeSelectDatepicker(this, e, 'booking_method')}
                                options={bookingMethodOption(0)} placeholder="Please select" />
                        </span>
                    </div>
                </div>

                <div className="row P_25_TB">
                    <div className="col-lg-2 col-md-3 col-xs-12 col-lg-offset-1">
                        <label>Caller First Name:</label>
                        <span className="required">
                            <input placeholder="First" type="text" name="caller_name" data-rule-required="true" value={this.state.caller_name || ''} onChange={(e) => handleChangeChkboxInput(this, e)} />
                        </span>
                    </div>
                    <div className="col-lg-2 col-md-3 col-xs-12">
                        <label>Caller Last Name:</label>
                        <input placeholder="Last" type="text" name="caller_lastname" value={this.state.caller_lastname || ''} onChange={(e) => handleChangeChkboxInput(this, e)} />
                    </div>
                    <div className="col-lg-2 col-md-3 col-xs-12">
                        <label>Caller Mobile:</label>
                        <span className="required">
                            <input placeholder="0000 000 000" type="text" name="caller_phone" data-rule-required="true" value={this.state.caller_phone || ''} onChange={(e) => handleChangeChkboxInput(this, e)} data-rule-phonenumber />
                        </span>
                    </div>
                    <div className="col-lg-2 col-md-3 col-xs-12">
                        <label>Caller Email:</label>
                        <input placeholder="example@example.com.au" type="email" name="caller_email" data-rule-required="true" value={this.state.caller_email || ''} onChange={(e) => handleChangeChkboxInput(this, e)} />
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1 col-sm-12"><h3 className="color P_15_TB by-1">Shift Details:</h3></div><div className="col-lg-1"></div>
                </div>

                <div className="row P_25_T">
                    <div className="col-lg-2 col-lg-offset-1 col-md-3 col-xs-12">
                        <label>Start Date Time:</label>
                        <span className="required">
                            {this.startDateTime()}
                        </span>
                    </div>
                    <div className="col-lg-2 col-md-3 col-xs-12">
                        <label>End Date Time:</label>
                        <span className="required">
                            {this.endDateTime()}
                        </span>
                    </div>
                    <div className="col-lg-4 col-md-3 col-xs-12  P_30_TB text-left">

                        <span>
                            <input type="checkbox" className="checkbox1 font" onClick={(e) => handleChangeChkboxInput(this, e)} id="so" name="so" value={this.state.so || ''} />
                            <label htmlFor="so">
                                <span ></span>S/O
                                    </label>
                        </span>

                        <span>
                            <input type="checkbox" className="checkbox1 font" id="ao" name="ao" onClick={(e) => handleChangeChkboxInput(this, e)} />
                            <label htmlFor="ao"><span></span>A/O</label>
                        </span>
                        <span>
                            <input type="checkbox" className="checkbox1 font" id="eco" name="eco" onClick={(e) => handleChangeChkboxInput(this, e)} />
                            <label htmlFor="eco"><span></span>ECO</label>
                        </span>
                    </div>
                </div>

                {this.state.completeAddress.map((value, idx) => (
                    <div className="row P_25_b AL_flex" key={idx}>
                        {/* <div className=""></div> */}
                        <div className="col-lg-3 col-md-3 col-xs-12 col-lg-offset-1">
                            <label>Address:</label>
                            <span className="required">
                                <input placeholder="Unit #/Street #, Street Name" type="text" name={'address' + idx} data-rule-required="true" value={value.address} onChange={(e) => this.handleShareholderNameChange(idx, e.target.value, 'address')} />
                            </span>
                        </div>

                        <div className="col-lg-1 col-md-2 col-xs-12">
                            <label>State:</label>
                            <span className="required">
                                <Select name={'state' + idx} required={true} simpleValue={true} searchable={false} clearable={false} value={value.state} onChange={(e) => this.handleShareholderNameChange(idx, e, 'state', 'select')} options={this.state.userState} placeholder="State" className="default_validation" />
                            </span>

                        </div>

                        <div className="col-lg-3 col-md-3 col-xs-12">
                            <label>Suburb:</label>
                            <span className="required modify_select">
                                <Select.Async cache={false} clearable={false} name={'suburb' + idx} className="default_validation" required={true}
                                    value={value.suburb} disabled={(value.state) ? false : true} loadOptions={(val) => getOptionsSuburb(val, value.state)} onChange={(e) => this.handleShareholderNameChange(idx, e, 'suburb')}
                                    placeholder="Search" />
                            </span>
                        </div>



                        <div className="col-lg-1 col-md-2 col-xs-12">
                            <label>PostCode:</label>
                            <span className="required">
                                <input placeholder="0000" type="text" minLength="4" maxLength="4" data-rule-number="true" data-rule-postcodecheck="true" name={'postal' + idx} data-rule-required="true" value={value.postal} onChange={(e) => this.handleShareholderNameChange(idx, e.target.value, 'postal')} />
                            </span>
                        </div>

                        <div className="col-lg-2 col-md-2 col-xs-12">
                            {idx > 0 ? <button onClick={(e) => this.handleRemoveHtml(e, idx, 'completeAddress')} className="button_plus__">
                                <i className="icon icon-decrease-icon Add-2-2" ></i>
                            </button> : (this.state.completeAddress.length == 3) ? '' : <button className="button_plus__" onClick={(e) => this.handleAddHtml(e, 'completeAddress', value)}>
                                <i className="icon icon-add-icons Add-2-1" ></i>
                            </button>}
                        </div>
                    </div>
                ))}
            </div>
        }
        else if (bookedBy == 2) {
            return <div>
                <div className="row P_25_T">
                    <div className="col-lg-1"></div>
                    {this.state.participant_member_ary.map((value, idx) => (
                        <div className="col-lg-4 col-md-6 col-xs-12" key={idx + 1}>
                            <label>Participant Name:</label>
                            <span className="required">
                                <div className="search_icons_right modify_select">
                                    <Select.Async
                                        cache={false}
                                        clearable={false}
                                        name="form-field-name" required={true}
                                        value={value.name}
                                        loadOptions={getOptionsParticipant}
                                        placeholder='Search'
                                        onChange={(e) => this.participantOnChange(e, 'participant_member_ary', idx)}
                                        className="default_validation"
                                    />

                                </div>
                            </span>
                        </div>
                    ))
                    }

                    <div className="col-lg-2 col-md-3 col-xs-12">
                        <label>Booking List </label>
                        <span >
                            <Select name="booker_id" searchable={false} clearable={false} value={this.state.booker_id} onChange={(e) => this.selectChange(e, 'booker_id')}
                                options={this.state.bookinglist} placeholder="Please select" />
                        </span>
                    </div>

                    <div className="col-lg-2 col-md-3  col-xs-12">
                        <label>Booking Method </label>
                        <span className="required">
                            <Select name="booking_list" required={true} searchable={false} simpleValue={true} clearable={false} value={this.state.booking_method} onChange={(e) => handleChangeSelectDatepicker(this, e, 'booking_method')}
                                options={bookingMethodOption(0)} placeholder="Please select" />
                        </span>
                    </div>
                </div>

                <div className="row P_25_TB">
                    <div className="col-lg-2 col-md-3  col-xs-12 col-lg-offset-1">
                        <label>Booker First Name:</label>
                        <span className="required">
                            <input placeholder="First" type="text" name="caller_name" data-rule-required="true" value={this.state.caller_name || ''} onChange={(e) => handleChangeChkboxInput(this, e)} />
                        </span>
                    </div>
                    <div className="col-lg-2 col-md-3 col-xs-12">
                        <label>Booker Last Name:</label>
                        <input placeholder="Last" type="text" name="caller_lastname" value={this.state.caller_lastname || ''} onChange={(e) => handleChangeChkboxInput(this, e)} />
                    </div>
                    <div className="col-lg-2 col-md-3 col-xs-12">
                        <label>Booker Mobile:</label>
                        <span className="required">
                            <input placeholder="0000 000 000" type="text" name="caller_phone" data-rule-required="true" value={this.state.caller_phone || ''} onChange={(e) => handleChangeChkboxInput(this, e)} data-rule-phonenumber />
                        </span>
                    </div>
                    <div className="col-lg-2 col-md-3 col-xs-12">
                        <label>Booker Email:</label>
                        <span className="required">
                            <input placeholder="example@example.com.au" type="email" name="caller_email" data-rule-required="true" value={this.state.caller_email || ''} onChange={(e) => handleChangeChkboxInput(this, e)} />
                        </span>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1 col-sm-12"><h3 className="color P_15_TB by-1">Shift Details:</h3></div><div className="col-lg-1"></div>
                </div>

                <div className="row P_25_T">
                    <div className="col-lg-2 col-lg-offset-1 col-md-3 col-xs-12">
                        <label>Start Date Time:</label>
                        <span className="required ">
                            {this.startDateTime()}
                        </span>
                    </div>
                    <div className="col-lg-2 col-md-3 col-xs-12">
                        <label>End Date Time:</label>
                        <span className="required">
                            {this.endDateTime()}
                        </span>
                    </div>
                    <div className="col-lg-4 col-md-3 col-xs-12  P_30_TB text-left">

                        <span>
                            <input type="checkbox" className="checkbox1 font" onClick={(e) => handleChangeChkboxInput(this, e)} id="so" name="so" value={this.state.so || ''} />
                            <label htmlFor="so">
                                <span ></span>S/O
                                    </label>
                        </span>

                        <span>
                            <input type="checkbox" className="checkbox1 font" id="ao" name="ao" onClick={(e) => handleChangeChkboxInput(this, e)} />
                            <label htmlFor="ao"><span></span>A/O</label>
                        </span>
                        <span>
                            <input type="checkbox" className="checkbox1 font" id="eco" name="eco" onClick={(e) => handleChangeChkboxInput(this, e)} />
                            <label htmlFor="eco"><span></span>ECO</label>
                        </span>
                    </div>
                </div>

                {this.state.completeAddress.map((value, idx) => (
                    <div className="row P_25_b AL_flex" key={idx}>
                        {/* <div className="col-lg-1"></div> */}
                        <div className="col-lg-3 col-md-3 col-xs-12 col-lg-offset-1">
                            <label>Address:</label>
                            <span className="required">
                                <input placeholder="Unit #/Street #, Street Name" type="text" name={'address' + idx} data-rule-required="true" value={value.address} onChange={(e) => this.handleShareholderNameChange(idx, e.target.value, 'address')} />
                            </span>
                        </div>

                        <div className="col-lg-1 col-md-2 col-xs-12">
                            <label>State:</label>
                            <span className="required">
                                <Select name={'state' + idx} required={true} simpleValue={true} searchable={false} clearable={false} value={value.state} onChange={(e) => this.handleShareholderNameChange(idx, e, 'state', 'select')} options={this.state.userState} placeholder="State" className="default_validation" />
                            </span>
                        </div>

                        <div className="col-lg-3 col-md-3 col-xs-12">
                            <label>Suburb:</label>
                            <span className="required modify_select">

                                <Select.Async cache={false} clearable={false} name={'suburb' + idx} className="default_validation" required={true}
                                    value={value.suburb} disabled={(value.state) ? false : true} loadOptions={(val) => getOptionsSuburb(val, value.state)} onChange={(e) => this.handleShareholderNameChange(idx, e, 'suburb')}
                                    placeholder="Search" />
                            </span>
                        </div>

                        <div className="col-lg-1 col-md-2 col-xs-12">
                            <label>PostCode:</label>
                            <span className="required">
                                <input placeholder="0000" data-rule-number="true" minLength="4" maxLength="4" type="text" name={'postal' + idx} data-rule-required="true" data-rule-postcodecheck="true" value={value.postal} onChange={(e) => this.handleShareholderNameChange(idx, e.target.value, 'postal')} />
                            </span>
                        </div>

                        <div className="col-lg-2 col-md-2 col-xs-12">
                            {idx > 0 ? <button onClick={(e) => this.handleRemoveHtml(e, idx, 'completeAddress')} className="button_plus__">
                                <i className="icon icon-decrease-icon Add-2-2" ></i>
                            </button> : (this.state.completeAddress.length == 3) ? '' : <button className="button_plus__" onClick={(e) => this.handleAddHtml(e, 'completeAddress', value)}>
                                <i className="icon icon-add-icons Add-2-1" ></i>
                            </button>}
                        </div>
                    </div>
                ))}
            </div>
        }
        else if (bookedBy == 3) {
            return <div>
                <div className="row P_25_TB">
                    {/* <div className="col-lg-1"></div> */}
                    <div className="col-lg-8 col-md-9 col-lg-offset-1">
                        {this.state.completeAddress.map((value, idx) => (

                            <span className="w-100 AL_flex" key={idx}>

                                <div className="col-lg-3 col-md-3 col-xs-12 pl-0">
                                    <label>Address:</label>
                                    <span className="required">
                                        <input placeholder="Unit #/Street #, Street Name" type="text" name={'address' + idx} data-rule-required="true" value={value.address} onChange={(e) => this.handleShareholderNameChange(idx, e.target.value, 'address')} />
                                    </span>
                                </div>

                                <div className="col-lg-2 col-md-2 col-xs-12">
                                    <label>State:</label>
                                    <span className="required">
                                        <Select name={'state' + idx} required={true} simpleValue={true} searchable={false} clearable={false} value={value.state} onChange={(e) => this.handleShareholderNameChange(idx, e, 'state', 'select')} options={this.state.userState} placeholder="State" className="default_validation" />
                                    </span>

                                </div>

                                <div className="col-lg-3 col-md-2 col-xs-12">
                                    <label>Suburb:</label>
                                    <span className="required modify_select">
                                        <Select.Async clearable={false} cache={false} name={'suburb' + idx} className="default_validation" required={true}
                                            value={value.suburb} disabled={(value.state) ? false : true} loadOptions={(val) => getOptionsSuburb(val, value.state)} onChange={(e) => this.handleShareholderNameChange(idx, e, 'suburb')}
                                            placeholder="Search" />
                                    </span>
                                </div>

                                <div className="col-lg-2 col-md-2 col-xs-12">
                                    <label>PostCode:</label>
                                    <span className="required">
                                        <input data-rule-number="true" placeholder="0000" type="text" minLength="4" maxLength="4" name={'postal' + idx} data-rule-required="true" data-rule-postcodecheck="true" value={value.postal} onChange={(e) => this.handleShareholderNameChange(idx, e.target.value, 'postal')} />
                                    </span>
                                </div>

                                <div className="col-lg-1 col-md-1 col-xs-12">
                                    {idx > 0 ? <button onClick={(e) => this.handleRemoveHtml(e, idx, 'completeAddress')} className="button_plus__">
                                        <i className="icon icon-decrease-icon Add-2-2" ></i>
                                    </button> : (this.state.completeAddress.length == 3) ? '' : <button  className="button_plus__" onClick={(e) => this.handleAddHtml(e, 'completeAddress', value)}>
                                        <i className="icon icon-add-icons Add-2-1" ></i>
                                    </button>}
                                </div>

                            </span>
                        ))}
                    </div>

                    <div className="col-lg-2 col-md-3">
                        <label>Booking Method </label>
                        <span className="required">
                            <Select name="booking_method" required={true} searchable={false} clearable={false} value={this.state.booking_method} onChange={(e) => this.selectChange(e, 'booking_method')}
                                options={bookingMethodOption(0)} placeholder="Please select" />
                        </span>
                    </div>
                </div>




                <div className="row P_25_b">
                    <div className="col-lg-2 col-md-3 col-xs-12 col-lg-offset-1">
                        <label>Caller First Name:</label>
                        <span className="required">
                            <input placeholder="First" type="text" name="caller_name" data-rule-required="true" value={this.state.caller_name || ''} onChange={(e) => handleChangeChkboxInput(this, e)} />
                        </span>
                    </div>
                    <div className="col-lg-2 col-md-3 col-xs-12">
                        <label>Caller Last Name:</label>
                        <input placeholder="Last" type="text" name="caller_lastname" value={this.state.caller_lastname || ''} onChange={(e) => handleChangeChkboxInput(this, e)} />
                    </div>
                    <div className="col-lg-2 col-md-3 col-xs-12">
                        <label>Caller Mobile:</label>
                        <span className="required">
                            <input placeholder="0000 000 000" type="text" name="caller_phone" data-rule-required="true" value={this.state.caller_phone || ''} onChange={(e) => handleChangeChkboxInput(this, e)} data-rule-phonenumber />
                        </span>
                    </div>
                    <div className="col-lg-2 col-md-3 col-xs-12">
                        <label>Caller Email:</label>
                        <span className="required">
                            <input placeholder="example@example.com.au" type="email" name="caller_email" data-rule-required="true" value={this.state.caller_email || ''} onChange={(e) => handleChangeChkboxInput(this, e)} />
                        </span>
                    </div>
                </div>

                {this.state.participant_member_ary.map((value, idx) => (
                    <div className="row mb-5" key={idx + 2}>
                        <div className="col-lg-1"></div>
                        <div className="col-lg-4 col-md-6 col-xs-12">
                            <label>Add Participant:</label>
                            <div className="search_icons_right modify_select">
                                <Select.Async
                                    cache={false}
                                    name="form-field-name"
                                    value={value.name}
                                    loadOptions={(e) => getOptionsParticipant(e)}
                                    placeholder='Search'
                                    onChange={(e) => this.participantOnChange(e, 'participant_member_ary', idx)}
                                />

                            </div>
                        </div>
                        <div className="col-lg-2 col-md-2 col-xs-12 P_20_T" style={{ display: 'none' }}>
                            {
                                idx > 0 ? <button onClick={(e) => this.handleRemoveHtml(e, idx, 'participant_member_ary')} className="button_unadd">
                                    <i className="icon icon-decrease-icon icon_cancel_1" ></i>
                                </button> : (this.state.participant_member_ary.length == 3) ? '' : <button className="add_i_icon" onClick={(e) => this.handleAddHtml(e, 'participant_member_ary', value)}>
                                    <i className="icon icon-add-icons" ></i>
                                </button>
                            }
                        </div>
                    </div>

                ))}

                <div className="row pb-5 P_25_TB">
                    <div className="col-lg-10 col-lg-offset-1 col-sm-12 mt-4"><h3 className="color P_15_TB by-1">Shift Details:</h3></div><div className="col-lg-1"></div>
                </div>

                <div className="row">
                    <div className="col-lg-2 col-lg-offset-1 col-md-3 col-xs-12">
                        <label>Start Time:</label>
                        <span className="required">
                            {this.startDateTime()}
                        </span>
                    </div>
                    <div className="col-lg-2 col-md-3 col-xs-12">
                        <label>End Time:</label>
                        <span className="required">
                            {this.endDateTime()}
                        </span>
                    </div>
                    <div className="col-lg-4 col-md-3 col-xs-12  P_30_TB text-left">

                        <span>
                            <input type="checkbox" className="checkbox1 font" onClick={(e) => handleChangeChkboxInput(this, e)} id="so" name="so" value={this.state.so || ''} />
                            <label htmlFor="so">
                                <span ></span>S/O
                                    </label>
                        </span>

                        <span>
                            <input type="checkbox" className="checkbox1 font" id="ao" name="ao" onClick={(e) => handleChangeChkboxInput(this, e)} />
                            <label htmlFor="ao"><span></span>A/O</label>
                        </span>
                        <span>
                            <input type="checkbox" className="checkbox1 font" id="eco" name="eco" onClick={(e) => handleChangeChkboxInput(this, e)} />
                            <label htmlFor="eco"><span></span>ECO</label>
                        </span>
                    </div>
                </div>
            </div>
        }
    }

    render() {
        return (
            <div>
                {(this.state.is_save && this.state.save_redirect == 1) ? <Redirect to='/admin/schedule/unfilled/unfilled' /> : ((this.state.is_save && this.state.save_redirect == 2) ? <Redirect to='/admin/schedule/unconfirmed/unconfirmed' /> : '')}
                <BlockUi tag="div" blocking={this.state.loading}>
                    <section className="manage_top">
                        <form id="create_shift_form" method="post"  >
                            <div className="container-fluid">

                                <div className="row  _Common_back_a">
                                    <div className="col-lg-10 col-lg-offset-1 col-sm-12"><Link className="d-inline-flex" to='/admin/schedule/unfilled/unfilled'><div className="icon icon-back-arrow back_arrow"></div></Link></div>
                                </div>
                                <div className="row"><div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div></div>

                                <div className="row _Common_He_a">
                                    <div className="col-lg-10 col-lg-offset-1 col-sm-12">
                                        <h1 className={'color'}>Creating a New Shift:</h1>
                                    </div>
                                </div>

                                <div className="row"><div className="col-lg-10 col-lg-offset-1 col-sm-12"><div className="bor_T"></div></div></div>

                                <div className="row">
                                    <div className="col-lg-2 col-lg-offset-1 P_25_TB col-md-3">
                                        <label>Booked For:</label>
                                        <span className="required">
                                            <Select name="booked_by" required={true} simpleValue={true} searchable={false} clearable={false} value={this.state.booked_by || 2} onChange={(e) => this.handleChange(e, 'booked_by')}
                                                options={bookedByDropDown(0)} placeholder="Booked By" />
                                        </span>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-lg-10 col-lg-offset-1 col-sm-12"><h3 className="color P_15_TB by-1">Booking Details:</h3></div><div className="col-lg-1"></div>
                                </div>

                                {this.callHtmlBookedBy(this.state.booked_by)}

                                <div className="row">
                                    <div className="col-lg-10 col-lg-offset-1 col-sm-12"><h3 className="color P_15_TB by-1">{(this.state.booked_by == 1) ? 'Shift & Org Requirements' : 'Shift Requirements'}:</h3></div><div className="col-lg-1"></div>
                                </div>

                                <div className="row P_25_TB">
                                    <label htmlFor="shift_requirement[]" className="error CheckieError" ></label>
                                    <div className="col-lg-4 col-lg-offset-1 col-md-6 col-xs-12">
                                        <label>Shift Requirements:</label>
                                        <div className="Schedules_Multiple_checkbox overflow-hidden">
                                            <div className="scroll_active_modal">
                                                {this.state.shift_requirement.map((value, key) => (
                                                    <span key={key + 1} className="w_50 d-inline-block pb-2">
                                                        <input type='checkbox' name="shift_requirement[]" className="checkbox1" checked={value.checked || ''} onChange={(e) => this.setcheckbox(key, value.checked, 'shift_requirement')} name="shift_requirement" data-rule-required="true" />
                                                        <label >
                                                            <div className="d_table-cell">
                                                                <span onClick={(e) => this.setcheckbox(key, value.checked, 'shift_requirement')}></span>
                                                            </div>
                                                            <div className="d_table-cell" onClick={(e) => this.setcheckbox(key, value.checked, 'shift_requirement')}>{value.label}</div>
                                                        </label>
                                                    </span>
                                                ))}
                                            </div>
                                        </div>
                                    </div>
                                    {(this.state.booked_by && this.state.booked_by == 1) ?
                                        <div className="col-lg-4 col-lg-offset-1 col-md-6 col-xs-12">
                                            <label>Org Requirements:</label>
                                            <div className="Schedules_Multiple_checkbox overflow-hidden">
                                                <div className="scroll_active_modal">
                                                    {this.state.shift_org_requirement.map((value, key) => (
                                                        <span key={key + 1} className="w_50 d-inline-block pb-2">
                                                            <input type='checkbox' name="oc_service" className="checkbox1" checked={value.checked || ''} onChange={(e) => this.setcheckbox(key, value.checked, 'shift_org_requirement')} name="shift_org_requirement" data-rule-required="true" />
                                                            <label >
                                                                <div className="d_table-cell">
                                                                    <span onClick={(e) => this.setcheckbox(key, value.checked, 'shift_org_requirement')}></span>
                                                                </div>
                                                                <div className="d_table-cell" onClick={(e) => this.setcheckbox(key, value.checked, 'shift_org_requirement')}>{value.label}</div>
                                                            </label>
                                                        </span>
                                                    ))}
                                                </div>
                                            </div>
                                        </div> : ''}
                                </div>

                                <div className="row">
                                    <div className="col-lg-10 col-lg-offset-1 col-sm-12"><h3 className="color P_15_TB by-1">Confimation Details:</h3></div><div className="col-lg-1"></div>
                                </div>
                                <div className="row P_25_TB">
                                    <div className="col-lg-2 col-lg-offset-1 col-md-3 col-xs-12">
                                        <label>Confirm With:</label>
                                        <span className="required">
                                            <Select name="confirm_with" required={true} simpleValue={true} searchable={false} clearable={false} value={this.state.confirm_with} onChange={(e) => this.selectConfirmWith(e, 'confirm_with')}
                                                options={(this.state.booked_by == 1) ? confirmWithSites(0) : confirmWith(0)} placeholder="Please select" />
                                        </span>
                                    </div>

                                    {(this.state.booked_by && ((this.state.booked_by == 1 && this.state.confirm_with >= 3) || (this.state.booked_by == 2 && this.state.confirm_with == 1))) ?
                                        <div className="col-lg-2 col-lg-offset-1 col-md-3 col-xs-12">
                                            <label>{(this.state.confirm_with == 1) ? confirmWith(this.state.confirm_with) : confirmWithSites(this.state.confirm_with)}:</label>
                                            <span className="required">
                                                <Select isLoading={this.state.loadingConfirmPerson} required={true} searchable={false} clearable={false} value={this.state.confirmPerson} onChange={(e) => this.selectConfirmerShift(e, 'confirmPerson')}
                                                    options={this.state.confirmOption} placeholder="Please select" />
                                            </span>
                                        </div>
                                        : ''}

                                </div>

                                <div className="row P_25_b">
                                    <div className="col-lg-2 col-md-3 col-lg-offset-1 col-xs-12">
                                        <label>First Name:</label>
                                        <span className="required">
                                            <input placeholder="First" type="text" name="confirm_with_f_name" data-rule-required="true" value={this.state.confirm_with_f_name || ''} onChange={(e) => handleChangeChkboxInput(this, e)} />
                                        </span>
                                    </div>
                                    <div className="col-lg-2 col-md-3 col-xs-12">
                                        <label>Last Name</label>
                                        <input placeholder="Last" type="text" name="confirm_with_l_name" data-rule-required="true" value={this.state.confirm_with_l_name || ''} onChange={(e) => handleChangeChkboxInput(this, e)} />
                                    </div>
                                    <div className="col-lg-2 col-md-3 col-xs-12">
                                        <label>Mobile:</label>
                                        <span className="required">
                                            <input placeholder="0000 000 000" type="text" name="confirm_with_mobile" data-rule-required="true" value={this.state.confirm_with_mobile || ''} onChange={(e) => handleChangeChkboxInput(this, e)} data-rule-phonenumber />
                                        </span>
                                    </div>
                                    <div className="col-lg-2 col-md-3 col-xs-12">
                                        <label>Email:</label>
                                        <span className="required">
                                            <input placeholder="example@example.com.au" type="email" name="confirm_with_email" data-rule-required="true" value={this.state.confirm_with_email || ''} onChange={(e) => handleChangeChkboxInput(this, e)} />
                                        </span>
                                    </div>


                                    <div className="col-lg-2 col-md-3 col-xs-12">
                                        <label>Confirm By:</label>
                                        <span className="required">
                                            <Select name="confirm_by" required={true} simpleValue={true} searchable={false} clearable={false} value={this.state.confirm_by || 1} onChange={(e) => handleChangeSelectDatepicker(this, e, 'confirm_by')} options={confirmBy(0)} placeholder="Please select" />
                                        </span>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-lg-10 col-lg-offset-1 col-md-12"><h3 className="color P_15_TB by-1">Misc:</h3></div><div className="col-lg-1"></div>
                                </div>

                                {this.state.preferred_member_ary.map((value, idx) => (
                                    <div className="row P_25_T" key={idx}>
                                        <div className="col-lg-4 col-lg-offset-1 col-md-6 col-xs-12">
                                            <label>Site Preferred Member:</label>
                                            <div className="search_icons_right modify_select">

                                                <Select.Async
                                                    cache={false}
                                                    name="form-field-name"
                                                    value={value.name}
                                                    loadOptions={(e) => getOptionsMember(e, 'type')}
                                                    placeholder='Search'
                                                    onChange={(e) => this.participantOnChange(e, 'preferred_member_ary', idx)}
                                                />

                                            </div>
                                        </div>
                                        <div className="col-lg-2 col-md-2 col-xs-12 P_20_T">
                                            {
                                                idx > 0 ? <button onClick={(e) => this.handleRemoveHtml(e, idx, 'preferred_member_ary')} className="button_unadd">
                                                    <i className="icon icon-decrease-icon icon_cancel_1" ></i>
                                                </button> : (this.state.preferred_member_ary.length == 1) ? '' : <button className="add_i_icon mt-2 pt-1" onClick={(e) => this.handleAddHtml(e, 'preferred_member_ary', value)}>
                                                    <i className="icon icon-add-icons" ></i>
                                                </button>
                                            }
                                        </div>
                                    </div>
                                ))
                                }

                                <div className="row P_25_TB">
                                    <div className="col-lg-2 col-md-3 col-lg-offset-1 col-xs-12">
                                        <label>Allocate Pref Member(s):</label>
                                        <Select name="allocate_pre_member" required={true} disabled={(this.state.push_to_app == 1 || this.state.autofill_shift == 1) ? true : false} simpleValue={true} searchable={false} clearable={false} value={this.state.allocate_pre_member || 2} onChange={(e) => handleChangeSelectDatepicker(this, e, 'allocate_pre_member')}
                                            options={shiftYesNo()} placeholder="" />
                                    </div>
                                    <div className="col-lg-2 col-md-3 col-xs-12">
                                        <label>Autofill Shift</label>
                                        <span className="required">
                                            <Select name="autofill_shift" disabled={(this.state.push_to_app == 1 || this.state.allocate_pre_member == 1) ? true : false} required={true} simpleValue={true} searchable={false} clearable={false} value={this.state.autofill_shift || 2} onChange={(e) => handleChangeSelectDatepicker(this, e, 'autofill_shift')}
                                                options={shiftYesNo()} placeholder="" />
                                        </span>
                                    </div>
                                    <div className="col-lg-2 col-md-3 col-xs-12">
                                        <label>Push to App:</label>
                                        <Select name="push_to_app" disabled={(this.state.allocate_pre_member == 1 || this.state.autofill_shift == 1) ? true : false} required={true} simpleValue={true} searchable={false} clearable={false} value={this.state.push_to_app || 2} onChange={(e) => handleChangeSelectDatepicker(this, e, 'push_to_app')}
                                            options={shiftYesNo()} placeholder="" />
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-lg-10 col-lg-offset-1 col-md-12 col-xs-12"><h3 className="color P_15_TB by-1">Quote or Create:</h3></div><div className="col-lg-1"></div>
                                </div>

                                <div className="row P_25_T">
                                    <div className="col-lg-1"></div>
                                    <div className="col-lg-3 col-md-5 col-xs-12">
                                        <div className="row">
                                            <div className="col-lg-6 col-md-5 AUD">
                                                <label>$59.70 <small>(AUD)</small></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="row P_25_T">
                                    <div className="col-lg-2 col-md-3 col-lg-offset-1 mb-3">
                                        <button className="but_submit" onClick={(e) => this.handleCreateShift(e, 3)}>Save Shift as Quote</button>
                                    </div>
                                    <div className="col-lg-2 col-md-3 col-lg-offset-6">
                                        <button className="but_submit" onClick={(e) => this.handleCreateShift(e, 1)}>Create Shift</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </section>
                </BlockUi>
            </div>
        );
    }
}
export default CreateShift
