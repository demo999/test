import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import ScheduleDashboard from '../../admin/schedule/ScheduleDashboard';

import { checkItsNotLoggedIn, postData, getQueryStringValue, changeTimeZone, handleDateChangeRaw, reFreashReactTable } from '../../../service/common.js';
import moment from 'moment';
import { RosterDropdown, AnalysisDropdown, unfilledShiftTypeFilterOption } from '../../../dropdown/ScheduleDropdown.js';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import Countdown from 'react-countdown-now';
import DatePicker from 'react-datepicker';

import ScheduleMenu from './ScheduleMenu';
import ScheduleHistory from './ScheduleHistory';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ManualMemberLookUp from './ManualMemberLookUp';
import { TotalShowOnTable } from '../../../service/TotalShowOnTable';
import { connect } from 'react-redux'
import SchedulePage from './SchedulePage';
import Pagination from "../../../service/Pagination.js";
import { ToastUndo } from 'service/ToastUndo.js'


const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {

        // request json
        var Request = {pageSize: pageSize, page: page, sorted: sorted, filtered: filtered};
        postData('schedule/ScheduleListing/get_unfilled_shifts', Request).then((result) => {
            let filteredData = result.data;

            const res = {
                rows: filteredData,
                pages: (result.count),
                total_count: (result.total_count),
            };
            resolve(res);
        });

    });
};


class ScheduleUnfilled extends Component {
    constructor(props) {
        super(props);
        this.state = {
            roster: 'roster',
            analysis: 'analysis',
            loading: false,
            shiftListing: [],
            counter: 0,
            selected: [],
            selectAll: 0,
            start_date: '',
            end_date: '',
            active_panel: 'unfilled'
        }
         this.reactTable = React.createRef();
    }
    
    closeModel = (key,status) => {
        if(status){
            reFreashReactTable (this,'fetchData');
        }
        this.setState({manual_assign: false})
    }

    fetchData = (state, instance) => {
        // function for fetch data from database
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered,
        ).then(res => {
            this.setState({
                shiftListing: res.rows,
                pages: res.pages,
                total_count: res.total_count,
                loading: false,
                selectAll: 0,
                userSelectedList: [],
                selected: []
            });
        });
    }

    moveToUnfilledOnApp = () => {
        var status = false;
        if (this.state.selectAll == 1) {
            status = true
        } else {
            var List = this.state.selected;
            Object.keys(List).forEach(function (key) {
                if (List[key]) {
                    status = true
                }
            });
        }

        toast.dismiss();
        if (status) {
            postData('schedule/ScheduleListing/move_to_app', this.state.selected).then((result) => {
                if (result.status) {
                    var tempfilter = { search_box: '', shift_type: '', shift_date: '', start_date: '', end_date: '', push_to_app: this.state.active_panel }
                    this.setState(tempfilter);
                    this.setState({ filtered: tempfilter })
                } else {
                    toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                    // toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }
            });
        } else {
            toast.error(<ToastUndo message={'Please select at least one shift'} showType={'e'} />, {
            // toast.error('Please select at least one shift', {
                position: toast.POSITION.TOP_CENTER,
                hideProgressBar: true
            });
        }
    }

    moveToAutoFill = () => {
        var status = false;
        if (this.state.selectAll == 1) {
            status = true
        } else {
            var List = this.state.selected;
            Object.keys(List).forEach(function (key) {
                if (List[key]) {
                    status = true
                }
            });
        }

        toast.dismiss();
        if (status) {
            this.setState({ autfill: true })
        } else {
            toast.error(<ToastUndo message={'Please select at least one shift'} showType={'e'} />, {
            // toast.error('Please select at least one shift', {
                position: toast.POSITION.TOP_CENTER,
                hideProgressBar: true
            });
        }
    }

    changeTabPanel = (panel) => {
        this.setState({ active_panel: panel }, () => {
            var tempfilter = { search_box: '', shift_type: '', shift_date: '', start_date: '', end_date: '', push_to_app: this.state.active_panel }
            this.setState(tempfilter);
            this.setState({ filtered: tempfilter })
        })

    }

    toggleRow = (id) => {
        const newSelected = Object.assign({}, this.state.selected);
        newSelected[id] = !this.state.selected[id];

        this.setState({
            selected: newSelected,
            selectAll: 2
        });
    }

    toggleSelectAll = () => {
        let newSelected = {};

        if (this.state.selectAll === 0) {
            this.state.shiftListing.forEach(x => {
                if (x.push_to_app != 2) {
                    newSelected[x.id] = true;
                }
            });
        }

        this.setState({
            selected: newSelected,
            selectAll: (this.state.selectAll === 0) ? 1 : 0
        });
    }

    searchBox = (key, value) => {
        var state = {}
        state[key] = value;
        this.setState(state, () => {
            var filter = { search_box: this.state.search_box, shift_type: this.state.shift_type, shift_date: this.state.shift_date, start_date: this.state.start_date, end_date: this.state.end_date, push_to_app: this.state.active_panel }
            this.setState({ filtered: filter });
        });
    }

    closeHistory = () => {
        this.setState({ open_history: false })
    }

    componentWillReceiveProps(nextProps){
          if(this.props.showTypePage =='' || this.props.showTypePage!=nextProps.showTypePage){
              if(nextProps.showTypePage=='app'){
                  this.changeTabPanel('unfilled_on_app');
              }else if(nextProps.showTypePage=='unfilled'){
                this.changeTabPanel('unfilled');
              }

          }
      }

    render() {


        const columns = [(this.state.active_panel == 'unfilled') ? {
            id: "checkbox", accessor: "",
            Cell: ({ original }) => {
                return (<span className="w_50 w-100  mt-2">
                    <input disabled={(original.push_to_app == 2) ? true : false} type='checkbox' className="checkbox1" checked={this.state.selected[original.id] === true} onChange={() => this.toggleRow(original.id)} />
                    <label>
                        <div className="d_table-cell"> <span onClick={() => original.push_to_app == 2 ? '' : this.toggleRow(original.id)}  ></span></div>
                    </label>
                </span>);
            },
            Header: x => {
                return (
                    <span className="w_50 w-100  mb-2">
                        <input type='checkbox' className="checkbox1" checked={this.state.selectAll === 1} ref={input => {
                            if(input){ input.indeterminate = this.state.selectAll === 2 }
                        }}
                            onChange={() => this.toggleSelectAll()} />
                        <label>
                            <div className="d_table-cell"> <span onClick={() => this.toggleSelectAll()}></span></div>
                        </label>
                    </span>
                );
            },
            sortable: false,

        } : { width: 0, headerStyle: { 'border': "0px solid #fff" } },
        { Header: 'ID', accessor: 'id', filterable: false, },
        { Header: 'Date', accessor: 'shift_date', filterable: false, Cell: props => <span>{changeTimeZone(props.original.start_time, "DD/MM/YYYY")}</span> },
        { Header: 'For', accessor: 'participantName', filterable: false, },
        { Header: 'Start', accessor: 'start_time', filterable: false, Cell: props => <span>{changeTimeZone(props.original.start_time, 'LT')}</span> },
        { Header: 'Duration', accessor: 'duration', filterable: false, },
        {
            Header: 'Suburb', accessor: '', sortable: false, filterable: false,
            Cell: props => <span>
                {(props.original.address.length > 0) ? props.original.address[0].suburb : "N/A"}
            </span>
        },
        {
            Cell: (props) => <span className="action_ix__">
              <i onClick={() => this.setState({manual_assign: true, selectShiftId: props.original.id})} className="fa icon-fillshift2-ie icon_h-1 mr-2 color"></i>
            <i onClick={() => this.setState({ historyId: props.original.id, open_history: true })} className="icon icon-pending-icons icon_h-1 mr-2"></i>
                <Link to={{ pathname: '/admin/schedule/details/' + props.original.id, state: this.props.props.location.pathname }}>
                    <i className="icon icon-views"></i></Link></span>, Header: <div className="">Action</div>, style: {
                        "textAlign": "right",
                    }, headerStyle: { border: "0px solid #fff" }, 
                    Header: <TotalShowOnTable countData={this.state.total_count} />,
                    sortable: false
        },
        {
            expander: true, sortable: false,
            Expander: ({ isExpanded, ...rest }) =>
                <div>{isExpanded ? <i className="icon icon-arrow-up"></i> : <i className="icon icon-arrow-down"></i>}</div>,
            headerStyle: { border: "0px solid #fff" },
        }]



        return (
            <div>

                <ScheduleDashboard />
                <SchedulePage pageTypeParms={this.props.props.match.params.page}/>

                {(this.state.autfill) ? <Redirect to={{ pathname: '/admin/schedule/fill_shift', state: this.state.selected }} /> : ''}
                <section className="manage_top">
                    <div className="container-fluid">
                        <ScheduleMenu back_url={'/admin/schedule/unfilled/unfilled'} default={true} landingPage={true} />

                        <div className="row">
                           
                            <div className="col-lg-10 col-sm-12 col-lg-offset-1">
                                <div className="tab-content">

                                    <div role="tabpanel" className="tab-pane active" id={this.state.active_panel}>
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="row">
                                                    <div className="col-md-12 P_7_TB"><h3>Shifts:</h3></div>
                                                    <div className="col-md-12"><div className="bor_T"></div></div>
                                                </div>
                                            </div>
                                        </div>


                                        <div className="row P_25_T">
                                            <div className="col-md-4 col-sm-8">
                                                <label>Search</label>
                                                <div className="table_search_new">
                                                    <input type="text" onChange={(e) => this.searchBox('search_box', e.target.value)} name="" value={this.state.search_box || ''} />
                                                    <button type="submit">
                                                        <span className="icon icon-search"></span>
                                                    </button>
                                                </div>
                                            </div>
                                            <div className="col-md-3 col-sm-4 mt-1">
                                                <label></label>
                                                <div className="box">
                                                    <Select clearable={false} name="shift_type" simpleValue={true} searchable={false} onChange={(e) => this.searchBox('shift_type', e)}
                                                        options={unfilledShiftTypeFilterOption(0)} value={this.state.shift_type} placeholder="Shift Type/Department" />

                                                </div>
                                            </div>

                                            <div className="col-md-5 col-sm-12">
                                                <div className="row">
                                                    <div className="col-sm-4">
                                                        <label>On</label>
                                                        <DatePicker autoComplete="new-password" onChangeRaw={handleDateChangeRaw}  minDate={moment()} utcOffset={0} isClearable={true} name="shift_date" onChange={(e) => this.searchBox('shift_date', e)} selected={this.state['shift_date'] ? moment(this.state['shift_date'], 'DD-MM-YYYY') : null} dateFormat="DD-MM-YYYY" className="text-center px-0" placeholderText="00/00/0000" />
                                                    </div>
                                                    <div className="col-sm-4">
                                                        <label>From</label>
                                                        <DatePicker autoComplete="new-password" onChangeRaw={handleDateChangeRaw}  minDate={moment()} utcOffset={0} isClearable={true} name="start_date" onChange={(e) => this.searchBox('start_date', e)} selected={this.state['start_date'] ? moment(this.state['start_date'], 'DD-MM-YYYY') : null} className="text-center px-0" placeholderText="00/00/0000" />

                                                    </div>
                                                    <div className="col-sm-4">
                                                        <label>To</label>
                                                        <DatePicker autoComplete="new-password" onChangeRaw={handleDateChangeRaw}  minDate={moment()} utcOffset={0} isClearable={true} name="end_date" onChange={(e) => this.searchBox('end_date', e)} selected={this.state['end_date'] ? moment(this.state['end_date'], 'DD-MM-YYYY') : null} className="text-center px-0" placeholderText="00/00/0000" />

                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div className="row">
                                            <div className="col-md-12 schedule_listings">
                                                <ReactTable
                                                  PaginationComponent={Pagination}
                                                    ref={this.reactTable}
                                                    columns={columns}
                                                    manual
                                                    data={this.state.shiftListing}
                                                    pages={this.state.pages}
                                                    loading={this.state.loading}
                                                    onFetchData={this.fetchData}
                                                    filtered={this.state.filtered}
                                                    defaultFiltered={{ push_to_app: 'unfilled' }}
                                                    defaultPageSize={10}
                                                    className="-striped -highlight"
                                                    noDataText="No Record Found"
                                                    minRows={2}
                                                    
                                                    previousText={<span className="icon icon-arrow-left privious"></span>}
                                                    nextText={<span className="icon icon-arrow-right next"></span>}

                                                    SubComponent={(props) => <div className="other_conter"><div className="col-md-6">
                                                        <ul>

                                                            <li><span className="color">End: </span> {changeTimeZone(props.original.end_time, 'LT')}</li>
                                                            {props.original.address.map((site, id) => (
                                                                <li key={id + 1}><span className="color">Site: </span>{site.site}</li>
                                                            ))}

                                                            {(props.original.memberNames.length > 0) ? props.original.memberNames.map((memberName, id) => (
                                                                <li key={id + 1}><span className="color">preferred Member: </span>{memberName.memberName}</li>
                                                            )) : <li><span className="color">Preferred Member: </span>N/A</li>}
                                                        </ul>
                                                    </div>
                                                        <div className="col-md-6 text-right">
                                                            <ul>
                                                                <li><span className="color">Expenses: </span> {props.original.expenses ? '$' + props.original.expenses : 'N/A'}</li>
                                                                <li><span className="color">KMs: </span>15 km</li>
                                                                <li><span className="start_in_color">Start In: </span> <Countdown date={Date.now() + props.original.diff} /></li>
                                                            </ul>
                                                        </div>
                                                    </div>}
                                                />

                                            </div>
                                        </div>

                                        <ScheduleHistory open_history={this.state.open_history} shiftId={this.state.historyId} closeHistory={this.closeHistory} />
                                        {(this.state.active_panel == 'unfilled') ? <div className="row text-right">
                                            <button onClick={this.moveToAutoFill} className="default_but_remove">
                                                <i className="icon icon-circule update_button_g mr-2"></i></button>
                                            <button onClick={this.moveToUnfilledOnApp} className="default_but_remove" >
                                                <i className="icon icon-mobile update_button_g"></i></button>
                                        </div> : ''}
                                        
                                        <ManualMemberLookUp modal_show={this.state.manual_assign} closeModel={this.closeModel} shiftId={this.state.selectShiftId} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    showPageTitle: state.ScheduleDetailsData.activePage.pageTitle,
    showTypePage: state.ScheduleDetailsData.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {
       
    }
}
export default connect(mapStateToProps, mapDispatchtoProps)(ScheduleUnfilled);
