import React, { Component } from 'react';
import {  Link } from 'react-router-dom';
import { connect } from 'react-redux'

class InternalNavigation extends Component {
    constructor(props, context) {
        super(props, context);
    }
    
    total_count = () => {
        var count = 0;
        if(this.props.team_count || this.props.department_count){
            if(this.props.team_count){
                    Object.keys(this.props.team_count).forEach((key) => {
                    var tmp_cnt = parseInt(this.props.team_count[key]);    
                    tmp_cnt = (tmp_cnt == NaN) ? 0: tmp_cnt
                    count = count + tmp_cnt;
                }); 
            }
            if(this.props.department_count){
                    Object.keys(this.props.department_count).forEach((key) => {
                    var tmp_cnt = parseInt(this.props.department_count[key]);    
                    tmp_cnt = (tmp_cnt == NaN) ? 0: tmp_cnt
                    count = count + tmp_cnt;
                }); 
            }
            if(count > 0)
            return <span className="mess_V1_noti pull-right">{count}</span>;
        }
    }
    
    inboxCount = () => {
          if(this.props.internal_imail_count){
               return <span className="mess_V1_noti pull-right">{this.props.internal_imail_count}</span>;
          }
        
    }
  
    
    render() {
        return (
                <aside className="col-lg-2 col-md-3 P_25_T">
                    <ul className="side_menu">
                        <li><Link className={(this.props.active == "inbox")? 'active': ''} to="/admin/imail/internal/inbox">Inbox {this.inboxCount()}</Link></li>
                        <li><Link className={(this.props.active == "draft")? 'active': ''} to="/admin/imail/internal/draft">Drafts </Link></li>
                        <li><Link className={(this.props.active == "group_message")? 'active': ''} to="/admin/imail/internal/group_message">Group Messages {this.total_count()}</Link>
                        
                        </li>
                        <li><Link className={(this.props.active == "archive")? 'active': ''} to="/admin/imail/internal/archive">Archived </Link></li>
                    </ul>
                </aside>
         );
    }
}

const mapStateToProps = (state) => {  return {
    team_count: state.NotificationReducer.team_count,
    department_count: state.NotificationReducer.department_count,
    internal_imail_count: state.NotificationReducer.internal_imail_count,
}}

export default connect(mapStateToProps)(InternalNavigation)  
