import React, { Component } from 'react';

import Select from 'react-select-plus';
import { postData, postImageData,  handleChangeSelectDatepicker,  handleRemoveShareholder } from '../../../../service/common.js';
import jQuery from "jquery";
import {  toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux'
import { replyMail, getInternalMailListing, getInternalMailContent} from '../actions/InternalImailAction';
import {   OverlayTrigger,  Tooltip } from 'react-bootstrap';
import 'react-block-ui/style.css';
import BlockUi from 'react-block-ui';
import { ToastUndo } from 'service/ToastUndo.js'



const getOptionsAdmin = (e, previousSelected) =>  {
     if (!e) {
        return Promise.resolve({ options: [] });
    }
    return postData('imail/Internal_imail/get_composer_admin_name',{search: e, previous_selected: previousSelected})
    .then((response) => {
      return { options: response };
    });
}

class ComposeNewMessage extends Component {
    constructor(props) {
        super(props);
        this.allowExtentsion = ['jpg','jpeg','png','xlx','xls','doc','docx','pdf','txt','csv','odt', 'rtf'];
        
        this.state = {
            attachments: [],
            to_user: [],
            cc_user: [],
            title: '',
            content: '',
            is_priority: false,
            forword_attachments: [],
            loading: false,
            open_draft: false,
            messageId: false,
            contentId: false,
        }
    }
    
    extensionError = () => {
         var error = <p>Sorry we are only supported <br/> jpg, jpeg, png, xlx, xls, doc, docx, pdf, odt, rtf</p>
          toast.error(error, {
               position: toast.POSITION.TOP_CENTER,
               hideProgressBar: true
         });
    }

    selectAttchment = (event) => {
        var attachments = this.state.attachments;
        var error = false;
        var tempState = event.target.files
        Object.keys(tempState).map((key, index) => {
            var ext = tempState[key].name.replace(/^.*\./, '');
            
            if(this.allowExtentsion.includes(ext)){
                attachments[(attachments.length+1)] = tempState[key];
            } else{
               error= true
            }
        });
        
        if(error){
            this.extensionError();
        }
        
        this.setState({ attachments: attachments });
    }
    
    handleRemoveShareholder(obj, e, index, stateName) {
            e.preventDefault();
            var state = {};
            var List = obj.state[stateName];
            List[index]['removed'] = true;
            state[stateName] = List;
            obj.setState(state);
        }
    
    componentDidMount() {
        if(this.props.reply_mail == true){
            this.resetSendMail();
            this.setState(this.props);
            this.getPrefilledData(this.props);
        }
    }
    
    getPrefilledData = (data) => {
        postData('imail/Internal_imail/get_mail_pre_filled_data', data).then((result) => {
               if (result.status) {
                    this.setState(result.data);
               }
        });
    }

    resetSendMail = () => {
        this.setState({ attachments: [], to_user: [], cc_user: [], title: '', content: '', is_priority: false });
    }

    composeMail = (e, type) => {
        e.preventDefault();

        jQuery('#compose_mail').validate();
        if (jQuery('#compose_mail').valid()) {
            this.setState({loading: true});
            const formData = new FormData()
            this.state.attachments.map((val, index) => {
                formData.append('attachments[]', val);
            })

            formData.append('to_user', JSON.stringify(this.state.to_user));
            formData.append('cc_user', JSON.stringify(this.state.cc_user));
            formData.append('title', this.state.title);
            formData.append('content', this.state.content);
            formData.append('submit_type', type);
            
            formData.append('contentId', this.state.contentId);

            formData.append('is_priority', this.state.is_priority);
            formData.append('forword_attachments', JSON.stringify(this.state.forword_attachments));
                            
            if(this.props.replyMailId){
                formData.append('messageId', this.props.replyMailId);
            }

            postImageData(this.props.submitUri, formData).then((result) => {
                if (result.status) {
                    var message = (type == "is_draft")? "Save as draft successfully.": "Send mail successfully.";
                    
 toast.success(<ToastUndo message={message} showType={'s'} />, {
                    // toast.success(message, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    
                    this.props.getInternalMailListing({type: this.props.mail_type});
                    
                    if(this.props.replyMailId){
                         this.props.replyMail(false, '');
                         this.props.getInternalMailContent({messageId: this.props.replyMailId, type: this.props.mail_type});
                         this.resetSendMail();
                    }else{
                        this.resetSendMail();
                    }
                }else{
                    toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                    // toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }
                this.setState({loading: false});
            });
        }
    }

    render() {
        return ( <BlockUi tag="div" blocking={this.state.loading}>
                <div className="Internal_M_D_3">
            <div className="mess_V1_1 pull-left pt-4">
                <form id="compose_mail">
                    <div className="by-1 py-2 col-md-12 mb-2">
                        <h3 className="pull-left color">{this.props.compose_title}</h3>
                        <a onClick={this.props.closeCompose}><i className="icon icon-cross-icons-1 color pull-right"></i></a>
                        </div>
                    <div className="compose_scroll">
                    <div className="col-md-12 px-0 my-2 search_icons_right modify_select">
                        <Select.Async multi={true}
                            clearable={false}
                            name="form-field-name" required={true}
                            value={this.state.to_user}
                            loadOptions={(e) => getOptionsAdmin(e, this.state.cc_user)}
                            placeholder='To:'
                            onChange={(e) => handleChangeSelectDatepicker(this, e, 'to_user')}
                            className="default_validation"
                            chache={false}
                        />
                    </div>
                    <div className="col-md-12 px-0 my-2 search_icons_right modify_select">
                        <Select.Async multi={true}
                            chache={false}
                            clearable={false}
                            name="form-field-name"
                            value={this.state.cc_user}
                            loadOptions={(e) => getOptionsAdmin(e, this.state.to_user)}
                            placeholder='CC:'
                            onChange={(e) => handleChangeSelectDatepicker(this, e, 'cc_user')}
                            className="default_validation"
                        />
                    </div>
                    <div className="col-md-12 px-0 my-2">
                        <input type="text" placeholder="Subject:" disabled={this.state.titleFixed} name="subject" onChange={(e) => this.setState({ title: e.target.value })} value={this.state.title} data-rule-required="true" />
                    </div>
                    <div className="col-md-12 px-0 my-2">
                        {this.state.attachments.map((val, index) => (
                            <div key={index + 1} className="attach_txt"><span>{val.name}</span><i onClick={(e) => handleRemoveShareholder(this, e, index, 'attachments')} className="icon icon-cross-icons-1"></i> </div>
                        ))}
                        {this.state.forword_attachments.map((val, index) => {
                             if(!val.removed){
                                return <div key={index + 1} className="attach_txt"><span>{val.filename}</span><i onClick={(e) => this.handleRemoveShareholder(this, e, index, 'forword_attachments')} className="icon icon-cross-icons-1"></i> </div>
                            }
                        })}
                    </div>

                    <div className="col-md-12 Imail_btn_left_v1 px-0 py-3">
                        <i><span className="upload_btn">
                            <label className="btn btn-default btn-sm center-block btn-file">
                            <OverlayTrigger  placement="bottom" overlay={<Tooltip id='selectAttchment' className="MSG_Tooltip"  placement={'top'}>Attach File</Tooltip>}><i className="icon icon-attach-im attach_im mr-0"></i></OverlayTrigger>
                                <input className="p-hidden" multiple type="file" onChange={this.selectAttchment} name="attachment" value="" />
                            </label>
                        </span>
                        </i>

                
                    <OverlayTrigger  placement="bottom" overlay={<Tooltip  id="is_priority" className="MSG_Tooltip"  placement={'top'}>High Priority</Tooltip>}><i onClick={() => this.setState({is_priority: (this.state.is_priority)? false: true})}   className={"icon icon-warning-im warning-im Opacity_03"+((this.state.is_priority)? 'active' : '')}></i></OverlayTrigger>
                    </div>
                    <div className="col-md-12 px-0 int_text_but">
                        <textarea className="w-100 textarea_height_3 textarea-max-size" name="mail" onChange={(e) => this.setState({ content: e.target.value })} value={this.state.content} placeholder="Your Message here" data-rule-required="true"></textarea>
                        <div className="Imail_btn_right_v1 int_text_but_v1">
                          
                       <button className="default_btn" disabled={this.state.loading} onClick={(e) => this.composeMail(e, 'is_draft')}><OverlayTrigger  placement="bottom" overlay={<Tooltip id='is_draft'  className="MSG_Tooltip"  placement={'top'}>Draft</Tooltip>}><i  className="icon icon-document5-ie"></i></OverlayTrigger></button>
                       <button className="default_btn" disabled={this.state.loading} onClick={(e) => this.composeMail(e, 'is_send')}><OverlayTrigger  placement="bottom" overlay={<Tooltip  id='is_send' className="MSG_Tooltip"  placement={'top'}>Send</Tooltip>}><i className="icon icon-share-icon"></i></OverlayTrigger></button>
                        </div>
                    </div>
                    </div>
                </form>
            </div>
        </div>
        </BlockUi>
        );
    }
}

const mapDispatchtoProps = (dispatch) => {
       return {
           getInternalMailListing : (request) => dispatch(getInternalMailListing(request)),
           getInternalMailContent : (request) => dispatch(getInternalMailContent(request)),
           replyMail: (status ,messageId) => dispatch(replyMail(status ,messageId)),
      }
}

    
const mapStateToProps = state => ({
     reply_mail : state.InternalImailReducer.reply_mail,
     replyMailId : state.InternalImailReducer.replyMailId,
     mail_type : state.InternalImailReducer.mail_type,
})

export default connect(mapStateToProps, mapDispatchtoProps)(ComposeNewMessage)    