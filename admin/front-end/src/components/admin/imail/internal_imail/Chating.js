import React from 'react';
import { connect } from 'react-redux'
import Websocket from 'react-websocket';
import moment from 'moment';
import { getLoginToken, getPinToken, postData } from '../../../../service/common.js';
import jQuery from "jquery";

import axios from 'axios';
import { BASE_URL, WS_URL } from '../../../../config.js';
import { getReadableFileSizeString } from '../../../../dropdown/imailDropdown.js';
import { toast } from 'react-toastify';
import { setGroupChatData, addNewGroupMessage, setGroupMessageAsRead, setCurrentTeamIdAndType } from '../actions/InternalImailAction';
import { decreaseGroupMessageCounter } from './../../notification/actions/NotificationAction';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { groupChatingLoder } from '../../../../service/CustomContentLoader.js';

import CircularProgressbar from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import GroupMemberView from './GroupMemberView';

class Chating extends React.Component {
    constructor(props) {
        super(props);
        this.allowExtentsion = ['jpg', 'jpeg', 'png', 'xlx', 'xls', 'doc', 'docx', 'pdf', 'csv','odt', 'rtf'];

        this.state = {
            chat_box: [],
            adminId: false,
            loading: true,
            reconnect: false,
            progress_loader: [],
            webscoket_status: false,
            request_queue: [],
            groupMember: []
        }
    }


    onEnterPress = (e) => {
        if (e.keyCode === 13 && e.shiftKey === false) {
            e.preventDefault();
            this.sendMessage();
        }
    }
    
    componentWillUnmount(){
        this.props.setCurrentTeamIdAndType(false, false);
    }

    
    componentDidMount() {
        this.getGroupChating(this.props);
        this.props.setCurrentTeamIdAndType(this.props.teamId, this.props.team_type);
    }

    componentWillReceiveProps(newProps) {
        if (newProps.teamId !== this.props.teamId || (newProps.teamId === this.props.teamId && newProps.team_type !== this.props.team_type)) {
            this.getGroupChating(newProps);
            this.props.setCurrentTeamIdAndType(newProps.teamId, newProps.team_type);
        }
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    markAsReead = (messageId, index) => {
        this.notifyMessageReaded(messageId);
        this.props.setGroupMessageAsRead(index);

        this.props.decreaseGroupMessageCounter(this.props.teamId, this.props.team_type);
    }
    
    getGroupChating = (props) => {
        this.setState({ loading: true })
        postData('imail/Internal_imail/get_group_chating', {teamId: props.teamId, team_type: props.team_type}).then((result) => {
               if (result.status) {
                     this.props.setGroupChatData(result.data);
                     this.setState({ adminId: result.adminId, loading: false});
                     this.bulkMessageMarkAsRead(result.data);
               }
        });
    }

    scrollToBottom = () => {
        var msgBox = jQuery('#gr_chat');
        if (msgBox.length > 0)
            msgBox[0].scrollTop = msgBox[0].scrollHeight;
    }

    notifyMessageReaded = (messageIds) => {
        var obj = { req_type: 'single_group_chat', ms_type: 'read_notify', token: getLoginToken(), messageId: messageIds }

        if (this.props.socketObj) {
            this.props.socketObj.sendMessage(JSON.stringify(obj));
        }
    }

    bulkMessageMarkAsRead = (data) => {
        var tempIds = [];
        data.map((val, index) => {
            if (val.status == 1) {
                tempIds[index] = val.id;
            }
        })

        if (tempIds.length > 0) {
            this.notifyMessageReaded(tempIds);
        }
    }

    sendMessage = () => {
        var msg = this.state.message;
        if (msg) {
            this.setState({ message: ''})
            var obj = { req_type: 'single_group_chat', ms_type: 'new_message', message_type: 1, message: msg, token: getLoginToken(), ty: this.props.team_type, tm: this.props.teamId }

            this.props.socketObj.sendMessage(JSON.stringify(obj));
        }
    }

    postAttachment = (url, data, index) => {
        data.append('pin', getPinToken());
        data.append('token', getLoginToken());

        return new Promise((resolve, reject) => {
            axios.post(BASE_URL + url, data, {
                onUploadProgress: progressEvent => {
                    var LIST = this.state.progress_loader;
                    LIST[index]['progress'] = progressEvent.loaded / progressEvent.total;
                    this.setState({ progress_loader: LIST });
                }
            }).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
                console.error(error);
            });
        });
    }

    extensionError = () => {
        var error = <p>Sorry we are only supported <br /> jpg, jpeg, png, xlx, xls, doc, docx, pdf, odt, rtf</p>
        toast.error(error, {
            position: toast.POSITION.TOP_CENTER,
            hideProgressBar: true
        });
    }
    
    getGroupMember = () => {
        postData('imail/Internal_imail/get_team_member', {teamId: this.props.teamId}).then((result) => {
               if (result.status) {
                    this.setState({groupMember: result.data})
               }
        });
    }

    uploadAttachment = (e) => {
        e.preventDefault();

        var files = e.target.files;
        var error = false;
        var progress_loader = [];
        Object.keys(files).map((key, index) => {
            var ext = files[key].name.replace(/^.*\./, '');

            if (this.allowExtentsion.includes(ext)) {
                progress_loader[index] = { message: files[key].name, progress: 0, temp: true, senderId: this.state.adminId, file: files[key] };
            } else {
                error = true
            }

        });

        if (error) {
            this.extensionError();
        }

        this.setState({ progress_loader: progress_loader }, () => {
            this.state.progress_loader.map((val, index) => {
                const formData = new FormData();

                formData.append('attachments', val.file);
                formData.append('ty', this.props.team_type);
                formData.append('tm', this.props.teamId);

                this.postAttachment('imail/Internal_imail/upload_group_chat_attachment', formData, index).then((response) => {
                    if (response.status) {
                        var ret = this.getData({ message: response.filename });
                        this.props.socketObj.sendMessage(ret);
                    }
                })
            });
        })
    }
    
    closeMemberView = () => {
        this.setState({openViewMember: false});
    }

    getData = (obj) => {
        var send_var = {
            req_type: 'single_group_chat', ms_type: 'new_message', message_type: 2, message: '',
            token: getLoginToken(), ty: this.props.team_type, tm: this.props.teamId
        }

        var mergess = { ...send_var, ...obj };
        return (JSON.stringify(mergess));
    }


    render() {
        return (
            <div className="Internal_M_D_3 mess_Teamchating">
                <div className="mess_V1_1 pull-left pt-4  w-100">

                    <div className="by-1 py-2 col-md-12">
                        <h3 className="pull-left color">{this.state.team_name}</h3>
                        <i className="icon icon-cross-icons-1 color pull-right"></i>
                    </div>
                    <div className="clearfix"></div>
                    {this.props.team_type === 'team'? <div onClick={() => this.setState({openViewMember: true, groupMember: []}, () => (this.getGroupMember()) )} className="mess_Cht_1 bb-1">View  Members in Group <i className="icon icon-notes"></i>
                    </div>: <React.Fragment/>}
                    
                    <div className="Gro_scroll_mess">
                        <div className="custom_scolling px-3" id="gr_chat">
                            <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={groupChatingLoder()} ready={!this.state.loading}>
                                {this.props.group_chat.map((val, index) => (
                                    (val.senderId == this.state.adminId) ?
                                        <SenderSection {...val} key={index + 1} />
                                        :
                                        <ReceiverSection {...val} key={index + 1} index={index} notifyMessageReaded={this.notifyMessageReaded} markAsReead={this.markAsReead} />
                                ))}

                                {this.state.progress_loader.map((val, index) => (
                                    <UploadFileProgress {...val} key={index + 1} />
                                ))}
                            </ReactPlaceholder>
                        </div>
                    </div>

                  
                    <div className="col-md-12 px-0 int_text_but">
                        <textarea onChange={(e) => this.setState({ message: e.target.value })} value={this.state.message} className="w-100 textarea_height_3 textarea-max-size"
                            onKeyDown={this.onEnterPress} placeholder="Your Message here"></textarea>
                        <div className="Imail_btn_right_v1 int_text_but_v1">
                            <div className="col-md-12 Imail_btn_left_v1 px-0 py-3">
                                <i><span className="upload_btn">
                                    <label className="btn btn-default btn-sm center-block btn-file">
                                        <OverlayTrigger placement="bottom" overlay={<Tooltip id="uploadAttachment" className="MSG_Tooltip" placement={'top'}>Add attachment</Tooltip>}><i className="icon icon-attach-im attach_im px-0 mx-0"></i></OverlayTrigger>
                                        <input className="p-hidden" multiple type="file" onChange={this.uploadAttachment} name="attachment" value="" />
                                    </label>
                                </span>
                                </i>
                                <OverlayTrigger placement="bottom" overlay={<Tooltip id='Send' className="MSG_Tooltip" placement={'top'}>Send</Tooltip>}><i onClick={this.sendMessage} className="icon icon-share-icon"></i></OverlayTrigger>
                            </div>
                        </div>
                    </div>
                </div>
                
                <GroupMemberView groupMember={this.state.groupMember} openModel ={this.state.openViewMember} closeModel={this.closeMemberView} />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    group_chat: state.InternalImailReducer.group_chat,
    socketObj: state.Permission.socketObj,
})

const mapDispatchtoProps = (dispatch) => {
    return {
        setGroupChatData: (data) => dispatch(setGroupChatData(data)),
        addNewGroupMessage: (data) => dispatch(addNewGroupMessage(data)),
        setGroupMessageAsRead: (messageId, index) => dispatch(setGroupMessageAsRead(messageId, index)),
        decreaseGroupMessageCounter: (teamId, type) => dispatch(decreaseGroupMessageCounter(teamId, type)),
        setCurrentTeamIdAndType: (teamId, team_type) => dispatch(setCurrentTeamIdAndType(teamId, team_type)),
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(Chating)


class ReceiverSection extends React.Component {
    render() {
        if (this.props.message_type == 1) {
            var msg = this.props.message;
        } else {
            var msg = <a href={this.props.attach_uri} download target="_blank|_self|_parent|_top|framename"><i className="icon icon-document3-ie"></i><small>{this.props.message}</small></a>
        }

        if (this.props.status == 1) {
            this.props.markAsReead(this.props.id, this.props.index);
        }
        return (
            <div className={'row pt-3'}>
                <div className={'col-md-7'}>
                    <div className="Group_v1">
                        <div className="Group_v1_b">
                            <div className="mess_vn_in_1">
                                <div className="mess_V_1">
                                    <span><img src={this.props.user_img} /></span>
                                </div>
                                <div className="mess_V_2">
                                    <div className="mess_V_a">
                                        <div className="mess_V_a1">{this.props.user_name}</div>
                                    </div>
                                    <div className="mess_V_b">{moment(this.props.created).format('DD/MM/YYYY LT')}</div>
                                </div>
                            </div>
                            <div className="Group_v1_a Group_F_dow"> {msg} </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

class SenderSection extends React.Component {
    render() {
        if (this.props.message_type == 1) {
            var msg = this.props.message;
        } else {
            var msg = <a href={this.props.attach_uri} download target="_blank|_self|_parent|_top|framename"><i className="icon icon-document3-ie"></i><small>{this.props.message}</small></a>
        }
        return (
            <div className="row pt-3 Group_replay">
                <div className="col-md-7 col-md-offset-5">
                    <div className="Group_v1">
                        <div className="Group_v1_b">
                            <div className="mess_vn_in_1">
                                <div className="mess_V_2">
                                    <div className="mess_V_b">{moment(this.props.created).format('DD/MM/YYYY LT')}</div>
                                    <div className="mess_V_a">
                                        <div className="mess_V_a1">{this.props.user_name}</div>
                                    </div>
                                </div>
                                <div className="mess_V_1">
                                    <span><img src={this.props.user_img} /></span>
                                </div>
                            </div>
                            <div className="Group_v1_a Group_F_dow">{msg}</div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

class UploadFileProgress extends React.Component {
    render() {

        if (this.props.progress >= 1) {
            return false;
        }
        return (
            <div className="row pt-3 Group_replay">
                <div className="col-md-7 col-md-offset-5">
                    <div className="Group_v1_a">
                        <div className="Upload_file_box_1">
                            <div className="Upload_FN">{this.props.file.name}</div>
                            <div className="Upload_FS">{getReadableFileSizeString(this.props.file.size)}</div>
                            <div className="w-100 text-right">
                                <div style={{ width: '70px', display: 'inline-block' }}>
                                    <CircularProgressbar
                                        percentage={Math.trunc(this.props.progress * 100)}
                                        text={`${Math.trunc(this.props.progress * 100)}%`}
                                        strokeWidth={7}
                                        styles={{
                                            path: { strokeLinecap: 'butt', stroke: 'var(--bg-color)', transition: 'stroke-dashoffset 0.5s ease 0s', },
                                            trail: { stroke: '#777', },
                                            text: { fill: 'var(--bg-color)', fontSize: '26px', fontWeight: '600', },
                                        }}
                                    />
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

