import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { postData } from '../../../../service/common.js';
import { connect } from 'react-redux'
import { getInternalMailContent, updateMailContentValue, updateMailListingValue, replyMail, removeMailFromListing, setMessageAllContent } from '../actions/InternalImailAction';
import moment from 'moment';
import jQuery from "jquery";
import ComposeNewMessage from './ComposeNewMessage';
import { downloadAttachment } from '../../../../dropdown/imailDropdown.js';

import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";

import { imailDetailsLoder2, imailDetailsLoder1 } from '../../../../service/CustomContentLoader.js';
import {  OverlayTrigger, Tooltip } from 'react-bootstrap';

class InternalDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            is_redirect: false,
            is_read: 1,
        }
    }

    componentDidMount() {
        if (this.props.mail.id != this.props.messageId) {
            if(this.props.re === 're' && this.props.contentId){
                 this.replyMail(this.props.contentId);
            }else{
                this.getAllInternalMail(this.props.messageId);
                
            }   
        }
    }
  
    componentDidUpdate() {
        this.scrollToBottom();
    }

    scrollToBottom = () => {
        var msgBox = jQuery('#main_internal_details');
        if (msgBox.length > 0)
            msgBox[0].scrollTop = msgBox[0].scrollHeight;
    }


    addFavourite = (val) => {
        var fav = (this.props.mail.is_fav == 1) ? 0 : 1;
        postData('imail/Internal_imail/add_to_favourite', { messageId: this.props.mail.id, is_fav: fav }).then((result) => {
            if (result.status) {
                var updateData = { is_fav: fav };
                this.props.updateMailContent(updateData);
            }
            this.setState({ loading: false });
        });
    }

    addToBlock = (val) => {
        var block = (this.props.mail.is_block == 1) ? 0 : 1;
        postData('imail/Internal_imail/add_to_block', { messageId: this.props.mail.id, is_block: block }).then((result) => {
            if (result.status) {
                var updateData = { is_block: block };
                this.props.updateMailContent(updateData);
                this.props.updateMailListingValue(updateData, this.props.mail.id);
            }
            this.setState({ loading: false });
        });
    }

    addToFlage = (val) => {
        var flage = (this.props.mail.is_flage == 1) ? 0 : 1;
        postData('imail/Internal_imail/add_to_flage', { messageId: this.props.mail.id, is_flage: flage }).then((result) => {
            if (result.status) {
                var updateData = { is_flage: flage };
                this.props.updateMailContent(updateData);
                this.props.updateMailListingValue(updateData, this.props.mail.id);
            }
            this.setState({ loading: false });
        });
    }

    addToArchive = (val) => {
        var archive = (this.props.mail.archive == 1) ? 0 : 1;
        postData('imail/Internal_imail/add_to_archive', { messageId: this.props.mail.id, archive: archive }).then((result) => {
            if (result.status) {
                this.props.removeMailFromListing(this.props.mail.id);
                this.setState({ is_redirect: true });
                this.props.setMessageAllContent({ content: [] });
            }
            this.setState({ loading: false });
        });
    }

    replyAllMail = (contentId) => {
         this.setState({messageId: this.props.messageId, contentId, contentId, action_type: 'reply_all', submitUri : 'imail/Internal_imail/reply_mail', compose_title : 'Compose Mail'}, () => {
              this.props.replyMail(true, this.props.messageId);
         })
    }

    replyMail = (contentId) => {
         this.setState({messageId: this.props.messageId, contentId, contentId, action_type: 'reply', submitUri : 'imail/Internal_imail/reply_mail', compose_title : 'Compose Mail'}, () => {
              this.props.replyMail(true, this.props.messageId);
         })
    }
    
    draftMailOpen = (contentId) => {
         this.setState({messageId: this.props.messageId, contentId, contentId, action_type: 'open_draft', submitUri : 'imail/Internal_imail/send_draft_mail', compose_title : 'Compose Mail'}, () => {
              this.props.replyMail(true, this.props.messageId);
         })
    }

    forwordMail = (contentId, content, attachments) => {
         this.setState({messageId: this.props.messageId, contentId, contentId, action_type: 'forword_mail', submitUri : 'imail/Internal_imail/compose_new_mail', compose_title : 'Compose Mail'}, () => {
              this.props.replyMail(true, this.props.messageId);
         })
    }

    readUnReadMail = (val) => {
        var action = (this.props.mail.is_read == 1)? 0 : 1;
        postData('imail/Internal_imail/mark_as_read_unread', { messageId: this.props.mail.id, action: action }).then((result) => {
            if (result.status) {
                var updateData = {is_read: action}
                this.props.updateMailContent(updateData);
            }
        });
    }

    getAllInternalMail = (messageId) => {
          this.props.getInternalMailContent({ messageId: messageId, type: this.props.mail_type });
    }

    closeCompose = () => {
        this.props.replyMail(false, '');
        this.getAllInternalMail(this.props.messageId);
    }

    render() {
        return (<Aux>
            {this.state.is_redirect ? <Redirect to={'/admin/imail/internal/'+this.props.mail_type} /> : ''}

            {this.props.reply_mail ?
                <ComposeNewMessage closeCompose={this.closeCompose}
                    submitUri={this.state.submitUri}
                    compose_title={this.state.compose_title}
                    messageId={this.state.messageId}
                    contentId={this.state.contentId}
                    action_type={this.state.action_type}
                />
                :
                <div className="Internal_M_D_2">
                    <div className="mess_V1_1">
                        <div className="mess_div_VB">
                            <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={imailDetailsLoder1()} ready={!this.props.is_fetching}>
                                <div className="mess_hadding_V1 px-4">

                                    <div className="row py-4">
                                        <div className="col-md-6 Imail_btn_left_v1">
                                        </div>
                                        {(this.props.mail_type === 'inbox' || this.props.mail_type === 'draft') ?
                                            <div className="col-md-6 Imail_btn_right_v1 text-right parant_active">
                                                    {(this.props.mail_type === 'inbox')?
                                                    <React.Fragment>
                                                    <OverlayTrigger placement="bottom" overlay={<Tooltip id="readUnReadMail" className="MSG_Tooltip" placement={'top'}>Mark as {(this.props.mail.is_read == 1) ? "unread" : 'read'}</Tooltip>}>
                                                        <i onClick={this.readUnReadMail} className={"icon icon-envelope-im " + ((this.props.mail.is_read == 1) ? "active" : '')}></i>
                                                    </OverlayTrigger>
                                                    <OverlayTrigger placement="bottom" overlay={<Tooltip id="addFavourite" className="MSG_Tooltip" placement={'top'}>Set as {(this.props.mail.is_fav == 1) ? "unfavorite" : 'favorite'}</Tooltip>}>
                                                        <i onClick={this.addFavourite} className={"icon icon-favorite-im " + ((this.props.mail.is_fav == 1) ? "active" : '')}></i>
                                                    </OverlayTrigger>
                                                    <OverlayTrigger placement="bottom" overlay={<Tooltip id="addToFlage" className="MSG_Tooltip" placement={'top'}>Set {(this.props.mail.is_flage == 1) ? "unflag" : 'flag'} of imail</Tooltip>}>
                                                        <i onClick={this.addToFlage} className={"icon icon-flag-im " + ((this.props.mail.is_flage == 1) ? "active" : '')}></i>
                                                    </OverlayTrigger>
                                                    <OverlayTrigger placement="bottom" overlay={<Tooltip id="addToBlock" className="MSG_Tooltip" placement={'top'}>{(this.props.mail.is_block == 1) ? "Unblock" : 'Block'} imail</Tooltip>}>
                                                        <i onClick={this.addToBlock} className={"icon icon-block-im " + ((this.props.mail.is_block == 1) ? "active" : '')}></i>
                                                    </OverlayTrigger>
                                                    </React.Fragment>: ''}
                                               
                                                    <OverlayTrigger placement="bottom" overlay={<Tooltip id="addToArchive" className="MSG_Tooltip" placement={'top'}>Archive</Tooltip>}>
                                                        <i onClick={this.addToArchive} className={"icon icon-archive-im " + ((this.props.mail.archive == 1) ? "active" : '')}></i>
                                                    </OverlayTrigger>
                                            </div> : ''}
                                        </div>
                                    <div className="col-md-12 bb-1">
                                    </div>
                                </div>
                            </ReactPlaceholder>
                            <div className="mess_div_scroll" id="main_internal_details">
                                <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={imailDetailsLoder2()} ready={!this.props.is_fetching}>
                                    {this.props.mail.content.map((val, index) => (
                                        <div className="mess_v1 pt-0" key={index + 1}>
                                            <div className="mess_vn_1 bt-1 pt-3">
                                                <div className="mess_vn_in_1">
                                                    <div className="mess_V_1">
                                                        <span><img src={val.user_img} /></span>
                                                    </div>
                                                    <div className="mess_V_2">
                                                        <div className="mess_V_a">
                                                            <div className="mess_V_a1">From: {val.user_name}</div>
                                                            <div className="mess_V_a2">{(val.is_reply == 1) ? "Re: " : "Subject: "} {this.props.mail.title}</div>
                                                        </div>
                                                        <div className="mess_V_b"> {moment(val.mail_date).format("DD/MM/YYYY LT")}</div>
                                                        <div className="mess_vn_2 justify-content-end" style={{ width: '140px', maxWidth: '140px' }}>
                                                            <div className="Imail_btn_right_v1 text-right">
                                                                {val.attachments.length > 0 ? <OverlayTrigger placement="bottom" overlay={<Tooltip id="attachments" className="MSG_Tooltip" placement={'top'}>Attach file</Tooltip>}><i className="icon icon-attach-im attach_im color"></i></OverlayTrigger> : ''}
                                                                {val.is_priority == 1 ? <OverlayTrigger placement="bottom" overlay={<Tooltip id="is_priority" className="MSG_Tooltip" placement={'top'}>High Priority</Tooltip>}><i className="icon icon-warning-im warning-im color"></i></OverlayTrigger> : ''}
                                                                {(val.is_draft == 1 && this.props.mail_type === 'draft') ? <OverlayTrigger placement="bottom" overlay={<Tooltip id="is_draft" className="MSG_Tooltip" placement={'top'}>Send</Tooltip>}><i onClick={(e) => this.draftMailOpen(val.id)} className="icon icon-share-icon"></i></OverlayTrigger> : ''}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="bb-1 mt-2 mb-3"></div>
                                                <div className="mess_vn_in_2">
                                                    <p className="mb-0 mt-2"></p>
                                                    <p className="my-0">{val.content}</p>
                                                    <p className="mb-4 mt-3">{val.user_name}</p>
                                                </div>

                                                <div className="my-5">
                                                    {val.attachments.map((file, index) => (
                                                        <a key={index + 1} href="javascript:void(0)" >
                                                            <span onClick={() => downloadAttachment(file.file_path, file.filename)} key={index + 1} className="mees_pdf_v1">
                                                                <i className="icon icon-file-im color"></i>
                                                                <p>{file.filename}</p>
                                                            </span>
                                                        </a>
                                                    ))}
                                                </div>
                                                
                                                {(this.props.mail_type === 'inbox' && this.props.mail.is_block == 0) ?
                                                <div className="col-md-6 Imail_btn_left_v1">
                                                    <OverlayTrigger placement="bottom" overlay={<Tooltip id="replyAllMail" className="MSG_Tooltip" placement={'top'}>Replay to all</Tooltip>}><i onClick={() => this.replyAllMail(val.id)} className="icon icon-single-back-im"></i></OverlayTrigger>
                                                    <OverlayTrigger placement="bottom" overlay={<Tooltip id="replyMail" className="MSG_Tooltip" placement={'top'}>Replay Mail</Tooltip>}><i onClick={() => this.replyMail(val.id)} className="icon icon-fully-back-im"></i></OverlayTrigger>
                                                    <OverlayTrigger placement="bottom" overlay={<Tooltip id="forwordMail" className="MSG_Tooltip" placement={'top'}>Forword Mail</Tooltip>}><i onClick={() => this.forwordMail(val.id, val.content, val.attachments)} className="icon icon-next-im"></i></OverlayTrigger>
                                                </div>: ''}
                                            </div>
                                        </div>
                                    ))}
                                </ReactPlaceholder>

                                <div style={{ float: "left", clear: "both" }}
                                    ref={(el) => { this.messagesEnd = el; }}>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>}
        </Aux>
        );
    }
}


const mapStateToProps = state => ({
    mail: state.InternalImailReducer.mail_content_listing,
    reply_mail: state.InternalImailReducer.reply_mail,
    is_fetching: state.InternalImailReducer.is_details_fetching,
})

const mapDispatchtoProps = (dispach) => {
    return {
        getInternalMailContent: (value) => dispach(getInternalMailContent(value)),
        updateMailContent: (value) => dispach(updateMailContentValue(value)),
        updateMailListingValue: (value, messageId) => dispach(updateMailListingValue(value, messageId)),
        replyMail: (status, messageId) => dispach(replyMail(status, messageId)),
        removeMailFromListing: (messageId) => dispach(removeMailFromListing(messageId)),
        setMessageAllContent: (mail_content) => dispach(setMessageAllContent(mail_content)),
    }
}

const Aux = (props) => props.children;

export default connect(mapStateToProps, mapDispatchtoProps)(InternalDetails)
