import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { getFullName} from '../../../service/common.js';
import { connect } from 'react-redux'

class MailDashboard extends Component {
    constructor(props) {
        super(props);
        this.active = this.props.active;
    }

    render() {
        return (
                <section className="manage_top">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-10 col-lg-offset-1 col-md-12 P_15_TB">
                                <Link to="/admin/dashboard"><div className="icon icon-back-arrow back_arrow"></div></Link>
                            </div>
                            <div className="col-lg-10 col-lg-offset-1 col-md-12 P_25_TB bor_T">
                                <h1>Welcome to Imail, {getFullName()}</h1>
                            </div>
                            <div className="col-lg-10 col-lg-offset-1 col-md-12 bor_T"></div>
                        </div>
                
                        <div className="row">
                            <div className="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1">
                                <div className="row your second three justify-content-center mt-7">
                                    <div className="col-lg-5 col-md-5 col-sm-5">
                                        <Link to={'/admin/imail/external/inbox'}>{this.props.external_imail_count > 0 ? <span className='MN_info_a1'>{this.props.external_imail_count}</span> : '' }<h4 className="text-center">External<br /> Imail</h4></Link>
                                        <Link to={'/admin/imail/external/inbox'} className="create_new"><span className="icon icon-share Ext_1"></span></Link>	
                                    </div>
                                    <div className="col-lg-2 col-md-1  col-sm-1 invisible"></div>
                                    <div className="col-lg-5 col-md-5 col-sm-5">
                                        <Link to={'/admin/imail/internal/inbox'}>{this.props.internal_imail_count > 0? <span className='MN_info_a1'>{this.props.internal_imail_count}</span> : '' }<h4 className="text-center">Internal<br /> Imail</h4></Link>
                                        <Link to={'/admin/imail/internal/inbox'} className="create_new"><span className="icon icon-share Int_1"></span></Link>
                                    </div>
                                </div>
                            </div>
                
                        </div>
                    </div>
                </section>
                );
    }
}


const mapStateToProps = state => ({
    external_imail_count: state.NotificationReducer.external_imail_count,
    internal_imail_count: state.NotificationReducer.internal_imail_count,
})

const mapDispatchtoProps = (dispach) => {
    return {
        
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(MailDashboard);    
