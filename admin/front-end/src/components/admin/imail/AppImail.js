import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import { ROUTER_PATH } from '../../../config.js';
import { checkItsNotLoggedIn, getPermission } from '../../../service/common.js';


import ImailDashboard from './ImailDashboard';
import ExternalImail from './external_imail/ExternalImail';
import {imailJson} from 'menujson/imail_menu_json';
import InternalImail from './internal_imail/InternalImail';
import { setFooterColor } from '../../admin/notification/actions/NotificationAction.js';
import Sidebar from '../Sidebar';
import { connect } from 'react-redux'
import {setSubmenuShow} from 'components/admin/actions/SidebarAction';

import PageNotFound from '../../admin/PageNotFound';
const permissionRediect = <Redirect to={'/admin/no_access'} />;

class AppImail extends React.Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn();
        this.permission = (getPermission() == undefined)? [] : JSON.parse(getPermission());
        this.state = {
            subMenuShowStatus:true,
            menus: imailJson,
            replaceData:{':id':0} 
        }
    }
    
    componentDidMount() {
         this.props.setSubmenuShow(1);
         this.props.setFooterColor('');
    }
    
    componentWillUnmount(){
        this.props.setFooterColor('');
    }
     
    render() {
        return ( <section className='asideSect__ Gold' style={{background:'none'}}>
        <Sidebar
          heading={'iMail'}
          menus={this.state.menus}
          subMenuShowStatus={this.state.subMenuShowStatus}
          replacePropsData={this.state.replaceData}
        />
                    <link rel="stylesheet" href={'/assets/css/data_table.css'} />
                        <Switch>
                            <Route exact path={ROUTER_PATH+'admin/imail/dashboard'} render={(props) => this.permission.access_imail ? <ImailDashboard props={props} /> : permissionRediect } />
                            <Route exact path={ROUTER_PATH+'admin/imail/external'} render={(props) => this.permission.access_imail ? <ExternalImail props={props} /> : permissionRediect } />
                            
                            <Route exact path={'/admin/imail/external/:type(archive|inbox|draft)'} render={(props) => this.permission.access_imail ? <ExternalImail props={props} /> : permissionRediect } />
                            <Route exact path={'/admin/imail/external/:type(archive|inbox|draft)/:id(\\d+)'} render={(props) => this.permission.access_imail ? <ExternalImail props={props} /> : permissionRediect } />
                            
                            <Route exact path={'/admin/imail/internal/:type(archive|inbox|draft|group_message)'} render={(props) => this.permission.access_imail ? <InternalImail props={props} /> : permissionRediect } />
                            <Route exact path={'/admin/imail/internal/:type(archive|inbox|draft|group_message)/:id(\\d+)'} render={(props) => this.permission.access_imail ? <InternalImail props={props} /> : permissionRediect } />
                            <Route exact path={'/admin/imail/internal/:type(archive|inbox|draft|group_message)/:team_type(team|department)/:id(\\d+)'} render={(props) => this.permission.access_imail ? <InternalImail props={props} /> : permissionRediect } />
                            <Route exact path={'/admin/imail/external/:type(archive|inbox|draft)/:id(\\d+)/:re(re)/:contentId(\\d+)'} render={(props) => this.permission.access_imail ? <ExternalImail props={props} /> : permissionRediect } />
                            <Route exact path={'/admin/imail/internal/:type(archive|inbox|draft)/:id(\\d+)/:re(re)/:contentId(\\d+)'} render={(props) => this.permission.access_imail ? <InternalImail props={props} /> : permissionRediect } />
                            
                            <Route path='/admin/imail/' component={PageNotFound}  />
                        </Switch>
                    </section>
                );
    }
}

const mapStateToProps = state => ({
   
})

const mapDispatchtoProps = (dispach) => {
    return {
        setFooterColor: (result) => dispach(setFooterColor(result)),
        setSubmenuShow: (result) => dispach(setSubmenuShow(result)),
    }
}

const AppImailData = connect(mapStateToProps, mapDispatchtoProps)(AppImail)
export { AppImailData as AppImail };