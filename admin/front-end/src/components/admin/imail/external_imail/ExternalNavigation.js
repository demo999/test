import React, { Component } from 'react';
import { Link} from 'react-router-dom';
import { connect } from 'react-redux'

class ExternalNavigation extends Component {
    inboxCount = () => {
          if(this.props.external_imail_count){
               return <span className="mess_V1_noti pull-right">{this.props.external_imail_count}</span>;
          }
        
    }
    render() {
        return (
                <aside className="col-lg-2 col-md-3 P_25_T">
                    <ul className="side_menu">
                        <li><Link className={(this.props.active == "inbox")? 'active': ''} to="/admin/imail/external/inbox">Inbox {this.inboxCount()}</Link></li>
                        <li><Link className={(this.props.active == "draft")? 'active': ''} to="/admin/imail/external/draft">Drafts </Link></li>
                        <li><Link className={(this.props.active == "archive")? 'active': ''} to="/admin/imail/external/archive">Archived </Link></li>
                    </ul>
                </aside>
         );
    }
}

const mapStateToProps = (state) => ({
    external_imail_count: state.NotificationReducer.external_imail_count,
})

export default connect(mapStateToProps)(ExternalNavigation)  