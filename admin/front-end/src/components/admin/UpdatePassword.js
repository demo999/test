import React, { Component } from 'react';
import BlockUi from 'react-block-ui';
import {Redirect } from 'react-router-dom';
import jQuery from "jquery";


import { checkItsNotLoggedIn, postData, logout } from '../../service/common.js';


class UpdatePassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        }
        checkItsNotLoggedIn();
    }

    handleChange = (e) => {
        var state = {};
        state[e.target.name] = (e.target.value);
        this.setState(state);
    }

    onSubmit = (e) => {
        e.preventDefault();
        
        jQuery('#update_password').validate();
        if (jQuery('#update_password').valid()) {
            this.setState({loading: true, error: '', success: ''});

            postData('admin/Dashboard/update_password', this.state).then((result) => {
                this.setState({loading: false});
                if (result.status) {
                    this.setState({success: <span><i className="icon icon-input-type-check"></i><div>{result.success}</div></span>});
                    setTimeout(() => logout(), 2000);
                } else {
                   this.setState({error: <span><i className="icon ocs-warning2-ie"></i><div>{result.error}</div></span>});
                }
            });



        }
    }

    componentDidMount() {
       
    }

    render() {
        return (
                <div>
                {(this.state.resetTrue)? <Redirect to='/'  />:''}
                    <BlockUi tag="div" blocking={this.state.loading}>
            <section className="gradient_color">
                      
                            <div className="col-md-10 col-md-offset-1">

                            <div className="row d-flex w-100 justify-content-center mt-5">
                            <div className="col-md-9 mt-5">
                                 <div className="H_B_1">Updating Your Password</div>
                            </div>
                            </div>


                                <form id="update_password" className="login100-form" >
                                <div className="row">
                                <div className="col-md-8 col-md-offset-2 text-center Pass_Ma1"><img src="/assets/images/admin/kye_icon.svg" alt="user" /></div>
                                    <div className="col-md-8 col-md-offset-2">
                                          <div className="limiter">
                                            <div className="login_1">
                                               <div className="col-md-6">
                                                      
                                                       <div className="input_2">
                                                            <input className="input_3"  type="password" name="password" placeholder="Confirm Current" 
                                                            onChange={this.handleChange} value={this.state.password || ''} data-rule-required="true" data-placement="right" />
                                                        </div>
                                                        <div className="input_2">
                                                            <input className="input_3" id="password" type="password" name="new_password" 
                                                            placeholder="Enter New" onChange={this.handleChange} value={this.state.new_password || ''} data-rule-strongPassword="true" data-rule-minlength="6" data-rule-required="true" data-placement="right" />
                                                        </div>
                                                        <div className="input_2">
                                                            <input className="input_3" type="password" name="confirm_password" placeholder="Confirm New" 
                                                            onChange={this.handleChange} value={this.state.confirm_password || ''} data-rule-required="true" data-rule-equalto="#password" data-msg-equalto="Please enter same password as above" data-placement="right" />
                                                        </div>
                                                      
                                                      <div className="col-md-10 col-md-offset-1">
                                                            <div className="login_but"><button disabled={this.state.loading} onClick={this.onSubmit} className="but_login orange">Save New Password</button></div>
                                                      </div>
                                                      <div className="success_login s_new w-100 d-inline-block">{this.state.success}</div>
                                                      <div className="error_login e_new w-100 d-inline-block">{this.state.error}</div>
                                                 </div>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </section>
                
                        
                    </BlockUi>
                </div>
                            );
            }
}
export default UpdatePassword;
