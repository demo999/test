import React, { Component } from 'react';

import ReactSelect from './ReactSelect';
import ReactStarSelect from './ReactStarSelect'
import ImageCropper from './ImageCropper';
import axios from 'axios';


class UI extends Component {

    state = {
        selectedFile :null,
        cat1:30,
        file:null
    }

    fileSelectedHandler = (e) => {
        this.setState({
            selectedFile:e.target.files[0]
        })
        console.log(e.target.files[0]);
    }

    fileUploadHandler = () => {
        const fd = new FormData();
        fd.append('image', this.state.selectedFile, this.state.selectedFile.name);
        axios.post('https://dummy-b14ce.firebaseio.com/iamges.json', fd)
        .then(res => {
            console.log(res);
        });
    }

    handleChange = (event) => {
        this.setState({cat1:event.target.value});
    }

    handleChange2 = (event) => {
       
        if(event.target.files[0] == undefined){
            this.setState({file:null})
        }
        else{
            this.setState({
              file: URL.createObjectURL(event.target.files[0])
            })
        }     
    }

    render() {

        return (
            <section style={{ minHeight: '500px', padding: '50px' }}>

                <div className='container'>

                    <div className='row'>

                        <div className='col-md-4'>

                            <ReactSelect />

                        </div>

                        <div className='col-md-4'>

                            <ReactStarSelect />

                        </div>


                        <div className='col-md-12'>

                            <ImageCropper />

                        </div>


                        {/* <div className='col-md-12'>

                            <input type='file'  onChange={this.fileSelectedHandler} />
                            <button onClick={this.fileUploadHandler}>Upload</button>

                        </div> */}


                        <div className='col-md-12' style={{margin:'20px 0'}}>
                                <div style={{visibility:this.state.cat1<81?'hidden':'visible'}}>
                                    <div className={'trvw unused' } > <div>{100 - this.state.cat1}%</div>Unused</div>
                                    <div className={'trvw used' }> <div>{this.state.cat1}%</div>Used</div>
                                </div>
                                <div className='main_dv'>
                                <div className='percBreakdown'>
                                {/* <span><div>80%</div>unused</span> */}
                                    <div className='cat1' style={{width:this.state.cat1 + '%', height:this.state.cat1 + '%'}}>
                                        <span className={'unusedCat' + ' ' + (this.state.cat1>80?'hidee':'')} style={{  top: - + (100 - this.state.cat1)}}>
                                            <div>{100 - this.state.cat1}%</div>Unused
                                        </span>

                                  
                                        <span>
                                            {this.state.cat1>80?(<div>Percentage Brokedown</div>):(<div>{this.state.cat1}%</div>)}
                                            
                                        </span>
                                    </div>
                                    <div className='hvrDv'></div>
                                </div>
 
                                
                                </div>

                                <input 
                                    id="typeinp" 
                                    type="range" 
                                    min="0" max="100" 
                                    value={this.state.cat1} 
                                    onChange={this.handleChange}
                                    step="1"
                                    className='slideRange'
                                  
                                />
                        </div>

                    </div>



                        <div className='col-md-12'>

                        <div>
                            <input type="file" onChange={this.handleChange2}/>
                            <img src={this.state.file}/>
                        </div>

                        </div>


                </div>


            </section>
        );
    }
}

export default UI;