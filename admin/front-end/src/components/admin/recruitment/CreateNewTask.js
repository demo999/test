import React, { Component } from 'react';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { ROUTER_PATH, BASE_URL, PAGINATION_SHOW, DATA_CONSTANTS } from '../../../config.js';
import { recruitmentStatus,recruitmentActionType,recruitmentLocation } from '../../../dropdown/recruitmentdropdown.js';
import { postData, checkItsNotLoggedIn, Aux, handleChangeChkboxInput, getOptionsRecruitmentStaff, handleChangeSelectDatepicker } from '../../../service/common.js';
import ReactTable from "react-table";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import jQuery from "jquery";
import { ToastContainer, toast } from 'react-toastify';
import Pagination from "../../../service/Pagination.js";
import { ToastUndo } from 'service/ToastUndo.js'

const WAIT_INTERVAL = DATA_CONSTANTS.FILTER_WAIT_INTERVAL;
const ENTER_KEY = DATA_CONSTANTS.FILTER_ENTER_KEY;

const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {
        // request json
        var Request = JSON.stringify({ pageSize: pageSize, page: page, sorted: sorted, filtered: filtered });
        postData('recruitment/RecruitmentDashboard/get_applicant_list', Request).then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.count),
                all_count: result.all_count,
            };
            resolve(res);
        });
    });
};
class MyModal extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn(ROUTER_PATH);
        this.timer = null;
        this.validator = '';
        this.state = { 
            applicantList: [],
            attachedApplicant:[],
            assigned_user:[],
            create_sub_task:false,
            tempTitle:'',
            tempDescription:'',
            tempTaskAssignTo:'',
            tempDueDate:'',
            subTaskName: '',
            subTaskDescription: '',
            subTaskDue_date:'',
            subTaskAssigned:'',
            loading: false,  
            view_sub_task: false,  
            action_location: '',
            action_status: '',
            filterSearch: '',
            subsTasks_ul: [],
            activeItem: null,
            action_type:false
        }
    }

    handleItemClick = (i) => {
        let { subsTasks_ul } = this.state;
        this.setState({
            activeItem: i,
            subTaskName: subsTasks_ul[i].name,
            subTaskDescription: subsTasks_ul[i].decription,
            subTaskAssigned: subsTasks_ul[i].assigned,
            subTaskDue_date: subsTasks_ul[i].due_date,
            create_sub_task:false,
            view_sub_task:true,
        })
    }
    componentDidMount() {
        /*let { subsTasks_ul } = this.state;
        this.setState({
            subTaskName: subsTasks_ul[0].name,
            subTaskDescription: subsTasks_ul[0].decription,
            subTaskAssigned: subsTasks_ul[0].due_date,
            subTaskDue_date:subsTasks_ul[0].assigned,
        })*/
    }

    addToAttachedApplicant=(e)=>
    {
        var tempState = [];
        if(e.target.checked)
        {
           this.setState({ attachedApplicant: [...this.state.attachedApplicant, e.target.value] },()=>{ });
        }
        else
        {
            var array = [...this.state.attachedApplicant]; // make a separate copy of the array
            var index = array.indexOf(e.target.value)
            if (index !== -1) {
                array.splice(index, 1);
                this.setState({attachedApplicant: array},()=>{});
            }
        }
       
    }
    /*This function is responsible to handle load filtered common all input after user stop writing*/
    handleChangeSearch =(e)=> {
        clearTimeout(this.timer)
        this.setState({ filterAllTyping: e.target.value })
        this.timer = setTimeout(this.triggerChange, WAIT_INTERVAL)
    }

    /* This function is responsible to handle load filtered common all input after user enter key use */
    // handleKeyDown =(e)=> {
    //     if (e.keyCode === ENTER_KEY) {
    //         this.triggerChange(e)
    //         clearTimeout(this.timer)
    //     }
    // }

    /* This function is responsible to call filter api   */
    triggerChange =(e)=> {
        let value = this.state.filterAllTyping;
        var requestData = { srch_box: value };
        this.setState({ filtered: requestData },()=>{ console.log(this.state.filtered)});
    }

    /*This function is responsible to handle load filtered common all input after user enter key use */
    // handleKeyDown =(e)=> {
    //     if (e.keyCode === ENTER_KEY) {
    //         this.triggerChange(e)
    //         clearTimeout(this.timer)
    //     }
    // }

    // searchData = (e) => {
    //     e.preventDefault();
    //     var requestData = { srch_box: this.state.srch_box };
    //     this.setState({ filtered: requestData });
    // }

    fetchData = (state, instance) => {
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
            this.setState({
                applicantList: res.rows,
                all_count: res.all_count,
                pages: res.pages,
                loading: false,
            });
        })
    }

    /*Check validation in sub-task and save it to temperary state*/
    checkSubTaskAndSave=()=>
    {   
        if(this.validator)
            this.validator.destroy();

        this.validator = jQuery("#task_form").validate({
              rules: {
              tempTitle: "required",
              tempDescription: "required",
              tempTaskAssignTo: "required",
              tempDueDate: "required",
            },
         });

         if (jQuery("#task_form").valid() ) {
               var oldState = this.state.subsTasks_ul;
               let de= {};
               de['name'] = this.state.tempTitle;
               de['decription'] = this.state.tempDescription;
               de['due_date'] = this.state.tempDueDate;
               de['assigned'] = this.state.tempTaskAssignTo.label;
               de['task_assigned_id'] = this.state.tempTaskAssignTo.value;
               de['new'] = true;
               oldState.push(de);
           this.setState({subsTasks_ul:oldState},()=>{ 
            this.setState({tempTitle:'',tempDescription:'',tempTaskAssignTo:'',tempDueDate:'',create_sub_task:false})
           });
        } else {
            this.validator.focusInvalid();
        }
         
    }

    handleSaveAction =(e)=>
    {
        e.preventDefault();
        if(this.validator)
            this.validator.destroy();

        this.selectValidationCheck();
        this.validator = jQuery("#task_form").validate({
              rules: {
              action_name: "required",
              time_duration: "required",
              assigned_user: "required",
            },
         });
         
        //if(jQuery("#task_form").valid())
        if(!this.state.loading && jQuery("#task_form").valid())
        {
            if(this.state.attachedApplicant.length == 0)
            {
                toast.dismiss();
                toast.error('Please select atleast one applicant to continue', {
                position: toast.POSITION.TOP_CENTER,
                hideProgressBar: true
                });
            }
            else
            {
                this.setState({loading: true},()=>{
                postData('recruitment/RecruitmentDashboard/create_action', this.state).then ((result) => {
                    if (result.status) {
                           this.setState({loading: false});
                           toast.dismiss();
                           toast.success(<ToastUndo message={'Action created successfully.'} showType={'s'} />, {
                        //    toast.success("Action created successfully.", {
                                position: toast.POSITION.TOP_CENTER,
                                hideProgressBar: true
                              }); 
                           this.props.closeModal();
                        } else{
                            toast.dismiss();
                            toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                            // toast.error(result.error, {
                                position: toast.POSITION.TOP_CENTER,
                                hideProgressBar: true
                              });
                             this.setState({loading: false});
                        }
                    });
                });
            }
        }
       else
       {
            this.validator.focusInvalid();
       }
    }

    selectValidationCheck=()=>
    {
        var isSubmit = 1;
        var tempState={};
        if(!this.state.action_type)
        {
            isSubmit = 0;
            tempState['action_type'+'_error'] = true;          
        }
        if(!this.state.action_status)
        {
            isSubmit = 0;
            tempState['action_status'+'_error'] = true;          
        }
        if(!this.state.action_location)
        {
            isSubmit = 0;
            tempState['action_location'+'_error'] = true;          
        }
        this.setState(tempState);
        return isSubmit;
    }

    callHtmlForValidation=(currentState)=>
    {
        return (this.state[currentState + '_error']) ? <div className={'tooltip fade top in'+ ((this.state[currentState + '_error'])? ' select-validation-error': '')} role="tooltip">
        <div className="tooltip-arrow"></div><div className="tooltip-inner">This field is required.</div></div> : '';
    }

    ab = () => {}
    //
    render() {
        const columns = [
            { Header: 'HCMGR-ID', accessor: 'id', filterable: false, },
            { Header: 'Name', accessor: 'applicant_name', filterable: false, },
            { Header: 'Email ', accessor: 'email', filterable: false, },
            { Header: 'Phone ', accessor: 'phone', filterable: false, },
            {
                Cell: (props) =><span>
                <input type="checkbox"  className="checkbox1 font" id={props.original.id} value={props.original.id} onChange={(e)=>this.addToAttachedApplicant(e)}/>
                <label htmlFor={props.original.id}><span></span></label>
                </span>, 
                Header: 'Action:'
            }
        ]
        return (
            <div className={this.props.showModal ? 'customModal show' : 'customModal'}>
                <div className="cstomDialog widBig">
                    <h3 className="cstmModal_hdng1--">
                        Create New Action
                        <span className="closeModal icon icon-close1-ie" onClick={this.props.closeModal}></span>
                    </h3>
                    <form id="task_form" method="post" autoComplete="off">
                        <div className="row bor_row_bef">
                            <div className="col-md-6">
                                <div className="csform-group">
                                    <label>Action Name:</label>
                                    <input type="text" className="csForm_control" onChange={(e) => handleChangeChkboxInput(this, e)} name="action_name"/>
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="csform-group">
                                    <label>Action Type:</label>
                                    <div className="cmn_select_dv">
                                        <Select name="action_type" className="custom_select"
                                            simpleValue={true}
                                            searchable={false} clearable={false}
                                            placeholder="Action Name"
                                            options={recruitmentActionType()}
                                            onChange={(e) => this.setState({ action_type: e, action_type_error:false})}
                                            value={this.state.action_type}
                                        />
                                        {this.callHtmlForValidation('action_type')}
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="csform-group">
                                    <label>Status:</label>
                                    <div className="cmn_select_dv">
                                        <Select name="action_status" className="custom_select"
                                            simpleValue={true}
                                            searchable={false} clearable={false}
                                            placeholder="Select Status"
                                            options={recruitmentStatus()}
                                            onChange={(e) => this.setState({ action_status: e, action_status_error:false})}
                                            value={this.state.action_status}
                                        />
                                        {this.callHtmlForValidation('action_status')}
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="csform-group">
                                    <label>Assign To User:</label>
                                    <span className="requireds modify_select">
                                        <div className="search_icons_right modify_select default_validation ">
                                            <Select.Async
                                                //multi
                                                name='assigned_user'
                                                loadOptions={(e) => getOptionsRecruitmentStaff(e)}
                                                clearable={false}
                                                placeholder='Search'
                                                cache={false}
                                                value={this.state.assigned_user}
                                                onChange={(e) => handleChangeSelectDatepicker(this, e, 'assigned_user')}
                                                
                                            />
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="csform-group">
                                    <label>Date/Time/Duration:</label>
                                    <input type="text" className="csForm_control" name="time_duration" onChange={(e) => handleChangeChkboxInput(this, e)} />
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="csform-group">
                                    <label>Location:</label>
                                    <div className="cmn_select_dv">
                                        <Select name="action_location" className="custom_select"
                                            simpleValue={true}
                                            searchable={false} Clearable={false}
                                            placeholder="Select Location"
                                            options={recruitmentLocation()}
                                            onChange={(e) => this.setState({ action_location: e,action_location_error:false })}
                                            value={this.state.action_location}
                                        />
                                        {this.callHtmlForValidation('action_location')}
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="bor_line"></div>
                            </div>
                        </div>
                        {/* row ends */}
                        <div className="row">
                            <div className="col-md-8">
                                <div className="csform-group">
                                    <div className="search_bar right">
                                        <input
                                            value={this.state.filterAllValue}
                                            onChange={this.handleChangeSearch}
                                            onKeyDown={this.handleKeyDown}
                                            className="srch-inp"
                                            placeholder="Search.."
                                        />
                                        <i className="icon icon-search2-ie"></i>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="csform-group">
                                    <div className="cmn_select_dv">
                                        <Select name="view_by_status"
                                            simpleValue={true}
                                            searchable={false} Clearable={false}
                                            placeholder="Filter by: All"
                                            onChange={(e) => this.setState({ filterSearch: e })}
                                            value={this.state.filterSearch}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* row ends */}
                        <div className="row">
                            <div className="col-md-12">
                                <div className="csform-group">
                                    <label>Attach Applicant/s:</label>
                                    <div className="data_table_cmn tableType2 createACtion_Table1">
                                        <ReactTable
                                        PaginationComponent={Pagination}
                                            showPagination={this.state.applicantList.length > PAGINATION_SHOW ? true : false}
                                            columns={columns}
                                            manual
                                            data={this.state.applicantList}
                                            pages={this.state.pages}
                                            loading={this.state.loading}
                                            onFetchData={this.fetchData}
                               
                                            filtered={this.state.filtered}
                                            defaultFiltered=""
                                            defaultPageSize={10}
                                            className="-striped -highlight"
                                            noDataText="No Record Found"
                                            minRows={1}
                                            previousText={<span className="icon icon-arrow-left privious"></span>}
nextText={<span className="icon icon-arrow-right next"></span>}
                                            //ref={this.reactTable}
                                        />
                                    </div>
                                    <div className="bor_line"></div>
                                </div>
                            </div>
                        </div>
                        {/* row ends */}
                        <div className="row">
                            <div className="col-md-4">
                                <div className="csform-group">
                                <label>Sub-Tasks:</label>
                                    <div className="subtasks_list">
                                        <ul className="subsTasks_ul">
                                            {

                                                (this.state.subsTasks_ul.length>0)?this.state.subsTasks_ul.map((task, i) => {
                                                    return (
                                                        <li key={i} className={this.state.activeItem === i ? 'active' : ''} onClick={this.handleItemClick.bind(this, i)} >
                                                            {task.name} <i className='icon icon-view2-ie'></i>
                                                        </li>
                                                    )
                                                }):''
                                            }
                                        </ul>
                                    </div>                           
                                    <div className="btn cmn-btn1 creat_subTsksBtn" onClick={()=>this.setState({create_sub_task:true,view_sub_task:false,activeItem:null})}>Create New Sub-Tasks <span>+</span></div>
                                </div>
                            </div>
                            
                            <div className="col-md-8">
                            
                                {(this.state.view_sub_task)?
                                <div className='subtask_info_box--'>
                                    <h3 className="subtsks_name bor_bot1">{this.state.subTaskName || ''}</h3>
                                    <p className='subtsks_detail bor_bot1'>{this.state.subTaskDescription || ''}</p>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="csform-group">
                                                <label>Sub-Task Assigned To:</label>
                                                <input type="text" className="csForm_control" readOnly={true} value={this.state.subTaskAssigned || ''}/>
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="csform-group">
                                                <label>Due Date:</label>
                                                 <input type="text" className="csForm_control" readOnly={true} value={moment(this.state.subTaskDue_date).format('D/M/Y')  || ''}/>
                                            </div>
                                        </div>
                                        <div className="col-md-2">
                                            <i className="icon icon-attatchments1-ie add_attachments1"></i>
                                        </div>
                                    </div>
                                </div>:''}
                                
                                {(this.state.create_sub_task)?
                                <div id="create_task">
                                    <div className='subtask_info_box--'>
                                         <div className="row">   
                                        <div className="col-md-12">
                                                <div className="csform-group">
                                                    <label>Title:</label>
                                                    <input type="text" className="csForm_control" placeholder="Title" value={this.state.tempTitle} name="tempTitle" onChange={(e) => handleChangeChkboxInput(this, e)} />
                                                </div>
                                        </div>
                                        
                                        <div className="col-md-12">
                                                <div className="csform-group">
                                                    <label>Description:</label>
                                                    <textarea className="csForm_control txt_area brRad10 textarea-max-size" placeholder="Description" value={this.state.tempDescription} name="tempDescription" onChange={(e) => handleChangeChkboxInput(this, e)}>
                                               </textarea>
                                                </div>
                                        </div>
                                        
                                            <div className="col-md-6">
                                                <div className="csform-group">
                                                    <label>Sub-Task Assigned To:</label>
                                                    <span className="requireds modify_select">
                                        <div className="search_icons_right modify_select default_validation ">
                                            <Select.Async
                                                name={'tempTaskAssignTo'}
                                                loadOptions={(e) => getOptionsRecruitmentStaff(e)}
                                                clearable={false}
                                                placeholder='Search'
                                                cache={false}
                                                value={this.state.tempTaskAssignTo}
                                                onChange={(e) => handleChangeSelectDatepicker(this, e, 'tempTaskAssignTo')}
                                               
                                            />
                                        </div>
                                    </span>
                                                </div>
                                            </div>
                                            <div className="col-md-4">
                                                <div className="csform-group">
                                                    <label>Due Date:</label>
                                                    <DatePicker
                                                        selected={this.state.tempDueDate ? moment(this.state.tempDueDate, 'DD-MM-YYYY') : null}
                                                        className="csForm_control"
                                                        onChange={(e) => handleChangeSelectDatepicker(this,e, 'tempDueDate')}
                                                        dateFormat="DD/MM/YYYY" 
                                                        name="tempDueDate"  
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-2">
                                                <i className="icon icon-attatchments1-ie add_attachments1"></i>
                                            </div>
                                            
                                        </div>
                                        
                                        <div className='row'>
                                            <div className="col-md-12">
                                                <a className="btn cmn-btn1 creat_task_btn__" onClick={()=>this.setState({create_sub_task:false,tempTitle:'',tempDescription:'',tempTaskAssignTo:'',tempDueDate:''})}>Cancel</a>
                                                <a className="btn cmn-btn1 creat_task_btn__" onClick={this.checkSubTaskAndSave}>Create New Sub-task</a>
                                            </div>
                                        </div>                                        
                                    </div>
                                 </div>
                                :''}
                                {/* subtask_info_box ends */}
                            </div>
                            <div className="col-md-12">
                                <button className="btn cmn-btn1 creat_task_btn__" onClick={(e) =>this.handleSaveAction(e)} >Create New Task & Send Email Invitations</button>
                            </div>
                        </div>
                        {/* row ends */}
                    </form>
                </div>
            </div>
        );
    }
}
export default MyModal;