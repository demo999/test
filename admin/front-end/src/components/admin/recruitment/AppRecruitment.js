import React, { Component } from 'react';
import { ROUTER_PATH, BASE_URL } from '../../../config.js';
import { BrowserRouter as Router, Route, Switch, NavLink } from 'react-router-dom';

import Dashboard from '../../admin/recruitment/RecruitmentDashboard';
import UserManagement from '../../admin/recruitment/UserManagement';
import RecruitmentAction from '../../admin/recruitment/RecruitmentAction';
import JobOpening from '../../admin/recruitment/JobOpening';
import CreateJob from '../../admin/recruitment/CreateJob';
import Applicants from '../../admin/recruitment/Applicants';
//import Training from '../../admin/recruitment/Training';
import GroupInterviewListing from '../../admin/recruitment/training/GroupInterviewListing';
import IpadListing from '../../admin/recruitment/training/IpadListing';
import CABDayListing from '../../admin/recruitment/training/CABDayListing';
import { postData, getPermission, checkItsNotLoggedIn } from 'service/common.js';
import ApplicantInfo from '../../admin/recruitment/ApplicantInfo';
import { setFooterColor } from '../../admin/notification/actions/NotificationAction.js';
import UI from './UI/UI';
/*import Tasks from '../../admin/recruitment/Tasks';*/

import ApplicantResult from '../../admin/recruitment/ApplicantResult';
import { connect } from 'react-redux'
import MyComponents from '../../admin/recruitment/MyComponents';
import Sidebar from '../Sidebar';
import {recruitmentJson} from 'menujson/recruitment_menu_json';
import {setActiveSelectPage} from 'components/admin/recruitment/actions/RecruitmentAction';


const menuJson = () => {
    let menu = recruitmentJson;
    return menu;
}

let cssLoaded = false;

class AppRecruitment extends Component {
    constructor(props) {
        checkItsNotLoggedIn(ROUTER_PATH);
        super(props);
        this.permission = (getPermission() == undefined) ? [] : JSON.parse(getPermission());
        this.state = {
            loadState: true,
            subMenuShowStatus:true,
            menus:menuJson(),
            replaceData:{':id':0}
        }
    }


    componentDidMount() {
        this.props.setFooterColor('recruitment_module');
    }

    componentWillUnmount() {
        this.props.setFooterColor('');
    }

    


    render() {


        return (
            <div className='recruitment_module bodyNormal' >

               <section className='asideSect__'>
                    <Sidebar 
                        heading={'Recruitment'} 
                        menus={this.state.menus}
                        subMenuShowStatus={this.state.subMenuShowStatus}
                        replacePropsData={this.state.replaceData}
                    />

                    <div className="container-fluid">
                        <div className='row justify-content-center d-flex'>

                            <div className='col-lg-11 col-md-12 col-sm-12'>
                                <Switch>

                                    <Route exact path={ROUTER_PATH + 'admin/recruitment/dashboard'} render={(props) => <Dashboard props={props} />} />
                                    <Route exact path={ROUTER_PATH + 'admin/recruitment/user_management'} render={(props) => this.permission.access_member ? <UserManagement props={props} /> : this.permissionRediect()} />
                                    <Route exact path={ROUTER_PATH + 'admin/recruitment/action/:page?'} render={(props) => <RecruitmentAction props={props} />} />
                                    <Route exact path={ROUTER_PATH + 'admin/recruitment/job_opening/create_job'} render={(props) => <CreateJob props={props} />} />
                                    <Route exact path={ROUTER_PATH + 'admin/recruitment/job_opening/:page?'} render={(props) => <JobOpening props={props} />} />
                                    <Route exact path={ROUTER_PATH + 'admin/recruitment/applicants'} render={(props) => <Applicants props={props} />} />
                                    <Route exact path={ROUTER_PATH + 'admin/recruitment/training/group_interview'} render={(props) => <GroupInterviewListing props={props} />} />
                                    <Route exact path={ROUTER_PATH + 'admin/recruitment/training/ipad'} render={(props) => <IpadListing props={props} />} />
                                    <Route exact path={ROUTER_PATH + 'admin/recruitment/training/cab_day'} render={(props) => <CABDayListing props={props} />} />
                                    <Route exact path={ROUTER_PATH + 'admin/recruitment/applicants/applicant_info'} render={(props) => <ApplicantInfo props={props} />} />
                                    <Route exact path={ROUTER_PATH + 'admin/recruitment/applicants/applicant_result'} render={(props) => <ApplicantResult props={props} />} />
                                    <Route exact path={ROUTER_PATH + 'admin/recruitment/MyComponents'} render={(props) => <MyComponents props={props} />} />
                                    <Route exact path={ROUTER_PATH + 'admin/recruitment/ui'} render={(props) => <UI props={props} />} />
                                </Switch>
                            </div>
                        </div>
                    </div>
                </section>
                
            </div>
        );
    }
}
const mapStateToProps = state => ({
    showTypePage: state.RecruitmentReducer.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {
        setFooterColor: (result) => dispach(setFooterColor(result))
    }
}

const AppRecruitmentData = connect(mapStateToProps, mapDispatchtoProps)(AppRecruitment)
export { AppRecruitmentData as AppRecruitment }; 