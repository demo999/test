import React, { Component } from 'react';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';

class EditApplicantInfo extends Component {

    constructor() {
        super();
        this.state = {
            stateOptions: '',

        }
    }



    render() {
        var stateOptions = [
            { value: '1', label: 'VIC' },
            { value: '2', label: 'Melbourne' },
            { value: '3', label: 'Sydney' }
        ];


        return (
            <div className={'customModal ' + (this.props.show ? ' show' : '')}>
                <div className="cstomDialog widBig">

                    <h3 className="cstmModal_hdng1--">
                        Edit Application Info
                        <span className="closeModal icon icon-close1-ie" onClick={this.props.close}></span>
                    </h3>

                    <form>

                        <div className='row pd_lf_15 mr_tb_20 d-flex justify-content-center flexWrap' >
                            <div className='col-sm-12 col-xs-12'>
                                <label className='bg_labs2 mr_b_20'><strong>Applicants Information</strong> </label>
                            </div>
                            <div className='col-md-11 col-xs-11'>

                                <div className='row '>

                                    <div class="col-sm-3">
                                        <div class="csform-group">
                                            <label>HCM ID:</label>
                                            <h3 class="QId">
                                                <b>000000</b>
                                            </h3>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="csform-group">
                                            <label class="pd_l_15">First Name:</label>
                                            <input type="text" name="" class="csForm_control" value="" />
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="csform-group">
                                            <label class="pd_l_15">Last Name:</label>
                                            <input type="text" name="" class="csForm_control" value="" />
                                        </div>
                                    </div>

                                    <div class="col-sm-5">
                                        <div class="csform-group">
                                            <label class="pd_l_15">Address:</label>
                                            <input type="text" name="" class="csForm_control" value="" />
                                        </div>
                                    </div>

                                    <div className='col-sm-3'>
                                        <div className="csform-group">
                                            <label class="pd_l_15">State:</label>
                                            <div className="cmn_select_dv ">
                                                <Select name="view_by_status "
                                                    required={true} simpleValue={true}
                                                    searchable={false} Clearable={false}
                                                    placeholder="Select State"
                                                    options={stateOptions}
                                                    onChange={(e) => this.setState({ stateOptions: e })}
                                                    value={this.state.stateOptions}
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="csform-group">
                                            <label class="pd_l_15">Area Code:</label>
                                            <input type="text" name="" class="csForm_control" value="" />
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="csform-group">
                                            <label class="pd_l_15">Email:</label>
                                            <input type="text" name="" class="csForm_control" value="" />
                                        </div>
                                    </div>
                                    <div className='col-sm-12'></div>
                                    <div class="col-sm-4">
                                        <div class="csform-group">
                                            <label class="pd_l_15">Phone:</label>
                                            <input type="text" name="" class="csForm_control" value="" />
                                        </div>
                                    </div>

                                </div>





                            </div>
                        </div>

                        <div className='row pd_lf_15 mr_tb_20 d-flex justify-content-center flexWrap' >

                            <div className='col-sm-12 col-xs-12'>
                                <label className='bg_labs2 mr_b_20'><strong>Application</strong> </label>
                            </div>

                            <div className='col-md-11 col-xs-11'>

                                <div className='row '>

                                    <div class="col-sm-3">
                                        <div class="csform-group">
                                            <label className='bg_labs3 cmn_font_clr'>Position:</label>
                                            <h3 class="QId">
                                               Qualified Career
                                            </h3>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="csform-group">
                                            <label className='bg_labs3 cmn_font_clr'>Date Applied:</label>
                                            <h3 class="QId">
                                               01/01/2019
                                            </h3>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="csform-group">
                                            <label className='bg_labs3 cmn_font_clr'>Applied through:</label>
                                            <h3 class="QId">
                                               Seek.com.au
                                            </h3>
                                        </div>
                                    </div>
                                    <div className='col-sm-12  bor_bot1 mr_tb_20'></div>
                                    
                                    <div className='col-sm-12'>
                                    <label className='bg_labs3 cmn_font_clr'>Attached Documents:</label>
                                    <div className='row attch_row mr_tb_10'>
                                            <div className='col-sm-2 col-xs-6'>
                                                <div className='attach_item'>
                                                    <h5><strong>Resume</strong></h5>
                                                    <i className='icon icon-document2-ie'></i>
                                                    <p>john_smith_resume_01-01-19.pdf</p>
                                                </div>
                                            </div>
                                            <div className='col-sm-2 col-xs-6'>
                                                <div className='attach_item'>
                                                    <h5><strong>Cover letter</strong></h5>
                                                    <i className='icon icon-document2-ie'></i>
                                                    <p>john_smith_Cover_letter_01-01-19.pdf</p>
                                                </div>
                                            </div>
                                            <div className='col-sm-2 col-xs-6'>
                                                <div className='attach_item'>
                                                    <h5><strong>Qualification</strong></h5>
                                                    <i className='icon icon-document2-ie'></i>
                                                    <p>john_smith_Qualification_01-01-19.pdf</p>
                                                </div>
                                            </div>

                                        </div>
                                    
                                    </div>


                                </div>
                            </div>

                            
                        
                        </div>




                        <div class="row trnMod_Foot__ disFoot1__">
                            <div class="col-sm-12 no-pad text-right">
                                <button type="submit" class="btn cmn-btn1 create_quesBtn">Save Changes</button>
                            </div>
                        </div>

                    </form>


                </div>
            </div>

        );
    }
}

export default EditApplicantInfo;

