import {recruitmentActiveTitle} from 'menujson/recruitment_menu_json';
export const setDepartmentData = (departmentData) => ({
        type: 'set_recruitment_department_data',
        departmentData
    })

export const setActiveSelectPageData= (value) => {
    return {
        type: 'set_active_page_recruitment',
        value
}}
    
export function setActiveSelectPage(request) {
    return (dispatch, getState) => {
        let pageData =recruitmentActiveTitle;
        let pageType = pageData.hasOwnProperty(request) ? request: 'details';
        let pageTypeTitle = pageData[pageType];
        return dispatch(setActiveSelectPageData({pageType:pageType,pageTitle:pageTypeTitle}))
    }
}

