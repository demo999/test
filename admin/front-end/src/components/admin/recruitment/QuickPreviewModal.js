import React, { Component } from 'react';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';

class QuickPreviewModal extends Component {

    constructor() {
        super();
        this.state = {
           
        }
    }



    render() {
      

        return (
            <div className={'customModal ' + (this.props.showModal? ' show' : '')}>
                <div className="cstomDialog widBig">

                    <h3 className="cstmModal_hdng1--">
                        Quick Preview Ad - Disability Support Worker (Casual)
                            <span className="closeModal icon icon-close1-ie" onClick={this.props.closeModal}></span>
                    </h3>

                    <div className='resume_frame'>
                    <div className='template1'>

                        <table className='content_mn_tble'>
                            <tr>
                                <td style={{background: '#f8931d',  width: '150px'}}>
                                    {/* <div className='left_part_yl'></div> */}
                                </td>
                                <td>
                                <div className='right_part_wh'>
                            <div className='content_main'>

                                    <img src='/assets/images/onCall.svg' className='logo_img'/>
                                    <h2 className='hdng_1'><strong>Disability Support Workers - Western Suburbs</strong></h2>
                                    <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                    when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                                    It has survived not only five centuries, but also the leap into electronic typesetting, 
                                    remaining essentially unchanged. It was popularised in the 1960s with the release of 
                                    Letraset sheets containing Lorem Ipsum passages, and more recently with desktop 
                                    publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                    </p>

                                    <br/>
                                    <h3><strong> Roles</strong></h3>
                                    <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                    when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                                    It has survived not only five centuries, but also the leap into electronic typesetting
                                    </p>
                                    <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                    when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                                    </p>

                                    <br/>
                                    <h3><strong> What we need from you</strong></h3>
                                    <ul>
                                        <li>Lorem Ipsum is simply dummy text of the printing</li>
                                        <li>Lorem Ipsum is simply dummy text</li>
                                        <li>Lorem Ipsum is simply</li>
                                        <li>Lorem Ipsum is simp</li>
                                        <li>Lorem Ipsum is simply dummy text of the printing</li>
                                        <li>Lorem Ipsum is simply dummy text</li>
                                        <li>Lorem Ipsum is simply</li>
                                        <li>Lorem Ipsum is simp</li>
                                        <li>Lorem Ipsum is simply dummy text of the printing</li>
                                    </ul>

                            </div>

                            <div className='footer_gr'>
                                    <table className='mn_tble'>
                                        <tr>
                                            <td width='50%'>
                                                <ul className='socio_link'>
                                                    <li><a href=""><img src='https://www.freeiconspng.com/uploads/fb-logo-icon-facebook-26.png' /></a></li>
                                                    <li><a href=""><img src='https://cdn1.iconfinder.com/data/icons/logotypes/32/square-twitter-512.png' /></a></li>
                                                    <li><a href=""><img src='https://www.iosicongallery.com/icons/instagram-2016-05-12/256.png' /></a></li>
                                                </ul>
                                            </td>
                                            <td width='50%'>
                                                <div className='foot_logo'><img src='/assets/images/onCall.svg' className=''/></div>
                                                <div className='site_lnk'><a href="">oncall.com.au</a></div>
                                            </td>
                                        </tr>
                                    </table>
                            </div>

                        </div>
                                </td>
                            </tr>
                        </table>

                        
                    </div>
                    </div>

                   


                </div>
            </div>

        );
    }
}

export default QuickPreviewModal;

