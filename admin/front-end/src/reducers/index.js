import { combineReducers } from 'redux'

import ParticipantReducer from '../components/admin/participant/reducer/ParticipantReducer'
import MemberReducer from '../components/admin/member/reducer/MemberReducer'
import Permission from './Permission'
import OrganisationReducer from '../components/admin/organisation/reducer/OrganisationReducer'
import NotificationReducer from '../components/admin/notification/reducer/NotificationReducer'
import ExternalImailReducer from '../components/admin/imail/reducer/ExternalImailReducer'
import InternalImailReducer from '../components/admin/imail/reducer/InternalImailReducer'
import RecruitmentReducer from '../components/admin/recruitment/reducer/RecruitmentReducer'
import sidebarData from '../components/admin/reducer/SidebarReducer'
import FmsCaseDetailsData from '../components/admin/fms/reducer/FmsReducer'
import UserDetailsData from '../components/admin/user/reducer/UserReducer'
import ScheduleDetailsData from '../components/admin/schedule/reducer/ScheduleReducer'
import DepartmentReducer from '../components/admin/crm/reducer/DepartmentReducer'
import CrmParticipantReducer from '../components/admin/crm/reducer/CrmParticipantReducer'
export default combineReducers({
  ParticipantReducer,
  MemberReducer,
  Permission,
  OrganisationReducer,
  NotificationReducer,
  ExternalImailReducer,
  InternalImailReducer,
  RecruitmentReducer,
  sidebarData,
  FmsCaseDetailsData,
  UserDetailsData,
  ScheduleDetailsData,
  DepartmentReducer,
  CrmParticipantReducer
})
