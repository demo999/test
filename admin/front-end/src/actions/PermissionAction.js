export const setPermissions = (permission) => ({
        type: 'setPermissions',
        permission
    })
    
    
    
/* set webscoket object
 *  
 *  here set webscoket for send request for every thing like reload notification, mail count, group
 *  message count
 */
export const setWebscoketObject = (obj) => ({
        type: 'set_websocket_object',
        obj
})