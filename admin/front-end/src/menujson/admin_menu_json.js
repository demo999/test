export const adminActiveTitle = {
    single_approval:'Approval',
    single_user_management:'User Managment',
    user_management:'User Managments',
    log:'Logs',
    report:'Report'
};

export const adminJson = [
    { name: 'Approvals', id:'approval', submenus: [{ id:'approval_id',name: 'id', pathstructure: '/admin/user/approval/:id',className:'','linkOnlyHide':true,type:1 }], path: '/admin/user/approval', className:'removeDropdown' },
    { name: 'Reports', id:'report',submenus: [], path: '/admin/user/reports' },
    { name: 'Logs',id:'log', submenus: [], path: '/admin/user/logs' },
    { name: 'User Management',id:'user_management', submenus: [ { id:'user_management_id',name: 'Onbording Analytics', pathstructure: '/admin/user/update/:AdminId',path: '/admin/user/update/:AdminId',className:'','linkOnlyHide':false,type:1 }], path: '/admin/user/list', className:'removeDropdown'},
];