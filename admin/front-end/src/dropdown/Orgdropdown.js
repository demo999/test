export function subOrgViewBy() {
   return [{value:'1',label:'Active'},{value:'0',label:'Inactive'}];
}

export function siteViewBy() {
   return [{value:'0',label:'Current'},{value:'1',label:'Archive'}];
}

export function orgFmsViewBy() {
   return [{value:'0',label:'Organisation'},{value:'1',label:'Sub-Orgs'}];
}

export function docsViewBy() {
   return [{value:'1',label:'Current'},{value:'0',label:'Archive'}];
}

export function subOrgStatus() {
   return [{value:'1',label:'Active'},{value:'0',label:'Inactive'}];
}

export function isParentOrg() {
   return [{value:'1',label:'Yes'},{value:'0',label:'No'}];
}

export function orgGst() {
   return [{value:'1',label:'Yes'},{value:'0',label:'No'}];
}

export function orgTax() {
   return [{value:'1',label:'Yes'},{value:'0',label:'No'}];
}

export function orgAddressCategory() {
   return [{value:'1',label:'Head Office'},{value:'2',label:'Billing'},{value:'3',label:'Other'}];
}


export function statusOptionProfile() {
   return [{value: 1, label: 'Active'}, {value: 0, label: 'Inactive'}];
}

export function orgContactViewBy() {
   return [{value:'0',label:'Current'},{value:'1',label:'Archive'}];
}

export function orgContactType() {
   return [{value:'3',label:'Key Contact'},{value:'4',label:'Billing Contact'}];
}

export function orgSiteType() {
   return [{value:'1',label:'Site'}];
}

export function contactViewBy() {
   return [{value:'0',label:'Current'},{value:'1',label:'Archive'}];
}