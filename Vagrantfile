# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "ubuntu/trusty64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  config.vm.network "forwarded_port", guest: 80, host: 8080
  config.vm.network "forwarded_port", guest: 3306, host: 3306, host_ip: "127.0.0.1"

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  config.vm.synced_folder ".", "/var/www/html"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "shell", inline: <<-'SHELL'
    add-apt-repository ppa:ondrej/php

    echo -e "\033[0;32mRunning updates...\033[0m"
    apt-get update
    apt-get upgrade -y
    apt-get install screen unzip zip

    echo -e "\033[0;32mInstalling Apache and PHP 7.2...\033[0m"
    apt-get install -y software-properties-common
    apt-get install -y apache2
    apt-get install -y php7.2 php7.2-curl php7.2-gd php7.2-json php7.2-mbstring php7.2-mysql php7.2-opcache php7.2-xml

    echo -e "\033[0;32mInstalling MySQL...\033[0m"
    debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
    debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
    apt-get -y install mysql-server
    sed -i -e 's?^bind-address?# bind-address?' /etc/mysql/my.cnf
    mysql -u root -proot -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root'; FLUSH PRIVILEGES;"
    service mysql restart

    echo -e "\033[0;32mInstalling xdebug...\033[0m"
    apt-get install -y php7.2-xdebug
    REMOTE_HOST="$(route -nee | awk '{ print $2 }' | sed -n 3p)"
    tee /etc/php/7.2/mods-available/xdebug.ini <<EOT &> /dev/null
zend_extension=xdebug.so
xdebug.show_error_trace = 1
xdebug.remote_enable=true
xdebug.remote_port=9000
xdebug.remote_connect_back=1
xdebug.remote_autostart=true
xdebug.remote_host=$REMOTE_HOST
EOT

    echo -e "\033[0;32mSetting up protection for .git directory...\033[0m"
    a2enmod rewrite
    sed -i -e 's?DocumentRoot .*?DocumentRoot /var/www/html/admin/back-end\
\
\t<Directory "/var/www/html">\
\t\tAllowOverride All\
\t\tRedirectMatch 404 /\.git\
\t</Directory>?' /etc/apache2/sites-available/000-default.conf
    service apache2 restart

    echo -e "\033[0;32mInstalling composer...\033[0m"
    EXPECTED_SIGNATURE="$(wget -q -O - https://composer.github.io/installer.sig)"
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    ACTUAL_SIGNATURE="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"

    if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]; then
        echo "Invalid composer installer!"
    else
        php composer-setup.php
        mv composer.phar /usr/local/bin/composer
    fi

    rm composer-setup.php

    echo -e "\033[0;32mInstalling npm...\033[0m"
    curl -sL https://deb.nodesource.com/setup_10.x | bash -
    apt-get install -y nodejs
    npm i -g npm

    echo -e "\033[0;32mInstalling code fixer...\033[0m"
    wget https://cs.symfony.com/download/php-cs-fixer-v2.phar -q -O php-cs-fixer
    chmod a+x php-cs-fixer
    mv php-cs-fixer /usr/local/bin/php-cs-fixer

    cd /var/www
    npm i chokidar
    tee .php_cs <<EOT &> /dev/null
<?php

\$finder = PhpCsFixer\Finder::create()
    ->exclude('front-end')
    ->exclude('application/third_party')
    ->exclude('assets')
    ->exclude('system')
    ->exclude('uploads')
    ->exclude('vendor')
    ->exclude('node_modules')
    ->notPath('adminer.php')
    ->in(__DIR__);

return PhpCsFixer\Config::create()
    ->setRules([
        '@PSR2' => true,
        '@Symfony' => true,
        'array_indentation' => true,
        'array_syntax' => ['syntax' => 'short'],
        'combine_consecutive_issets' => true,
        'concat_space' => ['spacing' => 'one'],
        'declare_equal_normalize' => ['space' => 'single'],
        'indentation_type' => true,
        'yoda_style' => ['equal' => false, 'identical' => false],
    ])
    ->setIndent('    ')
    ->setLineEnding("\n")
    ->setUsingCache(false)
    ->setFinder(\$finder);
EOT
    tee codewatch.js <<EOT &> /dev/null
const chokidar = require('chokidar');
const { exec } = require('child_process');

const watcher = chokidar.watch('/var/www/html/**/*.php', {
    usePolling: true,
    persistent: true
});

watcher.on('change', (path, stats) => {
    console.log(path);
    exec(\`php-cs-fixer fix "\${path}"\`, (err, stdout, stderr) => {
        if (err) {
            return;
        }

        console.log(\`==> \${stdout}\`);
        console.log('');
    });
});

const exitHandler = () => {
    console.log('Cleaning up...');
    watcher.close();
};

process.on('exit', exitHandler);
process.on('SIGINT', exitHandler);
EOT

    apt-get autoremove -y
  SHELL

  config.vm.post_up_message = "Run \033[0;32mscreen -dmLS codewatch bash -c \"while true; do node /var/www/codewatch.js; done\"\033[0m to start automated code style fixing and run \033[0;32mscreen -S codewatch -X quit\033[0m to stop it."
end
