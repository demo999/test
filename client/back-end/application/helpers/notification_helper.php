<?php

function notification_msgs($msg_key) {
    $global_ary = array(
		'update_your_detail' => array( 'title'=>'Update Profile Detail','description'=>'Update Participant Profile Detail'),
		'update_kin_detail' => array( 'title'=>'Update Kin Detail','description'=>'Update Participant Profile kin Detail'),
		'update_booker_detail' => array( 'title'=>'Update Booker Detail','description'=>'Update Participant Profile Booker Detail'),
		'update_address_detail' => array( 'title'=>'Update Address Detail','description'=>'Update Participant Address Detail'),
		'update_phone_detail' => array( 'title'=>'Update Phone','description'=>'Update Participant Phone Detail'),
		'update_email_detail' => array( 'title'=>'Update Email','description'=>'Update Participant Email Detail'),
		'update_care_requirements' => array( 'title'=>'Update Care Requirements','description'=>'Update Participant Care Requirements Detail'),
		'update_preferred_activities' => array( 'title'=>'Update Preferred Activities','description'=>'Update Participant Preferred Activities'),
		'update_preferred_places' => array( 'title'=>'Update Preferred Places','description'=>'Update Participant Preferred Places'),
		'update_contact_details' => array( 'title'=>'Update Contact Details','description'=>'Update Participant Contact Details'),
		'update_participant_care' => array( 'title'=>'Update Participant Care','description'=>'Update Participant Care Requirement'),
		
		'update_oc_services' => array( 'title'=>'Update Services Provided','description'=>'Update Participant OC Services'),
		'update_support_required' => array( 'title'=>'Update Support Required','description'=>'Update Participant Support Required'),
		'update_assistance_required' => array( 'title'=>'Update Assistance Required','description'=>'Update Participant Assistance Required'),
		'update_mobility_required' => array( 'title'=>'Update Mobility Required','description'=>'Update Participant Mobility Required'),
		'update_preferred_language' => array( 'title'=>'Update Preferred Language','description'=>'Update Participant Preferred Language'),		
		'update_language_interpreter' => array( 'title'=>'Update Language Interpreter','description'=>'Update Participant Language Interpreter'),		
		'update_hearing_interpreter' => array( 'title'=>'Update Hearing Interpreter','description'=>'Update Participant Hearing Interpreter'),		
		'add_new_shift' => array( 'title'=>'Add New Shift','description'=>'Insert Participant New Shift'),		
		'add_request_roster' => array( 'title'=>'Add New Request Roster','description'=>'Insert Participant Request Roster'),
		
		'add_new_goal' => array( 'title'=>'Add New Goal','description'=>'Participant Add new Goal'),
		'update_new_goal' => array( 'title'=>'Update Goal','description'=>'Participant update Goal'),
		'add_provide_feedback' => array( 'title'=>'Add Provide Feedback','description'=>'Participant Add Provide Feedback'),
		'update_change_password' => array( 'title'=>'Participant Change Password','description'=>'Participant Change Password'),
		'update_profile_pic' => array( 'title'=>'Update Profile Pic','description'=>'Participant Update Profile Picture'),
		'participant_remove_account' => array( 'title'=>'Participant remove account','description'=>'Participant send remove account request'),
		'participant_archive_goal' => array( 'title'=>'Participant archive goal','description'=>'Participant move goal to archive')
    );
    return $global_ary[$msg_key];
}

?>
