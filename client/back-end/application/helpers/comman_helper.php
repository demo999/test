<?php

require_once APPPATH . 'Classes/websocket/Websocket.php';

function encrypt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is my secret key';
    $secret_iv = 'This is my secret iv';
    $key = hash('sha256', $secret_key);
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ($action == 'encrypt') {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if ($action == 'decrypt') {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}

function pr($data, $die = 1) {
    print_r("<pre>");
    print_r($data);
    if ($die == 1) {
        die;
    }
}

function last_query($die = 0) {
    $ci = & get_instance();
    echo $ci->db->last_query();
    if ($die == 1) {
        die;
    }
}

function all_user_data() {
    $ci = & get_instance();
    echo '<pre>';
    print_r($ci->session->all_userdata());
    exit;
}

function request_handler($array_type = false) {
    $request_body = file_get_contents('php://input');
    $response = array();
    if ($array_type) {
        $request_body = json_decode($request_body, true);
        $response = $request_body['request_data']['data'];
    } else {
        $request_body = json_decode($request_body);
        $response = $request_body->request_data->data;
    }
    return $response;
}

function get_client_id($check_token = 1) {
    $request_body = file_get_contents('php://input');
    $request_body = json_decode($request_body);
    return $request_body->request_data->id;
}

/*
 *  verfiy token work on both option parameter and private varibale $ocs_token 
 *  return true if token verify and return false if expire and on time out
 */

function verifyAdminToken($token) {
    $CI = & get_instance();
    $return = false;


    if (!empty($token)) {
        $response = $CI->basic_model->get_row('admin_login', $columns = array('updated', 'adminId', 'token'), $where = array('token' => $token));

        if (!empty($response)) {
            $diff = strtotime(DATE_TIME) - strtotime($response->updated);
            if ($diff > $CI->config->item('jwt_token_time')) {
                // $return alredy set false
            } else {
                // update time of ocs token
                $CI->basic_model->update_records('admin_login', $columns = array('updated' => DATE_TIME), $where = array('token' => $token));
                $return = true;
            }
        }
    }

    return $return;
}

function dateRangeBetweenDateWithWeek($start_date, $end_date, $totalWeek) {
    $first = $start_date;
    $last = $end_date;

    $dates = array();
    $step = '+1 day';
    $format = 'Y-m-d';
    $current = strtotime($first);
    $last = strtotime($last);
    $weekNumber = 0;
    $cnt = count($totalWeek);

    $weekCount = 1;
    while ($current <= $last) {
        $date1 = date($format, $current);
        if (($weekNumber) == $cnt) {
            $weekNumber = 0;
        }

        $dates[$totalWeek[$weekNumber]][$weekCount][$date1] = date('D', $current);

        if (date('D', $current) == 'Sun') {
            $weekNumber++;
            $weekCount++;
        }

        $current = strtotime($step, $current);
    }
    return $dates;
}

function numberToDay($key = false) {
    $myDayAry = array(1 => 'Mon', 2 => 'Tue', 3 => 'Wed', 4 => 'Thu', 5 => 'Fri', 6 => 'Sat', 7 => 'Sun');
    if ($key)
        return $myDayAry[$key];
    else
        return $myDayAry;
}

function DateFormate($date, $formate) {
    return date($formate, strtotime($date));
}

function strTime($time) {
    return DateTime::createFromFormat('H:i', date('H:i', strtotime($time)));
}

function strTimes($time) {
    return (date('H:i', strtotime($time)));
}

function getLatLong($address) {

    error_reporting(0);

    $ci = & get_instance();
    if (!empty($address)) {
        $formattedAddr = str_replace(' ', '+', $address);
        $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . $formattedAddr . '&sensor=false&key=' . GOOGLE_MAP_KEY);
        $output = json_decode($geocodeFromAddr);

        $data = array();
        if ($output->results) {
            $data['lat'] = $output->results[0]->geometry->location->lat;
            $data['long'] = $output->results[0]->geometry->location->lng;

            if (!empty($data)) {
                return $data;
            } else {
                return false;
            }
        }
    }
}

function get_client_ip_server() {
    $ipaddress = '';
    if (array_key_exists('HTTP_CLIENT_IP', @$_SERVER)) {
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (array_key_exists('HTTP_X_FORWARDED_FOR', @$_SERVER)) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } elseif (array_key_exists('HTTP_X_FORWARDED', @$_SERVER)) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    } elseif (array_key_exists('HTTP_FORWARDED_FOR', @$_SERVER)) {
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    } elseif (array_key_exists('HTTP_FORWARDED', @$_SERVER)) {
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    } elseif (array_key_exists('REMOTE_ADDR', @$_SERVER)) {
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    } else {
        $ipaddress = 'UNKNOWN';
    }
    $ipaddresses = explode(',', $ipaddress);
    $ipaddress = isset($ipaddresses[0]) ? $ipaddresses[0] : 0;
    return $ipaddress;
}

function verify_server_request() {
    $CI = & get_instance();
    $request_server = $_SERVER['HTTP_ORIGIN'];

    $servers = $CI->config->item('request_accept_server');

    if (in_array($request_server, $servers)) {
        return true;
    } else {
        echo json_encode(array('status' => false, 'server_status' => true, 'error' => system_msgs('server_error')));
        exit();
    }
}

function get_profile_complete($user_type, $user_id) {
    /* Name = 5,DOB = 5,Gender = 5,NDIS = 5,Email = 10,Phone = 10,address = 10, Medicare = 5,CRN = 5, preffered lang = 5, language inter = 5, Hearing intrepe = 5, support required = 5,assistance required = 5, oc service = 5,Preference (Places and activity) 10 */
    $CI = & get_instance();
    /* $user_type ='MEMBER';
      $user_id = '3'; */

    $initial_per = 0;
    if ($user_type == 'PARTICIPANT') {
        $colown = array("tbl_participant.gender", "tbl_participant.gender", "tbl_participant.dob", "tbl_participant.crn_num", "tbl_participant.ndis_num", "tbl_participant.medicare_num", "tbl_participant_email.email", "tbl_participant_phone.phone", "tbl_participant_address.street as address", "tbl_participant_care_requirement.preferred_language", "tbl_participant_care_requirement.linguistic_interpreter", "tbl_participant_care_requirement.hearing_interpreter", "tbl_participant_care_requirement.require_assistance_other", "tbl_participant_care_requirement.support_require_other");

        $CI->db->select($colown);
        $CI->db->select(array("concat(tbl_participant.firstname,' ',tbl_participant.middlename,' ',tbl_participant.lastname) as full_name", false));
        $CI->db->from('tbl_participant');
        $CI->db->join('tbl_participant_email', 'tbl_participant_email.participantId = tbl_participant.id AND tbl_participant_email.primary_email = 1', 'LEFT');
        $CI->db->join('tbl_participant_phone', 'tbl_participant_phone.participantId = tbl_participant.id AND tbl_participant_phone.primary_phone = 1', 'LEFT');
        $CI->db->join('tbl_participant_address', 'tbl_participant_address.participantId = tbl_participant.id AND tbl_participant_address.primary_address = 1', 'LEFT');
        $CI->db->join('tbl_participant_care_requirement', 'tbl_participant_care_requirement.participantId = tbl_participant.id', 'LEFT');
        $CI->db->where(array('tbl_participant.id' => $user_id));
        $query = $CI->db->get();
        $row = $query->row_array();

        if (!empty($row)) {
            isset($row['full_name']) ? $initial_per = $initial_per + 5 : '';
            isset($row['dob']) ? $initial_per = $initial_per + 5 : '';
            isset($row['gender']) ? $initial_per = $initial_per + 5 : '';
            isset($row['email']) ? $initial_per = $initial_per + 10 : '';
            isset($row['phone']) ? $initial_per = $initial_per + 10 : '';
            isset($row['address']) ? $initial_per = $initial_per + 10 : '';
            isset($row['medicare_num']) ? $initial_per = $initial_per + 5 : '';
            isset($row['crn_num']) ? $initial_per = $initial_per + 5 : '';
            isset($row['preferred_language']) ? $initial_per = $initial_per + 5 : '';
            isset($row['linguistic_interpreter']) ? $initial_per = $initial_per + 5 : '';
            isset($row['hearing_interpreter']) ? $initial_per = $initial_per + 5 : '';
            isset($row['support_require_other']) ? $initial_per = $initial_per + 5 : '';
            isset($row['require_assistance_other']) ? $initial_per = $initial_per + 5 : '';
        }

        /* for OC services */
        $CI->db->from('tbl_participant_oc_services');
        $CI->db->select("oc_service");
        $where_ary = array('participantId' => $user_id);
        $CI->db->where($where_ary, null, false);
        $query = $CI->db->get() or die('MySQL Error: ' . $CI->db->_error_number());
        $oc_service_row = $query->row_array();

        if (!empty($oc_service_row))
            $initial_per = $initial_per + 5;

        /* For places and activity */
        $CI->db->from('tbl_participant_place');
        $CI->db->select("placeId");
        $where_ary = array('participantId' => $user_id);
        $CI->db->where($where_ary, null, false);
        $query = $CI->db->get() or die('MySQL Error: ' . $CI->db->_error_number());
        $place_row = $query->row_array();

        if (!empty($place_row))
            $initial_per = $initial_per + 5;

        $CI->db->from('tbl_participant_activity');
        $CI->db->select("activityId");
        $where_ary = array('participantId' => $user_id);
        $CI->db->where($where_ary, null, false);
        $query = $CI->db->get() or die('MySQL Error: ' . $CI->db->_error_number());
        $activity_row = $query->row_array();

        if (!empty($activity_row))
            $initial_per = $initial_per + 5;
    }
    else if ($user_type == 'MEMBER') {
        /* Name = 10,DOB = 10,Gender = 10,Email = 10,Phone = 10,address = 10, docs = 10        
          Preference (Places and activity) 30 */

        $CI->db->select("CONCAT(tbl_member.firstname,' ',tbl_member.middlename, ' ', tbl_member.lastname) AS full_name", FALSE);
        $dt_query = $CI->db->select(array('tbl_member.dob', 'tbl_member.gender', 'tbl_member_email.email', 'tbl_member_phone.phone', 'tbl_member_address.street as address'));
        $CI->db->from('tbl_member');
        $CI->db->join('tbl_member_email', 'tbl_member_email.memberId = tbl_member.id AND tbl_member_email.primary_email = 1', 'LEFT');
        $CI->db->join('tbl_member_phone', 'tbl_member_phone.memberId = tbl_member.id AND tbl_member_phone.primary_phone = 1', 'LEFT');
        $CI->db->join('tbl_member_address', 'tbl_member_address.memberId = tbl_member.id AND tbl_member_address.primary_address = 1', 'LEFT');

        $sWhere = array('tbl_member.id' => $user_id);
        $CI->db->where($sWhere, null, false);
        $query = $CI->db->get();
        $row_member = $query->row_array();

        if (!empty($row_member)) {
            isset($row_member['full_name']) ? $initial_per = $initial_per + 10 : '';
            isset($row_member['dob']) ? $initial_per = $initial_per + 10 : '';
            isset($row_member['gender']) ? $initial_per = $initial_per + 10 : '';
            isset($row_member['email']) ? $initial_per = $initial_per + 10 : '';
            isset($row_member['phone']) ? $initial_per = $initial_per + 10 : '';
            isset($row_member['address']) ? $initial_per = $initial_per + 10 : '';
        }

        /* For places and activity */
        $CI->db->from('tbl_member_place');
        $CI->db->select("placeId");
        $where_ary = array('memberId' => $user_id);
        $CI->db->where($where_ary, null, false);
        $query = $CI->db->get() or die('MySQL Error: ' . $CI->db->_error_number());
        $place_row = $query->row_array();

        if (!empty($place_row))
            $initial_per = $initial_per + 15;

        $CI->db->from('tbl_member_activity');
        $CI->db->select("activityId");
        $where_ary = array('memberId' => $user_id);
        $CI->db->where($where_ary, null, false);
        $query = $CI->db->get() or die('MySQL Error: ' . $CI->db->_error_number());
        $activity_row = $query->row_array();

        if (!empty($activity_row))
            $initial_per = $initial_per + 15;

        /* For docs */
        $CI->db->from('tbl_member_qualification');
        $CI->db->select("id");
        $where_ary = array('memberId' => $user_id, 'archive' => 0);
        $CI->db->where($where_ary, null, false);
        $query = $CI->db->get() or die('MySQL Error: ' . $CI->db->_error_number());
        $docs_row = $query->row_array();

        if (!empty($docs_row))
            $initial_per = $initial_per + 10;
    }
    return $initial_per;
}

function do_upload($config_ary) {
    $CI = & get_instance();
    $response = array();
    if (!empty($config_ary)) {
        $directory_path = $config_ary['upload_path'] . $config_ary['directory_name'];

        $config['upload_path'] = $directory_path;
        $config['allowed_types'] = isset($config_ary['allowed_types']) ? $config_ary['allowed_types'] : '';
        $config['max_size'] = isset($config_ary['max_size']) ? $config_ary['max_size'] : '';
        $config['max_width'] = isset($config_ary['max_width']) ? $config_ary['max_width'] : '';
        $config['max_height'] = isset($config_ary['max_height']) ? $config_ary['max_height'] : '';

        create_directory($directory_path);

        $CI->load->library('upload', $config);

        if (!$CI->upload->do_upload($config_ary['input_name'])) {
            $response = array('error' => $CI->upload->display_errors());
        } else {
            $response = array('upload_data' => $CI->upload->data());
        }
    }
    return $response;
}

function create_directory($directoryName) {
    if (!is_dir($directoryName)) {
        mkdir($directoryName, 0755);
    }
}

function AddNotification($userId, $title, $notification) {
    $NotificationData = array();
    $NotificationData['userId'] = $userId;
    $NotificationData['title'] = $title;
    $NotificationData['shortdescription'] = $notification;
    $NotificationData['user_type'] = 2;
    $NotificationData['created'] = DATE_TIME;
    $NotificationData['status'] = 0;
    $tableNotification = TBL_PREFIX . 'notification';
    $CI = & get_instance();
    $insert_query = $CI->db->insert($tableNotification, $NotificationData);
    $notificationId = $CI->db->insert_id();

    $wbObj = new Websocket();

    if ($wbObj->check_webscoket_on()) {
        $data = array('chanel' => 'server', 'req_type' => 'client_update_notification', 'token' => $wbObj->get_token(), 'data' => ['notificationId' => $notificationId]);
        $wbObj->send_data_on_socket($data);
    }
    
    if ($CI->db->affected_rows() > 0) {
        return 1;
    } else {
        return 0;
    }
}

function setting_length($x, $length) {
    if (strlen($x) <= $length) {
        return $x;
    } else {
        $y = substr($x, 0, $length) . '...';
        return $y;
    }
}