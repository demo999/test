<?php

use classRoles as adminRoles;

defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class Auth extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Participant_model');
        $this->load->helper(array('comman', 'auth'));
        if (!$this->session->userdata('username')) {
            // redirect('');
        }
    }

    public function index() {
        $this->render_view('components/admin//dashboard/AppDashboard');
    }

    public function client_login() {
        $this->load->helper('cookie');
        require_once APPPATH . 'Classes/participant/ParticipantLogin.php';
        $objparticipant = new ParticipantLoginClass\ParticipantLogin();
        $remeber = 1; //$_POST['remember'];		
        $response = array();
        if (empty($_POST['username']) || !isset($_POST['username'])) {
            $response = array('status' => false, 'error' => system_msgs('username_empty'));
        } elseif (empty($_POST['password']) || !isset($_POST['password'])) {
            $response = array('status' => false, 'error' => system_msgs('password_empty'));
        } else {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $currunt_date = date("Y-m-d H:i:s");
            $objparticipant->setLoginUpdate($currunt_date);
            $objparticipant->setLoginCreated($currunt_date);
            $objparticipant->setUserName($username);
            $objparticipant->setClientPassword($password);
            $response = $objparticipant->check_auth();
        }
        echo json_encode($response);
    }

    public function client_forget_password() {
        $this->load->helper('email_template_helper');
        $reqData = (object) $_POST;
        // get request data
        if (!empty($reqData)) {
            $username = $reqData->username;
            $where = array('username' => $username);
            $result = $this->basic_model->get_row('participant', $column = array('id', 'firstname', 'lastname', 'status'), $where);
            if (!empty($result)) {
                $participantEmail = $this->basic_model->get_row('participant_email', $column = array('email'), $where = array('primary_email' => 1, 'participantId' => $result->id));
                $rand = mt_rand(10, 100000);
                $token = encrypt_decrypt('encrypt', $rand);
                $where = array('id' => $result->id);
                $this->basic_model->update_records('participant', $data = array('token' => $token), $where);
                $userdata = array(
                    'firstname' => $result->firstname,
                    'lastname' => $result->lastname,
                    'email' => $participantEmail->email,
                    'url' => $this->config->item('server_url') . "reset_password/" . encrypt_decrypt('encrypt', $result->id) . '/' . $token . '/' . encrypt_decrypt('encrypt', strtotime(DATE_TIME)),
                );
                forgot_password_mail($userdata, $cc_email_address = null);
                $response = array('status' => true, 'success' => system_msgs('forgot_password_send_mail_succefully'));
            } else {
                $response = array('status' => false, 'error' => 'Your username not exist');
            }
            echo json_encode($response);
        }
    }

    public function verify_reset_password_token() {
        require_once APPPATH . 'Classes/participant/ParticipantLogin.php';
        // get request data
        $reqData = (object) $_POST;
        if ($reqData) {
            $user_id = encrypt_decrypt('decrypt', $reqData->id);
            $obj = new ParticipantLoginClass\ParticipantLogin();
            $obj->setParticipantid($user_id);
            $obj->setParticipantToken($reqData->token);
            $recieve_date_time = encrypt_decrypt('decrypt', $reqData->dateTime);
            $diff = strtotime(DATE_TIME) - $recieve_date_time;
            if ($diff > 3600) {
                $response = array('status' => false, 'error' => 'Your link is expire');
            } else {
                $result = $obj->verify_token();
                if (!empty($result)) {
                    $response = array('status' => true);
                } else {
                    $response = array('status' => false, 'error' => system_msgs('verfiy_password_error'));
                }
            }
            echo json_encode($response);
        }
    }

    function reset_password() {
        require_once APPPATH . 'Classes/participant/ParticipantLogin.php';
        // get request data
        $reqData = (object) $_POST;
        $obj = new ParticipantLoginClass\ParticipantLogin();
        if ($reqData) {
            $user_id = encrypt_decrypt('decrypt', $reqData->auth['id']);
            $obj->setParticipantid($user_id);
            $obj->setParticipantToken($reqData->auth['token']);
            $result = $obj->verify_token();
            if (!empty($result)) {
                $obj->setClientPassword($reqData->password);
                $obj->reset_password();
                echo json_encode(array('status' => true, 'success' => system_msgs('password_reset_successfully')));
            } else {
                echo json_encode(array('status' => false, 'error' => system_msgs('verfiy_password_error')));
            }
        } else {
            echo json_encode(array('status' => false, 'error' => system_msgs('verfiy_password_error')));
        }
    }

    function profile_pic() {
        $participantId = $_POST['id'];
        $participantToken = $_POST['token'];
        $clientResponse = client_auth_status_by_token($participantToken, $participantId);
        if (!$clientResponse['status']) {
            echo json_encode($clientResponse);
            exit;
        }
        require_once APPPATH . 'Classes/participant/ParticipantDocs.php';
        $docObj = new ParticipantDocClass\ParticipantDocs();
        if (!empty($_FILES) && $_FILES['myFile']['error'] == 0) {
            $docObj->setParticipantid($participantId);
            $docObj->setCreated(DATE_TIME);
            $config['upload_path'] = PARTICIPANT_UPLOAD_PATH;
            $config['input_name'] = 'myFile';
            $config['directory_name'] = $participantId;
            $config['allowed_types'] = 'jpg|jpeg|png';

            $is_upload = do_upload($config);
            if (isset($is_upload['error'])) {
                echo json_encode(array('status' => false, 'error' => strip_tags($is_upload['error'])));
                exit();
            } else {
                $docObj->setFilename($is_upload['upload_data']['file_name']);
                $rows = $docObj->UpdateProfileImage();
                $return = array('status' => true);
                echo json_encode($return);
                exit();
            }
        } else {
            echo json_encode(array('status' => false, 'error' => 'Error in uploading file.'));
            exit();
        }
    }

}
