<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Shift_Roster
 *
 * @author user Corner stone solution
 */
class Shift_Roster extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation', 'Logs_lib'));
        $this->load->model('Participant_model');
        $this->load->model('Shift_roster_model');
        $this->load->helper(array('comman', 'auth'));
        $clientResponse = client_auth_status();
        if (!$clientResponse['status']) {
            echo json_encode($clientResponse);
            exit;
        }
    }

    // Here to get participant roster info
    public function get_previous_rosters($ClientToken = null) {
        $reqData = request_handler();
        $client_id = get_client_id();
        require_once APPPATH . 'Classes/participant/ParticipantRoster.php';
        $objRoster = new \Roster\ParticipantRoster();
        $objRoster->setParticipantid($client_id);
        $response = $objRoster->getPreviousRoster();
        $arrResponse = array('status' => true, 'data' => $response);
        echo json_encode($arrResponse);
    }

    public function check_collapse_shifts() {
        $reqData = request_handler();
        $client_id = get_client_id();
        $collapse_shifts_count = 0;
        if ($reqData) {

            $response_valid = $this->validate_roster_data((array) $reqData);
            if ($response_valid['status']) {

                if (!$reqData->start_date) {
                    echo json_encode(array('status' => false, 'error' => 'Please select Start date first'));
                    exit();
                }
                if (empty($reqData->rosterList)) {
                    echo json_encode(array('status' => false, 'error' => 'Roster list not available'));
                    exit();
                }
                $roster = array('start_date' => $reqData->start_date, 'participantId' => $client_id, 'status' => 1, 'created' => DATE_TIME);
                $roster['is_default'] = ($reqData->is_default == 1) ? 1 : 0;
                if ($roster['is_default'] == 1) {
                    $roster['end_date'] = $reqData->end_date;
                } else {
                    $roster['end_date'] = date('Y-m-d', strtotime($roster['start_date'] . ' + 56 days'));
                }
                // get shift round
                $shift_round = $this->get_shift_round($reqData->rosterList);
                // make roster data
                $roster_data = $this->make_roster_data($reqData->rosterList, false);
                if (empty($roster_data)) {
                    echo json_encode(array('status' => false, 'error' => 'Please select at least one'));
                    exit();
                }
                $new_roster_shifts = $this->create_shift_from_roster((object) $roster, $roster_data, $shift_round);
                $new_shiftDate = array_column($new_roster_shifts, 'shift_date');
                $old_shifts = array();
                if (!empty($new_shiftDate)) {
                    $old_shifts = $this->Shift_roster_model->get_previous_shifts($roster['participantId'], $new_shiftDate);
                }
                $collapse_shifts = array();
                $oldShiftDates = array();
                if (!empty($old_shifts)) {
                    $oldShiftDates = array_column($old_shifts, 'shift_date');
                }
                $array_status = array(1 => 'Unfilled', 2 => 'Unconfirmed', 3 => 'Quote', 4 => 'Rejected', 5 => 'Cancelled', 6 => 'Confirmed');
                // if its specail roster check anothter specail roster already exist in this date
                //else check defualt roster already pending for aprroval
                if ($roster['is_default'] == 1) {
                    $checkResult = $this->Shift_roster_model->check_roster_start_end_date_already_exist($roster);
                    if (!empty($checkResult)) {
                        echo json_encode(array('status' => false, 'error' => 'On this date your another roster already exist'));
                        exit();
                    }
                } else {
                    $checkResult = $this->basic_model->get_record_where('participant_roster', array('start_date'), $where = array('status' => 2, 'is_default' => 0, 'participantId' => $client_id));
                    if (!empty($checkResult)) {
                        echo json_encode(array('status' => false, 'error' => 'Your default roster request already pending please wait for approval'));
                        exit();
                    }
                }
                if (!empty($new_roster_shifts)) {
                    foreach ($new_roster_shifts as $key => $val) {
                        $collapse_shifts[$key]['is_collapse'] = false;
                        $responseMatchedDate = $this->mappingDate($oldShiftDates, DateFormate($val['shift_date'], 'd-m-Y'));
                        $indexKey = 0;
                        if (!empty($responseMatchedDate)) {
                            foreach ($responseMatchedDate as $index => $old_shift_key) {
                                $o_str_time = $old_shifts[$old_shift_key]['start_time'];
                                $o_end_time = $old_shifts[$old_shift_key]['end_time'];
                                $condtion1 = (((strTime($val['start_time']) < strTime($o_str_time)) && (strTime($o_str_time) < strTime($val['end_time']))));
                                $condtion2 = (((strTime($val['start_time']) < strTime($o_end_time)) && (strTime($o_end_time) < strTime($val['end_time']))));
                                $condtion3 = (((strTime($o_str_time) < strTime($val['start_time'])) && (strTime($val['end_time']) < strTime($o_str_time))));
                                $condtion4 = (((strTime($o_end_time) < strTime($val['start_time'])) && (strTime($val['end_time']) < strTime($o_end_time))));
                                if ($condtion1 || $condtion2 || $condtion3 || $condtion4) {
                                    $collapse_shifts_count++;
                                    $collapse_shifts[$key]['is_collapse'] = true;
                                    $collapse_shifts[$key]['status'] = 2;
                                    $collapse_shifts[$key]['old'][$indexKey] = $old_shifts[$old_shift_key];
                                    $collapse_shifts[$key]['old'][$indexKey]['status'] = $array_status[$old_shifts[$old_shift_key]['status']];
                                    $collapse_shifts[$key]['old'][$indexKey]['active'] = false;
                                    $indexKey++;
                                }
                            }
                        }
                        $collapse_shifts[$key]['new'] = $val;
                        $startTime = new DateTime($val['start_time']);
                        $endTime = new DateTime($val['end_time']);
                        $interval = $endTime->diff($startTime);
                        $difference = $interval->format('%H.%i hrs.');
                        $collapse_shifts[$key]['new']['start_time'] = DateFormate($val['start_time'], 'h:i a');
                        $collapse_shifts[$key]['new']['end_time'] = DateFormate($val['end_time'], 'h:i a');
                        $collapse_shifts[$key]['new']['shift_date'] = DateFormate($val['shift_date'], 'd-m-Y');
                        $collapse_shifts[$key]['new']['duration'] = $difference;
                        $collapse_shifts[$key]['new']['status'] = 'Unfilled';
                    }
                }
                echo json_encode(array('status' => true, 'data' => $collapse_shifts, 'count' => $collapse_shifts_count));
            } else {
                echo json_encode($response_valid);
            }
        } else {
            echo json_encode(array('status' => false, 'error' => system_msgs('something_went_wrong')));
        }
    }

    function get_shift_round($rosterList) {
        $shift_round = array();
        if (!empty($rosterList)) {
            foreach ($rosterList as $key => $val) {
                $shift_round[] = $key + 1;
            }
        }
        return $shift_round;
    }

    function mappingDate($shiftDates, $singleDate) {
        $matchDateKey = array();
        if (!empty($shiftDates)) {
            foreach ($shiftDates as $key => $val) {
                if ($val == $singleDate) {
                    $matchDateKey[] = $key;
                }
            }
            return $matchDateKey;
        }
    }

    public function make_roster_data($data, $rosterId = false) {
        foreach ($data as $key => $weekData) {
            $dayCounter = 1;
            foreach ($weekData as $day => $multipleShift) {
                foreach ($multipleShift as $val) {
                    if ($val->is_active) {
                        $start_time = date('Y-m-d H:i:s', strtotime($val->start_time));
                        $end_time = date('Y-m-d H:i:s', strtotime($val->end_time));
                        $roster_data = array('week_day' => $dayCounter, 'start_time' => $start_time, 'end_time' => $end_time, 'week_number' => ($key + 1));
                        if (!empty($rosterId))
                            $roster_data['rosterId'] = $rosterId;
                        $rosters[] = $roster_data;
                    }
                }
                $dayCounter++;
            }
        }
        return $rosters;
    }

    function get_upcoming_shift() {
        $client_id = get_client_id();
        $reqData = request_handler();
        if (!empty($reqData)) {
            $result = $this->Shift_roster_model->participant_upcoming_shifts($reqData, $client_id);
            echo json_encode(array('status' => true, 'data' => $result));
        }
    }

    function get_shift_list_for_feedback() {
        $client_id = get_client_id();
        $reqData = request_handler();
        require_once APPPATH . 'Classes/participant/ParticipantShift.php';
        $objShift = new ShiftClass\Shift();
        if (!empty($reqData)) {
            $objShift->setShiftDate(DateFormate($reqData->shift_date, 'Y-m-d'));
            $shifts = $objShift->getCompletedShiftByDate($client_id);
            echo json_encode(array('status' => true, 'data' => $shifts));
        }
    }

    function get_ongoing_feedback() {
        /* $client_id = get_client_id();
          require_once APPPATH . 'Classes/fms/Fms.php';
          $objFms = new FmsClass\Fms();
          $objFms->setInitiated_by($client_id);
          $objFms->setInitiated_type(2);
          $result = $objFms->getOngoingFeedback();
          echo json_encode(array('status' => true, 'data' => $result));
         */
        $client_id = get_client_id();
        require_once APPPATH . 'Classes/fms/Fms.php';
        $objFms = new FmsClass\Fms();
        $objFms->setInitiated_by($client_id);
        $objFms->setInitiated_type(2);
        $result = $objFms->getOngoingFeedback();
        echo json_encode(array('status' => true, 'data' => $result));
    }

    function get_feedback_case_details() {
        $client_id = get_client_id();
        $reqData = request_handler();
        $case_id = $reqData->case_id;
        require_once APPPATH . 'Classes/fms/Fms.php';
        $objFms = new FmsClass\Fms();
        $objFms->setInitiated_by($client_id);
        $objFms->setInitiated_type(2);
        $objFms->setId($case_id);
        $result = $objFms->get_feedback_case_details();
        echo json_encode(array('status' => true, 'data' => $result));
    }

    function get_feedback_case_note_details() {
        $client_id = get_client_id();
        $reqData = request_handler();
        $case_id = $reqData->case_id;
        require_once APPPATH . 'Classes/fms/Fms.php';
        $objFms = new FmsClass\Fms();
        $objFms->setInitiated_by($client_id);
        $objFms->setInitiated_type(2);
        $objFms->setId($case_id);
        $result = $objFms->get_feedback_case_note_details();
        echo json_encode(array('status' => true, 'data' => $result));
    }

    function get_feedback_case_region_details() {
        $client_id = get_client_id();
        $reqData = request_handler();
        $case_id = $reqData->case_id;
        require_once APPPATH . 'Classes/fms/Fms.php';
        $objFms = new FmsClass\Fms();
        $objFms->setInitiated_by($client_id);
        $objFms->setInitiated_type(2);
        $objFms->setId($case_id);
        $result = $objFms->get_feedback_case_region_details();
        echo json_encode(array('status' => true, 'data' => $result));
    }

    function get_member_by_shift() {
        $client_id = get_client_id();
        $reqData = request_handler();
        require_once APPPATH . 'Classes/participant/ParticipantShift.php';
        $objShift = new ShiftClass\Shift();
        $objShift->setParticipantid($client_id);
        $objShift->setShiftId($reqData->shiftId);
        $result = $objShift->getMemberByShiftId($objShift);
        echo json_encode(array('status' => true, 'data' => $result));
    }

    function get_shift_details_by_id() {
        $client_id = get_client_id();
        $reqData = request_handler();
        require_once APPPATH . 'Classes/participant/ParticipantShift.php';
        $objShift = new ShiftClass\Shift();
        $objShift->setParticipantid($client_id);
        $objShift->setShiftId($reqData->shiftId);
        $result = $objShift->getShiftByShiftId($objShift);
        echo json_encode(array('status' => true, 'data' => $result));
    }

    function get_feedback_category_list() {
        $client_id = get_client_id();
        require_once APPPATH . 'Classes/participant/Participant.php';
        $objShift = new ParticipantClass\Participant();
        $result = $objShift->get_feedback_category($objShift);
        echo json_encode(array('status' => true, 'data' => $result));
    }

    function create_shift_from_roster($rosterData = false, $rosterDayData = false, $totalWeek = false) {
        $shiftRoster = array();
        $newRosterData = array();
        if (!empty($rosterDayData)) {
            foreach ($rosterDayData as $val) {
                $newRosterData[$val['week_number']][numberToDay($val['week_day'])] = array('start_time' => $val['start_time'], 'end_time' => $val['end_time']);
            }
        }
        $shiftCreateEndDate = date('Y-m-d', strtotime(date("Y-m-d") . ' + 57 days'));
        $date_range = dateRangeBetweenDateWithWeek($rosterData->start_date, $shiftCreateEndDate, $totalWeek);
        $shifts = array();
        if (!empty($date_range)) {
            foreach ($date_range as $week_number => $week) {
                foreach ($week as $days) {
                    foreach ($days as $date => $day) {
                        if (array_key_exists($week_number, $newRosterData)) {
                            if (array_key_exists($day, $newRosterData[$week_number])) {
                                $start_time = $newRosterData[$week_number][$day]['start_time'];
                                $end_time = $newRosterData[$week_number][$day]['end_time'];
                                $shift = array('booked_by' => 2, 'shift_date' => $date, 'start_time' => $start_time, 'end_time' => $end_time, 'status' => 1, 'created' => DATE_TIME, 'updated' => DATE_TIME);
                                $shiftRoster[] = $shift;
                            }
                        }
                    }
                }
            }
        }
        return $shiftRoster;
    }

    public function create_roster() {
        require_once APPPATH . 'Classes/participant/ParticipantRoster.php';
        $objRoster = new Roster\ParticipantRoster();
        $client_id = get_client_id();
        $reqData = request_handler();
        if ($reqData) {

            $response_valid = $this->validate_roster_data((array) $reqData);
            if ($response_valid['status']) {

                $roster_listing = $reqData->rosterList;
                if (!empty($roster_listing)) {
                    // get shift round
                    $shift_round = json_encode($this->get_shift_round($roster_listing));
                    $objRoster->setParticipantId($client_id);
                    $objRoster->setStartDate(date("Y-m-d", strtotime($reqData->start_date)));
                    $objRoster->setCreated(DATE_TIME);
                    $objRoster->setStatus(2); // set pending status
                    $objRoster->setShift_round($shift_round);
                    $is_default = ($reqData->is_default == 1) ? 1 : 0;
                    $objRoster->setIsDefault($is_default);
                    if ($is_default == 1) {
                        $objRoster->setTitle($reqData->title);
                        $objRoster->setEndDate(date("Y-m-d", strtotime($reqData->end_date)));
                    }
                    // start for rollback
                    $this->db->trans_begin();
                    if (!empty($reqData->rosterId)) {
                        // update roster
                        $rosterId = $reqData->rosterId;
                        $objRoster->setRosterId($rosterId);
                        $objRoster->updateRoster();
                    } else {
                        // create roster
                        $rosterId = $objRoster->createRoster();
                    }
                    // make roster data
                    $roster_data = $this->make_roster_data($roster_listing, $rosterId);
                    if (!empty($roster_data)) {
                        $this->db->trans_commit();
                        // first delete all data and then insert
                        $this->basic_model->delete_records('participant_roster_data', $where = array('id' => $rosterId));
                        $this->basic_model->insert_records('participant_roster_data', $roster_data, $multiple = true);
                        // check if collapse shift found
                        if (!empty($reqData->collapse_shift)) {
                            $rosterTempData = array('rosterId' => $rosterId, 'rosterData' => json_encode($reqData->collapse_shift));
                            $this->basic_model->insert_records('participant_roster_temp_data', $rosterTempData);
                        }
                        // Notification Add New Shift
                        $arrNotification = notification_msgs('add_request_roster');
                        AddNotification($client_id, $arrNotification['title'], $arrNotification['description']);
                        echo json_encode(array('status' => true));
                    } else {
                        // for previous data remove from roster table
                        $this->db->trans_rollback();
                        echo json_encode(array('status' => false, 'error' => 'Please select at least one day'));
                    }
                }
            } else {
                echo json_encode($response_valid);
            }
        } else {
            echo json_encode(array('status' => false, 'error' => system_msgs('something_went_wrong')));
        }
    }

    function create_shift_old() {
        $client_id = get_client_id();
        $reqData = request_handler();
        require_once APPPATH . 'Classes/participant/ParticipantShift.php';
        $objShift = new ShiftClass\Shift();
        require_once APPPATH . 'Classes/participant/ShiftLocation.php';
        if (!empty($reqData)) {
            $objShift->setShiftDate(DateFormate($reqData->shift_date, 'Y-m-d'));
            $objShift->setStartTime(DateFormate($reqData->start_time, 'H:i:s'));
            $objShift->setEndTime(DateFormate($reqData->end_time, 'H:i:s'));
            $objShift->setStatus(1);
            $objShift->setCreated(DATE_TIME);
            $response = $objShift->checkShiftDateTimeAlreadyExist($client_id);
            if (!empty($response)) {
                echo json_encode(array('status' => false, 'error' => 'On this date another shift already exist'));
            } else {
                $startDateTime = $this->concatDateTime($objShift->getShiftDate(), $objShift->getStartTime());
                $EndDateTime = $this->concatDateTime($objShift->getShiftDate(), $objShift->getEndTime());
                $reqData->start_time = $startDateTime;
                $reqData->end_time = $EndDateTime;
                $objShift->setStartTime($startDateTime);
                $objShift->setEndTime($EndDateTime);
                // Add Approval table
                $ApprovalData = array();
                $ApprovalData['userId'] = $client_id;
                $ApprovalData['user_type'] = 2;
                $ApprovalData['approval_area'] = 'ShiftCreate';
                $ApprovalData['approval_content'] = json_encode($reqData);
                $ApprovalData['created'] = DATE_TIME;
                $ApprovalData['status'] = 0;
                $this->basic_model->insert_records('approval', $ApprovalData);
                // create shift
                $shiftId = $objShift->create_shift();
                // Notification Add New Shift
                $arrNotification = notification_msgs('add_new_shift');
                AddNotification($client_id, $arrNotification['title'], $arrNotification['description']);
                if (!empty($reqData->location)) {
                    foreach ($reqData->location as $val) {
                        $objLocation = new ShiftLocation();
                        $objLocation->setAddress($val->address);
                        $objLocation->setPostal($val->postal);
                        $objLocation->setShiftid($shiftId);
                        $objLocation->setState($val->state);
                        $objLocation->setSuburb($val->suburb->value);
                        // get address lat long
                        $latLong = getLatLong($objLocation->getAddress() . ' ' . $objLocation->getSuburb() . ' ' . $objLocation->getState());
                        if (!empty($latLong)) {
                            $objLocation->setLat($latLong['lat']);
                            $objLocation->setLong($latLong['long']);
                        }
                        //create shift location
                        $objLocation->createShiftLocation();
                    }
                }
                // get participant information
                $participantInfo = $this->Shift_roster_model->get_participant_shift_related_information($client_id);
                // insert participant data
                $shift_participant = array('shiftId' => $shiftId, 'participantId' => $client_id, 'status' => 1, 'created' => DATE_TIME);
                $this->basic_model->insert_records('shift_participant', $shift_participant, $multiple = FALSE);
                // insert confirmation detials
                $shift_confirmation = array('firstname' => $participantInfo->firstname, 'lastname' => $participantInfo->lastname, 'email' => $participantInfo->email, 'phone' => $participantInfo->phone, 'confirm_by' => $participantInfo->prefer_contact, 'confirm_with' => 1, 'shiftId' => $shiftId);
                $this->basic_model->insert_records('shift_confirmation', $shift_confirmation, $multiple = FALSE);
                // insert shift requirement
                $shiftRequirement = array();
                if (!empty($reqData->requirement)) {
                    foreach ($reqData->requirement as $val) {
                        if (!empty($val->checked)) {
                            $shiftRequirement[] = array('requirementId' => $val->value, 'shiftId' => $shiftId);
                        }
                    }
                    if (!empty($shiftRequirement))
                        $this->basic_model->insert_records('shift_requirements', $shiftRequirement, $multiple = true);
                }elseif (!empty($participantInfo->requirement)) {
                    foreach ($participantInfo->requirement as $val) {
                        $shiftRequirement[] = array('requirementId' => $val->assistanceId, 'shiftId' => $shiftId);
                    }
                    $this->basic_model->insert_records('shift_requirements', $shiftRequirement, $multiple = true);
                }
                // prefered member.
                $preferedMemer = array();
                if (!empty($reqData->preferred_member_ary)) {
                    foreach ($reqData->preferred_member_ary as $val) {
                        if (!empty($val->name)) {
                            $preferedMemer[] = array('memberId' => $val->name->value, 'shiftId' => $shiftId);
                        }
                    }
                    if (!empty($preferedMemer))
                        $this->basic_model->insert_records('shift_preferred_member', $preferedMemer, $multiple = true);
                }
                echo json_encode(array('status' => true));
            }
        }
    }

    function check_shift_required_location($location_optional, $location_main) {
        $locations = json_decode($location_main);
        if (!empty($locations)) {
            $checked = false;
            foreach ($locations as $location) {
                if (empty($location->address)) {
                    $this->form_validation->set_message('check_shift_required_location', 'Shift address can not empty');
                    return false;
                }
                if (empty($location->state)) {
                    $this->form_validation->set_message('check_shift_required_location', 'Shift state can not empty');
                    return false;
                }
                if (empty($location->postal)) {
                    $this->form_validation->set_message('check_shift_required_location', 'Shift postcode can not empty');
                    return false;
                }
                if (empty($location->suburb->value)) {
                    $this->form_validation->set_message('check_shift_required_location', 'Shift suburb can not empty');
                    return false;
                }
            }
        } else {
            $this->form_validation->set_message('check_shift_required_location', 'participant support required can not empty');
            return false;
        }
        return true;
    }

    function validate_calendar_shift_roasterd($shift_data) {
        $participant_shiftdata = (array) $shift_data;
        try {
            $validation_rules = array(
                array('field' => 'shift_date', 'label' => 'shift_date', 'rules' => 'callback_check_shift_calendar_date_time[' . json_encode($participant_shiftdata) . ']'),
                array('field' => 'start_time', 'label' => 'start_time', 'rules' => 'required'),
                array('field' => 'end_time', 'label' => 'end_time', 'rules' => 'required'),
                array('field' => 'location_optional', 'label' => 'Location', 'rules' => 'callback_check_shift_required_location[' . json_encode($participant_shiftdata['location']) . ']')
            );
            $this->form_validation->set_data($participant_shiftdata);
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return;
    }

    function check_shift_calendar_date_time($start_time, $participant_shiftdata) {
        $shiftData = json_decode($participant_shiftdata);
        if (!empty($shiftData)) {
            /* time calculation */
            $startDate = DateFormate($shiftData->shift_date, 'Y-m-d');
            $startTime = DateFormate($shiftData->start_time, 'H:i:s');
            $endTime = DateFormate($shiftData->end_time, 'H:i:s');

            $startDateTime = $this->concatDateTime($startDate, $startTime);
            $endDateTime = $this->concatDateTime($startDate, $endTime);

            /* time calculation */
            $strTime = strtotime(DateFormate($startDateTime, 'Y-m-d H:i:s'));
            $endTime = strtotime(DateFormate($endDateTime, 'Y-m-d H:i:s'));
            $next_day_time = strtotime('+24 hours', $strTime);

            $strTimeCurrunt = strtotime(DATE_TIME);
            if (!empty($strTime)) {
                if ($strTime > $strTimeCurrunt) {
                    
                } else {
                    $this->form_validation->set_message('check_shift_calendar_date_time', 'Shift start time can not less then current time');
                    return false;
                }
            } else {
                $this->form_validation->set_message('check_shift_calendar_date_time', 'Shift start date time required can not empty');
                return false;
            }


            if (!empty($endTime)) {
                if ($endTime < $next_day_time) {
                    
                } else {
                    $this->form_validation->set_message('check_shift_calendar_date_time', 'Shift end time not greater then 24 hours');
                    return false;
                }
            } else {
                $this->form_validation->set_message('check_shift_calendar_date_time', 'Shift end date time required can not empty');
                return false;
            }

            if ($strTime > $endTime) {
                $this->form_validation->set_message('check_shift_calendar_date_time', 'Shift end time not less then start time');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_shift_calendar_date_time', 'Shift data required can not empty');
            return false;
        }
        return true;
    }

    function validate_shift_roasterd($shift_data) {
        $participant_shiftdata = (array) $shift_data;
        try {
            $validation_rules = array(
                //array('field' => 'shift_date', 'label' => 'shift_date', 'rules' => 'required'),
                array('field' => 'start_time', 'label' => 'start_time', 'rules' => 'callback_check_shift_date_time[' . json_encode($participant_shiftdata) . ']'),
                array('field' => 'end_time', 'label' => 'end_time', 'rules' => 'required'),
                array('field' => 'location_optional', 'label' => 'Location', 'rules' => 'callback_check_shift_required_location[' . json_encode($participant_shiftdata['location']) . ']')
            );
            $this->form_validation->set_data($participant_shiftdata);
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return;
    }

    function check_shift_date_time($start_time, $participant_shiftdata) {
        $shiftData = json_decode($participant_shiftdata);
        if (!empty($shiftData)) {
            $strTime = strtotime(DateFormate($shiftData->start_time, 'Y-m-d H:i:s'));
            $endTime = strtotime(DateFormate($shiftData->end_time, 'Y-m-d H:i:s'));
            $next_day_time = strtotime('+24 hours', $strTime);
            $strTimeCurrunt = strtotime(DATE_TIME);
            if (!empty($strTime)) {
                if ($strTime > $strTimeCurrunt) {
                    
                } else {
                    $this->form_validation->set_message('check_shift_date_time', 'Shift start time can not less then current time');
                    return false;
                }
            } else {
                $this->form_validation->set_message('check_shift_date_time', 'Shift start date time required can not empty');
                return false;
            }

            if (!empty($endTime)) {
                if ($endTime < $next_day_time) {
                    
                } else {
                    $this->form_validation->set_message('check_shift_date_time', 'Shift end time not greater then 24 hours');
                    return false;
                }
            } else {
                $this->form_validation->set_message('check_shift_date_time', 'Shift end date time required can not empty');
                return false;
            }

            if ($strTime > $endTime) {
                $this->form_validation->set_message('check_shift_date_time', 'Shift end time not less then start time');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_shift_date_time', 'Shift data required can not empty');
            return false;
        }
        return true;
    }

    function create_shift() {
        $client_id = get_client_id();
        $reqData = request_handler();
        require_once APPPATH . 'Classes/participant/ParticipantShift.php';
        $objShift = new ShiftClass\Shift();
        require_once APPPATH . 'Classes/participant/ShiftLocation.php';
        if (!empty($reqData)) {
            $response_valid = array();
            if ($reqData->popup) {
                $response_valid = $this->validate_calendar_shift_roasterd($reqData);
            } else {
                $response_valid = $this->validate_shift_roasterd($reqData);
            }
            if ($response_valid['status']) {
                if ($reqData->popup) {
                    $objShift->setShiftDate(DateFormate($reqData->shift_date, 'Y-m-d'));
                    $objShift->setStartTime(DateFormate($reqData->start_time, 'H:i:s'));
                    $objShift->setEndTime(DateFormate($reqData->end_time, 'H:i:s'));
                    $objShift->setStatus(1);
                    $objShift->setCreated(DATE_TIME);
                } else {
                    $shiftDate = DateFormate($reqData->start_time, 'Y-m-d');
                    $reqData->shift_date = $shiftDate;
                    $objShift->setShiftDate($shiftDate);
                    $objShift->setStartTime(DateFormate($reqData->start_time, 'Y-m-d H:i:s'));
                    $objShift->setEndTime(DateFormate($reqData->end_time, 'Y-m-d H:i:s'));
                    $objShift->setStatus(1);
                    $objShift->setCreated(DATE_TIME);
                }
                $startDateTime = $this->concatDateTime($objShift->getShiftDate(), $objShift->getStartTime());
                $EndDateTime = $this->concatDateTime($objShift->getShiftDate(), $objShift->getEndTime());
                $reqData->start_time = $startDateTime;
                $reqData->end_time = $EndDateTime;
                $objShift->setStartTime($startDateTime);
                $objShift->setEndTime($EndDateTime);
                $response = $objShift->checkShiftDateTimeAlreadyExist($client_id);
                if (!empty($response)) {
                    echo json_encode(array('status' => false, 'error' => 'On this date another shift already exist'));
                } else {
                    // Add Approval table								
                    $ApprovalData = array();
                    $ApprovalData['userId'] = $client_id;
                    $ApprovalData['user_type'] = 2;
                    $ApprovalData['approval_area'] = 'ShiftCreate';
                    $ApprovalData['approval_content'] = json_encode($reqData);
                    $ApprovalData['created'] = DATE_TIME;
                    $ApprovalData['status'] = 0;
                    $this->basic_model->insert_records('approval', $ApprovalData);
                    // create shift
                    // Notification Add New Shift
                    $arrNotification = notification_msgs('add_new_shift');
                    AddNotification($client_id, $arrNotification['title'], $arrNotification['description']);
                    if (IS_APPROVE) {
                        $shiftId = $objShift->create_shift();
                        if (!empty($reqData->location)) {
                            foreach ($reqData->location as $val) {
                                $objLocation = new ShiftLocation();
                                $objLocation->setAddress($val->address);
                                $objLocation->setPostal($val->postal);
                                $objLocation->setShiftid($shiftId);
                                $objLocation->setState($val->state);
                                $objLocation->setSuburb($val->suburb->value);
                                // get address lat long
                                $latLong = getLatLong($objLocation->getAddress() . ' ' . $objLocation->getSuburb() . ' ' . $objLocation->getState());
                                if (!empty($latLong)) {
                                    $objLocation->setLat($latLong['lat']);
                                    $objLocation->setLong($latLong['long']);
                                }
                                //create shift location
                                $objLocation->createShiftLocation();
                            }
                        }
                        // get participant information
                        $participantInfo = $this->Shift_roster_model->get_participant_shift_related_information($client_id);
                        // insert participant data
                        $shift_participant = array('shiftId' => $shiftId, 'participantId' => $client_id, 'status' => 1, 'created' => DATE_TIME);
                        $this->basic_model->insert_records('shift_participant', $shift_participant, $multiple = FALSE);
                        // insert confirmation detials
                        $shift_confirmation = array('firstname' => $participantInfo->firstname, 'lastname' => $participantInfo->lastname, 'email' => $participantInfo->email, 'phone' => $participantInfo->phone, 'confirm_by' => $participantInfo->prefer_contact, 'confirm_with' => 1, 'shiftId' => $shiftId);
                        $this->basic_model->insert_records('shift_confirmation', $shift_confirmation, $multiple = FALSE);
                        // insert shift requirement
                        $shiftRequirement = array();
                        if (!empty($reqData->requirement)) {
                            foreach ($reqData->requirement as $val) {
                                if (!empty($val->checked)) {
                                    $shiftRequirement[] = array('requirementId' => $val->value, 'shiftId' => $shiftId);
                                }
                            }
                            if (!empty($shiftRequirement))
                                $this->basic_model->insert_records('shift_requirements', $shiftRequirement, $multiple = true);
                        }elseif (!empty($participantInfo->requirement)) {
                            foreach ($participantInfo->requirement as $val) {
                                $shiftRequirement[] = array('requirementId' => $val->assistanceId, 'shiftId' => $shiftId);
                            }
                            $this->basic_model->insert_records('shift_requirements', $shiftRequirement, $multiple = true);
                        }
                        // prefered member.
                        $preferedMemer = array();
                        if (!empty($reqData->preferred_member_ary)) {
                            foreach ($reqData->preferred_member_ary as $val) {
                                if (!empty($val->name)) {
                                    $preferedMemer[] = array('memberId' => $val->name->value, 'shiftId' => $shiftId);
                                }
                            }
                            if (!empty($preferedMemer))
                                $this->basic_model->insert_records('shift_preferred_member', $preferedMemer, $multiple = true);
                        }
                    }
                    echo json_encode(array('status' => true));
                }
            }else {
                echo json_encode($response_valid);
            }
        } else {
            echo json_encode(array('status' => false, 'error' => system_msgs('something_went_wrong')));
        }
    }

    function concatDateTime($date, $time) {
        $newData = date('Y-m-d', strtotime($date));
        $newTime = date('H:i:s', strtotime($time));
        $dateTime = date('Y-m-d H:i:s', strtotime("$newData . $newTime"));
        return $dateTime;
    }

    function submit_feedback() {
        $client_id = get_client_id();
        $reqData = request_handler();
        require_once APPPATH . 'Classes/fms/Fms.php';
        $objFms = new FmsClass\Fms();
        if (!empty($reqData)) {
            $shift_date = DateFormate($reqData->shift_date, 'Y-m-d');
            /* start validation */
            $response_valid = $this->validate_shift_data((array) $reqData);
            /* end validation */
            if ($response_valid['status']) {
                $objFms->setShiftId($reqData->shiftId);
                $objFms->setInitiated_by($client_id);
                $objFms->setInitiated_type(2);
                $objFms->setCreated(DATE_TIME);
                $objFms->setStatus(0);
                $objFms->setEvent_date($shift_date);
                $objFms->setCategory($reqData->categoryid);
                $objFms->setAgainst_category(2);
                $objFms->setAgainst_by($reqData->shiftMemberId);
                
                //Set fms notes  class values				
                $objFms->setTitle($reqData->title);
                $objFms->setDescription($reqData->feedback_message);
                $objFms->setCreated_by($client_id);
                $objFms->setCreated_type(2);
                
                // set location class
                require_once APPPATH . 'Classes/fms/FmsLocation.php';
                $locationDetails = array();
                $locationInfo = $reqData->location;
                
                foreach ($locationInfo as $location) {
                    $objFMSLocation = new FmsLocationClass\FmsLocation();
                    //$objFMSLocation->setCaseId($caseId);
                    $objFMSLocation->setAddress($location->address);
                    $objFMSLocation->setSuburb($location->suburb->value);
                    $objFMSLocation->setPostal($location->postal);
                    $objFMSLocation->setState($location->state);
                    $objFMSLocation->setCategoryId($location->locationCategory);
                    $locationDetails[] = $objFMSLocation;
                }
                
                $objFms->setFmsLocation($locationDetails);
                $caseId = $objFms->create_case();
                
                // Notification Add New Feedback 
                $arrNotification = notification_msgs('add_provide_feedback');
                AddNotification($client_id, $arrNotification['title'], $arrNotification['description']);
                echo json_encode(array('status' => true));
            } else {
                echo json_encode($response_valid);
            }
        } else {
            echo json_encode(array('status' => false, 'error' => system_msgs('something_went_wrong')));
        }
    }

    function submit_feedback_reason() {
        $client_id = get_client_id();
        $reqData = request_handler();
        require_once APPPATH . 'Classes/fms/FmsReason.php';
        
        $objFmsReason = new FmsReasonClass\FmsReason();
        $objFmsReason->setReasonId($reqData->id);
        $objFmsReason->setCaseId($reqData->caseId);
        $objFmsReason->setTitle($reqData->title);
        $objFmsReason->setDescription($reqData->description);
        $objFmsReason->setCreated_by($client_id);
        $objFmsReason->setCreated_type(2);
        $objFmsReason->setCreatedDate(DATE_TIME);
        $objFmsReason->createReason();
        echo json_encode(array('status' => true));
    }

    function validate_shift_data($participant_data) {
        try {
            $validation_rules = array(
                array('field' => 'title', 'label' => 'Title', 'rules' => 'required'),
                array('field' => 'feedback_message', 'label' => 'feedback_message', 'rules' => 'required'),
                array('field' => 'shiftId', 'label' => 'Shift Id', 'rules' => 'required|greater_than[0]'),
                array('field' => 'categoryid', 'label' => 'Category', 'rules' => 'required|greater_than[0]'),
                array('field' => 'feedback_message', 'label' => 'Feedback Message', 'rules' => 'required'),
                array('field' => 'location[]', 'label' => 'Location', 'rules' => 'callback_check_participant_address'),
                array('field' => 'ShiftMemberOption[]', 'label' => 'ShiftMember', 'rules' => 'callback_check_participant_ShiftMember'),
                array('field' => 'shift_date', 'label' => 'shift date', 'rules' => 'required')
            );
            $this->form_validation->set_data($participant_data);
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return;
    }

    function check_participant_address($address) {
        if (!empty($address)) {
            if (empty($address->address)) {
                $this->form_validation->set_message('check_participant_address', 'Location can not be empty');
                return false;
            } elseif (empty($address->state)) {
                $this->form_validation->set_message('check_participant_address', 'State can not be empty');
                return false;
            } elseif (empty($address->suburb)) {
                $this->form_validation->set_message('check_participant_address', 'Suburb can not be empty');
                return false;
            } elseif (empty($address->postal)) {
                $this->form_validation->set_message('check_participant_address', 'Postal can not be empty');
                return false;
            } elseif (empty($address->locationCategory)) {
                $this->form_validation->set_message('check_participant_address', 'Location Category can not be empty');
                return false;
            }

            $postal_length = strlen($address->postal);
            if (($postal_length < 3) || ($postal_length > 5)) {
                $this->form_validation->set_message('check_participant_address', 'Postcode accept length between 3 to 5 character only');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_participant_address', 'address can not empty');
            return false;
        }
        return true;
    }

    function check_participant_ShiftMember($shiftMembers) {
        if (count($shiftMembers) > 0) {
            
        } else {
            $this->form_validation->set_message('check_participant_ShiftMember', 'Shift Member can not empty');
            return false;
        }
        return true;
    }

    function validate_roster_data($rosterData) {
        try {
            $validation_rules = array(
                array('field' => 'start_date', 'label' => 'validate_roster_data', 'rules' => 'callback_check_roster_data[' . json_encode($rosterData) . ']')
            );
            $this->form_validation->set_data($rosterData);
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return;
    }

    function check_roster_data($data, $rosterData) {

        $roster_Data = json_decode($rosterData);
        if (!empty($roster_Data)) {
            // Start Date
            $strDate = strtotime(DateFormate($roster_Data->start_date, 'Y-m-d'));
            // currunt date
            $strCurruntDate = strtotime(date('Y-m-d'));


            if (!empty($strDate)) {
                if ($strDate > $strCurruntDate) {
                    
                } else {
                    $this->form_validation->set_message('check_roster_data', 'Roster start date can not less then current date');
                    return false;
                }
                if ($strDate == $strCurruntDate) {
                    $this->form_validation->set_message('check_roster_data', 'Roster start date can not equal to current date');
                    return false;
                }
            } else {
                $this->form_validation->set_message('check_roster_data', 'Roster start date can not empty');
                return false;
            }



            // check is default
            if ($roster_Data->is_default == 1) {
                //$title=trim($roster_Data->title);

                if (!array_key_exists('title', $roster_Data)) {
                    $this->form_validation->set_message('check_roster_data', 'Roster title can not empty');
                    return false;
                }

                if (empty($roster_Data->title)) {
                    $this->form_validation->set_message('check_roster_data', 'Roster title can not empty');
                    return false;
                }

                $endDate = strtotime(DateFormate($roster_Data->end_date, 'Y-m-d'));
                if (empty($endDate)) {
                    $this->form_validation->set_message('check_roster_data', 'Roster end date can not empty');
                    return false;
                }

                if ($strDate > $endDate) {
                    $this->form_validation->set_message('check_roster_data', 'Roster end date not less then start date');
                    return false;
                }
                if ($strDate == $endDate) {
                    $this->form_validation->set_message('check_roster_data', 'Roster start and end same date not accepted');
                    return false;
                }
            }
        } else {
            $this->form_validation->set_message('check_roster_data', 'Roster details can not empty');
            return false;
        }
        return true;
    }

}
