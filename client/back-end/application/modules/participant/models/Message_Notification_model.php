<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'Classes/websocket/Websocket.php';

class Message_Notification_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_external_messages($reqData, $clientId) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $type = $filter->type;


        $src_columns = array('em.title', 'em.content', "concat(admin.firstname,' ',admin.lastname)");

        if (isset($filter->search_box) && $filter->search_box != "") {
            $this->db->group_start();
            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    if ($serch_column[0] != 'null')
                        $this->db->or_like($serch_column[0], $filter->search_box);
                }
                else if ($column_search != 'null') {
                    $this->db->or_like($column_search, $filter->search_box);
                }
            }
            $this->db->group_end();
        }

        if ($type == 'all') {
            $this->db->where('ema.archive', 0);
        } elseif ($type == 'favourite') {
            $this->db->where('ema.is_fav', 1);
            $this->db->where('ema.archive', 0);
        } elseif ($type == 'archive') {
            $this->db->where('ema.archive', 1);
        }

        if (!empty($filter->start_date)) {
            $time = "00:00:01";
            $date = date('Y-m-d', strtotime($filter->start_date));
            $combinedDT = date('Y-m-d H:i:s', strtotime("$date $time"));
            $this->db->where("emc.created >= '" . $combinedDT . "'");
        }
        if (!empty($filter->end_date)) {
            $time = "23:59:59";
            $date = date('Y-m-d', strtotime($filter->end_date));
            $combinedDT = date('Y-m-d H:i:s', strtotime("$date $time"));
            $this->db->where("emc.created <= '" . $combinedDT . "'");
        }


        $this->db->select(array('em.id', 'em.title', 'em.is_block', 'ema.is_fav', 'ema.is_flage', 'ema.archive', 'emc.content', 'emc.created', 'emc.is_priority', 'emc.id as contentId', 'emc.sender_type', 'emc.userId', 'emr.is_read'));

        $this->db->from('tbl_external_message as em');
        $this->db->join('tbl_external_message_content as emc', 'em.id = emc.messageId', 'left');
        $this->db->join('tbl_external_message_action as ema', 'ema.messageId = em.id AND ema.userId = ' . $clientId . ' AND ema.user_type = 2', 'left');
        $this->db->join('tbl_external_message_recipient as emr', 'emr.messageContentId = emc.id', 'left');
        $this->db->join('tbl_external_message_attachment as emattach', 'emattach.messageContentId = emc.id', 'left');

        $this->db->where("((emc.sender_type = 2 AND emc.userId = " . $clientId . ") Or (emr.recipinent_type = 2 AND emr.recipinentId = " . $clientId . "))");

        $this->db->where('emc.id IN (SELECT MAX(emc.id) FROM tbl_external_message_content as emc left join tbl_external_message_recipient as emr ON emr.messageContentId = emc.id where ((emc.sender_type = 2 AND emc.userId = ' . $clientId . ') Or (emr.recipinent_type = 2 AND emr.recipinentId = ' . $clientId . ')) GROUP BY emc.messageId )');

        $this->db->group_by('emc.messageId');
        $this->db->order_by('emc.created', 'desc');

        $query = $this->db->get();
        $result = $query->result();
//        last_query();

        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }


        if (!empty($result)) {
            foreach ($result as $val) {
                $val->content = setting_length($val->content, 100);
            }
        }

        return $return = array('count' => $dt_filtered_total, 'data' => $result);
    }

    public function get_message_notifiaction($objIMail) {
        $tbl_msg_notification = TBL_PREFIX . 'notification';

        $imail_data = $this->get_imail_notification($objIMail->getUserId());

        // Get Notification
        $this->db->select("concat(title, ' - ', shortdescription)  as text");
        $this->db->from($tbl_msg_notification);
        $this->db->where(array($tbl_msg_notification . '.sender_type' => 2));
        $this->db->where(array($tbl_msg_notification . '.user_type' => 2));
        $this->db->where(array($tbl_msg_notification . '.userId' => $objIMail->getUserId()));
        $this->db->order_by($tbl_msg_notification . '.created', 'desc');
//        $this->db->limit(2);
        $queryNotification = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());


        $arr_response = array();
        $arr_response['notification'] = array_column($queryNotification->result_array(), 'text');
        $arr_response['mail_data'] = $imail_data['data'];
        $arr_response['mail_count'] = $imail_data['mail_count'];

        return $arr_response;
    }

    public function get_imail_notification($participantId) {
        $select_colown = array('em.id', 'em.title', 'emc.content', 'emc.created', 'emc.id as contentId', 'emc.userId');

        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_colown)), false);

        $this->db->from('tbl_external_message as em');
        $this->db->join('tbl_external_message_content as emc', 'em.id = emc.messageId', 'left');
        $this->db->join('tbl_external_message_recipient as emr', 'emr.messageContentId = emc.id AND emr.recipinent_type = 2', 'left');

        $this->db->where(array('emr.is_read' => 0, 'emr.is_notify' => 0, 'emr.recipinentId' => $participantId, 'emc.is_draft' => 0));

        $this->db->order_by('emc.created', 'desc');

        $query = $this->db->get();
        $result = $query->row();

        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if (!empty($result)) {
            $result->content = setting_length($result->content, 100);
        }

        return ['data' => $result, 'mail_count' => $dt_filtered_total];
    }

    public function get_single_chat($messageId, $participantId) {

        $this->db->select(array('em.id', 'em.title', 'em.is_block', 'ema.is_fav', 'ema.is_flage', 'ema.archive'));
        $this->db->from('tbl_external_message as em');

        $this->db->join('tbl_external_message_action as ema', 'ema.messageId = em.id AND ema.userId = ' . $participantId . ' AND ema.user_type = 2', 'left');
        $this->db->where('ema.messageId', $messageId);


        $query = $this->db->get();
        $messageData = $query->row();

        $messageData->is_read = 1;

        if (!empty($messageData)) {
            // get all content of message
            $content_details = $this->get_external_mail_all_content($messageId, $participantId);

            // mark as read message
            $this->mark_read_unread($messageId, $participantId, 1);

            // if its comes blank mean this user not have permission to mail
            if (!empty($content_details)) {
                $messageData->content = $content_details;

                $return = array('status' => true, 'data' => $messageData);
            } else {
                $return = array('status' => false, 'error' => 'No mail found');
            }
        } else {
            $return = array('status' => false, 'error' => 'No mail found');
        }

        return $return;
    }

    function get_external_mail_all_content($messageId, $participantId) {
        $tbl_external_message_content = TBL_PREFIX . 'external_message_content';


        $this->db->select(array('emc.id', 'emc.created', 'is_priority', 'content', 'emr.is_read', 'is_reply', 'emr.is_notify', 'emc.sender_type', 'emc.userId'));

        $this->db->from($tbl_external_message_content . ' as emc');
        $this->db->join('tbl_external_message_recipient as emr', 'emr.messageContentId = emc.id AND emr.recipinent_type = 2', 'left');

        $this->db->order_by('emc.created', 'asc');
        $this->db->group_by('emc.id');
        $this->db->where('emc.messageId', $messageId);

        $this->db->group_start();
        $this->db->where("(emc.sender_type = 2 AND emc.userId = " . $participantId . ") Or (emr.recipinent_type =2 AND emr.recipinentId = " . $participantId . ")");
        $this->db->group_end();


        $query = $this->db->get();
        $result = $query->result();


        if (!empty($result)) {
            foreach ($result as $val) {
                $val->attachments = $this->get_mail_attachment($val->id);
            }
        }

        return $result;
    }

    function get_mail_attachment($messageContentId) {
        $this->db->select(array('id', 'filename'));

        $this->db->from('tbl_external_message_attachment');
        $this->db->where('tbl_external_message_attachment.messageContentId', $messageContentId);

        $query = $this->db->get();
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $val) {
                $val->file_path = $this->config->item('admin_url') . EXTERNAL_IMAIL_PATH . '/' . $messageContentId . '/' . $val->filename;
                $val->contentId = $messageContentId;
            }
        }

        return $result;
    }

    function mark_read_unread($messageId, $userId, $status) {
        if ($status == 0 || $status == 1) {

            $where = array('messageId' => $messageId, 'recipinent_type' => 2, 'recipinentId' => $userId);
            $data = array('is_read' => $status);

            if ($status == 1) {
                $data['is_notify'] = 1;
            }
            $this->basic_model->update_records('external_message_recipient', $data, $where);
        }
    }

    function reply_mail($reqData, $participantId) {

        // here intsert content data
        $contentId = $this->set_replay_content($reqData, $participantId);

        // set here relicant data
        $this->set_recipient_user($reqData, $contentId);

        return true;
    }

    function set_replay_content($reqData, $participantId) {
        $content_data = array(
            'messageId' => $reqData->messageId,
            'sender_type' => 2,
            'userId' => $participantId,
            'created' => DATE_TIME,
            'content' => $reqData->replyAnser,
            'is_priority' => 0,
            'is_reply' => 1,
            'is_draft' => 0,
        );

        return $this->basic_model->insert_records('external_message_content', $content_data, FALSE);
    }

    function set_recipient_user($reqData, $contentId) {
        $where_mc = ['id' => $reqData->lastContentId, 'messageId' => $reqData->messageId];
        $sender_user = $rec = $this->basic_model->get_record_where('external_message_content', ['userId as adminId'], $where_mc);

        $where_rc = ['recipinent_type' => 1, 'messageContentId' => $reqData->lastContentId, 'messageId' => $reqData->messageId];
        $recipient_user = $this->basic_model->get_record_where('external_message_recipient', ['recipinentId as adminId'], $where_rc);

        $recipient_user = array_merge($sender_user, $recipient_user);

        $recipient_data = array();

        $userIds = array();
        if (!empty($recipient_user)) {
            foreach ($recipient_user as $val) {
                $x['messageContentId'] = $contentId;
                $x['messageId'] = $reqData->messageId;
                $x['recipinent_type'] = 1;
                $x['recipinentId'] = $val->adminId;
                $x['is_read'] = 0;
                $x['is_notify'] = 0;

                $recipient_data[] = $x;

                $userIds[1][] = $val->adminId;
            }
        }

        if (!empty($recipient_data)) {
            $this->basic_model->insert_records('external_message_recipient', $recipient_data, true);

            $wbObj = new Websocket();

            // check websoket here send and alert
            if ($wbObj->check_webscoket_on()) {
                $data = array('chanel' => 'server', 'req_type' => 'user_external_imail_notification', 'token' => $wbObj->get_token(), 'data' => $userIds);
                $wbObj->send_data_on_socket($data);
            }
        }
    }

}
