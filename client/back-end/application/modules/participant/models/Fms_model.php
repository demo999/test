<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Fms_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    // ============================== Insert area Start ================================
    public function create_case($objFms) {
        $tbl_fms_case = TBL_PREFIX . 'fms_case';
        $caseData = array('shiftId' => $objFms->getShiftId(), 'initiated_by' => $objFms->getInitiated_by(), 'initiated_type' => $objFms->getInitiated_type(), 'created' => $objFms->getCreated(), 'status' => $objFms->getStatus(), 'event_date' => $objFms->getEvent_date());
        $this->db->insert($tbl_fms_case, $caseData);
        $CaseId = $this->db->insert_id();
        $this->insert_fms_category($CaseId, $objFms);
        $this->insert_fms_case_against($CaseId, $objFms);
        $this->insert_fms_notes($CaseId, $objFms);
        $this->insert_fms_locations($CaseId, $objFms->getFmsLocation());
    }

    function insert_fms_notes($CaseId, $objFms) {
        $tbl_fms_case_notes = TBL_PREFIX . 'fms_case_notes';
        $noteData = array('caseId' => $CaseId, 'title' => $objFms->getTitle(), 'description' => $objFms->getDescription(), 'created_by' => $objFms->getCreated_by(), 'created_type' => $objFms->getCreated_type());
        $this->db->insert($tbl_fms_case_notes, $noteData);
    }

    function insert_fms_category($CaseId, $objFms) {
        $tbl_fms_case_cat = TBL_PREFIX . 'fms_case_category';
        $categoryData = array('caseId' => $CaseId, 'categoryId' => $objFms->getCategory());
        $this->db->insert($tbl_fms_case_cat, $categoryData);
    }

    function insert_fms_case_against($CaseId, $objFms) {
        $tbl_fms_case_against_detail = TBL_PREFIX . 'fms_case_against_detail';
        $caseData = array('caseId' => $CaseId, 'against_category' => $objFms->getAgainst_category(), 'against_by' => $objFms->getAgainst_by(), 'created' => $objFms->getCreated());
        $this->db->insert($tbl_fms_case_against_detail, $caseData);
    }

    function insert_fms_locations($CaseId, $objFMSLocation) {
        $tableFmsLocation = TBL_PREFIX . 'fms_case_location';
        $bulk_insert_location = array();

        foreach ($objFMSLocation as $FmsLocation) {
            $arrLocation = array();
            $arrLocation['caseId'] = $CaseId; // $FmsLocation->getCaseId();
            $arrLocation['address'] = $FmsLocation->getAddress();
            $arrLocation['suburb'] = $FmsLocation->getSuburb();
            $arrLocation['state'] = $FmsLocation->getState();
            $arrLocation['postal'] = $FmsLocation->getPostal();
            $arrLocation['categoryId'] = $FmsLocation->getCategoryId();
            $bulk_insert_location[] = $arrLocation;
        }
        $this->bulk_record_opration($tableFmsLocation, 'insert', null, null, $bulk_insert_location);
    }

    function insert_update_fms_Reason($objFmsReason) {
        $tblFmsCaseReason = TBL_PREFIX . 'fms_case_reason';
        if ($objFmsReason->getReasonId() > 0) {
            //Update COde here
            $ReasonData = array('caseId' => $objFmsReason->getCaseId(), 'title' => $objFmsReason->getTitle(), 'description' => $objFmsReason->getDescription(), 'created_by' => $objFmsReason->getCreated_by(), 'created_type' => $objFmsReason->getCreated_type());

            $this->db->where('id', $objFmsReason->getReasonId());
            $this->db->update($tblFmsCaseReason, $ReasonData);
        } else {
            $ReasonData = array('caseId' => $objFmsReason->getCaseId(), 'title' => $objFmsReason->getTitle(), 'description' => $objFmsReason->getDescription(), 'created_by' => $objFmsReason->getCreated_by(), 'created_type' => $objFmsReason->getCreated_type(), 'created' => $objFmsReason->getCreatedDate());
            $this->db->insert($tblFmsCaseReason, $ReasonData);
        }

        return true;
    }

    // ============================== Insert area End ================================
    // ============================== Select area Start ================================
    public function getFeedbackCaseDetails($objFms) {

        $tbl_fms_case = TBL_PREFIX . 'fms_case';
        $tbl_participant = TBL_PREFIX . 'participant';
        $resultArray = array();
        $this->db->select(array($tbl_fms_case . '.created', $tbl_fms_case . '.id', $tbl_fms_case . '.event_date', "concat(tbl_participant.firstname , ' ', tbl_participant.lastname) as name"));
        $this->db->from($tbl_fms_case);
        $this->db->join($tbl_participant, $tbl_fms_case . '.initiated_by = ' . $tbl_participant . '.id', 'inner');
        $this->db->where(array($tbl_fms_case . '.id' => $objFms->getId(), $tbl_fms_case . '.status' => 0));
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $resultArray['case_detail'] = $query->row();

        // get FMS Region details
        $tbl_fms_reason = TBL_PREFIX . 'fms_case_reason';
        $this->db->select(array($tbl_fms_reason . '.id', $tbl_fms_reason . '.title', $tbl_fms_reason . '.description', $tbl_fms_reason . '.created_by', $tbl_fms_reason . '.created_type', $tbl_fms_reason . '.created'));
        $this->db->from($tbl_fms_reason);
        $this->db->where(array($tbl_fms_reason . '.caseId' => $objFms->getId()));
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $resultArray['caseReason'] = $query->result();


        return $resultArray;
    }

    public function getFeedbackCaseRegionDetails($objFms) {
        // get FMS Region details
        $tbl_fms_reason = TBL_PREFIX . 'fms_case_reason';
        $this->db->select(array($tbl_fms_reason . '.id', $tbl_fms_reason . '.title', $tbl_fms_reason . '.description', $tbl_fms_reason . '.created_by', $tbl_fms_reason . '.created_type', $tbl_fms_reason . '.created'));
        $this->db->from($tbl_fms_reason);
        $this->db->where(array($tbl_fms_reason . '.caseId' => $objFms->getId()));
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        return $query->result();
    }

    public function getFeedbackCaseNoteDetails($objFms) {
        // get FMS Notes details
        $tbl_fms_notes = TBL_PREFIX . 'fms_case_notes';
        $this->db->select(array($tbl_fms_notes . '.id', $tbl_fms_notes . '.title', $tbl_fms_notes . '.description', $tbl_fms_notes . '.created_by', $tbl_fms_notes . '.created_type'));
        $this->db->from($tbl_fms_notes);
        $this->db->where(array($tbl_fms_notes . '.caseId' => $objFms->getId()));
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $resultArray = array();
        $resultArray = $query->result();
        return $resultArray;
    }

    // ============================== Select area End ================================
    // =================== Private Methods ===========================
    private function bulk_record_opration($table, $type, $wherekey, $wherevalue, $memberData) {
        if ($type == 'insert') {
            $insert_query = $this->db->insert_batch($table, $memberData);
            $this->db->last_query();
            if ($this->db->affected_rows() > 0) {
                return 1;
            } else {
                return 0;
            }
        } else {
            $this->db->where($wherekey, $wherevalue);
            $this->db->update($table, $memberData);
            $this->db->last_query();
        }
    }

}
