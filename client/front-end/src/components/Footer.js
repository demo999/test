import React, { Component } from 'react';

class Footer extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
                <div>
                <footer className="text-center">
                    <div className="container">
                        <div className="row">
                            <a>
                                <h5>Terms &amp; Conditions</h5>
                            </a>
                            <h6>© 2018 All Right Reserved <span>ONCALL</span></h6>
                        </div>
                    </div>
                </footer>
               
                </div>
                );
    }
}

export default Footer
