import React, { Component } from 'react';
import BlockUi from 'react-block-ui';

import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router';
import jQuery from "jquery";
import { ROUTER_PATH, BASE_URL, NPM_URL } from '../../config.js';
import { checkItsLoggedIn, logout, checkLoginWithReturnTrueFalse } from '../../service/common.js';

class Header extends Component {
    constructor(props) {
        super(props);
}

logout(){
    logout(ROUTER_PATH);
}


render() {
   
    return (
<header>
   { (this.props.css == 'not_add') ? '' : <span></span> }
    
    
  <div className="container-fluid">
   <div className="row">
    <div className="col-md-4">
    <a className="left-i" onClick={this.logout.bind(this)} data-placement="right" data-toggle="tooltip" title="Log Out">
        <span className="icon icon-log-out "></span>
        </a>
    </div>
    <div className="col-md-4">
     <div className="header_search">
      <input type="text" className="form-control" placeholder="Search" />
      <button className="but_search" type="submit">
       <i className="icon"><img src={"/assets/images/search_icons_top.svg"}/></i>
      </button>  
     </div>
    </div>
    <div className="col-md-4 text-right right_icons">
     <a className="right-i" href="#"   data-placement="bottom" data-toggle="tooltip" title="Info">
      <span className="icon icon-info"></span>
     </a>
     <a className="right-i" href="#"  data-placement="bottom" data-toggle="tooltip" title="Notification">
      <span className="icon icon-notification-icons"></span>
     </a>
     <a className="right-i" href="#"  id="share" data-placement="bottom" data-toggle="tooltip" id="share" title="Share">
      <span className="icon icon-share"></span>
     </a>
    </div>
   </div>
  </div>
 </header>

);
}
}

export default Header
