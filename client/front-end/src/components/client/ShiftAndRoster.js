import React, { Component }
    from 'react';
import 'react-select-plus/dist/react-select-plus.css';
import 'react-toastify/dist/ReactToastify.css';
import { BrowserRouter as Router, Switch, Route, Link, Redirect }
    from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import '../../service/jquery.validate.js';

class ShiftAndRoster extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }


    render() {
        return (
            <div>
                <Header title={'Shift & Rosters'} />
                <section>
                    <div className="container">
                        <div className="row mt-3 mb-4">
                            <div className="col-lg-12">
                                <div className="line"></div>
                                <div className="labels mt-2" align="right">

                                </div>
                            </div>
                            <div className="col-md-12 back-arrow"><Link to="/dashboard"><i className="icon icon-back-arrow"></i></Link></div>
                        </div>
                        <div className="row">
                            <div className="Happy_text my-4 col-md-12">Use the calendar below to book, update or cancel shifts.
                    <span>You can book multiple shifts at a time to create your own personal roster.</span>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-lg-12">

                                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div className="row your second justify-content-center">
                                        <div className="col-lg-3 col-md-4 col-sm-6">
                                            <Link to="/shift_calander">
                                                <h4 align="center">My ONCALL Calendar</h4>
                                                <div className="User">
                                                    <span>
                                                        {/* <img src="/assets/images/client/roster-3.svg" className="img-fluid" alt="IMG" /> */}
                                                        <i className='icon icon-day-31 rstr_icn'></i>
                                                    </span>
                                                </div>
                                                <p align="center">View and edit your <br />shift or roster</p>
                                            </Link>
                                        </div>
                                        <div className="col-lg-3 col-md-4 col-sm-6">
                                            <Link to="/create_shift">
                                                <h4 align="center">Book Shift</h4>
                                                <div className="User">
                                                    <span>
                                                        {/* <img src="/assets/images/client/roster-1.svg" className="img-fluid" alt="IMG" /> */}
                                                        <i className='icon icon-roster-1 rstr_icn'></i>
                                                    </span>
                                                </div>
                                                <p align="center">Book single <br />shift or multiple shifts</p>
                                            </Link>
                                        </div>
                                        <div className="col-lg-3 col-md-4 col-sm-6">
                                            <Link to={'/roster_request'}>
                                                <h4 align="center">Request Roster</h4>
                                                <div className="User">
                                                    <span>
                                                        {/* <img src="/assets/images/client/roster-2.svg" className="img-fluid" alt="IMG" /> */}
                                                        <i className='icon icon-roster-2 rstr_icn'></i>
                                                    </span>
                                                </div>
                                                <p align="center">Create a roster that  <br />meets all your care needs</p>
                                            </Link>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </section>
                <Footer />
            </div>
        );
    }
}
export default ShiftAndRoster;