import { postData} from '../../../service/common.js';

export const setNotificationData = (object) => ({
        type: 'set_notification_data',
        object
    })

    

/* decrease unread message counter
 * 
 * this action use for decrease message counter by group and type 
 * in which two paramter is mandatory {contentId}
 */    
 export const decreaseNotificationCounter = (data) => ({
        type: 'decrease_notification_counter',
        data,
 })   
 
 
// ascronus middleware for fetch data of mail content
function fetchNotification(request) {
    return dispatch => {
        return postData('participant/Message_Notification/get_message_notification_alert', {}).then((result) => {
            if (result.status) {
                 dispatch(setNotificationData(result.data))
            } 
      });
    }
}

// middle ware for set data of mail content
export function getNotification(request) {
    return (dispatch, getState) => {
        return dispatch(fetchNotification(request))
    }
}