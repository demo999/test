import React, { Component }
    from 'react';

import Modal from 'react-bootstrap/lib/Modal';
import jQuery from "jquery";
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import 'react-select-plus/dist/react-select-plus.css';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { toast }
    from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Link }
    from 'react-router-dom';
import moment from 'moment-timezone';
import Header from './Header';
import Footer from './Footer';
import { postData, calendarColorCode, getOptionsSuburb, handleShareholderNameChange,handleDateChangeRaw } from '../../service/common';
import '../../service/jquery.validate.js';
import BigCalendar from 'react-big-calendar'
import Toolbar from 'react-big-calendar/lib/Toolbar';
import 'react-big-calendar/lib/css/react-big-calendar.css'
import {  customHeading, custNumberLine } from '../../service/CustomContentLoader.js';
class ShiftAndRoster extends Component {
    constructor(props) {
        super(props);
        this.state = {
            myEventsList: [],
        }
    }
    componentDidMount() {
        this.getParticipantEvent(moment());
    }
    createShift = (date) => {
        if (moment(date.slots[0]).isAfter(moment())) {
            this.setState({ openModal: true, shift_date: date.slots[0] });
        }
    }
    CreateShiftEvent = (event) => {
        this.setState({ openModalEvent: true, shift_Details: event });
    }
    changeState = (key, value) => {
        var state = {}
        state[key] = value
        this.setState(state);
    }
    eventStyleGetter(event, start, end, isSelected) {
        var backgroundColor = calendarColorCode(event.status);
        var style = {
            backgroundColor: backgroundColor,
        };
        return {
            style: style
        };
    }
    getParticipantEvent = (e) => {
        var requestData = { date: e };
        postData('participant/Shift_Roster/get_upcoming_shift', requestData).then((result) => {
            if (result.status) {
                var tempAry = result.data.shifts;
                var shiftDetailsarr = result.data.shiftdetails;
                this.setState({ cntConfirmed: result.data.confirmed, cntCancelled: result.data.cancelled, cntUnConfirmed: result.data.unconfirmed, cntunfilled: result.data.unfilled });
                if (tempAry.length > 0) {
                    tempAry.map((value, idx) => {
                        tempAry[idx]['end'] = value.end;
                        tempAry[idx]['start'] = value.start;
                    })
                    this.setState({ myEventsList: tempAry, shiftDetails: shiftDetailsarr });
                }
            } else {
                this.setState({ error: result.error });
            }
            this.setState({ loading: false });
        });
    }
    render() {
        moment.locale('ko', {
            week: {
                dow: 1,
                doy: 1,
            },
        });
        const localizer = BigCalendar.momentLocalizer(moment)
        return (
            <div>
                <Header title={'My ONCALL Calendar'} />
                <section>
                    <div className="container">
                        <div className="row mt-3 mb-4">
                            <div className="col-lg-12">
                                <div className="line"></div>
                                <div className="labels mt-2" align="right">
                                </div>
                            </div>
                            <div className="col-md-12 back-arrow"><Link to="/shift_roster"><i className="icon icon-back-arrow"></i></Link></div>
                        </div>
                        <div className="row">
                            <div className="Happy_text my-4 col-md-12">Use the calendar below to book, update or cancel shifts.
                                    <span>You can book multiple shifts at a time to create your own personal roster.</span>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 on_call_participant_calander">
                                <BigCalendar
                                    selectable
                                    localizer={localizer}									
                                    events={this.state.myEventsList}
                                    startAccessor="start"
                                    endAccessor="end"
                                    eventPropGetter={(this.eventStyleGetter)}
                                    views={['month']}
                                    defaultDate={this.state.default_date}
                                    onNavigate={this.getParticipantEvent}
                                    components={{ toolbar: CalendarToolbar, dateCellWrapper: DateCell }}
                                    onSelectSlot={(slot) => this.createShift(slot)}
                                    onSelectEvent={(event) => this.CreateShiftEvent(event)}
                                />
                                <div className="clearfix"></div>
                                <div className="row px-5">
                                    <div className="col-md-12 mt-3">
                                        <ul className="status_new stat4">
                                            <li><span className="Confirmed"></span>Confirmed: {this.state.cntConfirmed}</li>
                                            <li><span className="Unfilled"></span>Unfilled: {this.state.cntunfilled}</li>
                                            <li><span className="Unconfirmed"></span>Unconfirmed: {this.state.cntUnConfirmed}</li>
                                            <li><span className="Cancelled"></span>Cancelled: {this.state.cntCancelled}</li>
                                        </ul>
                                    </div>
                                    <div className="col-md-12 text-right">
                                        <Link to={'/create_shift'} className="add_i_icon"><i className="icon icon-add-icons"></i></Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <CreateShift openModal={this.state.openModal} changeState={this.changeState} shift_date={this.state.shift_date} />
                <CreateShiftEvent openModalEvent={this.state.openModalEvent} changeState={this.changeState} shift_Details={this.state.shift_Details} />
                
                <p className="empty_message w-100 text-center concern_msg">if Your have any concerns or need any help regarding a
shift/roster you booked, call us on: <b><a href="tel:03 9896 2468" data-rel="external">03 9896 2468</a></b></p>
                <Footer />
            </div>
        );
    }
}
export default ShiftAndRoster;
class CalendarToolbar extends Toolbar {
    componentDidMount() {
        const view = this.props.view;
    }
    render() {
        return (
            <div>
                <div className="rbc-btn-group">
                    <span className="icon icon-arrow-left" onClick={() => this.navigate('PREV')}></span>
                    <span className="icon icon-arrow-right" onClick={() => this.navigate('NEXT')}></span>
                </div>
                <div className="rbc-toolbar-label">{this.props.label}</div>
            </div>
        );
    }
}
class CreateShift extends Component {
    constructor(props) {
        super(props);
        this.state = {
            state: [],
            shift_date: this.props.shift_date,
            location: [{ address: '', state: '', suburb: '', postcode: '' }],
            popup: true
        }
    }
    createShift = (e) => {
        e.preventDefault();
        jQuery('#create_shift').validate({ ignore: [] });
        toast.dismiss();
        if (jQuery('#create_shift').valid()) {
            this.setState({ loading: true })
            postData('participant/Shift_Roster/create_shift', this.state).then((result) => {
                if (result.status) {
                    toast.success('Your shift is waiting for approval.', {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    this.props.changeState('openModal', false);
                    this.setState({ shift_date: '', start_time: '', end_time: '', address: '', state: '', suburb: '', postal: '' });
                } else {
                    toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }
                this.setState({ loading: false })
            });
        }
    }
    handleShareholderNameChange = (obj, stateName, index, fieldName, value) => {
        var state = {};        
        var List = obj.state[stateName];
        List[index][fieldName] = value
        if (fieldName == 'state') {
            List[index]['suburb'] = {}
            List[index]['postal'] = ''
        }
        if (fieldName == 'suburb' && value) {
            List[index]['postal'] = value.postcode
        }
        state[stateName] = List;
        obj.setState(state);
    }
    componentWillReceiveProps(newProps) {
        this.setState({ shift_date: newProps.shift_date })
    }
    selectChange = (value, key) => {
        var state = {}
        state[key] = value
        this.setState(state);
    }
    componentDidMount() {
        postData('participant/Dashboard/get_state', {}).then((result) => {
            if (result.status) {
                var details = result.data
                this.setState({ stateList: details });
            }
        });
    }
    render() {
        return (
            <Modal
                show={this.props.openModal}
                onHide={this.handleHide}
                className={'modal Modal_A size_add'}
                container={this}
                aria-labelledby="contained-modal-title"
            >
                <Modal.Body>
                    <h4 className="py-3 my-0 text-center heading_modal">Adding New Shift on: {moment(this.props.shift_date).format('DD/MM/YYYY')}<a className="close_i" onClick={() => this.props.changeState('openModal', false)}><img src="/assets/images/client/cross_icons.svg" /></a></h4>
                    <div className="my-0"><div className="line"></div></div>
                    <form id="create_shift">
                        <div className="container-fluid">
                            <div className="row P_25_TB mt-2 mb-3">
                                <div className="col-md-3 my-2">
                                    <label>Start:</label>
                                    <span className="input_gradient">
                                        <DatePicker required={true} className="text-center input_pass" maxTime={(this.state.end_time) ? moment(this.state.end_time) : moment().hours(23).minutes(59)}
                                            minTime={moment().hours(0).minutes(0)}
                                            selected={this.state.start_time ? moment(this.state.start_time) : null} name="start_time" onChange={(e) => this.selectChange(e, 'start_time')}
                                            showTimeSelect showTimeSelectOnly scrollableTimeDropdown timeIntervals={15} dateFormat="LT" onChangeRaw={handleDateChangeRaw} />
                                    </span>
                                </div>
                                <div className="col-md-3 my-2">
                                    <label>End:</label>
                                    <span className="input_gradient">
                                        <DatePicker required={true} className="text-center input_pass" selected={this.state.end_time ? moment(this.state.end_time) : null}
                                            name="end_time" minTime={(this.state.start_time) ? moment(this.state.start_time) : moment().hours(0).minutes(0)}
                                            maxTime={moment().hours(23).minutes(59)} onChange={(e) => this.selectChange(e, 'end_time')}
                                            showTimeSelect showTimeSelectOnly scrollableTimeDropdown timeIntervals={15} dateFormat="LT" onChangeRaw={handleDateChangeRaw} />
                                    </span>
                                </div>
                            </div>
                            {this.state.location.map((address, index) => (
                                <div className="row P_15_T mb-3" key={index + 1}>
                                    <div className="col-md-4">
                                        <label>Location: </label>
                                        <span className="input_gradient">
                                            <input type="text" className="input_pass" data-rule-required="true" onChange={(e) => handleShareholderNameChange(this, 'location', index, 'address', e.target.value)} value={address.address} name={'location' + index} placeholder="Unit #/Street #, Street Name, Suburb" />
                                        </span>
                                    </div>
                                    <div className="col-md-3">
                                        <label>State: </label>
                                        <Select clearable={false} className="default_validation my-select" simpleValue={true} required={true}
                                            value={address.state} onChange={(e) => this.handleShareholderNameChange(this, 'location', index, 'state', e)}
                                            options={this.state.stateList} placeholder="Please Select" />
                                    </div>
                                    <div className="col-md-3">
                                        <label>Suburb:</label>
                                        <Select.Async clearable={false} className="default_validation my-select srch_sel2" required={true}
                                            name={'suburb' + index} value={address.suburb} disabled={(address.state) ? false : true} loadOptions={(val) => getOptionsSuburb(val, address.state)} onChange={(e) => this.handleShareholderNameChange(this, 'location', index, 'suburb', e)}
                                            options={this.state.stateList} placeholder="Please Select" />
                                    </div>
                                    <div className="col-md-2">
                                        <label>Postcode: </label>
                                        <span className="input_gradient">
                                            <input type="text" className="input_pass text-center" value={address.postal} onChange={(e) => handleShareholderNameChange(this, 'location', index, 'postal', e.target.value)} data-rule-number="true" data-rule-required="true" minLength="3" maxLength="5" name={'postcode' + index} placeholder="0000" />
                                        </span>
                                    </div>
                                </div>
                            ))}
                        </div>
                        <div className="col-md-4 offset-md-4 mt-3">
                            <button type="submit" disabled={this.state.loading} onClick={this.createShift} className="submit_button w-100">Create Shift</button>
                        </div>
                    </form>
                </Modal.Body>
            </Modal>
        );
    }
}
class CreateShiftEvent extends Component {
    constructor(props) {
        super(props);
        this.statusshift = { 1: 'Unfilled', 2: 'Unconfirmed', 3: 'Quote', 4: 'Rejected', 5: 'Cancelled', 6: 'Completed', 7: 'Confirmed', 8: 'Archive' };
        this.state = {
            state: [],
            popup: true,
            shift_Details: [],
            shift: [],
            shift_location: [],
            shift_member: [],
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.shift_Details) {
            this.setState({ shift_Details: nextProps.shift_Details }, () => {
                this.getShiftDetails(this.state.shift_Details.id);
            });
        }
    }
    getShiftDetails = (shiftId) => {
        var requestData = { shiftId: shiftId };
        this.setState({ loading: true });
        postData('participant/Shift_Roster/get_shift_details_by_id', requestData).then((result) => {
            if (result.status) {
                this.setState(result.data);                
            } else {
                //this.setState({ error: result.error });
            }
            this.setState({ loading: false });
        });
    }
    render() {
        return (
            <Modal
                show={this.props.openModalEvent}
                onHide={this.handleHide}
                className={'modal Modal_A size_add'}
                container={this}
                aria-labelledby="contained-modal-title"
            >
                <Modal.Body>
                <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(10)} ready={!this.state.loading}>
                    <h4 className="py-3 my-0 text-center heading_modal">View Shift details<a className="close_i" onClick={() => this.props.changeState('openModalEvent', false)}><img src="/assets/images/client/cross_icons.svg" /></a></h4>
                </ReactPlaceholder>
                    <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={custNumberLine(5)} ready={!this.state.loading}>
                    <form id="create_shift">
                        <div className="container-fluid">
                            <div className="row P_25_TB mt-2 mb-3">
                                <ul className="user_info P_15_T fdInfo shfrUl23" >
                                    <li>Start: <span>{this.state.shift.start_time} </span></li>
                                    <li>End: <span> {this.state.shift.start_time} </span></li>
                                    <li>Status: <span> {this.statusshift[this.state.shift.status]}</span></li>
                                </ul>
                            </div>
                            <div className="row">
                                <div className="col-lg-12 mt-2"><div className="line"></div></div>
                            </div>
                            {(this.state.shift_location.length > 0) ? <span>
                                {this.state.shift_location.map((value, index) => (
                                    <div className="row P_15_T mb-3" key={index+1} >
                                        <div className="col-md-4">
                                            <label className='clr_cmn txt_bld'>Location: </label>
                                            <span className="">
                                                {value.address}
                                            </span>
                                        </div>
                                        <div className="col-md-3">
                                            <label className='clr_cmn txt_bld'>State: </label>
                                            <span>
                                                {value.state}
                                            </span>
                                        </div>
                                        <div className="col-md-3">
                                            <label className='clr_cmn txt_bld'>Suburb:</label>
                                            <span>{value.suburb}</span>
                                        </div>
                                        <div className="col-md-2">
                                            <label className='clr_cmn txt_bld'>Postcode: </label>
                                            <span className="">
                                                {value.postal}
                                            </span>
                                        </div>
                                    </div>
                                ))}  </span> : <div className="row">
                                    <div className="col-lg-12 mt-2">
                                        Currently no location found for this shift.
                                    </div>
                                </div>
                            }
                            <div className="row">
                                <div className="col-lg-12 mt-2"><div className="line"></div></div>
                            </div>
                            {(this.state.shift_member.length > 0) ?
                                <span>
                                    {this.state.shift_member.map((value, index) => (
                                        <div className="row P_15_T mb-3 mt-2" >
                                            <div className="col-md-4">
                                                <label className='clr_cmn txt_bld'>Member : </label>
                                                <span className="  ">
                                                    {value.name}
                                                </span>
                                            </div>
                                        </div>
                                    ))}  </span> : <div className="row">
                                    <div className="col-lg-12 mt-2">
                                        Currently no member found for this shift.
                                    </div>
                                </div>
                            }
                        </div>
                    </form>
                    </ReactPlaceholder>
                </Modal.Body>
            </Modal>
        );
    }
}
const DateCell = ({range, value, children}) => {   
		const now = new Date();
		now.setHours(0,0,0,0);   
		return (
		 <div className={ value < now ? "date-in-past" : "rbc-day-bg" }>
		  { children }
		 </div>
		)   
   }