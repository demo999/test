import React, { Component } from 'react';
import jQuery from "jquery";
import {Panel, Button, ProgressBar, Modal, PanelGroup} from 'react-bootstrap';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import 'react-select-plus/dist/react-select-plus.css';
import moment from 'moment';
import { ROUTER_PATH, BASE_URL } from '../../config.js';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { getGoalsClassNane } from '../../service/ParticipantDropdown.js';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { Link, Redirect } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import AddUpdateGoals from './AddUpdateGoals';
import {postData, archiveALL, getPercentage} from '../../service/common';
import { custNumberLine,customHeading, customNavigation } from '../../service/CustomContentLoader.js';
class GoalsHistory extends Component {
    constructor(props) {
        super(props);
		 this.state  ={
            loading: true,
            goals: []
        }
       // this.state = this.initialState;       
      }
    handleGoalClose = () => {  this.setState({ showcontact: false }); }
	handleGoalShow = (val) => {  this.setState({ showcontact: true, goal:val ,mode:'update'}); }
	 componentDidMount() {
        this.getGoalDetails();
    }
	handleDateChangeRaw = (e) => {
		e.preventDefault();
	}
	getGoalDetails=()=>{
		this.setState({loading: true});        
         postData('participant/Goals/get_participant_goal_history').then((result) => {
			 
           if (result.status) {
                var details = result.data
                this.setState({goals: details}, ()=>{
					
				});				
            } 
            this.setState({loading: false});
        });
	}
archiveHandle = (messageId) => {
         archiveALL(messageId,'external_message_recipient', 'Are you sure want to archive', 'participant/Goals/archive_participant_goal', '').then((result) => {			
		   if(result.status){
                this.getGoalDetails();
            }
        })
    }
handleClick= (e)=> {
    e.preventDefault();
    postData('participant/Goals/export_goal_report').then((result) => {			
           if (result.status) {                
				let fileurl='http://client.ydtwebstaging.com/client/archive/'+result.filename;
				 window.open(fileurl,'_blank');
            }else{
				
			} 
            
        });
  }
	
    render() {        
        return (
                <div>
                    <Header title="Goals" />	
                    <section className='pd_tb_30'>
                    <div className="container">	
                            <div className="row mt-3 mb-4">
                                <div className="col-lg-12">				
                                     <div className="line"></div>		
                                    <div className="labels mt-2" align="right">
                                    </div>				
                                </div>
                                <div className="col-md-12 back-arrow"><Link to="/dashboard"><i className="icon icon-back-arrow"></i></Link></div>
							<div className="col-md-12">
							 <div className="row justify-content-center">
                                    <div className="col-lg-3 col-md-4 col-sm-6 mb-sm-1 my-2 " align="center"><Link to={'/goals'} className="index_but">Goal Tracker</Link></div>
                                    <div className="col-lg-3 col-md-4 col-sm-6 mb-sm-1 my-2 active" align="center"><Link to={'/goals_history'} className="drafts_but">Goal History</Link></div> 
									 <div className="col-lg-3 col-md-4 col-sm-6 mb-sm-1 my-2" align="center"><a align="center" className="drafts_but plus_btn_14" onClick={()=>
									this.setState({showcontact: true, mode:'add'})}>Add New Goal<span className="icon icon-plus"></span></a>
									</div>
                                  </div>
							</div>
                            </div>
                                   <div className="row"> <div className="col-md-12 mt-3 mb-3"><div className="line"></div></div></div>
							<div className="row P_15_TB">
                            <div className="col-md-12">
                                <div className="panel-group accordion_me house_accordi" id="accordion" role="tablist" aria-multiselectable="true">								
									<ReactPlaceholder showLoadingAnimation ={true}  customPlaceholder={ custNumberLine(4)} ready={!this.state.loading} >
                                {(this.state.goals.length>0)?
                                this.state.goals.map((val, index) => (
								<Panel key={index+1} eventKey={index+1}>
                                        <Panel.Heading>
                                        <Panel.Title toggle>
                                                    <i className="more-less glyphicon glyphicon-plus"></i>
                                                    <div className="resident_name table_a">
													<div className="d-flex">
                                                        <div className="col-md-6">{val.title}</div>                                                        
														<span className="col-md-3">End Date: <span className="f_300">{moment(val.end_date).format('DD/MM/YYYY')}</span></span>
													</div>
                                                    </div>
                                        </Panel.Title>
                                        </Panel.Heading>
                                       <Panel.Body collapsible className="px-1 py-0"> 
                                                <div className="pt-3">
													<div className="col-md-12">
                                                        <div className="w-100">{val.title}</div>
														<span>Goal Progress Chart</span>
														<img src="/assets/images/graph.jpg" className="goalHistoryChart"  />
														
                                                    </div>                                   	
                                                </div>
                                </Panel.Body>
                        </Panel>
                                    )): <div className="no_record py-2">No Record Found</div>}
                                </ReactPlaceholder>
                                    <div className="exportgoalreport  my-2 " align="center">
                                        <Link to={'#'} className="exportgoalreport_btn index_but" onClick={this.handleClick} >Export Goal Report</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                      <AddUpdateGoals goal={this.state.goal} mode={this.state.mode} showcontact={this.state.showcontact} handleGoalClose={this.handleGoalClose}  getGoalDetails={this.getGoalDetails}	handleDateChangeRaw={this.handleDateChangeRaw}  />
                    </div> 
                    </section>
                    <Footer />
                </div>
                );
    }
}
export default GoalsHistory;