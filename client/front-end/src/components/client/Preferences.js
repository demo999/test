import React, { Component } from 'react';
import jQuery from "jquery";
import { Panel, Button, ProgressBar, Modal, PanelGroup } from 'react-bootstrap';
import { ROUTER_PATH, BASE_URL } from '../../config.js';
//import { checkItsNotLoggedIn } from '../../../service/common.js';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { customProfile, customHeading, custNumberLine } from '../../service/CustomContentLoader.js';
import { postData, getPercentage } from '../../service/common';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function setRadio(Obj, e, idx, tagType, value) {
    var List = Obj.state[tagType];
    List[idx].type = value;
    var state = {};
    state[tagType] = List;
    Obj.setState(state);
}

class Preferences extends Component {
    constructor(props) {
        super(props);
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handlePlaceShow = this.handlePlaceShow.bind(this);
        this.handlePlaceClose = this.handlePlaceClose.bind(this);
        this.state = {
            expanded: true,
            detail: true,
            req: true,
            Activity: [],
            Place: []
        };
    }
    componentDidMount() {
        this.getAboutDetails();
    }
    getAboutDetails = () => {
        this.setState({ loading: true });
        postData('participant/dashboard/participant_preferences', { id: 2 }).then((result) => {
            if (result.status) {
                var details = result.data;
                this.setState(details);
            }
            this.setState({ loading: false });
        });
    }

    handleClose = () => { this.setState({ showactivity: false }); }
    handleShow() { this.setState({ showactivity: true }); }
    handlePlaceClose() { this.setState({ showplace: false }); }
    handlePlaceShow() { this.setState({ showplace: true }); }

    render() {
        return (
            <div className="mt-2">


                <div className="row">
                    <div className="col-lg-12">
                        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                            <Panel eventKey="3">
                                <Panel.Heading>
                                    <Panel.Title toggle>
                                        <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(20)} ready={!this.state.loading}>
                                            <i activekey={this.state.detail + ''} className="more-less glyphicon glyphicon-plus"></i>Your Preferred Places
                                        </ReactPlaceholder>
                                    </Panel.Title>
                                </Panel.Heading>
                                <Panel.Body collapsible>
                                    <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={custNumberLine(5)} ready={!this.state.loading}>
                                        <div className="row">
                                            <div className="col-lg-4 col-sm-5">
                                                <div className="mt-3">
                                                    <div className="col-12 text-left">Favourites</div>
                                                    <div className="col-12">
                                                        <ul className="p-3 font-w-n">
                                                            {this.state.Place.map((object, index) => (
                                                                (object.type == 1) ? <li key={index + 1}>{object.name}</li> : ''
                                                            ))}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-lg-4 col-sm-4">
                                                <div className="mt-3">
                                                    <div className="col-12 text-left"><span className="required_head">Least Favourites:</span></div>
                                                    <div className="col-12">
                                                        <ul className="p-3">
                                                            {this.state.Place.map((object, index) => (
                                                                (object.type == 2) ? <li key={index + 1} className="required_li">{object.name}</li> : ''
                                                            ))}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-lg-4 col-sm-3 align-self-end">
                                                <a align="right" className="update_but pb-3  flx_upd1">Update
                                                    {/* <img src="/assets/images/client/update.svg" onClick={this.handlePlaceShow}/> */}
                                                    <i className='icon icon-update1-ie updte_ics' onClick={this.handlePlaceShow} ></i>
                                                </a>
                                            </div>
                                        </div>
                                    </ReactPlaceholder>
                                </Panel.Body>
                            </Panel>


                            <Panel eventKey="4">
                                <Panel.Heading>
                                    <Panel.Title toggle>
                                        <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(20)} ready={!this.state.loading}>
                                            <i className="more-less glyphicon glyphicon-plus"></i>
                                            Your Preferred Activities
                                        </ReactPlaceholder>
                                    </Panel.Title>
                                </Panel.Heading>
                                <Panel.Body collapsible>
                                    <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={custNumberLine(5)} ready={!this.state.loading}>
                                        <div id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo">

                                           
                                                <div className="row">
                                                    <div className="col-lg-4 col-sm-5">
                                                        <div className="mt-3">
                                                            <div className="col-12 text-left">Favourites</div>
                                                            <div className="col-12">
                                                                <ul className="p-3 font-w-n">
                                                                    {this.state.Activity.map((object, index) => (
                                                                        (object.type == 1) ? <li key={index + 1}>{object.name}</li> : ''
                                                                    ))}
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-4 col-sm-4">
                                                        <div className="mt-3">
                                                            <div className="col-12 text-left"><span className="required_head">Least Favourites:</span></div>
                                                            <div className="col-12">

                                                                <ul className="p-3">
                                                                    {this.state.Activity.map((object, index) => (
                                                                        (object.type == 2) ? <li key={index + 1} className="required_li">{object.name}</li> : ''
                                                                    ))}
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-4 col-sm-3 align-self-end">
                                                        <a align="right" className="update_but pb-3 flx_upd1">Update
                                                                {/* <img src="/assets/images/client/update.svg" onClick={this.handleShow}/> */}
                                                            <i className='icon icon-update1-ie updte_ics' onClick={this.handleShow} ></i>
                                                        </a>
                                                    </div>
                                                </div>
                                           

                                        </div>
                                    </ReactPlaceholder>
                                </Panel.Body>
                            </Panel>



                        </div>
                    </div>
                </div>

                <UpdatePlace showplace={this.state.showplace} Place={this.state.Place} handlePlaceClose={this.handlePlaceClose} getAboutDetails={this.getAboutDetails} />

                <UpdateActivity showactivity={this.state.showactivity} Activity={this.state.Activity} handleClose={this.handleClose} getAboutDetails={this.getAboutDetails} />



            </div>
        );
    }
}
export default Preferences;




class UpdateActivity extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Activity: []
        }
    }

    componentWillReceiveProps(newProps) {
        var newObj = JSON.parse(JSON.stringify(newProps));
        this.setState(newObj);
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.setState({ loading: true })
        postData('participant/dashboard/update_participant_activity', this.state.Activity).then((result) => {

            if (result.status) {
                toast.success('Participant activity is submitted for admin approval.', {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });

                this.props.getAboutDetails();
                this.props.handleClose();
            }
            this.setState({ loading: false })
        });
    }


    render() {
        return (
            <Modal show={this.props.showactivity} onHide={this.handleClose}
                className="modal Modal_A preferences_modal">
                <Modal.Body className="px-0 py-0">
                    <h4 className="py-4 my-2 px-4 text-left  bb-1-grye">Your Activity:<a className="close_i" onClick={this.props.handleClose}><img src="/assets/images/client/cross_icons.svg" /></a></h4>
                    <div className="text-visit-1 px-4">Please select your Activity:</div>
                    <form onSubmit={this.onSubmit}>
                        <div className="font-w-n px-4">
                            <div className="overflow-hidden">
                                <div className="overflow-scroll">
                                    {this.state.Activity.map((object, idx) => (
                                        <span className="d-flex s-w-d_div" key={idx + 1}>
                                            <h4 className="font_w_4"> {object.name}</h4>
                                            <div className="cartoon_div">
                                                <div className="d-inline danger-cartoon">
                                                    <input type="radio" className="radio_input" onChange={(e) => setRadio(this, e, idx, 'Activity', '0')} name={object.activityId} checked={(object.type == '0') ? true : false} />
                                                    <label onClick={(e) => setRadio(this, e, idx, 'Activity', 'none')} ><span><span></span></span></label>
                                                </div>
                                                <div className="d-inline worning-cartoon">
                                                    <input type="radio" className="radio_input" onChange={(e) => setRadio(this, e, idx, 'Activity', '2')} name={object.activityId} checked={(object.type == '2') ? true : false} />
                                                    <label onClick={(e) => setRadio(this, e, idx, 'Activity', '2')} ><span><span></span></span></label>
                                                </div>

                                                <div className="d-inline  success-cartoon">
                                                    <input type="radio" className="radio_input" onChange={(e) => setRadio(this, e, idx, 'Activity', '1')} name={object.activityId} checked={(object.type == '1') ? true : false} />
                                                    <label onClick={(e) => setRadio(this, e, idx, 'Activity', '1')} ><span><span></span></span></label>
                                                </div>
                                            </div>
                                        </span>
                                    ))
                                    }

                                </div>
                            </div>
                        </div>
                        <div className="col-md-12  mt-3 text-right pb-3">
                            <button disabled={this.state.loading} type="submit" className="flx_btn1">Save and Update 
                                {/* <img src="/assets/images/client/update.svg" /> */}
                                <i className='icon icon-update1-ie updte_ics' ></i>
                            </button>

                        </div>
                    </form>

                </Modal.Body>
            </Modal>

        )
    }
}



class UpdatePlace extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Place: []
        }
    }

    componentWillReceiveProps(newProps) {
        var newObj = JSON.parse(JSON.stringify(newProps));
        this.setState(newObj);
    }

    submitplaces = (e) => {
        e.preventDefault();
        this.setState({ loading: true })
        postData('participant/dashboard/update_participant_places', this.state.Place).then((result) => {

            if (result.status) {
                toast.success('Participant Places is submitted for admin approval.', {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
                this.props.getAboutDetails();
                this.props.handlePlaceClose();
            }
            this.setState({ loading: false })
        });
    }


    render() {
        return (
            <Modal show={this.props.showplace} onHide={this.props.handlePlaceClose} className="modal Modal_A preferences_modal">
                <Modal.Body className="px-0 py-0">
                    <h4 className="py-4 my-2 px-4 text-left bb-1-grye">Places:<a className="close_i" onClick={
                        this.props.handlePlaceClose}><img src="/assets/images/client/cross_icons.svg" /></a></h4>
                    <div className="text-visit-1 px-4">Please select your preferences, for places you like to visit:</div>
                    <form onSubmit={
                        this.submitplaces}>
                        <div className="font-w-n px-4">
                            <div className="overflow-hidden">
                                <div className="overflow-scroll">
                                    {this.state.Place.map((object, idx) => (
                                        <span className="d-flex s-w-d_div" key={idx + 1}>
                                            <h4 className="font_w_4"> {object.name}</h4>
                                            <div className="cartoon_div">
                                                <div className="d-inline danger-cartoon">
                                                    <input type="radio" className="radio_input" onChange={(e) => setRadio(this, e, idx, 'Place', '0')} name={object.placeId} checked={(object.type == '0') ? true : false} />
                                                    <label onClick={(e) => setRadio(this, e, idx, 'Place', 'none')} ><span><span></span></span></label>
                                                </div>
                                                <div className="d-inline worning-cartoon">
                                                    <input type="radio" className="radio_input" onChange={(e) => setRadio(this, e, idx, 'Place', '2')} name={object.placeId} checked={(object.type == '2') ? true : false} />
                                                    <label onClick={(e) => setRadio(this, e, idx, 'Place', '2')} ><span><span></span></span></label>
                                                </div>

                                                <div className="d-inline  success-cartoon">
                                                    <input type="radio" className="radio_input" onChange={(e) => setRadio(this, e, idx, 'Place', '1')} name={object.placeId} checked={(object.type == '1') ? true : false} />
                                                    <label onClick={(e) => setRadio(this, e, idx, 'Place', '1')} ><span><span></span></span></label>
                                                </div>
                                            </div>
                                        </span>
                                    ))
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12  mt-3 text-right pb-3">
                            <button disabled={this.state.loading} type="submit" className="flx_btn1">Save and Update 
                                {/* <img src="/assets/images/client/update.svg" /> */}
                                <i className='icon icon-update1-ie updte_ics' ></i>
                            </button>
                        </div>
                    </form>
                </Modal.Body>
            </Modal>
        )
    }
}