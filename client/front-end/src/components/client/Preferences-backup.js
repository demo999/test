import React, { Component } from 'react';
import jQuery from "jquery";
import { Panel,Button,ProgressBar,Modal} from 'react-bootstrap';
import { ROUTER_PATH, BASE_URL } from '../../config.js';
//import { checkItsNotLoggedIn } from '../../../service/common.js';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { customProfile,customHeading, custNumberLine } from '../../service/CustomContentLoader.js';
import {postData, getPercentage} from '../../service/common';

function setRadio(Obj, e, idx, tagType, value) {
        var List = Obj.state[tagType];
        List[idx].type = value;        
        var state = {};
        state[tagType] = List;
        Obj.setState(state); 
    }

class Preferences extends Component {
constructor(props) {
super(props);
   this.handleShow = this.handleShow.bind(this);
   this.handleClose = this.handleClose.bind(this);
   this.handlePlaceShow = this.handlePlaceShow.bind(this);
   this.handlePlaceClose = this.handlePlaceClose.bind(this);
   this.state = {
	   expanded: true,
      detail: true,
	  req: true,
	  Activity:[],
	  Place:[]
    };
}
 componentDidMount() {
    this.getAboutDetails();	
  }
  getAboutDetails = () => {
        this.setState({loading: true});
        postData('participant/dashboard/participant_preferences', {id:2}).then((result) => {			
			if(result.status) {
                var details = result.data;				
                this.setState(details);
            } 
            this.setState({loading: false});
        });
    }
    
  handleClose =()=> {  this.setState({ showactivity: false }); }
  handleShow() { this.setState({ showactivity: true }); }
  handlePlaceClose() {  this.setState({ showplace: false }); }
  handlePlaceShow() { this.setState({ showplace: true }); }
		
render() {	
return (
<div>
    <Header title={"Your Preferences"} />	
            <div className="container">
            <div className="row mt-3 mb-4">
                <div className="col-lg-12">				
                    <ProgressBar bsStyle="success" now={getPercentage()} max={100} />				
                    <div className="labels mt-2" align="right">
                        <span>{getPercentage()}%</span> Completed
                    </div>				
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div className="panel panel-default">
                            <div className="panel-heading" role="tab" id="headingOne">
                                <h4 className="panel-title">
                                    <a role="button" onClick={() => this.setState({ detail: !this.state.detail })}>
                                        <ReactPlaceholder showLoadingAnimation ={true}  customPlaceholder={ customHeading(20)} ready={!this.state.loading}>
                                                          <i activeKey={this.state.detail} className="more-less glyphicon glyphicon-plus"></i>Your Preferred Places
                                        </ReactPlaceholder>
                                    </a>								
                                </h4>
                            </div>
                            <Panel id="collapsible-panel-example-1" expanded={this.state.detail}>
                                <Panel.Collapse>
                                    <Panel.Body>
                                        <ReactPlaceholder showLoadingAnimation ={true}  customPlaceholder={ custNumberLine(5)} ready={!this.state.loading}>
                                                          <div className="row">
                                                <div className="col-lg-4 col-sm-5">
                                                    <div className="mt-3">
                                                        <div className="col-12 text-left">Favourites</div>
                                                        <div className="col-12">
                                                            <ul className="p-3 font-w-n">
                                                                {this.state.Place.map((object, index) =>(														
                                                                (object.type==1)?<li>{object.name}</li>:''
                                                                ))}	
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-lg-4 col-sm-4">
                                                    <div className="mt-3">
                                                        <div className="col-12 text-left"><span className="required_head">Least Favourites:</span></div>
                                                        <div className="col-12">												
                                                            <ul className="p-3">
                                                                {this.state.Place.map((object, index) =>(
                                                                (object.type==2)?<li className="required_li">{object.name}</li>:''														
                                                                ))}	
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-lg-4 col-sm-3 align-self-end">										
                                                    <a align="right" className="update_but pb-3 pr-3">Update <img src="/assets/images/client/update.svg" onClick={this.handlePlaceShow}/></a>
                                                </div>
                                            </div>
                                        </ReactPlaceholder>
                                    </Panel.Body>
                                </Panel.Collapse>
                            </Panel>
                        </div>
                        <div className="panel panel-default">
                            <div className="panel-heading" role="tab" id="headingTwo">
                                <h4 className="panel-title">
                                    <a className="collapsed" onToggle={this.toggle} 
                                       expanded={this.state.expanded}  onClick={() => this.setState({ req: !this.state.req })}>
                                        <ReactPlaceholder showLoadingAnimation ={true}  customPlaceholder={ customHeading(20)} ready={!this.state.loading}>
                                                          <i className="more-less glyphicon glyphicon-plus"></i>
                                            Your Preferred Activities
                                        </ReactPlaceholder>
                                    </a>								
                                </h4>
                            </div>
                            <Panel id="collapsible-panel-example-1" expanded={this.state.req}>
                                <Panel.Collapse>
                                    <Panel.Body>
                                        <ReactPlaceholder showLoadingAnimation ={true}  customPlaceholder={ custNumberLine(5)} ready={!this.state.loading}>
                                                          <div id="collapseTwo"  role="tabpanel" aria-labelledby="headingTwo">

                                                <div className="col-lg-12">
                                                    <div className="row">
                                                        <div className="col-lg-4 col-sm-5">
                                                            <div className="mt-3">
                                                                <div className="col-12 text-left">Favourites</div>
                                                                <div className="col-12">
                                                                    <ul className="p-3 font-w-n">													
                                                                        {this.state.Activity.map((object, index) =>(
                                                                        (object.type==1)?<li>{object.name}</li>:''
                                                                        ))}	
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-4 col-sm-4">
                                                            <div className="mt-3">
                                                                <div className="col-12 text-left"><span className="required_head">Least Favourites:</span></div>
                                                                <div className="col-12">

                                                                    <ul className="p-3">													
                                                                        {this.state.Activity.map((object, index) =>(
                                                                        (object.type==2)?<li className="required_li">{object.name}</li>:''														
                                                                        ))}	
                                                                    </ul>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-4 col-sm-3 align-self-end">
                                                            <a align="right" className="update_but pb-3 pr-3">Update <img src="/assets/images/client/update.svg" onClick={this.handleShow}/></a>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </ReactPlaceholder>
                                    </Panel.Body>
                                </Panel.Collapse>
                            </Panel>
                        </div>
                    </div>
                </div>
            </div>

            <UpdatePlace showplace={this.state.showplace} Place={this.state.Place} handlePlaceClose={this.handlePlaceClose}  getAboutDetails={this.getAboutDetails} />

            <UpdateActivity showactivity={this.state.showactivity} Activity={this.state.Activity} handleClose={this.handleClose}  getAboutDetails={this.getAboutDetails} />


        </div>    
        <Footer />
</div>
);
}
}
export default Preferences;




class UpdateActivity extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Activity: []
        }
    }
     
    componentWillReceiveProps(newProps) {
       this.setState(newProps);
    }
    	
	onSubmit =(e) => {
         e.preventDefault();		 
             postData('participant/dashboard/update_participant_activity', this.state.Activity).then ((result) => {
                this.props.getAboutDetails();
			    this.props.handleClose();
				if (result.status) {
					}				
            });
	}
    
    
    render() {
        return (
		<Modal show={this.props.showactivity} onHide={this.handleClose}
       className="modal Modal_A">
    <Modal.Body>
        <h4 className="py-3 my-0 text-center">Your Activity:<a className="close_i" onClick={this.props.handleClose}><img src="/assets/images/client/cross_icons.svg"/></a></h4>
        <div className="my-0"><div className="line"></div></div>
        <form onSubmit={this.onSubmit}>
            <div className="font-w-n">	
                <div className="overflow-hidden">
                    <div className="overflow-scroll">													
                        {this.state.Activity.map((object, idx) =>(												
                        <span  className="w_50 w-100 px-3 mb-5 text-center" key={idx + 1}>
                            <h4 className="font_w_4"> {object.name}</h4>
                            <div className="">
                                <div className="d-inline mr-3">
                                    <input type="radio" className="radio_input" onChange={(e) => setRadio(this,e, idx, 'Activity', '1')}  name={object.activityId} checked={(object.type == '1')? true: false} />
                                    <label onClick={(e) => setRadio(this,e, idx, 'Activity','1')} ><span><span></span></span>Favourite</label>
                                </div>
                                <div className="d-inline mr-3">
                                    <input type="radio" className="radio_input" onChange={(e) => setRadio(this,e, idx, 'Activity', '2')} name={object.activityId} checked={(object.type == '2')? true: false} />
                                    <label onClick={(e) => setRadio(this,e, idx, 'Activity','2')} ><span><span></span></span>Least Favourite</label>
                                </div>
                                <div className="d-inline">
                                    <input type="radio" className="radio_input" onChange={(e) => setRadio(this,e, idx, 'Activity', '0')} name={object.activityId} checked={(object.type == '0')? true: false} />
                                    <label onClick={(e) => setRadio(this,e, idx, 'Activity','none')} ><span><span></span></span>None of these</label>
                                </div>
                            </div>
                        </span>
                        ))
                        }

                    </div>	
                </div> 
            </div>
            <div className="col-md-6 offset-md-3 mt-3">
                <button type="submit" className="submit_button w-100">Submit</button>                        
            </div>            
        </form>

    </Modal.Body>          
</Modal>
		
            )
    }
}



class UpdatePlace extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Place: []
        }
    }
     
    componentWillReceiveProps(newProps) {
       this.setState(newProps);
    }
    
	submitplaces = (e)=> {		
		e.preventDefault();				
		postData('participant/dashboard/update_participant_places', this.state.Place).then ((result) => {         
			this.props.getAboutDetails();
			this.props.handlePlaceClose();
		});
	}
    
    
    render() {
        return (
               <Modal show={this.props.showplace} onHide={this.props.handlePlaceClose} className="modal Modal_A">         
    <Modal.Body>
        <h4 className="py-3 my-0 text-center">Places:<a className="close_i" onClick={
        this.props.handlePlaceClose}><img src="/assets/images/client/cross_icons.svg"/></a></h4>
        <form onSubmit={
        this.submitplaces}>
            <div className="font-w-n">	
                <div className="overflow-hidden">
                    <div className="overflow-scroll">											
                        {this.state.Place.map((object, idx) =>(												
                        <span  className="w_50 w-100 px-3 mb-5 text-center" key={idx + 1}>
                            <h4 className="font_w_4"> {object.name}</h4>
                            <div className="">
                                <div className="d-inline mr-3">
                                    <input type="radio" className="radio_input" onChange={(e) => setRadio(this,e, idx, 'Place', '1')}  name={object.placeId} checked={(object.type == '1')? true: false} />
                                    <label onClick={(e) => setRadio(this,e, idx, 'Place','1')} ><span><span></span></span>Favourite</label>
                                </div>
                                <div className="d-inline mr-3">
                                    <input type="radio" className="radio_input" onChange={(e) => setRadio(this,e, idx, 'Place', '2')} name={object.placeId} checked={(object.type == '2')? true: false} />
                                    <label onClick={(e) => setRadio(this,e, idx, 'Place','2')} ><span><span></span></span>Least Favourite</label>
                                </div>
                                <div className="d-inline">
                                    <input type="radio" className="radio_input" onChange={(e) => setRadio(this,e, idx, 'Place', '0')} name={object.placeId} checked={(object.type == '0')? true: false} />
                                    <label onClick={(e) => setRadio(this,e, idx, 'Place','none')} ><span><span></span></span>None of these</label>
                                </div>
                            </div>
                        </span>
                        ))
                        }
                    </div>
                </div>
            </div> 
            <div className="col-md-6 col-md-offset-3 mt-3">
                <button type="submit" className="submit_button w-100">Submit</button>
            </div>
        </form>
    </Modal.Body>         
</Modal> 
            )
    }
}