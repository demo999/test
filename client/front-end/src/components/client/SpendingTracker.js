import React, { Component } from 'react';
import Header from './Header';
import Footer from './Footer';
import { checkItsNotLoggedIn, postData, getFirstname, setFirstname, setPercentage } from '../../service/common.js';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import 'react-select-plus/dist/react-select-plus.css';
import Select from 'react-select-plus';


class SpendingTracker extends Component{
render() {
    return (
        <div>
           <Header title={'Spending Tracker'} />
           <section>
                <div class="container">
                    <div className="row mt-3 mb-4">
                        <div className="col-lg-12">				
                            <div className="line"></div>     			
                            <div className="labels mt-2" align="right"></div>				
                        </div>
                        <div className="col-md-12 back-arrow"><Link to="/dashboard"><i className="icon icon-back-arrow"></i></Link></div>
                    </div>
                    <div class="row">
                        <div class="Happy_text my-4 col-lg-12">
                        <span>Below is a detailed breakdown of your plan spending so far. </span>
                        Please be aware that it may take up to 48 hours for plan spending data to be reﬂected here.  </div>
                    </div>

                    <div class="row">
                    <div className="col-lg-4">
                    </div>	
                    <div className="col-lg-8">
                  
				<div className="row mt-5">
					<div className="col-lg-6">
                    <Select name={"girish"} clearable={false}  className="default_validation my-select" placeholder="Please Select" />
					</div>
					<div className="col-lg-12">
						<div className="labels mt-2">*Change Plan View</div>
					</div>
				</div>
				
				<div className="row my-4">
					<div className="col-lg-8 col-md-8 details_you_plan">
						<div className="row">
							<div className="col-lg-12 mb-2"><div className="line"></div></div>
							<div className="col-lg-7 col-6"> Total Funds:</div> <div className="col-lg-5 col-6"> $ 114,000.00</div>
                        </div>
						<div className="row">
							<div className="col-lg-12 my-2"><div className="line"></div></div>
							<div className="col-lg-7 col-6"> Total Spent:</div> <div className="col-lg-5 col-6">  $ 54,000.00</div>
                        </div>
						<div className="row">
							<div className="col-lg-12 my-2"><div className="line"></div></div>
							<div className="col-lg-7 py-1">By Item —</div>
						</div>
						<div className="row color_b">
							<div className="col-lg-12 my-2"><div className="line"></div></div>
							<div className="col-lg-7 col-6"> 01_043_0115_1_1:</div> <div className="col-lg-5 col-6"> $ 12,000.00 </div>
						</div>
						<div className="row color_g">
							<div className="col-lg-12 my-2"><div className="line"></div></div>
							<div className="col-lg-7 col-6"> 01_108_0004_1_1:</div> <div className="col-lg-5 col-6"> $ 18,000.00 </div>
						</div>
						<div className="row color_r">
							<div className="col-lg-12 my-2"><div className="line"></div></div>
							<div className="col-lg-7 col-6"> 01_008_0004_1_1:</div> <div className="col-lg-5 col-6"> $ 24,000.00 </div>
						</div>
						<div className="row t_r_color">
							<div className="col-lg-12 my-2"><div className="line"></div></div>
							<div className="col-lg-7 col-6"> Total Remaining:</div> <div className="col-lg-5 col-6"> $ 60,000.00 </div>
						</div>
                        </div>
                        <div className="col-lg-4 col-md-4 text-right align-self-end">
                            <p className="download_v1"><small className="text-right">DOWNLOAD </small><span className="ml-1"><i className="icon icon-download"></i></span> </p>
                        </div>
                        <div className="col-lg-12 plan_end_text my-4">Your plan ends on <span>: 19/12/2019</span></div>
                    </div>

              
                    </div>	
                    </div> 

                </div>
        </section>
          <Footer />
        </div>
        );

    }

}

export default  SpendingTracker;