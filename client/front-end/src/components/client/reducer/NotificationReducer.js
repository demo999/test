const initaileState = {
    NotificationData: [],
    ImailNotificationData: [],
    mail_count: 0
}

const NotificationReducer = (state = initaileState, action) => {

    switch (action.type) {
            
        case 'set_notification_data':
            console.log(action.object)
            return {...state, NotificationData: action.object.notification, ImailNotificationData: action.object.mail_data, mail_count: action.object.mail_count};
            
        case 'decrease_notification_counter':
            var mail_count = state.mail_count;
            mail_count--;
            
            return {...state, mail_count: mail_count};

        default:
            return state;
}
}

export default NotificationReducer
    