import React, { Component } from 'react';
import jQuery from "jquery";
import {Modal } from 'react-bootstrap';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import 'react-select-plus/dist/react-select-plus.css';
import moment from 'moment';
import { ROUTER_PATH, BASE_URL } from '../../config.js';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { getGoalsClassNane } from '../../service/ParticipantDropdown.js';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { Link, Redirect } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import { postData, archiveALL, getPercentage } from '../../service/common';
import { custNumberLine, customHeading, customNavigation } from '../../service/CustomContentLoader.js';

class UpdateGoals extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			goal: [],
		}
	}
	componentWillReceiveProps(newProps) {
		var newObj = JSON.parse(JSON.stringify(newProps));
		this.setState(newObj.goal);
		this.setState({ mode: newObj.mode })
		if (newObj.mode == 'add') {
			this.setState({ title: '', start_date: '', end_date: '' });
		}
	}


	submitgoals = (e) => {
		/* start */
		e.preventDefault();
		this.setState({ loading: true });
		jQuery('#form_details').validate({ ignore: [] });
		if (jQuery('#form_details').valid()) {

			if (this.state.mode == 'add') {
				postData('participant/Goals/add_participant_goal', this.state).then((result) => {

					if (result.status) {
						toast.success('Participant Goal submit successfully', {
							position: toast.POSITION.TOP_CENTER,
							hideProgressBar: true
						});
						this.props.getGoalDetails();
						this.props.handleGoalClose();
					}
					else {

						toast.error(result.error, {
							position: toast.POSITION.TOP_CENTER,
							hideProgressBar: true
						});

						this.setState({ error: result.error });
					}
				});

			}
			else {
				postData('participant/Goals/update_participant_goal', this.state).then((result) => {

					if (result.status) {
						toast.success('Participant Goal submit successfully', {
							position: toast.POSITION.TOP_CENTER,
							hideProgressBar: true
						});
						this.props.getGoalDetails();
						this.props.handleGoalClose();
					} else {
						toast.error(result.error, {
							position: toast.POSITION.TOP_CENTER,
							hideProgressBar: true
						});
						this.setState({ error: result.error });
					}
				});
			}

			this.props.handleDateChangeRaw(e);
			this.setState({ loading: false });
		}
		this.setState({ loading: false });
		/* end */
	}
	selectChange = (key, value) => {
		var state = {}
		/* if(key == 'shift_date'){
			  this.setState({shiftLoading: true})
			  postData('participant/Shift_Roster/get_shift_list_for_feedback', {shift_date: value}).then((result) => {
				 if (result.status) {
					  this.setState({loadedShiftOption: result.data})
				 } else {
					 this.setState({error: result.error});
				 }
				 this.setState({shiftLoading: false})
			 });
		 }*/
		state[key] = value;
		this.setState(state);
	}
	render() {
		return (
			<Modal show={this.props.showcontact} onHide={this.props.handleGoalClose}
				className="modal Modal_A size_add">
				<Modal.Body>
					<h4 className="py-3 my-0 text-center heading_modal "> {this.props.mode == 'add' ? 'New' : 'Update'} Goal Details:
						{/* <a className="close_i" onClick={this.props.handleGoalClose}>
						<img src="/assets/images/client/cross_icons.svg" /></a> */}
						<span onClick={this.props.handleGoalClose} className="icon icon-cross-icons-1 mdl_cross"></span>
					</h4>
					<div className="my-0 mb-3"><div className="line"></div></div>
					<form id="form_details"  >
						<div className="row">
							<div className="col-lg-10 mt-4">
								<label>Goal</label>
								<span className="input_2">
									<input className="input_pass" placeholder="Your Goal" type="text" name="goal" data-rule-required="true" onChange={(e) => this.selectChange('title', e.target.value)} value={this.state.title} />
								</span>
							</div>
							<div className="col-lg-3 mt-4" align="left">
								<label>Start Date</label>
								<span className="input_2">
									<DatePicker
										required={true}
										dateFormat="DD/MM/YYYY"
										placeholderText="00/00/0000"
										className="text-center input_pass"
										selected={this.state.start_date ? moment(this.state.start_date) : null}
										name="start_date"
										onChange={(e) => this.selectChange('start_date', e)}
										minDate={moment()}
										onChangeRaw={this.props.handleDateChangeRaw}
										autoComplete="off"
									/>
								</span>
							</div>
							<div className="col-lg-3  mt-4" align="left">
								<label>End Date</label>
								<span className="input_2">
									<DatePicker
										required={true}
										data-rule-email="true"
										dateFormat="DD/MM/YYYY"
										placeholderText="00/00/0000"
										className="text-center input_pass"
										selected={this.state.end_date ? moment(this.state.end_date) : null}
										name="end_date"
										onChange={(e) => this.selectChange('end_date', e)}
										minDate={(this.state.start_date) ? moment(this.state.start_date) : moment()}
										onChangeRaw={this.props.handleDateChangeRaw}
										autoComplete="off"
									/>
								</span>
							</div>
						</div>
						<div className="row justify-content-end">
							<div className="col-md-4 mt-3">
								<button type="submit" onClick={this.submitgoals} disabled={this.state.loading} className="submit_button w-100">Submit</button>
							</div>
						</div>
					</form>
				</Modal.Body>
			</Modal>
		)
	}
}
export default UpdateGoals;