import React, { Component } from 'react';
import jQuery from "jquery";
import { Panel, Button, ProgressBar, Modal, PanelGroup } from 'react-bootstrap';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import 'react-select-plus/dist/react-select-plus.css';
import 'react-datepicker/dist/react-datepicker.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import moment from 'moment';
import { ROUTER_PATH, BASE_URL } from '../../config.js';
import { TranslatorProvider } from "react-translate"
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import Preferences from './Preferences';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import "../../service/jquery.validate.js";
import { customProfile, customHeading, custNumberLine } from '../../service/CustomContentLoader.js';
import { postData, handleShareholderNameChange, handleAddShareholder, handleRemoveShareholder, getOptionsSuburb, getPercentage, handleDateChangeRaw } from '../../service/common';
import { prefterLanguageDropdown, interpretertDropdown, preferContactDropdown, Hearing_option, relationDropdown, Gender_option, sitCategoryListDropdown } from '../../service/ParticipantDropdown';
class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.English_option = [{ value: 1, label: 'Yes preferred' }, { value: 2, label: 'Yes but not preferred' }],
            this.handleSelect = this.handleSelect.bind(this);
        this.state = {
            expanded: false,
            detail: true,
            req: true,
            loading: true,
            success: false,
            emails: [],
            profile: [],
            address: [],
            phones: [],
            address: [],
            kinInfo: [],
            kinBookerInfo: [],
            ParticipantCare: [],
            Support_Required: [],
            Assistance_Services: [],
            mobility_services: [],
            OcServices: [],
            show: false,
            activeKey: '1',
        };
        moment.tz.setDefault("Australia/Melbourne");
        this.handleKinClose = this.handleKinClose.bind(this)
    }
    componentDidMount() {
        this.getAboutDetails();
    }
    componentWillReceiveProps(newProps) {

    }
    getAboutDetails = () => {
        this.setState({ loading: true });
        postData('participant/dashboard/participant_info', { id: 2 }).then((result) => {
            if (result.status) {
                var details = result.data;
                this.setState(details);
            }
            this.setState({ loading: false });
        });
    }
    openCloseModal = (key, value) => {
        var state = {}
        state[key] = value
        this.setState(state)
    }
    handleClose = () => { this.setState({ showactivity: false }); }
    handleShow = () => { this.setState({ showactivity: true }); }
    handlePlaceClose = () => { this.setState({ showplace: false }); }
    handlePlaceShow = () => { this.setState({ showplace: true }); }
    handleKinClose() { this.setState({ showkin: false }); }
    handleKinShow = () => { this.setState({ showkin: true }); }
    handleKinBookingClose = () => { this.setState({ showbookingkin: false }); }
    handleKinBookingShow = () => { this.setState({ showbookingkin: true }); }
    handleAddClose = () => { this.setState({ showadd: false }); }
    handleAddShow = () => { this.setState({ showadd: true }); }
    handleContactClose = () => { this.setState({ showcontact: false }); }
    handleContactShow = () => { this.setState({ showcontact: true }); }
    setToggel = (key, val) => {
        var state = {}
        state[key] = val
        this.setState(state)
    }
    handleSelect(activeKey) {
        this.setState({ activeKey });
    }
    render() {
        return (
            <div>
                <Header title={'Your Profile'} />
                <div className="container">
                    <div className="row Block_row mt-3 mb-4">
                        <div className="col-lg-12">
                            <ProgressBar bsStyle="success" now={parseInt(getPercentage())} max={100} />
                            <div className="labels mt-2" align="right">
                                <span>{getPercentage()}%</span> Completed
                </div>
                        </div>
                        <div className="col-md-12 back-arrow"><Link to="/dashboard"><i className="icon icon-back-arrow"></i></Link></div>
                    </div>
                    <div className="row Block_row">
                        <div className="col-lg-12">
                            <div className="panel-group accordion_me house_accordi">
                                <PanelGroup
                                    accordion
                                    id="accordion-controlled-example"
                                    activeKey={this.state.activeKey}
                                    onSelect={this.handleSelect}
                                >
                                    <Panel eventKey="1">
                                        <Panel.Heading>
                                            <Panel.Title toggle>
                                                <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(10)} ready={!this.state.loading}>
                                                    <i className="more-less glyphicon glyphicon-plus"></i>Your Details
                                </ReactPlaceholder>
                                            </Panel.Title>
                                        </Panel.Heading>
                                        <Panel.Body collapsible className="px-1 py-3">
                                            <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={custNumberLine(10)} ready={!this.state.loading}>
                                                <div id="collapseOne" role="tabpanel" aria-labelledby="headingOne">
                                                    <div className="row Block_row">
                                                        <div className="col-lg-10 font_s_set">
                                                            <div className="row mt-3 Block_row">
                                                                <div className="col-lg-3 col-5 text-right">My OCSID: </div>
                                                                <div className="col-lg-9 col-7">{this.state.profile.OCSID}</div>
                                                                <div className="col-lg-3 col-5 text-right">My NDIS No: </div>
                                                                <div className="col-lg-9 col-7">{this.state.profile.NDISNO}</div>
                                                                <div className="col-lg-3 col-5 text-right">Preferred Contact: </div>
                                                                <div className="col-lg-9 col-7">{(this.state.profile.PreferredContact == 1) ? 'Contact' : 'Email'}</div>
                                                            </div>
                                                            <div className="row pt-3 pb-3 Block_row">
                                                                <div className="col-lg-3 col-5 text-right">Medicare:</div>
                                                                <div className="col-lg-9 col-7">{this.state.profile.medicare}</div>
                                                                <div className="col-lg-3 col-5 text-right">CRN:</div>
                                                                <div className="col-lg-9 col-7">{this.state.profile.CRN}</div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-2 align-self-end">
                                                            <a align="right" className="update_but pb-3 pr-3 flx_upd1">Update
                                                                <i class='icon icon-update1-ie updte_ics' onClick={this.handlePlaceShow} ></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div className="row Block_row">
                                                        <div className="col-lg-10 font_s_set">
                                                            <div className="row Block_row">
                                                                <div className="col-lg-3 col-5 text-right">My Name:</div>
                                                                <div className="col-lg-9 col-7">{this.state.profile.firstname} {this.state.profile.middlename} {this.state.profile.lastname}</div>
                                                                <div className="col-lg-3 col-5 text-right">Gender: </div>
                                                                <div className="col-lg-9 col-7">{Gender_option(this.state.profile.gender)}</div>
                                                                <div className="col-lg-3 col-5 text-right">DOB: </div>
                                                                <div className="col-lg-9 col-7">{moment(this.state.profile.dob).format("DD/MM/YYYY")}</div>
                                                                <div className="col-lg-12 col-12 ">
                                                                    {this.state.kinInfo.length > 0 ?
                                                                        this.state.kinInfo.map((object, index) => (
                                                                            <div className="row Block_row" key={index + 1}>
                                                                                <div className='col-lg-3 col-5 text-right'>Next of Kin ({(object.primary_kin == 1) ? 'Primary' : 'Secondary'}):</div>
                                                                                <div className='col-lg-9 col-7'>{object.firstname} {object.lastname} ({object.relation}) -  {object.phone} </div></div>
                                                                        )) :
                                                                        <div className="row Block_row">
                                                                            <div className='col-lg-3 col-5 text-right'>Next of Kin:</div>
                                                                            <div className='col-lg-9 col-7'>N/A</div>
                                                                        </div>
                                                                    }
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-2 align-self-end">
                                                            <a align="right" className="update_but pb-3 pr-3 flx_upd1">Update
                                                                {/* <img src="/assets/images/client/update.svg" onClick={this.handleKinShow} /> */}
                                                                <i class='icon icon-update1-ie updte_ics' onClick={this.handleKinShow} ></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div className="row my-3 Block_row">
                                                        <div className="col-lg-10 font_s_set">
                                                            <div className="row Block_row">
                                                                <div className="col-lg-12 col-12 ">
                                                                    {this.state.kinBookerInfo.length > 0 ?
                                                                        this.state.kinBookerInfo.map((object, index) => (
                                                                            <div className="row Block_row" key={index + 1}>
                                                                                <div className='col-lg-3 col-5 text-right'>Booker :</div>
                                                                                <div className='col-lg-9 col-7'>{object.firstname} {object.lastname}({object.relation}) - {object.phone}  </div>
                                                                            </div>
                                                                        )) :
                                                                        <div className="row Block_row" >
                                                                            <div className='col-lg-3 col-5 text-right'>Booker :</div>
                                                                            <div className='col-lg-9 col-7'>N/A</div>
                                                                        </div>
                                                                    }
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-2 align-self-end">
                                                            <a align="right" className="update_but pb-3 pr-3 flx_upd1">Update
                                                                {/* <img src="/assets/images/client/update.svg" onClick={this.handleKinBookingShow} /> */}
                                                                <i class='icon icon-update1-ie updte_ics' onClick={this.handleKinBookingShow} ></i>

                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div className="row my-3 Block_row">
                                                        <div className="col-lg-10 font_s_set">
                                                            {this.state.address.map((object, index) => (
                                                                <div className="row Block_row" key={index + 1}>
                                                                    <div className='col-lg-3 col-5 text-right'>Address ({(object.primary_address == 1) ? 'Primary' : 'Secondary'}):</div>
                                                                    <div className='col-lg-9 col-7'>{object.street} {object.city.value}, {object.postal}, {object.stateName} {(object.site_category > 0) ? <span> - {sitCategoryListDropdown(object.site_category)} </span> : ''}</div></div>
                                                            ))}
                                                        </div>
                                                        <div className="col-lg-2 align-self-end">
                                                            <a align="right" className="update_but pb-3 pr-3 flx_upd1">Update
                                                                {/* <img src="/assets/images/client/update.svg" onClick={this.handleAddShow} /> */}
                                                                <i class='icon icon-update1-ie updte_ics' onClick={this.handleAddShow} ></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div className="row Block_row">
                                                        <div className="col-lg-10 font_s_set">
                                                            {this.state.phones.map((object, index) => (
                                                                <div className="row Block_row mt-3" key={index + 1}>
                                                                    <div className='col-lg-3 col-5 text-right'>Phone ({(object.primary_phone == 1) ? 'Primary' : 'Secondary'}):</div>
                                                                    <div className='col-lg-9 col-7'>{object.phone}</div></div>
                                                            ))}
                                                            {this.state.emails.map((object, index) => (
                                                                <div className="row Block_row mt-3" key={index + 1}>
                                                                    <div className='col-lg-3 col-5 text-right'>Email ({(object.primary_email == 1) ? 'Primary' : 'Secondary'}):</div>
                                                                    <div className='col-lg-9 col-7'>{object.email}</div></div>
                                                            ))}
                                                        </div>
                                                        <div className="col-lg-2 align-self-end">
                                                            <a align="right" className="update_but pb-3 pr-3 flx_upd1">Update
                                                                {/* <img src="/assets/images/client/update.svg" onClick={this.handleContactShow} /> */}
                                                                <i class='icon icon-update1-ie updte_ics' onClick={this.handleContactShow} ></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ReactPlaceholder>
                                        </Panel.Body>
                                    </Panel>
                                    <Panel eventKey="2">
                                        <Panel.Heading>
                                            <Panel.Title toggle>
                                                <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(20)} ready={!this.state.loading}>
                                                    <i className="more-less glyphicon glyphicon-plus"></i> Your Care Requirements
                                </ReactPlaceholder>
                                            </Panel.Title>
                                        </Panel.Heading>
                                        <Panel.Body collapsible>
                                            <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={custNumberLine(10)} ready={!this.state.loading}>
                                                <div id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo">
                                                    <div className="row Block_row">
                                                        <div className="col-lg-10 font_s_set">
                                                            <div className="row Block_row mt-3">
                                                                <div className="col-lg-3 col-5 text-right">Preferred Language:</div>
                                                                <div className="col-lg-9 col-7">{prefterLanguageDropdown(this.state.ParticipantCare.preferred_language)}</div>
                                                                <div className="col-lg-3 col-5 text-right">Language Interpreter:</div>
                                                                <div className="col-lg-9 col-7">{this.state.ParticipantCare.language_interpreter > 0 ? interpretertDropdown(this.state.ParticipantCare.language_interpreter) : 'N/A'}</div>
                                                                <div className="col-lg-3 col-5 text-right">Hearing Interpreter:</div>
                                                                <div className="col-lg-9 col-7">
                                                                    {(this.state.ParticipantCare.hearing_interpreter == 1) ? 'YES' : 'NO'}
                                                                </div>
                                                            </div>
                                                            <div className="row Block_row  mt-3">
                                                                <div className="col-lg-3 col-5 text-right">OC Services:</div>
                                                                <div className="col-lg-9 col-7">
                                                                    <ul className="pl-3">
                                                                        {this.state.OcServices.map((object, index) => (
                                                                            (object.service_id == 1) ? <li key={index + 1}>{object.name}</li> : ''
                                                                        ))}
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div className="row Block_row mt-3 pb-3">
                                                                <div className="col-lg-3 col-5 text-right">
                                                                    Support Required:
                                                    </div>
                                                                <div className="col-lg-9 col-7">
                                                                    <ul className="pl-3">
                                                                        {this.state.Support_Required.map((object, index) => (
                                                                            (object.service_id == 1) ? <li key={index + 1}>{object.name}</li> : ''
                                                                        ))}
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div className="row Block_row mt-3 pb-3">
                                                                <div className="col-lg-3 col-5 text-right">
                                                                    Assistance Required:
                                                    </div>
                                                                <div className="col-lg-9 col-7">
                                                                    <ul className="pl-3">
                                                                        {this.state.Assistance_Services.map((object, index) => (
                                                                            (object.service_id == 1) ? <li key={index + 1}>{object.name}</li> : ''
                                                                        ))}
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div className="row Block_row mt-3 pb-3">
                                                                <div className="col-lg-3 col-5 text-right">
                                                                    Mobility Required:
                                                    </div>
                                                                <div className="col-lg-9 col-7">
                                                                    <ul className="pl-3">
                                                                        {this.state.mobility_services.map((object, index) => (
                                                                            (object.service_id == 1) ? <li key={index + 1}>{object.name}</li> : ''
                                                                        ))}
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-2 align-self-end">
                                                            <a align="right" className="update_but pb-3  flx_upd1">Update 
                                                                {/* <img src="/assets/images/client/update.svg" onClick={this.handleShow} /> */}
                                                                <i class='icon icon-update1-ie updte_ics' onClick={this.handleShow} ></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ReactPlaceholder>
                                        </Panel.Body>
                                    </Panel>
                                    <Preferences />
                                </PanelGroup>
                            </div>
                            <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            </div>
                        </div>
                    </div>
                    <UpdateParticipantRequirement showactivity={this.state.showactivity} ParticipantCare={this.state.ParticipantCare} Support_Required={this.state.Support_Required} OcServices={this.state.OcServices} Assistance_Services={this.state.Assistance_Services} mobility_services={this.state.mobility_services} handleClose={this.handleClose} getAboutDetails={this.getAboutDetails} />
                    <UpdateEmailAndPhone showcontact={this.state.showcontact} handleContactClose={this.handleContactClose} emails={this.state.emails} phones={this.state.phones} getAboutDetails={this.getAboutDetails} />
                    <UpdateAddress showadd={this.state.showadd} handleAddClose={this.handleAddClose} address={this.state.address} getAboutDetails={this.getAboutDetails} />
                    <UpdateKin showkin={this.state.showkin} handleKinClose={this.handleKinClose} kinInfo={this.state.kinInfo} getAboutDetails={this.getAboutDetails} />
                    <UpdateKinBooking showbookingkin={this.state.showbookingkin} handleKinBookingClose={this.handleKinBookingClose} phones={this.state.phones} emails={this.state.emails} address={this.state.address} profile={this.state.profile} kinInfo={this.state.kinInfo} kinBookerInfo={this.state.kinBookerInfo} getAboutDetails={this.getAboutDetails} />
                    <UpdateParticipantInfo showplace={this.state.showplace} profile={this.state.profile} handlePlaceClose={this.handlePlaceClose} getAboutDetails={this.getAboutDetails} />
                </div>
                <Footer />
            </div>
        );
    }
}
export default Profile;
class UpdateEmailAndPhone extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            phones: [],
            emails: [],
        }
    }
    componentWillReceiveProps(newProps) {
        var newObj = JSON.parse(JSON.stringify(newProps));
        this.setState(newObj);
    }
    submitplaces = (e) => {
        e.preventDefault();
        jQuery('#form_details').validate();
        if (jQuery('#form_details').valid()) {
            this.setState({ loading: true })
            postData('participant/dashboard/update_participant_email_and_phone', this.state).then((result) => {

                if (result.status) {
                    toast.success('Contact detail is submitted for admin approval.', {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    //this.props.getAboutDetails();
                    this.props.handleContactClose();
                } else {
                    toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }
                this.setState({ loading: false })

            });
        }
    }
    selectChange(selectedOption, fieldname) {
        var state = this.state.profile;
        state[fieldname] = selectedOption;
        this.setState(state);
    }
    handleChange = (e) => {
        var state = this.state.profile;
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }
    render() {
        return (
            <Modal show={this.props.showcontact} onHide={this.props.handleContactClose}
                className="modal Modal_A size_add">
                <Modal.Body>
                    <h4 className="py-3 my-0 text-center heading_modal">Your Contact Details:<a className="close_i" onClick={this.props.handleContactClose}><img src="/assets/images/client/cross_icons.svg" /></a></h4>
                    <div className="my-0 mb-3"><div className="line"></div></div>
                    <form id="form_details" onSubmit={this.submitplaces}>
                        <div className="container-fluid">
                            <div className="row P_25_T new_remove">
                                <div className="col-md-6 pl-0">
                                    <label>Phone (Primary):</label>
                                    <span className="required">
                                        {this.state.phones.map((PhoneInput, idx) => (
                                            <div className="add_input row mb-4" key={idx + 1}>
                                                <div className="col-md-10">
                                                    <span className="input_2">
                                                        <input className="distinctPhone input_pass" type="text"
                                                            value={PhoneInput.phone}
                                                            name={'phone_primary' + idx}
                                                            placeholder="Phone"
                                                            onChange={(evt) => handleShareholderNameChange(this, 'phones', idx, 'phone', evt.target.value)}
                                                            data-rule-required="true" data-rule-notequaltogroup='[".distinctPhone"]' data-msg-notequaltogroup="Please enter a unique phone." data-rule-number="true" />
                                                    </span>
                                                </div>
                                                {idx > 0 ? <button className="col-md-2 pl-0 text-left" onClick={(e) => handleRemoveShareholder(this, e, idx, 'phones')}>
                                                    <i className="icon icon-decrease-icon icon_cancel_1" ></i>
                                                </button> : (this.state.phones.length == 3) ? '' : <button className="col-md-2 pl-0 text-left add_i_icon" onClick={(e) => handleAddShareholder(this, e, 'phones', PhoneInput)}>
                                                    <i className="icon icon-add-icons" ></i>
                                                </button>}
                                            </div>
                                        ))}
                                    </span>
                                </div>
                                <div className="col-md-6 pl-0">
                                    <label>Email</label>
                                    <span className="required">
                                        {this.state.emails.map((EmailInput, idx) => (
                                            <div className="add_input row mb-4" key={idx + 1}>
                                                <div className="col-md-10">
                                                    <span className="input_2">
                                                        <input className="input_pass distinctEmail pr-5" type="email"
                                                            value={EmailInput.email || ''}
                                                            name={'email_' + idx}
                                                            placeholder={idx > 0 ? 'Secondary email' : 'Password recovery email'}
                                                            onChange={(evt) => handleShareholderNameChange(this, 'emails', idx, 'email', evt.target.value)}
                                                            data-rule-required="true"
                                                            data-rule-email="true"
                                                            data-rule-notequaltogroup='[".distinctEmail"]'
                                                            data-msg-notequaltogroup="Please enter a unique email." />
                                                    </span>
                                                </div>
                                                {idx > 0 ? <button className="col-md-2 pl-0 text-left" onClick={(e) => handleRemoveShareholder(this, e, idx, 'emails')}>
                                                    <i className="icon icon-decrease-icon icon_cancel_1" ></i>
                                                </button> : (this.state.emails.length == 3) ? '' : <button className="col-md-2 pl-0 text-left add_i_icon" onClick={(e) => handleAddShareholder(this, e, 'emails', EmailInput)}>
                                                    <i className="icon icon-add-icons" ></i>
                                                </button>}
                                            </div>
                                        ))}
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4 offset-md-4 mt-3">
                            <button type="submit" disabled={this.state.loading} className="submit_button w-100">Submit</button>
                        </div>
                    </form>
                </Modal.Body>
            </Modal>
        )
    }
}
class UpdateAddress extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            address: []
        }
    }
    componentWillReceiveProps(newProps) {
        var newObj = JSON.parse(JSON.stringify(newProps.address));
        this.setState({ address: newObj });
        if (newProps.address.length == 0) {
            this.setState({ address: [{ street: '', state: '', city: '', postal: '', site_category: '' }] });
        }
    }
    componentDidMount() {
        postData('participant/Dashboard/get_state', {}).then((result) => {
            if (result.status) {
                var details = result.data
                this.setState({ stateList: details });
            }
        });
    }
    handleShareholderNameChange = (obj, stateName, index, fieldName, value) => {
        var state = {};
        var tempField = {};
        var List = obj.state[stateName];
        List[index][fieldName] = value
        if (fieldName == 'state') {
            List[index]['suburb'] = {}
            List[index]['postal'] = ''
        }
        if (fieldName == 'suburb' && value) {
            List[index]['postal'] = value.postcode
        }
        state[stateName] = List;
        obj.setState(state);
    }
    UpdateAddress = (e) => {
        e.preventDefault();


        jQuery('#participant_address').validate();
        if (jQuery('#participant_address').valid()) {
            this.setState({ loading: true })
            postData('participant/dashboard/update_participant_address', this.state).then((result) => {

                if (result.status) {
                    toast.success('Participant address is submitted for admin approval.', {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    //this.props.getAboutDetails();
                    this.props.handleAddClose();
                } else {
                    toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }
                this.setState({ loading: false })
            });
        }
    }
    selectChange(selectedOption, fieldname) {
        var state = this.state.profile;
        state[fieldname] = selectedOption;
        this.setState(state);
    }
    handleChange = (e) => {
        var state = this.state.profile;
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }
    render() {
        return (
            <Modal show={this.props.showadd} onHide={this.props.handleAddClose}
                className="modal Modal_A size_add">
                <Modal.Body>
                    <h4 className="py-3 my-0 text-center heading_modal">Update Your Address Details:<a className="close_i" onClick={
                        this.props.handleAddClose}><img src="/assets/images/client/cross_icons.svg" /></a></h4>
                    <div className="my-0"><div className="line"></div></div>
                    <form id="participant_address">
                        <div className="container-fluid">
                            <div className="row P_25_TB">
                                <div className="col-lg-12 col-12 px-0">
                                    {this.state.address.map((object, index) => (
                                        <div className="row mt-3" key={index + 1}>
                                            <div className='col-lg-12 col-5'><h4 className=" mb-3">Address ({(object.primary_address == 1) ? 'Primary' : 'Secondary'}):
                                </h4></div>
                                            <div className='col-lg-4 col-12 my-3'>
                                                <label>Street</label>
                                                <span className="input_gradient">
                                                    <input className="input_pass" placeholder="Street" type="text" name={'street' + index} value={object.street} onChange={
                                                        (e) => handleShareholderNameChange(this, 'address', index, 'street', e.target.value)} data-rule-required="true" />
                                                </span>
                                            </div>
                                            <div className='col-lg-2 col-12 my-3 px-0'>
                                                <label>State</label>
                                                <Select cache={false} clearable={false} className="default_validation my-select" simpleValue={true} required={true}
                                                    value={object.state} onChange={(e) => this.handleShareholderNameChange(this, 'address', index, 'state', e)}
                                                    options={this.state.stateList} placeholder="Please Select" />
                                            </div>
                                            <div className='col-lg-4 col-12 my-3'>
                                                <label>Suburb</label>
                                                <span className="">
                                                    <Select.Async clearable={false} className="default_validation my-select" required={true}
                                                        name={'suburb' + index} value={object.city} disabled={(object.state) ? false : true} loadOptions={(val) => getOptionsSuburb(val, object.state)} onChange={(e) => this.handleShareholderNameChange(this, 'address', index, 'city', e)}
                                                        options={this.state.stateList} placeholder="Please Select" cache={false} />
                                                </span>
                                            </div>
                                            <div className='col-lg-2 col-12 my-3 pl-lg-0'>
                                                <label>Postcode</label>
                                                <span className="input_gradient">
                                                    <input className="input_pass" placeholder="Postcode" type="text" name={'postal' + index} value={object.postal} onChange={
                                                        (e) => handleShareholderNameChange(this, 'address', index, 'postal', e.target.value)} data-rule-number="true" data-rule-required="true" minLength="3" maxLength="5" />
                                                </span>
                                            </div>
                                            <div className='col-lg-4 col-12 my-3'>
                                                <label>Site Category</label>
                                                <Select clearable={false} className="custom_select my-select default_validation " simpleValue={true} searchable={false} name={"relation" + index} onChange={
                                                    (e) => handleShareholderNameChange(this, 'address', index, 'site_category', e)} value={object.site_category} options={sitCategoryListDropdown(0)} placeholder="Site Category" required={true} cache={false} />
                                            </div>
                                            <div className='col-lg-4 col-12 my-3 align-self-end'>
                                                {index > 0 ? <button onClick={(e) => handleRemoveShareholder(this, e, index, 'address')}>
                                                    <i className="icon icon-decrease-icon icon_cancel_1" ></i>
                                                </button> : (this.state.address.length == 3) ? '' : <button className="add_i_icon" onClick={(e) => handleAddShareholder(this, e, 'address', object)}>
                                                    <i className="icon icon-add-icons" ></i>
                                                </button>}
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </div>
                            <div className="row P_25_T">
                            </div>
                        </div>
                        <div className="col-md-4 offset-md-4 mt-3">
                            <button disabled={this.state.loading} type="submit" onClick={this.UpdateAddress} className="submit_button w-100">Submit</button>
                        </div>
                    </form>
                </Modal.Body>
            </Modal>
        )
    }
}
class UpdateKin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            kinDetails: [],
        }
    }
    componentWillReceiveProps(newProps) {
        var newObj = JSON.parse(JSON.stringify(newProps.kinInfo));
        this.setState({ kinDetails: newObj });
        if (newProps.kinInfo.length == 0) {
            this.setState({ kinDetails: [{ firstname: '', lastname: '', relation: '', phone: '', email: '' }] });
        }
    }
    updateKinDetails = (e) => {
        e.preventDefault();
        jQuery('#kin_details').validate({ ignore: [] });
        if (jQuery('#kin_details').valid()) {
            this.setState({ loading: true })
            postData('participant/dashboard/update_participant_kin_details', this.state).then((result) => {

                if (result.status) {
                    toast.success('Participant kin detail is submitted for admin approval.', {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    //this.props.getAboutDetails();
                    this.props.handleKinClose();

                } else {
                    toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }
                this.setState({ loading: false })
            });
        }
    }
    render() {
        return (
            <Modal show={this.props.showkin} onHide={this.props.handleKinClose}
                className="modal Modal_A size_add">
                <Modal.Body>
                    <h4 className="py-3 my-0 text-center heading_modal">Your Kin Details:<a className="close_i" onClick={
                        this.props.handleKinClose}><img src="/assets/images/client/cross_icons.svg" /></a></h4>
                    <div className="my-0"><div className="line"></div></div>
                    <form id="kin_details" >
                        <div className="container-fluid">
                            <div className="row P_25_TB">
                                <div className="col-lg-12 col-12 px-0">
                                    {this.state.kinDetails.map((object, index) => (
                                        <div className="row" key={index + 1}>
                                            <div className='col-lg-12 col-5'><h4 className=" mb-3 py-4 bb-1 my-0 w-100">Next of Kin ({(object.primary_kin == 1) ? 'Primary' : 'Secondary'}):
                                </h4></div>
                                            <div className='col-lg-4 col-12 my-3 mb-3'>
                                                <label>First Name</label>
                                                <span className="input_2">
                                                    <input className="input_pass" placeholder="First Name" type="text" name={"firstname" + index} value={object.firstname} onChange={
                                                        (e) => handleShareholderNameChange(this, 'kinDetails', index, 'firstname', e.target.value)} required={true} />
                                                </span>
                                            </div>
                                            <div className='col-lg-4 col-12  my-3 mb-3'>
                                                <label>Last Name</label>
                                                <span className="input_2">
                                                    <input className="input_pass" placeholder="Last Name" type="text" name={"lastname" + index} value={object.lastname} onChange={
                                                        (e) => handleShareholderNameChange(this, 'kinDetails', index, 'lastname', e.target.value)} data-rule-required="true" />
                                                </span>
                                            </div>
                                            <div className='col-lg-3 col-12  my-3 mb-3'>
                                                <label>Relation</label>
                                                <Select clearable={false} className="custom_select my-select default_validation " required={true} simpleValue={true} searchable={true} name={"relation" + index} onChange={
                                                    (e) => handleShareholderNameChange(this, 'kinDetails', index, 'relation', e)} value={object.relation} options={relationDropdown(0)} placeholder="Contact" required={true} />
                                            </div>
                                            <div className='col-lg-4 col-12  my-3 mb-3'>
                                                <label>Contact Number</label>
                                                <span className="input_2">
                                                    <input className="input_pass contactBooker" placeholder="Phone" type="text" name={"phone" + index} value={object.phone} onChange={(e) => handleShareholderNameChange(this, 'kinDetails', index, 'phone', e.target.value)} data-rule-required="true" data-rule-notequaltogroup='[".contactBooker"]' data-msg-notequaltogroup="Please enter a unique phone." data-rule-number="true" />
                                                </span>
                                            </div>
                                            <div className='col-lg-4 col-12  my-3 mb-3'>
                                                <label>Email id</label>
                                                <span className="input_2">
                                                    <input className="input_pass emainBooker" placeholder="Email" type="email" name={"email" + index} value={object.email} onChange={(e) => handleShareholderNameChange(this, 'kinDetails', index, 'email', e.target.value)}
                                                        data-rule-required="true"
                                                        data-rule-notequaltogroup='[".emainBooker"]'
                                                        data-rule-email="true"
                                                        data-msg-notequaltogroup="Please enter a unique email."
                                                    />
                                                </span>
                                            </div>
                                            <div className="col-md-2 align-self-end  mb-3">
                                                {index > 0 ? <button className="button_unadd" onClick={(e) => handleRemoveShareholder(this, e, index, 'kinDetails')}>
                                                    <i className="icon icon-decrease-icon icon_cancel_1" ></i>
                                                </button> : (this.state.kinDetails.length == 3) ? '' : <button className="add_i_icon" onClick={(e) => handleAddShareholder(this, e, 'kinDetails', object)}>
                                                    <i className="icon icon-add-icons" ></i>
                                                </button>}
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4 offset-md-4 mt-3">
                            <button disabled={this.state.loading} onClick={(e) => this.updateKinDetails(e)} className="submit_button w-100">Submit</button>
                        </div>
                    </form>
                </Modal.Body>
            </Modal>
        )
    }
}
class UpdateKinBooking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            kinBookerInfo: [],
        }
    }
    componentWillReceiveProps(newProps) {
        var newObj = JSON.parse(JSON.stringify(newProps.kinBookerInfo));
        this.setState({ kinBookerInfo: newObj });
        if (newProps.kinBookerInfo.length == 0) {
            this.setState({ kinBookerInfo: [{ firstname: '', lastname: '', relation: '', phone: '', email: '' }] });
        }
    }
    submitkinbooking = (e) => {
        e.preventDefault();
        jQuery('#booker_details').validate();
        if (jQuery('#booker_details').valid()) {
            this.setState({ loading: true })
            postData('participant/dashboard/update_participant_bookinginfo', this.state).then((result) => {
                if (result.status) {
                    toast.success('Participant booker info is submitted for admin approval.', {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    //this.props.getAboutDetails();
                    this.props.handleKinBookingClose();
                } else {
                    toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }
                this.setState({ loading: false })
            });
        }
    }
    selectChange(selectedOption, fieldname) {
        var state = this.state.profile;
        state[fieldname] = selectedOption;
        this.setState(state);
    }
    handleChange = (e) => {
        var state = this.state.profile;
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }
    render() {
        return (
            <Modal show={this.props.showbookingkin} onHide={this.props.handleKinBookingClose}
                className="modal Modal_A size_add">
                <Modal.Body>
                    <h4 className="py-3 my-0 text-center heading_modal">Your Booker Details:<a className="close_i" onClick={
                        this.props.handleKinBookingClose}><img src="/assets/images/client/cross_icons.svg" /></a></h4>
                    <div className="my-0"><div className="line"></div></div>
                    <form id="booker_details" >
                        <div className="container-fluid">
                            <div className="row P_25_TB">
                                <div className="col-lg-12 col-12 px-0">
                                    {this.state.kinBookerInfo.map((object, index) => (
                                        <div className="row" key={index + 1}>
                                            <div className='col-lg-12 col-5'><h4 className=" mb-3 py-4 bb-1 my-0 w-100">Booker :
                                </h4></div>
                                            <div className='col-lg-4 col-7 my-3 mb-3'>
                                                <label>First Name</label>
                                                <span className="input_2">
                                                    <input className="input_pass" placeholder="First Name" type="text" name={"firstname" + index} value={object.firstname} onChange={(e) => handleShareholderNameChange(this, 'kinBookerInfo', index, 'firstname', e.target.value)} data-rule-required="true" />
                                                </span>
                                            </div>
                                            <div className='col-lg-4 col-7  my-3 mb-3'>
                                                <label>Last Name</label>
                                                <span className="input_2">
                                                    <input className="input_pass" placeholder="Last Name" type="text" name={"lastname" + index} value={object.lastname} onChange={(e) => handleShareholderNameChange(this, 'kinBookerInfo', index, 'lastname', e.target.value)} data-rule-required="true" />
                                                </span>
                                            </div>
                                            <div className='col-lg-3 col-7  my-3 mb-3'>
                                                <label>Relation</label>
                                                <Select clearable={false} className="custom_select my-select default_validation " simpleValue={true} name={"relation" + index} onChange={(e) => handleShareholderNameChange(this, 'kinBookerInfo', index, 'relation', e)} value={object.relation} options={relationDropdown(0)} placeholder="Contact" required={true} />
                                            </div>
                                            <div className='col-lg-4 col-7  my-3 mb-3'>
                                                <label>Contact Number</label>
                                                <span className="input_2">
                                                    <input className="input_pass distinctPhoneBookerDetails" placeholder="Phone" type="text" name={"phone" + index} value={object.phone} onChange={
                                                        (e) => handleShareholderNameChange(this, 'kinBookerInfo', index, 'phone', e.target.value)}
                                                        data-rule-required="true"
                                                        data-rule-notequaltogroup='[".distinctPhoneBookerDetails"]'
                                                        data-msg-notequaltogroup="Please enter a unique phone."
                                                        data-rule-number="true"
                                                    />
                                                </span>
                                            </div>
                                            <div className='col-lg-4 col-7  my-3 mb-3'>
                                                <label>Email id</label>
                                                <span className="input_2">
                                                    <input className="input_pass distinctEmailBookerDetails" placeholder="Email" type="email" name={"email" + index} value={object.email} onChange={(e) => handleShareholderNameChange(this, 'kinBookerInfo', index, 'email', e.target.value)}
                                                        data-rule-required="true"
                                                        data-rule-email="true"
                                                        data-rule-notequaltogroup='[".distinctEmailBookerDetails"]'
                                                        data-msg-notequaltogroup="Please enter a unique email."
                                                    />
                                                </span>
                                            </div>
                                            <div className="col-md-2 align-self-end mb-3">
                                                {index > 0 ? <button className="button_unadd" onClick={(e) => handleRemoveShareholder(this, e, index, 'kinBookerInfo')}>
                                                    <i className="icon icon-decrease-icon icon_cancel_1" ></i>
                                                </button> : (this.state.kinBookerInfo.length == 3) ? '' : <button className="add_i_icon" onClick={(e) => handleAddShareholder(this, e, 'kinBookerInfo', object)}>
                                                    <i className="icon icon-add-icons" ></i>
                                                </button>}
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4 offset-md-4 mt-3">
                            <button type="submit" disabled={this.state.loading} onClick={this.submitkinbooking} className="submit_button w-100">Submit</button>
                        </div>
                    </form>
                </Modal.Body>
            </Modal>
        )
    }
}
class UpdateParticipantRequirement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ParticipantCare: [],
            Support_Required: [],
            OcServices: [],
            Assistance_Services: [],
            mobility_services: []
        }
    }
    componentWillReceiveProps(newProps) {
        var newObj = JSON.parse(JSON.stringify(newProps));
        this.setState(newObj);
    }
    onSubmit = (e) => {
        e.preventDefault();
        var validator = jQuery('#participant_requirement').validate({ ignore: [], })
        if (jQuery('#participant_requirement').valid()) {
            toast.dismiss();
            this.setState({ loading: true })
            postData('participant/dashboard/update_care_requirements', this.state).then((result) => {

                this.setState({ loading: false })
                if (result.status) {
                    toast.success('Care Requirement detail is submitted for admin approval.', {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    //this.props.getAboutDetails();
                    this.props.handleClose();
                } else {
                    toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }
            });
        } else {
            toast.dismiss();
            for (var i = 0; i < validator.errorList.length; i++) {
                toast.error(validator.errorList[i]['message'], {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
            }
        }
    }
    setMutipleCheckbox(e, idx, tagType) {
        var List = this.state[tagType];
        if (List[idx]['service_id'] > 0) {
            List[idx]['service_id'] = 0
        } else {
            List[idx]['service_id'] = 1
        }
        var state = {};
        state[tagType] = List;
        this.setState(state);
    }
    selectChange(selectedOption, fieldname) {
        var state = this.state.ParticipantCare;
        state[fieldname] = selectedOption;
        this.setState(state);
    }
    render() {
        return (
            <Modal show={this.props.showactivity} onHide={this.props.handleClose}
                className="modal Modal_A size_add">
                <Modal.Body>
                    <h4 className="py-3 my-0 text-center heading_modal">Your Care Requirements:<a className="close_i" onClick={
                        this.props.handleClose}><img src="/assets/images/client/cross_icons.svg" /></a></h4>
                    <div className="my-0 mb-3"><div className="line"></div></div>
                    <form onSubmit={this.onSubmit} id="participant_requirement">
                        <div className="container-fluid">
                            <div className="row my-3">
                                <div className="col-lg-3 px-0 pb-3">
                                    <label>Preferred Language:</label>
                                    <span className="required">
                                        <Select clearable={false} className="custom_select my-select" name="participantPreferredlang" simpleValue={true} required={true} searchable={false} value={this.state.ParticipantCare['preferred_language']} onChange={(e) => this.selectChange(e, 'preferred_language')} options={prefterLanguageDropdown(0)} placeholder="Please Select" />
                                    </span>
                                </div>
                                <div className="col-lg-5  pb-3">
                                    <label>Language Interpreter:</label>
                                    <span className="required">
                                        <Select clearable={false} className="custom_select my-select" name="language_interpreter" simpleValue={true} required={true} searchable={false} value={this.state.ParticipantCare['language_interpreter']} onChange={(e) => this.selectChange(e, 'language_interpreter')}
                                            options={interpretertDropdown(0)} placeholder="Please Select" />
                                    </span>
                                </div>
                                <div className="col-lg-4  px-0 pb-3">
                                    <label>Hearing Interpreter:</label>
                                    <span className="required">
                                        <Select clearable={false} className="custom_select my-select" name="hearing_interpreter" simpleValue={true} required={true} searchable={false} value={this.state.ParticipantCare['hearing_interpreter']} onChange={(e) => this.selectChange(e, 'hearing_interpreter')}
                                            options={Hearing_option(0)} placeholder="Please Select" />
                                    </span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-12 bb-1 text-left px-0 pb-3 error_checkbox">
                                    <label>OC Services:</label>
                                    <span className="required">
                                        <div className="multiple_checkbox px-0 py-0 text_4style">
                                            <div className="scroll_active_modal">
                                                {this.state.OcServices.map((req, idx) => (
                                                    <span key={idx + 1} className="w_50 d-inline-block pb-2">
                                                        <input type='checkbox' name="OcServices" className="checkbox1 cutomHandleMessage" onChange={(e) => this.setMutipleCheckbox(e, idx, 'OcServices')} checked={(req.service_id > 0) ? true : false} name="OcServices" data-rule-required="true" data-msg-required="Please select at least one oc service" />
                                                        <label onClick={(e) => this.setMutipleCheckbox(e, idx, 'OcServices')}>
                                                            <div className="d_table-cell"><span ></span></div><div className="d_table-cell">{req.name}</div></label>
                                                    </span>
                                                ))}
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div className="row mt-3">
                                <div className="col-lg-12 bb-1 text-left px-0 pb-3">
                                    <label>Support Required:</label>
                                    <span className="required">
                                        <div className="multiple_checkbox px-0 py-0 text_4style">
                                            <div className="scroll_active_modal">
                                                {this.state.Support_Required.map((req, idx) => (
                                                    <span key={idx + 1} className="w_50 d-inline-block pb-2">
                                                        <input type='checkbox' name="Support_Required" className="checkbox1 cutomHandleMessage" onChange={(e) => this.setMutipleCheckbox(e, idx, 'Support_Required')} checked={(req.service_id > 0) ? true : false} name="Support_Required" data-rule-required="true" data-msg-required="Please select at least one support requirement" />
                                                        <label onClick={(e) => this.setMutipleCheckbox(e, idx, 'Support_Required')}>
                                                            <div className="d_table-cell"><span ></span></div><div className="d_table-cell">{req.name}</div></label>
                                                    </span>
                                                ))}
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div className="col-lg-12 bb-1 text-left px-0 pb-3">
                                    <label>Assistance Required:</label>
                                    <span className="required">
                                        <div className="multiple_checkbox px-0 py-0 text_4style">
                                            <div className="scroll_active_modal">
                                                {this.state.Assistance_Services.map((req, idx) => (
                                                    <span key={idx + 1} className="w_50 d-inline-block pb-2">
                                                        <input type='checkbox' name="Assistance_Services" className="checkbox1 cutomHandleMessage" onChange={(e) => this.setMutipleCheckbox(e, idx, 'Assistance_Services')} checked={(req.service_id > 0) ? true : false} name="Assistance_Services" data-rule-required="true" data-msg-required="Please select at least one asistance requirement" />
                                                        <label onClick={(e) => this.setMutipleCheckbox(e, idx, 'Assistance_Services')}>
                                                            <div className="d_table-cell"><span ></span></div><div className="d_table-cell">{req.name}</div></label>
                                                    </span>
                                                ))}
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div className="col-lg-12 bb-1 text-left px-0 pb-3">
                                    <label>Mobility Required:</label>
                                    <span className="required">
                                        <div className="multiple_checkbox px-0 py-0 text_4style">
                                            <div className="scroll_active_modal">
                                                {this.state.mobility_services.map((req, idx) => (
                                                    <span key={idx + 1} className="w_50 d-inline-block pb-2">
                                                        <input type='checkbox' name="mobility_services" className="checkbox1 cutomHandleMessage" onChange={(e) => this.setMutipleCheckbox(e, idx, 'mobility_services')} checked={(req.service_id > 0) ? true : false} name="mobility_services" data-rule-required="true" data-msg-required="Please select at least one Mobility requirement" />
                                                        <label onClick={(e) => this.setMutipleCheckbox(e, idx, 'mobility_services')}>
                                                            <div className="d_table-cell"><span ></span></div><div className="d_table-cell">{req.name}</div></label>
                                                    </span>
                                                ))}
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 offset-md-3 mt-3">
                            <button disabled={this.state.loading} type="submit" className="submit_button w-100">Submit</button>
                        </div>
                    </form>
                </Modal.Body>
            </Modal>
        )
    }
}
class UpdateParticipantInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentWillReceiveProps(newProps) {
        this.setState(newProps.profile);
    }
    submitParticipantInfo = (e) => {
        e.preventDefault();
        jQuery('#form_details').validate();
        if (jQuery('#form_details').valid()) {
            this.setState({ loading: true });
            postData('participant/dashboard/update_participant_profile', this.state).then((result) => {

                if (result.status) {
                    toast.success('Profile is submitted for admin approval.', {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    //this.props.getAboutDetails();
                    this.props.handlePlaceClose();

                } else {
                    toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }
                this.setState({ loading: false });

            });
        }
    }
    selectChange(selectedOption, fieldname) {
        var state = {};
        state[fieldname] = selectedOption;
        this.setState(state);
    }
    handleChange = (e) => {
        var state = {};
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }
    render() {
        return (
            <Modal show={this.props.showplace} onHide={this.props.handlePlaceClose}
                className="modal Modal_A size_add">
                <Modal.Body>
                    <h4 className="py-3 my-0 text-center heading_modal">Your Details:<a className="close_i" onClick={
                        this.props.handlePlaceClose}><img src="/assets/images/client/cross_icons.svg" /></a></h4>
                    <div className="my-0"><div className="line"></div></div>
                    <form id="form_details" onSubmit={
                        this.submitParticipantInfo}>
                        <div className="container-fluid">
                            <div className="row P_25_TB mt-4">
                                <div className="col-md-3 pl-0 mb-3">
                                    <label>Preferred Name:</label>
                                    <span className="input_2 ocs_id_hig">
                                        <input className="input_pass" placeholder="Preferred name" type="text" name="preferredname" value={this.state.preferredname || ''} onChange={
                                            this.handleChange} /></span>
                                </div>
                                <div className="col-md-2 pl-0 mb-3">
                                    <label>NDIS :</label>
                                    <span className="input_2">
                                        <input className="input_pass" placeholder="NDIS" type="text" name="NDISNO" value={this.state.NDISNO || ''} onChange={
                                            this.handleChange} />
                                    </span>
                                </div>
                                <div className="col-md-3 pl-0 mb-3">
                                    <label>Preferred Contact:</label>
                                    <span className="required">
                                        <Select clearable={false} className="custom_select my-select" data-rule-required={true} simpleValue={true} name="PreferredContact" onChange={
                                            (e) => this.selectChange(e, 'PreferredContact')} value={this.state.PreferredContact} dateFormat="DD/MM/YYYY" searchable={false} options={preferContactDropdown(0)} placeholder="Contact" data-rule-required="true" />
                                    </span>
                                </div>
                                <div className="col-md-3 pl-0 mb-3">
                                    <label>DOB:</label>
                                    <span className="input_2">
                                        <DatePicker className="input_pass" dateFormat="DD/MM/YYYY" maxDate={moment()} required={true} name="dob" onChange={
                                            (e) => this.selectChange(e, 'dob')} selected={this.state.dob ? moment(this.state.dob) : moment()} placeholderText="00/00/0000" onChangeRaw={handleDateChangeRaw} />
                                    </span>
                                </div>
                                <div className="col-md-4 pl-0 mb-3">
                                    <label>First Name:</label>
                                    <span className="input_2">
                                        <input className="input_pass" placeholder="First Name" type="text" name="firstname" value={this.state.firstname || ''} onChange={
                                            this.handleChange} data-rule-required="true" />
                                    </span>
                                </div>
                                <div className="col-md-4 pl-0 mb-3">
                                    <label>Middle Name:</label>
                                    <span className="input_2">
                                        <input className="input_pass" placeholder="Middel Name" type="text" name="middlename" value={this.state.middlename || ''} onChange={
                                            this.handleChange} />
                                    </span>
                                </div>
                                <div className="col-md-4 pl-0 mb-3">
                                    <label>Last Name:</label>
                                    <span className="input_2">
                                        <input className="input_pass" placeholder="Last Name" type="text" name="lastname" value={this.state.lastname || ''} onChange={
                                            this.handleChange} data-rule-required="true" />
                                    </span>
                                </div>
                                <div className="col-md-2 pl-0 mb-3">
                                    <label>Gender:</label>
                                    <span className="required">
                                        <Select clearable={false} className="custom_select my-select" data-rule-required={true} simpleValue={true} name="gender" onChange={
                                            (e) => this.selectChange(e, 'gender')} value={this.state.gender} searchable={false} options={Gender_option(0)} placeholder="Gender" data-rule-required="true" />
                                    </span>
                                </div>
                                <div className="col-md-2 pl-0 mb-3">
                                    <label>Medicare:</label>
                                    <span className="input_2">
                                        <input className="input_pass" placeholder="Medicare" type="text" name="medicare" value={this.state.medicare || ''} onChange={
                                            this.handleChange} />
                                    </span>
                                </div>
                                <div className="col-md-2 pl-0 mb-3">
                                    <label>CRN:</label>
                                    <span className="input_2">
                                        <input className="input_pass" placeholder="CRN" type="text" name="CRN" value={this.state.CRN || ''} onChange={
                                            this.handleChange} />
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4 offset-md-4 mt-3">
                            <button type="submit" disabled={this.state.loading} className="submit_button w-100">Submit</button>
                        </div>
                    </form>
                </Modal.Body>
            </Modal>
        )
    }
}