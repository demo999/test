import React, { Component }
from 'react';
import {Panel, Button, ProgressBar} from 'react-bootstrap';
import Modal from 'react-bootstrap/lib/Modal';
import jQuery from "jquery";
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import 'react-select-plus/dist/react-select-plus.css';
import moment from 'moment';

import { ToastContainer, toast }
from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {Link, Redirect }
from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import {postData, calendarColorCode, getOptionsSuburb, handleShareholderNameChange, handleAddShareholder, handleRemoveShareholder, getOptionsMember, getPercentage,handleDateChangeRaw} from '../../service/common';
import '../../service/jquery.validate.js';
import BigCalendar from 'react-big-calendar'
        import Toolbar from 'react-big-calendar/lib/Toolbar';
import 'react-big-calendar/lib/css/react-big-calendar.css'
    
class CreateShift extends Component {
    constructor(props) {
        super(props);
        this.state = {
            state: [],
            shift_date: '',
            requirement: [],
            location: [{address: '', state: '', suburb: '', postcode: ''}],
            preferred_member_ary:[{name:''}],
			popup:false
        }
    }
    
    createShift = (e) => {
        e.preventDefault();
        jQuery('#create_shift').validate({ignore: []});
        toast.dismiss();
        if(jQuery('#create_shift').valid()){
			
			var difference = this.getTimeDifferInHours();
        if (difference > 24) {
            this.errorMessage("Shift duration can maximum 24 hours.");
            return false;
        } else if (difference < 0) {
             this.errorMessage("Please check start date time and end date time.");
             return false;
        }
	
			
			
            this.setState({loading: true})
             postData('participant/Shift_Roster/create_shift', this.state).then((result) => {
				 
                if (result.status) {
                     toast.success('Your shift is waiting for approval.', {
                           position: toast.POSITION.TOP_CENTER,
                           hideProgressBar: true
                       });
                       setTimeout(() => this.setState({is_redirect: true}), 2000);
                }else{
                    toast.error(result.error, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                    this.setState({loading: false})
                }
               
        });
        }
    }
    
	getTimeDifferInHours = () => {
        var start_time = moment(this.state.start_time);
        var end_time = moment(this.state.end_time);

        const diff = end_time.diff(start_time);
        const diffDuration = moment.duration(diff);
        var dayHours = diffDuration.days() * 24
        return diffDuration.hours() + dayHours;
    }
	  errorMessage = (Message) => {
        toast.error(Message, {
            position: toast.POSITION.TOP_CENTER,
            hideProgressBar: true
        });
    }
	
    componentWillReceiveProps(newProps){
        this.setState({shift_date: newProps.shift_date})
    }
    
    setcheckbox = (idx, checked, tagType)=> { 
        var List = this.state[tagType];
        var state = {};
        if(List[idx].checked==undefined || List[idx].checked==false){
            List[idx].checked = true
         }else{
            List[idx].checked = false
        }
        state[tagType] = List;  
        this.setState(state); 
    }
    
    selectChange = (value, key) => {
        var state = {}
        state[key] = value
        this.setState(state);
    }
    
    handleShareholderNameChange = (obj, stateName, index, fieldName, value) => {
        var state = {};
        var tempField = {};
        var List = obj.state[stateName];
        List[index][fieldName] = value
        
        if(fieldName == 'state'){
            List[index]['suburb'] = {}
            List[index]['postal'] = ''
        }
        
        if(fieldName == 'suburb' && value){
             List[index]['postal'] = value.postcode
        }
   

        state[stateName] = List;
        obj.setState(state);
     }   
    
     componentDidMount() {
        postData('participant/Dashboard/get_state', {}).then((result) => {
            if (result.status) {
                var details = result.data
                this.setState({stateList: details});
            }
        });
        
        postData('participant/Dashboard/get_shift_requirement', { } ).then ((result) => {
            if (result.status) {
                this.setState({requirement: result.data.shift_data }, () => {   });
				this.setState({preferred_member: result.data.shift_member }, () => {   });				
            }
        });
    }

    render() {
        return ( 
                <div>
                <Header title={'Create New Shift'} />
                <section>
                    {this.state.is_redirect? <Redirect to='/shift_calander'  />: ''}
                    <div className="container">
                        <div className="row mt-3 mb-4">
                            <div className="col-lg-12">				
                                <div className="line"></div>     			
                                <div className="labels mt-2" align="right">
                                    
                                </div>				
                            </div>
                            <div className="col-md-12 back-arrow"><Link to="/shift_roster"><i className="icon icon-back-arrow"></i></Link></div>
                        </div>



                        <form id="create_shift">
                           
                                <SubHeading heading="Shift Details" />
                                <div className="row P_25_TB mt-2">
                                    
                                <div className="col-md-3 mb-3">
                                    <label>Start Date Time:</label>
                                    <span className="input_2 required-n">
                                       
									   <DatePicker onChangeRaw={handleDateChangeRaw} dateFormat="DD/MM/YYYY h:mm a" autoComplete="new-password"  placeholderText={"00/00/0000 00:00"} className="text-left input_pass"
            selected={this.state.start_time} name="start_time" onChange={(e) => this.selectChange(e, 'start_time')} 
            timeIntervals={15} showTimeSelect timeCaption="Time" required={true} maxDate={(this.state.end_time) ? moment().add(60, 'days') : moment().add(60, 'days')} minDate={moment()} minTime={(moment(this.state.start_time).format("DD-MM-YYYY") == moment().format("DD-MM-YYYY")) ? moment() : moment().hours(0).minutes(0)} maxTime={(this.state.end_time) ? moment(this.state.end_time) : moment().hours(23).minutes(59)}  />
										
									  
                                </span>
                            </div>
                            <div className="col-md-3 mb-3">
                                <label>End Date Time:</label>
                                <span className="input_2 required-n">                                    
									<DatePicker dateFormat="DD/MM/YYYY h:mm a" onChangeRaw={handleDateChangeRaw} autoComplete="new-password" placeholderText={"00/00/0000 00:00"} className="text-left input_pass" selected={this.state.end_time} name="end_time" onChange={(e) => this.selectChange(e, 'end_time')} showTimeSelect timeIntervals={15} timeCaption="Time" required={true} minDate={this.state.start_time ? moment(this.state.start_time) : moment()} maxDate={(this.state.start_time) ? moment(this.state.start_time).add(1, 'days') : moment().add(60, 'days')} minTime={(moment(this.state.start_time).format("DD-MM-YYYY") == moment(this.state.end_time).format("DD-MM-YYYY")) ? moment(this.state.start_time) : moment().hours(0).minutes(0)} maxTime={moment().hours(23).minutes(59)} />									
									
                            </span>
                        </div>
                         </div>

                        {this.state.location.map((address, index) => (                                  
                        <div className="row P_15_T mb-3" key={index+1}>
                            <div className="col-md-4  mb-3">
                                <label>Location: </label>
                                <span className="input_2 required-n">
                                    <input type="text" className="input_pass" data-rule-required="true" onChange={(e) => handleShareholderNameChange(this, 'location', index, 'address', e.target.value)} value={address.address} name={'location'+index}  placeholder="Unit #/Street #, Street Name" />
                                </span>
                            </div>
                            <div className="col-md-2 mb-3">
                                <label>State: </label>
                                <div className="required-n">
                                <Select name="state" clearable={false}  className="default_validation my-select" simpleValue={true} required={true} 
                                        value={address.state}  onChange={(e) => this.handleShareholderNameChange(this, 'location', index, 'state', e)} 
                                    options={this.state.stateList} placeholder="Please Select" />
                                </div>
                            </div>
                            <div className="col-md-3 mb-3">
                                <label>Suburb:</label> 
                                <div className="required-n">
                                    <Select.Async clearable={false} className="default_validation my-select srch_selct" required={true}
                                     name={'suburb'+index}   value={address.suburb} disabled={(address.state)? false: true} 
                                     loadOptions={(val) => getOptionsSuburb(val, address.state)} 
                                     onChange={(e) => this.handleShareholderNameChange(this, 'location', index, 'suburb', e)} 
                                                  options={this.state.stateList} placeholder="Please Select" />
                                </div>
                        </div>
                        <div className="col-md-2 mb-3">
                            <label>Postcode: </label> 
                            <span className="input_gradient required-n">
                                <input type="text" className="input_pass text-center" value={address.postal} onChange={(e) => handleShareholderNameChange(this, 'location', index, 'postal', e.target.value)}  data-rule-number="true" data-rule-required="true" minLength="3" maxLength="5"  name={'postcode'+index} placeholder="0000"  />
                            </span>
                        </div>


                        <div className="col-md-1 mb-3 align-self-end">
                            <label></label> 
                           
                                {index > 0 ? <button className="button_unadd mb_15" onClick={(e) => handleRemoveShareholder(this, e, index, 'location')}>
                                    <i  className="icon icon-decrease-icon icon_cancel_1" ></i>
                                </button> : (this.state.location.length == 3)? '':
                                <button onClick={(e) => handleAddShareholder(this, e, 'location', address)} className="add_i_icon default_but_remove mb_15"><i className="icon icon-add-icons"></i></button>}
                            
                        </div>

                    </div>
                    ))}
            
                    <SubHeading heading="Shift Requirements" secondLine={true} />
                     <div className="row">
                    
                    
                    <div className="col-lg-6 bb-1 text-left  pb-3">
                        <label>Shift Requirements:</label>
                        
                        <span className=""> 
                            <div className="multiple_checkbox px-0 py-0">
                                <div className="scroll_active_modal pt-0 px-0">
                                    <span>
                                        <div className="multiple_checkbox overflow-hidden text_8style px-0 py-0">
                                            <div className="scroll_active_modal">
                                                { this.state.requirement.map((value, key) => (
                                                <span key={key + 1} className="w_50 d-inline-block pb-2">
                                                    <input type='checkbox' name="Assistance_Services" className="checkbox1" onChange={(e) => this.setcheckbox(key,value.checked,'requirement')} checked={value.checked || ''}name="Assistance_Services"  />
                                                    <label>
                                                        <div className="d_table-cell"><span onClick={(e) => this.setcheckbox(key,value.checked,'requirement')}></span></div><div className="d_table-cell">{value.label}</div></label>
                                                </span>
                                                ))}
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </span>
                    </div>
                    </div>
                    
                    <SubHeading heading="Shift Preferred Member" secondLine={true} />
                     {this.state.preferred_member_ary.map((value, idx) => (
                        <div className="row P_25_T mb-3" key={idx}>
                            <div className="col-lg-4">
                                <label>Preferred Member:</label>
                                    <div className="search_icons_right">
                                     
                                    <Select
                                    className="my-select"
                                    name="form-field-name"
                                    value={value.name}
                                    options={this.state.preferred_member}
                                    placeholder=''
                                    onChange={(e)=> handleShareholderNameChange(this, 'preferred_member_ary', idx, 'name', e)}
                                    placeholder="Choose Prefered Member"
                                    />

                                    </div>
                            </div>
                            <div className="col-lg-2 col-md-2 P_20_T align-self-end">
                                {
                                    idx > 0 ? <button onClick={(e) => handleRemoveShareholder(this, e, idx, 'preferred_member_ary')} className="button_unadd">
                                            <i  className="icon icon-decrease-icon icon_cancel_1" ></i>
                                        </button> : (this.state.preferred_member_ary.length == 1)? '': <button className="add_i_icon" onClick={(e) => handleAddShareholder(this, e, 'preferred_member_ary', value)}>
                                            <i  className="icon icon-add-icons" ></i>
                                        </button>
                                }
                            </div>
                        </div>
                        ))
                    }

               
        <div className="row">
       
            <div className="col-md-4 offset-md-4 mt-5">
                <button type="submit" disabled={this.state.loading} onClick={this.createShift} className="submit_button w-100">Create Shift</button>                        
            </div> 
            </div>
            </form>
            </div>
            </section>            
            <Footer />
            </div>
            );
        }
}

export default CreateShift;

class SubHeading extends Component {
render() {
return <div className="row">
            {this.props.secondLine? <div className="col-lg-12 mt-3"><div className="line"></div></div>: ''}
            <div className="col-lg-12 my-3"><h4>{this.props.heading}:</h4></div>
            <div className="col-lg-12 mb-3"><div className="line"></div></div>
        </div>
    }
}