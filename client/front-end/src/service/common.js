import React, { Component } from 'react';
import jQuery from "jquery";
import moment from 'moment-timezone';
import { ROUTER_PATH, BASE_URL, LOGIN_DIFFERENCE } from '../config.js';
import { confirmAlert, createElementReconfirm } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
        import axios from 'axios';

//check user login or not
export function checkItsLoggedIn() {
    if (getLoginToken()) {
        window.location = ROUTER_PATH + 'Dashboard';
    }
}

export function checkItsNotLoggedIn() {
    check_loginTime();
    if (!getLoginToken()) {
        window.location = ROUTER_PATH;
    }
}

export function getLoginTIme() {
    return localStorage.getItem("dateTime")
}

export function setLoginTIme(dateTime) {
    localStorage.setItem("dateTime", dateTime);

}

export function checkLoginWithReturnTrueFalse() {
    if (!getLoginToken()) {
        return false
    } else {
        return true
    }
}


export function setRemeber(data) {
    var d = new Date();
    var cookieDays = 7;
    d.setTime(d.getTime() + (cookieDays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();

    setCookie('adminUsername', data.username, cookieDays);
    setCookie('adminPassword', data.password, cookieDays);
}

export function getRemeber() {
    var username = getCookie('adminUsername');
    var password = getCookie('adminPassword');
    var data = {username: ((username != undefined) ? username : ''), password: ((password != undefined) ? password : '')}
    return data;
}

export function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

export function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

export function check_loginTime() {
    var DATE_TIME = getLoginTIme();
    var server = moment(DATE_TIME)

    var currentDateTime = moment()

    const diff = currentDateTime.diff(server);
    const diffDuration = moment.duration(diff);

    if (diffDuration.days() > 0) {

        logout();
    } else if (diffDuration.hours() > 0) {
        logout();
    } else if (diffDuration.minutes() > LOGIN_DIFFERENCE) {
        logout();
    }
}

export function getLoginToken() {
    return localStorage.getItem("ocs_clienttoken603560")
}

export function setLoginToken(token) {
    localStorage.setItem("ocs_clienttoken603560", token);
}

export function logout() {
    //postData('admin/Login/logout', getLoginToken(ROUTER_PATH));
    localStorage.removeItem('ocs_clienttoken603560');
    localStorage.removeItem('ocs_clientid603560');
    window.location = ROUTER_PATH;
}

export function getFirstname() {
    return localStorage.getItem("firstname")
}

export function setFirstname(firstname) {
    localStorage.setItem("firstname", firstname);
}

export function getPercentage() {
    return localStorage.getItem("Percentage")
}

export function setPercentage(Percentage) {
    localStorage.setItem("Percentage", Percentage);
}

export function getPinToken() {
    return localStorage.getItem("ocs_clientid603560")
}
export function getOCSIdToken() {
    return localStorage.getItem("ocs_clientid603560")
}

export function setPinToken(token) {
    localStorage.setItem("ocs_clientid603560", token);
}

export function destroyPinToken() {
    localStorage.removeItem('ocs_clientid603560');
    window.location = 'admin/Login/dashboard';
}

export function getPermission() {
    return localStorage.getItem("permission")
}

export function setPermission(permission) {
    localStorage.setItem("permission", permission);
}

export function getQueryStringValue(key)
{
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}

export function postData(url, data) {
    var request_data = {'id': getOCSIdToken(), 'token': getLoginToken(), 'data': data}
    return new Promise((resolve, reject) => {
        fetch(BASE_URL + url, {
            method: 'POST',
            body: JSON.stringify({request_data})
        }).then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.token_status) {
                        logout();
                    }
                    if (responseJson.server_status) {
                        logout();
                    }
                    
                    // if token is verified then update date client side time
                    if (responseJson.status) {
                        setLoginTIme(moment())
                    }
                    
                    resolve(responseJson);
                })
                .catch((error) => {
                    reject(error);
                    console.error(error);
                });
    });
}

export function IsValidJson() {
    return true;
}

export function checkPinVerified() {
    if (!getPinToken()) {
        destroyPinToken();
    }
}

// arhive anything form table
export function archiveALL(id, table, msg, url) {
    var data = {id: id, table: table};

    if (!msg) {
        msg = 'Are you sure want to delete this.'
    }
    if (!url) {
        url = "participant/Dashboard/archive_all";
    }
    return new Promise((resolve, reject) => {
        confirmAlert({
            title: 'Confirmation', // Title dialog
            message: msg, // Message dialog
            childrenElement: () => '', // Custom UI or Component
            confirmLabel: 'Confirm', // Text button confirm
            cancelLabel: 'Cancel', // Text button cancel
            buttons: [
                {
                    label: 'Confirm',
                    onClick: () => postData(url, data).then((result) => {
                            resolve(result);
                        })
                },
                {
                    label: 'Cancel',
                    onClick: () => resolve({status: false})
                }
            ]
        })
    });
}

export function handleShareholderNameChange(obj, stateName, index, fieldName, value) {
    var state = {};
    var tempField = {};
    var List = obj.state[stateName];
    List[index][fieldName] = value

    state[stateName] = List;
    obj.setState(state);
}

export function handleAddShareholder(obj, e, stateName, object_array) {
    e.preventDefault();
    var state = {};
    var temp = object_array
    var list = obj.state[stateName];

    state[stateName] = list.concat([reInitializeObject(temp)]);
    obj.setState(state);
}


export function reInitializeObject(object_array) {
    var state = {}
    Object.keys(object_array).forEach(function (key) {
        state[key] = '';
    });
    return state;
}

export function handleRemoveShareholder(obj, e, index, stateName) {
    e.preventDefault();
    var state = {};
    var List = obj.state[stateName];

    state[stateName] = List.filter((s, sidx) => index !== sidx);
    obj.setState(state);
}

export function calendarColorCode(givenKey) {
    var myArray = [
        {value: '7', label: '#89e0a9'}, //confirmed light green
        {value: '2', label: '#e0da8c'}, //unconfirmed yellow
        {value: '5', label: '#ff7a7b'}, //cancelled  
        {value: '1', label: '#a3b5c7'}, //unfilled  	
                //cancelled  
    ];
    if (givenKey == 0) {
        return myArray;
    } else{
        var index = myArray.findIndex(x => x.value == givenKey)
        return myArray[index].label;
    }
}
export function getOptionsSuburb(input, state) {
    if (!input || (input.length < 3)) {
        return Promise.resolve({options: []});
    }

    return postData('participant/Dashboard/get_suburb?query=' + input + '&state=' + state, {}).then((result) => {
        return {options: result};
    });

}

export function getOptionsMember(e) {
    if (!e) {
        return Promise.resolve({options: []});
    }
    return postData('participant/Dashboard/get_member_name?query=' + e).then((response) => {
        return {options: response};
    });
}

export function handleDateChangeRaw(e) {
    e.preventDefault();
}

export function getTimeAgoMessage(DATE_TIME) {
    moment.tz.setDefault("Australia/Melbourne");
    var messageTime = moment(DATE_TIME)

    var currentDateTime = moment()

    const diff = currentDateTime.diff(messageTime);
    const diffDuration = moment.duration(diff);


    if (diffDuration.days() > 0) {
        return diffDuration.days() + ' Days.'
    } else if (diffDuration.hours() > 0) {
        return diffDuration.hours() + ' hr.'
    } else if (diffDuration.minutes() > 0) {
        return diffDuration.minutes() + ' Min.'
    } else {
        return 'Just now'
    }
}
export function postImageData(url, data) {
    data.append('id', getOCSIdToken());
    data.append('token', getLoginToken());

    return new Promise((resolve, reject) => {
        fetch(BASE_URL + url, {
            method: 'POST',
            body: data
        }).then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.token_status) {
                        logout();
                    }
                    if (responseJson.server_status) {
                        logout();
                    }
                    
                    // if token is verified then update date client side time
                    if (responseJson.status) {
                        setLoginTIme(moment())
                    }
                    
                    resolve(responseJson);
                })
                .catch((error) => {
                    reject(error);
                    console.error(error);
                });
    });
}

export const downloadAttachment = (url) => {
//    window.location.href = url;
    window.open(url, 'name');
}